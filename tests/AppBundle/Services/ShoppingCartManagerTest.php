<?php

namespace Tests\AppBundle\Services;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ShoppingCartManagerTest extends KernelTestCase
{
    private $shopCartManager;

    // Template à reprendre pour tester un service
    protected function setUp()
    {
        $kernel = self::bootKernel();
        $this->shopCartManager = $kernel
            ->getContainer()
            ->get('app.shopping_cart_manager');
    }

    /*
     * Test de la fonction _buildDate du service ShoppingCartManager
     * Il faut changer la façon de générer une date de test
     */
    // TODO : Tests à réadapter (au niveau des dates)

    public function testToday(){
        // Test sur le jour d'aujourd'hui avec le lendemain férié
        $dateNow = new DateTime();
        $daysClosed = array(4, 5, 6);
        $result = $this->shopCartManager->_buildDate($daysClosed, $dateNow);
        $this->assertEquals('2019-11-17', $result->format('Y-m-d'));

    }

    // Test quand le lendemain est un jour férié, en pleine journée
    public function testOnOneClosedDay(){
        $date = new DateTime();
        $daysClosed = array(0);
        $result = $this->shopCartManager->_buildDate($daysClosed, $date);
        $this->assertEquals('lundi 18 novembre', $result);
    }

    // Test quand le lendemain et le surlendemain sont des jours fériés, en pleine journée
    public function testOnTwoClosedDay(){
        $dateNow = new DateTime();
        $daysClosed = array(3, 4);
        $result = $this->shopCartManager->_buildDate($daysClosed, $dateNow);
        $this->assertEquals('vendredi 15 novembre', $result);
    }

    // Test quand le lendemain et le surlendemain et le sur-surlendemain sont des jours fériés, en pleine journée
    public function testOnThreeClosedDay(){
        $dateNow = new DateTime();
        $daysClosed = array(3, 4, 5);
        $result = $this->shopCartManager->_buildDate($daysClosed, $dateNow);
        $this->assertEquals('samedi 16 novembre', $result);
    }


}