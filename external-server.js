const net = require("net");
const fs = require("fs");
const path = require("path");
const xsock = require('cross-platform-sock');
const Loadable = require('react-loadable');
require('ignore-styles').default(['.sass', '.scss', '.less', '.css']);

const bundlePath = path.resolve("./web/build/shop/server/");
const unixSocketPath = path.resolve("./var/node.sock");

let bundleFileName = "bundle.js";
let currentArg;

// noinspection JSUnusedLocalSymbols
const ReactOnRailsPatch = require(path.join(bundlePath, 'react-on-rails-patch.js')).default;

class Handler {
    constructor() {
        this.queue = [];
        this.initialized = false;
    }

    handle(connection) {
        const callback = () => {
            connection.setEncoding("utf8");
            let data = [];
            let lastChar;
            connection.on("data", chunk => {
                data.push(chunk);
                lastChar = chunk.substr(chunk.length - 1);

                if (lastChar === "\0") {
                    data = data.join("");
                    // This function will be called by eval
                    // noinspection JSUnusedLocalSymbols
                    const handleResponse = (result) => {
                        connection.write(result);
                        console.log("Request processed");
                        connection.end();
                    };

                    eval(data.slice(0, -1));
                }
            });
        };

        if (this.initialized) {
            callback();
        } else {
            this.queue.push(callback);
        }
    }

    initialize() {
        console.log("Processing " + this.queue.length + " pending requests");
        var callback = this.queue.pop();
        while (callback) {
            callback();
            callback = this.queue.pop();
        }

        this.initialized = true;
    }
}

var handler = new Handler();

process.argv.forEach(val => {
    if (val[0] == "-") {
        currentArg = val.slice(1);
        return;
    }

    if (currentArg == "s") {
        bundleFileName = val;
    }
});

const fullBundlePath = path.join(bundlePath, bundleFileName);

try {
    fs.mkdirSync(bundlePath, {recursive: true});
} catch (e) {
    if (e.code != "EEXIST") throw e;
}

startListening();

fs.watchFile(fullBundlePath, curr => {
    if (curr && curr.blocks && curr.blocks > 0) {
        if (handler.initialized) {
            console.log(
                "Reloading server bundle must be implemented by restarting the node process!"
            );
            return;
        }

        startListening();
    }
});

function startListening() {
    require(fullBundlePath);

    console.log("Loaded server bundle: " + fullBundlePath);
    handler.initialize();

    console.time("PRELOAD_ASYNC_BUNDLES");

    Loadable.preloadAll().then(() => {
        console.timeEnd("PRELOAD_ASYNC_BUNDLES");

        const unixServer = net.createServer(function (connection) {
            handler.handle(connection);
        });

        const sock = xsock(unixSocketPath);

        xsock.unlink(sock, function () {
            unixServer.listen(sock);

            process.on("SIGINT", () => {
                unixServer.close();
                process.exit();
            });
        });
    }).catch((exception) => {
        console.log(exception);
    });
}