#!/bin/bash

php bin/console app:generate:catalog:jacob --env=prod;
php bin/console app:generate:catalog:jacob-particulier --env=prod;
cp var/jacob-products.csv mercurials/auto_importation/foodomarket/jacob;
cp var/jacob-particulier-products.csv mercurials/auto_importation/foodomarket/jacob-particulier;
sh importMercurialCommand.sh;
