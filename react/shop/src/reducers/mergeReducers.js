const mergeReducers = (...reducers) => {
    return (state, action) => {
        for (let reducer of reducers) {
            state = reducer(state, action);
        }

        return state;
    };
};

export default mergeReducers;
