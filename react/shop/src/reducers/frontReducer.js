import {Constants, States} from "shop/constants/frontConstants";
import {cloneDeep} from "lodash";
import Filter from "shop/utils/Filter";
import {frontState} from "shop/reducers/initialStates";

export default function frontReducer(state = frontState, action) {
    let products, shoppingListProducts, filteredShoppingListProducts;

    const assignProductNote = (product) => {
        if (product.cartItemId == action.cartItemId) {
            return Object.assign({}, product, {note: action.note});
        }

        return product;
    };

    switch (action.type) {
        case Constants.CLEAR_INITIAL_VALUES:
            const newState = cloneDeep(state);

            for (let property of action.properties) {
                if (newState.initialValues.hasOwnProperty(property)) {
                    delete newState.initialValues[property];
                }
            }

            return newState;
        case Constants.LOADING_RESOURCE:
            return Object.assign({}, state, {loading: action.loading});
        case Constants.CHANGE_STATE:
            return Object.assign({}, state, {currentState: action.newState});
        case Constants.LOGIN_ERROR:
            return Object.assign({}, state, {loginError: action.loginError});
        case Constants.LOGIN_TOKEN_RECEIVED:
            return Object.assign({}, state, {authToken: action.token});
        case Constants.LOGOUT:
            return Object.assign({}, state, {authToken: null});
        case Constants.TOGGLE_LEFT_PANEL:
            return Object.assign({}, state, {leftPanelVisibility: !state.leftPanelVisibility});
        case Constants.TOGGLE_RIGHT_PANEL:
            return Object.assign({}, state, {rightPanelVisibility: !state.rightPanelVisibility});
        case Constants.SHOW_LEFT_PANEL:
            return Object.assign({}, state, {leftPanelVisibility: true});
        case Constants.SHOW_RIGHT_PANEL:
            return Object.assign({}, state, {rightPanelVisibility: true});
        case Constants.HIDE_LEFT_PANEL:
            return Object.assign({}, state, {leftPanelVisibility: false});
        case Constants.HIDE_RIGHT_PANEL:
            return Object.assign({}, state, {rightPanelVisibility: false});
        case Constants.CREATE_SHOPPING_LIST:
            return Object.assign({}, state, {
                currentState: States.SHOPPING_LIST_CREATION,
                rightPanelVisibility: true,
                shoppingListProducts: [],
                filteredShoppingListProducts: [],
                shoppingListName: '',
                shoppingListId: null
            });
        case Constants.ADD_TO_SHOPPING_LIST:
            return Object.assign({}, state, {
                shoppingListProducts: [...state.shoppingListProducts, action.product],
                filteredShoppingListProducts: [...state.filteredShoppingListProducts, action.product]
            });
        case Constants.REMOVE_FROM_SHOPPING_LIST:
            products = [];

            state.shoppingListProducts.map((product) => {
                // noinspection JSUnresolvedVariable
                if (product.productId != action.product.productId || product.supplierId != action.product.supplierId) {
                    products.push(product);
                }
            });

            return Object.assign({}, state, {shoppingListProducts: products, filteredShoppingListProducts: products});
        case Constants.EDIT_SHOPPING_LIST:
            // noinspection JSUnresolvedVariable
            return Object.assign({}, state, {
                currentState: States.SHOPPING_LIST_EDITION,
                rightPanelVisibility: !state.isMobileOrTablet,
                shoppingListProducts: action.products,
                filteredShoppingListProducts: action.products,
                shoppingListName: action.listName,
                shoppingListId: action.id
            });
        case Constants.REMOVE_SHOPPING_LIST:
            return Object.assign({}, state, {currentState: States.SHOPPING_LIST_LISTING});
        case Constants.ORDER_SHOPPING_LIST:
            return Object.assign({}, state, {
                currentState: States.ORDER_CHOOSE_PRODUCTS,
                shoppingListProducts: action.products,
                filteredShoppingListProducts: action.products,
                shoppingListName: action.listName,
                shoppingListId: action.id
            });
        case Constants.UPDATE_SHOPPING_LIST_PRODUCT_QUANTITY:
            if (!action.product.sid) {
                return state;
            }

            shoppingListProducts = state.shoppingListProducts.map((product) => {
                // noinspection JSUnresolvedVariable
                if (product.productId == action.product.productId && product.supplierId == action.product.supplierId) {
                    product.quantity = action.quantity;
                    product.quantityUnitSelected = action.unit;
                }

                return product;
            });

            filteredShoppingListProducts = state.filteredShoppingListProducts.map((product) => {
                // noinspection JSUnresolvedVariable
                if (product.productId == action.product.productId && product.supplierId == action.product.supplierId) {
                    product.quantity = action.quantity;
                    product.quantityUnitSelected = action.unit;
                }

                return product;
            });

            return Object.assign({}, state, { shoppingListProducts: shoppingListProducts, filteredShoppingListProducts: filteredShoppingListProducts });
        case Constants.ADD_SHOPPING_LIST_ITEM_NOTE:
            return Object.assign({}, state, {
                shoppingListProducts: state.shoppingListProducts.map(assignProductNote),
                filteredShoppingListProducts: state.filteredShoppingListProducts.map(assignProductNote)
            });
        case Constants.FILTER_SHOPPING_LIST_PRODUCTS:
            return Object.assign({}, state, {
                filteredShoppingListProducts: Filter.filter(state.shoppingListProducts, action.terms, 'productName')
            });
        case Constants.ORDER_SINGLE_DONE:
            return Object.assign({}, state, { orderEmails: action.emails });
        case Constants.ORDER_ALL_DONE:
            return Object.assign({}, state, { orderEmails: action.emails });
        case Constants.FETCHED_PREVIOUS_ORDERS:
            return Object.assign({}, state, { currentState: States.PREVIOUS_ORDERS_LISTING, previousOrders: action.orders });
        case Constants.OPEN_DASHBOARD:
            return Object.assign({}, state, { currentState: States.DASHBOARD_HOME, url: action.url });
        default:
            return state;
    }
};
