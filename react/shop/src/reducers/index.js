import frontReducer from "./frontReducer";
import {frontState, cartState, debugState} from "shop/reducers/initialStates";
import {combineReducers} from "redux";
import {reducer as formReducer} from "redux-form";
import {connectRouter} from "connected-react-router";
import cartReducer from "shop/reducers/cartReducer";
// import mergeReducers from "shop/reducers/mergeReducers";

// Combine all reducers you may have here
export default (history) => combineReducers({
    router: connectRouter(history),
    frontState: frontReducer,
    shoppingCarts: cartReducer,
    debugState: (state = debugState) => state,
    form: formReducer,
});

export const initialStates = {
    frontState,
    debugState,
    shoppingCarts: cartState,
};
