import {States} from "shop/constants/frontConstants";

export const frontState = {
    loading: false,
    authToken: null,
    initialValues: null,
    schema: null,
    loginError: null,
    user: null,
    leftPanelVisibility: true,
    rightPanelVisibility: true,
    shoppingLists: [],
    currentState: States.SHOPPING_LIST,
    shoppingListProducts: [],
    filteredShoppingListProducts: [],
    previousOrders: [],
    totalSupplierProducts: 0,
    supplierProductsPage: 0,
    restaurants: [],
    restaurant: 0,
    suppliers: []
};

export const cartState = {};

export const debugState = {
    showRegisterModal: false,
    showFullRegisterModal: false
};
