import {Constants} from "shop/constants/frontConstants";
import {cartState} from "shop/reducers/initialStates";
import {
    addSupplierItem,
    forEachSuppliersItems,
    getItemByCartItemId,
    getSupplierItemsByCartId,
    removeSupplier,
    removeSupplierItem
} from "shop/utils/shoppingItems";

const cartReducer = (state = cartState, action) => {
    let shoppingCarts = state, cart;

    switch (action.type) {
        case Constants.ORDER_QUANTITY_CHANGED:
            if (action.product.cartItemId) {
                shoppingCarts = {
                    ...state,
                    [action.product.supplierId]: {
                        ...state[action.product.supplierId],
                        items: {
                            ...state[action.product.supplierId].items,
                            [action.product.productId]: {
                                ...state[action.product.supplierId].items[action.product.productId],
                                quantity: action.quantity,
                                quantityUnitSelected: action.unit
                            }
                        }
                    }
                };
            }

            return shoppingCarts;
        case Constants.ADD_ALL_TO_CART:
            shoppingCarts = {...state};

            for (let productAdded of action.products) {
                addSupplierItem(productAdded, shoppingCarts);
            }

            return shoppingCarts;
        case Constants.ADD_TO_CART:
            shoppingCarts = {...state};

            addSupplierItem(action.product, shoppingCarts);

            return shoppingCarts;
        case Constants.REMOVE_FROM_CART:
            shoppingCarts = {...state};

            removeSupplierItem(action.product.supplierId, action.product.productId, shoppingCarts);

            return shoppingCarts;
        case Constants.CARTS_EMPTIED:
            return {};
        case Constants.CART_EMPTIED:
            shoppingCarts = {...state};

            forEachSuppliersItems(shoppingCarts, (cart, supplierId) => {
                if (cart.id === action.cartId) {
                    removeSupplier(supplierId, shoppingCarts);
                    return true;
                }
            });

            return shoppingCarts;
        case Constants.ADD_SHOPPING_CART_ITEM_NOTE:
            const product = getItemByCartItemId(action.cartItemId, state);

            return {
                ...state,
                [product.supplierId]: {
                    ...state[product.supplierId],
                    items: {
                        ...state[product.supplierId].items,
                        [product.productId]: {
                            ...state[product.supplierId].items[product.productId],
                            note: action.note
                        }
                    }
                }
            };
        case Constants.ADD_CART_NOTE:
            cart = getSupplierItemsByCartId(action.cartId, state);

            return {
                ...state,
                [cart.supplierId]: {
                    ...state[cart.supplierId],
                    note: action.note
                }
            };
        case Constants.ORDER_SCHEDULED:
            cart = getSupplierItemsByCartId(action.cartId, state);

            return {
                ...state,
                [cart.supplierId]: {
                    ...state[cart.supplierId],
                    scheduledDate: action.scheduledDate
                }
            };
        default:
            return state;
    }
};

export default cartReducer;
