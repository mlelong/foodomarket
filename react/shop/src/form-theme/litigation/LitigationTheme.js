import SemanticUITheme from "shop/form-theme/semanticui/SemanticUITheme";
import PhotoArrayWidget from "./PhotoArrayWidget";

const LitigationTheme = {
    ...SemanticUITheme,
    "photo-array": PhotoArrayWidget
};

export default LitigationTheme;
