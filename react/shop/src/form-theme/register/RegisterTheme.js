import SemanticUiTheme from "shop/form-theme/semanticui/SemanticUITheme";
import CategoryMultipleWidget from "shop/form-theme/register/CategoryMultipleWidget";
import SupplierMultipleWidget from "shop/form-theme/register/SupplierMultipleWidget";
import TimeslotWidget from "shop/form-theme/register/TimeslotWidget/TimeslotWidget";

const RegisterTheme = {
    ...SemanticUiTheme,
    "category-multiple-choice": CategoryMultipleWidget,
    "supplier-multiple-choice": SupplierMultipleWidget,
    timeslot: TimeslotWidget
};

export default RegisterTheme;
