import React, {useState} from "react";
import {Field} from "redux-form";
import TimeRangeSlider from "react-time-range-slider";
import BaseField from "shop/form-theme/semanticui/BaseField";
require("./TimeslotWidget.scss");

const timeslotRenderer = field => {
    let inputValue = {start: '08:00', end: '10:00'};

    if (field.input.value && field.input.value.length > 0 && /\d+:\d+-\d+:\d+/.test(field.input.value)) {
        inputValue = field.input.value.split('-');
        inputValue = {start: inputValue[0], end: inputValue[1]};
    }

    const [value, setValue] = useState(inputValue);

    const timeChangeHandler = (time) => {
        setValue(time);

        field.input.onChange(`${time.start}-${time.end}`);
    };

    return (
        <BaseField field={field}>
            {field.label && <label htmlFor={field.id} style={{marginBottom: '2em'}}>{field.label}</label>}
            <TimeRangeSlider
                id={field.id}
                disabled={false}
                format={24}
                maxValue={"13:00"}
                minValue={"06:00"}
                name={field.input.name}
                onChange={timeChangeHandler}
                step={120}
                value={value}
            />
            <p style={{textAlign: 'center', fontWeight: 'bold', marginTop: '1em'}}>Entre {value.start} et {value.end}</p>
        </BaseField>
    );
};

const TimeslotWidget = props => {
    return (
        <Field
            component={timeslotRenderer}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            type={props.type}
        />
    );
};

export default TimeslotWidget;
