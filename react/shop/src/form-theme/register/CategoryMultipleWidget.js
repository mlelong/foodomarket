import React from "react";
import {Field} from "redux-form";
import BaseField from "shop/form-theme/semanticui/BaseField";
import {Card, Checkbox, Image} from "semantic-ui-react";
import pictoFruitsVegetables from "img/categories-picto/fruits-vegetables.png";
import pictoCreamery from "img/categories-picto/creamery.png";
import pictoButchery from "img/categories-picto/butchery.png";
import pictoSeafood from "img/categories-picto/seafood.png";
import pictoGrocery from "img/categories-picto/grocery.png";

const pictograms = {
    'FRUITS_LEGUMES': pictoFruitsVegetables,
    'CREMERIE': pictoCreamery,
    'BOUCHERIE': pictoButchery,
    'MAREE': pictoSeafood,
    'EPICERIE': pictoGrocery,
};

const zipObject = (props, values) =>
    props.reduce(
        (prev, prop, i) => Object.assign(prev, { [prop]: values[i] }),
        {}
    );

const changeValue = (checked, item, onChange, currentValue = []) => {
    if (checked) {
        if (currentValue.indexOf(checked) === -1) {
            return onChange([...currentValue, item]);
        }
    } else {
        return onChange(currentValue.filter(it => it !== item));
    }

    return onChange(currentValue);
};

const renderChoice = field => {
    const options = field.schema.items.enum;
    const optionNames = field.schema.items.enum_titles || options;
    const selectOptions = zipObject(options, optionNames);

    const onChange = (e, {value, checked}) => changeValue(checked, value, field.input.onChange, field.input.value);

    return (
        <BaseField field={field}>
            <Card.Group itemsPerRow={5} centered doubling id={"register-categories"}>
                {Object.entries(selectOptions).map(([value, name]) => (
                    <Card raised key={value} link
                          onClick={() => onChange(null, {value, checked: field.input.value.indexOf(value) === -1})}
                          // style={{backgroundColor: 'rgba(63, 170, 52, 0.21)'}}
                    >
                        <Image src={pictograms[value]}/>

                        <Card.Content extra>
                            <Checkbox
                                label={name}
                                value={value}
                                key={value}
                                checked={field.input.value.indexOf(value) !== -1}
                                onChange={onChange}
                            />
                        </Card.Content>
                    </Card>
                ))}
            </Card.Group>
        </BaseField>
    );
};

const validateCategories = (value) => {
    if (value.length === 0) {
        return <p>Vous devez sélectionner au moins une catégorie</p>;
    }
};

const CategoryMultipleWidget = props => {
    return (
        <Field
            component={renderChoice}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            schema={props.schema}
            multiple={props.multiple}
            validate={props.context.validator ? props.context.validator : validateCategories}
            // validate={props.context.currentStep === 4 ? validateCategories : undefined}
        />
    );
};

export default CategoryMultipleWidget;
