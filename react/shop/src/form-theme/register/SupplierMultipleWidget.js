import React from "react";
import {Field} from "redux-form";
import BaseField from "shop/form-theme/semanticui/BaseField";
import {Card, Checkbox, Header, Icon, Image} from "semantic-ui-react";
import PropTypes from "prop-types";
import Logos from "shop/pages/account/SupplierLogos";

const SupplierCard = props => {
    return (
        <Card link raised
              // className={"supplier-logo-card"}
              onClick={() => props.onChange(null, {value: props.name, checked: !props.checked})}
        >
            <Card.Content>
                <div style={{textAlign: 'center'}}>
                    {props.logo.length > 0 && <Image src={props.logo}/> || <Icon color="grey" name={"ellipsis horizontal"} size={"huge"}/>}
                </div>
            </Card.Content>
            <Card.Content extra>
                <Checkbox
                    label={props.name}
                    value={props.name}
                    checked={props.checked}
                    onChange={props.onChange}
                />
            </Card.Content>
        </Card>
    );
};

SupplierCard.propTypes = {
    name: PropTypes.string.isRequired,
    logo: PropTypes.string.isRequired,
    checked: PropTypes.bool
};

SupplierCard.defaultProps = {
    checked: false
};

const SupplierCategory = props => {
    const {category} = props;
    let suppliers = [], categoryDisplay = '';

    switch (category) {
        case 'FRUITS_LEGUMES':
            categoryDisplay = "Fruits & Légumes";
            suppliers = [
                "Metro", "Halles Prestige", "Halles Mandar",
                "Daumesnil", "Primeurs Passion", "Bermudes",
                "Halles Trottemant", "Transgourmet", "Terre Azur",
                "Autre primeur"
                //"Verger Etoile"
            ];
            break ;
        case 'CREMERIE':
            categoryDisplay = "Crèmerie";
            suppliers = [
                "Alazard", "Metro", "Daumesnil",
                "Domafrais", "Jacob", "Buisson",
                "Delon", "ABC Peyraud", "Ugalait",
                "Autre crémier"
            ];
            break ;
        case 'BOUCHERIE':
            categoryDisplay = "Boucherie";
            suppliers = [
                "Chassineau", "Jacob",
                "Eurodis", "Activia", "Fory Viandes",
                "Martin", "Huguenin", "Prodal",
                "Metro", "Autre boucher"
            ];
            break ;
        case 'MAREE':
            categoryDisplay = "Marée";
            suppliers = [
                "Terre Azur", "Reynaud",
                "Domafrais", "Metro", "Demarne", "J'Oceane",
                "L'Ecrevisse", "Placet", "Daguerre",
                "Autre poissonnier"
            ];
            break ;
        case 'EPICERIE':
            categoryDisplay = "Epicerie";
            suppliers = [
                "Domafrais", "Daumesnil",
                "Jacob", "EpiSaveurs", "Metro",
                "Autre épicier"
            ];
            break ;
    }

    return (
        <div style={{display: props.active ? 'block' : 'none'}}>
            <Header textAlign="center" style={{fontWeight: 'bold', color: '#21ba45'}}>{categoryDisplay} ?</Header>
            <Card.Group itemsPerRow={5} doubling>
                {suppliers.map((supplier, index) => <SupplierCard name={supplier} logo={Logos.hasOwnProperty(supplier) ? Logos[supplier] : ''} key={index} onChange={props.onChange} checked={props.value !== undefined && props.value.includes(supplier)}/>)}
            </Card.Group>
        </div>
    );
};

SupplierCategory.propTypes = {
    category: PropTypes.oneOf(['FRUITS_LEGUMES', 'CREMERIE', 'BOUCHERIE', 'MAREE', 'EPICERIE']).isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.arrayOf(PropTypes.string),
    active: PropTypes.bool
};

SupplierCategory.defaultProps = {
    active: true
};

const changeValue = (checked, item, onChange, currentValue = []) => {
    if (checked) {
        if (currentValue.indexOf(checked) === -1) {
            return onChange([...currentValue, item]);
        }
    } else {
        return onChange(currentValue.filter(it => it !== item));
    }

    return onChange(currentValue);
};

const renderChoice = field => {
    const onChange = (e, {value, checked}) => changeValue(checked, value, field.input.onChange, field.input.value);

    const {categories, substep} = field;

    const category = categories.filter((category, index) => index == substep - 1)[0];

    return (
        <BaseField field={field}>
            {/*{categories.map((category, index) => <SupplierCategory key={index} category={category} onChange={onChange} value={field.input.value} active={index == substep - 1}/>)}*/}
            <SupplierCategory category={category} onChange={onChange} value={field.input.value} active={true}/>
        </BaseField>
    );
};

const validateSuppliers = (value) => {
    if (value.length === 0) {
        return <p>Vous devez sélectionner au moins un fournisseur ou "Autre" si non listé</p>;
    }
};

const SupplierMultipleWidget = props => {
    return (
        <Field
            component={renderChoice}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            schema={props.schema}
            multiple={props.multiple}
            categories={props.context.data.categories}
            substep={props.context.substep}
            validate={props.context.validator ? props.context.validator : validateSuppliers}
            // validate={props.context.currentStep === 5 ? validateSuppliers : undefined}
        />
    );
};

export default SupplierMultipleWidget;
