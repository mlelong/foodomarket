import React from "react";
import BaseInputWidget from "./BaseInputWidget";

const NumberWidget = props => {
    const normalizer = props.schema.widget === 'integer' ? parseInt : parseFloat;

    return <BaseInputWidget type="number" {...props} normalizer={(v) => v && normalizer(v).toString()} />;
};

export default NumberWidget;
