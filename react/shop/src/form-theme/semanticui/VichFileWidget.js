import React from "react";
import {Fields} from "redux-form";
import {renderField} from "liform-react";

const Vich = fields => {
    const {vich} = fields.schema;

    return (
        <div className="vich-file-widget">
            {fields.names.map((field) =>
                renderField(
                    fields.schema.properties[field],
                    `${fields.fieldName}[${field}]`,
                    fields.theme,
                    '',//fields.prefix,
                    fields.context,
                    fields.schema.hasOwnProperty('required') && fields.schema.required.includes(field)
                )
            )}
            {vich && vich.hasOwnProperty('download_uri') && <a target="_blank" href={vich.download_uri}>{vich.download_label}</a>}
        </div>
    );
};

const VichFileWidget = props => {
    return (
        <Fields
            component={Vich}
            names={Object.keys(props.schema.properties)}
            props={props}
        />
    );
};

export default VichFileWidget;
