import {DefaultTheme} from "liform-react";
import StringWidget from "./StringWidget";
import ChoiceWidget from "./ChoiceWidget";
import CheckboxWidget from "./CheckboxWidget";
import TextareaWidget from "./TextareaWidget";
import EmailWidget from "./EmailWidget";
import PasswordWidget from "./PasswordWidget";
import NumberWidget from "./NumberWidget";
import ChoiceMultipleExpandedWidget from "./ChoiceMultipleExpandedWidget";
import FileWidget from "./FileWidget";
import PasswordRepeatedWidget from "./PasswordRepeatedWidget";
import ChoiceExpandedWidget from "./ChoiceExpandedWidget";
import VichFileWidget from "./VichFileWidget";
import InlineFileWidget from "./InlineFileWidget";
import ArrayWidget from "./ArrayWidget";
import DropzoneWidget from "./DropzoneWidget";
import PlacesAutocompleteWidget from "shop/form-theme/semanticui/PlacesAutocompleteWidget";
import HiddenWidget from "shop/form-theme/semanticui/HiddenWidget";

const SemanticUITheme = {
    ...DefaultTheme,
    array: ArrayWidget,
    string: StringWidget,
    choice: ChoiceWidget,
    "choice-expanded": ChoiceExpandedWidget,
    "choice-multiple-expanded": ChoiceMultipleExpandedWidget,
    boolean: CheckboxWidget,
    checkbox: CheckboxWidget,
    textarea: TextareaWidget,
    email: EmailWidget,
    password: PasswordWidget,
    integer: NumberWidget,
    number: NumberWidget,
    file: FileWidget,
    "inline-file": InlineFileWidget,
    "vich-file": VichFileWidget,
    repeatedpassword: PasswordRepeatedWidget,
    dropzone: DropzoneWidget,
    "places-autocomplete": PlacesAutocompleteWidget,
    "hidden": HiddenWidget
};

export default SemanticUITheme;
