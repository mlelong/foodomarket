import React from "react";
import BaseInputWidget from "./BaseInputWidget";

require('./HiddenWidget.scss');

const HiddenWidget = props => {
    return <BaseInputWidget type="hidden" {...props} />;
};

export default HiddenWidget;
