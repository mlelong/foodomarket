import React, {useRef} from "react";
import {Field} from "redux-form";
import BaseField from "shop/form-theme/semanticui/BaseField";
import {Input} from "semantic-ui-react";
import Script from "react-load-script";

const PlacesAutocompleteRenderer = field => {
    const autocomplete = useRef(undefined);
    const {context} = field;

    const handlePlaceSelect = () => {
        const place = autocomplete.current.getPlace();

        field.input.onChange(place.formatted_address);

        for (let component of place.address_components) {
            let fieldName;

            switch (component.types[0]) {
                case 'postal_code':
                    fieldName = 'zipCode';
                    break ;
                case 'locality':
                    fieldName = 'city';
                    break ;
            }

            if (fieldName) {
                context.change(fieldName, component.short_name);
            }
        }
    };

    const handleScriptLoad = () => {
        // Declare Options For Autocomplete
        const options = { types: ['address'] };

        // Initialize Google Autocomplete
        /*global google*/
        autocomplete.current = new google.maps.places.Autocomplete(document.getElementById(field.id), options);
        // Restrict to france
        autocomplete.current.setComponentRestrictions({'country': ['fr']});
        // Avoid paying for data that you don't need by restricting the
        // set of place fields that are returned to just the address
        // components and formatted address
        autocomplete.current.setFields(['address_components', 'formatted_address']);
        // Fire Event when a suggested name is selected
        autocomplete.current.addListener('place_changed', handlePlaceSelect);
    };

    return (
        <BaseField field={field}>
            <Script url="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPN_7_svR5FjaOyFD6StAZjPgYMx9kYl8&libraries=places"
                    onLoad={handleScriptLoad}
            />

            {field.label && <label htmlFor={field.id}>{field.label}</label>}

            <Input autoComplete="off" disabled={field.disabled} id={field.id} fluid {...field.input} type={field.type} placeholder={field.placeholder}/>
        </BaseField>
    );
};

const PlacesAutocompleteWidget = props => {
    return (
        <Field
            component={PlacesAutocompleteRenderer}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            type={props.type}
            context={props.context}
        />
    );
};

export default PlacesAutocompleteWidget;
