import React, {useEffect, useState} from "react";
import {change, Field} from "redux-form";
import {Dropdown} from "semantic-ui-react";
import PropTypes from "prop-types";
import {zipObject as _zipObject} from "lodash";
import {connect} from "react-redux";
import BaseField from "./BaseField";
import entries from "shop/utils/entries";

const RenderSelect = field => {
    const [value, setValue] = useState(field.input.value);
    const {dispatch, context} = field;
    const options = field.schema.enum;
    const optionNames = field.schema.enum_titles || options;
    const selectOptions = _zipObject(options, optionNames);
    const suiOptions = [];

    if (!field.multiple && !field.required) {
        suiOptions.push({text: field.placeholder || '', value: ''});
    }

    for (let [key, value] of entries(selectOptions)) {
        suiOptions.push({text: value, value: key});
        // suiOptions.push({text: key, value: value});
    }

    useEffect(() => {
        dispatch(change(context.formName, field.input.name, value));
    });

    return (
        <BaseField field={field}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}

            <Dropdown
                multiple={field.multiple}
                search
                fluid
                selection
                name={field.input.name}
                value={value}
                options={suiOptions}
                id={field.id}
                onChange={(e, {value}) => setValue(value)}
            />
        </BaseField>
    )
};

const SelectComponent = connect()(RenderSelect);

const ChoiceWidget = (props) => {
    return (
        <Field
            component={SelectComponent}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            schema={props.schema}
            multiple={props.multiple}
            context={props.context}
        />
    )
};

ChoiceWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    fieldName: PropTypes.string,
    label: PropTypes.string,
    theme: PropTypes.object,
    multiple: PropTypes.bool,
    required: PropTypes.bool
};

export default ChoiceWidget;