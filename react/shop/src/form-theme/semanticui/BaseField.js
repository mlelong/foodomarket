import React from "react";
import {Form, Message} from "semantic-ui-react";

function BaseField(props)
{
    const {field} = props;
    let className = '';

    if (field.type === 'hidden') {
        className = 'hidden';
    }

    return (
        <Form.Field key={`f-${field.id}`} required={field.required} error={!!(field.meta.touched && field.meta.error)} className={className}>
            {field.description && <Message info content={field.description}/>}
            {props.children}
            {field.meta.touched && field.meta.error && <span className="error" style={{color: 'red'}}>* {field.meta.error}</span>}
        </Form.Field>
    );
}

export default BaseField;
