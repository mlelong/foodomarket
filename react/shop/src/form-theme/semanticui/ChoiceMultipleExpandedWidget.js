import React from "react";
import {Field} from "redux-form";
import BaseField from "./BaseField";
import {Checkbox} from "semantic-ui-react";

const zipObject = (props, values) =>
    props.reduce(
        (prev, prop, i) => Object.assign(prev, { [prop]: values[i] }),
        {}
    );

const changeValue = (checked, item, onChange, currentValue = []) => {
    if (checked) {
        if (currentValue.indexOf(checked) === -1) {
            return onChange([...currentValue, item]);
        }
    } else {
        return onChange(currentValue.filter(it => it !== item));
    }

    return onChange(currentValue);
};

const renderChoice = field => {
    const options = field.schema.items.enum;
    const optionNames = field.schema.items.enum_titles || options;
    const selectOptions = zipObject(options, optionNames);

    const onChange = (e, {value, checked}) => changeValue(checked, value, field.input.onChange, field.input.value);

    return (
        <BaseField field={field}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}

            {Object.entries(selectOptions).map(([value, name]) => (
                <div>
                    <Checkbox
                        label={name}
                        value={value}
                        key={value}
                        checked={field.input.value.indexOf(value) !== -1}
                        onChange={onChange}
                    />
                </div>
            ))}
        </BaseField>
    );
};

const ChoiceMultipleExpandedWidget = props => {
    return (
        <Field
            component={renderChoice}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            schema={props.schema}
            multiple={props.multiple}
        />
    );
};

export default ChoiceMultipleExpandedWidget;
