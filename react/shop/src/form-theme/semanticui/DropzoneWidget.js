import React, {useCallback, useRef, useState} from "react";
import {Field} from "redux-form";
import {Icon} from "semantic-ui-react";
import {useDropzone} from "react-dropzone";

const File = field => {
    const preview = useRef(null);
    const [hasFile, setHasFile] = useState(false);

    const onDrop = useCallback(acceptedFiles => {
        const fileList = inputRef.current.files;
        const reader = new FileReader();

        reader.onabort = () => console.log('file reading was aborted');
        reader.onerror = () => console.log('file reading has failed');
        reader.onload = () => {
            // Do whatever you want with the file contents
            const dataUrl = reader.result;
            preview.current.src = dataUrl;

            setHasFile(true);
        };

        acceptedFiles.forEach(file => reader.readAsDataURL(file));

        field.input.onChange(fileList);
    }, []);
    const {getRootProps, getInputProps, isDragActive, inputRef} = useDropzone({
        accept: 'image/*',
        onDrop
    });

    return (
        <div {...getRootProps()} className="dropzone-container">
            {hasFile && <Icon name='close' color='red' title={"Supprimer cette image"} onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();

                field.input.onChange([]);
                setHasFile(false);
            }}/>}

            <React.Fragment>
                {isDragActive ? <p>Déposer la photo ici ...</p> : (!hasFile ? <Icon size='big' name='camera' className='feedback'/> : <Icon color='green' size='big' name='checkmark' className='feedback'/>)}
            </React.Fragment>

            <img style={{display: hasFile ? 'block' : 'none'}} ref={preview} className='preview'/>
            <input name={field.input.name} {...getInputProps()}/>
        </div>
    );
};

const DropzoneWidget = props => {
    return (
        <Field
            component={File}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            type={"file"}
        />
    );
};

export default DropzoneWidget;
