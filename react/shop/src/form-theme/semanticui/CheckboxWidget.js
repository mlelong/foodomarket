import React, {useEffect, useState} from "react";
import {change, Field, unregisterField} from "redux-form";
import PropTypes from "prop-types";
import {Checkbox} from "semantic-ui-react";
import {connect} from "react-redux";
import BaseField from "./BaseField";

const RenderCheckbox = field => {
    const [checked, setChecked] = useState(false);
    const {dispatch, context} = field;

    useEffect(() => {
        if (checked) {
            dispatch(change(context.formName, field.input.name, checked))
        } else {
            dispatch(unregisterField(context.formName, field.input.name));
        }
    });

    return (
        <BaseField field={field}>
            <Checkbox required={field.required} id={field.id} checked={checked} label={field.label} name={field.input.name} onChange={(e, {checked}) => setChecked(checked)}/>
        </BaseField>
    );
};

const CheckboxComponent = connect()(RenderCheckbox);

const CheckboxWidget = props => {
    return (
        <Field
            component={CheckboxComponent}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            context={props.context}
        />
    );
};

CheckboxWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    fieldName: PropTypes.string,
    label: PropTypes.string,
    theme: PropTypes.object
};

export default CheckboxWidget;