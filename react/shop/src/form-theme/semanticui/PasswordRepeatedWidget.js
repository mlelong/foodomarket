import React from "react";
import {Field} from "redux-form";
import BaseField from "./BaseField";
import {Input} from "semantic-ui-react";

const RenderPassword = field => {
    return (
        <BaseField field={field}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}
            <Input disabled={field.disabled} id={field.id} fluid {...field.input} type={field.type} placeholder={field.placeholder}/>
        </BaseField>
    )
};

const validateRepeatedPassword = (value, allValues) => {
    if (allValues.password != allValues.password_again) {
        return 'Les mots de passe ne correspondent pas.'
    }
};

const PasswordRepeatedWidget = (props) => {
    return (
        <Field
            component={RenderPassword}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={'field-'+props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            validate={validateRepeatedPassword}
            type={"password"}
        />
    )
};

export default PasswordRepeatedWidget;
