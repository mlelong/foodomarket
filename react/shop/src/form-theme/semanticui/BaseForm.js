import React from "react";
import {Button, Form, Message} from "semantic-ui-react";
import SemanticUITheme from "./SemanticUITheme";
import {renderFields} from "liform-react";

const BaseForm = props =>
{
    const {schema, handleSubmit, error, submitting, context, theme} = props;

    return (
        <Form onSubmit={handleSubmit} loading={submitting}>
            {renderFields(schema, theme || SemanticUITheme, "", context)}

            {error && <Message visible error content={error}/>}

            <div style={{textAlign: 'center'}}>
                <Button type={"submit"} className={"blue"} primary disabled={submitting} loading={submitting}>Ok</Button>
            </div>
        </Form>
    );
};

export default BaseForm;
