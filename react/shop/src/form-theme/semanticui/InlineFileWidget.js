import React from "react";
import {Field} from "redux-form";
import BaseField from "./BaseField";
import {Input} from "semantic-ui-react";

const processFile = (onChange, e) => {
    const files = e.target.files;
    return new Promise(() => {
        let reader = new FileReader();
        reader.addEventListener(
            "load",
            () => {
                onChange(reader.result);
            },
            false
        );
        reader.readAsDataURL(files[0]);
    });
};

const File = field => {
    return (
        <BaseField field={field}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}

            <Input
                name={field.input.name}
                onBlur={field.input.onBlur}
                onChange={processFile.bind(this, field.input.onChange)}
                required={field.required}
                type="file"
            />
        </BaseField>
    );
};

const InlineFileWidget = props => {
    return (
        <Field
            component={File}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            type={"file"}
        />
    );
};

export default InlineFileWidget;
