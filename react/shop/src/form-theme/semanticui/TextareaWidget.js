import React from "react";
import {Field} from "redux-form";
import PropTypes from "prop-types";
import {TextArea} from "semantic-ui-react";
import BaseField from "./BaseField";

const RenderTextarea = field => {
    return (
        <BaseField field={field}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}

            <TextArea
                {...field.input}
                id={field.id}
                required={field.required}
                placeholder={field.placeholder}
            />
        </BaseField>
    );
};

const TextareaWidget = props => {
    return (
        <Field
            component={RenderTextarea}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.hasOwnProperty('attr') ? props.schema.attr.placeholder : ''}
            description={props.schema.description}
        />
    );
};

TextareaWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    fieldName: PropTypes.string,
    label: PropTypes.string,
    theme: PropTypes.object,
    multiple: PropTypes.bool,
    required: PropTypes.bool
};

export default TextareaWidget;
