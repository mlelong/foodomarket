import React from "react";
import {Field} from "redux-form";
import {Input} from "semantic-ui-react";
import BaseField from "./BaseField";
import PropTypes from "prop-types";

const RenderInput = field => {
    return (
        <BaseField field={field}>
            {field.type !== 'hidden' && field.label && <label htmlFor={field.id}>{field.label}</label>}
            <Input disabled={field.disabled} id={field.id} fluid {...field.input} type={field.type} placeholder={field.placeholder}/>
        </BaseField>
    )
};

const BaseInputWidget = props => {
    const disabled = props.schema.hasOwnProperty('attr') && props.schema.attr.hasOwnProperty('disabled') && props.schema.attr.disabled;

    return (
        <Field
            component={RenderInput}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            type={props.type}
            normalize={props.normalizer}
            disabled={disabled}
        />
    );
};

BaseInputWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    type: PropTypes.string.isRequired,
    required: PropTypes.bool,
    fieldName: PropTypes.string,
    label: PropTypes.string,
    normalizer: PropTypes.func
};

export default BaseInputWidget;
