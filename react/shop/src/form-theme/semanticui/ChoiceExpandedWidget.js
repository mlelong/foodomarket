import React from "react";
import {Field} from "redux-form";
import BaseField from "./BaseField";
import {Radio} from "semantic-ui-react";

const zipObject = (props, values) =>
    props.reduce(
        (prev, prop, i) => Object.assign(prev, { [prop]: values[i] }),
        {}
    );

const renderChoice = field => {
    const options = field.schema.enum;
    const optionNames = field.schema.enum_titles || options;

    const selectOptions = zipObject(options, optionNames);

    return (
        <BaseField field={field}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}

            {Object.entries(selectOptions).map(([value, name]) => {
                return (
                    <Radio
                        key={value}
                        label={name}
                        name={field.input.name}
                        value={value}
                        checked={field.input.value === value}
                        onChange={() => field.input.onChange(value)}
                    />
                );
            })}
        </BaseField>
    );
};

const ChoiceExpandedWidget = props => {
    return (
        <Field
            component={renderChoice}
            label={props.label}
            name={props.fieldName}
            required={props.required}
            id={"field-" + props.fieldName}
            placeholder={props.schema.default}
            description={props.schema.description}
            schema={props.schema}
            multiple={props.multiple}
        />
    );
};

export default ChoiceExpandedWidget;
