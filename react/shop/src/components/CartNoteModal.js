import React from "react";
import {Button, Icon, Modal} from "semantic-ui-react";
import {connect} from "react-redux";
import FrontInput from "./FrontInput";
import {addCartNote} from "shop/actions/index";

class CartNoteModal extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            note: props.cartNote,
            modalOpen: false
        };

        this.onNoteChanged = this.onNoteChanged.bind(this);
        this.onNoteAdded = this.onNoteAdded.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleOpen() {
        this.setState({ modalOpen: true });
    }

    handleClose() {
        this.setState({ modalOpen: false });
    }

    onNoteChanged(data) {
        this.setState({note: data});
    }

    onNoteAdded() {
        const {cartId} = this.props;

        this.props.dispatch(addCartNote(cartId, this.state.note));
        this.handleClose();
    }

    render() {
        const {added} = this.props;
        const {note} = this.state;

        const iconName = added ? 'commenting' : 'comment';
        const triggerClassName = added ? 'add-comment-button added' : 'add-comment-button';

        return (
            <Modal open={this.state.modalOpen} onClose={this.handleClose} trigger={<Button fluid className={triggerClassName} content={"Ajouter un commentaire"} icon={iconName} labelPosition={"left"} onClick={this.handleOpen}/>} closeIcon={<Icon name={"close"}/>} closeOnDocumentClick={true}>
                <Modal.Content>
                    <p>Notes pour la commande</p>

                    <FrontInput placeholder={"Mes commentaires..."} onChange={this.onNoteChanged} inputValue={note} />
                </Modal.Content>
                <Modal.Actions>
                    <Button className={"action-button"} onClick={this.onNoteAdded}>Ok</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default connect(state => (
    {
    }
))(CartNoteModal)