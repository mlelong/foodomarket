import React from "react";
import {Button, Dropdown, Input, Label} from "semantic-ui-react";
import {debounce} from "lodash";

export default class QuantityWidget extends React.Component {
    state = QuantityWidget.init(this.props);

    debouncedQuantityChanged = debounce(() => {
        if (this.props.onQuantityChanged !== undefined && !this.state.hasFocus) {
            this.props.onQuantityChanged(this.state.quantity, this.state.unit);
        }
    }, 500);

    static init(props) {
        const {product} = props;
        let quantity = 0, quantityUnit = 'ORDER_KGPC', quantityUnitSelected = undefined, units = [];

        if (product !== undefined) {
            quantity = (product.quantity !== undefined && product.quantity > 0) ? product.quantity : '';
            quantityUnit = product.quantityUnit;
            quantityUnitSelected = product.quantityUnitSelected;
        }

        switch (quantityUnit) {
            case 'ORDER_KG':
            case 'KG':
                units = [
                    {text: 'kg', value: 'KG'}
                ];
                quantityUnit = 'KG';

                if (quantityUnitSelected != quantityUnit) {
                    quantityUnitSelected = quantityUnit;
                    props.onQuantityChanged !== undefined && props.onQuantityChanged(quantity, quantityUnitSelected);
                }

                break ;
            case 'ORDER_PC':
            case 'PC':
                units = [
                    {text: 'pc', value: 'PC'}
                ];
                quantityUnit = 'PC';

                if (quantityUnitSelected != quantityUnit) {
                    quantityUnitSelected = quantityUnit;
                    props.onQuantityChanged !== undefined && props.onQuantityChanged(quantity, quantityUnitSelected);
                }

                break ;
            case 'ORDER_L':
            case 'L':
                units = [
                    {text: 'L', value: 'L'}
                ];
                quantityUnit = 'L';

                if (quantityUnitSelected != quantityUnit) {
                    quantityUnitSelected = quantityUnit;
                    props.onQuantityChanged !== undefined && props.onQuantityChanged(quantity, quantityUnitSelected);
                }

                break ;
            case 'ORDER_CO':
            case 'ORDER_COLIS':
            case 'COLIS':
            case 'CO':
                units = [
                    {text: 'co', value: 'CO'}
                ];
                quantityUnit = 'CO';

                if (quantityUnitSelected != quantityUnit) {
                    quantityUnitSelected = quantityUnit;
                    props.onQuantityChanged !== undefined && props.onQuantityChanged(quantity, quantityUnitSelected);
                }

                break ;
            default:
                units = [
                    {text: 'kg', value: 'KG'},
                    {text: 'pc', value: 'PC'}
                ];
                quantityUnit = 'PC';

                if (quantityUnitSelected != 'KG' && quantityUnitSelected != 'PC') {
                    quantityUnitSelected = quantityUnit;
                    props.onQuantityChanged !== undefined && props.onQuantityChanged(quantity, quantityUnitSelected);
                }

                break ;
        }

        if (quantityUnitSelected === undefined || quantityUnitSelected === null || quantityUnitSelected === '') {
            quantityUnitSelected = quantityUnit;
        }

        return {
            quantity: quantity,
            unit: quantityUnitSelected,
            units: units,
            hasFocus: false
        };
    }

    // noinspection JSUnusedLocalSymbols
    componentDidUpdate(prevProps, prevState) {
        if (!this.state.hasFocus
            && (prevProps.product.quantity != this.props.product.quantity
            || prevProps.product.quantityUnitSelected != this.props.product.quantityUnitSelected)) {
            this.setState(QuantityWidget.init(this.props));
        }
    }

    // noinspection JSUnusedLocalSymbols
    onKeyDown = (event, data) => {
        this.setState({hasFocus: true});

        if (event.key === 'Enter') {
            if (this.props.onEnterPressed !== undefined) {
                this.props.onEnterPressed();
            } else {
                if (this.props.onQuantityChanged !== undefined) {
                    this.props.onQuantityChanged(this.state.quantity, this.state.unit);
                }
            }
        }
    };

    subQuantity = () => {
        const quantity = parseFloat(this.getQuantity());

        if (quantity - 1 >= 0) {
            this.onQuantityChanged(null, {value: quantity - 1});
        } else {
            this.onQuantityChanged(null, {value: 0});
        }
    };

    addQuantity = () => {
        const quantity = parseFloat(this.getQuantity());

        this.onQuantityChanged(null, {value: quantity + 1});
    };

    getQuantity = () => {
        let {quantity} = this.state;

        if (quantity === undefined || quantity === null || quantity === '')
            quantity = 0;

        return `${quantity}`;
    };

    // noinspection JSUnusedLocalSymbols
    onQuantityChanged = (event, data) => {
        if (data.value != this.state.quantity) {
            this.setState({quantity: data.value});
        }

        this.debouncedQuantityChanged();
    };

    // noinspection JSUnusedLocalSymbols
    onUnitChanged = (event, data) => {
        if (data.value != this.state.unit) {
            this.setState({unit: data.value});

            if (this.props.onQuantityChanged !== undefined && !this.state.hasFocus) {
                this.props.onQuantityChanged(this.state.quantity, data.value);
            }
        }
    };

    onFocus = () => {
        const {quantity} = this.state;

        if (quantity <= 0) {
            this.setState({quantity: ''});
        }
    };

    onBlur = () => {
        const {quantity, unit} = this.state;

        if (this.props.onQuantityChanged !== undefined
            && (quantity != this.props.product.quantity || unit != this.props.product.quantityUnitSelected)) {
            this.props.onQuantityChanged(quantity, unit);
        }

        this.setState({hasFocus: false});
    };

    render() {
        const {quantity,unit,units} = this.state;
        const {hasError} = this.props;
        const className = hasError === true ? "quantity-widget has-error" : "quantity-widget";

        return (
            <div className={"quantity-widget-container"}>
                <Button icon={"minus"} onClick={this.subQuantity}/>
                <Input
                    className={className}
                    value={quantity}
                    as={Label}
                    transparent={true}
                    type={"number"}
                    min={0}
                    placeholder={"0"}
                    labelPosition={"right"}
                    label={{basic: true, content:<Dropdown onChange={this.onUnitChanged} inline options={units} value={unit} />}}
                    onChange={this.onQuantityChanged}
                    onKeyDown={this.onKeyDown}
                    pattern="[0-9]+([\.,][0-9]+)?"
                    step="any"
                    onClick={this.onFocus}
                    onBlur={this.onBlur}
                />
                <Button icon={"plus"} onClick={this.addQuantity}/>
            </div>
        );
    }
}
