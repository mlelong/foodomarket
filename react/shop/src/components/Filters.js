import React from "react";
import PropTypes from "prop-types";
import {Accordion, Card, Form, Icon} from "semantic-ui-react";

export default class Filters extends React.Component {
    static propTypes = {
        filters: PropTypes.arrayOf(PropTypes.shape({
            key: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            values: PropTypes.array.isRequired,
        })).isRequired,

        results: PropTypes.arrayOf(PropTypes.shape({
            filters: PropTypes.objectOf(PropTypes.array)
        })).isRequired,

        onResultsFiltered: PropTypes.func.isRequired,

        itemName: PropTypes.string.isRequired,
        itemNamePluralized: PropTypes.string.isRequired,
    };

    static defaultProps = {
        filters: [],
        results: []
    };
    
    state = {
        activeFilters: {},
        filteredResults: this.props.results
    };

    filter = (name, value, data) => {
        const {activeFilters} = this.state;
        const {results} = this.props;
        const newState = {};

        if (data.checked) {
            newState.activeFilters = {...activeFilters, [name]: activeFilters.hasOwnProperty(name) ? [...activeFilters[name], value] : [value]};
        } else {
            newState.activeFilters = {...activeFilters, [name]: activeFilters[name].filter(e => e != value)};
        }

        newState.filteredResults = results.filter(result => {
            for (let key in newState.activeFilters) {
                if (newState.activeFilters.hasOwnProperty(key) && newState.activeFilters[key].length > 0) {
                    if (result.filters.hasOwnProperty(key)) {
                        if (!result.filters[key].some(f => newState.activeFilters[key].includes(f))) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }

            return true;
        });

        this.setState(newState);
    };

    clearFilter = (event, name) => {
        event.preventDefault();
        event.stopPropagation();

        const {activeFilters} = this.state;
        const {results} = this.props;

        if (activeFilters.hasOwnProperty(name)) {
            delete activeFilters[name];
        }

        this.setState({
            activeFilters: activeFilters,
            filteredResults: results.filter(result => {
                for (let key in activeFilters) {
                    if (activeFilters.hasOwnProperty(key) && activeFilters[key].length > 0) {
                        if (result.filters.hasOwnProperty(key)) {
                            return result.filters[key].some(f => activeFilters[key].includes(f));
                        }
                    }
                }

                return true;
            })});
    };

    clearFilters = (event) => {
        event.preventDefault();

        this.setState({activeFilters: {}, filteredResults: this.props.results});
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.results != this.props.results) {
            this.setState({filteredResults: this.props.results});
        }

        if (prevState.filteredResults != this.state.filteredResults) {
            this.props.onResultsFiltered(this.state.filteredResults);
        }
    }

    render() {
        const {filters, results, itemName, itemNamePluralized} = this.props;
        const {activeFilters, filteredResults} = this.state;

        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>Filtres <a onClick={(e) => this.clearFilters(e)} href="#" style={{float: 'right', color: '#DB2828', fontSize: '10px'}}><Icon name={"remove"} size={"small"}/> Tout réinitialiser</a></Card.Header>
                    <Card.Meta>{filteredResults.length} {filteredResults.length > 1 && itemNamePluralized || itemName}</Card.Meta>
                    <Card.Description>
                        <Accordion exclusive={false} panels={
                            filters.map((filter, index) => {
                                    return {
                                        key: index,
                                        title:
                                            <Accordion.Title className={"filter-accordion-title"}>
                                                <Icon name={"dropdown"}/>{filter.name}
                                                {activeFilters.hasOwnProperty(filter.key) && activeFilters[filter.key].length > 0 &&
                                                <a onClick={(e) => this.clearFilter(e, filter.key)}
                                                   href="#"
                                                   style={{
                                                       float: 'right',
                                                       color: '#DB2828',
                                                       fontSize: '10px'
                                                   }}>
                                                    <Icon name={"remove"} size={"small"}/> Réinitialiser
                                                </a>
                                                }
                                            </Accordion.Title>,
                                        content:
                                            <Accordion.Content>
                                                <Form>
                                                    <Form.Group grouped>
                                                        {filter.values.map((f, index2) => {
                                                            let resultsForFilterValue;
                                                            const itemsMatchingCounter = (items) => items.reduce((previousValue, result) => {
                                                                if (result.filters.hasOwnProperty(filter.key) && result.filters[filter.key].includes(f.id)) {
                                                                    for (let key in activeFilters) {
                                                                        if (key !== filter.key) {
                                                                            if (activeFilters.hasOwnProperty(key) && activeFilters[key].length > 0) {
                                                                                if (result.filters.hasOwnProperty(key)) {
                                                                                    if (!result.filters[key].some(f => activeFilters[key].includes(f))) {
                                                                                        return previousValue;
                                                                                    }
                                                                                } else {
                                                                                    return previousValue;
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    return previousValue + 1;
                                                                }

                                                                return previousValue;
                                                            }, 0);

                                                            resultsForFilterValue = itemsMatchingCounter(results);

                                                            return <Form.Checkbox
                                                                key={`${index}-${index2}`}
                                                                label={`${f.name} (${resultsForFilterValue})`}
                                                                checked={activeFilters.hasOwnProperty(filter.key) && activeFilters[filter.key].includes(f.id)}
                                                                onChange={(event, data) => this.filter(filter.key, f.id, data)}
                                                            />
                                                        })}
                                                    </Form.Group>
                                                </Form>
                                            </Accordion.Content>
                                    };
                                }
                            )
                        }/>
                    </Card.Description>
                </Card.Content>
            </Card>
        );
    }
}
