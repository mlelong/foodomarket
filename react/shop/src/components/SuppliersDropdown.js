import React from "react";
import {connect} from "react-redux";
import {Icon, Image, List, Popup} from "semantic-ui-react";

class SuppliersDropdown extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            activeSupplier: 0,
            isOpen: false
        };

        this.onSupplierSelected = this.onSupplierSelected.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleOpen() {
        this.setState({ isOpen: true });
    }

    handleClose() {
        this.setState({ isOpen: false });
    }

    // noinspection JSUnusedLocalSymbols
    onSupplierSelected(event, data) {
        if (data !== undefined) {
            const selectedSupplier = data['data-supplier'];

            this.setState({activeSupplier: selectedSupplier.supplierId, isOpen: false});

            if (this.props.onSupplierSelected !== undefined) {
                this.props.onSupplierSelected(selectedSupplier);
            }
        }
    }

    render() {
        const {suppliers} = this.props;
        let selectedItem;

        const list = suppliers.map((supplier, index) => {
            const className = "shopping-list-supplier-item" + ((supplier.supplierId == this.state.activeSupplier) ? " active" : "");

            const listItem = (
                    <List.Item key={index} data-supplier={supplier} className={className} onClick={this.onSupplierSelected}>
                        <Image src={supplier.supplierPicture}/>
                        <List.Content>
                            <h4>{supplier.supplierName}</h4>
                            {supplier.supplierProducts} produits
                        </List.Content>
                        <Icon name={"angle right"}/>
                    </List.Item>
            );

            if (supplier.supplierId == this.state.activeSupplier) {
                selectedItem = listItem;
            }

            return listItem;
        });

        if (selectedItem === undefined) {
            selectedItem = list[0];
        }

        return (
            <Popup trigger={<List>{selectedItem}</List>}
                   hoverable={false}
                   on={"click"}
                   open={this.state.isOpen}
                   position={"bottom left"}
                   onClose={this.handleClose}
                   onOpen={this.handleOpen}>
                <List>
                    {list}
                </List>
            </Popup>
        );
    }
}

export default connect(state => (
    {
    }
))(SuppliersDropdown)