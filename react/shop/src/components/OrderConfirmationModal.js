import React from "react";
import {connect} from "react-redux";
import {Button, Icon, Modal, Tab} from "semantic-ui-react";
import {generateUrl} from "shop/router";

class OrderConfirmationModal extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            modalOpen: true,
            // percent: 0,
            // index: 0
        };

        this.closeModal = this.closeModal.bind(this);

        // this.sendMail = this.sendMail.bind(this);
    }

    // sendMail() {
    //     const {emails} = this.props;
    //
    //     if (this.state.index < emails.length) {
    //         Get(generateUrl('app_front_sendMail', {id: emails[this.state.index].shoppingCartId})).then((response) => {
    //             if (response.ok) {
    //                 return response.json();
    //             } else {
    //                 throw Error(response.statusText);
    //             }
    //         }).then((data) => {
    //             if (data.ok) {
    //                 this.setState({index: this.state.index + 1, percent: (this.state.index + 1) * 100 / emails.length});
    //             } else {
    //                 throw Error(data.message);
    //             }
    //         }).catch((exception) => {
    //             console.log("Could not send email to supplier: ", exception);
    //         });
    //     } else {
    //         this.setState({modalOpen: false});
    //         document.location = generateUrl('app_front_index');
    //     }
    // }

    closeModal() {
        this.setState({modalOpen: false});
        document.location = generateUrl('app_front_index');
    }

    render() {
        const {emails} = this.props;

        const panes = emails.map((email) => {
            return {
                menuItem: email.supplierName,
                render: () => (
                    <Tab.Pane attached={false}>
                        <div dangerouslySetInnerHTML={{ __html: email.content }} />
                    </Tab.Pane>
                )
            }
        });

        // const buttonText = this.state.index < emails.length
        //     ? 'Envoyer le mail à ' + emails[this.state.index].supplierName
        //     : 'Terminer'
        // ;

        const buttonText = 'Fermer';

        return (
            <Modal open={this.state.modalOpen} closeIcon={<Icon name={"close"}/>} closeOnDocumentClick={false} closeOnDimmerClick={false} onClose={this.closeModal}>
                <Modal.Header>Commande effectuée avec succès !</Modal.Header>
                <Modal.Content>
                    {/*<Progress percent={this.state.percent} indicating />*/}
                    {/*<Tab activeIndex={this.state.index} menu={{ secondary: true, pointing: true }} panes={panes} />*/}
                    <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
                </Modal.Content>
                <Modal.Actions>
                    {/*<Button color='green' onClick={this.sendMail}>*/}
                    <Button color='green' onClick={this.closeModal}>
                        <Icon name='checkmark'/> {buttonText}
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default connect(state => (
    {

    }
))(OrderConfirmationModal)