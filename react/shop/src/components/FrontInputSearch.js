import React from "react";
import {Button, Icon, Input} from "semantic-ui-react";
import {connect} from "react-redux";

class FrontInputSearch extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            inputValue: props.inputValue !== null && props.inputValue !== undefined ? props.inputValue : ''
        };

        this.onChange = this.onChange.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onIconClicked = this.onIconClicked.bind(this);
    }

    onIconClicked() {
        this.onKeyDown({key:'Enter'},null);
    }

    // noinspection JSUnusedLocalSymbols
    onChange(event, data) {
        this.setState({inputValue: data.value});

        if (this.props.onChange !== undefined) {
            this.props.onChange(data.value);
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({inputValue: nextProps.inputValue});
        this.forceUpdate();
    }

    // noinspection JSUnusedLocalSymbols
    onKeyDown(event, data) {
        if (event.key == 'Enter') {
            if (this.props.onOk !== undefined) {
                this.props.onOk(this.state.inputValue);
            }
        }
    }

    render() {
        const fluid = this.props.fluid !== undefined ? this.props.fluid : true;

        return (
            <Input
                value={this.state.inputValue}
                fluid={fluid}
                id={"input-search"}
                action={<Button id={"button-search"} onClick={this.onIconClicked} icon={"search"} />}
                placeholder={this.props.placeholder}
                onChange={this.onChange}
                onKeyDown={this.onKeyDown}
            />
        );
    }
}

export default connect(state => (
    {
    }
))(FrontInputSearch)