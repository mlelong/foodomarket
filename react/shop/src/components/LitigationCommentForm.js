import React from "react";
import {Button, Divider, Form, Grid, Icon} from "semantic-ui-react";
import {renderField} from "liform-react";
import {isRequired} from "liform-react/lib/renderFields";

const LitigationCommentForm = (props) => {
    const {submitting, schema, context, handleSubmit, theme} = props;

    return (
        <Grid.Row style={{paddingTop: 0}} className="comment-form-row">
            <Grid.Column width={16}>
                <Divider/>
                <Form loading={submitting} onSubmit={handleSubmit} className={'litigation-comment-form'}>
                    <Grid>
                        <Grid.Row style={{paddingBottom: '0px'}}>
                            <Grid.Column textAlign="center">
                                {renderField({...schema.properties['pictures'], showLabel: false}, 'pictures', theme, "", {...context}, isRequired(schema, 'pictures'))}
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row style={{paddingTop: '0px'}}>
                            <Grid.Column computer={13} tablet={12} mobile={16}>
                                {renderField({...schema.properties['content'], showLabel: false}, 'content', theme, "", {...context}, isRequired(schema, 'content'))}
                            </Grid.Column>
                            <Grid.Column computer={3} tablet={4} mobile={16} verticalAlign={'middle'}>
                                <Button icon labelPosition='left' type='submit' fluid className={'blue submit-comment'} loading={submitting}>
                                    <Icon name='send'/>
                                    Envoyer
                                </Button>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form>
            </Grid.Column>
        </Grid.Row>
    );
};

export default LitigationCommentForm;
