import React from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {Button, Form, Icon, Message} from "semantic-ui-react";
import {cloneDeep} from "lodash";
import {Post} from "shop/utils/fetch";
import {States} from "shop/constants/frontConstants";
import {generateUrl} from "shop/router";

class SimpleContactForm extends React.Component {
    state = {
        submitting: false,
        submitted: false,
        error: '',
        restaurantName: {value: '', error: false, required: true},
        email: {value: '', error: false, required: true},
    };

    onChange = (e, data) => {
        const {name} = data;
        const state = cloneDeep(this.state);

        state[name].value = data.value;
        state[name].error = !!(state[name].required && state[name].value == '');

        if (name == 'email') {
            const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2,3}|fr|de|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b/i;
            state[name].error = !reg.test(state[name].value);
        }

        this.setState(state);
    };

    submit = () => {
        const newState = cloneDeep(this.state);
        const fields = ['restaurantName', 'email'];
        let hasError = false;

        for (let field of fields) {
            newState[field].error = !!(newState[field].required && newState[field].value.length === 0);

            hasError = !hasError ? newState[field].error : hasError;
        }

        if (hasError) {
            this.setState(newState);
            return ;
        }

        this.setState({submitting: true});

        const {
            restaurantName,
            email
        } = this.state;

        const {match} = this.props;
        const isSearchPage = match.path === States.SEARCH_CATALOG;
        const isProductPage = match.path === States.PRODUCT;
        let postUrl;

        if (isSearchPage) {
            postUrl = 'app_front_no_product_found_register';
        } else if (isProductPage) {
            postUrl = 'app_front_custom_offer_request_register';
        }

        Post(generateUrl(postUrl), {
            restaurantName: restaurantName.value,
            email: email.value,
        }).then(data => {
            if (data.ok) {
                this.setState({error: '', submitting: false, submitted: true});
            } else {
                this.setState({error: data.message, submitting: false});
            }
        }).catch(exception => {
            this.setState({error: exception, submitting: false});
        });
    };

    render() {
        const {match} = this.props;
        const {error, submitting, submitted, restaurantName, email} = this.state;
        const isSearchPage = match.path === States.SEARCH_CATALOG;
        const isProductPage = match.path === States.PRODUCT;

        return (
            <Message>
                <Message.Content>
                    <Message.Header style={{textAlign: 'center', paddingBottom: '1em'}}>
                        {isSearchPage && "Contactez-nous pour nous faire part de vos besoins"}
                        {isProductPage && "Contactez-nous pour obtenir une offre personnalisée"}
                    </Message.Header>
                    {!submitted &&
                    <Form>
                        {error.length > 0 &&
                        <Message negative>
                            <Message.Header>Une erreur est survenue</Message.Header>
                            <p>{error}</p>
                        </Message>
                        }

                        <Form.Group widths='equal'>
                            <Form.Input
                                fluid
                                label="Restaurant"
                                placeholder='Nom de votre restaurant'
                                type='text'
                                name='restaurantName'
                                value={restaurantName.value}
                                error={restaurantName.error}
                                onChange={this.onChange}
                            />

                            <Form.Input
                                fluid
                                label="Email"
                                placeholder='Email'
                                type='email'
                                name='email'
                                value={email.value}
                                error={email.error}
                                onChange={this.onChange}
                            />
                        </Form.Group>

                        <Button loading={submitting} className={"register-next blue"} primary
                                onClick={this.submit}>Ok</Button>
                    </Form>
                    ||
                    <Message icon success>
                        <Icon name={"smile"}/>
                        <Message.Content>
                            <Message.Header>Votre demande a bien été prise en compte</Message.Header>
                            <p>Merci pour votre retour, nous reviendrons vers vous dans les plus brefs délais.</p>
                        </Message.Content>
                    </Message>
                    }
                </Message.Content>
            </Message>
        );
    }
}

export default withRouter(connect()(SimpleContactForm));