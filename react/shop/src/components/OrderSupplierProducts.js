import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Segment} from "semantic-ui-react";
import {addToCart, changeQuantity, removeFromCart} from "shop/actions/index";
import ProductList from "./ProductList";

class OrderSupplierProducts extends React.Component {
    static propTypes = {
        supplierId: PropTypes.number.isRequired,
        promoOnly: PropTypes.bool
    };

    static defaultProps = {
        promoOnly: false
    };

    addToCart = (product) => {
        this.props.dispatch(addToCart(product));
    };

    removeFromCart = (product) => {
        this.props.dispatch(removeFromCart(product));
    };

    updateQuantity = (product, quantity, quantityUnit) => {
        if (product.cartItemId) {
            this.props.dispatch(changeQuantity(product, quantity, quantityUnit));
        }
    };

    render() {
        const {shoppingCarts, promoOnly} = this.props;

        return (
            <>
                <h3 className={"border-titre2"}><span
                    className={"shopping-list-creation-step-nb"}>2. </span>
                    Produits à ajouter à votre panier
                </h3>
                <Segment className={"shopping-list-supplier-products-segment"}>
                    <ProductList
                        addedProducts={shoppingCarts}
                        supplierId={this.props.supplierId}
                        onProductAdded={this.addToCart}
                        onProductRemoved={this.removeFromCart}
                        onQuantityUpdated={this.updateQuantity}
                        promoOnly={promoOnly}
                    />
                </Segment>
            </>

        );
    }
}

export default connect(state => ({
    shoppingCarts: state.shoppingCarts
}))(OrderSupplierProducts);
