import React from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {Grid, Icon, Image, List} from "semantic-ui-react";
import {PriceWidget} from "./PriceWidget";
import QuantityWidget from "./QuantityWidget";
import {addToCart, changeQuantity, removeFromCart} from "shop/actions/index";
import ProductNoteModal from "./ProductNoteModal";

class ShoppingListOrderLine extends React.Component {
    static propTypes = {
        product: PropTypes.object.isRequired,
        onNoteAdded: PropTypes.func,
        onQuantityChanged: PropTypes.func,
        isReplacement: PropTypes.bool,
        orderEnabled: PropTypes.bool
    };

    static defaultProps = {
        isReplacement: false,
        orderEnabled: true
    };

    state = {
        errorQuantity: this.props.product.quantity <= 0,
        noteAdded: this.props.product.note && this.props.product.note.length > 0,
        quantity: this.props.product.quantity,
        unit: this.props.product.quantityUnitSelected,
        loading: false
    };

    onAddToCart = () => {
        const {product, orderEnabled} = this.props;
        const {quantity, unit} = this.state;

        if (quantity > 0 && orderEnabled) {
            this.setState({loading: true});
            this.props.dispatch(addToCart(Object.assign({}, product, {quantity, quantityUnitSelected: unit})));
        }
    };

    onRemoveFromCart = () => {
        const {product} = this.props;

        this.setState({loading: true});

        this.props.dispatch(removeFromCart(product));
    };

    onQuantityChanged = (quantity, unit) => {
        const {product, onQuantityChanged} = this.props;

        if (quantity > 0) {
            this.setState({errorQuantity: false, quantity, unit});

            if (onQuantityChanged !== undefined) {
                onQuantityChanged(product, quantity, unit);
            }

            this.props.dispatch(changeQuantity(product, quantity, unit));
        } else {
            this.setState({errorQuantity: true, quantity, unit});
        }
    };

    onNoteChanged = (product, note) => {
        this.setState({noteAdded: note && note.length > 0});

        const {onNoteAdded} = this.props;

        if (onNoteAdded) {
            onNoteAdded(product, note);
        }
    };

    componentDidUpdate(prevProps) {
        if (prevProps.product.cartItemId !== this.props.product.cartItemId) {
            this.setState({loading: false});
        }
    }

    render() {
        const {product, orderEnabled} = this.props;
        const {noteAdded} = this.state;
        const addedToCart = product.cartItemId !== undefined;
        const className = addedToCart ? "order-item-row added" : "order-item-row";
        const noteClassName = noteAdded ? "order-item-note note-added" : "order-item-note";
        const {errorQuantity, quantity} = this.state;
        const addToCartClass = errorQuantity || quantity == 0 ? "order-item-order has-error" : "order-item-order";
        // const addToCartClass = errorQuantity ? "order-item-order has-error" : "order-item-order";

        const isInCatalog = !(product.hasOwnProperty('isInCatalog') && product.isInCatalog === false);

        const listItemClassName = orderEnabled ? "order-line" : "order-line order-disabled";

        return (
            <List.Item className={listItemClassName}>
                <Grid stackable={true}>
                    <Grid.Row className={className + (!isInCatalog ? ' non-orderable' : '')}>
                        <Grid.Column computer={9} tablet={9} mobile={16}>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column computer={16} tablet={12} mobile={12} className={"order-item-name"}>
                                        <div>
                                            <Image src={product.productPicture} />
                                            <div>
                                                <span className={"product-name"}>{product.productName}</span> {product.origin.length > 0 && <span className={"product-origin"}>{product.origin}</span>}<br/>
                                                <span className={"supplier-name"}>{product.productSupplier}</span>
                                                {isInCatalog &&
                                                    <div className="order-item-price">
                                                        &nbsp;-&nbsp;<span className={"price-price"}>
                                                            <PriceWidget product={product}/>
                                                        </span>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </Grid.Column>
                                    {/*<Grid.Column computer={3} tablet={4} mobile={4} className={"order-item-price"} textAlign={"right"}>*/}
                                        {/*<div>*/}
                                            {/*{isInCatalog &&*/}
                                            {/*<span className={"price-price"}>*/}
                                                {/*{(product.quantity * product.productPrice / 100).toFixed(2)}€*/}
                                            {/*</span>*/}
                                            {/*}*/}
                                        {/*</div>*/}
                                    {/*</Grid.Column>*/}
                                </Grid.Row>
                            </Grid>
                        </Grid.Column>
                        <Grid.Column computer={7} tablet={7} mobile={16}>
                            {isInCatalog &&
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={8} className={"order-item-quantity"}>
                                        <div>
                                            <QuantityWidget hasError={errorQuantity}
                                                            onQuantityChanged={this.onQuantityChanged}
                                                            product={product}/>
                                        </div>
                                    </Grid.Column>
                                    <Grid.Column width={4} className={noteClassName}>
                                        <ProductNoteModal
                                            product={product}
                                            onChanged={this.onNoteChanged}
                                        />
                                    </Grid.Column>
                                    <Grid.Column width={4} className={addToCartClass}>
                                        {addedToCart &&
                                        <div onClick={this.onRemoveFromCart}>
                                            <Icon loading={this.state.loading} name={"checkmark"}/><br/>
                                            Ajouté
                                        </div> ||
                                        <div onClick={this.onAddToCart}>
                                            <Icon loading={this.state.loading} name={orderEnabled ? "plus cart" : "lock"}/><br/>
                                            Commander
                                        </div>
                                        }
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </List.Item>
        );
    }
}

export default connect(state => (
    {
        currentState: state.frontState.currentState
    }
))(ShoppingListOrderLine)