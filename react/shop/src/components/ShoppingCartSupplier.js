import React from "react";
import {
    Accordion,
    Button,
    Card,
    Checkbox,
    Dimmer,
    Form,
    Grid,
    Header,
    Icon,
    Label,
    List,
    Loader,
    Menu,
    Message,
    Modal
} from "semantic-ui-react";
import PropTypes from "prop-types";
import ShoppingListOrderLine from "./ShoppingListOrderLine";
import CartNoteModal from "./CartNoteModal";
import ScheduleWidget from "./utils/ScheduleWidget";
import moment from "moment";
import {countItems, forEachSupplierItems} from "shop/utils/shoppingItems";
import {gql} from "apollo-boost";
import {graphql} from '@apollo/react-hoc';
import {compose} from "redux";
import Input from "semantic-ui-react/dist/commonjs/elements/Input";
import {isEmpty} from "lodash";

const style = {
    confirmModalTitle: {
        textTransform: 'uppercase',
        marginTop: 25
    },
    saveCardMessage: {
        marginTop: 5,
        display: 'block'
    },
    checkIcon: {
        marginRight: 0,
        marginBottom: 20
    },
    successBox: {
        textAlign: 'center'
    },
    selectedCard: {
        backgroundColor: "#33d573"
    },
    addNewCard: {
        fontSize: '1rem',
    },
    or: {
        fontSize: '1.5rem',
        fontWeight: 700,
    },
    editCardTitle: {
        fontSize: '1rem',
    },
    cardLabelInput: {
        marginBottom: 10,
        width: "100%"
    },
    cardDeleteButton: {
        width: "100%"
    }
}

//@TODO: put cards in state??
//@TODO: put modal/card in separate component
class ShoppingCartSupplier extends React.Component {
    static propTypes = {
        onDateScheduled: PropTypes.func.isRequired,
        onOrder: PropTypes.func.isRequired
    };

    state = {
        active: true,
        modalOpen: false,
        createCardAlias: true,
        updateCardAlias: false,
        mutationLoading: false,
        mutationError: false,
        paymentWithAliasError: false,
        deleteAliasMutationLoading: false,
        deleteAliasMutationError: false,
        editAliasMutationLoading: false,
        editAliasMutationError: false,
        aliasEditMod: false,
        showCardDeletionConfirmation: false,
        successfulPayment: false,
        cardsLabels: {},
        aliasToUse: 0,
        scheduledDate: ScheduleWidget.getFirstSelectableDate(this.props.supplier.closingDays, this.props.data.scheduledDate).firstSelectableDate
    };

    static getDerivedStateFromProps(props, state) {
        const newState = {};

        if (props.data.scheduledDate && props.data.scheduledDate != state.scheduledDate) {
            newState.scheduledDate = props.data.scheduledDate;
        }

        if (props.lyraAliases.lyraAliases && isEmpty(state.cardsLabels)) {
            const aliases = {};
            props.lyraAliases.lyraAliases.map((alias) => {
                aliases[alias._id] = alias.label;
            })

            newState.cardsLabels = aliases;
        }

        if (!isEmpty(props.lyraAliases.lyraAliases)) {
            newState.createCardAlias = false;
        }

        if (newState) {
            return newState;
        }

        return null;
    }

    onOrder = () => {
        const {supplier, paymentShopOrder} = this.props;
        const {id: cartId} = this.props.data;
        const {createCardAlias, updateCardAlias, aliasToUse} = this.state;

        if (supplier.payment_method === 'credit_card_online') {
            this.setMutationState(true);
            paymentShopOrder({
                variables: {
                    shoppingCartId: cartId,
                    createAlias: !aliasToUse || createCardAlias,
                    useAlias: !!aliasToUse,
                    aliasId: aliasToUse,
                }
            }).then((response) => {
                this.setMutationState(false);
                if (response.data.paymentShopOrder.order.paymentException) {
                    this.setMutationErrorState(true);
                } else {
                    if (response.data.paymentShopOrder.order.paymentUrl && aliasToUse) {
                        if (response.data.paymentShopOrder.order.paymentUrl === "0") {
                            this.setSuccessfulPaymentStateState(true);
                            this.setPaymentWithAliasError(null);
                        } else {
                            this.setPaymentWithAliasError(response.data.paymentShopOrder.order.paymentUrl)
                            this.setMutationErrorState(true);
                        }
                    } else {
                        window.location.replace(response.data.paymentShopOrder.order.paymentUrl);
                    }
                }
            }).catch((e) => {
                console.log(e);
                this.setMutationState(false);
                this.setMutationErrorState(true);
            });
        } else {
            this.props.onOrder(cartId);
            this.closeModal();
        }
    };

    deleteLyraAlias = (aliasId) => {
        const {deleteLyraAlias, lyraAliases} = this.props;
        this.setShowCardDeletionConfirmation(false);
        this.setDeleteAliasMutationState(true);

        deleteLyraAlias({variables: {aliasId: aliasId}})
            .then((response) => {
                this.setDeleteAliasMutationErrorState(false);
                this.setDeleteAliasMutationState(false);
                lyraAliases.refetch();
            })
            .catch((e) => {
                console.log(e);
                this.setDeleteAliasMutationErrorState(true);
            });
    }

    validateAliasEdition = (aliasId) => {
        const {editLyraAlias} = this.props;
        const {cardsLabels} = this.state;

        if (isEmpty(cardsLabels[aliasId])) {
            this.setAliasEditModState(0);
            return false;
        }

        this.setEditAliasMutationState(true);
        editLyraAlias({variables: {aliasId: aliasId, label: cardsLabels[aliasId]}})
            .then((response) => {
                this.setEditAliasMutationErrorState(false);
                this.setEditAliasMutationState(false);
                this.setAliasEditModState(0);
            })
            .catch((e) => {
                console.log(e);
                this.setEditAliasMutationState(false);
                this.setEditAliasMutationErrorState(true);
            });

    }

    getScheduledDate = () => {
        const {scheduledDate} = this.state;
        const {supplier} = this.props;

        return ShoppingCartSupplier._getScheduledDate(scheduledDate, supplier.closingDays);
    };

    static _getScheduledDate(scheduledDate, supplierClosingDays) {
        // Pour dès que possible
        if (!scheduledDate || scheduledDate.length === 0) {
            // Le premier jour possible
            scheduledDate = moment(ScheduleWidget.getFirstSelectableDate(supplierClosingDays, null).firstSelectableDate, 'DD/MM/YYYY');

            // Demain
            const tomorrow = moment().add(1, 'day');

            // Est-ce que le premier jour possible est après demain ?
            // Si oui alors on se met sur demain quand même, histoire d'afficher un warning et d'attirer l'attention
            // sur le fait qu'on ne peut pas se faire livrer demain !!
            // TODO: à voir si on préfère faire ça ou si on met directement la première date possible
            // TODO: Si on veut la première date possible alors il faut juste supprimer la condition et l'affectation ci cessous
            // TODO: et aussi jarter la variable tomorrow qui ne sert plus à rien
            if (tomorrow.isBefore(scheduledDate)) {
                scheduledDate = tomorrow;
            }
        }

        return scheduledDate;
    }

    static _isScheduledDateValid(supplier, scheduledDate) {
        const scheduledMoment = moment(ShoppingCartSupplier._getScheduledDate(scheduledDate, supplier.closingDays), 'DD/MM/YYYY');
        const firstSelectableDate = moment(ScheduleWidget.getFirstSelectableDate(supplier.closingDays, null).firstSelectableDate, 'DD/MM/YYYY');

        if (scheduledMoment.isBefore(firstSelectableDate, 'day')) {
            return false;
        }

        const closingDays = supplier.closingDays;
        const orderLimitTime = supplier.orderLimitTime;

        return !(closingDays.length > 0 && closingDays.includes(scheduledMoment.day()));
    }

    isScheduledDateValid = () => {
        const {supplier} = this.props;
        const scheduledDate = this.getScheduledDate();

        return ShoppingCartSupplier._isScheduledDateValid(supplier, scheduledDate);
    };

    onDateScheduled = (date) => {
        const {id: cartId} = this.props.data;
        this.props.onDateScheduled(cartId, date);
    };

    toggle = () => {
        this.setState({active: !this.state.active});
    };

    getTotalAmount() {
        const shoppingCart = this.props.data;
        let totalSupplierPrice = 0;

        forEachSupplierItems(shoppingCart, (product) => {
            if (product.hasOwnProperty('productPrice') && product.hasOwnProperty('productPriceUnit')) {
                switch (product.quantityUnitSelected) {
                    case 'KG':
                        totalSupplierPrice += (product.productPrice * product.quantity) / 100;
                        break;
                    case 'PC':
                        // totalSupplierPrice += (product.productPriceUnit / product.unitQuantity * product.quantity) / 100;
                        totalSupplierPrice += (product.productPriceUnit * product.quantity) / 100;
                        break;
                    case 'L':
                        totalSupplierPrice += (product.productPrice * product.quantity) / 100;
                        break;
                    case 'CO':
                        totalSupplierPrice += (product.productPriceUnit * product.quantity) / 100;
                        break;
                    default:
                        break;
                }
            }
        });

        return totalSupplierPrice;
    }

    openModal = () => {
        this.setState({modalOpen: true});
    };

    closeModal = () => {
        this.setState({modalOpen: false});
    };

    toggleCreateCardAlias = () => {
        this.setState((prevState) => ({createCardAlias: !prevState.createCardAlias}))
    }

    toggleUpdateCardAlias = () => {
        this.setState((prevState) => ({updateCardAlias: !prevState.updateCardAlias}))
    }

    setMutationState = (isLoading) => {
        this.setState({mutationLoading: isLoading})
    }

    setDeleteAliasMutationState = (isLoading) => {
        this.setState({deleteAliasMutationLoading: isLoading})
    }

    setDeleteAliasMutationErrorState = (error) => {
        this.setState({deleteAliasMutationError: error})
    }

    setEditAliasMutationState = (isLoading) => {
        this.setState({editAliasMutationLoading: isLoading})
    }

    setEditAliasMutationErrorState = (error) => {
        this.setState({editAliasMutationError: error})
    }

    setMutationErrorState = (error) => {
        this.setState({mutationError: error})
    }

    setSuccessfulPaymentStateState = (success) => {
        this.setState({successfulPayment: success})
    }

    setAliasToUse = (id) => {
        this.setState({aliasToUse: id});
    }

    setAliasEditModState = (state) => {
        this.setState({aliasEditMod: state})
    }

    setAliasLabel = (aliasId, label) => {
        this.setState((prevState) => {
            prevState[aliasId] = label;
            return {cardsLabels: prevState};
        })
    }

    setPaymentWithAliasError = (error) => {
        this.setState({paymentWithAliasError: error})
    }

    setShowCardDeletionConfirmation = (state) => {
        this.setState({showCardDeletionConfirmation: state})
    }

    render() {
        const shoppingCart = this.props.data;
        const {supplierName, note: cartNote, id: cartId} = shoppingCart;
        const {index, supplier, lyraAliases, editLyraAlias} = this.props;
        const {
            scheduledDate,
            createCardAlias,
            aliasEditMod,
            mutationLoading,
            mutationError,
            successfulPayment,
            aliasToUse,
            cardsLabels,
            deleteAliasMutationLoading,
            deleteAliasMutationError,
            editAliasMutationError,
            editAliasMutationLoading,
            paymentWithAliasError,
            showCardDeletionConfirmation,
        } = this.state;
        const {active} = this.state;
        const deployLabelIcon = active ? "angle up" : "angle down";
        const totalSupplierPrice = this.getTotalAmount();
        const products = [];
        const nbItems = countItems(shoppingCart);

        forEachSupplierItems(shoppingCart, (product, index) => {
            products.push(<ShoppingListOrderLine key={index} product={product}/>);
        });

        const amountForFreeShippingCost = supplier.freePort;
        const minOrderAmount = supplier.minimumOrder;
        const limitOrderTime = supplier.orderLimitTime;
        const closingDays = supplier.closingDays;
        const isScheduledDateValid = this.isScheduledDateValid();
        const orderable = totalSupplierPrice >= minOrderAmount;
        const missingAmount = minOrderAmount - totalSupplierPrice;
        const cardAliases = lyraAliases.lyraAliases;

        let shippingCost = supplier.shippingCost;

        let orderableClass = orderable ? "" : "non-orderable";
        let scheduleErrorMessage;

        if (amountForFreeShippingCost && totalSupplierPrice > amountForFreeShippingCost) {
            shippingCost = 0;
        }

        if (!isScheduledDateValid) {
            orderableClass += " schedule-error";

            const scheduledMoment = moment(this.getScheduledDate(), 'DD/MM/YYYY');
            const dayName = scheduledMoment.locale('fr').format('dddd');

            scheduleErrorMessage = `Attention, ${supplierName} ne livre pas le ${dayName} !`;
        }

        return (
            <Menu.Item className={"shopping-cart-supplier " + orderableClass}>
                <Accordion.Title index={index} active={active} className={"shopping-cart-supplier-title"}>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={6}>
                                <Header className={"supplier-name"}>
                                    {supplierName}
                                    {scheduledDate && " pour le " + moment(scheduledDate, 'DD/MM/YYYY').format('dddd Do MMMM')}
                                </Header>
                                <span className={"supplier-nb-items"}>{nbItems} produits</span>
                            </Grid.Column>
                            <Grid.Column width={10} className={"middle-column"} textAlign={"right"}>
                                <div>
                                    {limitOrderTime && <span className={"limit-order-time"}><Icon name={"clock"}/>Commandez avant <span>{limitOrderTime}</span> pour être livré demain !</span>}
                                    <br/>
                                    <span className={"supplier-items-price"}>
                                        Montant de votre commande : {totalSupplierPrice.toFixed(2)}€ {shippingCost ? '+ Livraison ' + shippingCost.toFixed(2) + '€ = ' + (totalSupplierPrice + shippingCost).toFixed(2) + '€' : ''}
                                    </span>
                                </div>
                                <Label className={"big-deploy-label"} onClick={this.toggle}>
                                    <Icon name={deployLabelIcon}/>
                                    <br/>
                                    {active ? 'Masquer' : 'Montrer'} les {nbItems} produits
                                </Label>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Accordion.Title>
                <Accordion.Content active={active}>
                    {products}
                </Accordion.Content>
                <Grid stackable>
                    <Grid.Row columns={3} className={"action-buttons"}>
                        <Grid.Column>
                            <ScheduleWidget onDateScheduled={this.onDateScheduled} date={scheduledDate}
                                            disabledDays={closingDays}
                                            chronofreshDelivery={supplier.chronofreshDelivery}/>
                            {!isScheduledDateValid && <span className="indicator-message">{scheduleErrorMessage}</span>}
                        </Grid.Column>

                        <Grid.Column>
                            <CartNoteModal added={cartNote != '' && cartNote != null && cartNote != undefined}
                                           cartId={cartId} cartNote={cartNote}/>
                        </Grid.Column>

                        <Grid.Column>
                            <Button fluid className={"order-button"} content={"Passer la commande"} icon={"checkmark"}
                                    labelPosition={"left"} onClick={this.openModal}/>
                            <Modal className="schedule-modal" size="small" open={this.state.modalOpen}
                                   closeIcon={<Icon name={"close"} onClick={this.closeModal}/>}>
                                <Modal.Header>{successfulPayment ? "Commande validée" : "Récapitulatif de commande"}</Modal.Header>
                                {successfulPayment ? (
                                    <>
                                        <Modal.Content>
                                            <Grid container columns={1}>
                                                <Grid.Column>
                                                    <Message positive size='large' style={style.successBox}>
                                                        <Icon color="green" name="check circle outline" size="huge"
                                                              style={style.checkIcon}/>
                                                        <Message.Header>Merci pour votre commande !</Message.Header>
                                                        <p>
                                                            Votre compte bancaire ne sera débité qu'a l'émission de la
                                                            facture par le fournisseur.
                                                        </p>
                                                    </Message>
                                                </Grid.Column>
                                            </Grid>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <Button onClick={() => window.document.location.reload(true)}>
                                                <Icon name='close'/> Revenir au panier
                                            </Button>
                                        </Modal.Actions>
                                    </>
                                ) : (
                                    <>
                                        <Modal.Content>
                                            {isScheduledDateValid &&
                                            <Modal.Description>
                                                <Grid>
                                                    <Grid.Row columns={1}>
                                                        <Grid.Column>
                                                            <h4 style={style.confirmModalTitle}>
                                                                <Icon name="shopping cart" color="green"/> détails
                                                            </h4>
                                                            <Grid divided='vertically'>
                                                                <Grid.Row only='computer tablet'>
                                                                    <Grid.Column mobile={16} tablet={8} computer={8}>
                                                                        <List divided relaxed>
                                                                            <List.Item>
                                                                                <List.Header>Fournisseur :</List.Header>
                                                                                <List.Content>
                                                                                    {supplierName}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            <List.Item>
                                                                                <List.Header>Date de livraison
                                                                                    :</List.Header>
                                                                                <List.Content>
                                                                                    {moment(this.getScheduledDate(), 'DD/MM/YYYY').format('dddd Do MMMM')}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                        </List>
                                                                    </Grid.Column>
                                                                    <Grid.Column mobile={16} tablet={8} computer={8}>
                                                                        <List divided relaxed>
                                                                            <List.Item>
                                                                                <List.Header>Nombre d'article
                                                                                    :</List.Header>
                                                                                <List.Content>
                                                                                    {nbItems}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            <List.Item>
                                                                                <List.Header>Note :</List.Header>
                                                                                <List.Content>
                                                                                    {cartNote}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                        </List>
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                            </Grid>
                                                            <Grid divided='vertically'>
                                                                <Grid.Row only='mobile'>
                                                                    <Grid.Column mobile={16} tablet={8} computer={8}>
                                                                        <List divided relaxed>
                                                                            <List.Item>
                                                                                <List.Header>Fournisseur :</List.Header>
                                                                                <List.Content>
                                                                                    {supplierName}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            <List.Item>
                                                                                <List.Header>Date de livraison
                                                                                    :</List.Header>
                                                                                <List.Content>
                                                                                    {moment(this.getScheduledDate(), 'DD/MM/YYYY').format('dddd Do MMMM')}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            <List.Item>
                                                                                <List.Header>Nombre d'article
                                                                                    :</List.Header>
                                                                                <List.Content>
                                                                                    {nbItems}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            <List.Item>
                                                                                <List.Header>Note :</List.Header>
                                                                                <List.Content>
                                                                                    {cartNote}
                                                                                </List.Content>
                                                                            </List.Item>
                                                                        </List>
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                            </Grid>
                                                        </Grid.Column>
                                                        <Grid.Column>
                                                            <h4 style={style.confirmModalTitle}>
                                                                <Icon name="euro sign" color="green"/> Paiements
                                                            </h4>
                                                            <Grid divided='vertically'>
                                                                <Grid.Row>
                                                                    <Grid.Column mobile={16} tablet={8} computer={8}>
                                                                        <List divided relaxed>
                                                                            <List.Item>
                                                                                <List.Header>Motant total HT (approximatif)
                                                                                    :</List.Header>
                                                                                <List.Content>
                                                                                    {totalSupplierPrice.toFixed(2)} €
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            <List.Item>
                                                                                <List.Header>Frais de port
                                                                                    :</List.Header>
                                                                                <List.Content>
                                                                                    {shippingCost} €
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            <List.Item>
                                                                                <List.Header>Montant Total TTC
                                                                                    (approximatif) :</List.Header>
                                                                                <List.Content>
                                                                                    {(totalSupplierPrice * 1.055).toFixed(2)} €
                                                                                </List.Content>
                                                                            </List.Item>
                                                                            {supplier.payment_method === 'credit_card_online' && (
                                                                                <List.Item>
                                                                                    <List.Header>Montant de
                                                                                        l'autorisation
                                                                                        bancaire :</List.Header>
                                                                                    <List.Content>
                                                                                        {(totalSupplierPrice * 1.055 * 1.3).toFixed(2)} €
                                                                                    </List.Content>
                                                                                </List.Item>
                                                                            )}
                                                                        </List>
                                                                    </Grid.Column>
                                                                    <Grid.Column mobile={16} tablet={8} computer={8}>
                                                                        <h5>Mode de paiement</h5>
                                                                        {supplier.payment_method === 'credit_card_online' ? (
                                                                            <>
                                                                                {lyraAliases.loading || lyraAliases.networkStatus === 4 ? (
                                                                                    <Dimmer active inverted>
                                                                                        <Loader active inline='centered'
                                                                                                size='massive'/>
                                                                                    </Dimmer>
                                                                                ) : (
                                                                                    <>
                                                                                        {!isEmpty(cardAliases) ? (
                                                                                            <>
                                                                                                <p>Sélectionnez la carte
                                                                                                    avec
                                                                                                    laquelle vous
                                                                                                    souhaitez
                                                                                                    effectuer le
                                                                                                    paiement</p>
                                                                                                {cardAliases.map((card) => {
                                                                                                    return (
                                                                                                        <Card
                                                                                                            onClick={aliasEditMod ? false : () => this.setAliasToUse(card._id)}
                                                                                                            key={card._id}>
                                                                                                            <Card.Content
                                                                                                                style={aliasToUse === card._id && !aliasEditMod ? style.selectedCard : {}}>
                                                                                                                {aliasEditMod === card._id ? (
                                                                                                                    <>
                                                                                                                        <Card.Header
                                                                                                                            style={style.editCardTitle}><Icon
                                                                                                                            name="credit card outline"/> Edition
                                                                                                                            de
                                                                                                                            la
                                                                                                                            carte
                                                                                                                        </Card.Header>
                                                                                                                        <Card.Description>
                                                                                                                            <Input
                                                                                                                                style={style.cardLabelInput}
                                                                                                                                size="mini"
                                                                                                                                placeholder='Nom de la carte'
                                                                                                                                value={cardsLabels[card._id]}
                                                                                                                                onChange={(e) => this.setAliasLabel(card._id, e.target.value)}/>
                                                                                                                            {!showCardDeletionConfirmation && (
                                                                                                                                <Button
                                                                                                                                    style={style.cardDeleteButton}
                                                                                                                                    loading={deleteAliasMutationLoading}
                                                                                                                                    size='mini'
                                                                                                                                    content='Supprimer la carte'
                                                                                                                                    color="red"
                                                                                                                                    icon='trash alternate'
                                                                                                                                    labelPosition='left'
                                                                                                                                    onClick={() => this.setShowCardDeletionConfirmation(true)}/>
                                                                                                                            )}
                                                                                                                            {showCardDeletionConfirmation && (
                                                                                                                                <>
                                                                                                                                    <p>Souhaitez-vous
                                                                                                                                        vraiment
                                                                                                                                        supprimer
                                                                                                                                        cette
                                                                                                                                        carte
                                                                                                                                        ?</p>
                                                                                                                                    <Button
                                                                                                                                        size='mini'
                                                                                                                                        content='Oui'
                                                                                                                                        color="red"
                                                                                                                                        onClick={() => this.deleteLyraAlias(card._id)}/>
                                                                                                                                    <Button
                                                                                                                                        size='mini'
                                                                                                                                        content='Non'
                                                                                                                                        onClick={() => this.setShowCardDeletionConfirmation(false)}/>
                                                                                                                                </>
                                                                                                                            )}
                                                                                                                            {(deleteAliasMutationError || editAliasMutationError) && (
                                                                                                                                <Message
                                                                                                                                    negative>
                                                                                                                                    Un
                                                                                                                                    problème
                                                                                                                                    est
                                                                                                                                    survenue
                                                                                                                                    lors
                                                                                                                                    de
                                                                                                                                    l'édition
                                                                                                                                    de
                                                                                                                                    la
                                                                                                                                    carte
                                                                                                                                </Message>
                                                                                                                            )}
                                                                                                                        </Card.Description>
                                                                                                                    </>
                                                                                                                ) : (
                                                                                                                    <>
                                                                                                                        <Card.Header><Icon
                                                                                                                            name="credit card outline"/> {card && card.label ? card.label : (
                                                                                                                            <>
                                                                                                                                {card && card.brand ? card.brand : ''}
                                                                                                                            </>
                                                                                                                        )}
                                                                                                                        </Card.Header>
                                                                                                                        <Card.Description>
                                                                                                                            <List>
                                                                                                                                <List.Item>{card && card.number ? card.number : ''}</List.Item>
                                                                                                                                <List.Item>Exp
                                                                                                                                    : {card && card.expirationDate ? moment(card.expirationDate).format('MM/YYYY') : ''}</List.Item>
                                                                                                                            </List>
                                                                                                                        </Card.Description>
                                                                                                                    </>
                                                                                                                )}

                                                                                                            </Card.Content>
                                                                                                            {aliasEditMod === card._id ? (
                                                                                                                <Button
                                                                                                                    content="Sauvegarder les modifications"
                                                                                                                    loading={editAliasMutationLoading}
                                                                                                                    color='green'
                                                                                                                    icon='checkmark'
                                                                                                                    onClick={(e) => {
                                                                                                                        e.stopPropagation();
                                                                                                                        this.validateAliasEdition(card._id)
                                                                                                                    }}/>
                                                                                                            ) : (
                                                                                                                <Button
                                                                                                                    content="Éditer la carte"
                                                                                                                    icon='edit'
                                                                                                                    onClick={(e) => {
                                                                                                                        e.stopPropagation();
                                                                                                                        this.setAliasEditModState(card._id)
                                                                                                                    }}/>
                                                                                                            )
                                                                                                            }
                                                                                                        </Card>)
                                                                                                })}
                                                                                                <p style={style.or}>OU</p>

                                                                                                <Card
                                                                                                    onClick={() => this.setAliasToUse(0)}>
                                                                                                    <Card.Content
                                                                                                        style={aliasToUse === 0 ? style.selectedCard : {}}>
                                                                                                        <Card.Header
                                                                                                            style={style.addNewCard}><Icon
                                                                                                            name="credit card outline"/> Ajouter
                                                                                                            une nouvelle
                                                                                                            carte
                                                                                                        </Card.Header>
                                                                                                    </Card.Content>
                                                                                                </Card>
                                                                                                <span
                                                                                                    style={style.saveCardMessage}>L'enregistrement s'effectuera au moment du paiement. Il est entièrement sécurisé par Lyra©</span>

                                                                                            </>
                                                                                        ) : (
                                                                                            <>
                                                                                                <Card>
                                                                                                    <Card.Content>
                                                                                                        <Card.Header><Icon
                                                                                                            name="credit card outline"/> Carte
                                                                                                            bancaire</Card.Header>
                                                                                                        <Card.Description>
                                                                                                            <List>
                                                                                                                <List.Item>Aucune
                                                                                                                    carte
                                                                                                                    enregistrée</List.Item>
                                                                                                            </List>
                                                                                                        </Card.Description>
                                                                                                    </Card.Content>
                                                                                                </Card>
                                                                                                <Form.Field>
                                                                                                    <Checkbox
                                                                                                        label='Enregistrer ma carte'
                                                                                                        checked={createCardAlias}
                                                                                                        onChange={this.toggleCreateCardAlias}/>
                                                                                                    <span
                                                                                                        style={style.saveCardMessage}>L'enregistrement s'effectuera au moment du paiement. Il est entièrement sécurisé par Lyra©</span>
                                                                                                </Form.Field>
                                                                                            </>
                                                                                        )
                                                                                        }
                                                                                    </>
                                                                                )}
                                                                            </>
                                                                        ) : (
                                                                            <Card>
                                                                                <Card.Content>
                                                                                    <Card.Header><Icon
                                                                                        name="university"/> Virement /
                                                                                        Prélévement</Card.Header>
                                                                                </Card.Content>
                                                                            </Card>
                                                                        )}
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                            </Grid>
                                                        </Grid.Column>
                                                    </Grid.Row>
                                                </Grid>
                                                {supplier.payment_method === 'credit_card_online' &&
                                                <Message info>
                                                    <Message.Header><Icon name="warning"/> A propos des paiements par
                                                        carte
                                                        bancaire</Message.Header>
                                                    <p>Lors d'un paiement par carte bancaire, et afin de prendre en
                                                        compte les
                                                        éventuelles variations de poids des articles, le montant total
                                                        de
                                                        l'autorisation bancaire est supérieur de 30% au total du panier.
                                                        Le
                                                        montant réellement
                                                        débité
                                                        sur votre compte sera lui ajusté au montant de la facture.</p>
                                                </Message>}
                                            </Modal.Description>
                                            }

                                            {!isScheduledDateValid &&
                                            <Modal.Description>
                                                <Message warning>
                                                    <p><b>{supplierName}</b> ne livre pas
                                                        le <b>{moment(this.getScheduledDate(), 'DD/MM/YYYY').format('dddd Do MMMM')}</b>.
                                                    </p>
                                                    <p>Nous vous invitons à choisir un autre jour de livraison ci
                                                        dessous:</p>
                                                </Message>
                                                <ScheduleWidget onDateScheduled={this.onDateScheduled}
                                                                date={scheduledDate}
                                                                disabledDays={closingDays} inline={true}/>
                                            </Modal.Description>
                                            }
                                        </Modal.Content>
                                        <Modal.Actions>
                                            {mutationError && (
                                                <Message negative>
                                                    <Message.Header style={{textAlign: 'left'}}>Un problème est
                                                        survenu</Message.Header>
                                                    {paymentWithAliasError === "51" && (
                                                        <p style={{textAlign: 'left'}}>
                                                            <strong>Cause de l'échéc</strong> : autorisation refusée
                                                            pour cause de plafond
                                                            dépassé.
                                                        </p>
                                                    )}
                                                    {paymentWithAliasError === "05" && (
                                                        <p style={{textAlign: 'left'}}>
                                                            <strong>Cause de l'échéc</strong> : autorisation refusée
                                                            suite à une erreur dans
                                                            le cryptogramme visuel saisi.
                                                        </p>
                                                    )}
                                                    {paymentWithAliasError !== "05" && paymentWithAliasError !== "51" && paymentWithAliasError !== false && (
                                                        <p style={{textAlign: 'left'}}>
                                                            <strong>Cause de l'échéc</strong> : échec de
                                                            l'autentification 3D Secure.
                                                        </p>
                                                    )}
                                                    <p style={{textAlign: 'left'}}>La commande n'a pu être validée. Si
                                                        le
                                                        problème persiste, contacter notre support</p>
                                                </Message>
                                            )}
                                            <Button onClick={this.closeModal}>
                                                <Icon name='close'/> Revenir au panier
                                            </Button>
                                            {isScheduledDateValid &&
                                            <Button color='green' loading={mutationLoading} onClick={this.onOrder}>
                                                <Icon name='checkmark'/> Confirmer la commande
                                            </Button>
                                            }
                                        </Modal.Actions>
                                    </>
                                )}
                            </Modal>

                            <span className="indicator-message">Il vous manque {missingAmount.toFixed(2)}€ pour atteindre votre minimum de commande</span>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Menu.Item>
        );
    }
}

export default compose(
    graphql(
        gql`
            mutation CreateWithPaymentShopOrder($shoppingCartId: String!, $createAlias: Boolean!, $useAlias: Boolean!, $aliasId: Int) {
                paymentShopOrder(input: {shoppingCartId: $shoppingCartId, createAlias: $createAlias, useAlias: $useAlias, orderLocation: "shop", aliasId: $aliasId}) {
                    order {
                        paymentUrl
                        paymentException
                        paymentStatus
                    }
                }
            }
        `, {name: 'paymentShopOrder'}
    ),
    graphql(
        gql`
            query lyraAliases {
                lyraAliases {
                    _id
                    number
                    brand
                    expirationDate
                    label
                }
            }
        `, {name: 'lyraAliases'}
    ),
    graphql(
        gql`
            mutation DeleteLyraAlias($aliasId: ID!) {
                deleteLyraAlias(input: {id: $aliasId}) {
                    lyraAlias {
                        _id
                    }
                }
            }
        `, {name: 'deleteLyraAlias'}
    ),
    graphql(
        gql`
            mutation EditLyraAlias($aliasId: Int!, $label: String!) {
                editLyraAlias(input: {id: $aliasId, label: $label}) {
                    lyraAlias {
                        _id
                        label
                    }
                }
            }
        `, {name: 'editLyraAlias'}
    ),
)(ShoppingCartSupplier);
