import React from "react";
import PropTypes from "prop-types";
import {Button, Modal} from "semantic-ui-react";
import {connect} from "react-redux";
import FrontInput from "./FrontInput";
import {addShoppingCartItemNote, addShoppingListItemNote} from "shop/actions/index";
import Icon from "semantic-ui-react/dist/es/elements/Icon/Icon";
import {States} from "shop/constants/frontConstants";
import {withRouter} from "react-router-dom";

class ProductNoteModal extends React.Component {
    static propTypes = {
        product: PropTypes.object.isRequired,
        onChanged: PropTypes.func
    };

    state = {
        note: this.props.product.note,
        modalOpen: false
    };

    handleOpen = () => {
        this.setState({ modalOpen: true });
    };

    handleClose = () => {
        this.setState({ modalOpen: false });
    };

    onNoteChanged = (data) => {
        this.setState({note: data});
    };

    onNoteAdded = () => {
        const {match, product, onChanged} = this.props;
        const {sid: shoppingListItemId, cartItemId: shoppingCartItemId} = product;
        const {note} = this.state;

        switch (match.path) {
            /**
             * Can add note on shopping list item or cart item independently based on product state (added to cart or list only)
             * Only available from Shopping list order page, otherwise adds note to cart item only
             */
            case States.SHOPPING_LIST_ORDER:
                if (shoppingCartItemId !== undefined) {
                    this.props.dispatch(addShoppingCartItemNote(shoppingCartItemId, note));
                }

                if (shoppingListItemId !== undefined) {
                    this.props.dispatch(addShoppingListItemNote(shoppingListItemId, note));
                }
                break ;
            case States.ORDER_CART_SUMMARY:
            case States.ORDER_PREVIOUS_ORDER:
                if (shoppingCartItemId !== undefined) {
                    this.props.dispatch(addShoppingCartItemNote(shoppingCartItemId, note));
                }

                break ;
        }

        if (onChanged !== undefined) {
            onChanged(product, note);
        }

        this.handleClose();
    };

    render() {
        const {productName, productSupplier: supplierName} = this.props.product;
        const {modalOpen, note} = this.state;
        const added = note !== undefined && note !== null && note.length > 0;

        const iconName = added ? 'commenting' : 'comment';

        return (
            <Modal open={modalOpen} onClose={this.handleClose} trigger={<div onClick={this.handleOpen}><Icon name={iconName}/><br/>Notes</div>} closeIcon={<Icon name={"close"}/>} closeOnDocumentClick={true}>
                <Modal.Content>
                    <p>Notes pour le produit <span className={"modal-product-name"}>{productName}</span>
                        &nbsp;à destination de <span className={"modal-product-name"}>{supplierName}</span>
                    </p>

                    <FrontInput placeholder={"Mes commentaires..."} onChange={this.onNoteChanged} inputValue={note} />
                </Modal.Content>
                <Modal.Actions>
                    <Button className={"action-button"} onClick={this.onNoteAdded}>Ok</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default withRouter(connect()(ProductNoteModal))