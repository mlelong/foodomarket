import React from "react";
import {connect} from "react-redux";
import {List} from "semantic-ui-react";
import ShoppingListProduct from "./ShoppingListProduct";
import FrontInput from "./FrontInput";
import {filterShoppingListProducts} from "shop/actions/index";

class ShoppingListProducts extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.onFilterProducts = this.onFilterProducts.bind(this);
    }

    onFilterProducts(terms) {
        this.props.dispatch(filterShoppingListProducts(terms));
    }

    render() {
        const { products } = this.props;
        const productsList = products.map((product, index) =>
            <ShoppingListProduct data={product} key={index} qty={product.quantity} qtyUnit={product.quantityUnitSelected}/>
        );

        return (
            <div>
                <FrontInput icon={"search"} placeholder={"Filtrez vos produits"} onChange={this.onFilterProducts }/>
                <List>
                    {productsList}
                </List>
            </div>
        );
    }
}

export default connect(state => (
    {
        products: state.frontState.filteredShoppingListProducts
    }
))(ShoppingListProducts)