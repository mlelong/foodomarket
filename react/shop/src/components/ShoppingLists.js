import React from "react";
import {connect} from "react-redux";
import {Button, Grid, Icon, List} from "semantic-ui-react";
import {Link} from "react-router-dom";
import ShoppingList from "./ShoppingList";
import FrontInput from "./FrontInput";
import {Constants, States} from "shop/constants/frontConstants";
import getShoppingLists from "shop/utils/ShoppingListFetcher";

class ShoppingLists extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            shoppingLists: []
        };

        this.onFilterLists = this.onFilterLists.bind(this);
    }

    componentDidMount() {
        this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: true });

        getShoppingLists((data) => {
            if (data !== null) {
                this.setState({shoppingLists: data.data});
            }

            this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: false });
        });
    }

    onFilterLists(terms) {
        this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: true });

        getShoppingLists((data) => {
            if (data !== null) {
                this.setState({shoppingLists: data.data});
            }

            this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: false });
        }, terms);
    }

    render() {
        const {shoppingLists} = this.state;
        const shoppingListsElements = shoppingLists.map((list) =>
            <ShoppingList data={list} key={list.id} shoppingListId={list.id}/>
        );

        return (
            <div>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={12}>
                            <FrontInput icon='search' placeholder="Filtrez vos listes d'achats..." onOk={this.onFilterLists}/>
                        </Grid.Column>
                        <Grid.Column width={4} only={"computer"} textAlign={"right"}>
                            <Link to={States.SHOPPING_LIST + States.SHOPPING_LIST_CREATE}>
                                <Button basic icon={"plus"} labelPosition={"left"} content={"Ajouter une liste"}/>
                            </Link>
                        </Grid.Column>
                        <Grid.Column width={4} only={"tablet mobile"} textAlign={"right"}>
                            <Link to={States.SHOPPING_LIST + States.SHOPPING_LIST_CREATE}>
                                <Button basic>
                                    <Icon name={"plus"}/>
                                </Button>
                            </Link>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <List className={"shopping-lists"}>
                                {shoppingListsElements}
                            </List>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

export default connect()(ShoppingLists)