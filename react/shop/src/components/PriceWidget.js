import React from "react";

export function PriceWidget(props) {
    const {product} = props;
    const {explicitContent, priceDetail} = product;

    return (
        <span className="price-widget">
            {explicitContent.length > 0 && explicitContent + ' - '}
            {priceDetail.price}€<span className="price-unit">{priceDetail.unit.length > 0 && `/${priceDetail.unit}`}{priceDetail.detail.length > 0 && priceDetail.detail}</span>
            <span> Prix HT</span>
        </span>
    );
}
