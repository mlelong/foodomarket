import React from "react";
import {connect} from "react-redux";
import {Button, Card, Form, Grid, Message} from "semantic-ui-react";
import {login} from "shop/actions";
import {FoodoMarketLogo} from "./utils/FoodoMarketLogo";

class Login extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            username: '',
            password: '',
            rememberme: '',
            loading: false
        };

        this.setUsername = this.setUsername.bind(this);
        this.setPassword = this.setPassword.bind(this);
        this.setRememberMe = this.setRememberMe.bind(this);
        this.submit = this.submit.bind(this);
    }

    render() {
        const { loginError } = this.props;

        return (
            <Grid verticalAlign='middle' centered style={{height:'100vh',backgroundColor:'#f7f7f7'}}>
                <Grid.Row columns={1}>
                    <Grid.Column mobile={15} tablet={12} computer={5}>
                        <Card id={"login-card"} fluid>
                            <Card.Content>
                                <div style={{textAlign:'center',paddingBottom:'25px'}}>
                                    <FoodoMarketLogo color={'#000'} size={'36px'}/><br/>
                                    <h2 style={{fontWeight:'normal',fontFamily:'Roboto-Light,sans-serif',color:'#9a9a9a'}}>Bienvenue</h2>
                                </div>

                                { loginError &&
                                <Message negative>
                                    <Message.Header>Impossible de vous connecter</Message.Header>
                                    {loginError === 'BAD_CREDENTIALS' && <p>E-Mail ou mot de passe incorrect</p>}
                                    {loginError === 'USER_ACCOUNT_IS_DISABLED' && <p>Votre compte n'est pas activé</p>}
                                </Message>
                                }

                                <Form className={"login-form"} onSubmit={this.submit}>
                                {/*<Form className={"login-form"} action={this.props.checkPath} method={"post"}>*/}
                                    <Form.Input fluid name={"_username"} className="front-input" transparent onChange={this.setUsername} placeholder="adresse e-mail" type='email' value={this.state.username}/>
                                    <Form.Input fluid name={"_password"} className="front-input" transparent onChange={this.setPassword} placeholder='mot de passe' type='password' value={this.state.password}/>
                                    <Form.Checkbox name="_rememberme" label="Se souvenir de moi" value={this.state.rememberme} onChange={this.setRememberMe} />
                                    <Button loading={this.state.loading} className={"blue"} type='submit'>Se connecter</Button>
                                </Form>
                            </Card.Content>
                        </Card>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.loginError) {
            this.setState({loading: false});
        }
    }

    submit(e) {
        e.preventDefault();

        this.setState({loading: true});
        this.props.dispatch(login(this.state.username, this.state.password, this.state.rememberme));
    }

    setUsername(e) {
        this.setState({ username: e.target.value });
    }

    setPassword(e) {
        this.setState({ password: e.target.value });
    }

    setRememberMe(e) {
        this.setState({ rememberme: e.target.value });
    }
}

export default connect(state => (
    {
        loginError : state.frontState.loginError,
        checkPath: state.frontState.checkPath
    }
))(Login)
