import React from "react";
import {connect} from "react-redux";
import {Menu} from "semantic-ui-react";
import {Link, withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";

class PlaceOrderMenu extends React.Component {
    render() {
        const {match} = this.props;

        return (
            <Menu stackable={true} pointing className="place-order-menu" compact>
                <Menu.Item name="orderList" active={match.path === States.PREVIOUS_ORDERS_LISTING}>
                    <Link to={States.PREVIOUS_ORDERS_LISTING}>Commandes</Link>
                </Menu.Item>

                <Menu.Item name="shoppingList" active={match.path === States.SHOPPING_LIST_LISTING}>
                    <Link to={States.SHOPPING_LIST_LISTING}>Listes d'achats</Link>
                </Menu.Item>

                <Menu.Item name="supplierList" active={match.path === States.SUPPLIER_ORDER}>
                    <Link to={States.SUPPLIER_ORDER}>Mercuriales</Link>
                </Menu.Item>

                <Menu.Item name="supplierListPromo" active={match.path === States.SUPPLIER_ORDER_PROMO}>
                    <Link to={States.SUPPLIER_ORDER_PROMO}>Promotions</Link>
                </Menu.Item>
            </Menu>
        );
    }
}

export default withRouter(connect()(PlaceOrderMenu))