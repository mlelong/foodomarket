import React from "react";
import {connect} from "react-redux";
import {Image, Label, List} from "semantic-ui-react";

class ProductLine extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.onToggle = this.onToggle.bind(this);
    }

    onToggle() {
        if (this.props.toggleProduct !== undefined) {
            this.props.toggleProduct(this.props.product);
        }
    }

    render() {
        const {product,addedToList} = this.props;

        return (
            <List.Item className={"shopping-list-product-item"}>
                <Image src={product.productPicture}/>
                <List.Content>
                    <h4>{product.productName}</h4>
                </List.Content>

                <div className={"item-actions"}>
                    {/*<QuantityWidget product={product} pid={product.productId} />*/}

                    {addedToList &&
                    <Label className={"add-label product-added-label"} icon={"checkmark"} data={product} onClick={this.onToggle}/> ||
                    <Label className={"add-label"} icon={"chevron right"} data={product} onClick={this.onToggle}/>
                    }
                </div>
            </List.Item>
        );
    }
}

export default connect(state => (
    {

    }
))(ProductLine)