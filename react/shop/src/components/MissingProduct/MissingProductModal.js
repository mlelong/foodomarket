import React from "react";
import {withRouter} from "react-router-dom";
import PropTypes from "prop-types";
import _ from "lodash";
import {Get, Post} from "shop/utils/fetch";
import {Button, Dropdown, Grid, Icon, Image, Input, Message, Modal, Search, Tab} from "semantic-ui-react";
import {connect} from "react-redux";
import {hideLoadingIndicator, showLoadingIndicator} from "shop/actions";
import {generateUrl} from "shop/router";

class MissingProductModal extends React.Component
{
    static propTypes = {
        onProductCreated: PropTypes.func
    };

    state = {
        activeIndex: 0,
        modalOpen: false,
        isLoading: false,
        value: '',
        results: [],
        product: undefined,
        supplier: undefined,
        productPrice: 0,
        productPriceUnit: undefined
    };

    handleResultSelect = (e, { result }) => {
        this.setState({ value: result.title, product: result, activeIndex: 1 });
    };

    handleOpen = () => {
        this.setState({modalOpen: true});
    };

    handleClose = () => this.setState({
        modalOpen: false,
        activeIndex: 0,
        isLoading: false,
        value: '',
        results: [],
        product: undefined,
        supplier: undefined,
        productPrice: 0,
        productPriceUnit: undefined
    });

    handleSearchChange = (e, { value }) => {
        if (value.length < 3) {
            this.setState({ value });
            return ;
        }

        this.setState({ isLoading: true, value });

        const {isMobileOrTablet} = this.props;

        setTimeout(() => {
            if (this.state.value.length < 3) {
                return ;
            }

            Get(generateUrl(isMobileOrTablet ? 'app_front_getProductsSimple' : 'app_front_getProductsCategory', {terms: this.state.value}))
                .then((data) => {
                    this.setState({
                        isLoading: false,
                        results: data,
                    });
                }).catch((exception) => {
                    console.log(exception);

                    this.setState({
                        isLoading: false,
                        results: [],
                    });
                });
            },
            500);
    };

    setSupplier = (e, {value}) => {
        this.setState({supplier: value, activeIndex: value != 0 ? 2 : 1});
    };

    // setProductPrice = (price) => {
    //     this.setState({productPrice: price});
    // };

    // noinspection JSUnusedLocalSymbols
    setProductPriceUnit = (e, data) => {
        this.setState({productPriceUnit: data.value});
    };

    previousStep = () => {
        const {activeIndex} = this.state;

        if (activeIndex > 0) {
            this.setState({activeIndex: activeIndex - 1});
        }
    };

    addToCatalog = () => {
        this.props.dispatch(showLoadingIndicator());

        const {supplier,product,productPrice,productPriceUnit} = this.state;

        Post(generateUrl('app_front_createNewProduct'), {
            supplierId: supplier,
            name: product.title,
            price: parseFloat(productPrice),
            unit: productPriceUnit,
            productId: product.id
        }).then((data) => {
            this.props.dispatch(hideLoadingIndicator());

            if (data.ok) {
                this.closeAndNotify(data.data);
            } else {
                throw Error(data.message);
            }
        }).catch((exception) => {
            this.props.dispatch(hideLoadingIndicator());
            const message = "Exception caught while creating new catalog reference: " + exception;

            console.log(message);
            alert(message);
        });
    };

    closeAndNotify(product) {
        this.handleClose();

        if (this.props.onProductCreated !== undefined) {
            this.props.onProductCreated(product);
        }
    }

    render() {
        const {isLoading, results, value, activeIndex, product, supplier: supplierId, productPrice, productPriceUnit} = this.state;
        let {trigger, isMobileOrTablet} = this.props;

        if (trigger === undefined) {
            trigger = <Button className={"flex-item-search2"} basic onClick={this.handleOpen}>Produit manquant ?</Button>;
        } else {
            trigger = React.cloneElement(trigger, {onClick: this.handleOpen})
        }

        let supplier = undefined;

        const supplierOptions = this.props.suppliers ? this.props.suppliers.map((option) => {
            if (option.supplierId == 0) {
                return {
                    key: option.supplierId,
                    image: option.supplierPicture,
                    text: '--- Fournisseur ---',
                    value: option.supplierId
                };
            }

            if (option.supplierId == supplierId) {
                supplier = option;
            }

            return {
                key: option.supplierId,
                image: option.supplierPicture,
                text: option.supplierName,
                value: option.supplierId
            }
        }).filter(e => e) : {};

        const ucOptions = [
            {key: 0, text: '---', value: undefined, selected: true, active: true},
            {key: 1, text: '/kg', value: 'KG'},
            {key: 2, text: '/pc', value: 'PC'},
            {key: 3, text: '/L', value: 'L'},
        ];

        const panes = [
            { menuItem: 'Produit', render: () => (
                <Tab.Pane style={{height: '100%'}}>
                    <Message info>
                        Trouver un produit dans notre catalogue
                    </Message>

                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={16} verticalAlign={"middle"}>
                                <Search
                                    category={!isMobileOrTablet}
                                    loading={isLoading}
                                    onResultSelect={this.handleResultSelect}
                                    onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
                                    results={results}
                                    value={value}
                                    placeholder={"Rechercher un produit (3 caractères min.)"}
                                    fluid
                                    input={<Input fluid/>}
                                    noResultsMessage={"Il n'y a pas de produit correspondant à votre recherche."}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Tab.Pane>
            )},
            { menuItem: 'Fournisseur', render: () => (
                <Tab.Pane>
                    <Button onClick={this.previousStep}>Précédent</Button>

                    <Message info>
                        Ajouter ce produit au catalogue du fournisseur de votre choix
                    </Message>

                    <Grid>
                        <Grid.Row>
                            <Grid.Column computer={8} tablet={16} mobile={16} verticalAlign={"middle"}>
                                <span className={"modal-label"}>Produit</span>
                                <Image src={product ? product.image : ''} style={{display: "inline-block"}}/>
                                {product.title}
                            </Grid.Column>
                            <Grid.Column computer={8} tablet={16} mobile={16} verticalAlign={"middle"}>
                                <Dropdown placeholder={"Fournisseur du produit"} fluid search selection options={supplierOptions} onChange={this.setSupplier}/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Tab.Pane>
            )},
            { menuItem: 'Prix / Options', render: () => (
                <Tab.Pane>
                    <Button onClick={this.previousStep}>Précédent</Button>

                    <Message info>
                        Paramétrer le prix et les spécificités du produit
                    </Message>

                    <Grid>
                        <Grid.Row>
                            <Grid.Column computer={8} tablet={16} mobile={16} verticalAlign={"middle"}>
                                <span className={"modal-label"}>Produit</span>
                                <Image src={product ? product.image : ''} style={{display: "inline-block"}}/>
                                {product.title}
                            </Grid.Column>
                            <Grid.Column computer={8} tablet={16} mobile={16} verticalAlign={"middle"}>
                                <span className={"modal-label"}>Fournisseur</span>
                                {supplier ? supplier.supplierName : ''}
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            {/*<Grid.Column computer={8} tablet={10} mobile={9}>*/}
                                {/*<span className={"modal-label"}>Prix</span>*/}
                                {/*<FrontInput placeholder={"Prix ou 0 si inconnu"} icon={"euro"}*/}
                                            {/*onChange={this.setProductPrice} fluid/>*/}
                            {/*</Grid.Column>*/}
                            <Grid.Column computer={8} tablet={6} only={"tablet computer"}>
                                <span className={"modal-label"}>Unité de commande</span>
                                <Dropdown placeholder={"Unité de commande"} fluid selection
                                          options={ucOptions}
                                          onChange={this.setProductPriceUnit}
                                />
                            </Grid.Column>
                            <Grid.Column mobile={16} only={"mobile"} verticalAlign={"bottom"}>
                                <Dropdown placeholder={"Unité de commande"} fluid selection
                                          options={ucOptions}
                                          onChange={this.setProductPriceUnit}
                                />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row only={"tablet computer"}>
                            <Grid.Column style={{height: '100%'}} verticalAlign={"bottom"}>
                                {product && supplier && productPrice !== undefined && productPriceUnit &&
                                <Message success>
                                    {/*Vous allez ajouter le produit {product.title} - {productPrice}€/{productPriceUnit}<br/>*/}
                                    Vous allez ajouter le produit {product.title}, unité de commande {productPriceUnit}<br/>
                                    au catalogue du fournisseur {supplier.supplierName}. Souhaitez vous continuer ?<br/>
                                    <br/>
                                    <i>Notez que votre fournisseur pourrait ne pas disposer de ce produit.</i>
                                    <br/><br/>
                                    <div style={{textAlign: 'center'}}>
                                        <Button primary className={"blue"} onClick={this.addToCatalog}>Ajouter au catalogue</Button>
                                        <Button negative onClick={this.handleClose}>Annuler et quitter</Button>
                                    </div>
                                </Message>
                                }
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row only={"mobile"}>
                            <Grid.Column style={{height: '100%'}} verticalAlign={"bottom"}>
                                {product && supplier && productPrice !== undefined && productPriceUnit &&
                                <Message success>
                                    {/*Vous allez ajouter le produit {product.title} - {productPrice}€/{productPriceUnit}<br/>*/}
                                    Vous allez ajouter le produit {product.title}, unité de commande {productPriceUnit}<br/>
                                    au catalogue du fournisseur {supplier.supplierName}. Souhaitez vous continuer ?<br/>
                                    <br/>
                                    <i>Notez que votre fournisseur pourrait ne pas disposer de ce produit.</i>
                                    <br/><br/>
                                    <div style={{textAlign: 'center'}}>
                                        <Button primary className={"blue"} onClick={this.addToCatalog} fluid style={{marginBottom: '8px'}}>Ajouter au catalogue</Button>
                                        <Button negative onClick={this.handleClose} fluid>Annuler et quitter</Button>
                                    </div>
                                </Message>
                                }
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Tab.Pane>
            )},
        ];

        return (
            <Modal className={"missing-product-modal"}
                   open={this.state.modalOpen}
                   onClose={this.handleClose}
                   trigger={trigger}
                   closeOnDocumentClick={false}
                   closeOnDimmerClick={true}
                   size={isMobileOrTablet ? 'fullscreen' : 'large'}
                   closeIcon={<Icon name={"close"} />}
            >
                <Modal.Header>
                    Produit Manquant ?
                </Modal.Header>
                <Modal.Content scrolling>
                    {isMobileOrTablet &&
                    <Tab
                        id={"missing-product-tab"}
                        menu={{
                            fluid: true,
                            vertical: false,
                            tabular: true,
                            attached: 'top'
                        }}
                        panes={panes}
                        activeIndex={activeIndex}
                    /> ||
                    <Tab
                        id={"missing-product-tab"}
                        menu={{
                            fluid: true,
                            vertical: true,
                            tabular: true
                        }}
                        panes={panes}
                        activeIndex={activeIndex}
                    />
                    }
                </Modal.Content>
            </Modal>
        );
    }
}

export default withRouter(connect(state => (
    {
        suppliers: state.frontState.suppliers,
        isMobileOrTablet: state.frontState.isMobileOrTablet
    }
))(MissingProductModal));