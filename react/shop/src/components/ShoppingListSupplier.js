import React from "react";
import {connect} from "react-redux";
import {Icon, Image, List} from "semantic-ui-react";

class ShoppingListSupplier extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        const {supplierId} = this.props.data;
        this.props.onClick(supplierId);
    }

    render() {
        const props = this.props.data;
        let className = "shopping-list-supplier-item";

        if (this.props.activeItem === props.supplierId) {
            className += " active";
        }

        if (props.supplierId != 0 && !props.order_enabled) {
            className += " order-disabled";
        }

        return (
            <List.Item className={className} onClick={this.onClick}>
                <Image src={props.supplierPicture}/>
                <List.Content>
                    <h4>{props.supplierName}</h4>
                    {props.supplierProducts} produits
                </List.Content>
                <Icon name={"angle right"} />
            </List.Item>
        );
    }
}

export default connect(state => (
    {
    }
))(ShoppingListSupplier)