import React from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {Button, Form, Icon, Message} from "semantic-ui-react";
import {cloneDeep} from "lodash";
import {Post} from "shop/utils/fetch";
import {generateUrl} from "shop/router";

class SimpleTelephoneForm extends React.Component {
    state = {
        submitting: false,
        submitted: false,
        error: '',
        telephone: {value: '', error: false, required: true},
    };

    onChange = (e, data) => {
        const {name} = data;
        const state = cloneDeep(this.state);

        state[name].value = data.value;
        state[name].error = !!(state[name].required && state[name].value == '');

        this.setState(state);
    };

    static getCookieValue(name) {
        const b = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)');

        return b ? b.pop() : '';
    }

    submit = () => {
        const newState = cloneDeep(this.state);
        const fields = ['telephone'];
        let hasError = false;

        for (let field of fields) {
            newState[field].error = !!(newState[field].required && newState[field].value.length === 0);

            hasError = !hasError ? newState[field].error : hasError;
        }

        if (hasError) {
            this.setState(newState);
            return ;
        }

        this.setState({submitting: true});

        const {
            telephone
        } = this.state;

        const email = SimpleTelephoneForm.getCookieValue('_email');

        Post(generateUrl('app_front_update_prospect_telephone'), {
            email: email,
            telephone: telephone.value
        }).then((data) => {
            if (data.ok) {
                this.setState({error: '', submitting: false, submitted: true});
            } else {
                this.setState({error: data.message, submitting: false});
            }
        }).catch((exception) => {
            this.setState({error: exception, submitting: false});
        });
    };

    render() {
        const {error, submitting, submitted, telephone} = this.state;

        return (
            <Message>
                <Message.Content>
                    <Message.Header style={{textAlign: 'center', paddingBottom: '1em'}}>
                        Je souhaiterais avoir des informations, rappelez-moi
                    </Message.Header>
                    {!submitted &&
                    <Form>
                        {error.length > 0 &&
                        <Message negative>
                            <Message.Header>Une erreur est survenue</Message.Header>
                            <p>{error}</p>
                        </Message>
                        }

                        <Form.Group widths='equal'>
                            <Form.Input
                                fluid
                                label="Téléphone"
                                placeholder='Votre numéro de téléphone'
                                type='text'
                                name='telephone'
                                value={telephone.value}
                                error={telephone.error}
                                onChange={this.onChange}
                            />
                        </Form.Group>

                        <Button id="submit-callback-button" loading={submitting} className={"register-next blue"} primary
                                onClick={this.submit}>Ok</Button>
                    </Form>
                    ||
                    <Message icon success>
                        <Icon name={"smile"}/>
                        <Message.Content>
                            <Message.Header>Votre demande a bien été prise en compte</Message.Header>
                            <p>Merci pour votre retour, nous reviendrons vers vous dans les plus brefs délais.</p>
                        </Message.Content>
                    </Message>
                    }
                </Message.Content>
            </Message>
        );
    }
}

export default withRouter(connect()(SimpleTelephoneForm));