import React, {Component} from "react";
import PropTypes from "prop-types";
import {Grid, Loader, Card} from "semantic-ui-react";
import {cloneDeep} from "lodash/lang";
// import ShoppingListSupplierProduct from "./ShoppingListSupplierProduct";
// import FrontInput from "./FrontInput";
import {Get} from "shop/utils/fetch";
// import MissingProductModal2 from "./MissingProduct/MissingProductModal";
import {connect} from "react-redux";
import {generateUrl} from "shop/router";
import {getSupplierItem} from "shop/utils/shoppingItems";
import ShoppingListSupplierProductVirtualized from "shop/components/ShoppingListSupplierProductVirtualized";
import {AutoSizer} from "react-virtualized";
import {List} from "react-virtualized";
import {CellMeasurer, CellMeasurerCache} from 'react-virtualized';
import FrontInputSearch from "shop/components/FrontInputSearch";
import WindowScroller from "react-virtualized/dist/commonjs/WindowScroller";


class ProductList extends Component {
    constructor(props) {
        super(props);
        this.cache = new CellMeasurerCache({
            fixedWidth: true,
            // defaultHeight: 100,
            // minHeight: 93,
            // minWidth: 75,
            // keyMapper: () => 1
        })
    }

    isLoading = false;
    loadBatch = [];
    state = {
        supplierId: this.props.supplierId,
        products: [],
        totalProducts: undefined,
        terms: '',
    };

    static propTypes = {
        supplierId: PropTypes.number,
        onProductAdded: PropTypes.func,
        onProductRemoved: PropTypes.func,
        onQuantityUpdated: PropTypes.func,
        addedProducts: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
        promoOnly: PropTypes.bool
    };

    static defaultProps = {
        supplierId: 0,
        promoOnly: false
    };

    addProduct = (product) => {
        if (this.props.onProductAdded !== undefined) {
            this.props.onProductAdded(product);
        }
    };

    removeProduct = (product) => {
        if (this.props.onProductRemoved !== undefined) {
            this.props.onProductRemoved(product);
        }
    };

    updateQuantity = (product, quantity, quantityUnit) => {
        if (this.props.onQuantityUpdated !== undefined) {
            this.props.onQuantityUpdated(product, quantity, quantityUnit);
        }
    };

    onFilter = (terms) => {
        this.setState({terms: terms, products: [], totalProducts: undefined});
    };

    onLoadMore = (page) => {
        if (!this.isLoading) {
            this.isLoading = true;

            const {supplierId, promoOnly} = this.props;
            const {products, terms} = this.state;

            Get(generateUrl('app_front_getSupplierProducts', {
                id: supplierId,
                terms: terms,
                page: page,
                promoOnly: promoOnly,
                limit: 50000 // Un peu de la triche pour l'instant
            }))
                .then(data => {
                    this.setState({totalProducts: data.total, products: [...products, ...data.items]});
                    this.isLoading = false;

                    let nextPage;

                    if ((nextPage = this.loadBatch.shift()) !== undefined) {
                        this.onLoadMore(nextPage);
                    }
                }).catch(exception => {
                console.log("Exception caught while fetching supplier product list", exception);
                this.isLoading = false;
            });
        } else {
            this.loadBatch.push(page);
        }
    };

    onMissingProductClicked = (e) => {
        e.preventDefault();
    };

    static getDerivedStateFromProps(props, state) {
        if (props.supplierId != state.supplierId) {
            return {
                supplierId: props.supplierId,
                products: [],
                totalProducts: undefined,
                terms: state.terms,
            }
        }

        return null;
    }

    componentDidMount() {
        this.isLoading = false;
        this.loadBatch = [];

        this.onLoadMore(0);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.supplierId != this.state.supplierId || prevState.terms != this.state.terms) {
            this.isLoading = false;
            this.loadBatch = [];

            this.onLoadMore(0);
        }
    }

    onProductCreated = (product) => {
        this.setState({
            terms: product.productName,
            supplierId: product.supplierId,
            products: [],
            totalProducts: undefined,
        });
    };

    render() {
        const {addedProducts, suppliers, isMobile} = this.props;
        const {totalProducts} = this.state;
        const products = cloneDeep(this.state.products);
        const hasMore = totalProducts === undefined || products.length < totalProducts;

        for (let product of products) {
            if (addedProducts instanceof Array) {
                let found = false;

                for (let addedProduct of addedProducts) {
                    if (product.productId == addedProduct.productId && product.supplierId == addedProduct.supplierId) {
                        product.quantity = addedProduct.quantity;
                        product.quantityUnit = addedProduct.quantityUnit;
                        product.quantityUnitSelected = addedProduct.quantityUnitSelected;
                        product.cartId = addedProduct.cartId;
                        product.cartItemId = addedProduct.cartItemId;
                        product.sid = addedProduct.sid;

                        product.added = true;

                        found = true;
                        break;
                    }
                }

                if (!found) {
                    delete product.cartId;
                    delete product.cartItemId;
                    delete product.sid;
                }
            } else {
                const addedProduct = getSupplierItem(product.supplierId, product.productId, addedProducts);

                if (addedProduct) {
                    product.quantity = addedProduct.quantity;
                    product.quantityUnit = addedProduct.quantityUnit;
                    product.quantityUnitSelected = addedProduct.quantityUnitSelected;
                    product.cartId = addedProduct.cartId;
                    product.cartItemId = addedProduct.cartItemId;
                    product.sid = addedProduct.sid;

                    product.added = true;
                } else {
                    delete product.cartId;
                    delete product.cartItemId;
                    delete product.sid;
                }
            }
        }

        const disabledSuppliers = [];

        for (let supplier of suppliers) {
            if (supplier['order_enabled'] === false) {
                disabledSuppliers.push(supplier.supplierId);
            }
        }

        // Nouvelle méthode pour la lib React virtualized
        const rowRenderer = ({index, style, key, parent, rowIndex}) => {
            const product = products[index];
            const addedToList = product.added === true;
            return (
                <CellMeasurer
                    key={key}
                    cache={this.cache}
                    parent={parent}
                    columnIndex={0}
                    rowIndex={index}
                >
                    {({ measure, registerChild }) => (
                        <div ref={registerChild} style={style} onLoad={measure} className="mercurialProductRow">
                            <ShoppingListSupplierProductVirtualized
                                data={product}
                                onAddToList={this.addProduct}
                                onRemoveFromList={this.removeProduct}
                                onQuantityChanged={this.updateQuantity}
                                addedToList={addedToList}
                                orderEnabled={!disabledSuppliers.includes(product.supplierId)}
                            />
                        </div>
                    )}
                </CellMeasurer>
            );
        };

        return (
            <Grid>
                <Grid.Row className={"sticky-card-search"}>
                    <Grid.Column width={16}>
                        <Card className={"card_search_product"} fluid>
                            <Card.Content className={"container-card-search"}>
                                <FrontInputSearch placeholder={"Chercher un produit"} fluid={false} onOk={this.onFilter}
                                            inputValue={this.state.terms}/>
                               {/* <MissingProductModal2 onProductCreated={this.onProductCreated}/>*/}
                            </Card.Content>
                        </Card>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16} className={"column_virtualized"}>
                        {totalProducts === 0 &&
                        <div className="product-list">
                            <p className="no-products-found">Aucun produit trouvé</p>
                        </div>
                        || totalProducts === undefined &&
                        <div className="ui list product-list">
                            <div className="item">
                                <Loader key={"loader"} active inline='centered'/>
                            </div>
                        </div>
                        ||
                            <WindowScroller>
                                {({ height, isScrolling, scrollTop, onChildScroll}) => (
                                        <AutoSizer disabledHeight>
                                            {({ width }) => (
                                                <List
                                                    autoHeight
                                                    isScrolling={isScrolling}
                                                    rowCount={this.state.products.length}
                                                    height={height}
                                                    width={width}
                                                    deferredMeasurementCache={this.cache}
                                                    rowHeight={this.cache.rowHeight}
                                                    rowRenderer={rowRenderer}
                                                    overscanRowCount={3}
                                                    scrollTop={scrollTop}
                                                    onScroll={onChildScroll}
                                                />
                                            )}
                                        </AutoSizer>
                                    )}
                            </WindowScroller>
                        }
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default connect(state => ({
    suppliers: state.frontState.suppliers,
    isMobile: state.frontState.isMobile
}))(ProductList);