import React, {Component} from "react";
import {connect} from "react-redux";
import {List} from "semantic-ui-react";
import ShoppingListSupplier from "./ShoppingListSupplier";

class SupplierList extends Component
{
    state = {
        activeSupplier: 0
    };

    supplierClick = (supplierId) => {
        this.setState({activeSupplier: supplierId});

        if (this.props.onSupplierSelected !== undefined) {
            this.props.onSupplierSelected(supplierId);
        }
    };

    render() {
        const {activeSupplier} = this.state;
        const {suppliers} = this.props;

        return (
            <List>
                {
                    suppliers && suppliers.map((supplier, index) =>
                        <ShoppingListSupplier
                            key={index}
                            data={supplier}
                            activeItem={activeSupplier}
                            onClick={this.supplierClick}
                        />
                    )
                }
            </List>
        );
    }
}

export default connect(state => ({ suppliers: state.frontState.suppliers }))(SupplierList);
