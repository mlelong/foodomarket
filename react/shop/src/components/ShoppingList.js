import React from "react";
import {Grid, Icon, List} from "semantic-ui-react";
import {connect} from "react-redux";
import {removeShoppingList} from "shop/actions/index";
import {States} from "shop/constants/frontConstants";
import {withRouter} from "react-router-dom";

class ShoppingList extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.onRemove = this.onRemove.bind(this);
        this.onOrder = this.onOrder.bind(this);
    }

    onRemove(e) {
        e.preventDefault();

        this.props.dispatch(removeShoppingList(this.props.data.id, function(response) {
            if (response.ok) {
                history.push(States.SHOPPING_LIST_LISTING);
            } else {
                const message = "Could not remove shopping list: " + data.message;

                console.log(message);
                alert(message);
            }
        }));
    }

    onOrder(e) {
        e.preventDefault();

        const {match, history, shoppingListId} = this.props;

        if (match.path === States.PREVIOUS_ORDERS_LISTING) {
            history.push(States.ORDER_PREVIOUS_ORDER.replace(':date', this.props.data.date.split('/').reverse().join('-')));
        } else {
            history.push(States.SHOPPING_LIST_ORDER.replace(':id', shoppingListId));
        }
    }

    render() {
        const props = this.props.data;
        const supplierNames = props.supplierNames.join(', ');

        return (
            <List.Item>
                <Grid>
                    <Grid.Row className={"shopping-list-row"}>
                        <Grid.Column computer={7} tablet={6} as={"a"} only={"computer tablet"} onClick={this.onOrder}>
                            <div>
                                <p className={"shopping-list-name"}>{props.listName}</p>
                            </div>
                        </Grid.Column>

                        <Grid.Column mobile={11} as={"a"} only={"mobile"} onClick={this.onOrder}>
                            <div>
                                <p className={"shopping-list-name"}>{props.listName}</p>
                                <p className={"additional-infos"}>{props.nbItems} produit{props.nbItems > 1 && 's'}, {props.nbSuppliers} fournisseur{props.nbSuppliers > 1 && 's'}<br/>({supplierNames})</p>
                            </div>
                        </Grid.Column>

                        <Grid.Column tablet={5} only={"tablet"} as={"a"} onClick={this.onOrder} className={"additional-infos"}>
                            <div>
                                <p>{props.nbItems} produit{props.nbItems > 1 && 's'}, {props.nbSuppliers} fournisseur{props.nbSuppliers > 1 && 's'}<br/>({supplierNames})</p>
                            </div>
                        </Grid.Column>
                        <Grid.Column computer={5} only={"computer"} as={"a"} onClick={this.onOrder} className={"additional-infos"}>
                            <div>
                                <p>{props.nbItems} produit{props.nbItems > 1 && 's'}, {props.nbSuppliers} fournisseur{props.nbSuppliers > 1 && 's'}<br/>({supplierNames})</p>
                            </div>
                        </Grid.Column>
                        <Grid.Column only={"computer"} className="shopping-list-col-order" computer={4} as={"a"} onClick={this.onOrder}>
                            <div>
                                <Icon name="plus cart"/><br/>
                                Commander
                            </div>
                        </Grid.Column>
                        <Grid.Column only={"mobile"} className="shopping-list-col-order" tablet={4} mobile={5} as={"a"} onClick={this.onOrder}>
                            <div>
                                <Icon name="plus cart"/>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </List.Item>
        );
    }
}

export default withRouter(connect()(ShoppingList))