import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Segment} from "semantic-ui-react";
import {
    addToShoppingList,
    removeFromShoppingList,
    updateShoppingListProductQuantity
} from "shop/actions/index";
import {States} from "shop/constants/frontConstants";
import {withRouter} from "react-router-dom";
import ProductList from "./ProductList";

class ShoppingListSupplierProducts extends React.Component
{
    state = {
        supplierId: this.props.supplierId
    };

    static propTypes = {
        supplierId: PropTypes.number.isRequired,
        shoppingListId: PropTypes.number
    };

    static defaultProps = {
        supplierId: 0
    };

    addToList = (product) => {
        const {shoppingListId, match} = this.props;
        const updateServerside = match.path === States.SHOPPING_LIST + States.SHOPPING_LIST_EDIT;

        this.props.dispatch(addToShoppingList(product, shoppingListId, updateServerside));
    };

    removeFromList = (product) => {
        const {shoppingListId, match} = this.props;
        const updateServerside = match.path === States.SHOPPING_LIST + States.SHOPPING_LIST_EDIT;

        this.props.dispatch(removeFromShoppingList(product, shoppingListId, updateServerside));
    };

    updateQuantity = (product, quantity, quantityUnit) => {
        const {shoppingListId, match} = this.props;
        const updateServerside = match.path === States.SHOPPING_LIST + States.SHOPPING_LIST_EDIT && product.sid !== undefined;

        this.props.dispatch(updateShoppingListProductQuantity(product, quantity, quantityUnit, updateServerside, shoppingListId));
    };

    render() {
        const {shoppingListProducts, supplierId} = this.props;

        return (
            <Segment className={"shopping-list-supplier-products-segment"}>
                <h3 className={"shopping-list-creation-step"}>
                    <span className={"shopping-list-creation-step-nb"}>2. </span>
                    Sélectionnez les produits à ajouter à votre liste
                </h3>

                <ProductList
                    addedProducts={shoppingListProducts}
                    onProductAdded={this.addToList}
                    onProductRemoved={this.removeFromList}
                    onQuantityUpdated={this.updateQuantity}
                    supplierId={supplierId}
                />
            </Segment>
        );
    }
}

export default withRouter(connect(state => (
    {
        shoppingListProducts: state.frontState.shoppingListProducts,
    }
))(ShoppingListSupplierProducts))