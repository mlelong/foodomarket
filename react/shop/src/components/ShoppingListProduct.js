import React from "react";
import {connect} from "react-redux";
import {Icon, Image, List} from "semantic-ui-react";
import {removeFromShoppingList} from "shop/actions/index";
import {PriceWidget} from "./PriceWidget";

class ShoppingListProduct extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.onRemove = this.onRemove.bind(this);
    }

    onRemove(e) {
        e.preventDefault();

        const {shoppingListId} = this.props;

        let updateServerside = false;

        if (shoppingListId !== undefined && shoppingListId !== null && shoppingListId !== 0 && shoppingListId !== '') {
            updateServerside = true;
        }

        this.props.dispatch(removeFromShoppingList(this.props.data, shoppingListId, updateServerside));
    }

    render() {
        const props = this.props.data;

        return (
            <List.Item className={"shopping-list-product-item"}>
                <Image src={props.productPicture} onClick={this.onRemove}>
                    <img src={props.productPicture} width={48} height={48}/>
                    <Icon name={"trash"}/>
                </Image>
                <List.Content>
                    <h4>{props.productName}</h4>
                    {props.productSupplier} - <PriceWidget product={props} />&nbsp;<span className={"product-quantity"}>Qté: {props.quantity}<span className={"product-quantity-unit"}>{props.quantityUnitSelected}</span></span>
                </List.Content>
            </List.Item>
        );
    }
}

export default connect(state => (
    {
        shoppingListId: state.frontState.shoppingListId
    }
))(ShoppingListProduct)