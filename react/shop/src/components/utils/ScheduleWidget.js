import React from "react";
import {DateInput} from "semantic-ui-calendar-react";
import moment from "moment";
import PropTypes from "prop-types";

export default class ScheduleWidget extends React.Component {
    static propTypes = {
        onDateScheduled: PropTypes.func.isRequired,
        date: PropTypes.string,
        disabledDays: PropTypes.arrayOf(PropTypes.number),
        inline: PropTypes.bool,
        chronofreshDelivery: PropTypes.bool,
    };

    static defaultProps = {
        inline: false
    };

    state = {
        date: ScheduleWidget.getFirstSelectableDate(this.props.disabledDays, this.props.date),
        disabledDates: []
    };

    static getDerivedStateFromProps(props, state) {
        if (props.date !== state.date) {
            const dates = ScheduleWidget.getFirstSelectableDate(props.disabledDays, props.date, props.chronofreshDelivery);

            return {
                date: dates.firstSelectableDate,
                disabledDates: dates.disabledDates
            };
        }

        return null;
    }

    static getFirstSelectableDate(disabledDays, propsDate, chronofreshDelivery) {

        if (!disabledDays) {
            disabledDays = [];
        }

        const disabledDates = [];
        const todayDate = moment().hour(0).minute(0).second(0).millisecond(0);
        const tomorow = moment().add(1, 'days').hour(0).minute(0).second(0).millisecond(0);

        //Orders delivered by chronofresh are always in A+C
        if(chronofreshDelivery) {
            disabledDates.push(todayDate);
            disabledDates.push(tomorow);

            //We are saturday and supplier is closed monday, disabling delivery on tuesday
            if(moment().day() === 6 && disabledDays.includes(1)) {
                disabledDates.push(moment().add(3, 'days').hour(0).minute(0).second(0).millisecond(0))
            }

            //We are sunday and supplier is closed monday, disabling delivery on tuesday
            if(moment().day() === 0 && disabledDays.includes(1)) {
                disabledDates.push(moment().add(2, 'days').hour(0).minute(0).second(0).millisecond(0))
            }

            //Chronofresh do not deliver on sunday
            if(!disabledDays.includes(0)){
                disabledDays.push(0);
            }

            //Chronofresh do not deliver on monday
            if(!disabledDays.includes(1)) {
                disabledDays.push(1);
            }
        }

        // On bloque pour le mois courant les dates correspondant aux jours désactivés
        for (let dayNum of disabledDays) {
            for (let i = 0; i < 5; ++i) {
                const disabedDate = moment().day(dayNum + (i * 7)).hour(0).minute(0).second(0).millisecond(0);

                // On bloque la date seulement si elle est supérieure à aujourd'hui
                if (disabedDate.isBefore(todayDate)) {
                    continue ;
                }

                // Ajout des jours non ouvrables (récupérés dans la boucle for) dans la liste des jours non ouvrables
                disabledDates.push(disabedDate);
            }
        }

        // Si il est plus de 2h du matin, alors on bloque la date d'aujourd'hui
        // TODO: on pourrait ici utiliser le orderLimitTime pour être complètement d'équerre
        if (moment().hour() > 2) {
            disabledDates.push(todayDate);
        }


        // On trie les dates par ordre croissant
        disabledDates.sort(function(a, b) {
            return a > b ? 1 : (a < b ? -1 : 0);
        });

        // On initialise la première date sélectionnable à aujourd'hui
        let firstSelectableDate = moment().hour(0).minute(0).second(0).millisecond(0);

        for (let disabledDate of disabledDates) {
            // Si cette date est avant la première date désactivée, alors c'est good !
            if (firstSelectableDate.isBefore(disabledDate)) {
                break ;
            }
            // Si c'est le même jour alors on incrémente d'un jour
            else if (firstSelectableDate.isSame(disabledDate)) {
                firstSelectableDate.add(1, 'day');
            }
            // Si c'est après alors on vérifie avec la prochaine date désactivée...
        }

        // Si il y avait une date dans les props (remontée depuis le serveur donc)
        if (propsDate) {
            const _propsDate = moment(propsDate, 'DD/MM/YYYY');

            // Alors seulement si cette date est après le premier jour sélectionnable on la prend à la place
            if (_propsDate.isAfter(firstSelectableDate)) {
                firstSelectableDate = _propsDate;
            }
        }

        // Le composant semantic calendar gueule si on lui donne pas un string
        // et de toute façon dans les props c'est une string aussi...
        firstSelectableDate = firstSelectableDate.format('DD/MM/YYYY');

        return {firstSelectableDate, disabledDates};
    }

    componentDidMount() {
        moment.locale('fr');
    }

    handleChange = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            if (this.state[name] !== value) {
                this.setState({[name]: value});
                this.props.onDateScheduled(value);
            }
        }
    };

    render() {
        const todayDate = moment().hour(0).minute(0).second(0).millisecond(0);

        return (
            <DateInput
                fluid
                name="date"
                dateFormat="DD/MM/YYYY"
                placeholder="Date de livraison souhaitée (défaut J+1)"
                value={this.state.date}
                iconPosition="left"
                icon={"calendar"}
                onChange={this.handleChange}
                className={"schedule-widget"}
                closable={true}
                closeOnMouseLeave={false}
                minDate={todayDate}
                popupPosition={"bottom center"}
                disable={this.state.disabledDates}
                inline={this.props.inline}
                localization={'fr'}
                readOnly={true}
            />
        );
    }
}