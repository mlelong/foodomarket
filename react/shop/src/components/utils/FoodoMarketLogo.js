import React from "react";

export const FoodoMarketLogo = (props) => {
    return (
        <span style={{color:props.color,fontSize:props.size + '!important'}} className="FoodoMarketLogo"><span className="text-style-1">Foodo</span>Market</span>
    );
};