import React from "react";
import {connect} from "react-redux";
import {Grid, Icon, Modal} from "semantic-ui-react";
import SuppliersDropdown from "./SuppliersDropdown";
import {
    addToCart,
    addToShoppingList,
    changeQuantity,
    removeFromCart,
    removeFromShoppingList,
    updateShoppingListProductQuantity
} from "shop/actions";
import ProductList from "./ProductList";
import {withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";

class AddProductModal extends React.Component {
    state = {
        supplierId: 0
    };

    selectSupplier = (supplier) => {
        this.setState({supplierId: supplier.supplierId})
    };

    handleClose = () => {
        this.props.onModalClosed();
    };

    addProduct = (product) => {
        const {match} = this.props;

        if (match.path === States.SHOPPING_LIST_ORDER || match.path === States.ORDER_PREVIOUS_ORDER) {
            this.props.dispatch(addToCart(product));
        } else if (match.path === States.SHOPPING_LIST_EDIT) {
            const shoppingListId = match.params.id;

            this.props.dispatch(addToShoppingList(product, shoppingListId, true));
        }
    };

    removeProduct = (product) => {
        const {match} = this.props;

        if (match.path === States.SHOPPING_LIST_ORDER || match.path === States.ORDER_PREVIOUS_ORDER) {
            this.props.dispatch(removeFromCart(product));
        } else if (match.path === States.SHOPPING_LIST_EDIT) {
            const shoppingListId = match.params.id;

            this.props.dispatch(removeFromShoppingList(product, shoppingListId, shoppingListId !== undefined));
        }
    };

    updateQuantity = (product, quantity, unit) => {
        const {match} = this.props;

        if (match.path === States.SHOPPING_LIST_ORDER || match.path === States.ORDER_PREVIOUS_ORDER) {
            this.props.dispatch(changeQuantity(product, quantity, unit));
        } else if (match.path === States.SHOPPING_LIST_EDIT) {
            const shoppingListId = match.params.id;

            this.props.dispatch(updateShoppingListProductQuantity(product, quantity, unit, true, shoppingListId));
        }
    };

    render() {
        const {match, suppliers, shoppingListProducts, shoppingCarts} = this.props;
        const {supplierId} = this.state;

        return (
            <Modal className={"add-product-modal"} open={this.props.modalOpen} onClose={this.handleClose} closeOnDocumentClick={false} closeOnDimmerClick={true} closeIcon={<Icon name={"close"}/>}>
                <Modal.Header>
                    <Grid>
                        <Grid.Row columns={1}>
                            <Grid.Column textAlign={"center"}>
                                Ajouter un produit au panier
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Modal.Header>

                <Modal.Content scrolling>
                    <SuppliersDropdown suppliers={suppliers} onSupplierSelected={this.selectSupplier} />

                    <ProductList
                        addedProducts={match.path === States.SHOPPING_LIST_ORDER || match.path === States.ORDER_PREVIOUS_ORDER ? shoppingCarts : shoppingListProducts}
                        onProductAdded={this.addProduct}
                        onProductRemoved={this.removeProduct}
                        onQuantityUpdated={this.updateQuantity}
                        supplierId={supplierId}
                    />
                </Modal.Content>
            </Modal>
        );
    }
}

export default withRouter(connect(state => (
    {
        suppliers: state.frontState.suppliers,
        shoppingListId: state.frontState.shoppingListId,
        shoppingListProducts: state.frontState.shoppingListProducts,
        shoppingCarts: state.shoppingCarts
    }
))(AddProductModal))