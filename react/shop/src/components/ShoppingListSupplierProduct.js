import React from "react";
import PropTypes from "prop-types";
import {Icon, Image, Label, List} from "semantic-ui-react";
import {PriceWidget} from "./PriceWidget";
import QuantityWidget from "./QuantityWidget";
import {withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {connect} from "react-redux";

class ShoppingListSupplierProduct extends React.PureComponent {
    state = {
        loading: false,
        quantity: this.props.data.quantity,
        quantityUnit: this.props.data.quantityUnitSelected
    };

    static propTypes = {
        data: PropTypes.object.isRequired,
        addedToList: PropTypes.bool,
        onQuantityChanged: PropTypes.func,
        onAddToList: PropTypes.func,
        onRemoveFromList: PropTypes.func,
        orderEnabled: PropTypes.bool
    };

    static defaultProps = {
        orderEnabled: true
    };

    onToggle = (e, {data}) => {
        e.preventDefault();

        const {authToken, addedToList, onRemoveFromList, onAddToList, orderEnabled, match} = this.props;
        const shoppingMode = match.path === States.SUPPLIER_ORDER || match.path === States.SUPPLIER_ORDER_PROMO || match.path === States.PRODUCT || !authToken;

        if (shoppingMode && !orderEnabled) {
            return ;
        }

        if (addedToList) {
            if (onRemoveFromList !== undefined) {
                this.setState({loading: true});
                onRemoveFromList(data);
            }
        } else {
            const {quantity, quantityUnit} = this.state;

            if (onAddToList !== undefined && quantity > 0) {
                this.setState({loading: !!authToken});
                onAddToList(Object.assign({}, data, {
                    quantity: quantity,
                    quantityUnitSelected: quantityUnit
                }));
            }
        }
    };

    onQuantityChanged = (quantity, quantityUnit) => {
        const product = this.props.data;

        this.setState({quantity: quantity, quantityUnit: quantityUnit});

        if (this.props.onQuantityChanged !== undefined) {
            this.props.onQuantityChanged(product, quantity, quantityUnit);
        }
    };

    componentDidUpdate(prevProps) {
        const {addedToList} = this.props;

        if (prevProps.addedToList !== addedToList) {
            this.setState({loading: false});
        }
    }

    getSupplierDisplay = (product) => {
        const {authToken, isAppDev} = this.props;
        let supplierName = product.productSupplier;

        if (authToken || isAppDev) {
            return supplierName;
        }

        let supplierNameLength = supplierName.length-2;
        supplierName = supplierName[2].toUpperCase() + '***************************************';
        supplierName =  supplierName.substr(0, supplierNameLength);

        return <a onClick={this.onSupplierNameClicked}>Chez {supplierName}</a>;
    };

    onSupplierNameClicked = (e) => {
        e.preventDefault();

        this.props.onAddToList({});
    };

    render() {
        const product = this.props.data;
        const {addedToList, match, authToken, isAppDev, orderEnabled} = this.props;
        const {loading, quantity} = this.state;

        const shoppingMode = match.path === States.SUPPLIER_ORDER || match.path === States.SUPPLIER_ORDER_PROMO || match.path === States.PRODUCT || !authToken;

        let label = '';

        if (addedToList) {
            label = shoppingMode
                ? <Label className={"add-label product-added-label"} icon={<Icon name={"checkmark"} loading={loading}/>} data={product} onClick={this.onToggle} content={"Ajouté"}/>
                : <Label className={"add-label product-added-label"} icon={<Icon name={"checkmark"} loading={loading}/>} data={product} onClick={this.onToggle} content={"Ajouté"}/>
            ;
        } else {
            if (quantity <= 0) {
                label = shoppingMode
                    ? <Label className={"add-label disabled"} icon={<Icon name={orderEnabled ? "plus cart" : "lock"} loading={loading}/>} data={product} onClick={this.onToggle} content={"Commander"}/>
                    : <Label className={"add-label disabled"} icon={<Icon name={"chevron right"} loading={loading}/>} data={product} onClick={this.onToggle} content={"Ajouter"}/>;
            } else {
                const orderButtonClassName = orderEnabled ? "add-label enabled" : "add-label disabled";

                label = shoppingMode
                    ? <Label className={orderButtonClassName} icon={<Icon name={orderEnabled ? "plus cart" : "lock"} loading={loading}/>} data={product} onClick={this.onToggle} content={"Commander"}/>
                    : <Label className={"add-label enabled"} icon={<Icon name={"chevron right"} loading={loading}/>} data={product} onClick={this.onToggle} content={"Ajouter"}/>;
            }
        }

        const supplierDisplay = this.getSupplierDisplay(product);
        let listItemClassName = "shopping-list-product-item";

        if (shoppingMode && !orderEnabled) {
            listItemClassName += " order-disabled";
        }

        if (product.promo) {
            listItemClassName += " promo";
        }

        return (
            <List.Item className={listItemClassName} >
                {product.promo &&
                <Label ribbon color='red'>Promotion</Label>
                }
                <Image src={product.productPicture}/>
                <List.Content>
                    {/*<NavLink to={States.PRODUCT_VARIANT.replace(':id', product.productId).replace(':supplierId', product.supplierId)}>*/}
                        <h4>{product.productName} {product.origin.length > 0 && <span className={"product-origin"}>{product.origin}<br/></span>} {isAppDev && <span style={{color: 'limegreen'}}>&nbsp;(ID:{product.productId} | {product.productName2} | REF:{product.supplierReference})</span>}</h4>
                    {/*</NavLink>*/}
                    {supplierDisplay} - <PriceWidget product={product}/>
                </List.Content>

                <div className={"item-actions"}>
                    <QuantityWidget product={product} onQuantityChanged={this.onQuantityChanged} />
                    {label}
                </div>
            </List.Item>
        );
    };
}

export default withRouter(connect(state => ({
    authToken: state.frontState.authToken,
    isAppDev: state.frontState.isAppDev
}))(ShoppingListSupplierProduct));