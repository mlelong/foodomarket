import React from "react";
import {Segment} from "semantic-ui-react";

const Loading = (props) => {
    return props.pastDelay ? <Segment id={"loading-segment"} loading></Segment> : null;
};

export default Loading;
