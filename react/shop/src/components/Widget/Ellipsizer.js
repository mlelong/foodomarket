import React, {useState} from "react";
import PropTypes from "prop-types";
import Measure from "react-measure";
import {Icon} from "semantic-ui-react";

const Ellipsizer = props => {
    const [open, setOpen] = useState(false);
    const [dimensions, setDimensions] = useState({
        width: -1,
        height: -1
    });
    const {height} = dimensions;

    return (
        <Measure
            bounds
            onResize={contentRect => {
                if (contentRect.bounds.width != dimensions.width) {
                    setDimensions(contentRect.bounds);
                }
            }}
        >
            {({ measureRef }) => {
                const containerStyle = {
                    position: 'relative',
                };

                if (!open && height > props.height) {
                    containerStyle.overflow = 'hidden';
                    containerStyle.maxHeight = `${props.height}px`;
                }

                const ellipsisStyle = {
                    cursor: 'pointer',
                    textAlign: 'center',
                    paddingTop: '8px',
                    paddingBottom: '2px',
                    fontSize: '18px',
                    color: '#00A947'
                };

                if (!open) {
                    ellipsisStyle.position = 'absolute';
                    ellipsisStyle.left = 0;
                    ellipsisStyle.right = 0;
                    ellipsisStyle.bottom = 0;
                    ellipsisStyle.background = 'linear-gradient(rgba(255, 255, 255, 0.7), #ffffff)';
                } else {
                    ellipsisStyle.position = 'relative';
                    ellipsisStyle.background = 'none';
                }

                return (
                    <div ref={measureRef}>
                        <div className="ellipsizer-container" style={containerStyle}>
                            {props.children}

                            {height > props.height &&
                            <div style={ellipsisStyle} className={"ellipsizer-ellipsis " + (open ? "open" : "close")} onClick={() => setOpen(!open)}>
                                <Icon name={open ? 'angle double up' : 'ellipsis horizontal'}/>
                            </div>
                            }
                        </div>
                    </div>
                )
            }}
        </Measure>
    );
};

Ellipsizer.propTypes = {
    height: PropTypes.number.isRequired,
};

Ellipsizer.defaultProps = {
    height: 100
};

export default Ellipsizer;
