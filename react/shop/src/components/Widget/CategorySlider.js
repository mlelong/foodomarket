import React from "react";
import {connect} from "react-redux";
import {Card, Header} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {generateUrl} from "shop/router";

class CategorySlider extends React.Component
{
    state = {
        categories: this.props.initialValues.hasOwnProperty('topCategories') ? this.props.initialValues.topCategories : []
    };

    componentDidMount() {
        const {categories} = this.state;
        // const {dispatch} = this.props;

        if (categories.length === 0) {
            Get(generateUrl('app_front_top_categories'))
                .then((data) => {
                    this.setState({categories: data.topCategories});
                }).catch((exception) => {
                    console.log(exception);
                })
            ;
        }
        // else {
            // dispatch(clearInitialValues(['topCategories']));
        // }
    }

    render() {
        const {isMobile} = this.props;
        const {categories} = this.state;

        return (
            <div>
                <Header dividing as={'h2'}>Nos rayons</Header>

                {categories.length > 0 &&
                <Card.Group itemsPerRow={!isMobile ? categories.length : 2} centered doubling={!isMobile}>
                    {categories.map((category, index) =>
                        <Card
                            as={Link}
                            to={States.BROWSE_CATALOG.replace(':slug', category.slug)}
                            key={index}
                            image={category.image}
                            header={category.name}
                            // description={category.description}
                            // extra={`${category.products} produits`}
                            meta={`${category.offers} offres`}
                        />
                    )}
                </Card.Group>
                }
            </div>
        );
    }
}

export default connect(state => ({
    isMobile: state.frontState.isMobile,
    initialValues: state.frontState.initialValues
}))(CategorySlider);