import React from "react";
import {Dropdown, Icon} from "semantic-ui-react";
import {connect} from "react-redux";
import {logout} from "shop/actions";
import {withRouter} from "react-router";
import {States} from "shop/constants/frontConstants";
import {generateUrl} from "shop/router";

class UserWidget extends React.Component
{
    static SIGN_OUT = 'sign-out';
    static MY_ACCOUNT = 'my-account';

    handleOptionClick = (event, {value}) => {
        const {dispatch, history} = this.props;

        switch (value) {
            case UserWidget.SIGN_OUT:
                dispatch(logout());
                break ;
            case UserWidget.MY_ACCOUNT:
                history.push(States.ACCOUNT);
                break ;
        }
    };

    switchRestaurant = (e, {value}) => {
        if (parseInt(this.props.restaurant) !== parseInt(value)) {
            document.location = generateUrl('app_front_switch_restaurant', {restaurantId: value});
        }
    };

    render() {
        const {user, restaurants, restaurant} = this.props;

        const trigger = (
            <span>
                <Icon id={"user-icon"} circular name={"user circle"} size={"large"}/> {user.username}
            </span>
        );

        return (
            <Dropdown id="user-widget" trigger={trigger} direction={"left"} floating>
                <Dropdown.Menu>
                    <Dropdown id={"restaurant-dropdown"} item placeholder={"Choisir un restaurant"} trigger={<span id={"restaurant-submenu-item"}>Restaurant</span>} options={restaurants} value={restaurant} onChange={this.switchRestaurant}/>
                    <Dropdown.Item key={UserWidget.MY_ACCOUNT} text="Mon compte" icon={"user"} value={UserWidget.MY_ACCOUNT} onClick={this.handleOptionClick}/>

                    <Dropdown.Divider></Dropdown.Divider>

                    <Dropdown.Item key={UserWidget.SIGN_OUT} text="Déconnexion" icon={"sign out"} value={UserWidget.SIGN_OUT} onClick={this.handleOptionClick}/>
                </Dropdown.Menu>
            </Dropdown>
        );
    }
}

export default withRouter(connect(state => (
    {
        user: state.frontState.user,
        restaurants: state.frontState.restaurants,
        restaurant: state.frontState.restaurant,
    }
))(UserWidget));