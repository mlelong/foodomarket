import React from "react";
import {connect} from "react-redux";
import {Header} from "semantic-ui-react";
import SliderWidget from "./SliderWidget";
import {generateUrl} from "shop/router";

const TopProductsWidget = props => {
    return <SliderWidget
        header={<Header dividing as={'h2'}>Nos meilleures offres</Header>}
        products={props.topProducts}
        url={generateUrl('app_front_top_products')}
    />
};

export default connect(state => ({
    topProducts: state.frontState.initialValues.topProducts
}))(TopProductsWidget);
