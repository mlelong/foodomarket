import React from "react";
import {connect} from "react-redux";
import {Header} from "semantic-ui-react";
import SliderWidget from "./SliderWidget";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {generateUrl} from "shop/router";

const PromosWidget = props => {
    return <SliderWidget
        header={<Header dividing as={'h2'}><Link to={States.CATALOG_PROMOS}>Les promos</Link></Header>}
        products={props.promos}
        url={generateUrl('app_front_promos')}
    />
};

export default connect(state => ({
    promos: state.frontState.initialValues.promos
}))(PromosWidget);
