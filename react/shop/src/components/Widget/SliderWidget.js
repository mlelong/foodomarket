import React, {useEffect, useState} from "react";
import {Get} from "shop/utils/fetch";
import {connect} from "react-redux";
import Slider from "react-slick";
import ProductCard from "shop/pages/catalog/ProductCard";
import PropTypes from "prop-types";

const SliderWidget = props => {
    const [products, setProducts] = useState(props.products);

    useEffect(() => {
        if (!products) {
            Get(props.url).then(data => {
                setProducts(data);
            });
        }
    });

    const {isMobile} = props;
    const settings = {
        dots: true,
        infinite: true,
        speed: 2000,
        autoplaySpeed: 5000,
        autoplay: true,
        slidesToShow: !isMobile ? 5 : 2,
        slidesToScroll: !isMobile ? 5 : 2,
        arrows: false
    };

    return (
        products && products.length >= settings.slidesToShow && <div>
            {props.header}

            <Slider {...settings}>
                {products.map(product =>
                    <ProductCard key={product.id} product={product}/>
                )}
            </Slider>
        </div>
        || <div></div>
    );
};

SliderWidget.propTypes = {
    url: PropTypes.string.isRequired,
    header: PropTypes.node.isRequired,
    products: PropTypes.array
};

export default connect(state => ({
    isMobile: state.frontState.isMobile,
}))(SliderWidget);
