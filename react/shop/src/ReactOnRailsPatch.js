import {renderToString} from "react-dom/server";
import ComponentRegistry from "react-on-rails/node_package/lib/ComponentRegistry";
import isCreateReactElementResultNonReactComponent from "react-on-rails/node_package/lib/isCreateReactElementResultNonReactComponent";
import createReactElement from "react-on-rails/node_package/lib/createReactElement";
import buildConsoleReplay from "react-on-rails/node_package/lib/buildConsoleReplay";
import handleError from "react-on-rails/node_package/lib/handleError";
import stringify from "@babel/runtime-corejs2/core-js/json/stringify";

const ReactOnRailsPatch = {
    handleReactElementOrRouterResult(options, reactElementOrRouterResult) {
        let htmlResult = '';
        let hasErrors = false;

        const {name, domNodeId, trace} = options;

        if (isCreateReactElementResultNonReactComponent(reactElementOrRouterResult)) {
            // We let the client side handle any redirect
            // Set hasErrors in case we want to throw a Rails exception
            hasErrors = !!reactElementOrRouterResult.routeError;

            if (hasErrors) {
                console.error("React Router ERROR: ".concat(stringify(reactElementOrRouterResult.routeError)));
            }

            if (reactElementOrRouterResult.redirectLocation) {
                if (trace) {
                    const redirectLocation = reactElementOrRouterResult.redirectLocation;
                    const redirectPath = redirectLocation.pathname + redirectLocation.search;
                    console.log("ROUTER REDIRECT: ".concat(name, " to dom node with id: ").concat(domNodeId, ", redirect to ").concat(redirectPath));
                } // For redirects on server rendering, we can't stop Rails from returning the same result.
                // Possibly, someday, we could have the rails server redirect.

            } else {
                htmlResult = reactElementOrRouterResult.renderedHtml;
            }
        } else {
            htmlResult = renderToString(reactElementOrRouterResult);
        }

        return stringify({
            html: htmlResult,
            consoleReplayScript: buildConsoleReplay(),
            hasErrors: hasErrors
        });
    },

    serverRenderReactComponent(options, handleResponse) {
        let htmlResult = '';
        let hasErrors = false;

        const {name, domNodeId, trace, props, railsContext} = options;

        const onError = (e) => {
            hasErrors = true;
            htmlResult = handleError({
                e: e,
                name: name,
                serverSide: true
            });

            handleResponse(stringify({
                html: htmlResult,
                consoleReplayScript: buildConsoleReplay(),
                hasErrors: hasErrors
            }));
        };

        try {
            const componentObj = ComponentRegistry.get(name);

            if (componentObj.isRenderer) {
                throw new Error("Detected a renderer while server rendering component '".concat(name, "'. See https://github.com/shakacode/react_on_rails#renderer-functions"));
            }

            const promiseOrReactElementOrRouterResult = createReactElement({
                componentObj: componentObj,
                domNodeId: domNodeId,
                trace: trace,
                props: props,
                railsContext: railsContext
            });

            if (promiseOrReactElementOrRouterResult instanceof Promise) {
                promiseOrReactElementOrRouterResult.then(reactElementOrRouterResult => {
                    handleResponse(ReactOnRailsPatch.handleReactElementOrRouterResult(options, reactElementOrRouterResult));
                }).catch(e => onError(e));
            } else {
                handleResponse(ReactOnRailsPatch.handleReactElementOrRouterResult(options, promiseOrReactElementOrRouterResult));
            }
        } catch (e) {
            onError(e);
        }
    }
};

export default ReactOnRailsPatch;
