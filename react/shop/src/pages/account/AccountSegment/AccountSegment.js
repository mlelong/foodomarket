import React from "react";
import {Grid, Segment, Step} from "semantic-ui-react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import Login from "../Login";
import {States} from "shop/constants/frontConstants";
import {Helmet} from "react-helmet-async";
import RequestPasswordReset from "../RequestPasswordReset";
import PasswordReset from "../PasswordReset";
import Register from "shop/pages/account/Register";
import RegisterFinal from "shop/pages/account/RegisterFinal";
require("./AccountSegment.scss");

function StepperRegister(props)
{
    const {isRegister, isRegisterFinal} = props;

    if (isRegister || isRegisterFinal) {
        return (
            <Grid.Column widescreen={7} largeScreen={7} tablet={12} mobile={16} className="segment centered">
                <Step.Group unstackable id={"stepper"} fluid>
                    <Step active={isRegister} completed={isRegisterFinal} id={"stepper_step1"}>
                        <Step.Content>
                            <Step.Title style={{color: 'green'}}>Etape 1</Step.Title>
                            <Step.Description> Informations personnelles </Step.Description>
                        </Step.Content>
                    </Step>
                    <Step active={isRegisterFinal} id={"stepper_step2"} >
                        <Step.Content>
                            <Step.Title style={{color: 'green'}}>Etape 2</Step.Title>
                            <Step.Description> Informations de livraison </Step.Description>
                        </Step.Content>
                    </Step>
                </Step.Group>

                {isRegister && <Register/>}
                {isRegisterFinal && <RegisterFinal/>}
            </Grid.Column>
        );
    }

    return <React.Fragment/>;
}
export const AccountSegment = props => {
    const {match} = props;
    let pageTitle = '';

    if (match.path === States.ACCOUNT_LOGIN) {
        pageTitle = 'Connexion';
    } else if (match.path === States.ACCOUNT_REGISTER) {
        pageTitle = 'Inscription';
    } else if (match.path === States.ACCOUNT_REQUEST_RESET_PASSWORD) {
        pageTitle = 'Mot de passe oublié';
    } else if (match.path === States.ACCOUNT_RESET_PASSWORD) {
        pageTitle = 'Changement de votre mot de passe';
    } else if (match.path === States.ACCOUNT_REGISTER_FINAL) {
        pageTitle = 'Finaliser mon inscription';
    }
    return (
        <Segment id={"account-segment"}>
            <Helmet
                titleTemplate="%s | FoodoMarket.com"
            >
                <title children={pageTitle}/>
                <meta name="description"
                      content="Professionnels de la restauration, achetez vos produits alimentaires au meilleur prix sur FoodoMarket. Livraison en A pour B dans toute l'île-de-France."/>
            </Helmet>
            <Grid centered>
                <StepperRegister isRegister={match.path === States.ACCOUNT_REGISTER} isRegisterFinal={match.path === States.ACCOUNT_REGISTER_FINAL} />

                {match.path === States.ACCOUNT_LOGIN &&
                <Grid.Column widescreen={8} largeScreen={8} tablet={12} mobile={16}>
                    <Login/>
                </Grid.Column>
                }
                {match.path === States.ACCOUNT_REQUEST_RESET_PASSWORD &&
                <Grid.Column widescreen={11} largeScreen={14} tablet={14} mobile={16}>
                    <RequestPasswordReset/>
                </Grid.Column>
                }
                {match.path === States.ACCOUNT_RESET_PASSWORD &&
                <Grid.Column widescreen={11} largeScreen={14} tablet={14} mobile={16}>
                    <PasswordReset/>
                </Grid.Column>
                }
            </Grid>
        </Segment>
    );
};
export default withRouter(connect()(AccountSegment));