import React, {useState} from "react";
import {Card, Message} from "semantic-ui-react";
import {connect} from "react-redux";
import BaseForm from "shop/form-theme/semanticui/BaseForm";
import Liform from "shop/utils/form/Liform";
import {Post} from "shop/utils/fetch";
import {generateUrl} from "shop/router";
import syncValidation from "shop/utils/form/syncValidation";

const RequestPasswordReset = props => {
    const [form, setForm] = useState(props.initialValues.hasOwnProperty('request-password-reset-form') ? props.initialValues['request-password-reset-form'] : null);
    const [submitted, setSubmitted] = useState(false);
    const [emailExists, setEmailExists] = useState(true);

    const onSubmit = (data) => {
        setEmailExists(true);

        return Post(generateUrl('app_front_request_password_reset'), data, 'sf-form')
            .then(data => {
                if (data.ok) {
                    setSubmitted(true);
                } else {
                    setEmailExists(false);
                }
            }).catch(exception => {
                console.log(exception);
            })
        ;
    };

    return (
        <Card id="password-reset-segment" fluid>
            <Card.Content>
                <div style={{textAlign:'center',paddingBottom:'25px'}}>
                    <h2 style={{fontWeight:'normal',fontFamily:'Roboto-Light,sans-serif',color:'#9a9a9a'}}>Mot de passe oublié ?</h2>
                </div>

                {!submitted &&
                <Message info>
                    <Message.Content>
                        <Message.Header>Pas de panique !</Message.Header>
                        <p>Saisissez votre email ci-dessous afin de demander un nouveau mot de passe.</p>
                    </Message.Content>
                </Message>
                }

                {!emailExists &&
                <Message negative>
                    <Message.Content>
                        Cette adresse email n'existe pas ou ne correspond pas à un compte client.
                    </Message.Content>
                </Message>
                }

                {!submitted &&
                <Liform baseForm={BaseForm} syncValidation={syncValidation(form.schema, {
                    email: "Votre email est requis."
                })} formKey={"requestPasswordReset"} schema={form.schema} initialValues={form.values} onSubmit={onSubmit}/> ||
                <Message positive>
                    <Message.Content>
                        <p>Vous allez recevoir un email contenant un lien qui vous permettra de redéfinir votre mot de passe.</p>
                        <p>Pensez à vérifier dans vos SPAM si vous ne voyez pas l'email.</p>
                    </Message.Content>
                </Message>
                }
            </Card.Content>
        </Card>
    );
};

export default connect(state => ({
    initialValues: state.frontState.initialValues
}))(RequestPasswordReset);
