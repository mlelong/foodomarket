import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Grid, Header, Message} from "semantic-ui-react";
import RegisterForm from "./RegisterForm";
import {Get, Post} from "shop/utils/fetch";
import {clearInitialValues} from "shop/actions";
import ReactGA from "react-ga";
import ReactPixel from "react-facebook-pixel";
import {generateUrl} from "shop/router";

function RegisterFull(props)
{
    const [form, setForm] = useState(props.initialValues.hasOwnProperty('register-form') ? props.initialValues['register-form'] : null);
    const [submitted, setSubmitted] = useState(false);
    const {dispatch} = props;

    useEffect(() => {
        if (form === null) {
            Get(generateUrl('app_front_register_form'))
                .then(data => setForm({schema: data.schema, values: data.values, step: data.step}))
            ;
        } else {
            return () => {
                dispatch(clearInitialValues(['register-form']));
            };
        }
    });

    const onSubmit = (data) => {
        return Post(generateUrl('app_front_register_form_submit', {step: 4}), data, 'sf-form')
            .then(data => {
                if (data.ok) {
                    ReactGA.event({
                        category: 'conversion',
                        action: 'Inscription'
                    }, ['analytics']);

                    if (typeof(gtag_report_conversion) === typeof(Function)) {
                        gtag_report_conversion();
                    }

                    ReactPixel.track('CompleteRegistration');

                    setSubmitted(true);
                } else {
                    console.log(data);
                }
            }).catch(exception => {
                console.log(exception);
            })
            ;
    };

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column computer={7} tablet={10} mobile={16}>
                    <Header as={'h1'} dividing>Inscription</Header>
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    {!submitted
                    && <RegisterForm form={form} onSubmit={onSubmit}/>
                    || <Message success visible>
                        <Message.Header>Merci</Message.Header>
                        <Message.Content>
                            <p>
                                Vous allez recevoir dans quelques instants un lien de validation par mail afin de finaliser votre inscription.<br/>
                                PS : Pour éviter toute problématique de mise en SPAM, n'oubliez pas d'ajouter olivier@foodomarket.fr à vos listes de contact
                            </p>
                        </Message.Content>
                    </Message>
                    }
                </Grid.Column>
            </Grid.Row>
        </Grid>
    );
}

export default connect(state => ({
    initialValues: state.frontState.initialValues,
}))(RegisterFull);
