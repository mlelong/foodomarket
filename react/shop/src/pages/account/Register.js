import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Button, Card, Form, Grid, Header, Message, Step} from "semantic-ui-react";
import {Get, Post} from "shop/utils/fetch";
import ReactGA from "react-ga";
import {generateUrl} from "shop/router";
import Liform, {renderFieldsByName} from "shop/utils/form/Liform";
import syncValidation from "shop/utils/form/syncValidation";
import SemanticUITheme from "shop/form-theme/semanticui/SemanticUITheme";
import {processSubmitErrors} from "liform-react";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";


const RegisterForm = props => {
    const {schema, handleSubmit, error, submitting, context, errorMessage} = props;

    context.change = props.change;

    return (
        <Form onSubmit={handleSubmit} loading={submitting}>
            <Card fluid>
                <Card.Content>
                    <Card.Header textAlign='center'>
                        Créer un compte <span style={{color: "#359A32"}}>FoodoMarket</span> maintenant <br/> pour profiter de tous ses avantages
                    </Card.Header>
                </Card.Content>

                {error && <Card.Content><Message visible error content={error}/></Card.Content>}

                <Card.Content>
                    <Card.Description>
                        {renderFieldsByName(schema, SemanticUITheme, ['firstName', 'email', 'mobile', 'password', 'password_again'], '', context)}
                    </Card.Description>
                </Card.Content>


                {errorMessage && <Card.Content><Message visible error header={errorMessage}/></Card.Content>}

                <Card.Content extra>
                    <div style={{textAlign: 'center'}}>
                        <Button color='green' size='large' type={"submit"} id="submit-register-button" disabled={submitting} loading={submitting}>
                            Valider
                        </Button>

                        <p> <br/> Déjà client ? <Link style={{color: "green"}} to={States.ACCOUNT_LOGIN}> Connectez-vous ! </Link></p>
                    </div>
                </Card.Content>
            </Card>
        </Form>
    );
};

function Register(props)
{
    const [form, setForm] = useState(props.form);
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(undefined);

    useEffect(() => {
        if (!form) {
            Get(generateUrl('app_front_acquisition_register_form'))
                .then(data => setForm(data.form))
            ;
        }
    });

    const onSubmit = (data) => {
        return Post(generateUrl('app_front_acquisition_register_form'), data, 'sf-form')
            .then(data => {
                if (data.ok) {
                    ReactGA.event({
                        category: 'conversion',
                        action: 'Inscription'
                    }, ['analytics']);

                    // Redirect to finalize subscription page
                    document.location = data.continue_url;

                    // setSubmitted(true);
                } else {
                    if (data.hasOwnProperty('message')) {
                        setError(data.message);
                    }

                    setForm(data.form);
                    processSubmitErrors(data.form.errors);
                }
            })
        ;
    };

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column computer={7} tablet={10} mobile={16}>

                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    {!submitted
                    && <div>
                        {form &&
                        <Liform
                            schema={form.schema}
                            initialValues={form.values}
                            enableReinitialize={false}
                            destroyOnUnmount={false}
                            formKey={'register'}
                            syncValidation={syncValidation(form.schema, {
                                firstName: 'Merci de renseigner votre nom',
                                email: "Merci de renseigner votre email",
                                mobile: "Merci de renseigner votre numéro de téléphone",
                                password: "Merci de renseigner votre mot de passe",
                                password_again: "Merci de retaper votre mot de passe",
                            })}
                            onSubmit={onSubmit}
                            baseForm={RegisterForm}
                            errorMessage={error}
                        />
                        }
                    </div>
                    || <Message success visible>
                        <Message.Header>Merci</Message.Header>
                        <Message.Content>
                            <p>
                                Vous allez recevoir dans quelques instants un lien de validation par mail afin de finaliser votre inscription.<br/>
                                PS : Pour éviter toute problématique de mise en SPAM, n'oubliez pas d'ajouter olivier@foodomarket.fr à vos listes de contact
                            </p>
                        </Message.Content>
                    </Message>
                    }
                </Grid.Column>
            </Grid.Row>
        </Grid>
    );
}

export default connect(state => ({
    form: state.frontState.initialValues['acquisition-reg-form'],
}))(Register);
