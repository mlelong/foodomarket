import Loadable from "react-loadable";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";
import Loading from "shop/components/Widget/Loading";

const accountLoader = Loadable({
    loader: () => import('./AccountSegment/AccountSegment'),
    loading: Loading,
    delay: 500
});

const accountCheckLoader = Loadable({
    loader: () => import('./AccountCheckSegment'),
    loading: Loading,
    delay: 500
});

const myAccountLoader = Loadable({
    loader: () => import('./MyAccountSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.ACCOUNT_LOGIN, true, accountLoader);
addRoute(States.ACCOUNT_REGISTER, true, accountLoader);
addRoute(States.ACCOUNT_REQUEST_RESET_PASSWORD, true, accountLoader);
addRoute(States.ACCOUNT_RESET_PASSWORD, true, accountLoader);
addRoute(States.ACCOUNT_CHECK, true, accountCheckLoader);
addRoute(States.ACCOUNT_REGISTER_FINAL, true, accountLoader);
addRoute(States.ACCOUNT, true, myAccountLoader);
