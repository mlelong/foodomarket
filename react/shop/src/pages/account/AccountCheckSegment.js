import React from "react";
import {Message, Segment} from "semantic-ui-react";
import {connect} from "react-redux";

export const AccountCheckSegment = props => {
    const {accountVerified} = props;

    return (
        <Segment id="account-check-segment">
            {accountVerified &&
            <Message positive>
                <Message.Header>Votre inscription est à présent finalisée.</Message.Header>
                <p>
                    Dès la validation de vos informations par notre équipe, vous pourrez accéder à vos grossistes dans cet espace et passer vos premières commandes.<br/>
                    Certains grossistes demandent des informations complémentaires que vous recevrez par email.
                </p>
            </Message>
            ||
            <Message negative>
                <Message.Header>Compte déjà activé</Message.Header>
                <p>Ce token a expiré</p>
            </Message>}
        </Segment>
    );
};

export default connect(state => ({
    accountVerified: state.frontState.initialValues.hasOwnProperty('account-verified') ? state.frontState.initialValues['account-verified'] : false
}))(AccountCheckSegment);
