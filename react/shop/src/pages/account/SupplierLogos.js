import logoHallesPrestige from "img/suppliers-logo/logo_hallesprestige.png";
import logoAlazard from "img/suppliers-logo/logo_alazard.jpg";
import logoBermudes from "img/suppliers-logo/logo_bermudes.png";
import logoChassineau from "img/suppliers-logo/logo_chassineau.png";
import logoDaumesnil from "img/suppliers-logo/logo_daumesnil.png";
import logoDemarne from "img/suppliers-logo/logo_demarne.jpg";
import logoDistriResto from "img/suppliers-logo/logo_distriresto.png";
import logoDomafrais from "img/suppliers-logo/logo_domafrais.png";
import logoHallesTrottemant from "img/suppliers-logo/logo_halles_trottemant.png";
import logoEpisaveurs from "img/suppliers-logo/logo_episaveurs.png";
import logoJacob from "img/suppliers-logo/logo_jacob.png";
import logoMandar from "img/suppliers-logo/logo_mandar.jpg";
import logoMetro from "img/suppliers-logo/logo_metro.jpg";
import logoTerreAzur from "img/suppliers-logo/logo_terreazur.png";
import logoTransgourmet from "img/suppliers-logo/logo_transgourmet.jpg";
import logoVergerEtoile from "img/suppliers-logo/logo_verger_etoile.jpg";
import logoReynaud from "img/suppliers-logo/logo_reynaud.webp";
import logoPrimeursPassion from "img/suppliers-logo/logo_primeurs_passion.png";
import logoEurodis from "img/suppliers-logo/logo_eurodis.png";
import logoActivia from "img/suppliers-logo/logo_activia.jpg";
import logoFory from "img/suppliers-logo/logo_fory_viandes.png";
import logoMartin from "img/suppliers-logo/logo_martin.png";
import logoHuguenin from "img/suppliers-logo/logo_huguenin.png";
import logoProdal from "img/suppliers-logo/logo_prodal.jpg";
import logoJoceane from "img/suppliers-logo/logo_joceane.jpg";
import logoLecrevisse from "img/suppliers-logo/logo_lecrevisse.jpg";
import logoBuisson from "img/suppliers-logo/logo_buisson.jpeg";
import logoDelon from "img/suppliers-logo/logo_delon.jpg";
import logoAbcPeyraud from "img/suppliers-logo/logo_abc_peyraud.jpeg";
import logoUgalait from "img/suppliers-logo/logo_ugalait.png";
import logoPlacet from "img/suppliers-logo/logo_placet.jpg";
import logoDaguerre from "img/suppliers-logo/logo_daguerre.png";

const Logos = {
    "Halles Prestige": logoHallesPrestige,
    "Alazard": logoAlazard,
    "Bermudes": logoBermudes,
    "Chassineau": logoChassineau,
    "Daumesnil": logoDaumesnil,
    "Distri-Resto": logoDistriResto,
    "EpiSaveurs": logoEpisaveurs,
    "Demarne": logoDemarne,
    "Domafrais": logoDomafrais,
    "Halles Trottemant": logoHallesTrottemant,
    "Jacob": logoJacob,
    "Halles Mandar": logoMandar,
    "Metro": logoMetro,
    "Terre Azur": logoTerreAzur,
    "Transgourmet": logoTransgourmet,
    "Verger Etoile": logoVergerEtoile,
    "Reynaud": logoReynaud,
    "Primeurs Passion": logoPrimeursPassion,
    "Eurodis": logoEurodis,
    "Activia": logoActivia,
    "Fory Viandes": logoFory,
    "Martin": logoMartin,
    "Huguenin": logoHuguenin,
    "Prodal": logoProdal,
    "J'Oceane": logoJoceane,
    "L'Ecrevisse": logoLecrevisse,
    "Buisson": logoBuisson,
    "Delon": logoDelon,
    "ABC Peyraud": logoAbcPeyraud,
    "Ugalait": logoUgalait,
    "Placet": logoPlacet,
    "Daguerre": logoDaguerre,
};

export default Logos;
