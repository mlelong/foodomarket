import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Grid, Menu, Message, Segment} from "semantic-ui-react";
import {processSubmitErrors} from "liform-react";
import BaseForm from "shop/form-theme/semanticui/BaseForm";
import {toast} from "react-semantic-toasts";
import PropTypes from "prop-types";
import {generateUrl} from "shop/router";
import {Get, Post} from "shop/utils/fetch";
import Liform from "shop/utils/form/Liform";

const FormContainer = props => {
    const [form, setForm] = useState(null);

    useEffect(() => {
        if (form === null) {
            Get(props.formUrl).then(data => {
                setForm(data.form);
            });
        }
    });

    return (
        <div>
            {props.headerMessage}

            {form &&
            <Liform
                schema={form.schema}
                initialValues={form.values}
                enableReinitialize={true}
                baseForm={BaseForm}
                formKey={props.formKey}
                onSubmit={data => {
                    return Post(props.formUrl, data, 'sf-form').then(data => {
                        if (!data.ok) {
                            processSubmitErrors(data.form.errors);
                        } else {
                            setForm(null);

                            if (props.postSubmit) {
                                props.postSubmit();
                            }
                        }
                    });
                }}
            />
            }
        </div>
    );
};

FormContainer.propTypes = {
    formUrl: PropTypes.string.isRequired,
    formKey: PropTypes.string.isRequired,
    headerMessage: PropTypes.node,
    postSubmit: PropTypes.func
};

const MyAccountLogin = props => {
    const [form, setForm] = useState(null);
    const url = generateUrl('app_front_update_password_form');

    useEffect(() => {
        if (form === null) {
            Get(url).then(data => {
                setForm({schema: data.schema, initialValues: data.values});
            });
        }
    });

    return (
        <div>
            <Message info>
                <Message.Content>
                    <p>
                        Ici vous pouvez mettre à jour votre mot de passe de connexion à foodomarket.
                    </p>
                </Message.Content>
            </Message>

            {form !== null && <Liform schema={form.schema} initialValues={form.initialValues} baseForm={BaseForm} formKey={"updatePassword"} onSubmit={(data) => {
                Post(url, data, 'sf-form')
                    .then(data => {
                        setForm({schema: JSON.parse(data.schema), initialValues: data.values});
                    })
                ;
            }}/>}
        </div>
    );
};

const MyAccountRestaurants = props => {
    return (
        <div>
            <Message info>
                <Message.Content>
                    <p>
                        Ici vous pouvez mettre à jour les coordonnées de votre restaurant.
                    </p>
                </Message.Content>
            </Message>
        </div>
    );
};

export const MyAccountBankDetails = props => {
    return <FormContainer
        formUrl={generateUrl('app_front_my_account_bank_details_form')}
        formKey="bankDetails"
        headerMessage={props.headerMessage}
        postSubmit={props.postSubmit}
    />;
};

MyAccountBankDetails.propTypes = {
    headerMessage: PropTypes.node,
    postSubmit: PropTypes.func
};

MyAccountBankDetails.defaultProps = {
    headerMessage:
        <Message info>
            <Message.Content>
                <p>
                    Ici vous pouvez mettre à jour vos coordonnées bancaires.
                </p>
            </Message.Content>
        </Message>,
    postSubmit: () => {
        toast({
            type: 'success',
            icon: null,
            title: "Enregistrement effectué",
            description: "L'iban et le rib ont bien été enregistrés",
            time: 3000,
        });
    }
};

export const SepaRequirements = props => {
    return <FormContainer
        formUrl={generateUrl('app_front_my_account_sepa_requirements_form')}
        formKey="sepaRequirements"
        headerMessage={props.headerMessage}
        postSubmit={props.postSubmit}
    />
};

SepaRequirements.propTypes = {
    headerMessage: PropTypes.node,
    postSubmit: PropTypes.func
};

SepaRequirements.defaultProps = {
    headerMessage:
        <Message info>
            <Message.Content>
                <p>
                    Ici vous pouvez mettre à jour vos coordonnées bancaires.
                </p>
            </Message.Content>
        </Message>
};

const MyAccountSegment = props => {
    const [activeItem, setActiveItem] = useState('bank-details');
    const handleItemClick = (e, { name }) => setActiveItem(name);

    return (
        <Segment id={"my-account-segment"}>
            <Grid>
                <Grid.Column width={3}>
                    <Menu pointing secondary vertical>
                        <Menu.Item
                            name='bank-details'
                            active={activeItem === 'bank-details'}
                            onClick={handleItemClick}
                        >Coordonnées bancaires</Menu.Item>
                    </Menu>
                </Grid.Column>
                <Grid.Column width={13}>
                    {activeItem === 'login' && <MyAccountLogin/>}
                    {activeItem === 'restaurants' && <MyAccountRestaurants/>}
                    {activeItem === 'bank-details' && <MyAccountBankDetails/>}
                </Grid.Column>
            </Grid>
        </Segment>
    );
};

export default connect()(MyAccountSegment);
