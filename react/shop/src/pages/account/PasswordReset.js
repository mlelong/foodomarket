import React, {useState} from "react";
import {Card, Message} from "semantic-ui-react";
import BaseForm from "shop/form-theme/semanticui/BaseForm";
import Liform from "shop/utils/form/Liform";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {withRouter} from "react-router";
import {Post} from "shop/utils/fetch";
import {generateUrl} from "shop/router";
import syncValidation from "shop/utils/form/syncValidation";

const PasswordReset = props => {
    const [form, setForm] = useState(props.initialValues.hasOwnProperty('password-reset-form') ? props.initialValues['password-reset-form'] : null);
    const [submitted, setSubmitted] = useState(false);
    const {match} = props;

    const onSubmit = formData => {
        return Post(generateUrl('app_front_reset_password_page', {token: match.params.token}), formData, 'sf-form')
            .then(data => {
                if (data.ok) {
                    setSubmitted(true);
                } else {
                    console.log(data);
                }
            })
        ;
    };

    return (
        <Card id="password-reset-segment" fluid>
            <Card.Content>
                <div style={{textAlign:'center',paddingBottom:'25px'}}>
                    <h2 style={{fontWeight:'normal',fontFamily:'Roboto-Light,sans-serif',color:'#9a9a9a'}}>Changement de votre mot de passe</h2>
                </div>

                {!submitted &&
                    <Message info>
                        <Message.Content>
                            <p>
                                Afin de modifier votre mot de passe, veuillez en saisir un nouveau ci-dessous.
                            </p>
                        </Message.Content>
                    </Message>
                }

                {!submitted &&
                <Liform baseForm={BaseForm} syncValidation={syncValidation(form.schema, {
                    password: "Veuillez saisir votre nouveau mot de passe.",
                    password_again: "",
                })} formKey={"passwordReset"} schema={form.schema} initialValues={form.values} onSubmit={onSubmit}/> ||
                <Message positive>
                    <Message.Content>
                        <p>Votre mot de passe a bien été modifié.</p>
                        <p>Vous pouvez dès à présent vous connecter <Link to={States.ACCOUNT_LOGIN}>en cliquant ici</Link>.</p>
                    </Message.Content>
                </Message>
                }
            </Card.Content>
        </Card>
    );
};

export default withRouter(connect(state => ({
    initialValues: state.frontState.initialValues
}))(PasswordReset));
