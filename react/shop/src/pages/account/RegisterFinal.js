import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Button, Card, Form, Grid, Message, Divider, Icon} from "semantic-ui-react";
import {Get, Post} from "shop/utils/fetch";
import ReactGA from "react-ga";
import ReactPixel from "react-facebook-pixel";
import {generateUrl} from "shop/router";
import Liform, {renderFieldsByName} from "shop/utils/form/Liform";
import syncValidation from "shop/utils/form/syncValidation";
import {processSubmitErrors} from "liform-react";
import {withRouter} from "react-router";
import RegisterTheme from "shop/form-theme/register/RegisterTheme";
import jwtDecode from "jwt-decode";
import {States} from "shop/constants/frontConstants";
import {Link} from "react-router-dom";


const RegisterFinalForm = props => {
    const {schema, handleSubmit, error, submitting, context, theme, errorMessage} = props;

    context.change = props.change;

    const onSubmit = (formData) => {
        return handleSubmit(formData);
    };

    return (
        <Form onSubmit={onSubmit} loading={submitting}>
            <Card fluid>

                <Card.Content>
                    <Button icon labelPosition='left' size='mini'>
                        <Icon name='arrow left' />
                        <Link style={{color: "green"}} to={States.ACCOUNT_REGISTER}>Retour étape 1</Link>
                    </Button>

                    <Divider section/>
                    <Card.Header textAlign='center'>
                        Informations de livraison
                    </Card.Header>
                    <br/>

                    {error && <Card.Content><Message visible error content={error}/></Card.Content>}

                    <Card.Description>
                        {renderFieldsByName(schema, theme, ['restaurantName', 'address', 'zipCode', 'city', 'deliveryHours', 'kbisFile'], '', context)}
                    </Card.Description>

                    {errorMessage && <Card.Content><Message visible error header={errorMessage}/></Card.Content>}
                    <Divider section/>
                    <div style={{textAlign: 'center'}}>
                        <Button color='green' size="large" type={"submit"} id={"submit-register-final-button"}
                                disabled={submitting} loading={submitting}>
                            Créer mon compte
                        </Button>
                    </div>
                </Card.Content>
            </Card>
        </Form>
    );
};

function RegisterFinal(props) {
    const [form, setForm] = useState(props.form);
    const [submitted, setSubmitted] = useState(false);
    const [error, setError] = useState(undefined);

    const {match} = props;

    useEffect(() => {
        if (!form) {
            Get(generateUrl('app_front_acquisition_finalize_registration_form', {token: match.params.token}))
                .then(data => setForm(data.form))
            ;
        }
    });

    useEffect(() => {
        if (submitted) {
            const timer = setTimeout(() => {
                document.location = generateUrl('app_front_first_connect');
            }, 5000);

            return () => clearTimeout(timer);
        }
    }, [submitted]);

    const onSubmit = (formData) => {
        return Post(generateUrl('app_front_acquisition_finalize_registration_form', {token: match.params.token}), formData, 'sf-form')
            .then(data => {
                if (data.ok) {

                    ReactGA.event({
                        category: 'conversion',
                        action: 'Inscription_Terminee'
                    }, ['analytics']);

                    if (typeof (gtag_report_conversion) === typeof (Function)) {
                        gtag_report_conversion();
                    }

                    ReactPixel.track('CompleteRegistration');

                    localStorage.setItem('_token', data.token);
                    localStorage.setItem('_refresh', data.refresh_token);

                    setSubmitted(true);
                } else {
                    if (data.hasOwnProperty('message')) {
                        setError(data.message);
                    }

                    setForm(data.form);
                    processSubmitErrors(data.form.errors);
                }
            })
            ;
    };

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column computer={9} tablet={10} mobile={16}>
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    {!submitted
                    && <div>
                        {form &&
                        <Liform
                            schema={form.schema}
                            initialValues={form.values}
                            enableReinitialize={false}
                            destroyOnUnmount={false}
                            formKey={'register-final'}
                            syncValidation={syncValidation(form.schema, {
                                restaurantName: "Merci de renseigner le nom de votre restaurant",
                                address: "Merci de renseigner l'adresse de votre restaurant",
                                zipCode: "Merci de renseigner le code postal de votre restaurant",
                                city: "Merci de renseigner la ville de votre restaurant",
                                deliveryHours: "Merci de choisir un créneau de livraison",
                            })}
                            onSubmit={onSubmit}
                            baseForm={RegisterFinalForm}
                            errorMessage={error}
                            theme={RegisterTheme}
                        />
                        }
                    </div>
                    || <Message success visible>
                        <Message.Header>Merci</Message.Header>
                        <Message.Content>
                            <p>
                                Vous allez être redirigé dans quelques secondes vers votre espace personnel.<br/>
                                Si la redirection ne fonctionne pas, <a href={generateUrl('app_front_first_connect')}>cliquez
                                ici</a>.
                            </p>
                        </Message.Content>
                    </Message>
                    }
                </Grid.Column>
            </Grid.Row>
        </Grid>
    );
}

export default withRouter(connect(state => ({
    form: state.frontState.initialValues['acquisition-reg-final-form'],
}))(RegisterFinal));
