import React, {useState} from "react";
import RegisterTheme from "shop/form-theme/register/RegisterTheme";
import {renderField} from "liform-react";
import {Button, Card, Form, Grid, Header, Icon, Message, Progress, Transition} from "semantic-ui-react";
import {isRequired} from "liform-react/lib/renderFields";
import {change, SubmissionError, untouch} from "redux-form";
import PropTypes from "prop-types";
import {Get, Post} from "shop/utils/fetch";
import {connect} from "react-redux";
import {generateUrl} from "shop/router";
import Liform from "shop/utils/form/Liform";
import syncValidation from "shop/utils/form/syncValidation";

const Stepper = props => {
    const {dispatch, steps, schema, initialValues, currentStep, onSubmit, validator} = props;
    const nbSteps = steps.length;

    const [step, setStep] = useState(currentStep);
    const [direction, setDirection] = useState(true);
    const [intermediate, setIntermediate] = useState(false);
    const [formData, setFormData] = useState(initialValues);
    const [substep, setSubstep] = useState(1);

    const next = () => {
        if (step == 5) {
            if (direction && substep < formData.categories.length) {
                setIntermediate(false);
                setSubstep(substep + 1);
                return ;
            }

            if (!direction && substep > 1) {
                setIntermediate(false);
                setSubstep(substep - 1);
                return ;
            }
        }

        if (direction && step != nbSteps) {
            setStep(step + 1);
            setIntermediate(false);
        }

        if (!direction && step > 1) {
            if (step - 1 == 5) {
                setSubstep(formData.categories.length);
            }

            setStep(step - 1);
            setIntermediate(false);
        }
    };

    const prev = () => {
        if (step > 1) {
            setDirection(false);
            setIntermediate(true);
        }
    };

    const stepSubmit = (data) => {
        // Suppliers
        if (step == 5) {
            if (substep < data.categories.length) {
                setDirection(true);
                setIntermediate(true);
                return ;
            }
        }

        setFormData(data);
        setDirection(true);

        // Categories
        if (step == 4) {
            dispatch(change('registerForm', 'currentSuppliers', []));
            dispatch(untouch('registerForm', 'currentSuppliers'));
        }

        if (steps[step - 1].onSubmit !== undefined) {
            return steps[step - 1].onSubmit(data, step, () => setIntermediate(true));
        } else {
            // Will make below transition invisibile, invoke the animation and process onHide callback
            if (step != nbSteps) {
                setIntermediate(true);
            }
        }

        if (step == nbSteps) {
            return onSubmit(data);
        }
    };

    return (
        <div>
            <Card raised fluid id={"register-form-container"}>
                <Card.Content>
                    <Card.Description>
                        {steps.map((_step, key) => {
                            const fieldSchema = {...schema};
                            const properties = {};

                            for (let field of _step.fields) {
                                properties[field] = fieldSchema.properties[field];
                            }

                            fieldSchema.properties = properties;
                            fieldSchema.required = fieldSchema.required.filter(requiredField => _step.fields.includes(requiredField));

                            const visible = !intermediate && step - 1 == key;

                            return (
                                <Transition key={key} visible={visible} animation='fade' duration={500} onHide={next}>
                                    <div>
                                        <Liform
                                            destroyOnUnmount={false}
                                            syncValidation={visible && (validator && validator(fieldSchema)) || undefined}
                                            baseForm={Step}
                                            fields={_step.fields}
                                            header={_step.header}
                                            hint={_step.hint}
                                            formKey={'registerForm'}
                                            theme={RegisterTheme}
                                            schema={fieldSchema}
                                            initialValues={initialValues}
                                            onSubmit={stepSubmit}
                                            stepNum={key + 1}
                                            currentStep={step}
                                            substepNum={substep}
                                            onPrev={prev}
                                            formData={formData}
                                        />
                                    </div>
                            </Transition>
                            );
                        })}
                    </Card.Description>
                </Card.Content>

                <Card.Content extra>
                    <Progress value={step} total={nbSteps} progress="ratio" color="green" style={{marginBottom: 0}}/>
                </Card.Content>
            </Card>
        </div>
    );
};

Stepper.propTypes = {
    schema: PropTypes.object.isRequired,
    initialValues: PropTypes.object,
    steps: PropTypes.arrayOf(PropTypes.shape({
        header: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
        fields: PropTypes.arrayOf(PropTypes.string).isRequired,
        hint: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
        onSubmit: PropTypes.func
    })).isRequired,
    currentStep: PropTypes.number,
    onSubmit: PropTypes.func.isRequired,
    validator: PropTypes.func
};

Stepper.defaultProps = {
    currentStep: 1
};

const ConnectedStepper = connect()(Stepper);

const Step = props => {
    const {
        dispatch, untouch, schema, handleSubmit, header,
        theme, submitting, context, error, fields, hint, stepNum, substepNum, onPrev, formData, currentStep } = props;
    const prev = () => {
        //reset the field's error
        for (let field of fields) {
            dispatch(untouch('registerForm', field));
        }

        onPrev();
    };

    return (
        <Form onSubmit={handleSubmit} loading={submitting}>
            <Header textAlign="center">{header}</Header>

            {fields.map(field => renderField(schema.properties[field], field, theme, "", {...context, data: formData, substep: substepNum, currentStep: currentStep}, isRequired(schema, field)))}

            {hint && <Message info content={hint}/>}

            {error && <Message visible error content={error}/>}

            <Grid columns={2}>
                <Grid.Column>
                    {stepNum > 1 &&
                    <Button onClick={prev} type={"button"} color="yellow" icon labelPosition='left'>
                        <Icon name='left arrow' />
                        Retour
                    </Button>}
                </Grid.Column>
                <Grid.Column textAlign="right">
                    <Button type={"submit"} loading={submitting} disabled={submitting} primary icon labelPosition='right'>
                        Continuer
                        <Icon name='right arrow' />
                    </Button>
                </Grid.Column>
            </Grid>
        </Form>
    );
};

function FormRenderer(props)
{
    const {schema, initialValues, step, onSubmit} = props;

    const registrationStepToFormStep = {
        transform: step => {
            switch (step) {
                case 1:
                    return 1;
                case 2:
                    return 4;
                case 3:
                    return 6;
                case 4:
                    return 7;
                default:
                    return 1;
            }
        },
        reverseTransform: step => {
            switch (step) {
                case 1:
                case 2:
                case 3:
                    return 1;
                case 4:
                case 5:
                    return 2;
                case 6:
                    return 3;
                case 7:
                    return 4;
                case 8:
                    return 4;
            }
        }
    };

    const stepSubmit = (data, step, callback) => {
        return Post(generateUrl('app_front_register_form_submit', {step: registrationStepToFormStep.reverseTransform(step)}), data, 'sf-form')
            .then(data => {
                if (data.ok) {
                    callback(data);
                } else {
                    console.log(data);
                }
            }).catch(exception => {
                console.log(exception);
                alert(exception);
            });
    };

    const steps = [
        {
            header: <p>Prêt à nous rejoindre ?<br/>En vous inscrivant, vous pourrez passer commande en moins de 30 secondes !</p>,
            fields: ['restaurant', 'email'],
            onSubmit: (data, step, callback) => {
                return Get(generateUrl('app_front_check_email', {email: data.email}))
                    .then(response => {
                        if (response.ok) {
                            callback(data);
                        } else {
                            throw new SubmissionError({email: response.message});
                        }
                    })
                ;
            }
        },
        {
            header: <p>Où se trouve votre restaurant ?</p>,
            fields: ['restaurantAddress', 'restaurantCity', 'restaurantZipCode'],
        },
        {
            header: <p>Votre de créneau de livraison souhaité ?</p>,
            fields: ['deliveryHours'],
            hint: <p>Nous indiquerons vos préférences aux grossistes !</p>,
            onSubmit: stepSubmit
        },
        {
            header: <p>Quelles catégories vous intéressent ?</p>,
            fields: ['categories'],
            hint: <p>Nous solliciterons uniquement les grossistes fournissant les produits dont vous avez besoin !</p>,
        },
        {
            header: <p>Chez qui achetez-vous vos produits en</p>,
            fields: ['currentSuppliers'],
            onSubmit: stepSubmit
        },
        {
            header: <p>C'est bientôt terminé.<br/>Pour vous ouvrir des comptes chez nos fournisseurs, il nous faudra votre KBis</p>,
            fields: ['kbisFile'],
            hint: 'Ce document contient les informations nécessaires afin de vous ouvrir des comptes chez les grossistes.',
            onSubmit: stepSubmit
        },
        {
            header: <p>Comment pouvons nous vous joindre ?</p>,
            fields: ['civility', 'firstname', 'lastname', 'telephone'],
        },
        {
            header: <p>C'est tout bon pour nous !<br/>Choisissez un mot de passe afin de vous connecter à foodomarket</p>,
            fields: ['password', 'password_again'],
        }
    ];

    const currentStep = registrationStepToFormStep.transform(step);
    const validator = () => syncValidation(schema, {
        // Step 1
        restaurant: "Le nom de votre restaurant est requis.",
        email: "Votre email est requis afin de vous connecter sur foodomarket et de nous mettre en relation.",
        restaurantAddress: "L'adresse de votre restaurant est requise.",
        restaurantZipCode: "Le code postal de votre restaurant est requis.",
        restaurantCity: "La ville de votre restaurant est requise.",

        // Step 2
        categories: "Veuillez nous indiquer les catégories de produit qui vous intéressent.",
        currentSuppliers: "Merci de nous indiquer les fournisseurs avec qui vous travaillez déjà.",

        // Step 3
        kbisFile: "Votre KBis est requis pour vous mettre en relation avec un grossiste.",

        // Step 4
        civility: "Merci de nous indiquer à qui nous nous adressons",
        firstname: "Merci de nous indiquer votre prénom.",
        lastname: "Merci de nous indiquer votre nom.",
        telephone: "Votre numéro de téléphone est requis afin d'être contacté pour toute nécessité.",
        password: "Votre mot de passe est requis afin de vous connecter à la boutique en ligne foodomarket.",
        password_again: "",
    });

    return (
        <ConnectedStepper currentStep={currentStep} validator={validator} onSubmit={onSubmit} schema={schema} initialValues={initialValues} steps={steps}/>
    );
}

function RegisterForm(props)
{
    const { form, onSubmit } = props;

    return (
        form && <FormRenderer schema={form.schema} step={form.step} initialValues={form.values} onSubmit={onSubmit}/> || <div>Chargement en cours</div>
    );
}

export default RegisterForm;
