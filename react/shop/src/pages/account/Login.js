import React from "react";
import {connect} from "react-redux";
import {Button, Card, Divider, Form, Icon, Message} from "semantic-ui-react";
import {login} from "shop/actions";
import {Link, NavLink} from "react-router-dom";
import {States} from "shop/constants/frontConstants";

class Login extends React.Component {
    state = {
        username: '',
        password: '',
        rememberme: '',
        loading: false
    };

    render() {
        const { loginError } = this.props;

        return (
            <Card id={"login-card"} fluid>
                    <Card.Content>
                        <Card.Header as="h1" textAlign='center'>
                            Déjà client <span style={{color: '#39A934'}}>FoodoMarket</span> ? Connectez-vous pour commander en ligne chez tous les fournisseurs
                        </Card.Header>

                        { loginError &&
                        <Message negative>
                            <Message.Header>Impossible de vous connecter</Message.Header>
                            {loginError === 'BAD_CREDENTIALS' && <p>E-Mail ou mot de passe incorrect</p>
                            || loginError === 'USER_ACCOUNT_IS_DISABLED' && <p>Votre compte n'est pas activé.<br/>Nous vous invitons à nous contacter pour effectuer l'activation.</p>
                            || <p>{loginError}</p>}
                        </Message>
                        }

                        <Form className={"login-form"} onSubmit={this.submit}>
                            <Form.Input label={"Adresse e-mail"} fluid name={"_username"}  onChange={this.setUsername} placeholder="Adresse e-mail" value={this.state.username}/>
                            <Form.Input label={"Mot de passe"} fluid name={"_password"} onChange={this.setPassword} placeholder='Mot de passe' type='password' value={this.state.password}/>
                            <NavLink to={States.ACCOUNT_REQUEST_RESET_PASSWORD} style={{color: '#39A934'}}> Mot de passe oublié ?</NavLink><br/><br/>
                            <Form.Checkbox name="_rememberme" label="Se souvenir de moi" value={this.state.rememberme} onChange={this.setRememberMe} />
                            <Button color='green' style={{margin: '10px'}}loading={this.state.loading} type='submit'>Se connecter</Button>
                        </Form>
                        <br/>
                    </Card.Content>
                <Card.Content extra textAlign={"center"}>
                    <p> Nouveau chez FoodoMarket ?  <Link to={States.ACCOUNT_REGISTER} style={{color: '#39A934'}}>Je créer mon compte !</Link></p>

                </Card.Content>
            </Card>
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.loginError) {
            this.setState({loading: false});
        }
    }

    submit = (e) => {
        e.preventDefault();

        this.setState({loading: true});
        this.props.dispatch(login(this.state.username, this.state.password, this.state.rememberme));
    };

    setUsername = (e) => {
        this.setState({ username: e.target.value });
    };

    setPassword = (e) => {
        this.setState({ password: e.target.value });
    };

    setRememberMe = (e) => {
        this.setState({ rememberme: e.target.value });
    };
}

export default connect(state => (
    {
        loginError : state.frontState.loginError,
        checkPath: state.frontState.checkPath
    }
))(Login);
