import React from "react";
import {connect} from "react-redux";
import {Get} from "shop/utils/fetch";
import {Accordion, Grid, Header, Image, Segment, Table} from "semantic-ui-react";
import {hideLoadingIndicator, showLoadingIndicator} from "shop/actions";
import {generateUrl} from "shop/router";

class OrderHistorySegment extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            orders: [],
        };
    }

    componentDidMount() {
        this.props.dispatch(showLoadingIndicator());

        Get(generateUrl('app_front_orderHistory'))
            .then(data => {
                this.props.dispatch(hideLoadingIndicator());

                if (data.ok) {
                    this.setState({orders: data.orders});
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while fetching order history: ", exception);
            })
        ;
    }

    render() {
        const {orders} = this.state;
        const {isMobileOrTablet} = this.props;

        const list = orders.map((suppliersOrders, mainIndex) => {
            const panels = suppliersOrders.orders.map((order, index) => {
                let pkey = 0;
                const rows = [];

                for (let product of order.products) {
                    rows.push(
                        <Table.Row key={pkey++}>
                            <Table.Cell>
                                <Image bordered={false} src={product.productPicture} width={48} height={48} inline />
                                {product.productName}
                            </Table.Cell>
                            <Table.Cell>{product.quantity}{product.quantityUnitSelected}</Table.Cell>
                            <Table.Cell>{product.productPrice}€</Table.Cell>
                            <Table.Cell>{product.amount}€</Table.Cell>
                        </Table.Row>
                    );

                    if (product.note !== null && product.note !== undefined && product.note.length > 0) {
                        rows.push(
                            <Table.Row key={pkey++}>
                                <Table.Cell colSpan='4'>
                                    <b>Note:</b> {product.note}
                                </Table.Cell>
                            </Table.Row>
                        );
                    }
                }

                return {
                    key: index,
                    title: <Accordion.Title index={index}>{order.time} - {order.supplierName} ({order.nbItems} produits)</Accordion.Title>,
                    content: (
                        <Accordion.Content>
                            <div>
                                <Table className={"history-table"} attached={"top"} unstackable size={isMobileOrTablet ? 'small' : 'large'} padded={!isMobileOrTablet}>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell>Produit</Table.HeaderCell>
                                            <Table.HeaderCell>Quantité</Table.HeaderCell>
                                            <Table.HeaderCell>Prix unitaire</Table.HeaderCell>
                                            <Table.HeaderCell>Montant</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {rows}
                                    </Table.Body>
                                    <Table.Footer>
                                        <Table.Row>
                                            <Table.HeaderCell colSpan='3' textAlign={"right"}>
                                                Total
                                            </Table.HeaderCell>
                                            <Table.HeaderCell>
                                                <b>{order.amount}€</b>
                                            </Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Footer>
                                </Table>
                                {order.cartNote &&
                                <Table attached={"bottom"} basic>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell>
                                                <b>Note sur la commande:</b> {order.cartNote}
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                                }
                            </div>
                        </Accordion.Content>
                    )
                };
            });

            return (
                <Grid className={"shopping-list-supplier-group"} key={mainIndex}>
                    <Grid.Row className={"supplier-name-section"}>
                        <Grid.Column>
                            <Header as={"h4"}>{suppliersOrders.date}</Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row className={"supplier-products-section"}>
                        <Grid.Column>
                            <Accordion styled fluid exclusive={false} panels={panels} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            );
        });

        return (
            <Segment id={"order-history-segment"}>
                <Header className="content-title" as="h1">Historique des commandes</Header>

                {list}
            </Segment>
        );
    }
}

export default connect(state => (
    {
        isMobileOrTablet: state.frontState.isMobileOrTablet
    }
))(OrderHistorySegment)