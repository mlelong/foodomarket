import Loadable from "react-loadable";
import Loading from "shop/components/Widget/Loading";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";

const orderHistoryLoader = Loadable({
    loader: () => import('./OrderHistorySegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.ORDER_HISTORY, true, orderHistoryLoader);
