import Loadable from "react-loadable";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";
import Loading from "shop/components/Widget/Loading";

const productLoader = Loadable({
    loader: () => import('./ProductSegment'),
    loading: Loading,
    delay: 500
});

const productVariantLoader = Loadable({
    loader: () => import('./ProductVariantSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.PRODUCT, true, productLoader);
addRoute(States.PRODUCT_VARIANT, true, productVariantLoader);
