import React from "react";
import {Card, Grid, Header, Icon, Image, List, Message, Segment, Statistic} from "semantic-ui-react";
import {NavLink, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {Get} from "shop/utils/fetch";
import {addToCart, changeQuantity, clearInitialValues, removeFromCart} from "shop/actions";
import {States} from "shop/constants/frontConstants";
import BreadcrumbRenderer from "shop/pages/catalog/BreadcrumbRenderer";
import ShoppingListSupplierProduct from "shop/components/ShoppingListSupplierProduct";
import {generateUrl} from "shop/router";
import ScrollableAnchor, {configureAnchors, goToAnchor} from "react-scrollable-anchor";
import {Helmet} from "react-helmet-async";
import SimpleTelephoneForm from "shop/components/SimpleTelephoneForm";
import ReactPixel from "react-facebook-pixel";
import {getSupplierItem} from "shop/utils/shoppingItems";
import {CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";

class ProductSegment extends React.Component
{
    state = {
        showLoginOrSubscribeMessage: false,
        product: this.props.initialValues.hasOwnProperty('product') ? this.props.initialValues.product : null,
        opacity: {
            min: 0,
            avg: 1,
            max: 0
        }
    };

    load = (slug) => {
        Get(generateUrl('app_front_product', {slug: slug}))
            .then(data => {
                this.trackProduct(data.product);
                this.setState({product: data.product});
            }).catch(exception => {
                console.log(exception);
                this.setState({product: {}});
            })
        ;
    };

    trackProduct = (product) => {
        const productId = product.id;

        ReactPixel.track('ViewContent', {
            content_ids: [`${productId}`],
            content_type: 'product'
        });
    };

    componentDidMount() {
        configureAnchors({offset: -100, scrollDuration: 400});

        if (this.state.product === null) {
            this.load(this.props.match.params.slug);
        } else {
            this.trackProduct(this.state.product);
            this.props.dispatch(clearInitialValues(['product']));
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.match.params.slug != this.props.match.params.slug) {
            this.load(this.props.match.params.slug);
        }

        if (this.state.showLoginOrSubscribeMessage && !prevState.showLoginOrSubscribeMessage) {
            goToAnchor('offres', false);
        }
    }

    addToCart = (product) => {
        const {authToken, dispatch} = this.props;

        if (authToken) {
            dispatch(addToCart(product));
        } else {
            this.setState({showLoginOrSubscribeMessage: true});
        }
    };

    removeFromCart = (product) => {
        const {dispatch} = this.props;

        dispatch(removeFromCart(product));
    };

    changeQuantity = (product, quantity, quantityUnit) => {
        const {dispatch} = this.props;

        dispatch(changeQuantity(product, quantity, quantityUnit));
    };

    contextProduct = (product) => {
        const shoppingCartProduct = getSupplierItem(product.supplierId, product.productId, this.props.shoppingCarts);

        if (shoppingCartProduct) {
            return Object.assign({}, product, {
                addedToList: true,
                quantity: shoppingCartProduct.quantity,
                quantityUnitSelected: shoppingCartProduct.quantityUnitSelected,
                cartItemId: shoppingCartProduct.cartItemId,
                cartId: shoppingCartProduct.cartId
            });
        }

        return product;
    };

    onlegendClicked = ({dataKey}) => {
        const {opacity} = this.state;

        this.setState({opacity: {...opacity, [dataKey]: (opacity[dataKey] > 0 ? 0 : 1) }});
    };

    formatLegend = (value, entry) => {
        const {color, dataKey} = entry;
        const {opacity} = this.state;

        return <span style={{ color, textDecoration: opacity[dataKey] > 0 ? 'none' : 'line-through' }}>{value}</span>;
    };

    render() {
        const {authToken, suppliers, isMobile} = this.props;
        const {showLoginOrSubscribeMessage, product, opacity} = this.state;

        if (product === null) {
            return <Segment id={"product-segment"} loading/>;
        }

        const priceEvolution = product.isBio ? product.priceEvolution.bio : product.priceEvolution.nobio;
        const disabledSuppliers = [];

        // When non logged, this variable does not exist
        // it is only usefull when connected
        if (suppliers) {
            for (let supplier of suppliers) {
                if (supplier['order_enabled'] === false) {
                    disabledSuppliers.push(supplier.supplierName);
                }
            }
        }

        let nbOffers = 0;
        const offers = product.variants.map((variant) => {
            return variant.offers.map((offer, index) => {
                if (offer.orderLineData === null) {
                    return (
                        <List.Item key={index}>
                            <NavLink target="_blank"
                                     to={States.PRODUCT_VARIANT.replace(':id', variant.id).replace(':supplierId', offer.supplierId)}>
                                {offer.supplierName} - {offer.price}€/{offer.saleUnit}
                            </NavLink>
                        </List.Item>
                    );
                } else {
                    const product = this.contextProduct(offer.orderLineData);

                    ++nbOffers;

                    return (
                        <ShoppingListSupplierProduct
                            key={index}
                            data={product}
                            onAddToList={this.addToCart}
                            onRemoveFromList={this.removeFromCart}
                            onQuantityChanged={this.changeQuantity}
                            addedToList={product.addedToList}
                            orderEnabled={!disabledSuppliers.includes(product.productSupplier)}
                        />
                    );
                }
            })
        });

        return (
            <Segment id={"product-segment"} loading={product === null}>
                <Helmet titleTemplate="%s | FoodoMarket.com">
                    <title children={`${product.name} sur FoodoMarket : ${nbOffers} offre${nbOffers > 1 ? 's' : ''}`}/>
                    <meta name="description" content={`Achetez notre ${product.name} sur FoodoMarket, la plateforme des restaurateurs. Prix livraison comprise.`}/>
                </Helmet>

                <Grid>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Header as={'h1'} dividing size={"large"}>{product.name}</Header>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <BreadcrumbRenderer taxons={product.taxons}/>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row centered>
                        <Grid.Column computer={6} tablet={8} mobile={14}>
                            <Card fluid>
                                <Image src={product.picture}/>
                                <div style={{position: 'relative', margin: 0, padding: 0}}>
                                    <i style={{position: 'absolute', right: '5px', top: '-25px', zIndex: 100, color: 'rgba(0,0,0,.4)'}}>Photo non contractuelle</i>
                                </div>
                                {product.hasOwnProperty('prices') && product.prices.kg.avg > 0 &&
                                <Card.Content>
                                    <Card.Meta>Tendance du marché, au kilo</Card.Meta>
                                    <Card.Description>
                                        <Grid centered>
                                            <Grid.Column textAlign={"center"} verticalAlign={"middle"}>
                                                <Statistic.Group widths={3} size={'mini'}>
                                                    <Statistic color={"green"}>
                                                        <Statistic.Value>{product.prices.kg.min}€</Statistic.Value>
                                                        <Statistic.Label>Min</Statistic.Label>
                                                    </Statistic>

                                                    <Statistic>
                                                        <Statistic.Value>{product.prices.kg.avg}€</Statistic.Value>
                                                        <Statistic.Label>Moy</Statistic.Label>
                                                    </Statistic>

                                                    <Statistic color={"red"}>
                                                        <Statistic.Value>{product.prices.kg.max}€</Statistic.Value>
                                                        <Statistic.Label>Max</Statistic.Label>
                                                    </Statistic>
                                                </Statistic.Group>
                                            </Grid.Column>
                                        </Grid>
                                    </Card.Description>
                                </Card.Content>
                                }
                                {product.hasOwnProperty('prices') && product.prices.unit.avg > 0 &&
                                <Card.Content>
                                    <Card.Meta>Tendance du marché, à la pièce</Card.Meta>
                                    <Card.Description>
                                        <Grid centered>
                                            <Grid.Column textAlign={"center"} verticalAlign={"middle"}>
                                                <Statistic.Group widths={3} size={'mini'}>
                                                    <Statistic color={"green"}>
                                                        <Statistic.Value>{product.prices.unit.min}€</Statistic.Value>
                                                        <Statistic.Label>Min</Statistic.Label>
                                                    </Statistic>

                                                    <Statistic>
                                                        <Statistic.Value>{product.prices.unit.avg}€</Statistic.Value>
                                                        <Statistic.Label>Moy</Statistic.Label>
                                                    </Statistic>

                                                    <Statistic color={"red"}>
                                                        <Statistic.Value>{product.prices.unit.max}€</Statistic.Value>
                                                        <Statistic.Label>Max</Statistic.Label>
                                                    </Statistic>
                                                </Statistic.Group>
                                            </Grid.Column>
                                        </Grid>
                                    </Card.Description>
                                </Card.Content>
                                }
                            </Card>
                        </Grid.Column>

                        {/*{authToken &&*/}
                        <Grid.Column computer={10} tablet={8} mobile={16} id={"offer-column"}>
                            {!authToken &&
                            <SimpleTelephoneForm/>
                            }

                            <ScrollableAnchor id={'offres'}>
                                <div></div>
                            </ScrollableAnchor>

                            <Header dividing as={'h3'} style={{marginTop:0}}>Les offres</Header>

                            {showLoginOrSubscribeMessage &&
                                <Message icon warning>
                                    <Icon name={"sign-in"}/>
                                    <Message.Content>
                                        <Message.Header>Inscrivez-vous sur FoodoMarket</Message.Header>
                                        <p>Vous devez vous <NavLink to={States.ACCOUNT_LOGIN}>connecter</NavLink> ou <NavLink to={States.ACCOUNT_REGISTER}>créer un compte</NavLink> pour passer commande.</p>
                                    </Message.Content>
                                </Message>
                            }

                            {product.hasOwnProperty('variants') && product.variants.length > 0 &&
                            <List className={"product-list"}>
                                {offers}
                            </List>
                            ||
                            <p>Aucune offre pour le moment.</p>
                            }
                        </Grid.Column>
                    </Grid.Row>

                    {priceEvolution.length >= 3 &&
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Card fluid>
                                <Card.Content>
                                    <Card.Header>Evolution du marché</Card.Header>
                                    <Card.Description>
                                        <ResponsiveContainer width={'100%'} aspect={isMobile ? 1 : 3}>
                                            <LineChart margin={{top: 15, right: 16, bottom: 5, left: 16}}
                                                       data={priceEvolution}>
                                                <CartesianGrid strokeDasharray="3 3"/>
                                                <XAxis dataKey="date"/>
                                                <YAxis yAxisId="left" domain={['dataMin', 'dataMax']}
                                                       padding={{top: 20, bottom: 20}}
                                                       unit={`€/${product.priceEvolution.unit}`}/>
                                                <Tooltip formatter={(value, name, props) => {
                                                    return value.toFixed(2) + '€';
                                                }}/>
                                                <Legend formatter={this.formatLegend} onClick={this.onlegendClicked}/>
                                                <Line yAxisId="left" dot={false}
                                                      name={`Prix ${product.priceEvolution.unit} min`}
                                                      strokeOpacity={opacity.min} fillOpacity={opacity.min}
                                                      type='monotone' dataKey='min' stroke='#21ba45'/>
                                                <Line yAxisId="left" dot={false}
                                                      name={`Prix ${product.priceEvolution.unit} moy`}
                                                      strokeOpacity={opacity.avg} fillOpacity={opacity.avg}
                                                      type='monotone' dataKey='avg' stroke='#1b1c1d'/>
                                                <Line yAxisId="left" dot={false}
                                                      name={`Prix ${product.priceEvolution.unit} max`}
                                                      strokeOpacity={opacity.max} fillOpacity={opacity.max}
                                                      type='monotone' dataKey='max' stroke='#db2828'/>
                                            </LineChart>
                                        </ResponsiveContainer>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                    </Grid.Row>
                    }
                </Grid>
            </Segment>
        );
    }
}

export default withRouter(connect(state => ({
    isMobile: state.frontState.isMobile,
    authToken: state.frontState.authToken,
    shoppingCarts: state.shoppingCarts,
    initialValues: state.frontState.initialValues,
    suppliers: state.frontState.suppliers
}))(ProductSegment));