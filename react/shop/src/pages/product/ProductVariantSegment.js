import React from "react";
import {withRouter} from "react-router-dom";
import {Get} from "shop/utils/fetch";
import {Card, Grid, Header, Icon, Segment, Table} from "semantic-ui-react";
import {CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import QuantityWidget from "shop/components/QuantityWidget";
import {connect} from "react-redux";
import {addToCart, changeQuantity, clearInitialValues, removeFromCart} from "shop/actions";
import BreadcrumbRenderer from "shop/pages/catalog/BreadcrumbRenderer";
import {generateUrl} from "shop/router";
import {getSupplierItem} from "shop/utils/shoppingItems";

class ProductVariantSegment extends React.Component
{
    state = {
        ok: true,
        data: this.props.initialValues.hasOwnProperty('variant') ? this.props.initialValues.variant : {
            productName: '',
            supplierName: '',
            picture: '',
            options: [],
            priceEvolution: [],
            srv: {
                productId: 0,
                supplierId: 0
            }
        },

        quantity: 0,
        unit: '',
        loading: false,
        errorQuantity: true,
    };

    onAddToCart = () => {
        const product = this.state.data.srv;
        const {quantity, unit} = this.state;

        if (quantity > 0) {
            this.setState({loading: true});
            this.props.dispatch(addToCart(Object.assign({}, product, {quantity, quantityUnitSelected: unit})));
        }
    };

    onRemoveFromCart = () => {
        const product = this.state.data.srv;

        this.setState({loading: true});

        this.props.dispatch(removeFromCart(product));
    };

    onQuantityChanged = (quantity, unit) => {
        const product = this.state.data.srv;

        if (quantity > 0) {
            this.setState({errorQuantity: false, quantity, unit});

            this.props.dispatch(changeQuantity(product, quantity, unit));
        } else {
            this.setState({errorQuantity: true, quantity, unit});
        }
    };

    componentDidMount() {
        const {match, dispatch} = this.props;

        if (this.state.data.productName === '') {
            Get(generateUrl('app_front_product_variant', {
                id: match.params.id,
                supplierId: match.params.supplierId
            })).then((data) => {
                if (data.ok) {
                    this.setState({ok: data.ok, data: data.data, unit: data.data.srv.quantityUnitSelected});
                } else {
                    this.setState({ok: data.ok});
                }
            });
        } else {
            dispatch(clearInitialValues(['variant']));
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.shoppingCarts !== this.props.shoppingCarts) {
            const {data} = this.state;

            const shoppingCartProduct = getSupplierItem(data.srv.supplierId, data.srv.productId, this.props.shoppingCarts);

            if (shoppingCartProduct) {
                data.srv.cartId = p.cartId;
                data.srv.cartItemId = p.cartItemId;
                data.srv.quantity = p.quantity;
                data.srv.quantityUnitSelected = p.quantityUnitSelected;
            } else {
                data.srv.quantity = 0;
                data.srv.cartId = undefined;
                data.srv.cartItemId = undefined;
            }

            this.setState({loading: false, data: data});
        }
    }

    static getDerivedStateFromProps(props, state) {
        const {data} = state;
        const shoppingCartProduct = getSupplierItem(data.srv.supplierId, data.srv.productId, props.shoppingCarts);

        if (shoppingCartProduct) {
            data.srv.cartId = p.cartId;
            data.srv.cartItemId = p.cartItemId;
            data.srv.quantity = p.quantity;
            data.srv.quantityUnitSelected = p.quantityUnitSelected;

            return {data: data, errorQuantity: p.quantity <= 0, quantity: p.quantity};
        }

        return null;
    }

    render() {
        const {isMobile} = this.props;
        const {data,errorQuantity,quantity} = this.state;
        const addedToCart = data.srv.cartItemId !== undefined;
        const addToCartClass = errorQuantity || quantity == 0 ? "order-item-order has-error" : "order-item-order";
        const className = addedToCart ? "order-item-row added" : "order-item-row";

        return (
            <Segment>
                <Grid>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Header dividing size={"large"}>{data.productName}</Header>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <BreadcrumbRenderer taxons={data.taxons}/>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Card.Group itemsPerRow={2} stackable>
                                <Card color="grey" image={data.picture} />

                                <Card color="brown" fluid>
                                    <Card.Content>
                                        <Card.Header>Caractéristiques</Card.Header>
                                        <Card.Meta>Options</Card.Meta>
                                    </Card.Content>
                                    <Card.Content>
                                        <Card.Description>
                                            <Table definition unstackable>
                                                <Table.Header>
                                                    <Table.Row>
                                                        <Table.HeaderCell/>
                                                        <Table.HeaderCell>Valeur</Table.HeaderCell>
                                                    </Table.Row>
                                                </Table.Header>
                                                <Table.Body>
                                                    {data.options.map((option, index) =>
                                                        <Table.Row key={index}>
                                                            <Table.Cell>{option.name}</Table.Cell>
                                                            <Table.Cell>{option.value}</Table.Cell>
                                                        </Table.Row>
                                                    )}
                                                </Table.Body>
                                            </Table>
                                        </Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Grid className={"order-line"}>
                                            {data.orderable &&
                                            <Grid.Row className={className}>
                                                <Grid.Column mobile={9} tablet={8} computer={8}
                                                             className={"order-item-quantity"}>
                                                    <div>
                                                        <QuantityWidget hasError={errorQuantity} product={data.srv}
                                                                        onQuantityChanged={this.onQuantityChanged}/>
                                                    </div>
                                                </Grid.Column>
                                                <Grid.Column mobile={7} tablet={8} computer={8}
                                                             className={addToCartClass}>
                                                    {addedToCart &&
                                                    <div onClick={this.onRemoveFromCart}>
                                                        <Icon loading={this.state.loading} name={"checkmark"}/><br/>
                                                        Ajouté
                                                    </div> ||
                                                    <div onClick={this.onAddToCart}>
                                                        <Icon loading={this.state.loading} name={"plus cart"}/><br/>
                                                        Commander
                                                    </div>
                                                    }
                                                </Grid.Column>
                                            </Grid.Row>
                                            ||
                                            <Grid.Row columns={1}>
                                                <Grid.Column>
                                                    Produit non commandable
                                                </Grid.Column>
                                            </Grid.Row>
                                            }
                                        </Grid>

                                    </Card.Content>
                                </Card>
                            </Card.Group>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Card color={"red"} fluid>
                                <Card.Content>
                                    <Card.Header>Evolution du prix pour le fournisseur {data.supplierName}</Card.Header>
                                    <Card.Description>
                                        <ResponsiveContainer aspect={isMobile ? 2 : 6}>
                                            <LineChart data={data.priceEvolution}>
                                                <CartesianGrid strokeDasharray="3 3"/>
                                                <XAxis dataKey="date"/>
                                                <YAxis yAxisId="left" dataKey="kg" unit={"€/kg"}/>
                                                <YAxis yAxisId="right" dataKey="pc" unit={"€/pc"} orientation="right"/>
                                                <Tooltip formatter={(value, name, props) => { return value.toFixed(2) + '€'; }}/>
                                                <Line yAxisId="left" type='monotone' dataKey='kg' stroke='#0294ff' fill='#0294ff' />
                                                <Line yAxisId="right" type='monotone' dataKey='pc' stroke='#0294ff' fill='#0294ff' strokeDasharray="3 4 5 2" />
                                            </LineChart>
                                        </ResponsiveContainer>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default withRouter(connect(state => ({
    shoppingCarts: state.shoppingCarts,
    isMobile: state.frontState.isMobile,
    initialValues: state.frontState.initialValues
}))(ProductVariantSegment));