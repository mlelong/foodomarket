import Loadable from "react-loadable";
import Loading from "shop/components/Widget/Loading";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";

const documentsLoader = Loadable({
    loader: () => import('./DocumentsSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.SEPA, true, documentsLoader);
