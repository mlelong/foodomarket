import React, {useState} from "react";
import {Message, Modal} from "semantic-ui-react";
import PropTypes from "prop-types";
import {Get, Put} from "shop/utils/fetch";
import {generateUrl} from "shop/router";
import {SepaRequirements} from "shop/pages/account/MyAccountSegment";

const canUseDOM = () => {
    return !!(typeof window !== 'undefined' && window.document && window.document.createElement);
};

const onMessage = (messageData) => {
    const data = JSON.parse(messageData.data);

    if (data.action === 'cal_callback') {
        Put(generateUrl('app_front_sepa_update_signature', {contractId: data.contract_id, signed: data.signed === true ? 1 : 0}))
            .then(data => {
                if (data.ok) {
                    document.location.reload();
                } else {
                    console.log(data);
                }
            })
        ;
    }
};

const SigningModal = props => {
    const [link, setLink] = useState(null);
    const [error, setError] = useState(null);
    const [fillRequirements, setFillRequirements] = useState(null);

    const checkRequirementsAndPrepareContract = () => {
        Get(generateUrl('app_front_sepa_check_requirements', {supplierId: props.supplier.id})).then(data => {
            if (data.isSigned) {
                // Nothing to do, contract has already been signed, let's opt for direct debit directly !
                document.location = generateUrl('app_front_sepa_switch_direct_debit', {supplierId: props.supplier.id});
            } else if (data.requirements) {
                Get(generateUrl('app_front_sepa_prepare_contract', {id: props.supplier.id})).then(data => {
                    if (data.ok) {
                        setLink(data.link);
                    } else {
                        setError(data.message);
                    }
                });
            } else {
                setFillRequirements(true);
            }
        });
    };

    const onOpen = () => {
        if (canUseDOM()) {
            window.addEventListener('message', onMessage, false);
        }

        checkRequirementsAndPrepareContract();
    };

    const onClose = () => {
        if (canUseDOM()) {
            window.removeEventListener('message', onMessage);
        }

        setLink(null);
        setError(null);

        document.location.reload();
    };

    return (
        <Modal
            closeOnDimmerClick={true}
            closeOnDocumentClick={true}
            closeIcon={true}
            size='large'
            trigger={props.trigger}
            onOpen={onOpen}
            onClose={onClose}
        >
            <Modal.Header>Mandat SEPA de {props.supplier.name}</Modal.Header>
            <Modal.Content>
                {link !== null &&
                    <iframe
                        src={link}
                        width={'100%'}
                        height={'100%'}
                        style={{width: '100%', height: '100%', minHeight: '80vh'}}
                    />
                }

                {error !== null &&
                    <Message negative>
                        <Message.Content>
                            <Message.Header>Une erreur est survenue</Message.Header>
                            <p>{error}</p>
                        </Message.Content>
                    </Message>
                }

                {link === null && error === null && fillRequirements === null &&
                    <p>Préparation du mandat en cours. Veuillez patienter quelques instants...</p>
                }

                {fillRequirements &&
                    <SepaRequirements postSubmit={() => {
                        setFillRequirements(null);

                        checkRequirementsAndPrepareContract();
                    }} headerMessage={
                        <Message success>
                            <Message.Content>
                                <Message.Header>
                                    Afin de signer le mandat de prélèvement SEPA, merci de renseigner votre Iban et de joindre votre RIB dans le formulaire ci dessous.<br/>
                                    Pensez aussi à vérifier que les autres informations (adresse, numéro de téléphone) sont valides.
                                </Message.Header>
                            </Message.Content>
                        </Message>}
                    />
                }
            </Modal.Content>
        </Modal>
    );
};

SigningModal.propTypes = {
    trigger: PropTypes.node.isRequired,
    supplier: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })
};

export default SigningModal;
