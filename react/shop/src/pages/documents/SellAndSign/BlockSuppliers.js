import React, {useEffect, useState} from "react";
import {Get} from "shop/utils/fetch";
import {generateUrl} from "shop/router";
import {List, Message} from "semantic-ui-react";

const BlockSuppliers = props => {
    const [waitingSuppliers, setWaitingSuppliers] = useState(null);

    useEffect(() => {
        if (waitingSuppliers === null) {
            Get(generateUrl('app_front_sepa_waiting_suppliers')).then(data => {
                setWaitingSuppliers(data);
            });
        }
    });

    return (
        waitingSuppliers !== null && waitingSuppliers.length > 0 ?
        <Message warning>
            <Message.Content>
                <Message.Header>Nous attendons toujours les mandats de prélèvement SEPA des fournisseurs suivants:</Message.Header>

                <List bulleted>
                    {waitingSuppliers.map((supplier, key) => <List.Item key={key}>{supplier.name}</List.Item>)}
                </List>
            </Message.Content>
        </Message>
        : <React.Fragment></React.Fragment>
    );
};

export default BlockSuppliers;