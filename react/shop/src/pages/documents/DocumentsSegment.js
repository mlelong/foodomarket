import React, {useEffect, useState} from "react";
import {Divider, Header, List, Segment, Table} from "semantic-ui-react";
import BlockSuppliers from "./SellAndSign/BlockSuppliers";
import {connect} from "react-redux";
import SigningModal from "./SellAndSign/SigningModal";
import {generateUrl} from "shop/router";
import {Get} from "shop/utils/fetch";
import SwitchPaymentMethodConfirmModal from "shop/pages/documents/SwitchPaymentMethodConfirmModal";

const DocumentsSegment = props => {
    const [supplierAccountLogs, setSupplierAccountLogs] = useState(props.supplierAccountLogs);
    const [waitingSepas, setWaitingSepas] = useState(props.waitingSepas);
    const [supplierLogs, setSupplierLogs] = useState(props.supplierLogs);

    useEffect(() => {
        if (!supplierAccountLogs) {
            Get(generateUrl('app_front_sepa_supplier_account_logs')).then(data => {
                setSupplierAccountLogs(data['supplier-account-logs']);
                setWaitingSepas(data['waiting-sepas']);
                setSupplierLogs(data['supplier-logs']);
            });
        }
    });

    const statuses = ['ACCOUNT_OPENED', 'ORDERING', 'WAITING', 'ACCOUNT_BLOCKED'];

    return (
        <Segment id={"documents-segment"}>
            <Header dividing as={'h1'}>Mes fournisseurs</Header>

            <BlockSuppliers/>

            {waitingSepas && waitingSepas.length > 0 &&
            <Table celled columns={3}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Fournisseur</Table.HeaderCell>
                        <Table.HeaderCell>Statut fournisseur</Table.HeaderCell>
                        <Table.HeaderCell>SEPA</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {waitingSepas.map((supplier, key) =>
                        <Table.Row key={key} positive={supplier.class === 'positive'}
                                   warning={supplier.class === 'warning'} negative={supplier.class === 'negative'}>
                            <Table.Cell>{supplier.name}</Table.Cell>
                            <Table.Cell>{supplier.status}</Table.Cell>
                            <Table.Cell>
                                {supplier.signed
                                && <React.Fragment>
                                    <a target="_blank" href={generateUrl('app_front_sepa_retrieve', {
                                        contractId: supplier.contractId,
                                        mode: 'view'
                                    })}>{supplier.closed && 'Voir' || 'Voir le brouillon'}</a>
                                    &nbsp;/&nbsp;
                                    <a target="_blank" href={generateUrl('app_front_sepa_retrieve', {
                                        contractId: supplier.contractId,
                                        mode: 'download'
                                    })}>{supplier.closed && 'Télécharger' || 'Télécharger le brouillon'}</a>
                                </React.Fragment>
                                || <SigningModal trigger={<a href='#'>Signer le mandat</a>} supplier={supplier}/>
                                }
                            </Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
            }

            {supplierLogs && statuses.map((status, index) => {
                if (!supplierLogs.hasOwnProperty(status)) {
                    return <React.Fragment key={index}/>
                }

                let displayStatus;

                switch (status) {
                    case 'WAITING':
                        displayStatus = 'Comptes en attente';
                        break ;
                    case 'ACCOUNT_OPENED':
                        displayStatus = 'Comptes ouverts';
                        break ;
                    case 'ACCOUNT_BLOCKED':
                        displayStatus = 'Comptes non ouverts';
                        break ;
                    case 'ORDERING':
                        displayStatus = 'Comptes ouverts CB';
                        break ;
                }

                return (
                    <React.Fragment key={index}>
                        <Divider horizontal style={{marginTop: '3em'}}>{displayStatus}</Divider>

                        <Table celled columns={5}>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Fournisseur</Table.HeaderCell>
                                    <Table.HeaderCell>Statut</Table.HeaderCell>
                                    <Table.HeaderCell>Modes de paiement</Table.HeaderCell>
                                    <Table.HeaderCell>SEPA</Table.HeaderCell>
                                    <Table.HeaderCell>Statut du mandat</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {supplierLogs[status].map(supplierLog =>
                                    <Table.Row
                                        key={supplierLog.id}
                                        positive={supplierLog.class === 'positive'}
                                        warning={supplierLog.class === 'warning'}
                                        negative={supplierLog.class === 'negative'}
                                    >
                                        <Table.Cell>{supplierLog.name}</Table.Cell>
                                        <Table.Cell>{supplierLog.status}</Table.Cell>
                                        <Table.Cell>
                                            <List as='ul'>
                                                {supplierLog.paymentModes.map((paymentMode, key) =>
                                                    <List.Item as='li' key={key}>{paymentMode}</List.Item>
                                                )}
                                            </List>
                                        </Table.Cell>
                                        <Table.Cell>
                                            {supplierLog.signed && supplierLog.contractId
                                            && <React.Fragment>
                                                <a target="_blank" href={generateUrl('app_front_sepa_retrieve', {
                                                    contractId: supplierLog.contractId,
                                                    mode: 'view'
                                                })}>{supplierLog.closed && 'Voir' || 'Voir le brouillon'}</a>
                                                &nbsp;/&nbsp;
                                                <a target="_blank" href={generateUrl('app_front_sepa_retrieve', {
                                                    contractId: supplierLog.contractId,
                                                    mode: 'download'
                                                })}>{supplierLog.closed && 'Télécharger' || 'Télécharger le brouillon'}</a>
                                            </React.Fragment>
                                            }
                                            {(status === 'WAITING' || status === 'ORDERING') && supplierLog.isSepa && <SwitchPaymentMethodConfirmModal
                                                trigger={<a style={{display: 'block'}} href='#'>Opter pour le prélèvement automatique</a>}
                                                supplier={supplierLog}
                                                paymentMode={"Prélèvement automatique"}
                                            />
                                            }
                                            {status === 'ACCOUNT_OPENED' && supplierLog.isCB && <SwitchPaymentMethodConfirmModal
                                                supplier={supplierLog}
                                                trigger={<a style={{display: 'block'}} href="#">Opter pour le paiement CB</a>}
                                                paymentMode={"Carte bancaire"}
                                            />}
                                        </Table.Cell>
                                        <Table.Cell
                                            positive={supplierLog.sellsignClass === 'positive'}
                                            warning={supplierLog.sellsignClass === 'warning'}
                                            negative={supplierLog.sellsignClass === 'negative'}
                                        >
                                            {supplierLog.sellsignStatus}
                                        </Table.Cell>
                                    </Table.Row>
                                )}
                            </Table.Body>
                        </Table>
                    </React.Fragment>
                );
            })}
        </Segment>
    );
};

export default connect(state => ({
    supplierAccountLogs: state.frontState.initialValues['supplier-account-logs'],
    waitingSepas: state.frontState.initialValues['waiting-sepas'],
    supplierLogs: state.frontState.initialValues['supplier-logs'],
}))(DocumentsSegment);
