import React from 'react';
import {Button, Icon, Modal} from "semantic-ui-react";
import {generateUrl} from "shop/router";
import SigningModal from "shop/pages/documents/SellAndSign/SigningModal";

const SwitchPaymentMethodConfirmModal = (props) => {
    const {paymentMode, trigger, supplier} = props;
    let url, useSigninModal;

    switch (paymentMode) {
        case 'Carte bancaire':
            url = generateUrl('app_front_sepa_switch_credit_card', {supplierId: supplier.id});
            useSigninModal = false;
            break ;
        case 'Prélèvement automatique':
            useSigninModal = true;
            break ;
    }

    const actions = [
        {key: 'no', basic: false, color: 'red', icon: 'remove', content: 'Non'},
    ];

    if (useSigninModal) {
        // noinspection JSCheckFunctionSignatures
        actions.push(
            {key: 'sigmodal', basic: false, as: SigningModal, trigger: <Button color='green'><Icon name='checkmark'/> Oui</Button>, supplier: supplier}
        );
    } else {
        actions.push(
            {key: 'switch', basic: false, color: 'green', as: 'a', href: url, icon: 'checkmark', content: 'Oui'}
        );
    }

    return (
        <Modal
            closeOnDimmerClick={true}
            closeOnDocumentClick={true}
            closeIcon={true}
            trigger={trigger}
            actions={actions}
            content={`Confirmer le changement de moyen de paiement en ${paymentMode} ?`}
        />
    );
};

export default SwitchPaymentMethodConfirmModal;
