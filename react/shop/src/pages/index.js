import "./home";
import "./dashboard";
import "./order";
import "./shopping-list";
import "./static";
import "./catalog";
import "./account";
import "./litigations";
import "./product";
import "./invoice";
import "./order-history";
import "./cart";
import "./supplier";
import "./documents";
import "./exception";

import {routes} from "shop/router/router";

export default routes;
