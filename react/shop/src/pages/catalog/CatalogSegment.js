import React from "react";
import {connect} from "react-redux";
import {Button, Card, Container, Grid, Header, Message, Modal, Segment} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import {Link, withRouter} from "react-router-dom";
import BreadcrumbRenderer from "./BreadcrumbRenderer";
import Filters from "shop/components/Filters";
import {generateUrl} from "shop/router";
import {clearInitialValues} from "shop/actions";
import {Helmet} from "react-helmet-async";
import ReactPixel from "react-facebook-pixel";
import ProductCard from "./ProductCard";
import {States} from "shop/constants/frontConstants";
import Ellipsizer from "shop/components/Widget/Ellipsizer";
import CatalogBannerIncitement from "./CatalogBannerIncitement/CatalogBannerIncitement"

class CatalogSegment extends React.Component {

    state = {
        loading: false,
        products: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.products : [],
        filters: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.filters : [],
        filteredProducts: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.products : [],
        breadcrumb: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.taxons : [],
        filterModalOpen: false,
        taxon: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.taxon : '',
        childTaxons: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.childTaxons : [],
    };

    componentDidMount() {
        const {match, dispatch} = this.props;

        if (this.state.products.length === 0) {
            this.loadProducts(match.params.slug);
        } else {
            this.trackProducts(this.state.products);
            dispatch(clearInitialValues(['catalog']));
        }
    }

    trackProducts = (products) => {
        const productIds = products.map(product => `${product.id}`);

        if (productIds.length > 0) {
            ReactPixel.track('ViewContent', {
                content_ids: productIds,
                content_type: 'product'
            });
        }
    };

    loadProducts = (slug) => {
        this.setState({loading: true});

        return Get(generateUrl('app_front_products_by_taxon', {slug: slug}))
            .then(data => {
                this.trackProducts(data.products);
                this.setState({
                    products: data.products,
                    filters: data.filters,
                    breadcrumb: data.taxons,
                    taxon: data.taxon,
                    childTaxons: data.childTaxons,
                    loading: false
                });
            }).catch(ignored => {
                this.setState({loading: false});
            })
            ;
    };

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.slug != this.props.match.params.slug) {
            this.loadProducts(this.props.match.params.slug);
        }
    }

    render() {
        const {loading, breadcrumb, filters, products, filteredProducts, filterModalOpen, taxon: mainTaxon, childTaxons} = this.state;
        // Récupération du token de l'authentification
        const {authToken, isAppDev} = this.props;

        const filteringCard = <Filters itemName={"produit"} itemNamePluralized={"produits"} filters={filters}
                                       results={products} onResultsFiltered={(filteredProducts) => {
            this.setState({filteredProducts});
        }}/>;

        let taxon = null;

        if (breadcrumb.length > 0) {
            taxon = breadcrumb[breadcrumb.length - 1].name;
        }

        const children = <div>
            {childTaxons.map((child, key) =>
                <Header as={'h4'} key={key} className="catalog-head-child">
                    <Link
                        to={States.BROWSE_CATALOG.replace(':slug', encodeURIComponent(child.slug))}>{child.name}</Link>
                </Header>
            )}
        </div>;

        // Slice le tableau filteredProducts en deux parties
        const firstFilteredProdutcs = filteredProducts.slice(0, 8);
        const secondFilteredProducts = filteredProducts.slice(8);

        return (
            <Segment id="catalog-segment" loading={loading}>
                {taxon &&
                <Helmet titleTemplate="%s | FoodoMarket.com">
                    <title children={`Notre offre ${taxon} au meilleur prix, livraison comprise`}/>
                    <meta name="description"
                          content={`Retrouvez un vaste choix de ${taxon} sur FoodoMarket, la plateforme des restaurateurs`}/>
                </Helmet>
                }

                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Header as={'h1'}>{mainTaxon}</Header>

                            <Ellipsizer key={mainTaxon} height={50}>
                                {children}
                            </Ellipsizer>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column computer={16} tablet={10} mobile={12}>
                            <BreadcrumbRenderer taxons={breadcrumb}/>
                        </Grid.Column>
                        <Grid.Column only={"tablet mobile"} tablet={6} mobile={4} textAlign={"right"}
                                     verticalAlign={'middle'}>
                            <Modal trigger={<a id="filter-trigger" href="#" onClick={(e) => {
                                e.preventDefault();
                                this.setState({filterModalOpen: true})
                            }}>Filtres</a>} closeIcon closeOnDocumentClick closeOnDimmerClick open={filterModalOpen}
                                   onClose={() => this.setState({filterModalOpen: false})}>
                                <Modal.Header>Filtrer les produits</Modal.Header>
                                <Modal.Content scrolling>
                                    {filteringCard}
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button primary fluid onClick={() => this.setState({filterModalOpen: false})}>
                                        Appliquer
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column computer={4} only={"computer"}>
                            {filteringCard}
                        </Grid.Column>

                        <Grid.Column computer={12} mobile={16}>

                            {filteredProducts.length > 0 &&
                            <>
                                <Card.Group itemsPerRow={4} doubling>
                                    {firstFilteredProdutcs.map(product =>
                                        <ProductCard key={product.id} product={product}/>
                                    )}
                                </Card.Group>
                                {!authToken && !isAppDev &&
                                <CatalogBannerIncitement/>
                                }


                                <Card.Group itemsPerRow={4} doubling>
                                    {secondFilteredProducts.map(product =>
                                        <ProductCard key={product.id} product={product}/>
                                    )}
                                </Card.Group>
                            </>
                            || !loading &&
                            <Message warning>
                                <Message.Header>Pas de résultat</Message.Header>
                                <p>Nous vous invitons à naviguer dans les autres catégories de produit.</p>
                            </Message>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default withRouter(connect(state => ({
    initialValues: state.frontState.initialValues,
    authToken: state.frontState.authToken,
    isMobile: state.frontState.isMobile,
    isAppDev: state.frontState.isAppDev
}))(CatalogSegment));
