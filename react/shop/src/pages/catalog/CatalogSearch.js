import React from "react";
import {Input, Label} from "semantic-ui-react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import ReactGA from "react-ga";

class CatalogSearch extends React.Component
{
    state = {
        terms: ''
    };

    onKeyDown = (event) => {
        if (event.key == 'Enter') {
            this.props.history.push(States.SEARCH_CATALOG.replace(':terms', encodeURIComponent(this.state.terms.length > 0 ? this.state.terms : ' ')));

            ReactGA.event({
                category: 'Recherche',
                action: 'Recherche de produit',
                label: this.state.terms
            }, ['analytics']);
        }
    };

    onChange = (event, {value}) => {
        this.setState({terms: value});
    };

    onIconClicked = () => {
        this.onKeyDown({key:'Enter'},null);
    };

    render() {
        return (
            <div id={"catalog-search-input"}>
                <Input
                    placeholder={"Rechercher un produit dans le catalogue..."}
                    fluid
                    action={<Label icon={"search"} onClick={this.onIconClicked}/>}
                    onKeyDown={this.onKeyDown}
                    onChange={this.onChange}
                />
            </div>
        );
    }
}

export default withRouter(connect()(CatalogSearch));