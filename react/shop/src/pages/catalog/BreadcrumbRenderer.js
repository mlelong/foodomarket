import React from "react";
import PropTypes from "prop-types";
import {States} from "shop/constants/frontConstants";
import {Link} from "react-router-dom";
import {Breadcrumb, Icon} from "semantic-ui-react";
import {connect} from "react-redux";

const BreadcrumbRenderer = props =>
{
    const {taxons, isMobile} = props;

    return (
        <Breadcrumb as={isMobile ? 'div' : 'ul'} id={isMobile ? "breadcrumb-mobile" : "breadcrumb"}>
            {taxons.map((taxon, i) => {
                return (
                    <React.Fragment key={i}>
                        {i == 0 &&
                        <Breadcrumb.Section as={isMobile ? 'div' : 'li'} active={i == taxons.length - 1}>
                            <a><Icon name='home'/></a>
                        </Breadcrumb.Section>
                        ||
                        <Breadcrumb.Section as={isMobile ? 'div' : 'li'} active={i == taxons.length - 1}>
                            <Link className={i == taxons.length - 1 ? 'active' : ''} to={States.BROWSE_CATALOG.replace(':slug', encodeURIComponent(taxons[i].slug))}>{taxons[i].name}</Link>
                        </Breadcrumb.Section>
                        }
                        {isMobile && i != taxons.length - 1 && <Breadcrumb.Divider icon='right angle'/>}
                    </React.Fragment>
                );
            })}
        </Breadcrumb>
    );
};

BreadcrumbRenderer.propTypes = {
    taxons: PropTypes.array
};

BreadcrumbRenderer.defaultProps = {
    taxons: [{id: 1, name: 'Catalogue', slug: 'category'}]
};

export default connect(state => ({
    isMobile: state.frontState.isMobile
}))(BreadcrumbRenderer);
