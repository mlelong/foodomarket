import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {Card, Icon, Image, Label} from "semantic-ui-react";
import {connect} from "react-redux";

const ProductCard = props => {
    const {product, isAppDev, authToken, suppliers} = props;

    const chooseStartingPrice = () => {
        if (!authToken) {
            return product.startingPriceOnline;
        }

        const supplier = suppliers.filter(s => s.supplierId === product.bestSupplier);

        if (supplier.length > 0) {
            if (supplier[0].payment_method === 'credit_card_online') {
                return product.startingPriceOnline;
            }
        }

        return product.startingPrice;
    };

    let startingPrice = chooseStartingPrice();
    //Extracting the price per kilo from prodcut starting price
    const regexParenthesis = /\(([^)]+)\)/;
    const matches = regexParenthesis.exec(startingPrice);

    if (matches) {
        startingPrice = matches[1].replace('soit', '');
    }

    return (
        <Card className="product-card" style={{minHeight: product.promo ? '445px' : '400px'}} as={Link} to={States.PRODUCT.replace(':slug', encodeURIComponent(product.slug))}>
            {product.promo &&
            <Label color='red' ribbon>
                Promotion
            </Label>
            }
            <Image fluid src={product.picture}/>
            <Card.Content>
                <Card.Header>{product.name}{isAppDev && ` (${product.id})`}</Card.Header>
                <Card.Meta>
                    {product.variants.length} offre{product.variants.length > 1 && 's'}<br/>
                    {startingPrice.length > 0 && <span className={"starting-price-text"}>à partir de <span className={"starting-price-price"}>{startingPrice}</span></span>}
                </Card.Meta>
            </Card.Content>
            <Card.Content extra>
                <Icon name={"truck"}/>
                {product.suppliers.length} fournisseur{product.suppliers.length > 1 && 's'}
            </Card.Content>
        </Card>
    );
};

ProductCard.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number.isRequired,
        promo: PropTypes.bool.isRequired,
        picture: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        startingPrice: PropTypes.string.isRequired,
        startingPriceOnline: PropTypes.string.isRequired,
        variants: PropTypes.array.isRequired,
        suppliers: PropTypes.array.isRequired,
        bestSupplier: PropTypes.number.isRequired
    })
};

export default connect(state => ({
    isAppDev: state.frontState.isAppDev,
    authToken: state.frontState.authToken,
    suppliers: state.frontState.suppliers
}))(ProductCard);
