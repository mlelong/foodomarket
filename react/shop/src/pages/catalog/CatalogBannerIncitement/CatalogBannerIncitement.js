import React from "react";
import {Card, Icon, List, Button, Grid, Container} from "semantic-ui-react";
import {States} from "shop/constants/frontConstants";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";

require("./CatalogBannerIncitement.scss");

// Ce composant a besoin d'être montré uniquement quand on est sur mobile
// Utiliser isMobile, si c'est en false alors montrer la list
const ListAdvantage = () => (
    <List>
        <List.Item>Des prix bas garantis</List.Item>
        <List.Item>Commande en un clic</List.Item>
        <List.Item>Sélection de fournisseur de qualité</List.Item>
        <List.Item>Une gestion de factures intégrée</List.Item>
        <List.Item>Des milliers de références</List.Item>
    </List>
);

const CatalogBannerIncitement = (props) => {
    const {isMobile, registrationToComplete} = props;

    return (
        <Grid.Column className={"grid-banner"}
                     widescreen={10}
                     largeScreen={14}
                     tablet={14}
                     mobile={16}>
            <Card fluid className={"card-banner"}>
                <Card.Header className={"header-banner"} textAlign="center">
                    Envie de passer commande <br/>
                    et de profiter de tous les avantages FoodoMarket ?
                </Card.Header>

                <Card.Description textAlign="center">
                    {registrationToComplete ? (
                        <Grid.Column>
                            <Button size="large" color="green">
                                <Icon name="pencil alternate"/>
                                <Link className={"link_banner"} to={States.ACCOUNT_REGISTER}>Completez votre inscription maintenant !</Link>
                            </Button>
                        </Grid.Column>
                    ) : (
                        <>
                            <Container className={"list-banner"} textAlign='center'>
                                {!isMobile &&
                                <ListAdvantage/>
                                }
                                <p> Créez votre compte FoodoMarket maintenant ! </p><br/>
                            </Container>

                            <Grid.Column>
                                <Button size="large" color="green">
                                    <Icon name="pencil alternate"/>
                                    <Link className={"link_banner"} to={States.ACCOUNT_REGISTER}>Créer mon compte</Link>
                                </Button>
                                <p className={"p-banner"}><br/> Déjà client ?
                                    <a className={"a-banner"} href="/shop/connexion"> Connectez-vous ! </a>
                                </p>
                            </Grid.Column>
                        </>
                    )}

                </Card.Description>
            </Card>
        </Grid.Column>
    );
};


export default withRouter(connect(state => ({
    isMobile: state.frontState.isMobile,
    registrationToComplete: state.frontState.registrationToComplete
}))(CatalogBannerIncitement));