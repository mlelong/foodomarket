import React from "react";
import PropTypes from "prop-types";
import {Dropdown, Menu} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import {States} from "shop/constants/frontConstants";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {generateUrl} from "shop/router";

class CatalogMenu extends React.Component
{
    static propTypes = {
        fluid: PropTypes.bool
    };

    static defaultProps = {
        fluid: false
    };

    state = {
        taxons: this.props.initialValues.hasOwnProperty('taxons') ? this.props.initialValues.taxons : null
    };

    componentDidMount() {
        if (this.state.taxons === null) {
            Get(generateUrl('app_front_taxons'))
                .then(data => {
                    this.setState({taxons: data.taxons});
                })
            ;
        }
    }

    static getDisplayTaxon(taxon) {
        // return `${taxon.text} (${taxon.products})`;
        return `${taxon.text}`;
    }

    render() {
        const {authToken} = this.props;
        const menuRenderer = (taxon) =>
            <Dropdown key={taxon.id} item text={CatalogMenu.getDisplayTaxon(taxon)} openOnFocus={false} scrolling={taxon.id != 0}>
                <Dropdown.Menu>
                    {taxon.children.map(child =>
                        child.hasOwnProperty('children') && Array.isArray(child.children) && child.children.length > 0
                            ? menuRenderer(child)
                            : <Dropdown.Item className={child.id === -1 ? 'promo-item' : ''} key={child.id} text={CatalogMenu.getDisplayTaxon(child)} as={Link} to={child.id !== -1 ? States.BROWSE_CATALOG.replace(':slug', encodeURIComponent(child.slug)) : States.CATALOG_PROMOS}/>
                    )}
                </Dropdown.Menu>
            </Dropdown>
        ;

        const {fluid} = this.props;
        const {taxons} = this.state;
        const taxonEntry = {
            id: 0,
            text: 'Produits',
            products: taxons ? taxons.products : 0,
            children: taxons ? taxons.children : []
         };

        return (
            <Menu id={"catalog-menu"} inverted compact borderless fluid={fluid} className={authToken ? "logged-in" : "logged-out"}>
                {menuRenderer(taxonEntry)}
            </Menu>
        );
    }
}

export default connect(state => ({
    initialValues: state.frontState.initialValues,
    authToken: state.frontState.authToken
}))(CatalogMenu)