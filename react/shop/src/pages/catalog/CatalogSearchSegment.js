import React from "react";
import {connect} from "react-redux";
import {Button, Card, Grid, Header, Message, Modal, Segment} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import {withRouter} from "react-router-dom";
import Filters from "shop/components/Filters";
import {generateUrl} from "shop/router";
import {clearInitialValues} from "shop/actions";
import {Helmet} from "react-helmet-async";
import SimpleTelephoneForm from "shop/components/SimpleTelephoneForm";
import ReactPixel from "react-facebook-pixel";
import ProductCard from "./ProductCard";

class CatalogSearchSegment extends React.Component
{
    state = {
        loading: false,
        products: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.products : [],
        filters: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.filters : [],
        filteredProducts: this.props.initialValues.hasOwnProperty('catalog') ? this.props.initialValues.catalog.products : [],
        filterModalOpen: false
    };

    componentDidMount() {
        const {match, dispatch} = this.props;

        if (this.state.products.length === 0) {
            this.loadProducts(match.params.terms);
        } else {
            this.trackProducts(this.state.products);
            dispatch(clearInitialValues(['catalog']));
        }
    }

    loadProducts = (terms) => {
        this.setState({loading: true});

        return Get(generateUrl('app_front_products_by_terms', {terms: terms}))
            .then(data => {
                this.trackProducts(data.products);
                this.setState({
                    products: data.products,
                    filters: data.filters,
                    loading: false
                });
            }).catch(ignored => {
                this.setState({loading: false});
            })
        ;
    };

    trackProducts = (products) => {
        const productIds = products.map(product => `${product.id}`);

        if (productIds.length > 0) {
            ReactPixel.track('ViewContent', {
                content_ids: productIds,
                content_type: 'product'
            });
        }
    };

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.terms != this.props.match.params.terms) {
            this.loadProducts(this.props.match.params.terms);
        }
    }

    render() {
        const {loading, filters, products, filteredProducts, filterModalOpen} = this.state;
        const filteringCard = <Filters itemName={"produit"} itemNamePluralized={"produits"} filters={filters} results={products} onResultsFiltered={(filteredProducts) => {
            this.setState({filteredProducts});
        }}/>;

        return (
            <Segment id="catalog-segment" loading={loading}>
                <Helmet
                    titleTemplate="%s | FoodoMarket.com"
                >
                    <title>Le marché en ligne des restaurateurs</title>
                    <meta name="description"
                          content="Professionnels de la restauration, achetez vos produits alimentaires au meilleur prix sur FoodoMarket. Livraison en A pour B dans toute l'île-de-France."/>
                </Helmet>
                <Grid>
                    <Grid.Row>
                        <Grid.Column computer={16} tablet={10} mobile={12}>
                            {this.props.match.params.terms.trim().length > 0 &&
                            <Header as={'h4'}>Résultats pour &laquo;
                                <strong>{this.props.match.params.terms}</strong>&raquo; :
                            </Header>
                            ||
                            <Header as={'h4'}>Tous les résultats :</Header>
                            }
                        </Grid.Column>
                        <Grid.Column only={"tablet mobile"} tablet={6} mobile={4} textAlign={"right"}>
                            <Modal trigger={<a id="filter-trigger" href="#" onClick={(e) => {e.preventDefault(); this.setState({filterModalOpen: true})}}>Filtres</a>} closeIcon closeOnDocumentClick closeOnDimmerClick open={filterModalOpen} onClose={() => this.setState({filterModalOpen: false})}>
                                <Modal.Header>Filtrer les produits</Modal.Header>
                                <Modal.Content scrolling>
                                    {filteringCard}
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button primary fluid onClick={() => this.setState({filterModalOpen: false})}>
                                        Appliquer
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column computer={4} only={"computer"}>
                            {filteringCard}
                        </Grid.Column>

                        <Grid.Column computer={12} mobile={16}>
                            {filteredProducts.length > 0 &&
                            <Card.Group itemsPerRow={4} doubling>
                                {filteredProducts.map(product =>
                                    <ProductCard key={product.id} product={product}/>
                                )}
                            </Card.Group>
                            || !loading &&
                            <div>
                                <Message warning>
                                    <Message.Header>Pas de résultat</Message.Header>
                                    <p>Il est possible que cette référence soit disponible mais pas encore en ligne.</p>
                                </Message>

                                <SimpleTelephoneForm/>
                            </div>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default withRouter(connect(state => ({
    initialValues: state.frontState.initialValues,
}))(CatalogSearchSegment));