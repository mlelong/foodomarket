import React from "react";
import {connect} from "react-redux";
import {Breadcrumb, Button, Card, Grid, Message, Modal, Segment} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import {Link, withRouter} from "react-router-dom";
import Filters from "shop/components/Filters";
import {generateUrl} from "shop/router";
import {clearInitialValues} from "shop/actions";
import {Helmet} from "react-helmet-async";
import ReactPixel from "react-facebook-pixel";
import ProductCard from "./ProductCard";
import {States} from "shop/constants/frontConstants";

class CatalogPromoSegment extends React.Component
{
    state = {
        loading: false,
        products: this.props.initialValues.hasOwnProperty('catalog-promos') ? this.props.initialValues['catalog-promos'].products : [],
        filters: this.props.initialValues.hasOwnProperty('catalog-promos') ? this.props.initialValues['catalog-promos'].filters : [],
        filteredProducts: this.props.initialValues.hasOwnProperty('catalog-promos') ? this.props.initialValues['catalog-promos'].products : [],
        filterModalOpen: false
    };

    componentDidMount() {
        const {dispatch} = this.props;

        if (this.state.products.length === 0) {
            this.loadProducts();
        } else {
            this.trackProducts(this.state.products);
            dispatch(clearInitialValues(['catalog-promos']));
        }
    }

    trackProducts = (products) => {
        const productIds = products.map(product => `${product.id}`);

        if (productIds.length > 0) {
            ReactPixel.track('ViewContent', {
                content_ids: productIds,
                content_type: 'product'
            });
        }
    };

    loadProducts = () => {
        this.setState({loading: true});

        return Get(generateUrl('app_front_products_by_promo'))
            .then(data => {
                this.trackProducts(data.products);
                this.setState({
                    products: data.products,
                    filters: data.filters,
                    breadcrumb: data.taxons,
                    loading: false
                });
            }).catch(ignored => {
                this.setState({loading: false});
            })
        ;
    };

    render() {
        const {loading, filters, products, filteredProducts, filterModalOpen} = this.state;
        const filteringCard = <Filters itemName={"produit"} itemNamePluralized={"produits"} filters={filters} results={products} onResultsFiltered={(filteredProducts) => {
            this.setState({filteredProducts});
        }}/>;

        return (
            <Segment id="catalog-segment" loading={loading}>
                <Helmet titleTemplate="%s | FoodoMarket.com">
                    <title children={`Nos offres promotionnelles au meilleur prix, livraison comprise`}/>
                    <meta name="description" content={`Retrouvez un vaste choix de promos sur FoodoMarket, la plateforme des restaurateurs`}/>
                </Helmet>

                <Grid>
                    <Grid.Row>
                        <Grid.Column computer={16} tablet={10} mobile={12}>
                            <Breadcrumb>
                                <Breadcrumb.Section active={false}>
                                    Catalogue
                                </Breadcrumb.Section>
                                <Breadcrumb.Divider icon='right angle'/>
                                <Breadcrumb.Section active={true} as={Link} to={States.CATALOG_PROMOS}>
                                    Promotions
                                </Breadcrumb.Section>
                            </Breadcrumb>
                        </Grid.Column>
                        <Grid.Column only={"tablet mobile"} tablet={6} mobile={4} textAlign={"right"}>
                            <Modal trigger={<a id="filter-trigger" href="#" onClick={(e) => {e.preventDefault(); this.setState({filterModalOpen: true})}}>Filtres</a>} closeIcon closeOnDocumentClick closeOnDimmerClick open={filterModalOpen} onClose={() => this.setState({filterModalOpen: false})}>
                                <Modal.Header>Filtrer les produits</Modal.Header>
                                <Modal.Content scrolling>
                                    {filteringCard}
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button primary fluid onClick={() => this.setState({filterModalOpen: false})}>
                                        Appliquer
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column computer={4} only={"computer"}>
                            {filteringCard}
                        </Grid.Column>

                        <Grid.Column computer={12} mobile={16}>
                            {filteredProducts.length > 0 &&
                            <Card.Group itemsPerRow={4} doubling>
                                {filteredProducts.map(product =>
                                    <ProductCard key={product.id} product={product}/>
                                )}
                            </Card.Group>
                            || !loading &&
                            <Message warning>
                                <Message.Header>Pas de résultat</Message.Header>
                                <p>Nous vous invitons à naviguer dans les autres catégories de produit.</p>
                            </Message>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default withRouter(connect(state => ({
    initialValues: state.frontState.initialValues,
}))(CatalogPromoSegment));
