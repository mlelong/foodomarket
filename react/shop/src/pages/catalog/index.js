import Loadable from "react-loadable";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";
import Loading from "shop/components/Widget/Loading";

const catalogLoader = Loadable({
    loader: () => import('./CatalogSegment'),
    loading: Loading,
    delay: 500
});

const catalogPromoLoader = Loadable({
    loader: () => import('./CatalogPromoSegment'),
    loading: Loading,
    delay: 500
});

const catalogSearchLoader = Loadable({
    loader: () => import('./CatalogSearchSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.BROWSE_CATALOG, true, catalogLoader);
addRoute(States.CATALOG_PROMOS, true, catalogPromoLoader);
addRoute(States.SEARCH_CATALOG, true, catalogSearchLoader);
