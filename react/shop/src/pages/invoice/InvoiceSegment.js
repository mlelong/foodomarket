import React from "react";
import {connect} from "react-redux";
import {Accordion, Grid, Header, Icon, Message, Segment, Table} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import {hideLoadingIndicator, showLoadingIndicator} from "shop/actions";
import {DateInput} from "semantic-ui-calendar-react";
import moment from "moment";
import {generateUrl} from "shop/router";

class InvoiceSegment extends React.Component
{
    state = {
        dateStart: moment().startOf('month').format('DD/MM/YYYY'),
        dateEnd: moment().format('DD/MM/YYYY'),
        invoices: []
    };

    componentDidMount() {
        moment.locale('fr');

        this.load();
    }

    load = () => {
        const {dispatch} = this.props;

        dispatch(showLoadingIndicator());

        Get(generateUrl('app_front_invoices', {start: this.state.dateStart, end: this.state.dateEnd}))
            .then(data => {
                dispatch(hideLoadingIndicator());

                if (data.ok) {
                    this.setState({invoices: data.invoices});
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                dispatch(hideLoadingIndicator());
                console.log(exception);
            })
        ;
    };

    handleDateChange = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            this.setState({[name]: value.split('-').join('/')});
        }
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.dateStart != this.state.dateStart || prevState.dateEnd != this.state.dateEnd) {
            this.load();
        }
    }

    render() {
        const {invoices} = this.state;

        const invoicesPanels = invoices.map((invoice, index) => {
            return {
                key: `invoice-${index}`,
                title: {
                    content:
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={12}>
                                    <Icon name={"dropdown"}/>
                                    {invoice.supplier} - {invoice.date} - {invoice.number}
                                </Grid.Column>

                                <Grid.Column width={4} textAlign={"right"}>
                                    <a target="_blank" onClick={(e) => {e.stopPropagation()}} href={generateUrl('app_front_invoice_download', {id: invoice.id})}>Télécharger la facture</a>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                },
                content: {
                    content:
                        <Accordion.Accordion exclusive={false} panels={invoice.deliveryNotes.map((deliveryNote, index2) => {
                            let totalInvoice = 0;
                            let totalFoodomarket = 0;
                            
                            return {
                                key: `deliveryNote-${index2}`,
                                title: `Livraison du ${deliveryNote.date} - ${deliveryNote.number}`,
                                content: {
                                    content:
                                        <Table celled structured>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell colSpan={5}>Facture</Table.HeaderCell>
                                                    <Table.HeaderCell colSpan={3}>FoodoMarket</Table.HeaderCell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.HeaderCell>Référence</Table.HeaderCell>
                                                    <Table.HeaderCell>Produit</Table.HeaderCell>
                                                    <Table.HeaderCell>Quantité</Table.HeaderCell>
                                                    <Table.HeaderCell>PU HT</Table.HeaderCell>
                                                    <Table.HeaderCell>Total HT</Table.HeaderCell>

                                                    <Table.HeaderCell>Produit</Table.HeaderCell>
                                                    <Table.HeaderCell>PU HT</Table.HeaderCell>
                                                    <Table.HeaderCell>Total HT</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>

                                            <Table.Body>
                                                {deliveryNote.items.map((item, index3) => {
                                                    totalInvoice += item.original.totalPrice;
                                                    totalFoodomarket += (item.computed.totalPrice > 0 ? item.computed.totalPrice : item.original.totalPrice);

                                                    return (
                                                        <Table.Row
                                                            key={`item-${index3}`}
                                                            negative={item.computed.unitPrice > 0 && (item.original.unitPrice.toFixed(2) - item.computed.unitPrice.toFixed(2)) > 0.03}
                                                            positive={(item.computed.unitPrice.toFixed(2) - item.original.unitPrice.toFixed(2)) > 0.03}
                                                            warning={item.computed.unitPrice == 0}
                                                        >
                                                            <Table.Cell>{item.original.reference}</Table.Cell>
                                                            <Table.Cell>{item.original.name}</Table.Cell>
                                                            <Table.Cell textAlign='right'>{item.original.quantity.toFixed(2)}</Table.Cell>
                                                            <Table.Cell textAlign='right'><strong>{item.original.unitPrice.toFixed(2)}€</strong></Table.Cell>
                                                            <Table.Cell textAlign='right'>{item.original.totalPrice.toFixed(2)}€</Table.Cell>

                                                            <Table.Cell>{item.computed.name}</Table.Cell>
                                                            <Table.Cell textAlign='right'><strong>{item.computed.unitPrice.toFixed(2)}€</strong></Table.Cell>
                                                            <Table.Cell textAlign='right'>{item.computed.totalPrice.toFixed(2)}€</Table.Cell>
                                                        </Table.Row>
                                                    );
                                                })}
                                            </Table.Body>

                                            <Table.Footer>
                                                <Table.Row>
                                                    <Table.Cell colSpan={4} textAlign='right'><strong>Total grossiste: </strong></Table.Cell>
                                                    <Table.Cell textAlign='right'><strong>{totalInvoice.toFixed(2)}€</strong></Table.Cell>
                                                    <Table.Cell colSpan={2} textAlign='right'><strong>Total FoodoMarket: </strong></Table.Cell>
                                                    <Table.Cell textAlign='right'><strong>{totalFoodomarket.toFixed(2)}€</strong></Table.Cell>
                                                </Table.Row>

                                                <Table.Row negative={(totalFoodomarket - totalInvoice) < 0} positive={(totalFoodomarket - totalInvoice) > 0} warning={(totalFoodomarket - totalInvoice) == 0}>
                                                    <Table.Cell colSpan={7} textAlign='right'>
                                                        <strong>Différence:</strong>
                                                    </Table.Cell>
                                                    <Table.Cell textAlign='right'>
                                                        <strong>{(totalFoodomarket - totalInvoice).toFixed(2)}€</strong>
                                                    </Table.Cell>
                                                </Table.Row>
                                            </Table.Footer>
                                        </Table>
                                    }
                            }
                        })}/>
                }
            };
        });

        const minDate = moment(this.state.dateStart.split('/').reverse().join('-'), 'YYYY-MM-DD').add(1, 'day');
        const maxDate = moment(this.state.dateEnd.split('/').reverse().join('-'), 'YYYY-MM-DD').subtract(1, 'day');

        return (
            <Segment>
                <Header dividing size={"large"} textAlign={"left"}>Factures</Header>

                <Grid stackable>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Message icon info>
                                <Icon name={"briefcase"}/>
                                <Message.Content>
                                    <Message.Header>
                                        Espace Factures
                                    </Message.Header>
                                    Retrouvez toutes vos factures dans cet espace.
                                </Message.Content>
                            </Message>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <DateInput
                                key={Math.random()}
                                fluid
                                label="Date de début"
                                name="dateStart"
                                placeholder="Date de début"
                                value={this.state.dateStart}
                                maxDate={maxDate}
                                icon={"calendar"}
                                onChange={this.handleDateChange}
                                closable={true}
                                closeOnMouseLeave={false}
                                popupPosition={"bottom center"}
                                localization={'fr'}
                                readOnly={true}
                            />
                        </Grid.Column>

                        <Grid.Column>
                            <DateInput
                                key={Math.random()}
                                fluid
                                name="dateEnd"
                                label="Date de fin"
                                placeholder="Date de fin"
                                value={this.state.dateEnd}
                                minDate={minDate}
                                icon={"calendar"}
                                onChange={this.handleDateChange}
                                closable={true}
                                closeOnMouseLeave={false}
                                popupPosition={"bottom center"}
                                localization={'fr'}
                                readOnly={true}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column width={16}>
                            {invoices.length > 0
                            && <div>
                                <Message icon warning>
                                    <Icon name={"exclamation triangle"}/>
                                    <Message.Content>
                                        <Message.Header>
                                            Avertissement sur les rapports
                                        </Message.Header>
                                        <p>
                                            Nous effectuons ici un contrôle du montant des produits facturés
                                            avec les tarifs de la période concernée issus de notre base de données.
                                        </p>
                                        <p>
                                            Ces informations vous aideront à effectuer des contrôles sur vos commandes,
                                            mais ne peuvent pas faire foi vis à vis des grossistes.<br/>
                                            Nous vous invitons à vous munir de votre mercuriale à date de livraison
                                            pour attester d'un réel problème de facturation.
                                        </p>
                                        <p>
                                            Enfin il peut y avoir des faux positifs dans le rapport. Prenez garde aux libellés affichés dans les colonnes "Facture -> Produit" et "FoodoMarket -> Produit".
                                        </p>
                                    </Message.Content>
                                </Message>
                                <Accordion className={"invoice-accordion"} styled panels={invoicesPanels} exclusive={false} fluid/>
                            </div>
                            || <Message negative>
                                <Message.Content>
                                    Pas de facture(s) pour la période du {this.state.dateStart} au {this.state.dateEnd}
                                </Message.Content>
                            </Message>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default connect(state => ({

}))(InvoiceSegment)