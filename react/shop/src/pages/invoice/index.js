import Loadable from "react-loadable";
import {addRoute} from "shop/router/router";
import Loading from "shop/components/Widget/Loading";
import {States} from "shop/constants/frontConstants";

const invoiceLoader = Loadable({
    loader: () => import('./InvoiceSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.INVOICE, true, invoiceLoader);
