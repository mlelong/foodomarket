import React from "react";
import {Grid, Header, Message, Segment, Accordion, Icon} from "semantic-ui-react";
import PlaceOrderMenu from "shop/components/PlaceOrderMenu";
import {connect} from "react-redux";
import OrderSupplierProducts from "shop/components/OrderSupplierProducts";
import SupplierList from "shop/components/SupplierList";
import {showRightPanel} from "shop/actions";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";

class SupplierOrderSegment extends React.PureComponent {
    state = {
        supplier: [],
        selectedSupplierId: 0,
        visible: true,
        activeIndex: 0
    };

    handleDismiss = () => {
        this.setState({visible: false})
    };

    componentDidMount() {
        const {isMobileOrTablet} = this.props;

        if (!isMobileOrTablet) {
            this.props.dispatch(showRightPanel());
        }
    }

    handleClick = (e, titleProps) => {
        const {index} = titleProps;
        const {activeIndex} = this.state;
        const newIndex = activeIndex === index ? -1 : index;

        this.setState({activeIndex: newIndex});
    };

    selectSupplier = (supplierId) => {
        this.setState({selectedSupplierId: supplierId});
    };

    render() {
        const {selectedSupplierId} = this.state;
        const {suppliers, firstConnection, isMobileOrTablet} = this.props;
        const disabledSuppliers = [];
        const {activeIndex} = this.state;

        for (let supplier of suppliers) {
            if (supplier['order_enabled'] === false) {
                disabledSuppliers.push(supplier.supplierName);
            }
        }

        return (
            <Segment basic id={"supplier-order-segment"}>
                <Header className="content-title" as="h1">Commander à partir de...</Header>

                <PlaceOrderMenu/>

                <Grid stackable={true} padded={false}>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Message info>
                                {firstConnection > 0 &&
                                <Message.Header>Nous avons fait une première sélection de fournisseurs pour
                                    vous</Message.Header>
                                || <Message.Header>Voici les fournisseurs sélectionnés pour vous</Message.Header>
                                }
                                <p>
                                    N'hésitez pas à nous appeler au <a href="tel:+33171546145">01 71 54 61 45</a> pour
                                    accéder à de nouvelles offres de fournisseurs.
                                </p>
                            </Message>

                            {disabledSuppliers.length > 0 && this.state.visible &&

                            <Message onDismiss={this.handleDismiss} negative>
                                <Message.Header>
                                    Attention, vous ne pouvez pas encore passer commande auprès des fournisseurs
                                    suivants:
                                </Message.Header>
                                <ul className="ui list">
                                    {disabledSuppliers.map((disabledSupplier, index) => <li key={index}
                                                                                            className={"item"}>{disabledSupplier}</li>)}
                                </ul>
                                <p>
                                    <Link to={States.SEPA}>Vous pouvez suivre le statut de vos ouvertures de compte sur
                                        la page 'Mes fournisseurs'</Link>
                                </p>
                            </Message>
                            }
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column tablet={16} computer={4} className={"grid-shopping-suppliers"}>
                            <Segment className={"shopping-list-suppliers-segment"}>
                                {isMobileOrTablet &&
                                <>
                                    <Accordion>
                                        <Accordion.Title
                                            active={activeIndex === 0}
                                            index={0}
                                            onClick={this.handleClick}
                                        >
                                            <h3 className={"border-titre1"}>
                                                <Icon name="dropdown"/>
                                                <span className={"shopping-list-creation-step-nb"}>1. </span>
                                                Sélectionnez vos fournisseurs
                                            </h3>

                                        </Accordion.Title>
                                        <Accordion.Content active={activeIndex === 0}>
                                            <SupplierList onSupplierSelected={this.selectSupplier}/>
                                        </Accordion.Content>
                                    </Accordion>
                                </>
                                }
                                {!isMobileOrTablet &&
                                <>
                                    <h3 className={"border-titre1"}>
                                        <span className={"shopping-list-creation-step-nb"}>1.</span>
                                        Sélectionnez vos fournisseurs
                                    </h3>
                                    <SupplierList onSupplierSelected={this.selectSupplier}/>
                                </>
                                }
                            </Segment>
                        </Grid.Column>

                        <Grid.Column tablet={16} computer={12}>
                            <Grid.Row>
                                <OrderSupplierProducts supplierId={selectedSupplierId}/>
                            </Grid.Row>

                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default connect(state => (
    {
        suppliers: state.frontState.suppliers,
        isMobileOrTablet: state.frontState.isMobileOrTablet,
        firstConnection: state.frontState.initialValues.firstConnection
    }
))(SupplierOrderSegment)