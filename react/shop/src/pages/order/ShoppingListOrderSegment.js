import React from "react";
import {Button, Grid, Header, Icon, List, Segment} from "semantic-ui-react";
import PlaceOrderMenu from "shop/components/PlaceOrderMenu";
import {connect} from "react-redux";
import SuppliersDropdown from "shop/components/SuppliersDropdown";
import FrontInput from "shop/components/FrontInput";
import ShoppingListOrderLine from "shop/components/ShoppingListOrderLine";
import splitList from "shop/utils/SplitList";
import AddProductModal from "shop/components/AddProductModal";
import {addAllToCart, addToCart, removeFromCart, showRightPanel} from "shop/actions";
import {Get} from "shop/utils/fetch";
import {withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import Filter from "shop/utils/Filter";
import {generateUrl} from "shop/router";
import {getSupplierItem} from "shop/utils/shoppingItems";

class ShoppingListOrderSegment extends React.Component {
    state = {
        selectedSupplier: 0,
        addProductModalOpen: false,
        listName: '',
        products: [],
        filteredProducts: []
    };

    componentDidMount() {
        const {match, dispatch, isMobileOrTablet} = this.props;

        if (!isMobileOrTablet) {
            dispatch(showRightPanel());
        }

        if (match.path === States.SHOPPING_LIST_ORDER) {
            Get(generateUrl('app_front_getShoppingListItems', {id: match.params.id}))
                .then(data => {
                    this.setState({listName: data.listName, products: data.data, filteredProducts: data.data});
                }).catch(exception => {
                    console.log(exception);
                    alert("Exception lors du chargement des produits de la liste: " + exception);
                })
            ;
        }
    }

    onFilter = (terms) => {
        this.setState({filteredProducts: Filter.filter(this.state.products, terms, 'productName')});
    };

    addProduct = () => {
        this.setState({addProductModalOpen: true});
    };

    onSupplierSelected = (supplier) => {
        this.setState({selectedSupplier: supplier.supplierId});
    };

    closeModal = () => {
        this.setState({addProductModalOpen: false});
    };

    orderAll = () => {
        const products = [];

        for (let product of this.state.filteredProducts) {
            if (product.quantity > 0) {
                products.push(product);
            }
        }

        this.props.dispatch(addAllToCart(products));
    };

    onProductAdded = (product) => {
        this.props.dispatch(addToCart(product));
    };

    onProductRemoved = (product) => {
        this.props.dispatch(removeFromCart(product));
    };

    onQuantityChanged = (product, quantity, unit) => {
        const {products, filteredProducts} = this.state;
        const f = (p) => {
            if (p.productId == product.productId && p.supplierId == product.supplierId) {
                p.quantity = quantity;
                p.quantityUnitSelected = unit;
            }

            return p;
        };

        this.setState({products: products.map(f), filteredProducts: filteredProducts.map(f)});
    };

    render() {
        const {shoppingCarts, isMobile, restaurantSuppliers} = this.props;
        const {selectedSupplier, listName} = this.state;
        const products = splitList(this.state.filteredProducts);
        const allSupplier = {
            supplierId: 0,
            supplierName: 'Tous les fournisseurs',
            supplierPicture: '',
            supplierProducts: 0
        };
        const suppliers = [];

        const disabledSuppliers = [];

        for (let supplier of restaurantSuppliers) {
            if (supplier['order_enabled'] === false) {
                disabledSuppliers.push(supplier.supplierId);
            }
        }

        const supplierProductList = products.map((supplier, index) => {
            allSupplier.supplierProducts += supplier.products.length;

            suppliers.push({
                supplierId: supplier.supplierId,
                supplierName: supplier.supplierName,
                supplierPicture: '',
                supplierProducts: supplier.products.length
            });

            if (selectedSupplier != 0 && selectedSupplier != supplier.supplierId) {
                return ;
            }

            return (
                <Grid className={"shopping-list-supplier-group"} key={index}>
                    <Grid.Row className={"supplier-name-section"}>
                        <Grid.Column>
                            <Header as={"h4"}>{supplier.supplierName}</Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row className={"supplier-products-section"}>
                        <Grid.Column>
                            <List>
                                {supplier.products.map((product, index) => {
                                    const shoppingCartProduct = getSupplierItem(product.supplierId, product.productId, shoppingCarts);
                                    let supplierProduct = product;

                                    // Copy shopping cart related information (cart id, cart item id, product note, real quantity in cart, etc...)
                                    if (shoppingCartProduct) {
                                        supplierProduct = {
                                            ...product,
                                            ...shoppingCartProduct
                                        };
                                    }

                                    return (
                                        <ShoppingListOrderLine
                                            key={index}
                                            product={supplierProduct}
                                            note={supplierProduct.note}
                                            onQuantityChanged={this.onQuantityChanged}
                                            orderEnabled={!disabledSuppliers.includes(supplierProduct.supplierId)}
                                        />
                                    );
                                })}
                            </List>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            );
        }).filter(function(e){return e});

        suppliers.unshift(allSupplier);

        return (
            <Segment className={isMobile ? "mobile" : ""}>
                <Header className="content-title" as="h1">Commander à partir de &laquo;{listName}&raquo;</Header>

                <PlaceOrderMenu/>

                <Grid>
                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <SuppliersDropdown suppliers={suppliers} onSupplierSelected={this.onSupplierSelected} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>

                <Grid>
                    <Grid.Row>
                        <Grid.Column width={12}>
                            <FrontInput icon='search' placeholder="Filtrez vos produits" onOk={this.onFilter}/>
                        </Grid.Column>
                        <Grid.Column width={4} only={"computer"} textAlign={"right"}>
                            <Button basic onClick={this.addProduct} icon={"plus"} labelPosition={"left"} content={"Ajouter un produit"}/>
                        </Grid.Column>
                        <Grid.Column width={4} only={"tablet mobile"} textAlign={"right"}>
                            <Button basic onClick={this.addProduct}>
                                <Icon name={"plus"}/>
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>

                {supplierProductList}

                <div id={"order-all-container"}>
                    <Button className={"order-all"} fluid content={"Ajouter tous les produits au panier"} onClick={this.orderAll}/>
                </div>

                <AddProductModal onAddToList={this.onProductAdded} onRemoveFromList={this.onProductRemoved} onModalClosed={this.closeModal} modalOpen={this.state.addProductModalOpen}/>
            </Segment>
        );
    }
}

export default withRouter(connect(state => (
    {
        isMobile: state.frontState.isMobile,
        shoppingCarts: state.shoppingCarts,
        isMobileOrTablet: state.frontState.isMobileOrTablet,
        restaurantSuppliers: state.frontState.suppliers
    }
))(ShoppingListOrderSegment));
