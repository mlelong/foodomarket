import Loadable from "react-loadable";
import Loading from "shop/components/Widget/Loading";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";

const orderPreviousOrderLoader = Loadable({
    loader: () => import('./OrderPreviousOrderSegment'),
    loading: Loading,
    delay: 500
});

const previousOrdersLoader = Loadable({
    loader: () => import('./PreviousOrdersSegment'),
    loading: Loading,
    delay: 500
});

const shoppingListListingLoader = Loadable({
    loader: () => import('./ShoppingListListingSegment'),
    loading: Loading,
    delay: 500
});

const shoppingListOrderLoader = Loadable({
    loader: () => import('./ShoppingListOrderSegment'),
    loading: Loading,
    delay: 500
});

const supplierOrderPromoLoader = Loadable({
    loader: () => import('./SupplierOrderPromoSegment'),
    loading: Loading,
    delay: 500
});

const supplierOrderLoader = Loadable({
    loader: () => import('./SupplierOrderSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.ORDER_PREVIOUS_ORDER, true, orderPreviousOrderLoader);
addRoute(States.PREVIOUS_ORDERS_LISTING, true, previousOrdersLoader);
addRoute(States.SHOPPING_LIST_LISTING, true, shoppingListListingLoader);
addRoute(States.SHOPPING_LIST_ORDER, true, shoppingListOrderLoader);
addRoute(States.SUPPLIER_ORDER_PROMO, true, supplierOrderPromoLoader);
addRoute(States.SUPPLIER_ORDER, true, supplierOrderLoader);
