import React from "react";
import {Header, Segment} from "semantic-ui-react";
import PlaceOrderMenu from "shop/components/PlaceOrderMenu";
import ShoppingLists from "shop/components/ShoppingLists";
import {connect} from "react-redux";
import {changeState} from "shop/actions";
import {States} from "shop/constants/frontConstants";

class ShoppingListListingSegment extends React.Component {
    componentDidMount() {
        this.props.dispatch(changeState(States.SHOPPING_LIST_LISTING));
    }

    render() {
        return (
            <Segment>
                <Header className="content-title" as="h1">Commander à partir de...</Header>
                <PlaceOrderMenu />
                <ShoppingLists />
            </Segment>
        );
    }
}

export default connect(state => (
    {
    }
))(ShoppingListListingSegment)