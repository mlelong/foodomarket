import React from "react";
import {Grid, Header, List, Message, Segment} from "semantic-ui-react";
import {connect} from "react-redux";
import PlaceOrderMenu from "shop/components/PlaceOrderMenu";
import ShoppingList from "shop/components/ShoppingList";
import {Constants} from "shop/constants/frontConstants";
import {showRightPanel} from "shop/actions";
import {DateInput} from "semantic-ui-calendar-react";
import moment from "moment";
import {Get} from "shop/utils/fetch";
import {generateUrl} from "shop/router";

class PreviousOrdersSegment extends React.Component {
    state = {
        previousOrders: [],
        dateStart: moment().subtract(20, 'day'),
        dateEnd: moment().add(1, 'day')
    };

    componentDidMount() {
        moment.locale('fr');

        const {dispatch, isMobileOrTablet} = this.props;

        if (!isMobileOrTablet) {
            dispatch(showRightPanel());
        }

        this.load();
    }

    load = () => {
        const {dispatch} = this.props;

        dispatch({ type: Constants.LOADING_RESOURCE, loading: true });

        Get(generateUrl('app_front_previouslyOrdered', {start: this.state.dateStart.format('YYYY-MM-DD'), end: this.state.dateEnd.format('YYYY-MM-DD')})).then((data) => {
            dispatch({ type: Constants.LOADING_RESOURCE, loading: false });

            if (data.ok) {
                this.setState({previousOrders: data.carts});
            } else {
                throw Error(data.message);
            }
        }).catch((exception) => {
            dispatch({ type: Constants.LOADING_RESOURCE, loading: false });
            console.log("Exception caught while fetching previous orders", exception);
            this.setState({previousOrders: null});
        });
    };

    handleDateChange = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            this.setState({[name]: moment(value, 'DD/MM/YYYY')});
        }
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.dateStart != this.state.dateStart
            || prevState.dateEnd != this.state.dateEnd) {
            this.load();
        }
    }

    render() {
        const {previousOrders} = this.state;

        const list = previousOrders.map((order, index) => {
            return <ShoppingList data={order} key={index} />
        });

        const minDate = moment(this.state.dateStart).add(1, 'day');
        const maxDate = moment(this.state.dateEnd).subtract(1, 'day');

        const dateStartValue = this.state.dateStart.format('DD/MM/YYYY');
        const dateEndValue = this.state.dateEnd.format('DD/MM/YYYY');

        return (
            <Segment>
                <Header className="content-title" as="h1">Commander à partir de...</Header>

                <PlaceOrderMenu />

                <Grid>
                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <DateInput
                                key={Math.random()}
                                dateFormat="DD/MM/YYYY"
                                fluid
                                label="Date de début"
                                name="dateStart"
                                placeholder="Date de début"
                                value={dateStartValue}
                                initialDate={dateStartValue}
                                maxDate={maxDate.format('DD/MM/YYYY')}
                                icon={"calendar"}
                                onChange={this.handleDateChange}
                                closable={true}
                                inlineLabel={false}
                                closeOnMouseLeave={false}
                                popupPosition={"bottom left"}
                                localization='fr'
                                readOnly={true}
                            />
                        </Grid.Column>

                        <Grid.Column>
                            <DateInput
                                key={Math.random()}
                                dateFormat="DD/MM/YYYY"
                                fluid
                                name="dateEnd"
                                label="Date de fin"
                                placeholder="Date de fin"
                                value={dateEndValue}
                                initialDate={dateEndValue}
                                minDate={minDate.format('DD/MM/YYYY')}
                                icon={"calendar"}
                                onChange={this.handleDateChange}
                                closable={true}
                                inlineLabel={true}
                                closeOnMouseLeave={false}
                                popupPosition={"bottom right"}
                                localization='fr'
                                readOnly={true}
                            />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>

                {previousOrders.length > 0 &&
                <List className={"shopping-lists"}>
                    {list}
                </List>
                ||
                <Message info>
                    <Message.Header>Nous n'avons pas trouvé de commande pour cette période</Message.Header>
                    <Message.Content>
                        Aucune commande n'a été passée sur notre plateforme entre le <b>&laquo;{dateStartValue}&raquo;</b> et le <b>&laquo;{dateEndValue}&raquo;</b>.<br/>
                        Vous pouvez changer la plage de date avec les sélecteurs ci-dessus.
                    </Message.Content>
                </Message>
                }
            </Segment>
        );
    }
}

export default connect(state => (
    {
        previousOrders: state.frontState.previousOrders,
        isMobileOrTablet: state.frontState.isMobileOrTablet
    }
))(PreviousOrdersSegment)