import React from "react";
import {Grid, Header, Message, Segment} from "semantic-ui-react";
import PlaceOrderMenu from "shop/components/PlaceOrderMenu";
import {connect} from "react-redux";
import OrderSupplierProducts from "shop/components/OrderSupplierProducts";
import SupplierList from "shop/components/SupplierList";
import {showRightPanel} from "shop/actions";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";

class SupplierOrderPromoSegment extends React.PureComponent {
    state = {
        supplier: [],
        selectedSupplierId: 0
    };

    componentDidMount() {
        const {isMobileOrTablet} = this.props;

        if (!isMobileOrTablet) {
            this.props.dispatch(showRightPanel());
        }
    }

    selectSupplier = (supplierId) => {
        this.setState({selectedSupplierId: supplierId});
    };

    render() {
        const {selectedSupplierId} = this.state;
        const {suppliers} = this.props;
        const disabledSuppliers = [];

        for (let supplier of suppliers) {
            if (supplier['order_enabled'] === false) {
                disabledSuppliers.push(supplier.supplierName);
            }
        }

        return (
            <Segment basic id={"supplier-order-segment"}>
                <Header className="content-title" as="h1">Commander à partir de...</Header>

                <PlaceOrderMenu />

                <Grid stackable={true} padded={false}>
                    <Grid.Row>
                        <Grid.Column tablet={16} computer={5}>
                            <Segment className={"shopping-list-suppliers-segment"}>
                                <h3 className={"shopping-list-creation-step"}><span className={"shopping-list-creation-step-nb"}>1.</span> Sélectionnez vos fournisseurs</h3>
                                <SupplierList onSupplierSelected={this.selectSupplier}/>
                            </Segment>
                        </Grid.Column>

                        <Grid.Column tablet={16} computer={11}>
                            <h3 className={"shopping-list-creation-step"}>
                                <span className={"shopping-list-creation-step-nb"}>2. </span>
                                Sélectionnez les produits à ajouter à votre panier
                            </h3>

                            {disabledSuppliers.length > 0 &&
                            <Message warning>
                                <Message.Header>
                                    Attention, vous ne pouvez pas encore passer commande auprès des fournisseurs suivants:
                                </Message.Header>
                                <ul className="ui list">
                                    {disabledSuppliers.map((disabledSupplier, index) => <li key={index} className={"item"}>{disabledSupplier}</li>)}
                                </ul>
                                <p>
                                    <Link to={States.SEPA}>Vous pouvez suivre le statut de vos ouvertures de compte sur la page 'Mes fournisseurs'</Link>
                                </p>
                            </Message>
                            }

                            <OrderSupplierProducts supplierId={selectedSupplierId} promoOnly={true}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default connect(state => (
    {
        suppliers: state.frontState.suppliers,
        isMobileOrTablet: state.frontState.isMobileOrTablet
    }
))(SupplierOrderPromoSegment)