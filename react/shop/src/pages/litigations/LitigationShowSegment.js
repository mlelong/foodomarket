import React, {useEffect, useRef, useState} from "react";
import {Get, Post, Put} from "shop/utils/fetch";
import {withRouter} from "react-router";
import {generateUrl} from "shop/router";
import {connect} from "react-redux";
import {Button, Grid, Icon, Image, Message, Modal, Segment} from "semantic-ui-react";
import {processSubmitErrors} from "liform-react";
import LitigationTheme from "shop/form-theme/litigation/LitigationTheme";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {clearInitialValues} from "shop/actions";
import ActionHeader from "shop/utils/ActionHeader";
import Liform from "shop/utils/form/Liform";
import syncValidation from "shop/utils/form/syncValidation";
import LitigationCommentForm from "shop/components/LitigationCommentForm";

require("style/litigation.scss");

const LitigationShowSegment = props => {
    const [litigation, setLitigation] = useState(props.litigation);
    const [litigationCommentForm, setLitigationCommentForm] = useState(props.litigationCommentForm);
    const [isClosingTicket, setIsClosingTicket] = useState(false);
    const messagesEnd = useRef(null);
    const {isMobile, dispatch, history, match} = props;

    useEffect(() => {
        if (!litigationCommentForm || !litigation) {
            Get(generateUrl('app_front_litigation_show', {id: props.match.params.id})).then(data => {
                setLitigationCommentForm(data['litigation-comment-form']);
                setLitigation(data['litigation']);
            });
        } else if (messagesEnd.current !== null) {
            messagesEnd.current.scrollIntoView();
        }

        return () => {
            dispatch(clearInitialValues(['litigation', 'litigation-comment-form']));
        };
    }, [props.match.params.id, litigation]);

    const closeTicket = () => {
        setIsClosingTicket(true);

        Put(generateUrl('app_front_litigation_close', {id: match.params.id})).then(data => {
            setIsClosingTicket(false);

            if (data.ok) {
                history.push(States.LITIGATION);
            }
        }).catch(ignored => {
            setIsClosingTicket(false);
        });
    };

    return (
        litigation &&
        <Segment id="litigation-show-segment">
            <Grid>
                <ActionHeader
                    title={`Litige avec ${litigation.supplier}`}
                    actions={
                        <Button as={Link} to={States.LITIGATION} icon labelPosition="left">
                            <Icon name="arrow left"/>
                            Retour
                        </Button>
                    }
                />
            </Grid>

            <Message info>
                <Message.Content>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={litigation.status != 3 ? 10 : 16}>
                                <strong>Problème:</strong> {litigation.problem}<br/>
                                <strong>Demande:</strong> {litigation.demand}<br/>
                                <strong>Vous souhaitez être rappelé:</strong> {litigation.ask_to_be_called && "Oui" || 'Non'}
                            </Grid.Column>
                            {litigation.status != 3 &&
                            <Grid.Column width={6} textAlign="right">
                                <Button loading={isClosingTicket} positive onClick={closeTicket}>Clôturer</Button>
                            </Grid.Column>
                            }
                        </Grid.Row>
                    </Grid>
                </Message.Content>
            </Message>

            <Grid id={"comments"}>
                {litigation.comments.map((comment, key) => {
                    const pos = comment.fromType === 'client' ? 'right' : 'left';

                    return (
                        <Grid.Row key={key} className={'comment-row'}>
                            <Grid.Column width={15} floated={pos}
                                         textAlign={pos}>
                                <div className={`comment ${pos} comment-${comment.fromType}`}>
                                    <p dangerouslySetInnerHTML={{__html: comment.content}}></p>
                                    {comment.photos.map((photo, pkey) =>
                                        <Modal
                                            key={pkey}
                                            size='large'
                                            closeIcon
                                            trigger={<Image title={"Agrandir"} className='comment-picture-thumbnail'
                                                            inline={!isMobile} fluid={isMobile}
                                                            src={isMobile ? photo.thumbnail_mobile : photo.thumbnail}/>}
                                        >
                                            <Modal.Content image scrolling>
                                                <Image src={photo.original} wrapped/>
                                            </Modal.Content>
                                        </Modal>
                                    )}
                                    <div className='comment-from'>{comment.from}</div>
                                    <div className='comment-date'>{comment.date}</div>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    );
                })}

                <div ref={messagesEnd}></div>
            </Grid>

            {litigationCommentForm &&
            <Grid style={{marginTop: 0}}>
                <Liform
                    syncValidation={syncValidation(litigationCommentForm.schema)}
                    schema={litigationCommentForm.schema}
                    initialValues={litigationCommentForm.values}
                    baseForm={LitigationCommentForm}
                    theme={LitigationTheme}
                    formKey={'litigationComment'}
                    onSubmit={data => {
                        return Post(generateUrl('app_front_litigation_comment_add', {id: props.match.params.id}), data, 'sf-form').then(data => {
                            if (!data.ok) {
                                processSubmitErrors(data.form.errors);
                            } else {
                                setLitigationCommentForm(data.form);
                                setLitigation({...litigation, comments: [...litigation.comments, data.comment]});
                            }
                        });
                    }}
                />
            </Grid>
            }
        </Segment>
        || <React.Fragment/>
    );
};

export default withRouter(connect(state => ({
    litigation: state.frontState.initialValues['litigation'],
    litigationCommentForm: state.frontState.initialValues['litigation-comment-form'],
    isMobile: state.frontState.isMobile
}))(LitigationShowSegment));
