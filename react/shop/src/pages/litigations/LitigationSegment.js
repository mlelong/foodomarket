import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {Button, Grid, Icon, Segment, Table} from "semantic-ui-react";
import {connect} from "react-redux";
import {States} from "shop/constants/frontConstants";
import {generateUrl} from "shop/router";
import ActionHeader from "shop/utils/ActionHeader";
import {Get} from "shop/utils/fetch";

const LitigationSegment = props => {
    const [litigations, setLitigations] = useState(props.litigations);

    useEffect(() => {
        if (!litigations) {
            Get(generateUrl('app_front_litigation_list')).then(data => {
                setLitigations(data['litigations']);
            });
        }
    });

    return (
        <Segment id={"litigations-segment"}>
            <Grid>
                <ActionHeader
                    title={"Mes litiges"}
                    actions={
                        <Button as={Link} to={States.LITIGATION_NEW} icon primary className={"blue"} labelPosition="left">
                            <Icon name="plus"/>
                            Créer
                        </Button>}
                />
            </Grid>

            {litigations && litigations.length > 0 &&
            <Table celled columns={5}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Litige ouvert le</Table.HeaderCell>
                        <Table.HeaderCell>Grossiste</Table.HeaderCell>
                        <Table.HeaderCell>Problème</Table.HeaderCell>
                        <Table.HeaderCell>Statut</Table.HeaderCell>
                        <Table.HeaderCell>Action</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {litigations.map((litigation, key) =>
                        <Table.Row key={key} positive={litigation.class === 'positive'}
                                   negative={litigation.class === 'negative'}>
                            <Table.Cell>{litigation.created}</Table.Cell>
                            <Table.Cell>{litigation.supplier}</Table.Cell>
                            <Table.Cell>{litigation.problem}</Table.Cell>
                            <Table.Cell>{litigation.status}</Table.Cell>
                            <Table.Cell>
                                <Link to={States.LITIGATION_SHOW.replace(':id', litigation.id)}>
                                    Voir le détail
                                </Link>
                            </Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
            ||
            <div>Vous n'avez pas enregistré de litige.</div>
            }
        </Segment>
    );
};



export default connect(state => ({
    litigations: state.frontState.initialValues['litigations'],
}))(LitigationSegment);

