import Loadable from "react-loadable";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";
import Loading from "shop/components/Widget/Loading";

const litigationLoader = Loadable({
    loader: () => import('./LitigationSegment'),
    loading: Loading,
    delay: 500
});

const litigationNewLoader = Loadable({
    loader: () => import('./LitigationNewSegment'),
    loading: Loading,
    delay: 500
});

const litigationShowLoader = Loadable({
    loader: () => import('./LitigationShowSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.LITIGATION, true, litigationLoader);
addRoute(States.LITIGATION_SHOW, true, litigationShowLoader);
addRoute(States.LITIGATION_NEW, true, litigationNewLoader);
