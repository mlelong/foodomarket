import React, {useEffect, useState} from "react";
import {Button, Grid, Icon, Segment} from "semantic-ui-react";
import {generateUrl} from "shop/router";
import {connect} from "react-redux";
import {processSubmitErrors} from "liform-react";
import Liform from "shop/utils/form/Liform";
import BaseForm from "shop/form-theme/semanticui/BaseForm";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {withRouter} from "react-router";
import ActionHeader from "shop/utils/ActionHeader";
import {Get, Post} from "shop/utils/fetch";
import LitigationTheme from "shop/form-theme/litigation/LitigationTheme";

require("style/litigation.scss");

const LitigationNewSegment = (props) => {
    const [form, setForm] = useState(props.litigationForm);
    const {history} = props;

    useEffect(() => {
        if (!form) {
            Get(generateUrl('app_front_litigation_form')).then(data => {
                setForm(data);
            });
        }
    });

    return (
        <Segment>
            <Grid>
                <ActionHeader
                    title={"Création d'un nouveau litige"}
                    actions={
                        <Button as={Link} to={States.LITIGATION} icon labelPosition="left">
                            <Icon name="arrow left"/>
                            Retour
                        </Button>
                    }
                />
                <Grid.Row>
                    <Grid.Column>
                        {form &&
                            <Liform
                                schema={form.schema}
                                initialValues={form.values}
                                baseForm={BaseForm}
                                theme={LitigationTheme}
                                formKey={'newLitigation'}
                                onSubmit={formData => {
                                    return Post(generateUrl('app_front_litigation_add'), formData, 'sf-form').then(data => {
                                        if (!data.ok) {
                                            processSubmitErrors(data.form.errors);
                                        } else {
                                            setForm(null);
                                            history.push(States.LITIGATION_SHOW.replace(':id', data['litigationId']));
                                        }
                                    });
                                }}
                            />
                        }
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    );
};

export default withRouter(connect(state => ({
    litigationForm: state.frontState.initialValues['litigation-form']
}))(LitigationNewSegment));
