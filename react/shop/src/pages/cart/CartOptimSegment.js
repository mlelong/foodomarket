import React from "react";
import {connect} from "react-redux";
import {Button, Checkbox, Form, Image, List, Message, Segment, Table} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import entries from "shop/utils/entries";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {withRouter} from "react-router";
import {generateUrl} from "shop/router";

class CartOptimSegment extends React.Component
{
    state = {
        maxDeliveries: 1,
        computedDeliveries: 1,
        optimum: null,
        filterMode: false,
        originalTotal: 0,
        optimTotal: 0,
        shipping: [],
        loading: false,
        error: '',
        suppliers: [],
        selectedSuppliers: [],
        algo: 't'
    };

    componentDidMount() {
        Get(generateUrl('app_front_shopping_cart_optimize_suppliers'))
            .then(data => {
                this.setState({suppliers: data});
            }).catch(exception => {
                console.log(exception);
                alert(exception);
            })
        ;
    }

    optimize = () => {
        this.setState({loading: true});

        let selectedSuppliers = this.state.suppliers.reduce((p, c) => {if (c.checked) {p.push(c);} return p;}, []);

        if (selectedSuppliers.length === 0) {
            selectedSuppliers = this.state.suppliers;
        }

        const selectedSuppliersId = selectedSuppliers.map(s => s.id);

        Get(generateUrl('app_front_shopping_cart_optimize_result', {deliveries: this.state.maxDeliveries, suppliers: selectedSuppliersId, algo: this.state.algo}))
            .then(data => {
                this.setState({
                    error: data.hasOwnProperty('message') ? data.message : '',
                    computedDeliveries: this.state.maxDeliveries,
                    selectedSuppliers: selectedSuppliers,
                    optimum: data.data,
                    originalTotal: data.originalTotal,
                    filterMode: false,
                    optimTotal: data.optimTotal,
                    shipping: data.hasOwnProperty('shipping') ? data.shipping : [],
                    loading: false
                });
            }).catch(exception => {
                console.log(exception);
                alert(exception);
                this.setState({loading: false});
            })
        ;
    };

    handleChange = (event, {value}) => {
        this.setState({maxDeliveries: value});
    };

    continueWithoutOptim = () => {
        const {history} = this.props;

        history.push(States.ORDER_CART_SUMMARY);
    };

    continueWithOptim = () => {
        // Remove older carts and replace them with optim newer ones
        Get(generateUrl('app_front_shopping_cart_optimize'))
            .then((data) => {
                if (data.ok) {
                    // Force a server reload, because the cart is still unchanged in client
                    document.location = generateUrl('app_front_shopping_cart_page');
                } else {
                    console.log(data);
                    alert(data.message);
                }
            }).catch((exception) => {
                console.log(exception);
                alert(exception);
            })
        ;
    };

    renderShippingForSupplier = (shippings, supplier) => {
        const items = [];

        for (let shipping of shippings) {
            if (shipping.supplier === supplier) {
                items.push(shipping);
                break ;
            }
        }

        return (
            <List divided>
                {items.map((supplier, index) =>
                    <List.Item key={index}>
                        <List.Header>{supplier.supplier}</List.Header>
                        <List.Content>
                            <List divided>
                                <List.Item>
                                    Minimum de commande: <span className="right floated">{supplier.minOrder}€</span>
                                </List.Item>
                                <List.Item>
                                    Sous-total: <span className={"right floated"}>{supplier.subTotal}€</span>
                                </List.Item>
                                <List.Item>
                                    Livraison: <span className={"right floated"}>{supplier.shippingCost == 0 && supplier.defaultShipping != 0 && supplier.defaultShipping != 9999 && <del>{supplier.defaultShipping}€&nbsp;</del>}{supplier.shippingCost != 9999 && supplier.shippingCost || 0}€</span>
                                </List.Item>
                                <List.Item>
                                    Total: <span className={"right floated"}><b>{supplier.total}€</b></span>
                                </List.Item>
                            </List>
                        </List.Content>
                    </List.Item>
                )}
            </List>
        );
    };

    formatCell = (cell) => {
        if (Object.keys(cell).length === 0) {
            return {
                status: '',
                html: ''
            };
        }

        let hasChanged = false;
        let variantHasChanged = false;

        if (cell.hasOwnProperty('newId')) {
            variantHasChanged = cell.newId != cell.id;
            hasChanged = variantHasChanged || cell.quantity != cell.newQuantity || cell.unit != cell.newUnit;
        }

        if (hasChanged) {
            return {
                status: 'positive',
                html:
                    <span>
                        <del>{cell.quantity}{cell.unit} | {cell.price}€</del><br/>
                        {variantHasChanged && cell.newName && <br/>}
                        {cell.newQuantity}{cell.newUnit} | {cell.newPrice}€
                    </span>
            }
        }

        const showProductName = true;//!cell.hasOwnProperty('original');

        return {
            status: !cell.hasOwnProperty('optim') && 'warning' || !cell.hasOwnProperty('original') && 'positive',
            html:
                <span>
                    {showProductName && <span>{cell.name}</span>}
                    {showProductName && <br/>}
                    {cell.quantity}{cell.unit} | {cell.price}€
                </span>
        };
    };

    filter = (filterMode) => {
        this.setState({filterMode: filterMode});
    };

    renderOptim = () => {
        const {optimum, filterMode, originalTotal, optimTotal, shipping} = this.state;
        let hasOptim = optimum !== null && optimum.hasOwnProperty('optim');

        if (!hasOptim) {
            return <div></div>;
        }

        const suppliers = {};

        for (let [supplierId, supplierData] of entries(optimum.original)) {
            suppliers[supplierId] = supplierData.supplier;
        }

        for (let [supplierId, supplierData] of entries(optimum.optim.suppliers)) {
            suppliers[supplierId] = supplierData.supplier;
        }

        const headerCells = [];

        for (let [supplierId, supplierName] of entries(suppliers)) {
            headerCells.push(supplierName);
        }

        const rows = [];

        for (let [key, productArray] of entries(optimum.optim.products)) {
            const cells = [];
            let originalPicture = '';
            let hasDiff = false;

            for (let [supplierId, supplierName] of entries(suppliers)) {
                let cell = {};

                if (optimum.original.hasOwnProperty(supplierId) && optimum.original[supplierId].products.hasOwnProperty(key)) {
                    originalPicture = optimum.original[supplierId].products[key].picture;

                    cell.id = optimum.original[supplierId].products[key].id;
                    cell.original = true;
                    cell.name = optimum.original[supplierId].products[key].name;
                    cell.quantity = optimum.original[supplierId].products[key].quantity;
                    cell.unit = optimum.original[supplierId].products[key].unit;
                    cell.price = optimum.original[supplierId].products[key].price;
                    cell.total = optimum.original[supplierId].products[key].total;

                    for (let product of productArray) {
                        if (supplierName === product.supplier) {
                            cell.optim = true;
                            cell.newId = product.id;
                            cell.newName = product.name;
                            cell.newQuantity = product.quantity;
                            cell.newUnit = product.unit;
                            cell.newPrice = product.price;
                            cell.newTotal = product.total;

                            hasDiff = hasDiff || cell.newId != cell.id || cell.newQuantity != cell.quantity || cell.newUnit != cell.unit || cell.newPrice != cell.price;

                            break ;
                        }
                    }
                } else {
                    for (let product of productArray) {
                        if (supplierName === product.supplier) {
                            cell.id = product.id;
                            cell.optim = true;
                            cell.name = product.name;
                            cell.quantity = product.quantity;
                            cell.unit = product.unit;
                            cell.price = product.price;
                            cell.total = product.total;

                            hasDiff = true;

                            break ;
                        }
                    }
                }

                cells.push(cell);
            }

            if (filterMode && !hasDiff) {
                continue ;
            }

            rows.push(
                <Table.Row key={key}>
                    <Table.Cell textAlign={"center"} collapsing>
                        <Image src={originalPicture} inline/>
                    </Table.Cell>

                    {cells.map((cell, ckey) => {
                        const formattedCell = this.formatCell(cell);

                        return (
                            <Table.Cell key={ckey} positive={formattedCell.status === 'positive'} warning={formattedCell.status === 'warning'} textAlign={"center"}>
                                {formattedCell.html}
                            </Table.Cell>
                        );
                    })}
                </Table.Row>
            );
        }

        const savings = (typeof originalTotal === 'string' ? parseFloat(originalTotal.replace(' ', '')) : originalTotal)
            - (typeof optimTotal === 'string' ? parseFloat(optimTotal.replace(' ', '')) : optimTotal);

        const savingsPercent = 100 - ((typeof optimTotal === 'string' ? parseFloat(optimTotal.replace(' ', '')) : optimTotal) * 100
        / (typeof originalTotal === 'string' ? parseFloat(originalTotal.replace(' ', '')) : originalTotal));

        const originalShippings = {};
        const optimShippings = {};
        let originalIsValid = true;
        let optimIsValid = true;

        for (let supplier of headerCells) {
            for (let s of shipping.original) {
                if (s.supplier === supplier) {
                    originalShippings[supplier] = s;

                    if (originalIsValid) {
                        if (!s.isValid) {
                            originalIsValid = false;
                        }
                    }

                    break ;
                }
            }

            for (let s of shipping.optim) {
                if (s.supplier === supplier) {
                    optimShippings[supplier] = s;

                    if (originalIsValid) {
                        if (!s.isValid) {
                            optimIsValid = false;
                        }
                    }

                    break ;
                }
            }
        }

        return (
            <Table celled striped structured columns={Object.keys(suppliers).length + 1}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell textAlign={"center"} collapsing>
                            <Button primary={!filterMode} onClick={() => this.filter(!filterMode)}>{filterMode && 'Voir tout' || 'Voir les différences'}</Button>
                        </Table.HeaderCell>

                        {headerCells.map((cell, key) => <Table.HeaderCell key={key}>{cell}</Table.HeaderCell>)}
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {rows.map(row => row)}
                </Table.Body>

                <Table.Footer>
                    <Table.Row positive={savings < 0 && originalIsValid} warning={savings > 0 && originalIsValid} negative={!originalIsValid}>
                        <Table.Cell collapsing style={{fontSize: '16px'}} textAlign={"left"}>
                            <u>Panier actuel:</u><br/>
                            Total livraison comprise: <b>{originalTotal}€</b>
                            {!originalIsValid && <p><b>Le minimum de commande n'est pas atteint !</b></p>}
                        </Table.Cell>

                        {headerCells.map((supplier, key) => <Table.Cell key={key} positive={originalShippings.hasOwnProperty(supplier) && originalShippings[supplier].isValid} negative={originalShippings.hasOwnProperty(supplier) && !originalShippings[supplier].isValid}>{this.renderShippingForSupplier(shipping.original, supplier)}</Table.Cell>)}
                    </Table.Row>

                    <Table.Row positive={savings > 0 && optimIsValid} warning={savings < 0 && optimIsValid} negative={!optimIsValid}>
                        <Table.Cell collapsing style={{fontSize: '16px'}} textAlign={"left"}>
                            <u>Panier optimisé:</u><br/>
                            Total livraison comprise: <b>{optimTotal}€</b><br/>
                            {(originalIsValid || savings > 0) &&
                                <p>(soit <b>{Math.abs(savings).toFixed(2)}€ ({Math.abs(savingsPercent).toFixed(2)}%)</b> {savings > 0 && "d'économies" || "de perte"})</p>
                            }
                            {!optimIsValid && <p><b>Le minimum de commande n'est pas atteint !</b></p>}
                        </Table.Cell>

                        {headerCells.map((supplier, key) => <Table.Cell key={key} negative={optimShippings.hasOwnProperty(supplier) && !optimShippings[supplier].isValid}>{this.renderShippingForSupplier(shipping.optim, supplier)}</Table.Cell>)}
                    </Table.Row>

                    <Table.Row>
                        <Table.Cell colSpan={headerCells.length + 1} textAlign={"center"}>
                            <Button.Group>
                                <Button negative={savings > 0} positive={savings < 0} onClick={this.continueWithoutOptim}>Continuer avec votre panier actuel</Button>
                                <Button.Or text={"ou"}/>
                                <Button negative={savings < 0} positive={savings > 0} onClick={this.continueWithOptim}>Continuer avec le panier optimisé</Button>
                            </Button.Group>
                        </Table.Cell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        );
    };

    toggleSupplier = (event, {value: supplierId, checked}) => {
        const {suppliers} = this.state;

        for (let supplier of suppliers) {
            if (supplier.id == supplierId) {
                supplier.checked = checked;
            }
        }

        this.setState({suppliers});
    };

    downloadReport = () => {
        const {optimum} = this.state;

        const csvContent = "data:text/csv;charset=utf-8," + optimum.report.map(e=>e.join(",")).join("\n");
        const encodedUri = encodeURI(csvContent);
        const link = document.createElement("a");

        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "optim-report.csv");

        document.body.appendChild(link); // Required for FF

        link.click(); // This will download the data file named "optim-report.csv".
    };

    render() {
        const {isAppDev} = this.props;
        const {error, maxDeliveries, computedDeliveries, loading, optimum, suppliers, selectedSuppliers} = this.state;
        const hasError = maxDeliveries.length === 0 || maxDeliveries == 0;
        let hasOptim = optimum !== null && optimum.hasOwnProperty('optim');
        const selectedSupplierNames = [];

        for (let supplier of selectedSuppliers) {
            selectedSupplierNames.push(supplier.name);
        }

        return (
            <Segment>
                <Message info>
                    <Message.Header>
                        Pour optimiser vos achats, indiquez-nous d'abord combien de livraisons vous pouvez accepter pour votre commande
                    </Message.Header>
                    <Message.Content>
                        <Form style={{marginTop: '1em'}}>
                            <Form.Input error={hasError} label={"Nombre de livraisons maximum"} type={"number"} value={maxDeliveries} onChange={this.handleChange}/>

                            <Form.Group inline>
                                {suppliers.map((value, key) => <Form.Field key={key} inline><Checkbox onChange={this.toggleSupplier} label={value.name} value={value.id} checked={value.checked} /></Form.Field>)}
                            </Form.Group>

                            <Form.Input
                                label='Algo'
                                value={this.state.algo}
                                onChange={(e, {value}) => this.setState({algo: value})}
                            />

                            <Form.Button disabled={hasError} loading={loading} positive content={"Optimiser"} onClick={this.optimize}/>
                        </Form>

                        {hasOptim && isAppDev && <p>Temps de calcul: {optimum.time}s <Button onClick={this.downloadReport} secondary>Voir le rapport</Button></p>}
                    </Message.Content>
                </Message>

                {error.length > 0 &&
                <Message error>
                    <Message.Header>
                        Une erreur est survenue
                    </Message.Header>
                    <Message.Content>
                        {error}
                    </Message.Content>
                </Message>
                }

                {hasOptim && this.renderOptim()}

                {optimum !== null && !hasOptim &&
                <Message warning>
                    <Message.Header>Aucune optimisation trouvée</Message.Header>
                    <Message.Content>
                        Pour <b>{computedDeliveries} livraison{computedDeliveries > 1 && 's'}</b> avec <b>{selectedSupplierNames.join(', ')}</b>, aucun optimum n'a pu être trouvé.<br/>
                        Vous pouvez ré-essayer en augmentant le nombre de livraisons, ou <Link to={States.ORDER_CART_SUMMARY}>valider votre commande sans modification</Link>.
                    </Message.Content>
                </Message>
                }
            </Segment>
        );
    }
}

export default withRouter(connect(state => ({
    isMobile: state.frontState.isMobile,
    isAppDev: state.frontState.isAppDev,
}))(CartOptimSegment));