import React from "react";
import {connect} from "react-redux";
import {Accordion, Button, Grid, Header, Message, Segment, Image, Icon} from "semantic-ui-react";
import ShoppingCartSupplier from "shop/components/ShoppingCartSupplier";
import OrderConfirmationModal from "shop/components/OrderConfirmationModal";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {changeState, orderAll, orderSingle, scheduleOrder} from "shop/actions";
import {forEachSuppliersItems} from "shop/utils/shoppingItems";
import entries from "shop/utils/entries";
import {clearInitialValues} from "shop/actions";

const style = {
    checkIcon: {
        marginRight: 0,
        marginBottom: 20
    },
    successBox: {
        textAlign: 'center'
    }
}

class ShoppingCartSegment extends React.Component {
    state = {
        carts: this.props.shoppingCarts
    };

    static getDerivedStateFromProps(props, state) {
        if (props.shoppingCarts !== state.carts) {
            return {
                carts: props.shoppingCarts
            }
        }

        return null;
    }

    componentDidMount() {
        this.props.dispatch(changeState(States.ORDER_CART_SUMMARY));
    }

    onDateScheduled = (cartId, date) => {
        this.props.dispatch(scheduleOrder(cartId, date));
    };

    onSingleOrder = (cartId) => {
        for (let [supplierId, cart] of entries(this.state.carts)) {
            if (cart.id == cartId) {
                this.props.dispatch(orderSingle(cart));
                break;
            }
        }
    };

    onOrder = () => {
        this.props.dispatch(orderAll(this.state.carts));
    };

    componentWillUnmount() {
        this.props.dispatch(clearInitialValues(['paymentResult']));
    }

    render() {
        const {suppliers, paymentResult, transactionResult} = this.props;
        let canOrderAll = true;
        const panels = [];

        forEachSuppliersItems(this.state.carts, (panel, index) => {
            let currentSupplier;

            for (let supplier of suppliers) {
                if (supplier.supplierId == panel.supplierId) {
                    currentSupplier = supplier;
                    break;
                }
            }

            canOrderAll = canOrderAll && ShoppingCartSupplier._isScheduledDateValid(currentSupplier, panel.scheduledDate);

            panels.push(<ShoppingCartSupplier onDateScheduled={this.onDateScheduled} onOrder={this.onSingleOrder}
                                              key={index} index={index} data={panel} supplier={currentSupplier}/>);
        });

        const {orderEmails, isMobile, user} = this.props;
        let modal = '';

        if (orderEmails !== null && orderEmails !== undefined && orderEmails.length > 0) {
            modal = <OrderConfirmationModal emails={orderEmails}/>
        }

        const canUseArbitration = user.hasOwnProperty('arbitration') && user.arbitration;

        return (
            <Segment className={isMobile ? "mobile" : ""}>
                {modal}

                <Header className="content-title" as="h1">Mon panier</Header>

                <Grid columns={2}>
                    <Grid.Column>
                        <Link to={States.SUPPLIER_ORDER}>
                            <Button className={"back-to-shopping-lists"} content={"Retour aux mercuriales"}
                                    icon={"arrow left"} labelPosition={"left"}/>
                        </Link>
                    </Grid.Column>

                    <Grid.Column textAlign={"right"}>
                        {canUseArbitration &&
                        <Link to={States.SHOPPING_CART_OPTIMIZE}>
                            <Button color={"violet"} content={"Optimiser ma commande"} icon={"settings"}
                                    labelPosition={"right"}/>
                        </Link>
                        }
                    </Grid.Column>
                </Grid>

                <Grid container columns={1}>
                    {paymentResult === "00" && (
                        <Grid.Column>
                            <Message positive size='large' style={style.successBox}>
                                <Icon color="green" name="check circle outline" size="huge" style={style.checkIcon}/>
                                <Message.Header>Merci pour votre commande !</Message.Header>
                                <p>
                                    Votre compte bancaire ne sera débité qu'a l'émission de la facture par le
                                    fournisseur.
                                </p>
                            </Message>
                        </Grid.Column>
                    )}
                    {paymentResult !== "00" && paymentResult !== null && paymentResult !== undefined && (
                        <Grid.Column>
                            <Message negative size='large'>
                                <Message.Header>Oups !</Message.Header>
                                <p>
                                    Le paiement de votre commande n'a pas pu être validé, par conséquent, celle-ci a été
                                    annulée.
                                    Votre compte bancaire ne sera pas débité. Vous pouvez à nouveau tenter de passer
                                    commande.
                                </p>
                                {paymentResult === "51" && (
                                    <p>
                                        <strong>Cause de l'échéc</strong> : autorisation refusée pour cause de plafond
                                        dépassé.
                                    </p>
                                )}
                                {paymentResult === "05" && (
                                    <p>
                                        <strong>Cause de l'échéc</strong> : autorisation refusée suite à une erreur dans
                                        le cryptogramme visuel saisi.
                                    </p>
                                )}
                                {paymentResult !== "05" && paymentResult !== "51" && transactionResult === "REFUSED" && (
                                    <p>
                                        <strong>Cause de l'échéc</strong> : échec de l'autentification 3D Secure.
                                    </p>
                                )}
                                {paymentResult !== "05" && paymentResult !== "51" && transactionResult === "ABANDONED" && (
                                    <p>
                                        <strong>Cause de l'échéc</strong> : Vous avez quitté prématurément la page de paiement.
                                    </p>
                                )}
                                <p>
                                    Si le problème persiste, contactez-nous.
                                </p>
                            </Message>
                        </Grid.Column>
                    )}
                </Grid>

                <Accordion fluid={true} exclusive={false} className={"shopping-cart"}>
                    {panels}
                </Accordion>

                {/* <div id={"order-all-container"}>
                    <Button disabled={!canOrderAll} fluid className={"order-all-button"} content={"Passer toutes les commandes"} icon={"checkmark"} labelPosition={"left"} onClick={this.onOrder}/>
                </div>*/}
            </Segment>
        );
    }
}

export default connect(state => (
    {
        user: state.frontState.user,
        suppliers: state.frontState.suppliers,
        isMobile: state.frontState.isMobile,
        shoppingCarts: state.shoppingCarts,
        orderEmails: state.frontState.orderEmails,
        paymentResult: state.frontState.initialValues.paymentResult,
        transactionResult: state.frontState.initialValues.transactionResult,
    }
))(ShoppingCartSegment);
