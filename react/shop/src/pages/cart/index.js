import Loadable from "react-loadable";
import Loading from "shop/components/Widget/Loading";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";

const shoppingCartLoader = Loadable({
    loader: () => import('./ShoppingCartSegment'),
    loading: Loading,
    delay: 500
});

const cartOptimLoader = Loadable({
    loader: () => import('./CartOptimSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.ORDER_CART_SUMMARY, true, shoppingCartLoader);
addRoute(States.SHOPPING_CART_OPTIMIZE, true, cartOptimLoader);
