import React from "react";
import {Cell, Legend, Pie, PieChart, ResponsiveContainer, Tooltip} from "recharts";
import {connect} from "react-redux";

class CategorySpendPie extends React.PureComponent
{
    render() {
        const {data,isMobile} = this.props;
        const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#ff3da3', '#C0C0C0'];

        return (
            <ResponsiveContainer width={"100%"} height={"100%"} aspect={isMobile ? 1 : 1.7}>
                <PieChart>
                    <Pie
                        data={data}
                        nameKey={'name'}
                        dataKey={'amount'}
                        paddingAngle={0}
                        innerRadius={10}
                        outerRadius={80}
                        label={(d) => { return d.value.toFixed(2) + '€'; }}
                        labelLine={true}
                        isAnimationActive={true}
                    >
                        {
                            data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]}/>)
                        }
                    </Pie>
                    <Tooltip formatter={(value, name, props) => { return value.toFixed(2) + '€'; }}/>
                    <Legend />
                </PieChart>
            </ResponsiveContainer>
        );
    }
}

export default connect(state => ({
    isMobile: state.frontState.isMobile
}))(CategorySpendPie);