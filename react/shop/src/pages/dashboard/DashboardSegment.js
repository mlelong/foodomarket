import React from "react";
import {Button, Card, Grid, Header, Message, Segment} from "semantic-ui-react";
import SupplierSpendPie from "./SupplierSpendPie";
import {Get} from "shop/utils/fetch";
import PurchaseGraph from "./PurchaseGraph";
import TopProductPurchases from "./TopProductPurchases";
import moment from "moment";
import {DateInput} from "semantic-ui-calendar-react";
import CategorySpendPie from "./CategorySpendPie";
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {generateUrl} from "shop/router";

class DashboardSegment extends React.Component
{
    state = {
        supplierSpendData: [],
        purchases: [],
        topProductPurchases: [],
        totalCategory: [],
        hasData: false,
        mode: 'orders',
        dateStart: moment().subtract(1, 'month'),
        dateEnd: moment().add(1, 'day')
    };

    componentDidMount() {
        moment.locale('fr');

        this.load();
    }

    load = () => {
        const linkName = this.state.mode === 'invoices'
            ? 'app_front_dashboard_stats_invoices'
            : 'app_front_dashboard_stats_orders'
        ;

        Get(generateUrl(linkName, {start: this.state.dateStart.format('YYYY-MM-DD'), end: this.state.dateEnd.format('YYYY-MM-DD')}))
            .then((data) => {
                this.setState({
                    supplierSpendData: data.supplierSpend,
                    purchases: data.purchases,
                    topProductPurchases: data.topProductPurchases,
                    totalCategory: data.totalCategory,
                    hasData: data.purchases.length > 0
                })
            })
        ;
    };

    handleDateChange = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            this.setState({[name]: moment(value, 'DD/MM/YYYY')});
        }
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.dateStart != this.state.dateStart
            || prevState.dateEnd != this.state.dateEnd
            || prevState.mode != this.state.mode) {
            this.load();
        }
    }

    render() {
        const {suppliers} = this.props;
        const {
            supplierSpendData,
            purchases,
            topProductPurchases,
            totalCategory,
            hasData
        } = this.state;

        const items = [
            {
                header: 'Dépenses par fournisseur',
                description: <SupplierSpendPie data={supplierSpendData}/>
            },
            {
                header: 'Dépenses par catégorie',
                description: <CategorySpendPie data={totalCategory}/>
            },
            {
                header: 'Achats sur la période',
                description: <PurchaseGraph data={purchases}/>
            },
            {
                header: 'Top Produits',
                description: <TopProductPurchases data={topProductPurchases}/>
            },
        ];

        const minDate = moment(this.state.dateStart).add(1, 'day');
        const maxDate = moment(this.state.dateEnd).subtract(1, 'day');

        return (
            <Segment id={"dashboard-segment"}>
                <Header dividing size={"large"} textAlign={"center"}>Tableau de bord</Header>

                {suppliers.length <= 1 &&
                <div>
                    <Message info>
                        <Message.Content>
                            <Message.Header>Bienvenue dans votre espace client !</Message.Header>
                            <p>
                                Il ne contient pour l'instant aucun fournisseur, ceux-ci apparaîtront dès la validation de
                                vos informations par notre équipe.
                            </p>
                        </Message.Content>
                    </Message>
                </div>
                ||
                <Grid stackable>
                    <Grid.Row columns={1}>
                        <Grid.Column textAlign={"center"}>
                            <Button.Group>
                                <Button
                                    active={this.state.mode === 'orders'}
                                    positive={this.state.mode === 'orders'}
                                    onClick={() => {
                                        this.setState({mode: 'orders'})
                                    }}
                                >
                                    Commandes
                                </Button>
                                <Button.Or text='ou'/>
                                <Button
                                    active={this.state.mode === 'invoices'}
                                    positive={this.state.mode === 'invoices'}
                                    onClick={() => {
                                        this.setState({mode: 'invoices'})
                                    }}
                                >
                                    Factures
                                </Button>
                            </Button.Group>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <DateInput
                                key={Math.random()}
                                dateFormat="DD/MM/YYYY"
                                fluid
                                label="Date de début"
                                name="dateStart"
                                placeholder="Date de début"
                                value={this.state.dateStart.format('DD/MM/YYYY')}
                                initialDate={this.state.dateStart.format('DD/MM/YYYY')}
                                maxDate={maxDate.format('DD/MM/YYYY')}
                                icon={"calendar"}
                                onChange={this.handleDateChange}
                                closable={true}
                                inlineLabel={true}
                                closeOnMouseLeave={false}
                                popupPosition={"bottom center"}
                                localization={'fr'}
                                readOnly={true}
                            />
                        </Grid.Column>

                        <Grid.Column>
                            <DateInput
                                key={Math.random()}
                                dateFormat="DD/MM/YYYY"
                                fluid
                                name="dateEnd"
                                label="Date de fin"
                                placeholder="Date de fin"
                                value={this.state.dateEnd.format('DD/MM/YYYY')}
                                initialDate={this.state.dateEnd.format('DD/MM/YYYY')}
                                minDate={minDate.format('DD/MM/YYYY')}
                                icon={"calendar"}
                                onChange={this.handleDateChange}
                                closable={true}
                                inlineLabel={true}
                                closeOnMouseLeave={false}
                                popupPosition={"bottom center"}
                                localization={'fr'}
                                readOnly={true}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row columns={1}>
                        <Grid.Column>
                            {hasData
                            && <Card.Group itemsPerRow={2} items={items} stackable/>
                            ||
                            <div>
                                <Message info>
                                    <Message.Header>Bienvenue dans la boutique en ligne FoodoMarket</Message.Header>
                                    <p>
                                        C'est votre première visite ?<br/>
                                        Dès votre première commande, cette page vous affichera des statistiques sur
                                    </p>
                                    <ul>
                                        <li>vos dépenses par fournisseur</li>
                                        <li>vos dépenses par catégorie de produits</li>
                                        <li>votre top 5 produit</li>
                                        <li>votre courbe de dépenses journalières</li>
                                    </ul>
                                    <p>
                                        La période est définissable ci dessus. Par défaut, elle est paramétrée sur le
                                        mois en cours.
                                    </p>
                                </Message>

                                <Message>
                                    <Message.Header>Que faire ?</Message.Header>
                                    <p>
                                        Vous pouvez dès maintenant paramétrer des <NavLink target="_blank"
                                                                                           to={`${States.SHOPPING_LIST}${States.SHOPPING_LIST_CREATE}`}>listes
                                        d'achats</NavLink> pour une réutilisation ultérieure,<br/>
                                        ou <NavLink target="_blank" to={`${States.SUPPLIER_ORDER}`}>passer
                                        commande</NavLink> avec tous les catalogues fournisseur.
                                    </p>
                                </Message>
                            </div>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                }
            </Segment>
        );
    }
}

export default connect(state => ({
    suppliers: state.frontState.suppliers,
    firstConnection: state.frontState.initialValues.firstConnection
}))(DashboardSegment);