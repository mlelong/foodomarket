import React from "react";
import {Area, AreaChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";
import connect from "react-redux/es/connect/connect";

class PurchaseGraph extends React.PureComponent
{
    render() {
        const {data,isMobile} = this.props;

        return (
            <ResponsiveContainer width={"100%"} height={"100%"} aspect={isMobile ? 1 : 1.7}>
                <AreaChart data={data}>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="date"/>
                    <YAxis unit={"€"}/>
                    <Tooltip formatter={(value, name, props) => { return value.toFixed(2) + '€'; }}/>
                    <Area type='monotone' dataKey='Montant'  stroke='#0294ff' fill='#0294ff' opacity={0.5}/>
                </AreaChart>
            </ResponsiveContainer>
        );
    }
}

export default connect(state => ({
    isMobile: state.frontState.isMobile
}))(PurchaseGraph);