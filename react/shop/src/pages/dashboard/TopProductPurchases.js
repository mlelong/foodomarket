import React from "react";
import {connect} from "react-redux";
import {Table} from "semantic-ui-react";

class TopProductPurchases extends React.PureComponent
{
    render() {
        const {data} = this.props;

        return (
            <Table compact={"very"}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Produit</Table.HeaderCell>
                        <Table.HeaderCell>Quantité</Table.HeaderCell>
                        <Table.HeaderCell>Montant</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {data.map((product, index) =>
                        <Table.Row key={index}>
                            <Table.Cell>
                                {/*<NavLink to={States.PRODUCT_VARIANT.replace(':id', product.id).replace(':supplierId', product.supplierId)}>*/}
                                    {product.name}
                                {/*</NavLink>*/}
                            </Table.Cell>
                            <Table.Cell>{product.quantity.toFixed(2)}</Table.Cell>
                            <Table.Cell>{product.amount.toFixed(2)}€</Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
        );
    }
}

export default connect(state => ({
    isMobile: state.frontState.isMobile
}))(TopProductPurchases);
