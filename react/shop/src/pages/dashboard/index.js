import Loadable from "react-loadable";
import {States} from "shop/constants/frontConstants";
import {addRoute} from "shop/router/router";
import Loading from "shop/components/Widget/Loading";

const dashboardLoader = Loadable({
    loader: () => import('./DashboardSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.DASHBOARD_HOME, true, dashboardLoader);
