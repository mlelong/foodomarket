import React from "react";
import {connect} from "react-redux";
import {Button, Card, Grid, Header, Icon, Image, Message, Modal, Segment} from "semantic-ui-react";
import {Get} from "shop/utils/fetch";
import {Link, withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import Filters from "shop/components/Filters";
import {generateUrl} from "shop/router";

class SupplierCatalogSegment extends React.Component
{
    state = {
        supplierName: '',
        loading: true,
        products: [],
        filters: [],
        filteredProducts: [],
        filterModalOpen: false
    };

    componentDidMount() {
        const {match} = this.props;

        this.loadProducts(match.params.id);
    }

    loadProducts = (supplierId) => {
        this.setState({loading: true});

        return Get(generateUrl('app_front_supplier_catalog', {id: supplierId}))
            .then(data => {
                this.setState({
                    products: data.products,
                    filters: data.filters,
                    supplierName: data.supplierName,
                    loading: false
                });
            }).catch(exception => {
                console.log(exception);
                this.setState({loading: false});
            })
        ;
    };

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id != this.props.match.params.id) {
            this.loadProducts(this.props.match.params.id);
        }
    }

    render() {
        const {loading, filters, products, filteredProducts, filterModalOpen, supplierName} = this.state;
        const filteringCard = <Filters itemName={"produit"} itemNamePluralized={"produits"} filters={filters} results={products} onResultsFiltered={(filteredProducts) => {
            this.setState({filteredProducts});
        }}/>;

        return (
            <Segment id="catalog-segment" loading={loading}>
                <Grid>
                    <Grid.Row>
                        <Grid.Column computer={16} tablet={10} mobile={12}>
                            <Header as={'h4'}>
                                Catalogue du fournisseur <strong>{supplierName}</strong> :
                            </Header>
                        </Grid.Column>
                        <Grid.Column only={"tablet mobile"} tablet={6} mobile={4} textAlign={"right"}>
                            <Modal trigger={<a id="filter-trigger" href="#" onClick={(e) => {e.preventDefault(); this.setState({filterModalOpen: true})}}>Filtres</a>} closeIcon closeOnDocumentClick closeOnDimmerClick open={filterModalOpen} onClose={() => this.setState({filterModalOpen: false})}>
                                <Modal.Header>Filtrer les produits</Modal.Header>
                                <Modal.Content scrolling>
                                    {filteringCard}
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button primary fluid onClick={() => this.setState({filterModalOpen: false})}>
                                        Appliquer
                                    </Button>
                                </Modal.Actions>
                            </Modal>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                        <Grid.Column computer={4} only={"computer"}>
                            {filteringCard}
                        </Grid.Column>

                        <Grid.Column computer={12} mobile={16}>
                            {filteredProducts.length > 0 &&
                            <Card.Group itemsPerRow={4} doubling>
                                {filteredProducts.map(product =>
                                    <Card key={product.id} as={Link} to={States.PRODUCT.replace(':slug', product.slug)}>
                                        <Image fluid src={product.picture}/>
                                        <Card.Content>
                                            <Card.Header>{product.name}</Card.Header>
                                            <Card.Meta>{product.variants.length} offre{product.variants.length > 1 && 's'}</Card.Meta>
                                        </Card.Content>
                                        <Card.Content extra>
                                            <Icon name={"truck"}/>
                                            {product.suppliers.length} fournisseur{product.suppliers.length > 1 && 's'}
                                        </Card.Content>
                                    </Card>
                                )}
                            </Card.Group>
                            ||
                            <Message warning>
                                <Message.Header>Pas de résultat</Message.Header>
                                <p>Nous vous invitons à réessayer avec d'autres termes de recherche plus larges.</p>
                            </Message>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default withRouter(connect()(SupplierCatalogSegment));