import React from "react";
import {Button, Card, Grid, Header, Image, Segment} from "semantic-ui-react";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {Get} from "shop/utils/fetch";
import Filters from "shop/components/Filters";
import {States} from "shop/constants/frontConstants";
import {generateUrl} from "shop/router";

class SuppliersSegment extends React.Component
{
    state = {
        suppliers: [],
        filteredSuppliers: [],
        filters: []
    };

    componentDidMount() {
        Get(generateUrl('app_front_suppliers'))
            .then((data) => {
                if (data.ok) {
                    this.setState({suppliers: data.suppliers, filters: data.filters});
                }
            })
        ;
    }

    render() {
        const {suppliers, filteredSuppliers, filters} = this.state;

        const filteringCard = <Filters itemName={"fournisseur"} itemNamePluralized={"fournisseurs"} filters={filters} results={suppliers} onResultsFiltered={(filteredSuppliers) => {
            this.setState({filteredSuppliers});
        }}/>;

        return (
            <Segment>
                <Header dividing as={'h2'}>Mes fournisseurs</Header>

                <Grid>
                    <Grid.Column width={4}>
                        {filteringCard}
                    </Grid.Column>

                    <Grid.Column width={12}>
                        <Grid padded={false} stretched>
                            {filteredSuppliers.map((supplier, index) =>
                                <Grid.Row key={index}>
                                    <Grid.Column width={2}>
                                        <Image src={supplier.logo} fluid/>
                                    </Grid.Column>
                                    <Grid.Column width={11}>
                                        <Card fluid>
                                            <Card.Content>
                                                <Card.Header>
                                                    {supplier.name}
                                                </Card.Header>
                                                <Card.Meta>
                                                    {supplier.products} produits<br/>
                                                </Card.Meta>
                                            </Card.Content>
                                            <Card.Content extra>
                                                {supplier.categories}
                                            </Card.Content>
                                        </Card>
                                    </Grid.Column>
                                    <Grid.Column width={3}>
                                        <Button as={Link} to={States.SUPPLIER_CATALOG.replace(':id', supplier.id)} primary className={"blue"}>Voir le catalogue</Button>
                                    </Grid.Column>
                                </Grid.Row>
                            )}
                        </Grid>
                    </Grid.Column>
                </Grid>
            </Segment>
        );
    }
}

export default withRouter(connect()(SuppliersSegment));