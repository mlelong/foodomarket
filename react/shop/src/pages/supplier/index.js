import Loadable from "react-loadable";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";
import Loading from "shop/components/Widget/Loading";

const supplierLoader = Loadable({
    loader: () => import('./SuppliersSegment'),
    loading: Loading,
    delay: 500
});

const supplierCatalogLoader = Loadable({
    loader: () => import('./SupplierCatalogSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.SUPPLIERS, true, supplierLoader);
addRoute(States.SUPPLIER_CATALOG, true, supplierCatalogLoader);
