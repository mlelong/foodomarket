import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Grid, Header, Icon, List} from "semantic-ui-react";
import {Link, withRouter} from "react-router-dom";
import FrontInput from "shop/components/FrontInput";
import {Constants, States} from "shop/constants/frontConstants";
import getShoppingLists from "shop/utils/ShoppingListFetcher";
import ShoppingListItem from "./ShoppingListItem";
import {changeState, removeShoppingList} from "shop/actions";

class ShoppingListIndex extends Component
{
    state = {
        shoppingLists: []
    };

    componentDidMount() {
        this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: true });

        getShoppingLists((data) => {
            if (data !== null) {
                this.setState({shoppingLists: data.data});
            }

            this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: false });
        });

        this.props.dispatch(changeState(States.SHOPPING_LIST));
    }

    onFilterLists = (terms) => {
        this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: true });

        getShoppingLists((data) => {
            if (data !== null) {
                this.setState({shoppingLists: data.data});
            }

            this.props.dispatch({ type: Constants.LOADING_RESOURCE, loading: false });
        }, terms);
    };

    onRemoveList = (id) => {
        const that = this;

        this.props.dispatch(removeShoppingList(id, function(response) {
            if (response.ok) {
                that.setState({shoppingLists: that.state.shoppingLists.filter((e) => { return e.id != id; })});
            } else {
                const message = "Could not remove shopping list: " + response.message;

                console.log(message);
                alert(message);
            }
        }));
    };

    render() {
        const {match} = this.props;
        const {shoppingLists} = this.state;
        const shoppingListsElements = shoppingLists.map((list) =>
            <ShoppingListItem onRemove={this.onRemoveList} data={list} key={list.id} shoppingListId={list.id}/>
        );

        return (
            <div>
                <Header className="content-title" as="h1">Mes listes d'achats</Header>

                <Grid>
                    <Grid.Row>
                        <Grid.Column width={12}>
                            <FrontInput icon='search' placeholder="Filtrez vos listes d'achats..." onOk={this.onFilterLists}/>
                        </Grid.Column>
                        <Grid.Column width={4} only={"computer"} textAlign={"right"}>
                            <Link to={`${match.url}${States.SHOPPING_LIST_CREATE}`}>
                                <Button basic icon={"plus"} labelPosition={"left"} content={"Ajouter une liste"}/>
                            </Link>
                        </Grid.Column>
                        <Grid.Column width={4} only={"tablet mobile"} textAlign={"right"}>
                            <Link to={`${match.url}${States.SHOPPING_LIST_CREATE}`}>
                                <Button basic>
                                    <Icon name={"plus"}/>
                                </Button>
                            </Link>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <List className={"shopping-lists"}>
                                {shoppingListsElements}
                            </List>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

export default withRouter(connect()(ShoppingListIndex))