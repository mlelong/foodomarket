import React from "react";
import {Button, Grid, Header} from "semantic-ui-react";
import {connect} from "react-redux";
import ShoppingListSupplierProducts from "shop/components/ShoppingListSupplierProducts";
import {Link, withRouter} from "react-router-dom";
import {editShoppingList} from "shop/actions";
import SupplierList from "shop/components/SupplierList";
import {States} from "shop/constants/frontConstants";

class ShoppingListEdit extends React.Component {
    state = {
        selectedSupplierId: 0
    };

    selectSupplier = (supplierId) => {
        this.setState({selectedSupplierId: supplierId});
    };

    componentDidMount() {
        this.props.dispatch(editShoppingList(this.props.match.params.id));
    }

    render() {
        const {shoppingListName, match} = this.props;
        const {selectedSupplierId} = this.state;

        return (
            <div id={"shopping-list-edit"}>
                <Link to={States.SHOPPING_LIST}>
                    <Button className={"back-to-shopping-lists"} content={"Mes Listes D'achats"} icon={"arrow left"} labelPosition={"left"}/>
                </Link>

                <Header className="content-title" as="h1">Modification de &laquo;{shoppingListName}&raquo;</Header>

                <Grid stackable={true}>
                    <Grid.Row>
                        <Grid.Column tablet={16} computer={5}>
                            <h3 className={"shopping-list-creation-step"}><span className={"shopping-list-creation-step-nb"}>1.</span> Sélectionnez vos fournisseurs</h3>
                            <SupplierList onSupplierSelected={this.selectSupplier}/>
                        </Grid.Column>

                        <Grid.Column tablet={16} computer={11}>
                            <ShoppingListSupplierProducts shoppingListId={parseInt(match.params.id)} supplierId={selectedSupplierId} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

export default withRouter(connect(state => (
    {
        shoppingListName: state.frontState.shoppingListName
    }
))(ShoppingListEdit))