import React, {Component} from "react";
import PropTypes from "prop-types";
import {Grid, Icon, List} from "semantic-ui-react";
import {connect} from "react-redux";
import {States} from "shop/constants/frontConstants";
import {withRouter} from "react-router-dom";

class ShoppingListItem extends Component
{
    static propTypes = {
        shoppingListId: PropTypes.number.isRequired,
        onRemove: PropTypes.func,
    };

    onRemove = (e) => {
        e.preventDefault();

        const {shoppingListId, onRemove} = this.props;

        if (onRemove !== undefined) {
            onRemove(shoppingListId);
        }
    };

    onEdit = (e) => {
        e.preventDefault();

        const {match, history, shoppingListId} = this.props;

        if (match.path === States.PREVIOUS_ORDERS_LISTING) {
            history.push(States.ORDER_PREVIOUS_ORDER.replace(':date', this.props.data.date.split('/').reverse().join('-')));
        } else {
            // history.push(States.SHOPPING_LIST_ORDER.replace(':id', shoppingListId));
            history.push(`${match.url}${States.SHOPPING_LIST_EDIT.replace(':id', shoppingListId)}`);
        }
    };

    render() {
        const props = this.props.data;
        const supplierNames = props.supplierNames.join(', ');

        return (
            <List.Item>
                <Grid>
                    <Grid.Row className={"shopping-list-row"}>
                        <Grid.Column computer={7} as={"a"} only={"computer"} onClick={this.onEdit}>
                            <div>
                                <p className={"shopping-list-name"}>{props.listName}</p>
                            </div>
                        </Grid.Column>
                        <Grid.Column tablet={6} mobile={8} only={"tablet mobile"} as={"a"} onClick={this.onEdit}>
                            <div>
                                <p className={"shopping-list-name"}>{props.listName}</p>
                                <p className={"additional-infos"}>{props.nbItems} produit{props.nbItems > 1 && 's'}, {props.nbSuppliers} fournisseur{props.nbSuppliers && 's'}<br/>({supplierNames})</p>
                            </div>
                        </Grid.Column>
                        <Grid.Column tablet={6} computer={5} only={"tablet computer"} as={"a"} onClick={this.onEdit} className={"additional-infos"}>
                            <div>
                                <p>{props.nbItems} produit{props.nbItems > 1 && 's'}, {props.nbSuppliers} fournisseur{props.nbSuppliers && 's'}<br/>({supplierNames})</p>
                            </div>
                        </Grid.Column>

                        <Grid.Column className="shopping-list-col-edit" computer={2} tablet={2} mobile={4} onClick={this.onEdit}>
                            <div>
                                <Icon name="edit"/><br/>&Eacute;diter
                            </div>
                        </Grid.Column>
                        <Grid.Column className="shopping-list-col-remove" computer={2} tablet={2} mobile={4} onClick={this.onRemove}>
                            <div>
                                <Icon name="trash"/><br/>Supprimer
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </List.Item>
        );
    }
}

export default withRouter(connect()(ShoppingListItem))