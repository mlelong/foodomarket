import React from "react";
import {Route, withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {Segment} from "semantic-ui-react";
import ShoppingListIndex from "./ShoppingListIndex";
import ShoppingListEdit from "./ShoppingListEdit";
import ShoppingListCreate from "./ShoppingListCreate";

export const ShoppingListSegment = props =>
{
    const {match} = props;

    return (
        <Segment id={"shopping-list-segment"}>
            <Route exact path={`${match.path}`} component={ShoppingListIndex}/>
            <Route exact path={`${match.path}${States.SHOPPING_LIST_CREATE}`} component={ShoppingListCreate}/>
            <Route exact path={`${match.path}${States.SHOPPING_LIST_EDIT}`} component={ShoppingListEdit}/>
        </Segment>
    );
};

export default withRouter(ShoppingListSegment);
