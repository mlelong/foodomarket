import Loadable from "react-loadable";
import {States} from "shop/constants/frontConstants";
import {addRoute} from "shop/router/router";
import Loading from "shop/components/Widget/Loading";

const shoppingListLoader = Loadable({
    loader: () => import('./ShoppingListSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.SHOPPING_LIST, false, shoppingListLoader);
