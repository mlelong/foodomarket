import Loadable from "react-loadable";
import Loading from "shop/components/Widget/Loading";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";

const error404Loader = Loadable({
    loader: () => import('./Error404Segment'),
    loading: Loading,
    delay: 500
});

const error500Loader = Loadable({
    loader: () => import('./Error500Segment'),
    loading: Loading,
    delay: 500
});

addRoute(States.ERROR, false, error500Loader);
addRoute(null, false, error404Loader);
