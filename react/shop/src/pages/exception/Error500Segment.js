import React, {useState} from "react";
import {Grid, Icon, Message, Segment} from "semantic-ui-react";
import {connect} from "react-redux";
import TopProductsWidget from "shop/components/Widget/TopProductsWidget";
import CategorySlider from "shop/components/Widget/CategorySlider";

function Error500Segment(props)
{
    const [exception] = useState(props.exception);
    const subject = encodeURIComponent("Erreur sur le site foodomarket");
    const body = exception ? encodeURIComponent(`Sur la page: ${exception.uri} \n\n J'ai rencontré l'erreur: \n\n [${exception.code}] => ${exception.message} \n`) : '';

    return (
        <Segment id={"error-segment"}>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Message icon negative>
                            <Icon name={"frown outline"}/>
                            <Message.Content>
                                <Message.Header>Le serveur a rencontré une erreur</Message.Header>

                                {exception &&
                                <div>
                                    <p>Code: {exception.code}</p>
                                    <p>Erreur: {exception.message}</p>
                                </div>
                                }

                                <div>
                                    <a href={`mailto:hello@foodomarket.com?subject=${subject}&body=${body}`}>Cliquer ici pour nous envoyer un mail de rapport</a>
                                </div>
                            </Message.Content>
                        </Message>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <CategorySlider/>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <TopProductsWidget/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    );
}

export default connect(state => ({
    exception: state.frontState.initialValues.exception
}))(Error500Segment);