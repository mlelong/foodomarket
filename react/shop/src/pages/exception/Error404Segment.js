import React from "react";
import {Grid, Icon, Message, Segment} from "semantic-ui-react";
import TopProductsWidget from "shop/components/Widget/TopProductsWidget";
import CategorySlider from "shop/components/Widget/CategorySlider";

function Error404Segment()
{
    return (
        <Segment id={"not-found-segment"}>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Message icon negative>
                            <Icon name={"frown outline"}/>
                            <Message.Content>
                                <Message.Header>La page que vous avez demandé n'existe pas ou a été supprimée.</Message.Header>
                            </Message.Content>
                        </Message>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <CategorySlider/>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <TopProductsWidget/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    );
}

export default Error404Segment;