import React from "react";

const LegalNotice = () => {
    return (
        <div className="ui grid content-section">
            <div className="one column row">
                <div className="column">
                    <h1>Mentions Légales</h1>

                    <h2>Informations juridiques</h2>

                    <p>
                        SOCIETE ENMB<br/>
                        8 rue de l'est<br/>
                        92100 Boulogne Billancourt<br/>
                        France
                    </p>

                    <p>SIRET : 830 160 248 000 27</p>

                    <p>Forme juridique : SAS</p>

                    <h2>Crédits</h2>

                    <p>
                        Création du site et développement Web : ENMB<br/>
                        Graphisme et design : ENMB<br/>
                        Photos : ENMB<br/>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default LegalNotice;
