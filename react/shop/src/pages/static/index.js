import Loadable from "react-loadable";
import {addRoute} from "shop/router/router";
import {States} from "shop/constants/frontConstants";
import Loading from "shop/components/Widget/Loading";

const staticLoader = Loadable({
    loader: () => import('./StaticSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.ABOUT_US, true, staticLoader);
addRoute(States.LEGAL_NOTICE, true, staticLoader);
addRoute(States.PRIVACY_POLICY, true, staticLoader);
