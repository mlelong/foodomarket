import React from "react";

const AboutUs = () => {
    return (
        <div className="ui grid content-section">
            <div className="one column row">
                <div className="column row-left">
                    <h1>Qui sommes nous ?</h1>

                    <p>
                        Nous sommes des restaurateurs boulonnais et avons créé FoodoMarket : une plateforme
                        collaborative pour comparer et optimiser les achats des restaurateurs et professionnels de l'alimentation.
                    </p>
                    <p>
                        Que vous ayez un restaurant, un traiteur, un bar, un primeur ou même un supermarché,
                        FoodoMarket vous permet d'accéder aux meilleures offres de nos grossistes partenaires et
                        ainsi vous fournir en produits de qualité optimale, à des prix inférieurs.
                    </p>

                    <p>
                        Un gain de temps et d'argent indispensable pour vous concentrer sur votre objectif
                        initial : régaler et satisfaire vos clients !
                    </p>

                    <h1>Comment ça marche ?</h1>

                    <p>
                        Foodomarket vous met en relation avec le grossiste dont l'offre est la plus adaptée à
                        vos besoins. Vos habitudes de commande et de logistique ne changent pas : le fournisseur
                        livre la marchandise directement dans votre cuisine ou local !
                    </p>
                </div>
            </div>
        </div>
    );
};

export default AboutUs;
