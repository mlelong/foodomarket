import React from "react";

const PrivacyPolicy = () => {
    return (
        <div className="ui grid content-section">
            <div className="one column row">
                <div className="column row-left">
                    <h1>Politique de confidentialité</h1>
                    <p>
                        Devant le développement des nouveaux outils de communication, il est nécessaire de porter
                        une attention particulière à la protection de la vie privée.
                        C'est pourquoi nous nous engageons à respecter la confidentialité des informations que nous
                        collectons.
                    </p>
                    <hr/>
                    <h2>Collecte des informations</h2>
                    <p>
                        Nous collectons les informations suivantes :
                    </p>
                    <ul>
                        <li>Société</li>
                        <li>Nom</li>
                        <li>Prénom</li>
                        <li>Adresse électronique</li>
                        <li>Numéro de téléphone</li>
                    </ul>

                    <p>
                        Les informations que nous collectons sont recueillies par des formulaires et grâce à
                        l'interactivité établie entre vous et notre site Web.
                        Nous utilisons également, comme indiqué dans la section suivante, des fichiers témoins
                        et/ou journaux
                        pour réunir des informations vous concernant.
                    </p>
                    <hr/>
                    <h2>Formulaires&nbsp; et interactivité:</h2>
                    <p>
                        Vos informations personnelles sont collectées par le biais de formulaires, à savoir
                        :
                    </p>
                    <ul>
                        <li>Formulaire d'inscription au site web</li>
                        <li>Formulaire d'évaluation</li>
                        <li>Formulaire de commande</li>
                    </ul>
                    <p>
                        Nous utilisons les informations ainsi collectés pour les finalités suivantes :
                    </p>
                    <ul>
                        <li>Suivi de la commande</li>
                        <li>Informations / Offres promotionnelles</li>
                        <li>Statistiques</li>
                        <li>Contact</li>

                    </ul>
                    <hr/>
                    <h2>Droit d'opposition et de retrait</h2>
                    <p>
                        Nous nous engageons à vous offrir un droit d'opposition et de retrait quant à
                        vos informations.<br/>
                        Le droit d'opposition s'entend comme étant la possibilité offerte aux
                        internautes de refuser que leurs informations
                        personnelles soient utilisées à certaines fins mentionnées lors de la
                        collecte.<br/>
                    </p>
                    <p>
                        Le droit de retrait s'entend comme étant la possibilité offerte aux internautes
                        de demander à ce que leurs
                        informations ne figurent plus, par exemple, dans une liste de diffusion.<br/>
                    </p>
                    <p>
                        Pour pouvoir exercer ces droits, vous pouvez nous envoyer un email à
                        hello@foodomarket.com<br/>
                    </p>
                    <hr/>
                    <h2>Droit d'accès</h2>
                    <p>
                        Nous nous engageons à reconnaître un droit d'accès et de rectification aux
                        personnes
                        concernées désireuses de consulter, modifier, voire radier les informations
                        les concernant.<br/>
                    </p>

                    <p>
                        Pour pouvoir exercer ces droits, vous pouvez nous envoyer un email à
                        hello@foodomarket.com<br/>
                    </p>
                    <hr/>
                    <h2>Sécurité</h2>
                    <p>

                        Les informations personnelles que nous collectons sont conservées
                        dans un environnement sécurisé. Les personnes travaillant pour nous sont
                        tenues de respecter la confidentialité de vos informations.</p>
                    <p>Pour assurer la sécurité de vos informations personnelles, nous avons
                        recours aux mesures suivantes :
                    </p>
                    <ul>
                        <li>Gestion des accès - personne autorisée</li>
                        <li>Gestion des accès - personne concernée</li>
                        <li>Sauvegarde informatique</li>
                        <li>Identifiant / mot de passe</li>
                        <li>Pare-feu (Firewalls)</li>
                    </ul>

                    <p>
                        Nous nous engageons à maintenir un haut degré de confidentialité en
                        intégrant les dernières innovations technologiques permettant d'assurer
                        la confidentialité de vos transactions. Toutefois, comme aucun mécanisme
                        n'offre une sécurité infaillible, une part de risque est toujours
                        présente
                        lorsque l'on utilise Internet pour transmettre des informations.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default PrivacyPolicy;
