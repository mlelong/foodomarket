import React from "react";
import {withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {Segment} from "semantic-ui-react";
import AboutUs from "./AboutUs";
import LegalNotice from "./LegalNotice";
import PrivacyPolicy from "./PrivacyPolicy";
import {Helmet} from "react-helmet-async";

const StaticSegment = (props) => {
    const {location} = props;
    let component, title;

    switch (location.pathname) {
        case States.ABOUT_US:
            component = <AboutUs/>;
            title = 'Qui sommes-nous';
            break ;
        case States.LEGAL_NOTICE:
            component = <LegalNotice/>;
            title = 'Mentions légales';
            break ;
        case States.PRIVACY_POLICY:
            component = <PrivacyPolicy/>;
            title = 'Politique de confidentialité';
            break ;
    }

    return (
        <Segment id={"static-segment"}>
            <Helmet
                titleTemplate="%s | FoodoMarket.com"
            >
                <title children={title}/>
                <meta name="description"
                      content="Professionnels de la restauration, achetez vos produits alimentaires au meilleur prix sur FoodoMarket. Livraison en A pour B dans toute l'île-de-France."/>
            </Helmet>

            {component}
        </Segment>
    );
};

export default withRouter(StaticSegment);
