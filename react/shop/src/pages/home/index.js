import Loadable from "react-loadable";
import {States} from "shop/constants/frontConstants";
import {addRoute} from "shop/router/router";
import Loading from "shop/components/Widget/Loading";

const homeLoader = Loadable({
    loader: () => import('./HomeSegment'),
    loading: Loading,
    delay: 500
});

addRoute(States.VISITOR_HOME, true, homeLoader);
