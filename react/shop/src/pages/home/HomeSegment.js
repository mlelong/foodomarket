import React from "react";
import {Card, Embed, Grid, Header, Icon, Image, Segment, Message} from "semantic-ui-react";
import jei from "img/logo-jei.jpg";
import bpi from "img/logo-bpi-france-2.png";
import idf from "img/logo-idf.png";
import videoPlaceholder from "img/video-placeholder.png";
import {Helmet} from "react-helmet-async";
import TopProductsWidget from "shop/components/Widget/TopProductsWidget";
import CategorySlider from "shop/components/Widget/CategorySlider";
import PromosWidget from "shop/components/Widget/PromosWidget";
import {connect} from "react-redux";

export class HomeSegment extends React.Component
{
    render() {
        const { isAvalaible } = this.props;

        return (
            <Segment id={"home-segment"}>
                <Helmet
                    titleTemplate="%s | FoodoMarket.com"
                >
                    <title>Le marché en ligne des restaurateurs</title>
                    <meta name="description"
                          content="Professionnels de la restauration, achetez vos produits alimentaires au meilleur prix sur FoodoMarket. Livraison en A pour B dans toute l'île-de-France."/>
                </Helmet>
                <Grid>
                    <Grid.Column width={16}>
                        <Header as={'h1'} style={{textAlign: 'center'}}>Le marché en ligne des restaurateurs<br/><span style={{fontSize: '17px'}}>et professionnels de l'alimentaire</span></Header>


                        <Grid padded={false}>
                            <Grid.Column width={16}>
                                <CategorySlider/>
                            </Grid.Column>

                            <Grid.Column width={16}>
                                <TopProductsWidget/>
                            </Grid.Column>

                            <Grid.Column width={16}>
                                <PromosWidget/>
                            </Grid.Column>

                            <Grid.Column width={16}>
                                <Header dividing as={'h2'}>Comment ça marche ?</Header>
                            </Grid.Column>

                            <Grid.Column width={16}>
                                <Card.Group itemsPerRow={3} doubling centered>
                                    <Card>
                                        <Card.Content style={{textAlign: 'center'}}>
                                            <Icon name={"handshake"} size="huge" color="olive"/>
                                        </Card.Content>
                                        <Card.Content style={{textAlign: 'center', fontSize: '16px'}}>
                                            Nous vous proposons les meilleurs fournisseurs en fonction de votre profil.
                                        </Card.Content>
                                    </Card>

                                    <Card>
                                        <Card.Content style={{textAlign: 'center'}}>
                                            <Icon name={"balance"} size="huge" color="green"/>
                                        </Card.Content>
                                        <Card.Content style={{textAlign: 'center', fontSize: '16px'}}>
                                            Vous passez vos commandes auprès des fournisseurs aux conditions préférentielles négociées.
                                        </Card.Content>
                                    </Card>

                                    <Card>
                                        <Card.Content style={{textAlign: 'center'}}>
                                            <Icon name={"chart line"} size="huge" color="teal"/>
                                        </Card.Content>
                                        <Card.Content style={{textAlign: 'center', fontSize: '16px'}}>
                                            Nous contrôlons les évolutions tarifaires quotidiennement.
                                        </Card.Content>
                                    </Card>
                                </Card.Group>
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>

                    <Grid.Column width={16}>
                        <Header as={'h2'} dividing>Nos outils</Header>

                        <p style={{textAlign: 'center', fontSize: '18px'}}>
                            Vous pouvez passer vos commandes par téléphone ou par email comme vous avez l'habitude de le faire,
                            mais si vous désirez bénéficier de nos contrôles automatiques sur les tarifs ainsi que nos suggestions intelligentes pour optimiser vos achats,
                            c'est très simple, utilisez notre plateforme comme Sophie ci-dessous :
                        </p>

                        <Embed id={"YQhBzWu7f2Q"} icon={"play circle"} source={"youtube"} placeholder={videoPlaceholder}/>
                    </Grid.Column>

                    <Grid.Column width={16}>
                        <Header as={'h2'} dividing>&Eacute;lue entreprise innovante</Header>

                        <Grid centered>
                            <Grid.Column widescreen={10} largeScreen={14} mobile={16}>
                                <Card.Group itemsPerRow={3} id={"partners-group"}>
                                    <Card as={'a'} href={"http://www.enseignementsup-recherche.gouv.fr/cid5738/la-jeune-entreprise-innovante-j.e.i.html"} target="_blank">
                                        <Card.Content>
                                            <Image src={jei}/>
                                        </Card.Content>
                                    </Card>

                                    <Card as={'a'} href={"https://www.bpifrance.fr/"} target="_blank">
                                        <Card.Content>
                                            <Image src={bpi}/>
                                        </Card.Content>
                                    </Card>

                                    <Card as={'a'} href={"https://www.iledefrance.fr/"} target="_blank">
                                        <Card.Content>
                                            <Image src={idf}/>
                                        </Card.Content>
                                    </Card>
                                </Card.Group>
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>
                </Grid>
            </Segment>
        );
    }
}

export default connect(state => (
    {
        isAvalaible: state.frontState.isAvalaible
    }
))(HomeSegment);
