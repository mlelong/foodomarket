import {applyMiddleware, compose, createStore} from "redux";
import {routerMiddleware} from "connected-react-router";
import thunkMiddleware from "redux-thunk";
import reducers, {initialStates} from "shop/reducers";
import history from "./createHistory";

export default function configureStore(props, context) {
    // This is how we get initial props from Symfony into redux.
    const {authToken, initialValues} = props;
    const {frontState, shoppingCarts, debugState} = initialStates;
    const {shoppingCarts: serverShoppingCarts, frontState: serverFrontState, debugState: serverDebugState} = props.initialStates;

    const isMobileOrTablet = serverFrontState.isMobile || serverFrontState.isTablet;
    const showPanels = authToken && !isMobileOrTablet;

    // Redux expects to initialize the store using an Object
    const initialState = {
        frontState: {
            ...frontState,
            ...serverFrontState,
            isMobileOrTablet: isMobileOrTablet,
            leftPanelVisibility: showPanels,
            rightPanelVisibility: showPanels,
            authToken,
            initialValues
        },
        shoppingCarts: {
            ...shoppingCarts,
            ...serverShoppingCarts
        },
        debugState: {
            ...debugState,
            ...serverDebugState
        }
    };

    // use devtools if we are in a browser and the extension is enabled
    let composeEnhancers = typeof(window) !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        reducers(history),
        initialState,
        composeEnhancers(
            applyMiddleware(
                routerMiddleware(history),
                thunkMiddleware
            )
        )
    );
}
