import React from "react";
import ReactOnRails from "react-on-rails";
import {Provider} from "react-redux";
import App from "shop/layout/App";
import {BrowserRouter} from "react-router-dom";
import {ConnectedRouter} from "connected-react-router";
import history from "shop/store/createHistory";
import {HelmetProvider} from "react-helmet-async";
import ScrollToTop from "react-router-scroll-top";
import Loadable from "react-loadable";
import ReactDOM from "react-dom";
import ReactGA from "react-ga";
import ReactPixel from "react-facebook-pixel";
import {ApolloProvider} from '@apollo/react-hooks';
import ApolloClient, {InMemoryCache} from 'apollo-boost';
import {FetchWithReconnect} from "shop/utils/fetch";

const ClientApp = (initialProps, context, domNodeId) => {
    const store = ReactOnRails.getStore('frontStore');
    const {base, graphqlUri} = context;
    const supportsHistory = 'pushState' in window.history;

    // Init google analytics
    ReactGA.initialize([
        {
            trackingId: 'UA-109839249-1',
            gaOptions: {
                name: 'analytics',
                siteSpeedSampleRate: 100
            }
        },
    ], { debug: false, alwaysSendToDefaultTracker: true });

    // Init facebook pixel
    ReactPixel.init('216691788895045');

    const client = new ApolloClient({
        cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
        uri: graphqlUri,
        credentials: 'same-origin',
        fetch: FetchWithReconnect
    });

    Loadable.preloadReady().then(() => {
        const reactElement = (
            <ApolloProvider client={client}>
                <HelmetProvider>
                    <Provider store={store}>
                        <ConnectedRouter history={history}>
                            <BrowserRouter basename={`${base}/shop`} forceRefresh={!supportsHistory}>
                                <ScrollToTop>
                                    <App/>
                                </ScrollToTop>
                            </BrowserRouter>
                        </ConnectedRouter>
                    </Provider>
                </HelmetProvider>
            </ApolloProvider>
        );

        ReactDOM.hydrate(reactElement, document.getElementById(domNodeId));
    });
};

export default ClientApp;
