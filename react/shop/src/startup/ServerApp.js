import React from "react";
import ReactDOMServer from "react-dom/server";
import {StaticRouter} from "react-router";
import {HelmetProvider} from "react-helmet-async";
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import history from "shop/store/createHistory";
import ScrollToTop from "react-router-scroll-top";
import Loadable from "react-loadable";
import App from "shop/layout/App";
import {getBundles} from "react-loadable-ssr-addon";
import manifest from "dist/manifest";
import ReactOnRails from "react-on-rails";
import {ApolloProvider} from '@apollo/react-hooks';
import {InMemoryCache} from 'apollo-boost';
import {ApolloClient} from 'apollo-client';
import {getDataFromTree} from "@apollo/react-ssr";
import {createHttpLink} from 'apollo-link-http';

const ServerApp = (props, railsContext) => {
    const store = ReactOnRails.getStore('frontStore');
    const {location, base, graphqlUri} = railsContext;
    const context = {};
    const helmetContext = {};
    const modules = [];

    const client = new ApolloClient({
        ssrMode: true,
        // Remember that this is the interface the SSR server will use to connect to the
        // API server, so we need to ensure it isn't firewalled, etc
        link: createHttpLink({
            uri: graphqlUri,
            credentials: 'same-origin',
            headers: {
                cookie: railsContext.cookie,
            },
        }),
        cache: new InMemoryCache(),
    });

    const component = (
        <ApolloProvider client={client}>
            <HelmetProvider context={helmetContext}>
                <Provider store={store}>
                    <ConnectedRouter history={history}>
                        <StaticRouter basename={`${base}/shop`} location={location} context={context}>
                            <ScrollToTop>
                                <Loadable.Capture report={moduleName => modules.push(moduleName)}>
                                    <App/>
                                </Loadable.Capture>
                            </ScrollToTop>
                        </StaticRouter>
                    </ConnectedRouter>
                </Provider>
            </HelmetProvider>
        </ApolloProvider>
    );

    // First fetch GraphQL query data and then do the render
    return getDataFromTree(component).then(() => {
        const apolloState = client.extract();
        const renderedHtml = {
            componentHtml: ReactDOMServer.renderToString(component)
        };

        const modulesToBeLoaded = [...modules, ...manifest.entrypoints];
        const bundles = getBundles(manifest, modulesToBeLoaded);

        const {helmet} = helmetContext;

        renderedHtml.title = helmet.title.toString();
        renderedHtml.meta = helmet.meta.toString();

        const styles = bundles.css || [];
        const scripts = bundles.js || [];

        // Adds the dynamically loaded scripts and styles to the html response
        // so that it can be loaded by the client on first page load instead of asynchronously fetching them, causing a delay
        renderedHtml.scripts = scripts.map(bundle => `<script type="text/javascript" src="${bundle.publicPath}"></script>`);
        renderedHtml.styles = styles.map(bundle => `<link rel="stylesheet" type="text/css" href="${bundle.publicPath}"/>`);

        // Pour le SSR de Apollo GraphQL, met le résultat des requêtes GraphQL récupéré lors du rendu ci dessus à disposition du client
        renderedHtml.scripts.unshift(`<script type="text/javascript">window.__APOLLO_STATE__=${JSON.stringify(apolloState).replace(/</g, '\\\u003c')};</script>`);

        return {renderedHtml};
    });
};

export default ServerApp;
