import ReactOnRails from "react-on-rails";
import ShopApp from "./ClientApp";
import configureStore from "shop/store/FrontStore";

const frontStore = configureStore;

ReactOnRails.registerStore({ frontStore });
ReactOnRails.register({ ShopApp });
