import ReactOnRails from "react-on-rails";
import configureStore from "shop/store/FrontStore";
import ShopApp from "./ServerApp";

const frontStore = configureStore;

ReactOnRails.registerStore({ frontStore });
ReactOnRails.register({ ShopApp });
