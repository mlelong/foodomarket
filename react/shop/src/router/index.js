// Export these functions that are needed to register a new route (addRoute)
// and to generate url from symfony route name (generateUrl)
export {addRoute, generateUrl} from "./router";
