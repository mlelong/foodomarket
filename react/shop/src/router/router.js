import FosJSRouting from "../../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js";
import fos_js_routes from "../../../../web/js/fos_js_routes.json";

FosJSRouting.setRoutingData(fos_js_routes);

export const routes = [];

/**
 * Ajoute une nouvelle route
 *
 * @param {string|null} uri
 * @param {boolean} exact
 * @param {Loadable} loadable
 */
export const addRoute = (uri, exact, loadable) => {
    const routeData = uri != null ? {
        path: uri,
        exact: exact,
        component: loadable
    } : {
        component: loadable
    };

    if (exact) {
        routes.unshift(routeData);
    } else {
        routes.push(routeData);
    }
};

/**
 * Generates the URL for a route.
 *
 * @param {string} name
 * @param {Object.<string, string>} opt_params
 * @return {string}
 */
export const generateUrl = (name, opt_params = {}) => {
    return FosJSRouting.generate(name, opt_params);
};
