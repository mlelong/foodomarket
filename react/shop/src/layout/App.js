import React from "react";
import {connect} from "react-redux";
import {Dimmer, Grid, Icon, Image, Loader, Menu, Segment, Button} from "semantic-ui-react";
import {Switch} from "react-router";
import {Link, NavLink, Redirect, Route, withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import PanelLeft from "./Panels/PanelLeft";
import PanelRight from "./Panels/PanelRight";
import CatalogMenu from "shop/pages/catalog/CatalogMenu";
import CatalogSearch from "shop/pages/catalog/CatalogSearch";
import UserWidget from "shop/components/Widget/UserWidget";
import Footer from "./Footer";
import {renderRoutes} from "react-router-config";
import routes from "shop/pages";
import {SemanticToastContainer} from "react-semantic-toasts";
import ReactGA from "react-ga";
import ReactTimeout from "react-timeout";
import RegisterModal from "./Modals/RegisterModal";
import logo1 from "img/logo-1.png";
import ReactPixel from "react-facebook-pixel";
import RedirectModal from "shop/layout/Modals/RedirectModal";

class App extends React.Component {
    state = {
        // Controls whether the register modal should be shown again
        skipShowRegisterModal: false,
        // Controls the register modal open state
        showRegisterModal: this.props.debug.showRegisterModal,
    };

    // Show the register modal after 30 seconds
    registerTimeout = 30000;

    showRegisterModalHandle = undefined;

    componentDidMount() {
        const {location} = this.props;

        ReactGA.pageview(location.pathname, ["analytics"]);
        ReactPixel.pageView();

        if (this.shouldShowRegisterModal()) {
            // Show the register modal after 20 seconds
            this.showRegisterModalHandle = this.props.setTimeout(
                  this.showRegisterModal,
                  this.registerTimeout
            );
        }
    }

    // Show modal if page is produit, catalogue or recherche
    shouldShowByPage = () => {
        const {location} = this.props;

        return location.pathname.match(`\/(produit|catalogue|recherche)\/[\\S\\s]+`) !== null;
    };

    shouldShowByAuthOrCookie = () => {
        const { authToken } = this.props;
        const splittedCookie = document.cookie.split(";");
        const hasEmailCookie = splittedCookie.filter(item => item.includes("_email=")).length;
        const hasRestaurantCookie = splittedCookie.filter(item => item.includes("_restaurant=")).length;

        return !authToken && (!hasEmailCookie || !hasRestaurantCookie);
    };

    shouldShowRegisterModal = () => {
        return !this.props.isAppDev && !this.state.skipShowRegisterModal && this.shouldShowByPage() && this.shouldShowByAuthOrCookie();
    };

    showRegisterModal = () => {
        this.setState({ showRegisterModal: true });
    };

    hideRegisterModal = () => {
        this.setState({ showRegisterModal: false, skipShowRegisterModal: true });
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {location} = this.props;

        if (location.pathname !== prevProps.location.pathname) {
            ReactGA.pageview(location.pathname, ["analytics"]);
            ReactPixel.pageView();
        }

        const shouldShowRegisterModal = this.shouldShowRegisterModal();

        if (shouldShowRegisterModal && this.showRegisterModalHandle === undefined) {
            // Show the register modal after 20 seconds
            this.showRegisterModalHandle = this.props.setTimeout(
                    this.showRegisterModal,
                    this.registerTimeout
            );
        } else if (
                !shouldShowRegisterModal &&
                this.showRegisterModalHandle !== undefined
        ) {
            this.props.clearTimeout(this.showRegisterModalHandle);
            this.showRegisterModalHandle = undefined;
            this.hideRegisterModal();
        }
    }

    render() {
        const {
            authToken,
            loading,
            leftPanelVisibility,
            rightPanelVisibility,
            location,
            phoneNumber,
            registrationToComplete
        } = this.props;

        const { showRegisterModal } = this.state;

        const showShoppingList = location.pathname.match(`${States.SHOPPING_LIST}\/[\\S\\s]+`) !== null;

        let panelCenterClass = "panel-center";

        if (leftPanelVisibility) {
            panelCenterClass += " left-visible";
        }

        if (rightPanelVisibility) {
            panelCenterClass += " right-visible";
        }

        const isRegisterPage =
                location.pathname.match(`${States.ACCOUNT_REGISTER}`) !== null
                || location.pathname.match(`${States.ACCOUNT_REGISTER_FINAL.replace('/:token', '\/.+')}`) !== null;

        // Transformation de phoneNumber pour la href
        let numWithoutSpace = removeSpace(phoneNumber);
        let numWithoutZero = numWithoutSpace.substring(1,numWithoutSpace.length);
        let refNumber = "tel:+33" + numWithoutZero;

        return (
            <div className={"ui sixteen wide column"}>
                <Segment
                    basic
                    className={
                        "main-content" +
                        (!authToken ? " non-logged" : "") +
                        (isRegisterPage ? " register-page" : "")
                    }
                >
                    <Dimmer active={loading} page={true} inverted={true}>
                        <Loader size="massive" inverted={true}/>
                    </Dimmer>
                    <Segment id={"foodo-menu-segment"} basic padded={false} inverted>
                        <Grid id={"menu-grid"}>
                            <Grid.Row style={{paddingBottom: "0"}}>
                                <Grid.Column width={16} only={"tablet mobile"}>
                                    <Menu inverted id={"foodo-menu"} borderless>
                                        <Menu.Item as={Link} to={States.VISITOR_HOME}>
                                            <Image src={logo1} height={40}/>
                                        </Menu.Item>
                                    </Menu>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row style={{paddingTop: "0"}}>
                                <Grid.Column
                                    width={4}
                                    only={"computer"}
                                    verticalAlign={"middle"}
                                >
                                    <Link
                                        to={States.VISITOR_HOME}
                                        style={{display: "inline-block"}}
                                    >
                                        <Image src={logo1} height={70}/>
                                    </Link>
                                </Grid.Column>
                                <Grid.Column
                                    tablet={16}
                                    computer={10}
                                    mobile={16}
                                    verticalAlign={"middle"}
                                >
                                    <Grid padded={false} centered>
                                        <Grid.Row only={"computer tablet"}>
                                            <Grid.Column
                                                computer={3}
                                                tablet={3}
                                                only={"computer tablet"}
                                                textAlign={"right"}
                                            >
                                                <CatalogMenu/>
                                            </Grid.Column>

                                            <Grid.Column
                                                width={6}
                                                only={"computer tablet"}
                                                verticalAlign={"top"}
                                                style={{top: 1}}
                                            >
                                                <CatalogSearch/>
                                            </Grid.Column>

                                            <Grid.Column
                                                width={7}
                                                only={"computer tablet"}
                                                className={"space-button"}
                                            >
                                                {(authToken && <UserWidget/>) || (
                                                    <div className={"login-subscribe"}>
                                                        <NavLink to={States.ACCOUNT_REGISTER}>
                                                            <Button style={{color: '#005700'}} size="small"
                                                                    color='green'>
                                                                Inscription
                                                            </Button>
                                                        </NavLink>
                                                        <NavLink to={States.ACCOUNT_LOGIN}>
                                                            <Button size="small" id={"custom-button-login"} basic
                                                                    color='green'>
                                                                Connexion
                                                            </Button>
                                                        </NavLink>
                                                    </div>
                                                )}
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row only={"mobile"} style={{paddingBottom: "0"}}>
                                            <Grid.Column
                                                width={14}
                                                verticalAlign={"middle"}
                                                style={{minHeight: "40px"}}
                                            >
                                                <CatalogSearch/>
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row only={"mobile"} style={{paddingTop: "0"}}>
                                            {(authToken && (
                                                <Grid.Column width={8} style={{paddingLeft: "70px"}}>
                                                    <CatalogMenu/>
                                                </Grid.Column>
                                            )) || (
                                                <Grid.Column width={8}>
                                                    <CatalogMenu/>
                                                </Grid.Column>
                                            )}

                                            <Grid.Column
                                                width={8}
                                                verticalAlign={"middle"}
                                                textAlign={"right"}
                                                style={{paddingRight: "86px"}}
                                            >
                                                {(authToken && <UserWidget/>) || (
                                                    <div className={"login-subscribe"}>
                                                        <NavLink to={States.ACCOUNT_REGISTER}>
                                                            Inscription
                                                        </NavLink>
                                                        <NavLink to={States.ACCOUNT_LOGIN}>
                                                            Connexion
                                                        </NavLink>
                                                    </div>
                                                )}
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </Grid.Column>
                                <Grid.Column
                                    width={4}
                                    only={"computer tablet"}
                                >
                                    <a
                                        id="callback-phone"
                                        href={refNumber}
                                        style={{
                                            color: "white",
                                            position: "fixed",
                                            top: "29px",
                                            right: "30px"
                                        }}
                                    >
                                        <Icon name={"phone"}/>
                                        {phoneNumber}
                                    </a>
                                </Grid.Column>
                                <Grid.Column
                                    width={3}
                                    only={"mobile"}
                                >
                                    <a
                                        id="callback-phone"
                                        href={refNumber}
                                        style={{
                                            color: "white",
                                            position: "fixed",
                                            top: "8px",
                                            right: "8px"
                                        }}
                                    >
                                        <Icon name={"phone"}/>
                                        {phoneNumber}
                                    </a>
                                </Grid.Column>
                            </Grid.Row>


                        </Grid>
                    </Segment>

                    {/*Without Sidebar*/}
                    <Segment id={"foodo-content"}>
                        {authToken && <PanelLeft/>}
                        {authToken && <PanelRight shoppingListMode={showShoppingList}/>}

                        <div id="panel-center" className={panelCenterClass}>
                            <div id="toast-overlay">
                                <SemanticToastContainer position="top-right"/>
                            </div>

                            <Switch>
                                {authToken && (
                                    <Route
                                        exact
                                        path="/"
                                        render={() => <Redirect to={States.DASHBOARD_HOME}/>}
                                    />
                                )}
                                {renderRoutes(routes)}
                            </Switch>
                        </div>
                    </Segment>

                    {/*With sidebar*/}
                    {/*<Sidebar.Pushable as={Segment} id={"foodo-content"}>*/}
                    {/*{authToken && <PanelLeft />}*/}
                    {/*{authToken && <PanelRight shoppingListMode={showShoppingList}/>}*/}

                    {/*<Sidebar.Pusher>*/}
                    {/*<div id="panel-center" className={panelCenterClass}>*/}
                    {/*<div id="toast-overlay">*/}
                    {/*<SemanticToastContainer position="top-right" />*/}
                    {/*</div>*/}

                    {/*<Switch>*/}
                    {/*{authToken && <Route exact path="/" render={() => <Redirect to={States.DASHBOARD_HOME}/>}/>}*/}
                    {/*{renderRoutes(routes)}*/}
                    {/*</Switch>*/}
                    {/*</div>*/}
                    {/*</Sidebar.Pusher>*/}
                    {/*</Sidebar.Pushable>*/}
                </Segment>

                {!authToken && <Footer/>}

                {!registrationToComplete && <RedirectModal/>}

                <RegisterModal
                    open={showRegisterModal}
                    onRegisterDone={this.hideRegisterModal}
                />
                </div>
        );
    }
}

function removeSpace(str)
{
    str = str.replace(/[\s]+/g,""); // Enlève les espaces doubles, triples, etc.
    str = str.replace(/^[\s]/, ""); // Enlève les espaces au début
    str = str.replace(/[\s]$/,""); // Enlève les espaces à la fin
    return str;
}


export default withRouter(
    connect(state => ({
        authToken: state.frontState.authToken,
        phoneNumber: state.frontState.phoneNumber,
        leftPanelVisibility: state.frontState.leftPanelVisibility,
        rightPanelVisibility: state.frontState.rightPanelVisibility,
        loading: state.frontState.loading,
        url: state.frontState.url,
        isMobile: state.frontState.isMobile,
        debug: state.debugState,
        isAppDev: state.frontState.isAppDev,
        registrationToComplete: state.frontState.registrationToComplete
    }))(ReactTimeout(App))
);
