import React from "react";
import PropTypes from "prop-types";
import {Button, Divider, Embed, Form, Header, Message, Modal} from "semantic-ui-react";
import {connect} from "react-redux";
import {Get, Post} from "shop/utils/fetch";
import ReactGA from "react-ga";
import {withRouter} from "react-router";
import ReactPixel from "react-facebook-pixel";
import {generateUrl} from "shop/router";
import videoPlaceholder from "img/video-placeholder.png";
import Liform from "shop/utils/form/Liform";
import {processSubmitErrors, renderFields} from "liform-react";
import SemanticUITheme from "shop/form-theme/semanticui/SemanticUITheme";
import syncValidation from "shop/utils/form/syncValidation";
import {Link} from "react-router-dom";
import {States} from "shop/constants/frontConstants";

const RegisterModalBaseForm = props =>
{
    const {schema, handleSubmit, error, submitting, context, theme} = props;

    return (
        <Form onSubmit={handleSubmit} loading={submitting}>
            {renderFields(schema, theme || SemanticUITheme, "", context)}

            {error && <Message visible error content={error}/>}

            <div style={{textAlign: 'center'}}>
                <Button type={"submit"} id="submit-application-button" className={"blue"} primary disabled={submitting} loading={submitting}>Je reçois les tarifs par mail</Button>

                <Button type={"button"} as={Link} to={States.ACCOUNT_LOGIN}>Déjà un compte ? Cliquez ici pour vous connecter</Button>
            </div>
        </Form>
    );
};

class RegisterModal extends React.Component
{
    static propTypes = {
        open: PropTypes.bool,
        onRegisterDone: PropTypes.func.isRequired
    };

    static defaultProps = {
        open: false
    };

    state = {
        open: this.props.open,
        submitted: false,
        error: '',
        form: undefined
    };

    componentDidMount() {
        if (!this.state.form) {
            Get(generateUrl('app_front_acquisition_timeout_modal_form'))
                .then(data => {
                    this.setState({form: data.form});
                });
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.open != this.props.open) {
            this.setState({open: this.props.open});
        }
    }

    close = () => {
        this.props.onRegisterDone();
    };

    submit = (data) => {
        return Post(generateUrl('app_front_acquisition_timeout_modal_form'), data, 'sf-form')
            .then(response => {
                if (response.ok) {
                    ReactGA.event({
                        category: 'conversion',
                        action: 'Prospect'
                    }, ['analytics']);

                    ReactPixel.track('SubmitApplication');

                    this.setState({submitted: true});
                } else {
                    processSubmitErrors(data.errors);
                }
            }).catch(error => {
                this.setState({error: error});
            });
    };

    render() {
        const {open, submitted, error, form} = this.state;

        return (
            <Modal size={'small'} dimmer='blurring' open={open} closeOnEscape={false} closeOnDimmerClick={false}>
            {/*<Modal dimmer='blurring' open={open} closeOnEscape={true} closeOnDimmerClick={true} closeIcon onClose={this.close}>*/}
                <Modal.Header style={{textAlign: 'center'}}>
                    {submitted && "Merci" || "Je veux recevoir les tarifs foodomarket"}
                </Modal.Header>

                {submitted &&
                <Modal.Content>
                    <p>
                        Nous allons vous envoyer les tarifs par mail.<br/>
                        Pour toute question, n'hésitez pas à nous contacter au <a href="tel:+33171546145">01 71 54 61 45</a>&nbsp;
                        ou par mail à <a href="mailto:hello@foodomarket.com">hello@foodomarket.com</a>
                    </p>
                    <div style={{textAlign: 'center'}}>
                        <Divider/>
                        <Header as={'h4'}>Je découvre comment fonctionne FoodoMarket</Header>
                        <Embed id={"YQhBzWu7f2Q"} icon={"play circle"} source={"youtube"} placeholder={videoPlaceholder}/>
                        <br/>
                        <Link to={States.ACCOUNT_REGISTER} style={{fontSize: '16px'}}>Je suis intéressé, je m'inscris ici</Link>
                    </div>
                </Modal.Content>
                ||
                <Modal.Content>
                    {error.length > 0 &&
                    <Message negative>
                        <Message.Header>Une erreur est survenue</Message.Header>
                        <p>{error}</p>
                    </Message>
                    }

                    {form &&
                        <Liform
                            schema={form.schema}
                            formKey={'timeout-modal'}
                            initialValues={form.values}
                            onSubmit={this.submit}
                            baseForm={RegisterModalBaseForm}
                            syncValidation={syncValidation(form.schema, {
                                firstname: 'Merci de nous indiquer votre prénom',
                                restaurant: 'Merci de nous indiquer le nom de votre restaurant',
                                email: 'Merci de nous indiquer votre email'
                            })}
                        />
                    }
                </Modal.Content>
                }

                {submitted &&
                <Modal.Actions style={{textAlign: 'center'}}>
                    <Button positive content='Ok' onClick={this.close}/>
                </Modal.Actions>
                }
            </Modal>
        );
    }
}

export default withRouter(connect()(RegisterModal));
