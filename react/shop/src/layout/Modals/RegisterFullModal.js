import React from "react";
import PropTypes from "prop-types";
import {Button, Modal} from "semantic-ui-react";

const RegisterFullModal = props => {
    return (
        <Modal dimmer='blurring' open={props.open} size='tiny' closeOnEscape={true} closeOnDocumentClick={true} closeIcon onClose={props.onLater}>
            <Modal.Header style={{textAlign:"center"}}>
                Je m'inscris gratuitement sur FoodoMarket
            </Modal.Header>

            <Modal.Content style={{fontSize: '16px'}}>
                <p>
                    Je peux choisir parmi plus de 2500 produits livrés par les grossistes de la marketplace
                </p>
                <p>
                    J'achète en &laquo;deux clics&raquo; chez plusieurs fournisseurs, et je gagne 15 minutes par jour.
                </p>
                <p>
                    Je compare les prix pour optimiser mes achats (-12 % constatés en moyenne).
                </p>
            </Modal.Content>

            <Modal.Actions>
                <Button id="register-later-button" onClick={props.onLater} negative>Plus tard</Button>
                <Button
                    id="register-now-button"
                    onClick={props.onSubscribe}
                    primary
                    content="Je m'inscris pour profiter des offres !"
                />
            </Modal.Actions>
        </Modal>
    );
};

RegisterFullModal.propTypes = {
    open: PropTypes.bool.isRequired,
    onSubscribe: PropTypes.func.isRequired,
    onLater: PropTypes.func.isRequired
};

export default RegisterFullModal;
