import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import {Button, Grid, Modal} from "semantic-ui-react";
import logo from "img/logo-foodomarket.png";
import ReactPixel from "react-facebook-pixel";

const RedirectModal = props => {
    const {authToken} = props;

    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (
            !open && !authToken && typeof window !== "undefined"
            && document.cookie.replace(/(?:(?:^|.*;\s*)VISITOR_TYPE\s*\=\s*([^;]*).*$)|^.*$/, "$1") !== "professional"
        ) {
            setOpen(true);
        }
    });

    const close = () => {
        document.cookie = 'VISITOR_TYPE=professional; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/';
        ReactPixel.trackCustom('Visitor Pro');
        setOpen(false);
    };

    const redirect = () => {
        ReactPixel.trackCustom('Visitor Particulier');
        document.location = 'https://foodomarket.club';
    };

    return (
        <Modal dimmer={"blurring"} open={open} size="tiny">
            <Modal.Content style={{textAlign: "center"}}>
                <img src={logo} height="150"/>

                <br/>

                <p style={{fontSize: "18px"}}>
                    Le marché en ligne alimentaire
                </p>

                <b style={{fontSize: "20px"}}>Vous êtes ?</b>
            </Modal.Content>

            <Modal.Actions style={{textAlign: "center"}}>
                <Grid>
                    <Grid.Row columns={2}>
                        <Grid.Column>
                            <Button onClick={close} color={"green"} fluid>Un professionnel</Button>
                        </Grid.Column>
                        <Grid.Column>
                            <Button onClick={redirect} color={"blue"} fluid>Un particulier</Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Modal.Actions>
        </Modal>
    );
};

export default connect(state => ({
    authToken: state.frontState.authToken
}))(RedirectModal);
