import React from "react";
import {Confirm} from "semantic-ui-react";

export default class ConfirmModal extends React.Component
{
    constructor(props, context) {
        super(props, context);

        this.onCancel = this.onCancel.bind(this);
        this.onConfirm = this.onConfirm.bind(this);
    }

    onCancel() {
        const {onCancel} = this.props;

        if (onCancel !== undefined) {
            onCancel();
        }
    }

    onConfirm() {
        const {onConfirm} = this.props;

        if (onConfirm !== undefined) {
            onConfirm();
        }
    }

    render() {
        const {modalOpen,header,content} = this.props;

        return (
            <Confirm
                header={header}
                content={content}
                open={modalOpen}
                onCancel={this.onCancel}
                onConfirm={this.onConfirm}
                cancelButton={"Annuler"}
                confirmButton={"Confirmer"}
            />
        );
    }
}