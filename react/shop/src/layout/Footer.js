import React from "react";
import {Grid, Header, Icon, List, Segment} from "semantic-ui-react";
import moment from "moment";
import {NavLink, withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {connect} from "react-redux";

class Footer extends React.Component {
    render() {

        const {
            phoneNumber
        } = this.props;

        // Transformation de phoneNumber pour la href
        let numWithoutSpace = removeSpace(phoneNumber);
        let numWithoutZero = numWithoutSpace.substring(1, numWithoutSpace.length);
        let refNumber = "tel:+33" + numWithoutZero;

        return (
            <Segment as={'footer'} inverted attached={"top"} className={"footer"}>
                <Grid centered stackable>
                    <Grid.Row columns={3}>
                        <Grid.Column>
                            <div>
                                <Header as={'h5'}>&Agrave; propos</Header>

                                <List size={"large"}>
                                    <List.Item>
                                        <Icon name={"user"}/>
                                        <List.Content>
                                            <NavLink to={States.ABOUT_US}>Qui sommes-nous ?</NavLink>
                                        </List.Content>
                                    </List.Item>
                                    <List.Item>
                                        <Icon name={"info circle"}/>
                                        <List.Content>
                                            <NavLink to={States.LEGAL_NOTICE}>Mentions légales</NavLink>
                                        </List.Content>
                                    </List.Item>
                                    <List.Item>
                                        <Icon name={"user secret"}/>
                                        <List.Content>
                                            <NavLink to={States.PRIVACY_POLICY}>Politique de confidentialité</NavLink>
                                        </List.Content>
                                    </List.Item>
                                </List>
                            </div>
                        </Grid.Column>

                        <Grid.Column>
                            <div>
                                <Header as={'h5'}>Des questions ? Discutons-en !</Header>

                                <List size={"large"}>
                                    <List.Item>
                                        <Icon name={"facebook"}/>
                                        <List.Content>
                                            <a href="https://www.facebook.com/foodomarket/" target="_blank">Facebook</a>
                                        </List.Content>
                                    </List.Item>
                                    <List.Item>
                                        <Icon name={"instagram"}/>
                                        <List.Content>
                                            <a href="https://www.instagram.com/foodomarket/"
                                               target="_blank">Instagram</a>
                                        </List.Content>
                                    </List.Item>
                                    <List.Item>
                                        <Icon name={"phone"}/>
                                        <List.Content>
                                            <a href={refNumber}> {phoneNumber}</a>
                                        </List.Content>
                                    </List.Item>
                                    <List.Item>
                                        <Icon name={"envelope outline"}/>
                                        <List.Content>
                                            <a href="mailto:hello@foodomarket.com">hello@foodomarket.com</a>
                                        </List.Content>
                                    </List.Item>
                                </List>
                            </div>
                        </Grid.Column>

                        <Grid.Column verticalAlign={"bottom"}>
                            <div>
                                <List.Item>&copy; {moment().format('YYYY')} Foodomarket</List.Item>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

function removeSpace(str) {
    str = str.replace(/[\s]+/g, ""); // Enlève les espaces doubles, triples, etc.
    str = str.replace(/^[\s]/, ""); // Enlève les espaces au début
    str = str.replace(/[\s]$/, ""); // Enlève les espaces à la fin
    return str;
}

export default withRouter(
    connect(state => ({
        phoneNumber: state.frontState.phoneNumber
    }))(Footer)
);
