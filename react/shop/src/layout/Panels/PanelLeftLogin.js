import React from "react";
import {connect} from "react-redux";
import {Item} from "semantic-ui-react";
import {logout} from "shop/actions/index";

class PanelLeftLogin extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.onLogout = this.onLogout.bind(this);
    }

    onLogout(e) {
        e.preventDefault();
        this.props.dispatch(logout());
    }

    render() {
        return (
            <Item className={"login"}>
                <Item.Image size="medium" shape="rounded" />
                <Item.Content verticalAlign='middle'>
                    <span className="login-welcome">Bienvenue {this.props.user && this.props.user.username || 'test'} !</span>
                        <br/>
                        {/*<a className="login-welcome" href="">Mon profil</a>*/}
                        {/*<br/>*/}
                        <a className="login-welcome" href="" onClick={this.onLogout}>Déconnexion</a>
                </Item.Content>
            </Item>
        );
    }
}

export default connect(state => (
    {
        user: state.frontState.user
    }
))(PanelLeftLogin)