import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Button, Icon, Label, Ref, Sidebar, Transition} from "semantic-ui-react";
import ShoppingListPanel from "./ShoppingListPanel";
import ShoppingCartPanel from "./ShoppingCartPanel";
import {toggleRightPanel} from "shop/actions";
import {countAllItems} from "shop/utils/shoppingItems";

class PanelRight extends React.Component {
    state = {
        targetRef: undefined,
        visible: true
    };

    static propTypes = {
        shoppingListMode: PropTypes.bool
    };

    static defaultProps = {
        shoppingListMode: false
    };

    handleTargetRef = (targetRef) => {
        this.setState({targetRef});
    };

    animate = () => {
        this.setState({visible: !this.state.visible})
    };

    toggle = () => {
        this.state.targetRef.blur();
        this.props.dispatch(toggleRightPanel());
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.shoppingListMode !== this.props.shoppingListMode) {
            this.setState({visible: true});
        } else if (prevProps.rightPanelVisibility !== this.props.rightPanelVisibility && !this.props.rightPanelVisibility) {
            this.setState({visible: true});
        } else if (prevProps.shoppingCarts !== this.props.shoppingCarts
            || prevProps.shoppingListProducts !== this.props.shoppingListProducts) {
            this.animate();
        }
    }

    render() {
        const {rightPanelVisibility, shoppingListMode, shoppingListProducts, shoppingCarts} = this.props;
        const {visible} = this.state;
        const component = shoppingListMode ? <ShoppingListPanel/> : <ShoppingCartPanel/>;

        return (
            <div>
                {this.state.targetRef &&
                <Sidebar target={this.state.targetRef} id="panel-right" className="panel-right" width="wide" visible={rightPanelVisibility}
                         animation='overlay' direction='right'>
                    {component}
                </Sidebar>
                }

                <Ref innerRef={this.handleTargetRef}>
                    {shoppingListMode &&
                    <Transition animation={"pulse"} duration={500} visible={visible}>
                        <Button animated id="toggle-right-sidebar" className={rightPanelVisibility ? "visible" : ""} onClick={this.toggle}>
                            <Button.Content visible>
                                <Icon name={rightPanelVisibility ? "close" : "list ul"}/>
                            </Button.Content>
                            <Button.Content hidden>
                                {rightPanelVisibility && 'Fermer' || 'Liste'}
                            </Button.Content>
                            {!rightPanelVisibility &&
                            <Label circular color={"red"} content={shoppingListProducts.length} size={"tiny"}
                                   className="badge"/>
                            }
                        </Button>
                    </Transition>
                    ||
                    <Transition animation={"pulse"} duration={500} visible={visible}>
                        <Button animated id="toggle-right-sidebar" className={rightPanelVisibility ? "visible" : ""} onClick={this.toggle}>
                            <Button.Content visible>
                                <Icon name={rightPanelVisibility ? "close" : "cart"}/>
                            </Button.Content>
                            <Button.Content hidden>
                                {rightPanelVisibility && 'Fermer' || 'Panier'}
                            </Button.Content>

                            {!rightPanelVisibility &&
                            <Label circular color={"red"} content={countAllItems(shoppingCarts)} size={"tiny"}
                                   className="badge"/>
                            }
                        </Button>
                    </Transition>
                    }
                </Ref>
            </div>
        )
    }
}

export default connect(state => (
    {
        rightPanelVisibility: state.frontState.rightPanelVisibility,
        shoppingListProducts: state.frontState.shoppingListProducts,
        shoppingCarts: state.shoppingCarts
    }
))(PanelRight)