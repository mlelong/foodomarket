import React from "react";
import {connect} from "react-redux";
import {Button, Icon, Menu, Ref, Sidebar} from "semantic-ui-react";
import {Link, withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {hideLeftPanel, toggleLeftPanel} from "shop/actions";

class PanelLeft extends React.Component {
    state = {
        targetRef: undefined
    };

    onLinkClicked = () => {
        const {isMobileOrTablet} = this.props;

        if (isMobileOrTablet) {
            this.props.dispatch(hideLeftPanel());
        }
    };

    handleTargetRef = (targetRef) => {
        this.setState({targetRef});
    };

    toggle = () => {
        this.state.targetRef.blur();
        this.props.dispatch(toggleLeftPanel());
    };

    render() {
        const {leftPanelVisibility, location} = this.props;
        const {targetRef} = this.state;

        const leftTarget = (
            <Button id="toggle-left-sidebar" active={false} compact className={leftPanelVisibility ? "visible" : ""} animated='fade' onClick={this.toggle}>
                <Button.Content visible>
                    <Icon name={leftPanelVisibility ? 'close' : 'sidebar'} />
                </Button.Content>
                <Button.Content hidden>
                    {leftPanelVisibility && 'Fermer' || 'Menu'}
                </Button.Content>
            </Button>
        );

        return (
            <div>
                {targetRef &&
                <Sidebar target={targetRef} borderless id="panel-left" className="panel-left" as={Menu}
                         width="thin" visible={leftPanelVisibility} animation='overlay' direction='left' vertical
                         inverted>
                    <Menu.Item link={true} as={Link} onClick={this.onLinkClicked} to={States.DASHBOARD_HOME} active={location.pathname === States.DASHBOARD_HOME} position="left">
                        <Icon name="home" size="large"/> <span className="text">Tableau de bord</span>
                    </Menu.Item>

                    {/*<Menu.Item link={true} as={Link} to={States.SUPPLIERS} onClick={this.onLinkClicked} position="left" active={location.pathname === States.SUPPLIERS}>*/}
                    {/*    <Icon name="truck" size="large"/> <span className="text">Mes fournisseurs</span>*/}
                    {/*</Menu.Item>*/}

                    <Menu.Item link={true} as={Link} onClick={this.onLinkClicked} to={States.SUPPLIER_ORDER} position="left" active={location.pathname === States.SUPPLIER_ORDER}>
                        <Icon name="arrow down cart" size="large"/> <span className="text">Passer une commande</span>
                    </Menu.Item>

                    <Menu.Item link={true} as={Link} onClick={this.onLinkClicked} to={States.SHOPPING_LIST} position="left" active={location.pathname.startsWith(States.SHOPPING_LIST)}>
                        <Icon name="clipboard list" size="large"/> <span className="text">Mes listes d'achats</span>
                    </Menu.Item>

                    <Menu.Item link={true} as={Link} onClick={this.onLinkClicked} to={States.ORDER_HISTORY} active={location.pathname === States.ORDER_HISTORY} position="left">
                        <Icon name="shipping fast" size="large"/> <span className="text">Mes commandes</span>
                    </Menu.Item>

                    <Menu.Item link={true} as={Link} onClick={this.onLinkClicked} to={States.INVOICE} active={location.pathname === States.INVOICE} position={"left"}>
                        <Icon name="barcode" size="large"/> <span className="text">Mes factures</span>
                    </Menu.Item>

                    <Menu.Item link={true} as={Link} onClick={this.onLinkClicked} to={States.SEPA} active={location.pathname === States.SEPA} position={"left"}>
                        <Icon name="warehouse" size="large"/> <span className="text">Mes fournisseurs</span>
                    </Menu.Item>

                    <Menu.Item link={true} as={Link} onClick={this.onLinkClicked} to={States.LITIGATION} active={location.pathname === States.LITIGATION} position={"left"}>
                        <Icon name="bomb" size="large"/> <span className="text">Mes litiges</span>
                    </Menu.Item>

                </Sidebar>
                }

                <Ref innerRef={this.handleTargetRef}>
                    {leftTarget}
                </Ref>
            </div>
        );
    }
}

export default withRouter(connect(state => (
    {
        leftPanelVisibility: state.frontState.leftPanelVisibility,
        isMobileOrTablet: state.frontState.isMobileOrTablet
    }
))(PanelLeft))
