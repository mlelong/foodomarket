import React from "react";
import {connect} from "react-redux";
import {Grid, Icon} from "semantic-ui-react";
import {removeFromCart} from "shop/actions";
import {PricePanelWidget} from "./PricePanelWidget";

class ShoppingCartPanelProduct extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.onRemoveProduct = this.onRemoveProduct.bind(this);
    }

    onRemoveProduct(e) {
        const {product} = this.props;
        this.props.dispatch(removeFromCart(product));
    }

    render() {
        const {product} = this.props;
        const {quantity} = this.props;
        const {quantityUnit} = this.props;

        return (
            <Grid className={"shopping-cart-panel-product"}>
                <Grid.Row>
                    <span className={"product-name"}>{product.productName}</span>
                </Grid.Row>
                <Grid.Row columns={3}>
                    <Grid.Column>
                        <span className={"product-quantity"}>Qté: {quantity}<span className={"product-quantity-unit"}>{quantityUnit}</span></span>
                    </Grid.Column>
                    <Grid.Column>
                        <PricePanelWidget product={product} />
                    </Grid.Column>
                    <Grid.Column>
                        <Icon name={"trash"} onClick={this.onRemoveProduct}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default connect(state => (
    {
    }
))(ShoppingCartPanelProduct)