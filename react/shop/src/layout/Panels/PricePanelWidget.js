import React from "react";

export function PricePanelWidget(props) {
    const {product} = props;
    let price, unit;

    switch (product.quantityUnitSelected) {
        case 'ORDER_KG':
        case 'KG':
            price = product.productPrice / 100;
            unit = 'kg';
            break ;
        case 'ORDER_PC':
        case 'PC':
            price = product.productPriceUnit / 100 / product.unitQuantity;
            unit = 'pc';
            break ;
        case 'ORDER_L':
        case 'L':
            price = product.productPrice / 100;
            unit = 'L';
            break ;
        case 'ORDER_CO':
        case 'ORDER_COLIS':
        case 'COLIS':
        case 'CO':
            price = product.productPriceUnit / 100;
            unit = 'colis';
            break ;
        default:
            price = product.productPrice / 100;
            unit = 'kg';
            break ;
    }

    return (
        <span className="product-price">
            {price.toFixed(2)}€<span className="product-price-unit">/{unit}</span>
        </span>
    );
}
