import React from "react";
import {connect} from "react-redux";
import {Accordion, Button, Grid, Header, Icon, Label, Menu} from "semantic-ui-react";
import ShoppingCartPanelProduct from "./ShoppingCartPanelProduct";
import ConfirmModal from "shop/layout/Modals/ConfirmModal";
import {emptyCart} from "shop/actions";
import {countItems, forEachSupplierItems} from "shop/utils/shoppingItems";

class ShoppingCartPanelSupplier extends React.Component
{
    state = {
        active: true,
        emptyCartModalOpen: false
    };

    onEmptyCart = (e) => {
        e.preventDefault();

        this.setState({emptyCartModalOpen: true});
    };

    confirmEmptyCart = () => {
        const {id: cartId} = this.props.data;

        this.setState({emptyCartModalOpen: false});
        this.props.dispatch(emptyCart(cartId));
    };

    cancelEmptyCart = () => {
        this.setState({emptyCartModalOpen: false});
    };

    toggle = () => {
        this.setState({active: !this.state.active});
    };

    render() {
        const shoppingCart = this.props.data;
        const {supplierName, supplierId} = shoppingCart;
        const {index,suppliers} = this.props;
        const {active,emptyCartModalOpen} = this.state;
        const nbItems = countItems(shoppingCart);
        const deployLabelIcon = active ? "angle up" : "angle down";

        let totalSupplierPrice = 0;
        const products = [];

        forEachSupplierItems(shoppingCart, (product, index) => {
            if (product.hasOwnProperty('productPrice') && product.hasOwnProperty('productPriceUnit')) {
                switch (product.quantityUnitSelected) {
                    case 'KG':
                        totalSupplierPrice += (product.productPrice * product.quantity) / 100;
                        break ;
                    case 'PC':
                        // totalSupplierPrice += (product.productPriceUnit / product.unitQuantity * product.quantity) / 100;
                        totalSupplierPrice += (product.productPriceUnit * product.quantity) / 100;
                        break ;
                    case 'L':
                        totalSupplierPrice += (product.productPrice * product.quantity) / 100;
                        break ;
                    case 'CO':
                        totalSupplierPrice += (product.productPriceUnit * product.quantity) / 100;
                        break ;
                    default:
                        break ;
                }
            }

            products.push(<ShoppingCartPanelProduct key={index} product={product} quantity={product.quantity} quantityUnit={product.quantityUnitSelected}/>);
        });

        let amountForFreeShippingCost = 0;
        let minOrderAmount = 0;
        let limitOrderTime = 'N/C';
        let shippingCost = 0;

        for (let supplier of suppliers) {
            if (supplier.supplierId === supplierId) {

                amountForFreeShippingCost = supplier.freePort;
                minOrderAmount = supplier.minimumOrder;
                limitOrderTime = supplier.orderLimitTime;
                shippingCost = supplier.shippingCost;

                break ;
            }
        }

        const orderable = totalSupplierPrice >= minOrderAmount ? "" : "non-orderable";
        const missingAmount = minOrderAmount - totalSupplierPrice;

        return (
            <Menu.Item className={"shopping-cart-supplier " + orderable}>
                <Accordion.Title index={index} active={active}>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={12} onClick={this.toggle}>
                                <Header className={"supplier-name"}>{supplierName}</Header>
                                <span className={"supplier-nb-items"}>{nbItems} produits</span>
                            </Grid.Column>
                            <Grid.Column width={1}>
                                <Icon name={"trash"} onClick={this.onEmptyCart} />
                            </Grid.Column>
                            <Grid.Column width={3} onClick={this.toggle}>
                                <Label className={"deploy-label"} content={<Icon name={deployLabelIcon} />}/>
                                <br/>
                                <span className={"supplier-items-price"}>{totalSupplierPrice.toFixed(2)}€</span>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Accordion.Title>
                <Accordion.Content active={active}>
                    {products}
                </Accordion.Content>
                <Grid className={"panel-actions"}>
                    <Grid.Row>
                        <Grid.Column>
                            <Button fluid className={"order-button"} content={"Passer la commande"} icon={"in cart"} labelPosition={"left"} onClick={this.props.onOrder} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <span className="indicator-message">Il vous manque {missingAmount.toFixed(2)}€ pour atteindre votre minimum de commande</span>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <ConfirmModal
                    modalOpen={emptyCartModalOpen}
                    header={""}
                    content={"Etes-vous sûr de vouloir supprimer le panier " + supplierName + " et les " + nbItems + " produits qu'il contient ?"}
                    onConfirm={this.confirmEmptyCart} onCancel={this.cancelEmptyCart}
                />
            </Menu.Item>
        );
    }
}

export default connect(state => (
    {
        suppliers: state.frontState.suppliers
    }
))(ShoppingCartPanelSupplier)