import React from "react";
import {connect} from "react-redux";
import {Button} from "semantic-ui-react";
import ShoppingListProducts from "shop/components/ShoppingListProducts";
import FrontInput from "shop/components/FrontInput";
import {States} from "shop/constants/frontConstants";
import {hideRightPanel, saveShoppingList, updateShoppingList} from "shop/actions";
import {withRouter} from "react-router-dom";

class ShoppingListPanel extends React.Component {
    state = {
        listName: this.props.shoppingListName,
        isTyping: false
    };

    static getDerivedStateFromProps(props, state) {
        if (state.listName === undefined || !state.isTyping) {
            return {listName: props.shoppingListName};
        }

        return null;
    }

    onOk = (e) => {
        e.preventDefault();

        this.props.dispatch(hideRightPanel());
    };

    onSave = (e) => {
        e.preventDefault();

        const {currentState, history, shoppingListId, shoppingListProducts, isMobileOrTablet} = this.props;
        const {listName} = this.state;

        switch (currentState) {
            case States.SHOPPING_LIST_EDITION:
                this.props.dispatch(updateShoppingList(listName, shoppingListId, (response) => {
                    if (response.ok) {
                        history.push(States.SHOPPING_LIST);
                    } else {
                        const errorMessage = `Could not update shopping list with id '${shoppingListId}' : ${response.message}`;

                        console.log(errorMessage);
                        alert(errorMessage);
                    }
                }));
                break ;
            case States.SHOPPING_LIST_CREATION:
                this.props.dispatch(saveShoppingList(listName, shoppingListProducts, (response) => {
                    if (response.ok) {
                        history.push(States.SHOPPING_LIST);
                    } else {
                        const errorMessage = 'Could not create shopping list: ' + response.message;

                        console.log(errorMessage);
                        alert(errorMessage);
                    }
                }));
                break ;
        }

        if (isMobileOrTablet) {
            this.props.dispatch(hideRightPanel());
        }
    };

    onListnameChanged = (name) => {
        this.setState({listName: name, isTyping: true});
    };

    render() {
        const productsAdded = this.props.shoppingListProducts.length;
        const {listName: shoppingListName} = this.state;

        return (
            <div className={"shopping-list-panel"}>
                <div className={"infos-container"}>
                    <FrontInput className={"my-list"} icon={"pencil"} placeholder="Nom de votre liste" inputValue={shoppingListName} onChange={this.onListnameChanged} />

                    {productsAdded > 0 &&
                    <div>
                        <br/>
                        <span className={"products-in-list"}>{productsAdded} produits dans la liste</span>
                        <br/>

                        <h3 className={"shopping-list-creation-step third-step"}>
                            <span className={"shopping-list-creation-step-nb"}>3. </span>
                            Gérez votre liste d'achats
                        </h3>

                        <ShoppingListProducts/>

                        <h3 className={"shopping-list-creation-step"}>
                            <span className={"shopping-list-creation-step-nb"}>4. </span>
                            Enregistrez votre liste
                        </h3>
                    </div>
                    }
                </div>

                {productsAdded > 0 &&
                <Button fluid
                        className={"save-shopping-list-button"}
                        content={"Enregistrer la liste prédéfinie"}
                        icon={"checkmark"}
                        labelPosition={"left"}
                        onClick={this.onSave}
                /> ||
                <Button fluid
                        className={"save-shopping-list-button"}
                        content={"Ok"}
                        icon={"checkmark"}
                        labelPosition={"left"}
                        onClick={this.onOk}
                />
                }
            </div>
        )
    }
}

export default withRouter(connect(state => (
    {
        currentState: state.frontState.currentState,
        shoppingListId: state.frontState.shoppingListId,
        shoppingListProducts: state.frontState.shoppingListProducts,
        shoppingListName: state.frontState.shoppingListName,
        isMobileOrTablet: state.frontState.isMobileOrTablet
    }
))(ShoppingListPanel))