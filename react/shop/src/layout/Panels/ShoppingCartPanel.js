import React from "react";
import {connect} from "react-redux";
import {Accordion, Button, Grid, Image, Menu, Popup} from "semantic-ui-react";
import ShoppingCartPanelSupplier from "./ShoppingCartPanelSupplier";
import cart from "img/cart.png";
import {emptyCarts, toggleRightPanel} from "shop/actions";
import ConfirmModal from "shop/layout/Modals/ConfirmModal";
import {withRouter} from "react-router-dom";
import {States} from "shop/constants/frontConstants";
import {forEachSuppliersItems} from "shop/utils/shoppingItems";

class ShoppingCartPanel extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            activeIndex: -1,
            emptyCartsModalOpen: false
        };

        this.onOrder = this.onOrder.bind(this);
        this.onEmptyCarts = this.onEmptyCarts.bind(this);
        this.confirmEmptyCarts = this.confirmEmptyCarts.bind(this);
        this.cancelEmptyCarts = this.cancelEmptyCarts.bind(this);
    }

    onOrder() {
        const {history} = this.props;

        this.props.dispatch(toggleRightPanel());

        history.push(States.ORDER_CART_SUMMARY);
    }

    onEmptyCarts() {
        this.setState({emptyCartsModalOpen: true});
    }

    confirmEmptyCarts() {
        this.setState({emptyCartsModalOpen: false});
        this.props.dispatch(emptyCarts());
    }

    cancelEmptyCarts() {
        this.setState({emptyCartsModalOpen: false});
    }

    render() {
        const {emptyCartsModalOpen} = this.state;
        const {shoppingCarts} = this.props;
        let nbItems = 0;
        const panels = [];

        forEachSuppliersItems(shoppingCarts, (shoppingCart, key) => {
            panels.push(
                <ShoppingCartPanelSupplier
                    onOrder={this.onOrder}
                    key={key}
                    index={key}
                    data={shoppingCart}
                />
            );

            nbItems += Object.keys(shoppingCart.items).length;
        });

        return (
            <Grid className={"shopping-cart-panel"}>
                <Grid.Row>
                    <Grid.Column width={10}>
                        <span className={"my-list"}>Ma commande</span>
                        {nbItems > 0 && <span className={"products-in-cart"}>{nbItems} produits dans le panier</span>}
                    </Grid.Column>
                    {nbItems > 0 &&
                    <Grid.Column width={3} textAlign={"right"}>
                        <Popup
                            trigger={<Button className={"remove-all-button"} color={"red"} icon={"trash"} onClick={this.onEmptyCarts}/>}
                            content={"Vider le panier"}
                            inverted
                        />
                    </Grid.Column>
                    }
                    {nbItems > 0 &&
                    <Grid.Column width={3} textAlign={"center"}>
                        <Popup
                            trigger={<Button className={"mini-order-all"} icon={"in cart"} onClick={this.onOrder}/>}
                            content={"Commander"}
                            inverted
                        />
                    </Grid.Column>
                    }
                </Grid.Row>
                <Grid.Row>
                    {nbItems > 0 &&
                    <Accordion fluid={true} styled={true} as={Menu} vertical exclusive={false}>
                        {panels}
                    </Accordion> ||
                    <div className="empty-cart-photo-container">
                        <Image className="empty-cart-photo" src={cart} />
                        <i>Votre panier est vide !</i>
                    </div>
                    }
                </Grid.Row>

                <ConfirmModal
                    modalOpen={emptyCartsModalOpen}
                    header={"Attention: suppression des paniers d'achats"}
                    content={"Etes-vous sûr de vouloir supprimer vos paniers d'achats ?"}
                    onConfirm={this.confirmEmptyCarts} onCancel={this.cancelEmptyCarts}
                />
            </Grid>
        );
    }
}

export default withRouter(connect(state => (
    {
        shoppingCarts: state.shoppingCarts
    }
))(ShoppingCartPanel));
