import {generateUrl} from "shop/router";
import entries from "shop/utils/entries";

function handleError(error) {
    console.log(error);
}

// function getCookie(name) {
//     const cookie = document.cookie.split(';').filter((value) => value.indexOf(name) >= 0);
//
//     return cookie.length > 0 ? cookie[0].split('=')[1].trim() : undefined;
// }

async function handleReconnect(response, url, options = {}) {
    const status = response.status;

    if (response.redirected) {
        document.location = response.url;
    }

    if (status === 403 || status === 401) {
        // Try to issue a new access token from refresh token, if any
        if (typeof window !== undefined) {
            const refreshToken = localStorage.getItem('_refresh');

            if (refreshToken) {
                // Call refresh token endpoint
                const ret = await Post(generateUrl('gesdinet_jwt_refresh_token'), {refresh_token: refreshToken})
                    .then(response => {
                        if (response.hasOwnProperty('token') && response.hasOwnProperty('refresh_token')) {
                            localStorage.setItem('_token', response.token);
                            localStorage.setItem('_refresh', response.refresh_token);

                            return true;
                        }

                        return false;
                    }).catch(ignored => {
                        return false;
                    });

                if (ret) {
                    // If we got a new token and refresh token, let's retry the request, without calling handleRequest again
                    // to avoid potential infinite loop
                    return Fetch(url, options);
                } else {
                    // Otherwise let's clear the local storage from outdated/invalid data
                    localStorage.removeItem('_token');
                    localStorage.removeItem('_refresh');
                }
            }
        }

        document.location = generateUrl('app_front_connection_page');
    } else if (status === 500) {
        await response.json().then(data => {
            document.location = generateUrl('app_front_500_page', {exception: {code: data.code, message: data.message, uri: data.uri}});
        });
    }

    return response;
}

function dataToFormData(formName, data, formData = new FormData())
{
    for (let [key, value] of entries(data)) {
        if (value instanceof FileList) {
            formData.append(`${formName}[${key}]`, value[0]);
        } else if (typeof value === 'object') {
            formData = dataToFormData(`${formName}[${key}]`, value, formData);
        } else {
            formData.append(`${formName}[${key}]`, value);
        }
    }

    return formData;
}

function postPutOptions(url, data = {}, type = 'json', options = {}, method = 'POST') {
    options = {
        ...options,
        method: method,
        headers: {},
        body: null
    };

    if (type === 'json') {
        options.body = JSON.stringify(data);
        options.headers = {
            'Accept'        : 'application/json',
            'Content-Type'  : 'application/json'
        };
    } else if (type === 'sf-form') {
        options.body = dataToFormData('form', data);
    } else {
        options.body = data;
    }

    return options;
}

function Fetch(url, options = {}) {
    let option = {
        ...options,
        // credentials: 'include',
    };

    option.headers = option.headers || {};
    option.headers['X-Requested-With'] = 'XMLHttpRequest';

    return fetch(url, option);
}

export function FetchWithReconnect(url, options = {}) {
    return Fetch(url, options)
        .then(response => {
            return handleReconnect(response, url, options);
        })
    ;
}

function FetchJSON(url, options = {}) {
    return FetchWithReconnect(url, options)
        .then(response => {
            if (response.ok) {
                return response.json();
            }

            throw Error(response.statusText);
        }).catch(e => {
            handleError(e);
        });
}

export function Get(url, options = {}) {
    return FetchJSON(url, options);
}

export function Post(url, data = {}, type = 'json', options = {}) {
    return FetchJSON(url, postPutOptions(url, data, type, options, 'POST'));
}

export function Put(url, data = {}, type = 'json', options = {}) {
    return FetchJSON(url, postPutOptions(url, data, type, options, 'PUT'));
}

export function Delete(url, options = {}) {
    options = {
        ...options,
        method: 'DELETE'
    };

    return FetchJSON(url, options);
}
