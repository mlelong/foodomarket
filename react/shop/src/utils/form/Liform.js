import React from "react";
import compileSchema from "liform-react/lib/compileSchema";
import {reduxForm} from "redux-form";
import {renderField} from "liform-react";
import PropTypes from "prop-types";
import BaseForm from "shop/form-theme/semanticui/BaseForm";
import SemanticUITheme from "shop/form-theme/semanticui/SemanticUITheme";
import {isRequired} from "liform-react/lib/renderFields";

const Liform = props => {
    const initialValues = props.initialValues;

    props.schema.showLabel = false;
    const schema = compileSchema(props.schema);
    const formName = props.formKey || props.schema.title || "form";
    const FinalForm = reduxForm({
        form: props.formKey || props.schema.title || "form",
        validate: props.syncValidation,
        initialValues: initialValues,
        enableReinitialize: props.enableReinitialize,
        destroyOnUnmount: props.destroyOnUnmount,
        context: { ...props.context, formName }
    })(props.baseForm);

    const cleanedProps = {...props};

    delete cleanedProps['syncValidation'];
    delete cleanedProps['enableReinitialize'];
    delete cleanedProps['destroyOnUnmount'];
    delete cleanedProps['context'];

    return (
        <FinalForm
            renderFields={renderField.bind(this)}
            {...cleanedProps}
            schema={schema}
        />
    );
};

Liform.propTypes = {
    enableReinitialize: PropTypes.bool.isRequired,
    destroyOnUnmount: PropTypes.bool.isRequired,
    theme: PropTypes.any,
    baseForm: PropTypes.any
};

Liform.defaultProps = {
    enableReinitialize: true,
    destroyOnUnmount: false,
    theme: SemanticUITheme,
    baseForm: BaseForm
};

export default Liform;

export const renderFieldsByName = (schema, theme, fields, prefix = '', context = {}) => {
    return fields.map(field => schema.properties.hasOwnProperty(field)
        ? renderField(schema.properties[field], field, theme, prefix, context, isRequired(schema, field))
        : ''
    );
};
