import Ajv from "ajv";
import ajvErrors from "ajv-errors";
import localizeAjv from "ajv-i18n";
import {setError} from "liform-react";
import merge from "deepmerge";

const syncValidation = (schema, errorMessages = null) => {
    if (errorMessages !== null) {
        schema.errorMessage = {
            properties: errorMessages
        };
    }

    let ajv = new Ajv({
        errorDataPath: "property",
        allErrors: true,
        jsonPointers: true
    });

    ajv = ajvErrors(ajv);

    return values => {
        const valid = ajv.validate(schema, values);

        if (valid) {
            return {};
        }

        const ajvErrors = ajv.errors;

        localizeAjv.fr(ajvErrors);

        let errors = ajvErrors.map(error => {
            return setError(error, schema);
        });

        // We need at least two elements
        errors.push({});
        errors.push({});

        return merge.all(errors);
    };
};

export default syncValidation;
