export default class Filter
{
    static filter(list, terms, field) {
        const filtered = [];

        for (let item of list) {
            if (Filter.passFilter(terms, item[field])) {
                filtered.push(item);
            }
        }

        return filtered;
    }

    static passFilter(terms, value) {
        const pattern = "[\\s\\S]*(" + terms.split(' ').join('[\\s\\S]+') + ")[\\s\\S]*";
        const regexp = new RegExp(pattern, 'i');

        return regexp.test(value);
    }
}