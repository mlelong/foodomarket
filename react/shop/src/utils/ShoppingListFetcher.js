import {Get} from "./fetch";
import {generateUrl} from "shop/router";

export default function getShoppingLists(callback, terms = '') {
    Get(generateUrl('app_front_getShoppingLists', {terms: terms}))
        .then(data => {
            callback(data);
        }).catch(exception => {
            console.log("Exception caught during fetching Shopping Lists", exception);
            callback(null);
        })
    ;
}
