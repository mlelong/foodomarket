export default function splitList(products) {
    const listItemData = {
        supplierId: 0,
        supplierName: '',
        products: []
    };

    let list = [];

    for (let p of products) {
        if (!list.hasOwnProperty(p.supplierId)) {
            list[p.supplierId] = Object.assign({}, listItemData, {
                supplierId: p.supplierId,
                supplierName: p.productSupplier,
                products: []
            });
        }

        list[p.supplierId].products.push(p);
    }

    return list.filter(function(e){return e});
}