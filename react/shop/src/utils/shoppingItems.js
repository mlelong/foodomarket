/**
 * Counts total products from either a ShoppingLists or ShoppingCarts object
 *
 * @param {Object} suppliersItems
 * @returns {number}
 */
export function countAllItems(suppliersItems) {
    let count = 0;

    forEachSuppliersItems(suppliersItems, (supplierItems) => {
        count += Object.keys(supplierItems.items).length;
    });

    return count;
}

/**
 * Count the products of a specific supplier from either a ShoppingLists or ShoppingCarts object
 *
 * @param {int} supplierId
 * @param {Object} suppliersItems
 * @returns {number}
 */
export function countSupplierItems(supplierId, suppliersItems) {
    const supplierItems = suppliersItems[supplierId];

    return supplierItems ? Object.keys(supplierItems.items).length : 0;
}

/**
 *
 * @param {Object} supplierItems
 * @returns {number}
 */
export function countItems(supplierItems) {
    return Object.keys(supplierItems.items).length;
}

/**
 * Get a specific supplier item from either a ShoppingLists or ShoppingCarts object
 *
 * @param {int} supplierId
 * @param {int} productId
 * @param {Object} suppliersItems
 */
export function getSupplierItem(supplierId, productId, suppliersItems) {
    const supplierItems = suppliersItems[supplierId];

    return supplierItems ? supplierItems.items[productId] : undefined;
}

/**
 * Remove a specific supplier item from either a ShoppingLists or ShoppingCarts object
 *
 * @param {int} supplierId
 * @param {int} productId
 * @param {Object} suppliersItems
 */
export function removeSupplierItem(supplierId, productId, suppliersItems) {
    if (suppliersItems.hasOwnProperty(supplierId)) {
        suppliersItems[supplierId] = {
            ...suppliersItems[supplierId],
            items: {...suppliersItems[supplierId].items}
        };

        delete suppliersItems[supplierId].items[productId];

        if (Object.keys(suppliersItems[supplierId].items).length === 0) {
            delete suppliersItems[supplierId];
        }
    }
}

/**
 * Remove a specific supplier from either a ShoppingLists or ShoppingCarts object
 *
 * @param {int} supplierId
 * @param {Object} suppliersItems
 */
export function removeSupplier(supplierId, suppliersItems) {
    if (suppliersItems.hasOwnProperty(supplierId)) {
        delete suppliersItems[supplierId];
    }
}

/**
 *
 * @param item
 * @param {Object} suppliersItems
 */
export function addSupplierItem(item, suppliersItems) {
    if (!suppliersItems.hasOwnProperty(item.supplierId)) {
        suppliersItems[item.supplierId] = {
            id: item.cartId,
            note: item.note,
            supplierId: item.supplierId,
            supplierName: item.productSupplier,
            items: {}
        };
    }

    suppliersItems[item.supplierId] = {
        ...suppliersItems[item.supplierId],
        items: {
            ...suppliersItems[item.supplierId].items,
            [item.productId]: item
        }
    };
}

/**
 *
 * @param {int} cartId
 * @param {Object} suppliersItems
 * @returns {Object|undefined}
 */
export function getSupplierItemsByCartId(cartId, suppliersItems) {
    return forEachSuppliersItems(suppliersItems, (supplierItems) => {
        if (supplierItems.id === cartId) {
            return supplierItems;
        }
    });
}

/**
 *
 * @param cartItemId
 * @param {Object} suppliersItems
 */
export function getItemByCartItemId(cartItemId, suppliersItems) {
    return forEachSuppliersItems(suppliersItems, (supplierItems) => {
        return forEachSupplierItems(supplierItems, (item) => {
            if (item.cartItemId === cartItemId) {
                return item;
            }
        })
    });
}

export function forEachSuppliersItems(suppliersItems, callback) {
    let ret;

    for (let key of Object.keys(suppliersItems)) {
        if (suppliersItems.hasOwnProperty(key)) {
            ret = callback(suppliersItems[key], key);

            if (ret) {
                return ret;
            }
        }
    }
}

export function forEachSupplierItems(supplierItems, callback) {
    let ret;

    for (let key of Object.keys(supplierItems.items)) {
        if (supplierItems.items.hasOwnProperty(key)) {
            ret = callback(supplierItems.items[key], key);

            if (ret) {
                return ret;
            }
        }
    }
}
