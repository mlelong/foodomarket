import React from "react";
import PropTypes from "prop-types";
import {Grid, Header} from "semantic-ui-react";

require('style/action-header.scss');

const ActionHeader = props => {
    return (
        <Grid.Row className={'action-header'}>
            <Grid.Column computer={13} tablet={12} mobile={9} verticalAlign='bottom'>
                <Header as={props.titleTag} dividing>{props.title}</Header>
            </Grid.Column>
            <Grid.Column computer={3} tablet={4} mobile={7} textAlign="right" verticalAlign='bottom'>
                {props.actions}
            </Grid.Column>
        </Grid.Row>
    );
};

ActionHeader.propTypes = {
    title: PropTypes.string.isRequired,
    titleTag: PropTypes.string.isRequired,
    actions: PropTypes.node.isRequired
};

ActionHeader.defaultProps = {
    titleTag: 'h1'
};

export default ActionHeader;
