import {Constants} from "shop/constants/frontConstants";
// import jwtDecode from "jwt-decode";
import {toast} from "react-semantic-toasts";
import ReactPixel from "react-facebook-pixel";
import {generateUrl} from "shop/router";
import {Delete, Get, Post, Put} from "shop/utils/fetch";
import entries from "shop/utils/entries";

export function clearInitialValues(properties) {
    return dispatch => {
        dispatch({type: Constants.CLEAR_INITIAL_VALUES, properties: properties});
    }
}

export function login(username, password, rememberme) {
    return dispatch => {
        dispatch({ type: Constants.LOGIN_ERROR, loginError: false});

        let data = {username, password};

        Post(generateUrl('shop_login_check'), data)
            .then((data) => {
                if (data.hasOwnProperty('ok') && !data.ok) {
                    dispatch({ type: Constants.LOGIN_ERROR, loginError: data.message});
                    return ;
                }

                if (data.hasOwnProperty('type') && data.type === 'individual') {
                    document.location = 'https://app.foodomarket.com';
                } else {
                    localStorage.setItem('_token', data.token);
                    localStorage.setItem('_refresh', data.refresh_token);

                    document.location = generateUrl('app_front_index');
                }

                // dispatch({ type: Constants.LOGIN_TOKEN_RECEIVED, token: data.token });
            }).catch((exception) => {
                dispatch({ type: Constants.LOGIN_ERROR, loginError: exception});
            })
        ;
    }
}

export function logout() {
    return dispatch => {
        if (typeof window !== undefined) {
            localStorage.removeItem('_token');
            localStorage.removeItem('_refresh');
        }

        document.location = generateUrl('app_front_logout');
        // dispatch({type: Constants.LOGOUT});
    }
}

export function toggleLeftPanel() {
    return dispatch => {
        dispatch({ type: Constants.TOGGLE_LEFT_PANEL });
    };
}

export function toggleRightPanel() {
    return dispatch => {
        dispatch({ type: Constants.TOGGLE_RIGHT_PANEL });
    };
}

export function showLeftPanel() {
    return dispatch => {
        dispatch({ type: Constants.SHOW_LEFT_PANEL });
    };
}

export function showRightPanel() {
    return dispatch => {
        dispatch({ type: Constants.SHOW_RIGHT_PANEL });
    };
}

export function hideLeftPanel() {
    return dispatch => {
        dispatch({ type: Constants.HIDE_LEFT_PANEL });
    };
}

export function hideRightPanel() {
    return dispatch => {
        dispatch({ type: Constants.HIDE_RIGHT_PANEL });
    };
}

export function createShoppingList() {
    return dispatch => {
        dispatch({ type: Constants.CREATE_SHOPPING_LIST });
    };
}

export function addToShoppingList(product, shoppingListId, updateServerside = false) {
    return dispatch => {
        if (!updateServerside) {
            dispatch({type: Constants.ADD_TO_SHOPPING_LIST, product: product});
        } else {
            Post(generateUrl('app_front_addToShoppingList', {id: shoppingListId}), {
                pid: product.productId,
                qty: product.quantity,
                unit: product.quantityUnitSelected,
                supplierId: product.supplierId
            }).then(data => {
                if (data.ok) {
                    dispatch({
                        type: Constants.ADD_TO_SHOPPING_LIST,
                        product: Object.assign({}, product, {sid: data.sid})
                    });

                    toast({
                        type: 'success',
                        icon: null,
                        title: "Produit ajouté à la liste",
                        description: `${product.quantity}${product.quantityUnitSelected} de ${product.productName} ajoutés à la liste`,
                        time: 3000,
                    });
                } else {
                    console.log("Could not insert product to shopping list", data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while inserting product to shopping list", exception);

                toast({
                    type: 'error',
                    icon: null,
                    title: "Produit non ajouté à la liste",
                    description: `${exception}`,
                    time: 3000,
                });
            });
        }
    };
}

export function removeFromShoppingList(product, shoppingListId, updateServerside = false) {
    return dispatch => {
        if (!updateServerside) {
            dispatch({type: Constants.REMOVE_FROM_SHOPPING_LIST, product: product});
        } else {
            Delete(generateUrl('app_front_removeFromShoppingList', {
                id: shoppingListId,
                sid: product.sid
            })).then(data => {
                if (data.ok) {
                    dispatch({type: Constants.REMOVE_FROM_SHOPPING_LIST, product: product});

                    toast({
                        type: 'success',
                        icon: null,
                        title: "Produit supprimé de la liste",
                        description: `${product.productName} a bien été supprimé de la liste`,
                        time: 3000,
                    });
                } else {
                    console.log("Could not delete product from shopping list", data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while removing product from shopping list", exception);

                toast({
                    type: 'error',
                    icon: null,
                    title: "Produit non supprimé de la liste",
                    description: `${exception}`,
                    time: 3000,
                });
            });
        }
    };
}

export function editShoppingList(id) {
    return dispatch => {
        Get(generateUrl('app_front_getShoppingListItems', {id: id}))
            .then(data => {
                dispatch({type: Constants.EDIT_SHOPPING_LIST, id: data.listId, products: data.data, listName: data.listName});
            }).catch(exception => {
                console.log("Exception caught while fetching shopping list with id `" + id + "`", exception);
            })
        ;
    };
}

export function removeShoppingList(id, callback) {
    return dispatch => {
        Delete(generateUrl('app_front_removeShoppingList', {id: id}))
            .then(data => {
                callback(data);

                toast({
                    type: 'success',
                    icon: null,
                    title: "Liste d'achat supprimée",
                    description: `La liste a bien été supprimée`,
                    time: 3000,
                });
            })
            .catch(exception => {
                callback({ok: false, message: "Exception caught while removing shopping list with id `" + id + "`: " + exception});

                toast({
                    type: 'success',
                    icon: null,
                    title: "Liste d'achat non supprimée",
                    description: `${exception}`,
                    time: 3000,
                });
            })
        ;
    };
}

export function orderShoppingList(data) {
    return dispatch => {
        dispatch({type: Constants.ORDER_SHOPPING_LIST, id: data.id, products: data.items, listName: data.listName});
    };
}

export function addAllToCart(products) {
    return dispatch => {
        Put(generateUrl('app_front_addAllToCart'), {products: products})
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ADD_ALL_TO_CART, products: data.products});

                    const contents = [];
                    let value = 0;
                    let productPrice;

                    for (let product of products) {
                        productPrice = (['KG', 'L'].includes(product.quantityUnitSelected) ? product.productPrice : product.productPriceUnit) / 100;
                        contents.push({id: `${product.pid}`, quantity: product.quantity, item_price: productPrice});
                        value += productPrice * product.quantity;
                    }

                    ReactPixel.track('AddToCart', {
                        content_type: 'product',
                        value: value,
                        currency: 'EUR',
                        contents: contents
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while adding products to cart", exception);
            })
        ;
    }
}

export function addToCart(product) {
    return dispatch => {
        const productData = {
            v: product.productId,
            s: product.supplierId,
            q: product.quantity,
            u: product.quantityUnitSelected,
            note: product.note
        };

        Put(generateUrl('app_front_addToCart'), { product: productData })
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ADD_TO_CART, product: Object.assign({}, product, {
                        cartItemId: data.cartItemId,
                        cartId: data.cartId,
                        note: data.previousNote
                    })});

                    toast({
                        type: 'success',
                        icon: null,
                        title: 'Produit ajouté du panier',
                        description: `${productData.q}${productData.u} de ${product.productName} ont été ajoutés au panier`,
                        time: 3000,
                    });

                    let productPrice = (['KG', 'L'].includes(product.quantityUnitSelected) ? product.productPrice : product.productPriceUnit) / 100;

                    ReactPixel.track('AddToCart', {
                        content_type: 'product',
                        value: productPrice * product.quantity,
                        currency: 'EUR',
                        contents: [{id: `${product.pid}`, quantity: product.quantity, item_price: productPrice}]
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while adding product to cart", exception);

                toast({
                    type: 'error',
                    icon: null,
                    title: 'Produit non ajouté du panier',
                    description: `${exception}`,
                    time: 3000,
                });

                document.location.reload(true);
            })
        ;
    };
}

export function removeFromCart(product) {
    return dispatch => {
        Delete(generateUrl('app_front_removeFromCart', {itemId: product.cartItemId}))
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.REMOVE_FROM_CART, product: product});

                    toast({
                        type: 'success',
                        icon: null,
                        title: 'Produit enlevé du panier',
                        description: `${product.productName} a été enlevé du panier`,
                        time: 3000,
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while removing product from cart", exception);

                toast({
                    type: 'error',
                    icon: null,
                    title: 'Produit non enlevé du panier',
                    description: `${exception}`,
                    time: 3000,
                });

                document.location.reload(true);
            })
        ;
    }
}

export function changeQuantity(product, quantity, unit) {
    return dispatch => {
        if (product.cartItemId === undefined) {
            dispatch({type: Constants.ORDER_QUANTITY_CHANGED, product: product, quantity: quantity, unit: unit});
        } else {
            Put(generateUrl('app_front_updateCartItemQuantity', {itemId: product.cartItemId}), {
                qty: quantity,
                unit: unit
            }).then(data => {
                if (data.ok) {
                    dispatch({
                        type: Constants.ORDER_QUANTITY_CHANGED,
                        product: product,
                        quantity: quantity,
                        unit: unit
                    });

                    toast({
                        type: 'success',
                        icon: null,
                        title: 'Quantité mise à jour',
                        description: `${quantity}${unit} de ${product.productName} au panier`,
                        time: 3000,
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while updating shopping cart item quantity", exception);

                toast({
                    type: 'error',
                    icon: null,
                    title: 'Quantité non mise à jour',
                    description: `${exception}`,
                    time: 3000,
                });

                document.location.reload(true);
            });
        }
    };
}

export function emptyCarts() {
    return dispatch => {
        Delete(generateUrl('app_front_emptyCarts'))
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.CARTS_EMPTIED});

                    toast({
                        type: 'success',
                        icon: null,
                        title: 'Paniers vidés',
                        description: `Tous les paniers ont bien été vidés`,
                        time: 3000,
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while emptying carts: ", exception);

                toast({
                    type: 'error',
                    icon: null,
                    title: 'Paniers non vidés',
                    description: `${exception}`,
                    time: 3000,
                });

                document.location.reload(true);
            })
        ;
    }
}

export function emptyCart(cartId) {
    return dispatch => {
        Delete(generateUrl('app_front_emptyCart', {id: cartId}))
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.CART_EMPTIED, cartId: cartId});

                    toast({
                        type: 'success',
                        icon: null,
                        title: 'Panier vidé',
                        description: `Ce panier à bien été vidé`,
                        time: 3000,
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while emptying carts: ", exception);

                toast({
                    type: 'error',
                    icon: null,
                    title: 'Panier non vidé',
                    description: `${exception}`,
                    time: 3000,
                });

                document.location.reload(true);
            })
        ;
    }
}

export function updateShoppingListProductQuantity(product, quantity, quantityUnit, updateServerside, shoppingListId) {
    return dispatch => {
        dispatch({type: Constants.UPDATE_SHOPPING_LIST_PRODUCT_QUANTITY, product: product, quantity: quantity, unit: quantityUnit});

        // Updates the quantity/unit on the server too
        if (updateServerside) {
            Put(generateUrl('app_front_updateShoppingListItemQuantity', {id: shoppingListId, sid: product.sid}), {qty: quantity, unit: quantityUnit})
                .then(data => {
                    if (data.ok) {
                        toast({
                            type: 'success',
                            icon: null,
                            title: 'Quantité modifiée',
                            description: `${quantity}${quantityUnit} de ${product.productName} dans la liste`,
                            time: 3000,
                        });
                    } else {
                        console.log("Could not udpate quantity for item " + product.sid, data.message);
                    }
                }).catch(exception => {
                    console.log("Exception caught while updating quantity of item `" + product.sid + "`", exception);

                    toast({
                        type: 'error',
                        icon: null,
                        title: 'Quantité non modifiée',
                        description: `${exception}`,
                        time: 3000,
                    });
                })
            ;
        }
    };
}

export function saveShoppingList(listName, listProducts, callback) {
    return dispatch => {
        const products = listProducts.map((product) => {
            return {
                productId: product.productId,
                supplierId: product.supplierId,
                qty: product.quantity,
                unit: product.quantityUnitSelected
            }
        });

        Post(generateUrl('app_front_addShoppingList'), {
            name: listName,
            products: products
        }).then(data => {
            callback(data);

            toast({
                type: 'success',
                icon: null,
                title: "Liste d'achat enregistrée",
                description: `La liste ${listName} a bien été enregistrée`,
                time: 3000,
            });
        }).catch(exception => {
            callback({ok: false, message: exception});

            toast({
                type: 'error',
                icon: null,
                title: "Liste d'achats non enregistrée",
                description: `${exception}`,
                time: 3000,
            });
        });
    };
}

export function updateShoppingList(listName, shoppingListId, callback) {
    return dispatch => {
        Post(generateUrl('app_front_updateShoppingList', {id: shoppingListId}), {name: listName})
            .then(data => {
                callback(data);

                toast({
                    type: 'success',
                    icon: null,
                    title: "Liste d'achats mise à jour",
                    description: `La liste ${listName} a bien été mise à jour`,
                    time: 3000,
                });
            }).catch(exception => {
                callback({ok: false, message: exception});

                toast({
                    type: 'error',
                    icon: null,
                    title: "Liste d'achat non mise à jour",
                    description: `${exception}`,
                    time: 3000,
                });
            })
        ;
    };
}

export function addShoppingListItemNote(shoppingListItemId, note) {
    return dispatch => {
        Put(generateUrl('app_front_setShoppingListItemNote', {itemId: shoppingListItemId}), {note: note})
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ADD_SHOPPING_LIST_ITEM_NOTE, shoppingListItemId: shoppingListItemId, note: note});
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while setting shopping list product note", exception);
            })
        ;
    }
}

export function addShoppingCartItemNote(cartItemId, note) {
    return dispatch => {
        Put(generateUrl('app_front_setProductNote', {itemId: cartItemId}), {note: note})
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ADD_SHOPPING_CART_ITEM_NOTE, cartItemId: cartItemId, note: note});
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while setting product note", exception);
            })
        ;
    }
}

export function addCartNote(cartId, note) {
    return dispatch => {
        Put(generateUrl('app_front_setCartNote', {id: cartId}), {note: note})
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ADD_CART_NOTE, cartId: cartId, note: note});
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while setting product note", exception);
            })
        ;
    }
}

export function orderAll(carts) {
    return dispatch => {
        Get(generateUrl('app_front_orderCarts'))
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ORDER_ALL_DONE, emails: data.emails});

                    const contents = [];
                    let value = 0;
                    let productPrice;

                    for (let [supplierId, cart] of entries(carts)) {
                        for (let[variantId, product] of entries(cart.items)) {
                            productPrice = (['KG', 'L'].includes(product.quantityUnitSelected) ? product.productPrice : product.productPriceUnit) / 100;
                            contents.push({id: `${product.pid}`, quantity: product.quantity, item_price: productPrice});
                            value += productPrice * product.quantity;
                        }
                    }

                    ReactPixel.track('Purchase', {
                        content_type: 'product',
                        value: value,
                        currency: 'EUR',
                        contents: contents
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while ordering all shopping carts", exception);
            })
        ;
    }
}

export function orderSingle(cart) {
    const cartId = cart.id;

    return dispatch => {
        Get(generateUrl('app_front_orderCart', {id: cartId}))
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ORDER_SINGLE_DONE, emails: data.emails});

                    const contents = [];
                    let value = 0;
                    let productPrice;

                    for (let [variantId, product] of entries(cart.items)) {
                        productPrice = (['KG', 'L'].includes(product.quantityUnitSelected) ? product.productPrice : product.productPriceUnit) / 100;
                        contents.push({id: `${product.pid}`, quantity: product.quantity, item_price: productPrice});
                        value += productPrice * product.quantity;
                    }

                    ReactPixel.track('Purchase', {
                        content_type: 'product',
                        value: value,
                        currency: 'EUR',
                        contents: contents
                    });
                } else {
                    throw Error(data.message);
                }
            }).catch(exception => {
                console.log("Exception caught while ordering all shopping carts", exception);
            })
        ;
    }
}

export function scheduleOrder(cartId, scheduledDate, callback) {
    return dispatch => {
        Put(generateUrl('app_front_scheduleOrder', {id: cartId, date: scheduledDate.split('/').reverse().join('-')}))
            .then(data => {
                if (data.ok) {
                    dispatch({type: Constants.ORDER_SCHEDULED, cartId: cartId, scheduledDate: scheduledDate});
                } else {
                    throw Error(data.message);
                }

                if (callback instanceof Function) {
                    callback(data);
                }
            }).catch(exception => {
                const message = "Exception caught while scheduling order: " + exception;

                if (callback instanceof Function) {
                    callback({ok: false, message: message});
                }
            })
        ;
    }
}

export function filterShoppingListProducts(terms = '') {
    return dispatch => {
        dispatch({type: Constants.FILTER_SHOPPING_LIST_PRODUCTS, terms: terms});
    };
}

export function showLoadingIndicator() {
    return dispatch => {
        dispatch({type: Constants.LOADING_RESOURCE, loading: true});
    };
}

export function hideLoadingIndicator() {
    return dispatch => {
        dispatch({type: Constants.LOADING_RESOURCE, loading: false});
    };
}

export function changeState(newState) {
    return dispatch => {
        dispatch({type: Constants.CHANGE_STATE, newState: newState});
    };
}
