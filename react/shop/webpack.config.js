const Encore = require("@symfony/webpack-encore");
const MomentLocalesPlugin = require("moment-locales-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const ReactLoadableSSRAddon = require("react-loadable-ssr-addon");
const path = require("path");
const CompressionPlugin = require("compression-webpack-plugin");

Encore
  // directory where all compiled assets will be stored
  .setOutputPath("web/build/shop/client")
  // what's the public path to this directory (relative to your project's document root dir)
  .setPublicPath("/build/shop/client")
  // empty the outputPath dir before each build
  .cleanupOutputBeforeBuild()
  // will output as web/build/client.js
  .addEntry("app", [
    "@babel/polyfill",
    "isomorphic-fetch",
    "./react/shop/src/clientEntryPoint.js"
  ])
  // allow sass/scss files to be processed
  .enableSassLoader()
  // create hashed filenames (e.g. app.abc123.css)
  .enableVersioning(Encore.isProduction())
  .enableSourceMaps(!Encore.isProduction())
  .configureDefinePlugin(options => {
    options["process.env"].BROWSER = true;
  })
  .disableSingleRuntimeChunk()
  .addPlugin(
    new MomentLocalesPlugin({
      localesToKeep: ["fr"]
    })
  )
  .addAliases({
    dist: path.resolve(__dirname, "dist/"),
    shop: path.resolve(__dirname, "src/"),
    img: path.resolve(__dirname, "img/"),
    style: path.resolve(__dirname, "sass/")
  })
  .addPlugin(
    new ReactLoadableSSRAddon({
      filename: path.resolve(__dirname, "dist/manifest.json")
    })
  )
  .configureWatchOptions(function(watchOptions) {
    watchOptions.ignored = [path.join(__dirname, "dist"), watchOptions.ignored];
  });

if (Encore.isProduction()) {
  Encore.addPlugin(
    new TerserPlugin({
      sourceMap: false,
      cache: true,
      parallel: true,
      terserOptions: {
        toplevel: true
      }
    })
  ).addPlugin(new CompressionPlugin());
}

const config = Encore.getWebpackConfig();

config.optimization = {
  splitChunks: {
    chunks: "all"
  }
};

if (Encore.isDevServer()) {
  config.devServer.port = 8080;
}

module.exports = config;
