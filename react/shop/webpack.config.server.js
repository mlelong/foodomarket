const Encore = require('@symfony/webpack-encore');
const TerserPlugin = require('terser-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const path = require('path');

Encore
    // directory where all compiled assets will be stored
    .setOutputPath('web/build/shop/server')
    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/build/shop/server')
    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()
    // will output as web/build/app.js
    .addEntry('bundle', ['@babel/polyfill', 'isomorphic-fetch', './react/shop/src/serverEntryPoint.js'])
    .addEntry('react-on-rails-patch', ['./react/shop/src/ReactOnRailsPatch.js'])
    .enableSourceMaps(!Encore.isProduction())
    .configureDefinePlugin((options) => {
        options['process.env'].BROWSER = false;
    })
    .disableSingleRuntimeChunk()
    .addPlugin(new MomentLocalesPlugin({
        localesToKeep: ['fr'],
    }))
    .addAliases({
        dist: path.resolve(__dirname, 'dist/'),
        shop: path.resolve(__dirname, 'src/'),
        img: path.resolve(__dirname, 'img/'),
        style: path.resolve(__dirname, 'sass/')
    })
    .addExternals(nodeExternals())
    .addLoader({test: /\.scss/, loader: 'ignore-loader'})
;

if (Encore.isProduction()) {
    Encore
        .addPlugin(new TerserPlugin({
            sourceMap: true,
            cache: true,
            parallel: true,
            terserOptions: {
                toplevel: true
            }
        }))
    ;
}

const config = Encore.getWebpackConfig();

config.output.libraryTarget = 'commonjs2';
config.target = 'node';

module.exports = config;
