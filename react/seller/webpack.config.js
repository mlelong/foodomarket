const Encore = require('@symfony/webpack-encore');
const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');

Encore
    // directory where all compiled assets will be stored
    .setOutputPath('web/build/seller')
    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/build/seller')
    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()
    // will output as web/build/client.js
    .addEntry('seller', ['@babel/polyfill', 'isomorphic-fetch', './react/seller/src/entryPoint.js'])
    // allow sass/scss files to be processed
    .enableSassLoader()
    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    .enableSourceMaps(!Encore.isProduction())
    .configureDefinePlugin((options) => {
        options['process.env'].BROWSER = true;
    })
    .disableSingleRuntimeChunk()
    .addAliases({
        seller: path.resolve(__dirname, 'src/'),
        shop: path.resolve(__dirname, '../shop/src/'),
        style: path.resolve(__dirname, '../shop/sass/')
    })
;

if (Encore.isProduction()) {
    Encore
        .addPlugin(new TerserPlugin({
            sourceMap: false,
            cache: true,
            parallel: true,
            terserOptions: {
                toplevel: true
            }
        }))
    ;
}

const config = Encore.getWebpackConfig();

config.optimization = {
    splitChunks: {
        chunks: 'all'
    }
};

if (Encore.isDevServer()) {
    config.devServer.port = 8081;
}

module.exports = config;
