import ReactOnRails from "react-on-rails";
import SellerStore from "seller/store/SellerStore";
import LitigationList from "seller/components/litigation/LitigationList";
import LitigationShow from "seller/components/litigation/LitigationShow";

ReactOnRails.registerStore({SellerStore});
ReactOnRails.register({LitigationList});
ReactOnRails.register({LitigationShow});
