import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {combineReducers}  from 'redux';
import {reducer as formReducer} from 'redux-form';

export default function configureStore(props, context) {
    // use devtools if we are in a browser and the extension is enabled
    let composeEnhancers = typeof(window) !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        combineReducers({
            form: formReducer
        }),
        {},
        composeEnhancers(
            applyMiddleware(
                thunkMiddleware
            )
        )
    );
};
