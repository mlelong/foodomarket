import React from "react";
import {Button, Table} from "semantic-ui-react";

const LitigationList = props => {
    const {litigations} = props;

    return (
        <div>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Restaurant</Table.HeaderCell>
                        <Table.HeaderCell>Problème</Table.HeaderCell>
                        <Table.HeaderCell>Demande</Table.HeaderCell>
                        <Table.HeaderCell>Date</Table.HeaderCell>
                        <Table.HeaderCell>Statut</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {litigations.map((litigation, key) =>
                        <Table.Row key={key} positive={litigation.statusId == 3} negative={litigation.statusId == 1}>
                            <Table.Cell>{litigation.id}</Table.Cell>
                            <Table.Cell>{litigation.restaurant}</Table.Cell>
                            <Table.Cell>{litigation.problem}</Table.Cell>
                            <Table.Cell>{litigation.demand}</Table.Cell>
                            <Table.Cell>{litigation.created}</Table.Cell>
                            <Table.Cell>{litigation.status}</Table.Cell>
                            <Table.Cell>
                                <Button as={'a'} href={`/seller/litige/${litigation.hash}`}>
                                    Répondre
                                </Button>
                            </Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
        </div>
    );
};

export default LitigationList;
