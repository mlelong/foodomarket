import React, {useState, useEffect, useRef} from "react";
import {Post} from "shop/utils/fetch";
import {Button, Grid, Icon, Image, Message, Modal, Segment} from "semantic-ui-react";
import {processSubmitErrors} from "liform-react";
import LitigationTheme from "shop/form-theme/litigation/LitigationTheme";
import ActionHeader from "shop/utils/ActionHeader";
import ReactOnRails from "react-on-rails";
import {Provider} from "react-redux";
import {generateUrl} from "shop/router";
import Liform from "shop/utils/form/Liform";
import syncValidation from "shop/utils/form/syncValidation";
import LitigationCommentForm from "shop/components/LitigationCommentForm";

require("./Litigation.scss");

const LitigationShow = props => {
    const [litigation, setLitigation] = useState(props.litigation);
    const [litigationCommentForm, setLitigationCommentForm] = useState(props.litigationCommentForm);
    const messagesEnd = useRef(null);
    const {isMobile} = props;

    useEffect(() => {
        if (messagesEnd.current !== null) {
            messagesEnd.current.scrollIntoView();
        }
    });

    return (
        litigation &&
        <Segment id="litigation-show-segment">
            <Grid id="litigation-header">
                <ActionHeader
                    title={`Litige avec ${litigation.restaurant}`}
                    actions={
                        <Button as={'a'} href={'/seller/litiges'} icon labelPosition="left">
                            <Icon name="arrow left"/>
                            Retour
                        </Button>
                    }
                />

                <Grid.Row>
                    <Grid.Column>
                        <Message info>
                            <Message.Content>
                                <strong>Problème:</strong> {litigation.problem}<br/>
                                <strong>Demande:</strong> {litigation.demand}<br/>
                                <strong>Souhaite être rappelé:</strong> {litigation.ask_to_be_called && "Oui" || 'Non'}
                            </Message.Content>
                        </Message>
                    </Grid.Column>
                </Grid.Row>
            </Grid>

            <Grid id={"comments"}>
                {litigation.comments.map((comment, key) => {
                    const pos = comment.fromType === 'supplier' ? 'right' : 'left';

                    return (
                        <Grid.Row key={key} className={'comment-row'}>
                            <Grid.Column width={15} floated={pos}
                                         textAlign={pos}>
                                <div className={`comment ${pos} comment-${comment.fromType}`}>
                                    <p dangerouslySetInnerHTML={{__html: comment.content}}></p>
                                    {comment.photos.map((photo, pkey) =>
                                        <Modal
                                            key={pkey}
                                            size='large'
                                            closeIcon
                                            trigger={<Image title={"Agrandir"} className='comment-picture-thumbnail'
                                                            inline={!isMobile} fluid={isMobile}
                                                            src={isMobile ? photo.thumbnail_mobile : photo.thumbnail}/>}
                                        >
                                            <Modal.Content image scrolling>
                                                <Image src={photo.original} wrapped/>
                                            </Modal.Content>
                                        </Modal>
                                    )}
                                    <div className='comment-from'>{comment.from}</div>
                                    <div className='comment-date'>{comment.date}</div>
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                    );
                })}

                <div ref={messagesEnd}></div>
            </Grid>

            {litigationCommentForm &&
            <Grid id="litigation-comment-form-grid" style={{marginTop: 0}}>
                <Liform
                    syncValidation={syncValidation(litigationCommentForm.schema)}
                    schema={litigationCommentForm.schema}
                    initialValues={litigationCommentForm.values}
                    theme={LitigationTheme}
                    baseForm={LitigationCommentForm}
                    formKey={'litigationComment'}
                    onSubmit={data => {
                        return Post(generateUrl('app_seller_litigation_comment_add', {hash: litigation.hash}), data, 'sf-form').then(data => {
                            if (!data.ok) {
                                processSubmitErrors(data.form.errors);
                            } else {
                                setLitigationCommentForm(data.form);
                                setLitigation({...litigation, comments: [...litigation.comments, data.comment]});
                            }
                        });
                    }}
                />
            </Grid>
            }
        </Segment>
        || <React.Fragment/>
    );
};

const LitigationShowComponent = (initialProps, context) => {
    const store = ReactOnRails.getStore('SellerStore');

    return (
        <Provider store={store}>
            <LitigationShow {...initialProps}/>
        </Provider>
    );
};

export default LitigationShowComponent;
