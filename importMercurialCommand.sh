#!/bin/bash

#FOODOMARKET

bash importMercurial.sh FOODOMARKET_LALAUZE;

bash importMercurial.sh FOODOMARKET_MEATEAM;

bash importMercurial.sh FOODOMARKET_VERGER_DE_SOUAMA;

bash importMercurial.sh FOODOMARKET_GRAND_AUGUSTE;

bash importMercurial.sh FOODOMARKET_DOMAFRAIS;

bash importMercurial.sh FOODOMARKET_WINESNROSES_CAVE;

bash importMercurial.sh FOODOMARKET_JACOB;

bash importMercurial.sh FOODOMARKET_JACOB_PARTICULIER;

#bash importMercurial.sh FOODOMARKET_JACOB_EPICERIE;
#
#bash importMercurial.sh FOODOMARKET_JACOB_CREMERIE;
#
#bash importMercurial.sh FOODOMARKET_JACOB_BOUCHERIE;

bash importMercurial.sh FOODOMARKET_VERGERETOILE;

bash importMercurial.sh FOODOMARKET_LAURANCEPRIMEUR;

bash importMercurial.sh FOODOMARKET_REYNAUD_POISSON;

bash importMercurial.sh FOODOMARKET_REYNAUD_COQUILLAGE;

bash importMercurial.sh FOODOMARKET_REYNAUD_SALAISON;

bash importMercurial.sh FOODOMARKET_JOCEANE;

bash importMercurial.sh FOODOMARKET_HP3;

bash importMercurial.sh FOODOMARKET_HP3_PARTICULIER;

bash importMercurial.sh FOODOMARKET_HP4;

bash importMercurial.sh FOODOMARKET_HP5;

bash importMercurial.sh FOODOMARKET_PEYRAUD;

bash importMercurial.sh FOODOMARKET_DEMARNE;

bash importMercurial.sh FOODOMARKET_DAUMESNIL_CREMERIE;

bash importMercurial.sh FOODOMARKET_DAUMESNIL_FRUITSLEGUMES;

bash importMercurial.sh FOODOMARKET_ALAZARD;

bash importMercurial.sh FOODOMARKET_BEAUGRAIN;

bash importMercurial.sh FOODOMARKET_MTC;

bash importMercurial.sh FOODOMARKET_SUN7FRUITS;

bash importMercurial.sh FOODOMARKET_SAINFRUIT_MODE_DAUMESNIL;

bash importMercurial.sh FOODOMARKET_SAINFRUIT_FRUITSLEGUMES;

bash importMercurial.sh FOODOMARKET_TOPATLANTIQUE;

bash importMercurial.sh FOODOMARKET_ESTIVEAU;

bash importMercurial.sh FOODOMARKET_GARONNE_FRUITS;

bash importMercurial.sh FOODOMARKET_PRIMADOUR;

bash importMercurial.sh FOODOMARKET_SOBOMAR;

bash importMercurial.sh FOODOMARKET_AQUITAINE_PRIMEURS;

bash importMercurial.sh FOODOMARKET_BRUYERRE;

bash importMercurial.sh FOODOMARKET_CENTRALE_DU_FRAIS;

#VITALIS

bash importMercurial.sh VITALIS_MANDAR;

bash importMercurial.sh VITALIS_DEMARNE;

bash importMercurial.sh VITALIS_HP;

bash importMercurial.sh VITALIS_TERRAZUR;

# INVALIDATE CACHE & POPULATE STATS

php bin/console app:cache:remove-product-price 1 --env=prod --displaydebug;
php bin/console app:cache:populate-product-statistics 1 --env=prod --displaydebug;
# clear restaurant price cache
redis-cli --scan --pattern *restaurant-[^4]* | xargs redis-cli unlink;

exit 0

