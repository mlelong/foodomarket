import chug from 'gulp-chug';
import gulp from 'gulp';
import yargs from 'yargs';
import cp from 'child_process';
import path from 'path';

const { argv } = yargs
    .options({
        rootPath: {
            description: '<path> path to web assets directory',
            type: 'string',
            requiresArg: true,
            required: false,
        },
        nodeModulesPath: {
            description: '<path> path to node_modules directory',
            type: 'string',
            requiresArg: true,
            required: false,
        },
        env: {
            description: 'Used for shop compilation, use `prod` to enable minification, `dev` to disable',
            type: 'string',
            requiresArg: true,
            required: false
        }
    });

const config = [
    '--rootPath',
    argv.rootPath || path.resolve('./web/assets'),
    '--nodeModulesPath',
    argv.nodeModulesPath || path.resolve('./node_modules'),
];

export const buildAdmin = function buildAdmin() {
    return gulp.src('vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/gulpfile.babel.js', { read: false })
        .pipe(chug({ args: config, tasks: 'build' }));
};
buildAdmin.description = 'Build admin assets.';

export const watchAdmin = function watchAdmin() {
    return gulp.src('vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/gulpfile.babel.js', { read: false })
        .pipe(chug({ args: config, tasks: 'watch' }));
};
watchAdmin.description = 'Watch admin asset sources and rebuild on changes.';

export const buildSsr = function buildSsr() {
    const cmd = argv.env === 'dev' ? 'yarn run webpack-serverside-dev' : 'yarn run webpack-serverside-prod';
    return cp.exec(cmd);
};
buildSsr.description = 'Build the serverside version of the shop';

export const buildClient = function buildClient() {
    const cmd = argv.env === 'dev' ? 'yarn run webpack-dev' : 'yarn run webpack-prod';
    return cp.exec(cmd);
};
buildClient.description = 'Build the client version of the shop';

export const buildShopReact = gulp.series(buildClient, buildSsr);

export const buildSeller = function buildSeller() {
    const cmd = argv.env === 'dev' ? 'yarn run webpack-seller-dev' : 'yarn run webpack-seller-prod';
    return cp.exec(cmd);
};
buildSeller.description = 'Builds the seller office, only client side';

export const build = gulp.parallel(buildAdmin, buildSeller, buildShopReact);
build.description = 'Build assets.';

gulp.task('admin', buildAdmin);
gulp.task('admin-watch', watchAdmin);
gulp.task('shop-ssr', buildSsr);
gulp.task('shop-client', buildClient);
gulp.task('seller', buildSeller);

export default build;
