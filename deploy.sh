#!/usr/bin/env bash

rm -rf var/cache/*;
php composer.phar install;
yarn install;
php bin/console doctrine:cache:clear-metadata;
php bin/console doctrine:schema:update --force;
php bin/console cache:clear;
php bin/console cache:clear --env=prod;
php bin/console fos:js-routing:dump --format=json;
yarn run webpack-serverside-prod;
yarn run webpack-prod;