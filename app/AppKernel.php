<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Sylius\Bundle\CoreBundle\Application\Kernel;

/**
 * @author Paweł Jędrzejewski <pawel@sylius.org>
 * @author Gonzalo Vilaseca <gvilaseca@reiss.co.uk>
 */
class AppKernel extends Kernel
{
    /**
     * {@inheritdoc}
     */
    public function registerBundles(): array
    {
        $bundles = [
            new AppBundle\AppBundle(),

            new Sylius\Bundle\AdminBundle\SyliusAdminBundle(),
//            new \Sylius\Bundle\ShopBundle\SyliusShopBundle(),

            new FOS\OAuthServerBundle\FOSOAuthServerBundle(), // Required by SyliusApiBundle
            new Sylius\Bundle\AdminApiBundle\SyliusAdminApiBundle(),

            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new Sg\DatatablesBundle\SgDatatablesBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),

            new Limenius\ReactBundle\LimeniusReactBundle(),
            new Limenius\LiformBundle\LimeniusLiformBundle(),
            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new Gesdinet\JWTRefreshTokenBundle\GesdinetJWTRefreshTokenBundle(),
            new SunCat\MobileDetectBundle\MobileDetectBundle(),

            new Joli\ApacheTikaBundle\ApacheTikaBundle(),

            new Lexik\Bundle\TranslationBundle\LexikTranslationBundle(),

            // v2
//            new SendinBlue\SendinBlueApiBundle\SendinBlueApiBundle(),
            // v3
            new SendinBlue\Bundle\ApiBundle\SendinBlueApiBundle(),

            new Vich\UploaderBundle\VichUploaderBundle(),
            new Snc\RedisBundle\SncRedisBundle(),

            new JMS\JobQueueBundle\JMSJobQueueBundle(),

            new SecIT\ImapBundle\ImapBundle(),
            new Symfony\WebpackEncoreBundle\WebpackEncoreBundle(),

            new ApiPlatform\Core\Bridge\Symfony\Bundle\ApiPlatformBundle(),

            new ApiBundle\ApiBundle(),
//            new Nelmio\CorsBundle\NelmioCorsBundle(),
        ];

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
//            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return array_merge(parent::registerBundles(), $bundles);
    }
}
