<?php

declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200327164650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Ajoute les taux de taxe sur toutes les variantes';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // Ajoute la taxe alimentaire sur tous les produits
        $this->addSql("UPDATE sylius_product_variant SET tax_category_id = 5 WHERE 1");

        // Maintenant on va mettre celle à 20% sur les exceptions
        $this->addSql("
            UPDATE sylius_product_variant
            SET tax_category_id = 6
            WHERE
                  id IN (
                      SELECT spv.id
                      FROM sylius_product_variant spv
                          INNER JOIN sylius_product sp on spv.product_id = sp.id
                          INNER JOIN sylius_product_taxon spt ON sp.id = spt.product_id
                      WHERE
                          spt.taxon_id IN (504, 506, 551, 553, 555)
            );
        ");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("UPDATE sylius_product_variant SET tax_category_id = NULL WHERE 1");
    }
}
