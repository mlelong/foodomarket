<?php

declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200311092257 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Migre les codes clients de SupplierAccountLogs vers une collection de codes plutot qu\'un code unique';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // On modifie la base de données pour rajouter une colonne de type array et supprimer une colonne maintenant inutile
        $this->addSql("ALTER TABLE sylius_supplier_account_log ADD client_codes LONGTEXT NOT NULL COMMENT '(DC2Type:array)', DROP client_code_failover;");

        // On doit mettre les codes clients de "client_code" vers l'array "client_codes", en mode serialisé
        $logs = $this->connection->fetchAll("SELECT id, client_code FROM sylius_supplier_account_log WHERE COALESCE(client_code, '') != '';");

        foreach ($logs as $log) {
            $this->addSql("UPDATE sylius_supplier_account_log SET client_codes = :codes WHERE id = :id;", ['codes' => serialize([$log['client_code']]), 'id' => $log['id']]);
        }

        // On va serialiser un array vide pour ceux qui ont pas de code
        $logs = $this->connection->fetchAll("SELECT id, client_code FROM sylius_supplier_account_log WHERE COALESCE(client_code, '') = '';");

        foreach ($logs as $log) {
            $this->addSql("UPDATE sylius_supplier_account_log SET client_codes = :codes WHERE id = :id;", ['codes' => serialize([]), 'id' => $log['id']]);
        }

        // La migration est faite, on peut supprimer le code client
        $this->addSql("ALTER TABLE sylius_supplier_account_log DROP client_code");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // On ne peut pas migrer en down, car en up on supprime la colonne la plus utile "client_code"
    }
}
