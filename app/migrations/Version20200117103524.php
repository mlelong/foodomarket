<?php

declare(strict_types=1);

namespace Sylius\Migrations;

use DateTime;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200117103524 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Créé les infos de matching pour les nouveaux fournisseurs basés sur le format de mercu Daumesnil';
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $suppliers = [62];
        $variantCache = [];
        $spis = $this->connection->fetchAll("SELECT product_id, label, reference FROM sylius_supplier_product_informations spi WHERE supplier_id = 22");
        $spvis = $this->connection->fetchAll("SELECT product_id, product_variant_id, product_label, variant_label, reference, weight, content, sales_unit, unit_quantity, display_name, order_ref, manually_created FROM sylius_supplier_product_variant_informations WHERE supplier_id = 22");

        foreach ($suppliers as $supplierId) {
            $res = $this->connection->executeQuery("SELECT COUNT(*) AS c FROM sylius_supplier_product_informations WHERE supplier_id = $supplierId")->fetch();

            if ((int)$res['c'] > 0) {
                continue ;
            }

            foreach ($spis as $spi) {
                $sql = "INSERT INTO sylius_supplier_product_informations SET supplier_id = $supplierId";

                foreach ($spi as $field => $value) {
                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                $this->addSql($sql);
            }

            foreach ($spvis as $spvi) {
                $sql = "INSERT INTO sylius_supplier_product_variant_informations SET supplier_id = $supplierId";

                foreach ($spvi as $field => $value) {
                    if ($field === 'product_variant_id') {
                        // On doit dupliquer la variante si on ne l'a pas déjà fait
                        if (!isset($variantCache[$value])) {
                            $variantCache[$value] = $this->duplicateVariant($value);
                        }

                        $value = $variantCache[$value];
                    }

                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                $this->addSql($sql);
            }
        }
    }

    /**
     * @param $variantId
     * @return string
     * @throws DBALException
     */
    private function duplicateVariant($variantId)
    {
        $variant = $this->connection->fetchAssoc("SELECT product_id, tax_category_id, shipping_category_id, code, position, version, on_hold, on_hand, tracked, width, height, depth, weight, shipping_required, enabled FROM sylius_product_variant WHERE id = $variantId");
        $translations = $this->connection->fetchAll("SELECT name, locale FROM sylius_product_variant_translation WHERE translatable_id = $variantId");
        $optionValues = $this->connection->fetchAll("SELECT option_value_id FROM sylius_product_variant_option_value WHERE variant_id = $variantId");

        $sql = "INSERT INTO sylius_product_variant SET product_id = {$variant['product_id']}";

        unset($variant['product_id']);
        $variant['code'] = uniqid();
        $variant['created_at'] = $variant['updated_at'] = (new DateTime())->format('Y-m-d H:i:s');

        foreach ($variant as $field => $value) {
            if ($value !== null) {
                $sql .= ", $field=\"$value\"";
            } else {
                $sql .= ", $field=NULL";
            }
        }

        // Création d'une nouvelle variante
        if ($this->connection->exec($sql)) {
            $newVariantId = $this->connection->lastInsertId($sql);

            // Création des traductions
            foreach ($translations as $translation) {
                $sql = "INSERT INTO sylius_product_variant_translation SET translatable_id = $newVariantId";

                foreach ($translation as $field => $value) {
                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                if (!$this->connection->exec($sql)) {
                    throw new DBALException("Impossible de créer une traduction pour cette variante");
                }
            }

            foreach ($optionValues as $optionValue) {
                $sql = "INSERT INTO sylius_product_variant_option_value SET variant_id = $newVariantId";

                foreach ($optionValue as $field => $value) {
                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                if (!$this->connection->exec($sql)) {
                    throw new DBALException("Impossible de créer les options de variante");
                }
            }

            return $newVariantId;
        } else {
            throw new DBALException("Impossible de créer une nouvelle variante");
        }
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
