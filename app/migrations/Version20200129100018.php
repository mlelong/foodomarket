<?php

declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200129100018 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Duplique le catalogue Top-Atlantique pour le fournisseur Sobomar Atl';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $suppliers = [72];
        $spis = $this->connection->fetchAll("SELECT product_id, label, reference FROM sylius_supplier_product_informations spi WHERE supplier_id = 64");
        $spvis = $this->connection->fetchAll("SELECT product_id, product_variant_id, product_label, variant_label, reference, weight, content, sales_unit, unit_quantity, display_name, order_ref, manually_created FROM sylius_supplier_product_variant_informations WHERE supplier_id = 64");
        $spps = $this->connection->fetchAll("SELECT restaurant_id, product_id, product_variant_id, kg_price, unit_price, channel_code, created_at, updated_at, mercuriale_id, valid_from, valid_to, promo FROM sylius_supplier_product_price WHERE supplier_id = 64");
        $spphs = $this->connection->fetchAll("SELECT restaurant_id, product_id, product_variant_id, kg_price, unit_price, channel_code, created_at, updated_at FROM sylius_supplier_product_price_history WHERE supplier_id = 64");

        foreach ($suppliers as $supplierId) {
            $this->addSql("UPDATE sylius_supplier SET mercurialPattern=\"TopAtlantiqueMercurialPdf\" WHERE id = $supplierId");

            $res = $this->connection->executeQuery("SELECT COUNT(*) AS c FROM sylius_supplier_product_informations WHERE supplier_id = $supplierId")->fetch();

            if ((int)$res['c'] > 0) {
                continue ;
            }

            foreach ($spis as $spi) {
                $sql = "INSERT INTO sylius_supplier_product_informations SET supplier_id = $supplierId";

                foreach ($spi as $field => $value) {
                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                $this->addSql($sql);
            }

            foreach ($spvis as $spvi) {
                $sql = "INSERT INTO sylius_supplier_product_variant_informations SET supplier_id = $supplierId";

                foreach ($spvi as $field => $value) {
                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                $this->addSql($sql);
            }

            foreach ($spps as $spp) {
                $sql = "INSERT INTO sylius_supplier_product_price SET supplier_id = $supplierId";

                foreach ($spp as $field => $value) {
                    if ($field === 'channel_code') {
                        $value = 'foodomarket-sobomaratl';
                    }

                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                $this->addSql($sql);
            }

            foreach ($spphs as $spph) {
                $sql = "INSERT INTO sylius_supplier_product_price_history SET supplier_id = $supplierId";

                foreach ($spph as $field => $value) {
                    if ($field === 'channel_code') {
                        $value = 'foodomarket-sobomaratl';
                    }

                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                $this->addSql($sql);
            }
        }
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM sylius_supplier_product_informations WHERE supplier_id = 72");
        $this->addSql("DELETE FROM sylius_supplier_product_variant_informations WHERE supplier_id = 72");
        $this->addSql("DELETE FROM sylius_supplier_product_price WHERE supplier_id = 72");
        $this->addSql("DELETE FROM sylius_supplier_product_price_history WHERE supplier_id = 72");
    }
}
