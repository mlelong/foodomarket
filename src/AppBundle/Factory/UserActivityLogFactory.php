<?php

namespace AppBundle\Factory;

use AppBundle\Entity\UserActivityLog;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class UserActivityLogFactory implements FactoryInterface
{
    /**
     * @return UserActivityLog
     */
    public function createNew()
    {
        return new UserActivityLog();
    }

    /**
     * @param Request $request
     * @return UserActivityLog
     */
    public function createFromRequest(Request $request)
    {
        $log = $this->createNew();

        $log->setIp($request->getClientIp());
        $log->setUtmSource($request->get('utm_source'));
        $log->setUtmMedium($request->get('utm_medium'));
        $log->setUtmCampaign($request->get('utm_campaign'));
        $log->setUtmContent($request->get('utm_content'));
        $log->setUtmTerm($request->get('utm_term'));
        $log->setUtmEmail($request->get('utm_email'));
        $log->setEmail($request->get('email'));
        $log->setRoute($request->attributes->get('_route'));
        $log->setRouteParams($request->attributes->get('_route_params'));
        $log->setQueryParams($request->query->all());
        $log->setRequestParams($request->request->all());
        $log->setReferer($request->headers->get('referer'));
        $log->setCookieEmail($request->cookies->get('_email'));
        $log->setCookieRestaurant($request->cookies->get('_restaurant'));

        return $log;
    }
}