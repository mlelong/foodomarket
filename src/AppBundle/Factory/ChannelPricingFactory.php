<?php
/**
 * Created by PhpStorm.
 * User: chekib
 * Date: 07/06/17
 * Time: 22:04
 */

namespace AppBundle\Factory;

use AppBundle\Entity\ChannelPricing;
use Sylius\Component\Resource\Factory\FactoryInterface;

class ChannelPricingFactory implements FactoryInterface
{

    /**
     * @return object
     */
    public function createNew()
    {
        return new ChannelPricing();
    }

}