<?php


namespace AppBundle\Util;


use AppBundle\Entity\Supplier;
use Exception;

class SupplierUtil
{
    /**
     * @param Supplier $supplier
     * @param string $postcode
     * @return bool
     */
    private static function isAvailableByPostcode(Supplier $supplier, string $postcode)
    {
        $zones = str_replace(',', "\n", $supplier->getFacadeSupplier()->getDeliveryZones());
        $zones = explode("\n", $zones);

        foreach ($zones as $zone) {
            $zone = trim($zone);

            try {
                // Everywhere
                if ($zone === '*') {
                    return true;
                }

                // Nowhere
                if ($zone === '-') {
                    return false;
                }

                if (strpos($zone, '*') !== false) {
                    $zone = str_replace('*', '', $zone);
                    $zone = "^$zone";
                }

                if (preg_match("/$zone/", $postcode)) {
                    return true;
                }
            } catch (Exception $e) {
            }
        }

        return false;
    }

    /**
     * @param Supplier $supplier
     * @param string $postcode
     * @return float|null
     */
    public static function getDeliveryCost(Supplier $supplier, string $postcode)
    {
        if (self::isAvailableByPostcode($supplier, $postcode)) {
            return 0.0;
        }

        $supplierId = $supplier->getFacadeSupplier()->getId();

        switch ($supplierId) {
            case 79: // Jacob Particulier
            case 83: // Beaugrain
                // Ne livre ni la corse ni les dom tom
                if (preg_match('/^(20|97)/', $postcode)) {
                    return null;
                }

                $AlpesDeHauteProvence = explode(',', '04110,04140,04150,04230,04240,04250,04260,04280,04320,04340,04370,04400,04530');
                $HautesAlpes = explode(',', '05110,05120,05170,05220,05240,05250,05290,05310,05320,05340,05470,05480,05560,05600');
                $AlpesMaritimes = explode(',', '06260,06340,06380,06390,06420,06430,06440,06450,06460,06470,06540,06660,06670,06690,06710,06730,06750,06790,06830,06850,06910,06950');
                $Ariege = explode(',', '09140');
                $Cantal = explode(',', '15300,15400');
                $CharenteMaritime = explode(',', '17111,17123,17190,17310,17370,17410,17480,17550,17580,17590,17630,17650,17670,17740,17840,17880,17940');
                $CotesDArmor = explode(',', '22870');
                $Finistere = explode(',', '29242,29253,29259,29990');
                $HauteGaronne = explode(',', '31110,31350');
                $Isere = explode(',', '38114,38142,38520,38750,38860');
                $Jura = explode(',', '39200,39220,39260,39310,39400');
                $Loire = explode(',', '42660,42920');
                $HauteLoire = explode(',', '43150');
                $Morbihan = explode(',', '56360,56590,56780,56840');
                $PuyDeDome = explode(',', '63150,63240,63610,63650');
                $PyreneesAtlantiques = explode(',', '64130,64440,64570');
                $HautesPyrenees = explode(',', '65110,65120,65200');
                $PyreneesOrientales = explode(',', '66120,66210,66760,66800');
                $Savoie = explode(',', '73120,73130,73150,73210,73300,73320,73440,73450,73480,73530,73550,73620,73640,73700,73710,73870');
                $HauteSavoie = explode(',', '74110,74120,74170,74220,74260,74300,74310,74340,74360,74390,74400,74430,74440,74450,74470,74660,74920');
                $Vendee = explode(',', '85350');

                $exceptions = array_merge(
                    $AlpesDeHauteProvence, $HautesAlpes, $AlpesMaritimes, $Ariege, $Cantal,
                    $CharenteMaritime, $CotesDArmor, $Finistere, $HauteGaronne, $Isere,
                    $Jura, $Loire, $HauteLoire, $Morbihan, $PuyDeDome, $PyreneesAtlantiques,
                    $HautesPyrenees, $PyreneesOrientales, $Savoie, $HauteSavoie, $Vendee
                );

                // Il y a des exceptions ou la livraison est plus chère
                if (in_array($postcode, $exceptions)) {
                    return 14.90;
                }

                // Pour Beaugrain c'est 0, à part les exceptions plus haut
                return $supplierId == 83 ? 0.0 : 9.9;
            default: // Delivery is not supported
                return null;
        }
    }
}
