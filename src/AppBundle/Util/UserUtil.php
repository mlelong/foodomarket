<?php


namespace AppBundle\Util;


use AppBundle\Entity\Customer;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShopUser;
use Exception;

class UserUtil
{
    /**
     * Retrieves the prospect from either:
     * - a prospect (trivial)
     * - a customer
     * - a shop user
     * - a restaurant (if unique)
     *
     * @param object $entity
     * @return Prospect|mixed|null
     * @throws Exception IF the entity is a restaurant, and its relations lead to multiple prospects, or if $entity is invalid
     */
    public static function getProspect(object $entity)
    {
        if ($entity instanceof Prospect) {
            return $entity;
        } elseif ($entity instanceof Customer) {
            return $entity->getProspect();
        } elseif ($entity instanceof ShopUser) {
            return $entity->getCustomer()->getProspect();
        } elseif ($entity instanceof Restaurant) {
            $prospects = [];

            foreach ($entity->getCustomers() as $customer) {
                $prospect = $customer->getProspect();
                $prospects[$prospect->getId()] = $prospect;
            }

            $count = count($prospects);

            if ($count === 1) {
                return current($prospects);
            } elseif ($count === 0) {
                return null;
            } else {
                throw new Exception("Multiple prospects found for restaurant {$entity->getName()} ({$entity->getId()})", 1);
            }
        }

        throw new Exception("Invalid entity type to fetch prospect from !", -1);
    }
}
