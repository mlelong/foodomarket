<?php


namespace AppBundle\Util;


use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;

class PriceModifier
{
    /**
     * Adds:
     * - 10% fees for Jacob and individuals when out of delivery zone (when ok)
     * - 5.5% fees for individual to get ttc price (this is very dirty)
     *
     * @param float $amount
     * @param Prospect $client
     * @param string $zipCode
     * @param Supplier $supplier
     * @param bool $asInt
     * @return float
     */
    public static function modify(float $amount, Prospect $client, string $zipCode, Supplier $supplier, bool $asInt = false)
    {
        $clientType = $client->getType();

        // Jacob particulier
//        if ($supplier->getFacadeSupplier()->getId() === 79) {
//            $zones = explode("\n", $supplier->getDeliveryZones());
//            $outOfZone = true;
//
//            foreach ($zones as $zone) {
//                if (preg_match("/^$zone/", $zipCode)) {
//                    $outOfZone = false;
//                    break ;
//                }
//            }
//
//            if ($outOfZone) {
////                $amount *= 1.1;
//            }
//        }

        // Si c'est un particulier, ou si c'est un pro en mode paiement CB online, on ajoute la TVA
        if ($clientType === Prospect::TYPE_INDIVIDUAL || $client->getAccountStatus($supplier) === 'ORDERING') {
            $amount *= 1.055;
        }

        if ($asInt) {
            return (int)$amount;
        }

        return floatval(number_format($amount, 2, '.', ''));
    }
}
