<?php

namespace AppBundle\Util;

class BestSimilars
{
    /** @var mixed */
    private $value;
    /** @var int */
    private $batchSize;
    /** @var array */
    private $batch = [];
    /** @var callable */
    private $comparator;
    /** @var bool */
    private $isSorted = false;

    public function __construct($value, int $batchSize, callable $comparator)
    {
        $this->value = $value;
        $this->batchSize = $batchSize;
        $this->comparator = $comparator;
    }

    public function add($value)
    {
        $score = \Closure::fromCallable($this->comparator)->call($this, $value);

        $this->batch[] = [
            'score' => $score,
            'value' => $value
        ];

        $this->isSorted = false;

        if (count($this->batch) > $this->batchSize) {
            $this->sort();
            array_pop($this->batch);
        }
    }

    private function sort()
    {
        if (!$this->isSorted) {
            $this->isSorted = usort($this->batch, function ($a, $b) {
                return $a['score'] < $b['score'];
            });
        }
    }

    public function getBestValues()
    {
        $this->sort();

        return array_map(function($item) {
            return $item['value'];
        }, $this->batch);
    }

    public function getBests()
    {
        $this->sort();

        return $this->batch;
    }
}