<?php

namespace AppBundle\SellerOffice\Controller;

use AppBundle\Entity\Email;
use AppBundle\Entity\Litigation;
use AppBundle\Entity\LitigationComment;
use AppBundle\Entity\Supplier;
use AppBundle\MiddleOffice\Controller\Traits\FormHelper;
use AppBundle\Repository\LitigationRepository;
use AppBundle\Services\LitigationService;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueV3Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class LitigationController extends AbstractController
{
    use FormHelper;

    public function index()
    {
        /** @var Supplier|null $supplier */
        $supplier = $this->getUser();

        return $this->render('AppBundle:SellerOffice/Litigation:index.html.twig', [
            'props' => [
                'litigations' => $supplier !== null
                    ? $this->get('app.service.litigation')->getLitigationsBySupplier($supplier)
                    : []
            ]
        ]);
    }

    public function show(string $hash)
    {
        $litigationService = $this->get('app.service.litigation');
        /** @var Litigation $object */
        $object = $this->get('app.repository.litigation')->findOneBy(['hash' => $hash]);
        $litigation = $litigationService->getLitigation($object);
        $litigationCommentForm = $this->liformize($litigationService->getLitigationCommentForm());

        return $this->render('AppBundle:SellerOffice/Litigation:show.html.twig', [
            'props' => [
                'litigation' => $litigation,
                'litigationCommentForm' => $litigationCommentForm,
                'isMobile' => false
            ]
        ]);
    }

    public function addComment(Request $request)
    {
        $hash = $request->get('hash');
        /** @var Litigation $litigation */
        $litigation = $this->get('app.repository.litigation')->findOneBy(['hash' => $hash]);
        $litigationService = $this->get('app.service.litigation');

        $form = $litigationService->getLitigationCommentForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var LitigationComment $litigationComment */
            $litigationComment = $form->getData();

            $litigationService->addComment($litigation, $litigationComment, $this->getUser());

            $email = $this
                ->get('app.factory.email')
                ->createBuilder(Email::MANAGER_SENDINBLUE)
                ->addTo($litigationComment->getLitigation()->getProspect()->getEmail())
                ->addBcc('olivier@foodomarket.com')
                ->addReplyTo('olivier@foodomarket.com')
                ->addVariable('URL', $this->generateUrl('app_front_litigation_show_page', ['id' => $litigationComment->getLitigation()->getId()], Router::ABSOLUTE_URL))
                ->addVariable('FIRSTNAME', $litigationComment->getLitigation()->getProspect()->getFirstName())
                ->addVariable('MESSAGE', nl2br($litigationComment->getContent()))
                ->addVariable('SUPPLIER', $litigationComment->getLitigation()->getSupplier()->getName())
                ->setTemplateId(43)
                ->build()
            ;

            $this->get('app.sendinblue.manager')->addEmailToQueue($email);

            // Resets the form for a clear redraw
            $form = $litigationService->getLitigationCommentForm();

            return $this->json(['ok' => true, 'form' => $this->liformize($form), 'comment' => $litigationService->getLitigationComment($litigationComment)]);
        }

        return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
    }

    public static function getSubscribedServices()
    {
        $services = parent::getSubscribedServices();

        return array_merge($services, [
            'app.service.litigation' => LitigationService::class,
            'app.repository.litigation' => LitigationRepository::class,
            'app.factory.email' => EmailFactory::class,
            'app.sendinblue.manager' => SendInBlueV3Manager::class
        ]);
    }
}
