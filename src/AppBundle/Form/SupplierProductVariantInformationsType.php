<?php

/**
 * Created by PhpStorm.
 * User: chekib
 * Date: 26/05/17
 * Time: 18:46
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\SupplierProductVariantInformations;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Bundle\TaxonomyBundle\Form\Type\TaxonAutocompleteChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductAutocompleteChoiceType;
use AppBundle\Form\ProductVariantAutocompleteChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionValueChoiceType;
use Sylius\Component\Product\Repository\ProductOptionRepositoryInterface;

class SupplierProductVariantInformationsType extends AbstractType
{

    /**
     * @var ProductOptionRepositoryInterface
     */
    private $productOptionRepository;

    /**
     * ImportLineType constructor.
     * @param ProductOptionRepositoryInterface $productOptionRepository
     */
    public function __construct(ProductOptionRepositoryInterface $productOptionRepository)
    {
        $this->productOptionRepository = $productOptionRepository;

        $this->optionMap['UM'] = 'salesUnit';
        $this->optionMap['OP'] = 'originCountry';
        $this->optionMap['OV'] = 'origin';
        $this->optionMap['CA'] = 'caliber';
        $this->optionMap['CO'] = 'conditioning';
        $this->optionMap['AP'] = 'naming';
        $this->optionMap['VA'] = 'variety';
        $this->optionMap['CC'] = 'category';
        $this->optionMap['UQ'] = 'unitQuantity';
        $this->optionMap['UCQ'] = 'unitContentQuantity';
        $this->optionMap['UCM'] = 'unitContentMeasurement';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $informations = $options['data'];

        $builder

            ->add('manuallyCreated', ChoiceType::class, [
                'label'     => 'Création Manuelle',
                'expanded'  => true,
                'choices'   => array('OUI' => '1', 'NON' => '0'),
            ])

            ->add('reference', TextType::class, [
                'label'     => 'Référence fournisseur',
                'required'  => false,
            ])

            ->add('productLabel', TextType::class, [
                'label' => 'Libellé fichier',
            ])

            ->add('taxon', TaxonAutocompleteChoiceType::class, [
                'label'         => 'Taxon',
                'mapped'        => false,
                'data_class'    => null,
                'data'          => $informations->getProduct()->getMainTaxon(),
                ])

            ->add('product', ProductAutocompleteChoiceType::class, ['label' => 'Produit', 'required' => false])
            //->add('productVariant', ProductVariantAutocompleteChoiceType::class, ['label' => 'Variante', 'required' => false])

            ->add('weight', TextType::class, [
                'label' => 'Poids',
            ])

            ->add('unitQuantity', NumberType::class, [
                'label' => 'Quantité unitaire',
            ])

            /*->add('salesUnit', TextType::class, [
                'label' => 'Unité de mesure Mercurial',
            ])
           */

            ->add('salesUnit', ProductOptionValueChoiceType::class, array_merge([
                        'label'     => 'Unité de mesure Mercuriale',
                        'required'  => false,
                        'mapped'    => false,
                        ],
                        $this->getSelectedOptionValue('UM', $informations->getProductVariant()->getOptionSupplierSalesUnit())
                    ))

            ->add('unitQuantity', TextType::class, array_merge([
                        'attr' => ['class' => 'sensitive-field'],
                        'label'     => 'Quantité unitaire',
                        'required'  => false,
                        'mapped'    => false,
                        'data'      => $informations->getProductVariant()->getOptionUnitQuantity()
                    ]
                    ))

            ->add('unitContentQuantity', ProductOptionValueChoiceType::class, array_merge([
                        'attr'      => ['class' => 'sensitive-field'],
                        'label'     => 'Quantité du contenu',
                        'required'  => false,
                        'mapped'    => false,
                    ],
                        $this->getSelectedOptionValue('UCQ', $informations->getProductVariant()->getOptionUnitContentQuantity())
                    ))

            ->add('unitContentMeasurement', TextType::class, array_merge([
                        'attr'      => ['class' => 'sensitive-field'],
                        'label'     => 'Unité de mesure du contenu',
                        'required'  => false,
                        'mapped'    => false,
                        'data'      => $informations->getProductVariant()->getOptionUnitQuantity()
                    ]
                    ))

            ->add('originCountry', ProductOptionValueChoiceType::class, array_merge([
                        'attr'      => ['class' => 'sensitive-field'],
                        'label'     => 'Pays d\'origine',
                        'required'  => false,
                        'mapped'    => false,
                    ],
                        $this->getSelectedOptionValue('OP', $informations->getProductVariant()->getOptionOriginCountry())
                    ))

            ->add('caliber', ProductOptionValueChoiceType::class, array_merge([
                        'label'     => 'Calibre',
                        'required'  => false,
                        'mapped'    => false,
                    ],
                        $this->getSelectedOptionValue('CA', $informations->getProductVariant()->getOptionCaliber())
            ))

            ->add('conditionning', ProductOptionValueChoiceType::class, array_merge([
                        'label'     => 'Conditionnement',
                        'required'  => false,
                        'mapped'    => false,
                    ],
                        $this->getSelectedOptionValue('CO', $informations->getProductVariant()->getOptionConditionning())
            ))

            ->add('category', ProductOptionValueChoiceType::class, array_merge([
                        'label'     => 'Catégorie',
                        'required'  => false,
                        'mapped'    => false,
                    ],
                        $this->getSelectedOptionValue('CC', $informations->getProductVariant()->getOptionCategory())
            ))

            ->add('variety', ProductOptionValueChoiceType::class, array_merge([
                        'label'     => 'Variété',
                        'required'  => false,
                        'mapped'    => false,
                    ],
                        $this->getSelectedOptionValue('VA', $informations->getProductVariant()->getOptionVariety())
            ))

            ->getForm();
    }

    private function getSelectedOptionValue(string $code, ?string $value) {
        /** @var ProductOptionInterface $option */
        $option = $this->productOptionRepository->findOneBy(['code' => $code]);
        if (!$option) {
            return null;
        }
        $optionValues = $option->getValues();
        $matchingValue = null;

        foreach ($optionValues as $optionValue) {
            if ($optionValue->getCode() == $value) {
                $matchingValue = $optionValue;
                break ;
            }
        }

        return array('option' => $option, 'data' => $matchingValue);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SupplierProductVariantInformations::class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vitalis_supplier_product_variant_informations_type';
    }
}
