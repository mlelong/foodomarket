<?php

namespace AppBundle\Form\Liform\Serializer\Normalizer;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormView;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Limenius\Liform\FormUtil;

/**
 * Normalize instances of FormView
 */
class InitialValuesNormalizer implements NormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function normalize($form, $format = null, array $context = [])
    {
        $formView = $form->createView();
        return $this->getValues($form, $formView);
    }

    private function getValues(Form $form, FormView $formView)
    {
        if (!empty($formView->children)) {
            if (in_array('choice', FormUtil::typeAncestry($form)) &&
                $formView->vars['expanded']
            ) {
                if ($formView->vars['multiple']) {
                    return $this->normalizeMultipleExpandedChoice($formView);
                } else {
                    return $this->normalizeExpandedChoice($formView);
                }
            } elseif (in_array('collection', FormUtil::typeAncestry($form))) {
                $data = array();
                foreach ($formView->children as $name => $child) {
                    // Skip empty values because
                    // https://github.com/erikras/redux-form/issues/2149
                    if (empty($child->children) && ($child->vars['value'] === null || $child->vars['value'] === '')) {
                        continue;
                    }
                    $data[$name] = $this->getValues($form[$name], $child);
                }
            } else {
                // Force serialization as {} instead of []
                $data = (object)array();
                foreach ($formView->children as $name => $child) {
                    // Skip empty values because
                    // https://github.com/erikras/redux-form/issues/2149
                    if (empty($child->children) && ($child->vars['value'] === null || $child->vars['value'] === '')) {
                        continue;
                    }
                    $data->{$name} = $this->getValues($form[$name], $child);
                }
            }

            return $data;
        } else {
            // handle separatedly the case with checkboxes, so the result is
            // true/false instead of 1/0
            if (isset($formView->vars['checked'])) {
                return $formView->vars['checked'];
            }

            if ($formView->vars['value'] instanceof Collection) {
                return $formView->vars['value']->toArray();
            }

            return $formView->vars['value'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Form;
    }

    private function normalizeMultipleExpandedChoice($formView)
    {
        $data = array();
        foreach ($formView->children as $name => $child) {
            if ($child->vars['checked']) {
                $data[] = $child->vars['value'];
            }
        }
        return $data;
    }

    private function normalizeExpandedChoice($formView)
    {
        foreach ($formView->children as $name => $child) {
            if ($child->vars['checked']) {
                return $child->vars['value'];
            }
        }
        return null;
    }
}
