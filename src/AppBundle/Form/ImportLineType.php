<?php

namespace AppBundle\Form;

use AppBundle\Datatables\ImportLineDatatable;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\ProductVariant;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sonata\CoreBundle\Form\Type\ImmutableArrayType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionValueChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionValueCollectionType;
use Sylius\Bundle\TaxonomyBundle\Form\Type\TaxonAutocompleteChoiceType;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValue;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Product\Model\ProductOptionValueTranslation;
use Sylius\Component\Product\Repository\ProductOptionRepositoryInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportLineType extends AbstractType
{
    /**
     * @var ProductOptionRepositoryInterface
     */
    private $productOptionRepository;

    /**
     * @var array
     */
    private $optionMap = [];

    /**
     * ImportLineType constructor.
     * @param ProductOptionRepositoryInterface $productOptionRepository
     */
    public function __construct(ProductOptionRepositoryInterface $productOptionRepository)
    {
        $this->productOptionRepository = $productOptionRepository;

        $this->optionMap['UM'] = 'salesUnit';
        $this->optionMap['OP'] = 'originCountry';
        $this->optionMap['OV'] = 'origin';
        $this->optionMap['CA'] = 'caliber';
        $this->optionMap['CO'] = 'conditioning';
        $this->optionMap['AP'] = 'naming';
        $this->optionMap['VA'] = 'variety';
        $this->optionMap['CC'] = 'category';
        $this->optionMap['UQ'] = 'unitQuantity';
        $this->optionMap['UCQ'] = 'unitContentQuantity';
        $this->optionMap['UCM'] = 'unitContentMeasurement';
    }

    private function prepareOptionValues(FormEvent $event): Collection {
        /** @var ImportLine $importLine */
        $importLine = $event->getData();
        $optionValues = $importLine->getOptionValues();

        /** @var ProductOptionInterface[] $supportedOptions */
        $supportedOptions = $this->productOptionRepository->findAll();

        if ($optionValues->isEmpty() && $importLine->getProduct() !== null) {
            foreach ($supportedOptions as &$supportedOption) {
                /** @var ProductOptionInterface $umOption */
                $umOptionValue = new ProductOptionValue();
                $umOptionValue->setCurrentLocale('fr_FR');

                foreach ($supportedOption->getValues() as $optionValue) {
                    if ($optionValue->getValue() == $event->getForm()->get('content')->get($this->optionMap[$optionValue->getOptionCode()])->getData()) {
                        $umOptionValue = $optionValue;
                        break ;
                    }
                }

                $optionValues->add($umOptionValue);
            }
        }

        return $optionValues;
    }

    private function getSelectedOptionValue(string $code, ?string $value) {
        /** @var ProductOptionInterface $option */
        $option = $this->productOptionRepository->findOneBy(['code' => $code]);
        $optionValues = $option->getValues();
        $matchingValue = null;

        foreach ($optionValues as $optionValue) {
            if ($optionValue->getCode() == $value) {
                $matchingValue = $optionValue;
                break ;
            }
        }

        return array('option' => $option, 'data' => $matchingValue);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ImportLine $importLine */
        $importLine = $options['data'];

        $builder
            ->add('id', HiddenType::class)
            ->add('content', ImmutableArrayType::class, array(
                'keys' => array(
                    array('reference', TextType::class, ['required' => false, 'label' => 'Référence', 'disabled' => true]),
                    array('supplierName', TextType::class, ['label' => 'Libellé fichier', 'disabled' => true, 'required' => false]),
                    array('price', IntegerType::class, ['label' => 'Prix fichier', 'disabled' => true, 'required' => false]),
                    array('productName', TextType::class, ['label' => 'Libellé produit', 'required' => true, 'attr' => ['class' => 'sensitive-field']]),
                    array('variantName', TextType::class, ['label' => 'Libellé variante', 'required' => true, 'attr' => ['class' => 'sensitive-field']]),
                    array('taxon', TextType::class, ['label' => 'Déterminant taxon/catégorie', 'required' => true, 'attr' => ['class' => 'sensitive-field']]),
                    array('weight', TextType::class, [ 'label' => 'Poids', 'required' => true, 'attr' => ['class' => 'sensitive-field']]),
                    array('salesUnit', TextType::class, ['label' => 'Unité de facturation', 'required' => true, 'attr' => ['class' => 'sensitive-field']]),
                    array('priceKg', IntegerType::class, ['label' => 'Prix au kilo', 'required' => true, 'attr' => ['class' => 'sensitive-field']]),
                    array('priceVariant', TextType::class, ['label' => 'Prix de vente', 'required' => true, 'attr' => ['class' => 'sensitive-field']]),

                    array('caliber', TextType::class, ['label' => 'Calibre', 'required' => false]),
                    array('variety', TextType::class, ['label' => 'Variété', 'required' => false]),
                    array('brand', TextType::class, ['label' => 'Marque', 'required' => false]),

                    array('salesUnit', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Unité de mesure Mercuriale',
                        'required' => true,
                        'attr' => ['class' => 'sensitive-field']],
                        $this->getSelectedOptionValue('UM', $importLine->getSalesUnit())
                    )),
                    array('originCountry', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Pays d\'origine',
                        'required' => false],
                        $this->getSelectedOptionValue('OP', $importLine->getOriginCountry())
                    )),
                    array('conditioning', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Conditionnement',
                        'required' => false],
                        $this->getSelectedOptionValue('CO', $importLine->getConditioning())
                    )),
                    array('origin', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Origine',
                        'required' => false],
                        $this->getSelectedOptionValue('OV', $importLine->getOrigin())
                    )),
                    array('naming', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Appellation',
                        'required' => false],
                        $this->getSelectedOptionValue('AP', $importLine->getNaming())
                    )),
                    array('category', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Catégorie',
                        'required' => false],
                        $this->getSelectedOptionValue('CC', $importLine->getCategory())
                    )),
                    array('bio', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Bio',
                        'required' => false],
                        $this->getSelectedOptionValue('BI', $importLine->getBio())
                    )),
                    array('orderUnit', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Unité de commande',
                        'required' => true],
                        $this->getSelectedOptionValue('UC', $importLine->getOrderUnit())
                    )),
                    array('unitQuantity', TextType::class, [
                        'label' => 'Nombre d\'unités',
                        'required' => true,
                        'attr' => ['class' => 'sensitive-field'],
                        'data' => $importLine->getUnitQuantity()
                        ])
                    ,
                    array('unitContentQuantity', TextType::class, [
                        'label' => 'Contenu de l\'unité',
                        'required' => true,
                        'data' => $importLine->getUnitContentQuantity()
                        ])
                    ,
                    array('unitContentMeasurement', ProductOptionValueChoiceType::class, array_merge([
                        'label' => 'Unité de mesure du contenu',
                        'required' => true],
                        $this->getSelectedOptionValue('UCM', $importLine->getUnitContentMeasurement())
                    )),
                ),
                'label' => false,
                )
            )
            ->add('taxon', TaxonAutocompleteChoiceType::class, ['required' => true, 'attr' => ['class' => 'taxon-importline']])
            ->add('submit', SubmitType::class, ['label' => 'Importer au catalogue', 'attr' => ['class' => 'ui button green submit-importline']])
            ->add('validateLine', SubmitType::class, ['label' => 'Sauvegarder', 'attr' => ['class' => 'ui button primary update-importline']])
        ;

        $builder->get('content')
            ->addModelTransformer(new CallbackTransformer(
                function ($contentAsArray) {
                    // transform the array to a string
                    return unserialize($contentAsArray);
                },
                function ($contentAsString) {
                    foreach ($contentAsString as $param => $value) {
                        if ($value instanceof ProductOptionValueInterface) {
//                            $contentAsString[$param] = $value->getValue();
                            $contentAsString[$param] = $value->getCode();
                        }
                    }

                    // transform the string back to an array
                    return serialize($contentAsString);
                }
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ImportLine::class,
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'vitalis_import_line_type';
    }
}
