<?php

namespace AppBundle\Form\Extension;

use Sylius\Bundle\OrderBundle\Form\Type\CartItemType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

final class CartItemTypeExtension extends AbstractTypeExtension
{
    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return CartItemType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('quantity', NumberType::class, [
            'attr' => ['min' => 1],
            'label' => 'sylius.ui.quantity',
        ]);
    }

    public static function getExtendedTypes()
    {
        return [CartItemType::class];
    }
}