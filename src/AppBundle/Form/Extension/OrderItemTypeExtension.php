<?php

namespace AppBundle\Form\Extension;

use Sylius\Bundle\OrderBundle\Form\Type\OrderItemType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderItemTypeExtension extends AbstractTypeExtension
{
    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return OrderItemType::class;
    }

    public static function getExtendedTypes()
    {
        return [OrderItemType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', NumberType::class, [
                'attr' => ['min' => 1],
                'label' => 'sylius.ui.quantity',
            ])
        ;
    }
}