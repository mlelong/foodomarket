<?php

namespace AppBundle\Form\Extension;

use AppBundle\Entity\ImportLine;
use AppBundle\Form\ImportLineType;
use AppBundle\Services\Matching;
use AppBundle\Services\ProductManager;
use Sylius\Bundle\TaxonomyBundle\Form\Type\TaxonAutocompleteChoiceType;
use Sylius\Component\Product\Model\Product;
use Sylius\Component\Taxonomy\Model\Taxon;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class MatchingExtension extends AbstractTypeExtension
{
    /** @var ProductManager  */
    protected $productManager;

    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return ImportLineType::class;
    }

    public static function getExtendedTypes()
    {
        return [ImportLineType::class];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(
                FormEvents::POST_SET_DATA,
                array($this, 'onPostSetData')
            )
        ;
    }

    public function onPostSetData(FormEvent $event)
    {
        $data = unserialize($event->getData());
//        $import = $event->getForm()->getParent()->getParent()->getData();

        /** @var ImportLine $line */
        $line = $event->getData();
        $import = $line->getImport();

        /** @var \Sylius\Component\Core\Model\Product $product */
        $product = $line->getProduct();

        if ($product === null)
            $product = $this->productManager->getMatchingProduct($import->getSupplier(), $data['supplierName']);

        $taxon = $line->getTaxon();

        if ($taxon === null) {
            if ($product !== null)
                $taxon = $product->getMainTaxon();
            else
                $taxon = $this->productManager->getMatchingTaxon($data['taxon']);
        }

        $products = $taxon !== null
            ? $this->productManager->findMatchingProducts($taxon->getCode(), $data['originCountry'])
            : $this->productManager->findAll()
        ;

        if ($product) {
            $event->getForm()->add('product', EntityType::class, [
                'label' => 'Produit',
                'class' => Product::class,
                'disabled' => false,
//                'choices' => [$product],
                'choices' => $products,
                'data' => $product,
                'attr' => ['class' => 'product-importline'],
                'required' => false
        ]);
        } else {
            $event->getForm()->add('product', EntityType::class, [
                'label' => 'Produit',
                'class' => Product::class,
                'empty_data' => null,
                'placeholder' => 'select one product',
//                'choices' => $this->productManager->findMatchingProducts($data['taxon'], $data['originCountry']),
                'choices' => $products,
                'attr' => ['class' => 'product-importline'],
                'required' => false
            ]);
        }

        if (!$taxon) {
/*        if ($taxon) {
            $event->getForm()->add('taxon_select', EntityType::class, [
                'mapped' => false,
                'class' => Taxon::class,
                'disabled' => true,
                'choices' => [$taxon],
            ]);
        } else {*/
            $event->getForm()->remove('taxon');
            $event->getForm()->add('taxon', TaxonAutocompleteChoiceType::class, ['required' => true, 'attr' => ['class' => 'taxon-importline']]);
        } else {
            $line->setTaxon($taxon);
        }
    }

    /**
     * Pass the matched entity to the view
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $data = unserialize($form->getData());
//        $import = $form->getParent()->getParent()->getData();
        $import = $form->getData()->getImport();

        $product = $this->productManager->getMatchingProduct($import->getSupplier(), $data['productName']);

        if ($product) {
            $view->vars['matched_product'] = $product;
        } else {
            $view->vars['matching_products'] = $this->productManager->findMatchingProducts($data['taxon'], $data['originCountry']);
        }

        $taxon = $this->productManager->getMatchingTaxon($data['taxon']);
        if ($taxon) {
            $view->vars['matched_taxon'] = $taxon;
        }

        $productAttribute = $this->productManager->getMatchingProductAttribute('origin_country', $data['originCountry']);
        if ($productAttribute) {
            $view->vars['matched_productAttribute'] = $productAttribute;
        }
    }
}
