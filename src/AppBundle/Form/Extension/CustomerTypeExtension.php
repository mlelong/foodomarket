<?php


namespace AppBundle\Form\Extension;

use AppBundle\Form\RestaurantType;
use Sylius\Bundle\CustomerBundle\Form\Type\CustomerType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

final class CustomerTypeExtension extends AbstractTypeExtension
{
    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return CustomerType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $builder
//            ->add('restaurants', CollectionType::class, [
//                'required' => false,
//                'allow_add' => true,
//                'allow_delete' => true,
//                'label' => 'Restaurants',
//                'entry_type' => RestaurantType::class
//            ])
//        ;
    }

    public static function getExtendedTypes()
    {
        return [CustomerType::class];
    }
}
