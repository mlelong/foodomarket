<?php

namespace AppBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Core\Model\Channel;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ChannelToCodeTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * ChannelToCodeTransformer constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (Channel) to a string (channelCode).
     *
     * @param  Channel|null $channel
     *
     * @return string
     */
    public function reverseTransform($channel)
    {
        if (null === $channel) {
            return '';
        }

        return $channel->getCode();
    }

    /**
     * Transforms a string (channelCode) to an object (Channel).
     *
     * @param  string $channelCode
     *
     * @return Channel|null
     *
     * @throws TransformationFailedException if object (Channel) is not found.
     */
    public function transform($channelCode)
    {
        // no issue number? It's optional, so that's ok
        if (!$channelCode) {
            return;
        }

        $channel = $this->em
            ->getRepository(Channel::class)
            // query for the channel with this code
            ->findOneByCode($channelCode)
        ;

        if (null === $channel) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An Channel with code "%s" does not exist!',
                $channelCode
            ));
        }

        return $channel;
    }
}
