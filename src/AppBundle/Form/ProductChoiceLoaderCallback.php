<?php

namespace AppBundle\Form;

use AppBundle\Repository\ProductRepository;
use Sylius\Component\Core\Model\Product;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;

class ProductChoiceLoaderCallback extends CallbackChoiceLoader
{
    /** @var ProductRepository */
    private $productRepository;

    /**
     * @param ProductRepository $productRepository
     */
    public function setProductRepository(ProductRepository $productRepository): void
    {
        $this->productRepository = $productRepository;
    }

    public function loadChoicesForValues(array $values, $value = null)
    {
        $choices = [];

        foreach ($values as $value) {
            /** @var Product $product */
            $product = $this->productRepository->find($value);

            if ($product !== null) {
                $choices[] = [
                    'name' => $product->getName(),
                    'id' => "{$product->getId()}"
                ];
            }
        }

        return $choices;
    }

    public function loadValuesForChoices(array $choices, $value = null)
    {
        $values = [];

        foreach ($choices as $choice) {
            $values[] = "{$choice['id']}";
        }

        return $values;
    }
}