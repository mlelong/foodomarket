<?php

namespace AppBundle\Form;

use AppBundle\Entity\Supplier;
use AppBundle\Repository\SupplierRepository;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;

class SupplierChoiceLoaderCallback extends CallbackChoiceLoader
{
    /** @var SupplierRepository */
    private $supplierRepository;

    /**
     * @param SupplierRepository $supplierRepository
     */
    public function setSupplierRepository(SupplierRepository $supplierRepository): void
    {
        $this->supplierRepository = $supplierRepository;
    }

    public function loadChoicesForValues(array $values, $value = null)
    {
        $choices = [];

        foreach ($values as $value) {
            /** @var Supplier $supplier */
            $supplier = $this->supplierRepository->find($value);

            if ($supplier !== null) {
                $choices[] = [
                    'name' => $supplier->getName(),
                    'id' => "{$supplier->getId()}"
                ];
            }
        }

        return $choices;
    }

    public function loadValuesForChoices(array $choices, $value = null)
    {
        $values = [];

        foreach ($choices as $choice) {
            $values[] = "{$choice['id']}";
        }

        return $values;
    }
}