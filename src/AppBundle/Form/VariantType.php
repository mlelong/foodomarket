<?php

/**
 * Created by PhpStorm.
 * User: chekib
 * Date: 26/05/17
 * Time: 18:46
 */

namespace AppBundle\Form;

use Sylius\Component\Core\Model\ProductVariant;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VariantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'sylius.form.variant.name',
            ])

            ->add('weight', TextType::class, [
                'label' => 'sylius.form.weight',
            ])

            ->getForm();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProductVariant::class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vitalis_variant_type';
    }
}
