<?php

namespace AppBundle\Form;

use AppBundle\Entity\Import;
use AppBundle\Entity\Restaurant;
use Sylius\Component\Core\Model\Channel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pricesDate', DateType::class, [
                'label' => 'sylius.form.import.prices.date',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'disabled' => false,
                'html5' => true,
            ])
            ->add('filename', TextType::class, [
                'label' => 'sylius.form.import.filename',
            ])
            ->add('fetcher', TextType::class, [
                'label' => 'sylius.form.import.fetcher',
            ])
            ->add('channel', EntityType::class, [
                'label' => 'sylius.form.import.channel',
                'class' => Channel::class,
            ])
            ->add('Restaurant', EntityType::class, [
                'label' => 'sylius.form.import.restaurant',
                'class' => Restaurant::class,
                'required' => false,
            ])
//            ->add('lines', CollectionType::class, [
//                'entry_type' => ImportLineType::class
//            ])
            ->getForm();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Import::class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vitalis_import_type';
    }
}
