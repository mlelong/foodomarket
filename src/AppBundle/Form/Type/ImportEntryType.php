<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\MercurialEntry;
use AppBundle\Entity\ProductVariant;
use AppBundle\Form\ProductVariantAutocompleteChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductAutocompleteChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionValueCollectionType;
use Sylius\Bundle\TaxonomyBundle\Form\Type\TaxonAutocompleteChoiceType;
use Sylius\Component\Product\Repository\ProductOptionRepositoryInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportEntryType extends AbstractType
{
    /**
     * @var ProductOptionRepositoryInterface
     */
    private $productOptionRepository;

    /**
     * ImportEntryType constructor.
     * @param ProductOptionRepositoryInterface $productOptionRepository
     */
    public function __construct(ProductOptionRepositoryInterface $productOptionRepository)
    {
        $this->productOptionRepository = $productOptionRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, ['disabled' => true])
            ->add('reference', TextType::class, ['label' => 'Référence', 'disabled' => true])
            ->add('fileLabel', TextType::class, ['label' => 'Libellé fichier', 'disabled' => true])
            ->add('weight', IntegerType::class, ['label' => 'Poids'])
            ->add('units', IntegerType::class, ['label' => 'Nombre de pièce(s)'])
            ->add('filePrice', IntegerType::class, ['label' => 'Prix fichier', 'disabled' => true])
            ->add('kgPrice', IntegerType::class, ['label' => 'Prix au kilo'])
            ->add('unitPrice', IntegerType::class, ['label' => 'Prix de vente'])
            ->add('taxon', TaxonAutocompleteChoiceType::class, ['label' => 'Taxon'])
            ->add('product', ProductAutocompleteChoiceType::class, ['label' => 'Produit', 'required' => false])
            ->add('productVariant', ProductVariantAutocompleteChoiceType::class, ['label' => 'Variante', 'required' => false])
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event) {
            /** @var MercurialEntry $importLine */
            $importLine = $event->getData();

            if ($importLine->getProduct() != null) {
                $optionValues = $this->prepareOptionValues($event);

                $event->getForm()->add('productVariant', EntityType::class, [
                    'required' => false,
                    'label' => 'Variante',
                    'class' => ProductVariant::class,
                    'placeholder' => '---',
                    'choices' => $importLine->getProduct()->getVariants(),
                    'choice_label' => 'name',
                    'attr' => ['class' => 'variant-importline']
                ]);

                $event->getForm()->add('options', ProductOptionValueCollectionType::class, [
                    'label' => false,
                    'mapped' => false,
                    'options' => $this->productOptionRepository->findAll(),
                    'data' => $optionValues,
                    'required' => false
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MercurialEntry::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'import_entry';
    }

}