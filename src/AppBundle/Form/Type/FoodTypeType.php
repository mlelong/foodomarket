<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Services\RedisCacheManager;
use Doctrine\ORM\EntityRepository;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;

class FoodTypeType extends AbstractResourceType
{
    /**
     * @var RedisCacheManager
     */
    private $cacheManager;

    public function __construct(string $dataClass, $validationGroups = [], RedisCacheManager $cacheManager)
    {
        parent::__construct($dataClass, $validationGroups);

        $this->cacheManager = $cacheManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => FoodTypeTranslationType::class,
            ])
            ->add('images', CollectionType::class, [
                'entry_type' => FoodTypeImageType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => false,
            ])
            ->add('position', NumberType::class, [
                'required' => true,
            ])
            ->add('variants', EntityType::class, [
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'expanded' => false,
                'class' => ProductVariant::class,
                'attr' => [
                    'class' => 'fluid search'
                ],
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->select('spp')
                        ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.productVariant = v')
                        ->where('spp.restaurant = 4')
                        ->andWhere('v.enabled = 1')
                    ;
                },
                'choice_label' => function(SupplierProductPrice $price) {
                    /** @var ProductVariant $variant */
                    $variant = $price->getProductVariant();
                    /** @var ProductOptionValue $salesUnit */
                    $salesUnit = $variant->getOptionSupplierSalesUnit();

                    if ($salesUnit === false) {
                        $salesUnit = 'KG';
                        $primitivePrice = bcdiv($price->getKgPrice(), 100, 2);
                    } else {
                        switch ($salesUnit->getCode()) {
                            case 'KG':
                            case 'L':
                                $salesUnit = 'KG';
                                $primitivePrice = bcdiv($price->getKgPrice(), 100, 2);
                                break;
                            default:
                                $salesUnit = 'PC';
                                $primitivePrice = bcdiv($price->getUnitPrice(), 100, 2);
                                break;
                        }
                    }

                    return "{$price->getProductVariant()->getDisplayName()} | {$price->getSupplier()->getName()} | {$primitivePrice}€/{$salesUnit}";
                },
                'choice_value' => function($variant) {
                    if ($variant instanceof SupplierProductPrice)
                        return $variant->getProductVariant()->getId();

                    return $variant->getId();
                }
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function() {
                $this->cacheManager->deleteItem(RedisCacheManager::CACHE_KEY_FOOD_TYPES);
            });
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_food_type';
    }
}