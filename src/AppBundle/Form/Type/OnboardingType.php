<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Onboarding;
use AppBundle\Entity\Supplier;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OnboardingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('chosenSupplier', EntityType::class, [
                'class' => Supplier::class,
                'required' => false,
                'empty_data' => null
            ])
            ->add('supplierName', TextType::class, ['required' => false])
            ->add('restaurantName', TextType::class, ['required' => true])
            ->add('address', TextType::class, ['required' => false])
            ->add('zipCode', TextType::class, ['required' => false])
            ->add('city', TextType::class, ['required' => false])
            ->add('contactName', TextType::class, ['required' => true])
            ->add('contactEmail', EmailType::class, ['required' => true])
            ->add('contactPassword', PasswordType::class, ['required' => false])
            ->add('deliveryRange', TextType::class, ['required' => false])
            ->add('telephone', TextType::class, ['required' => true])
            ->add('kbis', FileType::class, ['required' => false])
            ->add('deliveryForm', FileType::class, ['required' => false])
            ->add('comment', TextareaType::class, ['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Onboarding::class, 'csrf_protection' => false]);
    }
}