<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class LiformVichFileType extends VichFileType
{
    public function buildLiform(FormInterface $form, array $schema)
    {
        $schema['vich'] = [];
        $downloadUri = $form->getConfig()->getOption('download_uri');
        $downloadLabel = $form->getConfig()->getOption('download_label');

        $object = $form->getParent()->getData();

        if ($downloadUri && $object) {
            $link = $this->resolveUriOption($downloadUri, $object, $form);
            $label = $this->resolveDownloadLabel($downloadLabel, $object, $form);

            if (!empty($link)) {
                $schema['vich'] = [
                    'download_uri' => $link,
                    'download_label' => $label['download_label']
                ];
            }
        }

        return $schema;
    }
}
