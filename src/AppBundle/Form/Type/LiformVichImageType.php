<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Vich\UploaderBundle\Form\DataTransformer\FileTransformer;
use Vich\UploaderBundle\Form\Type\VichFileType;

class LiformVichImageType extends VichFileType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('file', FileType::class, [
            'required' => $options['required'],
            'label' => $options['label'],
            'attr' => $options['attr'],
            'translation_domain' => $options['translation_domain'],
            'liform' => [
                'widget' => 'dropzone'
            ]
        ]);

        $builder->addModelTransformer(new FileTransformer());

        if ($options['allow_delete']) {
            $this->buildDeleteField($builder, $options);
        }
    }
    public function buildLiform(FormInterface $form, array $schema)
    {
        $schema['vich'] = [];
        $downloadUri = $form->getConfig()->getOption('download_uri');
        $downloadLabel = $form->getConfig()->getOption('download_label');

        $object = $form->getParent()->getData();

        if ($downloadUri && $object) {
            $link = $this->resolveUriOption($downloadUri, $object, $form);
            $label = $this->resolveDownloadLabel($downloadLabel, $object, $form);

            if (!empty($link)) {
                $schema['vich'] = [
                    'download_uri' => $link,
                    'download_label' => $label['download_label']
                ];
            }
        }

        return $schema;
    }
}
