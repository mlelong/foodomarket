<?php

namespace AppBundle\Form\Type\Filter;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductTaxonFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('taxon', EntityType::class, array(
            'class' => 'Sylius\Component\Core\Model\Taxon',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('taxon')
                    ->orderBy('taxon.code', 'ASC')
                    ;
            },
            'choice_label' => 'name',
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }
}