<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Mercuriale;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierCategory;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichFileType;

class MercurialeType extends AbstractResourceType
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(string $dataClass, $validationGroups = [], ObjectManager $objectManager)
    {
        parent::__construct($dataClass, $validationGroups);

        $this->objectManager = $objectManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dlcFrom', DateType::class, [
                'required' => true,
                'label' => 'Entrée en vigueur des tarifs',
                'widget' => 'single_text',
                'html5' => false,
                'constraints' => [
                    new NotBlank(['message' => 'Il faut spécifier une date de début de validité', 'groups' => $this->validationGroups])
                ]
            ])
            ->add('dlcTo', DateType::class, [
                'required' => false,
                'label' => 'Fin de validité des tarifs',
                'widget' => 'single_text',
                'html5' => false,
                'constraints' => [
                    new NotBlank(['message' => 'Il faut spécifier une date de fin de validité', 'groups' => $this->validationGroups])
                ]
            ])
            ->add('supplier', EntityType::class, [
                'required' => true,
                'label' => 'Fournisseur',
                'class' => Supplier::class,
                'multiple' => false,
                'query_builder' => function(EntityRepository $repository) {
                    return $repository
                        ->createQueryBuilder('s')
                        ->where('s.enabled = 0')
                        ->orderBy('s.name', 'ASC')
                    ;
                },
                'constraints' => [
                    new NotBlank(['message' => 'Il faut spécifier un fournisseur', 'groups' => $this->validationGroups])
                ]
            ])
            ->add('category', ChoiceType::class, [
                'required' => true,
                'label' => 'Catégorie',
                'choices' => [
                    'Fruits & Légumes' => Supplier::CAT_FRUITS_LEGUMES,
                    'Crèmerie' => Supplier::CAT_CREMERIE,
                    'Boucherie' => Supplier::CAT_BOUCHERIE,
                    'Marée' => Supplier::CAT_MAREE,
                    'Epicerie' => Supplier::CAT_EPICERIE,
                    'Cave' => Supplier::CAT_CAVE,
                    Supplier::getDisplayCategory(Supplier::CAT_CONSOMMABLES) => Supplier::CAT_CONSOMMABLES,
                    Supplier::getDisplayCategory(Supplier::CAT_TRAITEUR) => Supplier::CAT_TRAITEUR,
                    Supplier::getDisplayCategory(Supplier::CAT_FRUITS_LEGUMES_BIO) => Supplier::CAT_FRUITS_LEGUMES_BIO,
                ],
                'multiple' => false,
                'constraints' => [
                    new NotBlank(['message' => 'Il faut spécifier une catégorie', 'groups' => $this->validationGroups])
                ]
            ])
            ->add('restaurant', EntityType::class, [
                'required' => true,
                'label' => 'Restaurant',
                'class' => Restaurant::class,
                'multiple' => false,
                'constraints' => [
                    new NotBlank(['message' => 'Il faut spécifier un restaurant', 'groups' => $this->validationGroups])
                ]
            ])
            ->add('file', VichFileType::class, [
                'required' => true,
                'label' => 'Mercuriale',
                'allow_delete' => true,
                'constraints' => [
                    new NotBlank(['message' => 'Il faut joindre une mercuriale', 'groups' => $this->validationGroups])
                ]
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            /** @var Mercuriale|null $mercuriale */
            $mercuriale = $event->getData();
            $form = $event->getForm();

            if ($mercuriale !== null && $mercuriale->getSupplier() !== null) {
                $supplierCategories = $mercuriale->getSupplier()->getFacadeSupplier()->getCategories();
                $mercuriales = $this->objectManager->getRepository(Mercuriale::class)->findBySupplier($mercuriale->getSupplier());
                $categories = [];

                foreach ($supplierCategories as $category) {
                    $found = false;

                    foreach ($mercuriales as $_mercuriale) {
                        if ($_mercuriale->getCategory() === $category) {
                            $found = true;
                        }
                    }

                    if (!$found) {
                        $categories[Supplier::getDisplayCategory($category)] = $category;
                    }
                }

                if (!empty($mercuriale->getCategory()) && !isset($categories[Supplier::getDisplayCategory($mercuriale->getCategory())])) {
                    $categories[Supplier::getDisplayCategory($mercuriale->getCategory())] = $mercuriale->getCategory();
                }

                $form
                    ->add('supplier', EntityType::class, [
                        'required' => true,
                        'label' => 'Fournisseur',
                        'class' => Supplier::class,
                        'multiple' => false,
                        'disabled' => true
                    ])
                    ->add('category', ChoiceType::class, [
                        'required' => true,
                        'label' => 'Catégorie',
                        'choices' => $categories,
                        'multiple' => false,
                        'constraints' => [
                            new NotBlank(['message' => 'Il faut spécifier une catégorie', 'groups' => $this->validationGroups])
                        ]
                    ])
                ;
            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
            /** @var Mercuriale $mercuriale */
            $mercuriale = $event->getData();

            $supplierCategory = $this->objectManager->getRepository(SupplierCategory::class)->findOneBy([
                'supplier' => $mercuriale->getSupplier(),
                'category' => $mercuriale->getCategory()
            ]);

            if ($supplierCategory === null) {
                $supplierCategory = new SupplierCategory();

                $supplierCategory->setSupplier($mercuriale->getSupplier());
                $supplierCategory->setCategory($mercuriale->getCategory());

                $this->objectManager->persist($supplierCategory);
            }
        });
    }

    public function getBlockPrefix()
    {
        return 'app_mercuriale';
    }
}