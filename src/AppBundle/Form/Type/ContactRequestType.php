<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ContactRequest;
use AppBundle\Entity\FoodType;
use AppBundle\Entity\Supplier;
use AppBundle\Form\DataTransformer\ContactRequestCategoryTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactRequestType extends AbstractResourceType
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * ContactRequestType constructor.
     * @param string $dataClass
     * @param array $validationGroups
     * @param ObjectManager $objectManager
     */
    public function __construct(string $dataClass, array $validationGroups, ObjectManager $objectManager)
    {
        parent::__construct($dataClass, $validationGroups);

        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('restaurantName', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.restaurant_name',
            ])
            ->add('address', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.address',
            ])
            ->add('zipCode', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.zip_code',
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.city',
            ])
            ->add('deliveryHours', ChoiceType::class, [
                'required' => true,
                'data' => '_',
                'choices' => [
                    'Avant 6h' => '_6',
                    'Entre 6h et 8h' => '6_8',
                    'Entre 8h et 10h' => '8_10',
                    'Entre 10h et 12h' => '10_12',
                    'Entre 12h et 14h' => '12_14',
                    'Entre 14h et 16h' => '14_16',
                    'Entre 16h et 18h' => '16_18',
                    'Après 18h' => '18_',
                    'Autre' => '_',
                ],
                'label' => 'showcase.savings.form.full.fields.delivery_hours',
            ])
            ->add('deliveryHoursComment', TextareaType::class, [
                'required' => false,
                'attr' => ['placeholder' => 'showcase.savings.form.full.placeholders.delivery_hours_comment'],
                'label' => 'showcase.savings.form.full.fields.delivery_hours_comment',
            ])
            ->add('managerName', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.manager_name',
            ])
            ->add('managerFirstName', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.manager_first_name',
            ])
            ->add('companyName', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.company_name',
            ])
            ->add('siren', TextType::class, [
                'required' => false,
                'label' => 'showcase.savings.form.full.fields.siren',
            ])
            ->add('telephone', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.telephone',
            ])
            ->add('email', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.full.fields.email',
            ])
            ->add('turnover', IntegerType::class, [
                'required' => false,
                'label' => 'showcase.savings.form.full.fields.turnover',
            ])
            ->add('categories', ChoiceType::class, [
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'showcase.savings.form.full.fields.fruits_vegetables' => Supplier::CAT_FRUITS_LEGUMES,
                    'showcase.savings.form.full.fields.creamery' => Supplier::CAT_CREMERIE,
                    'showcase.savings.form.full.fields.butchery' => Supplier::CAT_BOUCHERIE,
                    'showcase.savings.form.full.fields.seafood' => Supplier::CAT_MAREE,
                    'showcase.savings.form.full.fields.grocery' => Supplier::CAT_EPICERIE,
                ],
                'label' => 'showcase.savings.form.full.fields.categories',
            ])
            ->add('categoriesFull', CollectionType::class, [
                'entry_type' => ContactRequestCategoryType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'label' => 'showcase.savings.form.full.fields.categories',
            ])

            ->add('profile', EntityType::class, ['class' => FoodType::class, 'label' => 'showcase.savings.form.full.fields.profile', 'choice_label' => 'type'])
        ;

        $builder->get('categoriesFull')->addModelTransformer(new ContactRequestCategoryTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_contact_request';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ContactRequest::class, 'validation_groups' => 'request_form_full']);
    }
}