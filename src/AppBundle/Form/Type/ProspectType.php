<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierCategory;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ProspectType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kbisFile', VichFileType::class, ['required' => false, 'label' => 'KBis', 'allow_delete' => true, 'download_label' => 'Télécharger le KBis'])
            ->add('ribFile', VichFileType::class, ['required' => false, 'label' => 'RIB', 'allow_delete' => true, 'download_label' => 'Télécharger le RIB'])
            ->add('iban', TextType::class, ['required' => false, 'label' => 'Iban'])
            ->add('enabled', CheckboxType::class, ['required' => false, 'label' => 'Activer le client'])
            ->add('hubspotId', TextType::class, ['required' => false, 'label' => 'ID Client Hubspot'])
            ->add('civility', TextType::class, ['required' => false, 'label' => 'Civilité'])
            ->add('firstName', TextType::class, ['required' => false, 'label' => 'Prénom du gérant'])
            ->add('contactName', TextType::class, ['required' => true, 'label' => 'Nom du gérant'])
            ->add('restaurantName', TextType::class, ['required' => true, 'label' => 'Nom du restaurant'])
            ->add('address', TextType::class, ['required' => true, 'label' => 'Adresse du restaurant'])
            ->add('zipCode', TextType::class, ['required' => true, 'label' => 'Code postal du restaurant'])
            ->add('city', TextType::class, ['required' => true, 'label' => 'Ville du restaurant'])
            ->add('companyName', TextType::class, ['required' => false, 'label' => 'Nom de la société'])
            ->add('siren', TextType::class, ['required' => false, 'label' => 'SIREN'])
            ->add('phone', TextType::class, ['required' => true, 'label' => 'Téléphone'])
            ->add('mobile', TextType::class, ['required' => false, 'label' => 'Portable'])
            ->add('email', TextType::class, ['required' => true, 'label' => 'Email principal'])
            ->add('supplementaryEmails', TextType::class, ['required' => false, 'label' => 'Emails supplémentaires (séparé par une virgule)'])
            ->add('currentSupplier', TextType::class, ['required' => false, 'label' => 'Grossiste actuel'])
            ->add('deliveryHours', TextType::class, ['required' => false, 'label' => 'Horaires de livraison'])
            ->add('newsletterPrices', CheckboxType::class, ['required' => false, 'label' => 'Newsletter Prix'])
            ->add('emailSubscribed', CheckboxType::class, ['required' => false, 'label' => 'Newsletter'])
            ->add('canUseArbitration', CheckboxType::class, ['required' => false, 'label' => 'Peut utiliser l\'arbitrage'])
            ->add('categories', ChoiceType::class, [
                'required' => false,
                'label' => 'Catégories',
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'Fruits & Légumes' => Supplier::CAT_FRUITS_LEGUMES,
                    'Crèmerie' => Supplier::CAT_CREMERIE,
                    'Boucherie' => Supplier::CAT_BOUCHERIE,
                    'Marée' => Supplier::CAT_MAREE,
                    'Epicerie' => Supplier::CAT_EPICERIE,
                ]
            ])
            ->add('suppliers', EntityType::class, [
                'required' => false,
                'label' => 'Comptes grossiste ouverts',
                'class' => SupplierCategory::class,
                'multiple' => true,
                'expanded' => false,
                'attr' => ['class' => 'ui search']
            ])
            ->add('customers', EntityType::class, [
                'class' => Customer::class,
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'expanded' => false,
                'label' => 'Utilisateurs du shop'
            ])
            ->add('type', ChoiceType::class, [
                'required' => true,
                'label' => 'Type de prospect',
                'multiple' => false,
                'expanded' => true,
                'choices' => [
                    'Professionnel' => Prospect::TYPE_PROFESSIONNAL,
                    'Particulier' => Prospect::TYPE_INDIVIDUAL
                ]
            ]);
    }

    public function getBlockPrefix()
    {
        return 'app_prospect';
    }
}