<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Prospect;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterStepTwoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('restaurantName', TextType::class,
                [
                    'label' => 'Nom du restaurant',
                    'constraints' => [new NotBlank()],
                ]
            )
            ->add('address', TextType::class,
                [
                    'label' => 'Adresse de livraison',
                    'constraints' => [new NotBlank()],
                    'attr' => ['placeholder' => '1 rue de Rivoli, 75007, Paris'],
                    'liform' => ['widget' => 'places-autocomplete']
                ]
            )
            ->add('zipCode', TextType::class,
                [
                    'label' => 'Code postal',
                    'constraints' => [new NotBlank()]
                ]
            )
            ->add('city', TextType::class,
                [
                    'label' => 'Ville',
                    'constraints' => [new NotBlank()]
                ]
            )
            ->add('deliveryHours', TextareaType::class,
                [
                    'label' => 'Créneau de livraison souhaité (indiquer 2h min)',
                    'liform' => ['widget' => 'timeslot'],
                    'required' => true,
                    'empty_data' => '08:00-10:00',
                    'data' => '08:00-10:00',
                ]
            )
            ->add('kbisFile', LiformVichFileType::class, [
                'label' => 'Copie de votre kbis',
                'constraints' => [
//                    new NotBlank(['groups' => ['step4']])
                    new NotBlank(['groups' => ['step3']])
                ]
            ]);
    }

    public function getBlockPrefix()
    {
        return 'form';
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'data_class' => Prospect::class,
            // Désactivation du CSRF pour ce formulaire
            'csrf_protection' => false
        ));
    }
}
