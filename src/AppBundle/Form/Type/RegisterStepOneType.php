<?php


namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use AppBundle\Entity\Prospect;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterStepOneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class,
                [
                    'label' => 'Votre prénom',
                    'constraints' => [new NotBlank()],

                ]
            )
            ->add('email', EmailType::class,
                [
                    'label' => 'Votre email',
                    'attr' => ['placeholder' => 'exemple@email.fr'],
                    'liform' => ['widget' => 'email'],
                    'constraints' => [new NotBlank()]
                ]
            )
            ->add('mobile', TelType::class,
                [
                    'label' => 'Votre numéro de téléphone',
                    'constraints' => [new NotBlank()],
                    'attr' => ['pattern' => '^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$']
                ]
            )
            // Chose à vérifier : Avoir un message clair quand les deux mdp ne sont pas identiques
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'label' => 'Crééz votre mot de passe',
                'liform' => ['widget' => 'password'],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('password_again', PasswordType::class, [
                'mapped' => false,
                'label' => 'Confirmez votre mot de passe',
                'liform' => ['widget' => 'repeatedpassword'],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) {
                /** @var Prospect $data */
                $data = $event->getData();

                $data->setPlainPassword($event->getForm()->get('password')->getData());
                $event->setData($data);
            })
        ;
    }

    public function getBlockPrefix()
    {
        return 'form';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Prospect::class,
            // Désactivation du CSRF pour ce formulaire
            'csrf_protection' => false
        ));
    }

}