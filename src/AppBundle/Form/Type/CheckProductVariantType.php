<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProductVariant;
use Sylius\Component\Product\Model\ProductOptionValue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CheckProductVariantType extends AbstractType
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ProductVariant $variant */
        $variant = $options['data'];
        /** @var ProductOptionValue[] $optionValues */
        $optionValues = $variant->getOptionValues();
        $options = [];

        foreach ($optionValues as $optionValue) {
            $options[] = $optionValue->getOption();
        }

        $builder
            ->add('optionValues', CustomProductOptionValueCollectionType::class, ['options' => $options, 'label' => false])
            ->add('weight', NumberType::class)
            ->add('submit', SubmitType::class, ['label' => 'Ok', 'attr' => ['class' => 'ui button primary', 'formnovalidate' => true]])
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
            /** @var ProductVariant $variant */
            $variant = $event->getData();

            foreach ($variant->getOptionValues() as $id => $optionValue) {
                if ($optionValue === null) {
                    $variant->getOptionValues()->remove($id);
                }
            }

            $event->setData($variant);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => ProductVariant::class]);
    }
}