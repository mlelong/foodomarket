<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Step 1
            ->add('restaurant', TextType::class, [
                'mapped' => false,
                'label' => 'Nom de votre restaurant',
                'attr' => ['placeholder' => 'Mon restaurant'],
                'constraints' => [
                    new NotBlank(['groups' => ['step1']]),
                ]
            ])
            ->add('email', EmailType::class, [
                'mapped' => false,
                'label' => 'Email',
                'attr' => ['placeholder' => 'exemple@email.fr'],
                'liform' => ['widget' => 'email'],
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            ->add('restaurantAddress', TextType::class, [
                'mapped' => false,
                'label' => 'Rue',
                'attr' => ['placeholder' => '1 rue de Rivoli, 75007, Paris'],
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            ->add('restaurantCity', TextType::class, [
                'mapped' => false,
                'label' => 'Ville',
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            ->add('restaurantZipCode', TextType::class, [
                'mapped' => false,
                'label' => 'Code postal',
                'attr' => ['pattern' => '^[0-9]{5,5}$'],
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            ->add('deliveryHours', TextareaType::class, [
                'mapped' => false,
                'label' => false,
                'liform' => ['widget' => 'timeslot'],
                'required' => false,
                'empty_data' => '08:00-10:00'
            ])

            // Step 2
            ->add('categories', ChoiceType::class, [
                'mapped' => false,
                'label' => "Catégories de produits",
                'choices' => [
                    Supplier::getDisplayCategory(Supplier::CAT_FRUITS_LEGUMES) => Supplier::CAT_FRUITS_LEGUMES,
                    Supplier::getDisplayCategory(Supplier::CAT_CREMERIE) => Supplier::CAT_CREMERIE,
                    Supplier::getDisplayCategory(Supplier::CAT_BOUCHERIE) => Supplier::CAT_BOUCHERIE,
                    Supplier::getDisplayCategory(Supplier::CAT_MAREE) => Supplier::CAT_MAREE,
                    Supplier::getDisplayCategory(Supplier::CAT_EPICERIE) => Supplier::CAT_EPICERIE,
                ],
                'multiple' => true,
                'expanded' => true,
                'liform' => ['widget' => 'category-multiple-choice'],
                'constraints' => [
                    new NotBlank(['groups' => ['step2']])
                ]
            ])
            ->add('currentSuppliers', ChoiceType::class, [
                'mapped' => false,
                'label' => 'Vos fournisseurs actuels',
                "multiple" => true,
                "expanded" => true,
                'choices' => [
                    "Halles Prestige" => "Halles Prestige",
                    "Alazard" => "Alazard",
                    "Bermudes" => "Bermudes",
                    "Chassineau" => "Chassineau",
                    "Daumesnil" => "Daumesnil",
                    "Distri-Resto" => "Distri-Resto",
                    "EpiSaveurs" => "EpiSaveurs",
                    "Demarne" => "Demarne",
                    "Domafrais" => "Domafrais",
                    "Halles Trottemant" => "Halles Trottemant",
                    "Jacob" => "Jacob",
                    "Halles Mandar" => "Halles Mandar",
                    "Metro" => "Metro",
                    "Terre Azur" => "Terre Azur",
                    "Transgourmet" => "Transgourmet",
                    "Verger Etoile" => "Verger Etoile",
                    "Reynaud" => "Reynaud",
                    "Primeurs Passion" => "Primeurs Passion",
                    "Eurodis" => "Eurodis",
                    "Activia" => "Activia",
                    "Fory Viandes" => "Fory Viandes",
                    "Martin" => "Martin",
                    "Huguenin" => "Huguenin",
                    "Prodal" => "Prodal",
                    "J'Oceane" => "J'Oceane",
                    "L'Ecrevisse" => "L'Ecrevisse",
                    "Buisson" => "Buisson",
                    "Delon" => "Delon",
                    "ABC Peyraud" => "ABC Peyraud",
                    "Ugalait" => "Ugalait",
                    "Placet" => "Placet",
                    "Daguerre" => "Daguerre",
                    "Autre",
                    "Autre crémier",
                    "Autre boucher",
                    "Autre épicier",
                    "Autre primeur",
                    "Autre poissonnier",
                ],
                'liform' => ['widget' => 'supplier-multiple-choice'],
                'constraints' => [
                    new NotBlank(['groups' => ['step2']])
                ]
            ])

            // Step 3
            ->add('kbisFile', LiformVichFileType::class, [
                'label' => 'Extrait de KBis',
                'constraints' => [
                    new NotBlank(['groups' => ['step3']])
                ]
            ])

            // Step 4
            ->add('firstname', TextType::class, [
                'mapped' => false,
                'label' => 'Prénom',
                'constraints' => [
                    new NotBlank(['groups' => ['step4']])
                ]
            ])
            ->add('lastname', TextType::class, [
                'mapped' => false,
                'label' => 'Nom',
                'constraints' => [
                    new NotBlank(['groups' => ['step4']])
                ]
            ])
            ->add('telephone', TextType::class, [
                'mapped' => false,
                'label' => 'Téléphone portable',
                'attr' => ['pattern' => '^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$'],
                'constraints' => [
                    new NotBlank(['groups' => ['step4']])
                ]
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'label' => 'Mot de passe',
                'liform' => ['widget' => 'password'],
                'constraints' => [
                    new NotBlank(['groups' => ['step4']])
                ]
            ])
            ->add('password_again', PasswordType::class, [
                'mapped' => false,
                'label' => 'Confirmation du mot de passe',
                'liform' => ['widget' => 'repeatedpassword'],
                'constraints' => [
                    new NotBlank(['groups' => ['step4']])
                ]
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $formEvent) {
            /** @var Prospect|null $prospect */
            $prospect = $formEvent->getData();
            $form = $formEvent->getForm();

            $form->add('civility', ChoiceType::class, [
                'mapped' => false,
                'multiple' => false,
                'expanded' => true,
                'label' => 'Civilité',
                'choices' => [
                    'Monsieur' => 'MONSIEUR',
                    'Madame' => 'MADAME',
                    'Mademoiselle' => 'MADEMOISELLE',
                ],
                'data' => $prospect !== null && $prospect->getCivility() !== null ? $prospect->getCivility() : 'MONSIEUR',
                'liform' => ['widget' => ''],
                'constraints' => [
                    new NotBlank(['groups' => ['step4']])
                ]
            ]);
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $formEvent) {
            /** @var Prospect|null $prospect */
            $prospect = $formEvent->getData();
            $form = $formEvent->getForm();

            if ($prospect !== null) {
                $form->get('restaurant')->setData($prospect->getRestaurantName());
                $form->get('restaurantAddress')->setData($prospect->getAddress());
                $form->get('restaurantZipCode')->setData($prospect->getZipCode());
                $form->get('restaurantCity')->setData($prospect->getCity());
                $form->get('email')->setData($prospect->getEmail());
                $form->get('telephone')->setData($prospect->getPhone());
                $form->get('categories')->setData($prospect->getCategories());
                $form->get('deliveryHours')->setData($prospect->getDeliveryHours());
                $form->get('currentSuppliers')->setData(explode(',', $prospect->getCurrentSupplier()));
            }
        });

        $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $formEvent) {
            /** @var Prospect|null $prospect */
            $prospect = $formEvent->getData();
            $form = $formEvent->getForm();

            if ($prospect->getRegistrationStep() == 1) {
                $prospect->setEmail($form->get('email')->getData());
                $prospect->setRestaurantName($form->get('restaurant')->getData());
                $prospect->setAddress($form->get('restaurantAddress')->getData());
                $prospect->setZipCode($form->get('restaurantZipCode')->getData());
                $prospect->setCity($form->get('restaurantCity')->getData());
                $prospect->setDeliveryHours($form->get('deliveryHours')->getData());
            } elseif ($prospect->getRegistrationStep() == 2) {
                $prospect->setCategories($form->get('categories')->getData());
                $prospect->setCurrentSupplier(implode(',', $form->get('currentSuppliers')->getData()));
            } elseif ($prospect->getRegistrationStep() == 4) {
                $prospect->setMobile($form->get('telephone')->getData());
                $prospect->setPhone($form->get('telephone')->getData());
                $prospect->setFirstName($form->get('firstname')->getData());
                $prospect->setContactName($form->get('lastname')->getData());
                $prospect->setCivility($form->get('civility')->getData());
                $prospect->setPlainPassword($form->get('password')->getData());
            }

            $formEvent->setData($prospect);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired(['validation_groups'])
            ->setDefaults([
                'data_class' => Prospect::class,
                'csrf_protection' => false
            ])
        ;
    }

    public function getBlockPrefix()
    {
        return 'form';
    }
}