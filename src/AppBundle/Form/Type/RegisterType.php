<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Prospect;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('restaurantName', TextType::class, [
                'label' => false,
                'required' => true,
                'attr' => ['placeholder' => 'Restaurant']
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'required' => true,
                'attr' => ['placeholder' => 'E-mail']
            ])
            ->add('newsletterPrices', CheckboxType::class, [
                'required' => false,
                'label' => 'showcase.register.form.newsletter_price'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Prospect::class,
            'validation_groups' => 'app_register',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_register';
    }
}