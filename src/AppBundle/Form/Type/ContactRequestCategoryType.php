<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\ProductChoiceLoaderCallback;
use AppBundle\Form\SupplierChoiceLoaderCallback;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\SupplierRepository;
use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactRequestCategoryType extends AbstractType
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    public function __construct(ProductRepository $productRepository, SupplierRepository $supplierRepository)
    {
        $this->productRepository = $productRepository;
        $this->supplierRepository = $supplierRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', HiddenType::class, [
                'required' => true,
            ])
            ->add('purchaseVolume', MoneyType::class, [
                'required' => true,
                'label' => "Volume d'achat moyen",
                'divisor' => 100,
                'attr' => [
                    'class' => 'purchase-volume'
                ],
//                'constraints' => [
//                    new NotBlank(['payload' => ['show' => false], 'groups' => ['request_form_full']])
//                ]
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $formEvent) {
            $data = $formEvent->getData();
            $form = $formEvent->getForm();

            $productChoiceLoaderCallback = null;
            $supplierChoiceLoaderCallback = null;

            if ($data != null) {
                /**
                 * Suppliers
                 */
                $supplierChoiceLoaderCallback = new SupplierChoiceLoaderCallback(function() use($data) {
                    $products = [];

                    foreach ($data['suppliers'] as $product) {
                        $products[$product['name']] = $product['id'];
                    }

                    return $products;
                });

                $supplierChoiceLoaderCallback->setSupplierRepository($this->supplierRepository);

                $form->add('suppliers', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Fournisseurs actuels',
                    'multiple' => true,
                    'expanded' => false,
                    'choice_loader' => $supplierChoiceLoaderCallback,
                    'attr' => [
                        'class' => 'ui autocomplete search suppliers-search'
                    ]
                ]);

                $form->add('otherSuppliers', TextType::class, [
                    'required' => false,
                    'label' => 'Autres fournisseurs',
                    'attr' => [
                        'placeholder' => 'Autres fournisseurs...'
                    ]
                ]);

                /**
                 * Products
                 */
                $productChoiceLoaderCallback = new ProductChoiceLoaderCallback(function() use($data) {
                    $products = [];

                    foreach ($data['products'] as $product) {
                        $products[$product['name']] = $product['id'];
                    }

                    return $products;
                });

                $productChoiceLoaderCallback->setProductRepository($this->productRepository);

                $form->add('products', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Produits',
                    'multiple' => true,
                    'expanded' => false,
                    'choice_loader' => $productChoiceLoaderCallback,
                    'attr' => [
                        'class' => 'ui autocomplete search products-search'
                    ]
                ]);
            } else {
                /**
                 * Suppliers
                 */
                $supplierChoiceLoaderCallback = new SupplierChoiceLoaderCallback(function() {return [];});
                $supplierChoiceLoaderCallback->setSupplierRepository($this->supplierRepository);

                $form->add('suppliers', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Fournisseurs actuels',
                    'multiple' => true,
                    'expanded' => false,
                    'choice_loader' => $supplierChoiceLoaderCallback,
                    'attr' => [
                        'class' => 'ui autocomplete search suppliers-search'
                    ]
                ]);

                $form->add('otherSuppliers', TextType::class, [
                    'required' => false,
                    'label' => 'Autres fournisseurs',
                    'attr' => [
                        'placeholder' => 'Autres fournisseurs...'
                    ]
                ]);

                /**
                 * Products
                 */
                $productChoiceLoaderCallback = new ProductChoiceLoaderCallback(function() {return [];});
                $productChoiceLoaderCallback->setProductRepository($this->productRepository);

                $form->add('products', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Produits',
                    'multiple' => true,
                    'expanded' => false,
                    'choice_loader' => $productChoiceLoaderCallback,
                    'attr' => [
                        'class' => 'ui autocomplete search products-search'
                    ]
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'validation_groups' => 'request_form_full'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_contact_request_category';
    }

}