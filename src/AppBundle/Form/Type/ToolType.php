<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ToolDescription;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class ToolType extends AbstractResourceType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ToolTranslationType::class,
            ])
            ->add('position', NumberType::class)
            ->add('descriptions', EntityType::class, [
                'class' => ToolDescription::class,
                'multiple' => true,
                'expanded' => false,
                'by_reference' => false,
                'choice_label' => 'title',
                'attr' => [
                    'class' => 'fluid search'
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_tool';
    }
}