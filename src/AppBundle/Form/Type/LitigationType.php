<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\Litigation;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LitigationType extends AbstractResourceType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('status', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Statut',
                    'choices' => array_flip(Litigation::STATUS_LABEL)
                ]
            )
            ->add('supplier', EntityType::class,
                [
                    'required' => true,
                    'label' => 'Grossiste',
                    'class' => Supplier::class,
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('supplier')
                            ->andWhere('supplier.enabled = 1')
                            ->andWhere('supplier.facadeSupplier is null')
                            ->orderBy('supplier.name', 'ASC')
                        ;
                    }
                ]
            )
            ->add('prospect', EntityType::class,
                [
                    'required' => true,
                    'label' => 'Prospect',
                    'class' => Prospect::class,
                    'choice_label' => 'restaurantName',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('prospect')
                            ->orderBy('prospect.restaurantName', 'ASC')
                        ;
                    }
                ]
            )
            ->add('restaurant', EntityType::class,
                [
                    'required' => true,
                    'label' => 'Restaurant',
                    'class' => Restaurant::class,
                    'choice_label' => 'name',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('restaurant')
                            ->orderBy('restaurant.name', 'ASC')
                        ;
                    }
                ]
            )
            ->add('problem', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Quel est le problème ?',
                    'choices' => array_flip(Litigation::PROBLEM_LABEL)
                ]
            )
            ->add('demand', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Que voulez vous ?',
                    'choices' => array_flip(Litigation::DEMAND_LABEL)
                ]
            )
            ->add('askToBeCalled', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Voulez vous être rappelé ?',
                    'choices' => ['Oui' => true,
                                   'Non' => false
                                ],
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add('comments', CollectionType::class,
                [
                    'entry_type' => LitigationCommentType::class,
                    'allow_add' => true,
                    'by_reference' => false,
                    'label' => 'Réponses'
                ]
            )

            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Litigation::class, 'validation_groups' => 'litigation']);
    }



}