<?php

declare(strict_types=1);

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Services\RedisCacheManager;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionValueChoiceType;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Webmozart\Assert\Assert;

final class CustomProductOptionValueCollectionType extends AbstractType
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $optionCode
     * @param ArrayCollection|ProductOptionValue[] $values
     * @return ProductOptionValueInterface[]
     */
    private function getSortedOptionValues(string $optionCode, $values)
    {
        $values = $this->container->get('app.redis.cache.manager')->get(
            str_replace('{code}', $optionCode, RedisCacheManager::CACHE_KEY_ADMIN_PRODUCT_OPTION_VALUES),
            null,
            function() use($values) {
                $values = $values->map(function(ProductOptionValue $value) {
                    return [
                        'id' => $value->getId(),
                        'code' => $value->getCode(),
                        'value' => $value->getValue()
                    ];
                })->toArray();

                usort($values, function(array $a, array $b) {
                    $aName = $a['value'];
                    $bName = $b['value'];

                    $tmpA = str_replace(',', '.', $aName);
                    $tmpB = str_replace(',', '.', $bName);

                    if (is_numeric($tmpA) && is_numeric($tmpB)) {
                        if ((float)$tmpA === (float)$tmpB) {
                            return 0;
                        }

                        return ((float)$tmpA < (float)$tmpB) ? -1 : 1;
                    }

                    return strcasecmp($aName, $bName);
                });

                return $values;
            }
        );

        $dummies = [];

        foreach ($values as $value) {
            $dummies[] = new class($value) extends ProductOptionValue {
                /** @var array */
                private $value;

                public function __construct(array $value) {
                    parent::__construct();

                    $this->value = $value;
                }

                public function getId()
                {
                    return $this->value['id'];
                }

                public function getCode(): ?string {
                    return $this->value['code'];
                }

                public function getValue(): ?string {
                    return $this->value['value'];
                }
            };
        }

        return $dummies;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException
     * @throws InvalidConfigurationException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->assertOptionsAreValid($options);

        /** @var ProductOptionInterface[] $allOptions */
        $allOptions = $this->container->get('sylius.repository.product_option')->findAll();

        foreach ($options['options'] as $i => $option) {
            if (!$option instanceof ProductOptionInterface) {
                throw new InvalidConfigurationException(
                    sprintf('Each object passed as option list must implement "%s"', ProductOptionInterface::class)
                );
            }
        }

        $lastIndex = count($options['options']);

        $requiredOptions = $options['requiredOptions'];

        foreach ($allOptions as $option) {
            /**
             * @var int $i
             * @var ProductOptionInterface $existingOption
             */
            foreach ($options['options'] as $i => $existingOption) {
                if ($existingOption->getCode() == $option->getCode()) {
                    $builder->add((string) $existingOption->getCode(), ProductOptionValueChoiceType::class, [
                        'label' => $option->getName() ?: $option->getCode(),
                        'option' => $option,
                        'property_path' => '[' . $i . ']',
                        'block_name' => 'entry',
                        'required' => isset($requiredOptions[$existingOption->getCode()]),
                        'empty_data' => null,
                        'choices' => $this->getSortedOptionValues($option->getCode(), $option->getValues())
                    ]);

                    continue 2;
                }
            }

            $builder->add((string) $option->getCode(), ProductOptionValueChoiceType::class, [
                'label' => $option->getName() ?: $option->getCode(),
                'option' => $option,
                'property_path' => '[' . $lastIndex . ']',
                'block_name' => 'entry',
                'required' => isset($requiredOptions[$option->getCode()]),
                'empty_data' => null,
                'choices' => $this->getSortedOptionValues($option->getCode(), $option->getValues())
            ]);

            ++$lastIndex;
        }

        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $formEvent) {
            /** @var ProductOptionValue[]|ArrayCollection|null $data */
            $data = $formEvent->getData();

            if ($data !== null) {
                $form = $formEvent->getForm();

                foreach ($data as $optionValue) {
                    $form->get($optionValue->getOptionCode())->setData($optionValue);
                }
            }
        });

        $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $formEvent) {
            $data = $formEvent->getData();

            foreach ($data as $key => $optionValue) {
                if ($optionValue === null) {
                    $data->remove($key);
                }
            }

            $newData = new ArrayCollection();

            foreach ($data as $datum) {
                $newData->add($this->container->get('sylius.repository.product_option_value')->find($datum->getId()));
            }

            $formEvent->setData($newData);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefault('options', null)
            ->setDefault('requiredOptions', ['UC' => true, 'UM' => true])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'sylius_product_option_value_collection';
    }

    /**
     * @param mixed $options
     *
     * @throws \InvalidArgumentException
     */
    private function assertOptionsAreValid($options)
    {
        Assert::false((
            !isset($options['options']) ||
            !is_array($options['options']) &&
            !($options['options'] instanceof \Traversable && $options['options'] instanceof \ArrayAccess)),
            'array or (\Traversable and \ArrayAccess) of "Sylius\Component\Variation\Model\OptionInterface" must be passed to collection'
        );
    }
}
