<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\LitigationComment;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LitigationCommentType extends AbstractResourceType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('content', TextareaType::class,
                [
                    'required' => true,
                    'label' => 'Description',
                    'attr' => [
                        'placeholder' => 'Ecrivez ici votre commentaire'
                    ]
                ]
            )
            ->add('pictures', CollectionType::class,
                [
                    'required' => false,
                    'entry_type' => LitigationCommentPictureType::class,
                    'allow_add' => true,
                    'by_reference' => false,
                    'label' => 'Photos',
                    'liform' => [
                        'widget' => 'photo-array'
                    ]
                ]
            )
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => LitigationComment::class, 'validation_groups' => 'litigation_comment']);
    }

    public function getBlockPrefix()
    {
        return 'form';
    }
}