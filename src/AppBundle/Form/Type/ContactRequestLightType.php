<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ContactRequest;
use AppBundle\Entity\FoodType;
use AppBundle\Form\DataTransformer\EntityHiddenTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactRequestLightType extends AbstractResourceType
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * ContactRequestType constructor.
     * @param string $dataClass
     * @param array $validationGroups
     * @param ObjectManager $objectManager
     */
    public function __construct(string $dataClass, array $validationGroups, ObjectManager $objectManager)
    {
        parent::__construct($dataClass, $validationGroups);
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('managerName', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.light.fields.manager_name',
            ])
            ->add('managerFirstName', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.light.fields.manager_first_name',
            ])
            ->add('telephone', TextType::class, [
                'required' => true,
                'label' => 'showcase.savings.form.light.fields.telephone',
            ])
            ->add('profile', HiddenType::class, ['required' => false])
        ;

        $builder->get('profile')->addModelTransformer(new EntityHiddenTransformer($this->objectManager, FoodType::class, 'id'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_contact_request_light';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ContactRequest::class, 'validation_groups' => 'request_form_light']);
    }
}