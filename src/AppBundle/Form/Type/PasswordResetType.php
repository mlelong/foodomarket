<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PasswordResetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'label' => 'Nouveau mot de passe',
                'liform' => ['widget' => 'password'],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('password_again', PasswordType::class, [
                'mapped' => false,
                'label' => 'Confirmation du mot de passe',
                'liform' => ['widget' => 'repeatedpassword'],
                'constraints' => [
                    new NotBlank()
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['csrf_protection' => false]);
    }

    public function getBlockPrefix()
    {
        return 'form';
    }
}