<?php

namespace AppBundle\Form;

use AppBundle\Entity\ChannelPricing;
use AppBundle\Entity\Restaurant;
use AppBundle\Form\DataTransformer\ChannelToCodeTransformer;
use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Sylius\Component\Core\Model\Channel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChannelPricingType extends AbstractType
{
    protected $channelToCodeTransformer;

    /**
     * ChannelPricingType constructor.
     *
     * @param ChannelToCodeTransformer $channelToCodeTransformer
     */
    public function __construct(ChannelToCodeTransformer $channelToCodeTransformer)
    {
        $this->channelToCodeTransformer = $channelToCodeTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', MoneyType::class, [
                'label' => 'sylius.ui.price',
            ])
            ->add('originalPrice', MoneyType::class, [
                'label' => 'sylius.ui.original_price',
            ])
            ->add('channelCode', EntityType::class, [
                'class' => Channel::class,
                'required' => true
            ])
            ->add('restaurant', EntityType::class, [
                'class' => Restaurant::class,
                'empty_data'   => null,
                'required' => false
            ])
        ;

        $builder->get('channelCode')
            ->addModelTransformer($this->channelToCodeTransformer)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ChannelPricing::class
        ));

    }
}
