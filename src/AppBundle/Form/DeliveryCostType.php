<?php

/**
 * Created by PhpStorm.
 * User: chekib
 * Date: 26/05/17
 * Time: 18:46
 */

namespace AppBundle\Form;

use AppBundle\Entity\Supplier;
use AppBundle\Entity\DeliveryCost;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DeliveryCostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supplier', EntityType::class, [
                'label' => 'sylius.form.import.supplier',
                'class' => Supplier::class,
            ])
            ->add('deliveryFees', NumberType::class, [
                'attr' => ['min' => 0],
                'label' => 'sylius.ui.delivery_fees',
            ])
            ->add('freeDeliveryThreshold', NumberType::class, [
                'attr' => ['min' => 0],
                'label' => 'sylius.ui.free_delivery_threshold',
            ])
            ->getForm();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DeliveryCost::class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'delivery_cost_type';
    }
}
