<?php

namespace AppBundle\Form;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Restaurant;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class RestaurantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $prospect = $options['prospect'];

        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Actif',
            ])
            ->add('street', TextType::class, [
                'label' => 'Rue',
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
            ])
            ->add('postcode', TextType::class, [
                'label' => 'Code postal',
            ])
            ->add('countryCode', ChoiceType::class, [
                'label' => 'Code Pays',
                'choices' => ['FR' => 'FR'],
                'empty_data' => 'FR',
            ])
            ->add('customers', EntityType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(['message' => "Il faut spécifier un utilisateur du shop dans le restaurant !", 'groups' => 'back-office']),
                    new NotNull(['message' => "Il faut spécifier un utilisateur du shop dans le restaurant !", 'groups' => 'back-office']),
                    new Count(['min' => 1, 'minMessage' => "Il faut spécifier un utilisateur du shop dans le restaurant !", 'groups' => 'back-office'])
                ],
                'label' => 'Utilisateurs du shop',
                'class' => Customer::class,
                'multiple' => true,
                'expanded' => false,
                'attr' => ['class' => 'ui search'],
                'by_reference' => false,
                'query_builder' => function(EntityRepository $em) use($prospect) {
                    $qb = $em->createQueryBuilder('c');

                    if ($prospect !== null) {
                        $qb
                            ->where('c.prospect = :prospect')
                            ->setParameter('prospect', $prospect);
                    }

                    return $qb->orderBy('c.email', 'ASC');
                }
            ])
            ->getForm();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Restaurant::class,
            'prospect' => null,
            'validation_groups' => 'back-office'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vitalis_restaurant_type';
    }
}
