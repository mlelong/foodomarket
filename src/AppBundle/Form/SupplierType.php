<?php

/**
 * Created by PhpStorm.
 * User: chekib
 * Date: 26/05/17
 * Time: 18:46
 */

namespace AppBundle\Form;

use AppBundle\Entity\AdminUser;
use AppBundle\Entity\Supplier;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SupplierType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('logo', VichImageType::class, ['required' => false, 'label' => 'Logo', 'allow_delete' => true])
            ->add('name', TextType::class, [
                'label' => 'Nom',
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Actif',
            ])
            ->add('email', TextType::class, [
                'label' => 'Email',
            ])
            ->add('commercialEmails', CollectionType::class, [
                'label' => 'Email(s) pour ouverture de compte',
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('orderEmails', CollectionType::class, [
                'label' => 'Email(s) pour passage de commande',
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->add('phone', TextType::class, [
                'label' => 'Téléphone'
            ])
            ->add('accountOpening', CheckboxType::class, [
                'label' => 'Ouverture de compte nécessaire pour passer commande',
            ])
            ->add('applicationNeeded', CheckboxType::class, [
                'label' => 'Validation nécessaire avant ouverture',
            ])
            ->add('paymentOnDelivery', CheckboxType::class, [
                'label' => 'Paiement possible à la livraison',
            ])
            ->add('paymentOnline', CheckboxType::class, [
                'label' => 'Paiement possible en ligne',
            ])
            ->add('paymentOffline', CheckboxType::class, [
                'label' => 'Paiement possible par virement/cheque',
            ])
            ->add('sepa', VichFileType::class, ['required' => false, 'label' => 'Autorisation SEPA', 'allow_delete' => true])
            ->add('howToOrder', TextareaType::class, [
                'label' => 'Comment contacter le grossiste ? (envoyé par email)',
                'required' => true
            ])
            ->add('orderModality', TextareaType::class, ['label' => 'Modalité de commande (envoyé par email)', 'required' => false])
            ->add('orderAdditionalElements', TextareaType::class, ['label' => 'Eléments complémentaires pour commander (envoyé par email)', 'required' => false])
            ->add('orderLimitTime', TextType::class, ['label' => 'Heure limite de commande', 'required' => false])
            ->add('minimumOrder', NumberType::class, ['label' => 'Minimum de commande'])
            ->add('freePort', NumberType::class, ['label' => 'Franco de port'])
            ->add('shippingCost', NumberType::class, ['label' => 'Frais de livraison'])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false
            ])
            ->add('deliveryTerms', TextareaType::class, [
                'label' => 'Modalités de livraison',
                'required' => false
            ])
            ->add('deliveryOnSunday', CheckboxType::class, [
                'label' => 'Livraison le dimanche',
            ])
            ->add('deliveryOnMonday', CheckboxType::class, [
                'label' => 'Livraison le lundi',
            ])
            ->add('deliveryOnTuesday', CheckboxType::class, [
                'label' => 'Livraison le mardi',
            ])
            ->add('deliveryOnWednesday', CheckboxType::class, [
                'label' => 'Livraison le mercredi',
            ])
            ->add('deliveryOnThursday', CheckboxType::class, [
                'label' => 'Livraison le jeudi',
            ])
            ->add('deliveryOnFriday', CheckboxType::class, [
                'label' => 'Livraison le vendredi',
            ])
            ->add('deliveryOnSaturday', CheckboxType::class, [
                'label' => 'Livraison le samedi',
            ])
            ->add('street', TextType::class, [
                'label' => 'Rue',
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
            ])
            ->add('postcode', TextType::class, [
                'label' => 'Code postal',
            ])
            ->add('countryCode', TextType::class, [
                'label' => 'sylius.form.countryCode',
            ])
            ->add('mercurialPattern', TextType::class, [
                'label' => 'Patten fetcher mercurial',
            ])
            ->add('invoicePattern', TextType::class, [
                'label' => 'Pattern fetcher invoice',
            ])
            ->add('channel', EntityType::class, [
                'class' => 'Sylius\Component\Core\Model\Channel',
            ])
            ->add('categories', CollectionType::class, [
                'entry_type'   => TextType::class,
                'entry_options'  => [],
                'label' => 'Catégories des produits vendus',
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('employees', CollectionType::class, [
                'entry_type' => EntityType::class,
                'entry_options' => [
                    'class' => AdminUser::class,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
            ->add('facadeSupplier', EntityType::class, [
                'class' => Supplier::class,
                'multiple' => false,
                'expanded' => false,
                'label' => 'Grossiste de facade',
                'required' => false
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Supplier::class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vitalis_supplier_type';
    }
}
