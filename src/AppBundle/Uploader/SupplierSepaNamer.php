<?php

namespace AppBundle\Uploader;

use AppBundle\Entity\Supplier;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class SupplierSepaNamer implements NamerInterface
{
    /**
     * Creates a name for the file being uploaded.
     *
     * @param Supplier $object The object the upload is attached to
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object
     *
     * @return string The file name
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $supplierName = $object->getName();
        $extension = $mapping->getFile($object)->guessExtension();

        return "SEPA {$supplierName}.{$extension}";
    }
}