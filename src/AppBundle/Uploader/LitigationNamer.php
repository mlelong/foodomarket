<?php


namespace AppBundle\Uploader;


use AppBundle\Entity\LitigationCommentPicture;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class LitigationNamer implements NamerInterface
{
    /**
     * Creates a name for the file being uploaded.
     *
     * @param LitigationCommentPicture $object The object the upload is attached to
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object
     *
     * @return string The file name
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $restaurantName = $object->getComment()->getLitigation()->getProspect()->getRestaurantName();
        $extension = $mapping->getFile($object)->guessExtension();

        return "Litige_{$restaurantName}_".date('Ymd_His')."_".rand(10,99).".{$extension}";
    }
}