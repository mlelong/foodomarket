<?php

namespace AppBundle\Uploader;

use AppBundle\Entity\Mercuriale;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class MercurialeNamer implements NamerInterface
{
    /**
     * Creates a name for the file being uploaded.
     *
     * @param Mercuriale $object The object the upload is attached to
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object
     *
     * @return string The file name
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $supplierName = $object->getSupplier()->getName();
        $dlcFrom = $object->getDlcFrom()->format('Y-m-d');
        $dlcTo = $object->getDlcTo() !== null ? $object->getDlcTo()->format('Y-m-d') : '';
        $extension = $mapping->getFile($object)->guessExtension();

        return "{$supplierName}_$dlcFrom" . (!empty($dlcTo) ? "_$dlcTo" : '') . ".$extension";
    }
}