<?php


namespace AppBundle\Uploader;


use AppBundle\Entity\Prospect;
use Cocur\Slugify\Slugify;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class ProspectRibNamer implements NamerInterface
{
    /**
     * Creates a name for the file being uploaded.
     *
     * @param Prospect $object The object the upload is attached to
     * @param PropertyMapping $mapping The mapping to use to manipulate the given object
     *
     * @return string The file name
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $restaurantName = $object->getRestaurantName();
        $extension = $mapping->getFile($object)->guessExtension();

        $restaurantName = (new Slugify())->slugify($restaurantName);

        return "RIB {$restaurantName}.{$extension}";
    }
}