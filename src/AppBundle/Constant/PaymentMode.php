<?php

namespace AppBundle\Constant;

use Exception;

abstract class PaymentMode
{
    const CASH          = 'CASH';
    const CHEQUE        = 'CHEQUE';
    const CREDIT_CARD   = 'CREDIT_CARD';
    const BANK_TRANSFER = 'BANK_TRANSFER';
    const DIRECT_DEBIT  = 'DIRECT_DEBIT';

    /**
     * @param string $mode
     * @return string
     * @throws Exception
     */
    public static function toString(string $mode)
    {
        switch ($mode) {
            case self::CASH:
                return 'Espèces';
            case self::CHEQUE:
                return 'Chèque';
            case self::CREDIT_CARD:
                return 'Carte bancaire';
            case self::BANK_TRANSFER:
                return 'Virement bancaire';
            case self::DIRECT_DEBIT:
                return 'Prélèvement automatique';
            default:
                throw new Exception("Payment mode `$mode` is not supported.");
        }
    }
}
