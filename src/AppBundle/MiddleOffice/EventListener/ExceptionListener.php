<?php

namespace AppBundle\MiddleOffice\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

class ExceptionListener
{
    /** @var RouterInterface */
    private $router;

    /** @var ParameterBag */
    private $parameterBag;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(RouterInterface $router, ParameterBag $parameterBag, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->parameterBag = $parameterBag;
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $pathInfo = $event->getRequest()->getPathInfo();

        // Skip when admin
        if (stripos($pathInfo, '/admin/') !== false) {
            return ;
        }

        $exception = $event->getThrowable();

        $isImage = $this->isImage($pathInfo);

        if (!$isImage) {
            $this->logger->error("ExceptionListener: $pathInfo => {$exception->getTraceAsString()}\n");
        }

        if ($event->getRequest()->isXmlHttpRequest()) {
            $event->setResponse(new JsonResponse([
                'ok' => false,
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'uri' => $event->getRequest()->getUri()
            ]));
        } else {
            if ($exception instanceof NotFoundHttpException) {
                if (!$isImage) {
                    $event->setResponse(new RedirectResponse($this->router->generate('app_front_404_page')));
                } else {
                    $event->setResponse(new Response('', 404));
                }
            } else {
                $response = new RedirectResponse($this->router->generate('app_front_500_page', ['exception' => [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                    'uri' => $event->getRequest()->getUri()
                ]], RouterInterface::ABSOLUTE_URL));

                $event->setResponse($response);
            }
        }
    }

    private function isImage(string $pathInfo)
    {
        return preg_match('/\.(a?png|jpe?g|gif|webp)$/i', $pathInfo) > 0;
    }
}
