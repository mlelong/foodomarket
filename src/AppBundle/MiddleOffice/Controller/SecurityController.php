<?php


namespace AppBundle\MiddleOffice\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends AbstractController
{
    public function login(Request $request)
    {
    }

    public function logout()
    {
        // Redirect to homepage
        $response = $this->redirectToRoute('app_front_index');

        // Remove the authentication token and refresh token from cookies
        $response->headers->clearCookie('_token');
        $response->headers->clearCookie('_refresh');

        // Remove the foodo switch session param (if any)
        $this->get('session')->remove('_foodo_switch');

        return $response;
    }
}
