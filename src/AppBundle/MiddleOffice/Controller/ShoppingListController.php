<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Entity\Supplier;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\RedisCacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ShoppingListController extends AbstractController
{
    use ShopContext;

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function addProductAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $restaurantStockRepository = $this->get('app.repository.restaurant_stock');
        $restaurantStockItemRepository = $manager->getRepository(RestaurantStockItem::class);
        $productVariantRepository = $this->get('sylius.repository.product_variant');
        $supplierRepository = $this->get('app.repository.supplier');
        /** @var Restaurant $restaurant */
        $restaurant = $this->getRestaurant();

        /** @var int $id Id of the shopping list */
        $id = $request->get('id');
        /** @var int $productVariantId The product variant id */
        $productVariantId = $request->get('pid');
        /** @var float $qty The product quantity */
        $qty = $request->get('qty');
        /** @var string $qtyUnit The quantity unit */
        $qtyUnit = $request->get('unit');
        /** @var int $supplierId The supplier id for this variant */
        $supplierId = $request->get('supplierId');

        /** @var RestaurantStock $restaurantStock */
        $restaurantStock = $restaurantStockRepository->find($id);
        /** @var ProductVariant $productVariant */
        $productVariant = $productVariantRepository->find($productVariantId);
        /** @var Supplier $supplier */
        $supplier = $supplierRepository->find($supplierId);

        if ($restaurantStock === null || $restaurantStock->getRestaurant()->getId() != $restaurant->getId()) {
            return new JsonResponse(['ok' => false, 'message' => "Unable to find shopping list with id `$id`"]);
        }

        if ($productVariant === null) {
            return new JsonResponse(['ok' => false, 'message' => "Unable to find product with id `$productVariantId`"]);
        }

        if ($supplier === null) {
            return new JsonResponse(['ok' => false, 'message' => "Unable to find supplier with id `$supplierId`"]);
        }

        $restaurantStockItem = $restaurantStockItemRepository->findOneBy([
            'restaurantStock' => $restaurantStock, 'productVariant' => $productVariant, 'supplier' => $supplier
        ]);

        if ($restaurantStockItem !== null) {
            return new JsonResponse(['ok' => false, 'message' => "Item already added"]);
        }

        $restaurantStockItem = new RestaurantStockItem();

        $restaurantStockItem->setStock($qty);
        $restaurantStockItem->setProductVariant($productVariant);
        $restaurantStockItem->setDeleted(0);
        $restaurantStockItem->setRestaurantStock($restaurantStock);
        $restaurantStockItem->setStockUnit($qtyUnit);
        $restaurantStockItem->setSupplier($supplier);

        $manager->persist($restaurantStockItem);
        $manager->flush();

        // Invalidate shopping list cache
        $this->get('app.redis.cache.manager')->deleteItem(
            str_replace(
                '{restaurantId}',
                $restaurant->getId(),
                RedisCacheManager::CACHE_KEY_SHOP_USER_SHOPPING_LISTS)
        );

        return new JsonResponse(['ok' => true, 'sid' => $restaurantStockItem->getId()]);
    }

    /**
     * Removes a product from the shopping list
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteProductAction(Request $request)
    {
        $id = $request->get('id');
        $stockId = $request->get('sid');

        $restaurantStockItemRepository = $this->getDoctrine()->getRepository(RestaurantStockItem::class);

        /** @var RestaurantStockItem $restaurantStockItem */
        $restaurantStockItem = $restaurantStockItemRepository->find($stockId);

        if ($restaurantStockItem !== null && $restaurantStockItem->getRestaurantStock()->getId() == $id) {
            $restaurant = $this->getRestaurant();
            $manager = $this->getDoctrine()->getManager();

            $manager->remove($restaurantStockItem);
            $manager->flush();

            // Invalidate shopping list cache
            $this->get('app.redis.cache.manager')->deleteItem(
                str_replace(
                    '{restaurantId}',
                    $restaurant->getId(),
                    RedisCacheManager::CACHE_KEY_SHOP_USER_SHOPPING_LISTS)
            );

            return new JsonResponse(['ok' => true]);
        }

        return new JsonResponse(['ok' => false, 'message' => "Unknown shopping list id `$id` or shopping list item `$stockId`"]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateQuantityAction(Request $request)
    {
        $id = $request->get('id');
        $stockId = $request->get('sid');
        $restaurantStockItemRepository = $this->getDoctrine()->getRepository(RestaurantStockItem::class);

        /** @var RestaurantStockItem $restaurantStockItem */
        $restaurantStockItem = $restaurantStockItemRepository->find($stockId);

        if ($restaurantStockItem !== null && $restaurantStockItem->getRestaurantStock()->getId() == $id) {
            $restaurantStockItem->setStock($request->get('qty'));
            $restaurantStockItem->setStockUnit(strtoupper($request->get('unit')));

            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(['ok' => true]);
        }

        return new JsonResponse(['ok' => false, 'message' => "Unknown shopping list id `$id` or shopping list item `$stockId`"]);
    }

    public function addAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();

        $listName = $request->get('name');
        $products = $request->get('products');

        /** @var Restaurant $restaurant */
        $restaurant = $this->getRestaurant();
        /** @var SupplierRepository $supplierRepository */
        $supplierRepository = $this->get('app.repository.supplier');
        /** @var ProductVariantRepository $productVariantRepository */
        $productVariantRepository = $this->get('sylius.repository.product_variant');

        $restaurantStock = new RestaurantStock();

        $restaurantStock->setDeleted(0);
        $restaurantStock->setName($listName);
        $restaurantStock->setRestaurant($this->get('app.repository.restaurant')->find($restaurant->getId()));

        $manager->persist($restaurantStock);
        $manager->flush();

        foreach ($products as $product) {
            /** @var Supplier $supplier */
            $supplier = $supplierRepository->find($product['supplierId']);
            $variant = $productVariantRepository->find($product['productId']);

            $restaurantStockItem = new RestaurantStockItem();

            $restaurantStockItem->setDeleted(0);
            $restaurantStockItem->setStock($product['qty']);
            $restaurantStockItem->setStockUnit(strtoupper($product['unit']));
            $restaurantStockItem->setSupplier($supplier);
            $restaurantStockItem->setProductVariant($variant);
            $restaurantStockItem->setRestaurantStock($restaurantStock);

            $manager->persist($restaurantStockItem);
        }

        $manager->flush();

        // Invalidate shopping list cache
        $this->get('app.redis.cache.manager')->deleteItem(
            str_replace(
                '{restaurantId}',
                $restaurant->getId(),
                RedisCacheManager::CACHE_KEY_SHOP_USER_SHOPPING_LISTS)
        );

        return new JsonResponse(['ok' => true]);
    }

    public function updateAction(Request $request)
    {
        $id = $request->get('id');
        $manager = $this->getDoctrine()->getManager();
        /** @var RestaurantStock $restaurantStock */
        $restaurantStock = $manager->getRepository(RestaurantStock::class)->find($id);

        if ($restaurantStock === null) {
            return new JsonResponse(['ok' => false, 'message' => "Unable to find shopping list with id `$id`"]);
        }

        $restaurantStock->setName($request->get('name'));
        $manager->flush();

        $restaurant = $this->getRestaurant();

        // Invalidate shopping list cache
        $this->get('app.redis.cache.manager')->deleteItem(
            str_replace(
                '{restaurantId}',
                $restaurant->getId(),
                RedisCacheManager::CACHE_KEY_SHOP_USER_SHOPPING_LISTS)
        );

        return new JsonResponse(['ok' => true]);
    }

    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $manager = $this->getDoctrine()->getManager();
        $restaurantStock = $manager->getRepository(RestaurantStock::class)->find($id);

        $manager->remove($restaurantStock);
        $manager->flush();

        $restaurant = $this->getRestaurant();

        // Invalidate shopping list cache
        $this->get('app.redis.cache.manager')->deleteItem(
            str_replace(
                '{restaurantId}',
                $restaurant->getId(),
                RedisCacheManager::CACHE_KEY_SHOP_USER_SHOPPING_LISTS)
        );

        return new JsonResponse(['ok' => true]);
    }

    public function setProductNoteAction(Request $request)
    {
        $id = $request->get('itemId');
        $manager = $this->getDoctrine()->getManager();
        $shoppingListItemRepository = $manager->getRepository(RestaurantStockItem::class);

        $shoppingListItem = $shoppingListItemRepository->find($id);

        if ($shoppingListItem === null) {
            return new JsonResponse(['ok' => false, 'message' => "Unable to find shopping list item with id `$id`"]);
        }

        $shoppingListItem->setNote($request->get('note'));

        $manager->flush();
        return new JsonResponse(['ok' => true]);
    }
}