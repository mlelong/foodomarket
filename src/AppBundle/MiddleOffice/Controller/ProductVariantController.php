<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierCategory;
use AppBundle\Entity\SupplierProductPriceHistory;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use Sylius\Component\Core\Model\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductVariantController extends AbstractController
{
    use ShopContext;

    public function indexAction(Request $request)
    {
        return $this->responseAction(['variant' => $this->getProductVariantData($request)]);
    }

    public function productVariantAction(Request $request)
    {
        return new JsonResponse(['ok' => true, 'data' => $this->getProductVariantData($request)]);
    }

    private function getProductVariantData(Request $request)
    {
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($this->getUser()->getCustomer()->getProspect()->getId());
        /** @var ProductVariant $variant */
        $variant = $this->get('sylius.repository.product_variant')->find($request->get('id'));
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($request->get('supplierId'));

        if ($variant === null) {
            return new JsonResponse(['ok' => false]);
        }

        /** @var Product $product */
        $product = $variant->getProduct();

        $srvProduct = $this->get('app.service.product_finder')->getProduct($variant, $supplier, $this->getRestaurant());

        $taxon = $product->getMainTaxon();

        $taxons = [
            [
                'id' => $taxon->getId(),
                'name' => $taxon->getName(),
                'slug' => $taxon->getSlug()
            ]
        ];

        while ($taxon = $taxon->getParent()) {
            $taxons[] = [
                'id' => $taxon->getId(),
                'name' => $taxon->getName(),
                'slug' => $taxon->getSlug()
            ];
        }

        $orderable = array_reduce($prospect->getSuppliers()->toArray(), function($carry, SupplierCategory $supplierCategory) use($supplier) {
            if ($carry) {
                return $carry;
            }

            if ($supplier->getEnabled()) {
                return $supplierCategory->getSupplier()->getFacadeSupplier()->getId() === $supplier->getId();
            }

            return $supplierCategory->getSupplier()->getId() === $supplier->getId();
        }, false);

        $data = [
            'taxons' => array_reverse($taxons),
            'productName' => $variant->getFullDisplayName(),
            'supplierName' => !$supplier->getEnabled()  && $supplier->getFacadeSupplier() !== null ? $supplier->getFacadeSupplier()->getName() : $supplier->getName(),
            'picture' => $this->get('liip_imagine.cache.manager')->getBrowserPath(
                !$product->getImages()->isEmpty() ? $product->getImages()->first()->getPath() : 'shop_default_product.png',
                'front_big'
            ),
            'options' => $variant->getOptionValues()->map(function(ProductOptionValue $optionValue) {
                return [
                    'name' => $optionValue->getOption()->getName(),
                    'value' => $optionValue->getValue()
                ];
            })->getValues(),
            'priceEvolution' => $this->getPriceEvolution($variant, $supplier),
            'srv' => $srvProduct,
            'orderable' => $orderable
        ];

        return $data;
    }

    private function getPriceEvolution(ProductVariant $variant, Supplier $supplier)
    {
        $supplierProductPriceHistoryRepository = $this->get('app.repository.supplier_product_price_history');
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($this->getUser()->getCustomer()->getProspect()->getId());

        if ($supplier->getEnabled() && !$supplier->getPartnerSuppliers()->isEmpty()) {
            $partnerSupplier = $supplier->getPartnerSuppliers()->first();

            foreach ($prospect->getSuppliers() as $supplierCategory) {
                if ($supplierCategory->getSupplier()->getFacadeSupplier()->getId() == $supplier->getId()) {
                    $partnerSupplier = $supplierCategory->getSupplier();

                    break;
                }
            }

            /** @var SupplierProductPriceHistory[] $prices */
            $prices = $supplierProductPriceHistoryRepository->findBy(
                ['productVariant' => $variant, 'supplier' => $partnerSupplier, 'restaurant' => 4],
                ['createdAt' => 'ASC', 'id' => 'ASC']
            );
        } else {
            /** @var SupplierProductPriceHistory[] $prices */
            $prices = $supplierProductPriceHistoryRepository->findBy(
                ['productVariant' => $variant, 'supplier' => $supplier, 'restaurant' => 4],
                ['createdAt' => 'ASC', 'id' => 'ASC']
            );
        }

        $evolution = [];

        foreach ($prices as $price) {
            $date = $price->getCreatedAt()->format('d/m/Y');

            $evolution[$date] = [
                'date' => $date,
                'kg' => floatval(bcdiv($price->getKgPrice(), 100, 2)),
                'pc' => floatval(bcdiv($price->getUnitPrice(), 100, 2)),
            ];
        }

        return array_values($evolution);
    }
}