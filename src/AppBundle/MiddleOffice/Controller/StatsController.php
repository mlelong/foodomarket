<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierCategory;
use AppBundle\Entity\SupplierProductPriceHistory;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Services\ProductListService;
use DateTime;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\OrderPaymentStates;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StatsController extends AbstractController
{
    use ShopContext;

    public function dashboardStatsInvoicesAction(Request $request)
    {
        $dateStart = new DateTime($request->get('start'));
        $dateEnd = new DateTime($request->get('end'));

        $prospect = $this->getUser()->getCustomer()->getProspect();
        $invoiceRepository = $this->get('app.repository.invoice');
        /** @var Invoice[] $invoices */
        $invoices = $invoiceRepository->createQueryBuilder('invoice')
            ->where('invoice.prospect = :prospect')
            ->andWhere('invoice.date BETWEEN :start AND :end')
            ->orderBy('invoice.date', 'ASC')
            ->setParameter('prospect',  $prospect->getId())
            ->setParameter('start', $dateStart)
            ->setParameter('end', $dateEnd)
            ->getQuery()
            ->getResult();

        $supplierSpendStats = [];
        $purchaseStats = [];
        $purchaseBySupplierStats = [];
        $sumBySupplierStats = [];
        $sumByCategoryStats = [
            Supplier::CAT_FRUITS_LEGUMES => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_FRUITS_LEGUMES),
                'amount' => 0
            ],
            Supplier::CAT_CREMERIE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_CREMERIE),
                'amount' => 0
            ],
            Supplier::CAT_BOUCHERIE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_BOUCHERIE),
                'amount' => 0
            ],
            Supplier::CAT_MAREE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_MAREE),
                'amount' => 0
            ],
            Supplier::CAT_EPICERIE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_EPICERIE),
                'amount' => 0
            ],
            'Autres' => [
                'name' => 'Autres',
                'amount' => 0
            ]
        ];

        $orderedSuppliers = [];

        foreach ($invoices as $invoice) {
            // Supplier spend stats
            $supplierId = $invoice->getSupplier()->getId();
            $supplierName = $invoice->getSupplier()->getName();

            $orderedSuppliers = array_unique(array_merge($orderedSuppliers, [$supplierName]));

            if (!isset($supplierSpendStats[$supplierId])) {
                $supplierSpendStats[$supplierId] = [
                    'name' => $supplierName,
                    'amount' => 0
                ];
            }

            $supplierSpendStats[$supplierId]['amount'] += $invoice->getTotal();

            // Sum by supplier
            if (!isset($sumBySupplierStats[$supplierName])) {
                $sumBySupplierStats['date'] = 'Du ' . $dateStart->format('d/m/Y') . ' au ' . $dateEnd->format('d/m/Y');
                $sumBySupplierStats[$supplierName] = 0;
            }

            $sumBySupplierStats[$supplierName] += $invoice->getTotal();

            // Purchase by category stats
            foreach ($invoice->getInvoiceDeliveryNotes() as $deliveryNote) {
                // Purchase Stats
                $date = $deliveryNote->getDate()->format('d/m/Y');

                if (!isset($purchaseStats[$date])) {
                    $purchaseStats[$date] = [
                        'date' => $date,
                        'Montant' => 0
                    ];
                }

                $purchaseStats[$date]['Montant'] += $deliveryNote->getTotal();

                // Purchase by supplier stats
                if (!isset($purchaseBySupplierStats[$date])) {
                    $purchaseBySupplierStats[$date] = [
                        'date' => $date
                    ];
                }

                if (!isset($purchaseBySupplierStats[$date][$supplierName])) {
                    $purchaseBySupplierStats[$date][$supplierName] = 0;
                }

                $purchaseBySupplierStats[$date][$supplierName] += $deliveryNote->getTotal();

                /** @var InvoiceDeliveryNoteItem $item */
                foreach ($deliveryNote->getItems() as $item) {
                    if ($item->getVariant() !== null) {
                        /** @var ProductInterface $product */
                        $product = $item->getVariant()->getProduct();

                        if (ProductListService::isFruitsVegetables($product)) {
                            $sumByCategoryStats[Supplier::CAT_FRUITS_LEGUMES]['amount'] += $item->getTotalPrice();
                        } elseif (ProductListService::isCreamery($product)) {
                            $sumByCategoryStats[Supplier::CAT_CREMERIE]['amount'] += $item->getTotalPrice();
                        } elseif (ProductListService::isSeafood($product)) {
                            $sumByCategoryStats[Supplier::CAT_MAREE]['amount'] += $item->getTotalPrice();
                        } elseif (ProductListService::isButchery($product)) {
                            $sumByCategoryStats[Supplier::CAT_BOUCHERIE]['amount'] += $item->getTotalPrice();
                        } else if (ProductListService::isGrocery($product)) {
                            $sumByCategoryStats[Supplier::CAT_EPICERIE]['amount'] += $item->getTotalPrice();
                        } else {
                            $sumByCategoryStats['Autres']['amount'] += $item->getTotalPrice();
                        }
                    } else {
                        $sumByCategoryStats['Autres']['amount'] += $item->getTotalPrice();
                    }
                }
            }
        }

        foreach ($purchaseBySupplierStats as &$purchaseBySupplierStat) {
            foreach ($orderedSuppliers as $supplierName) {
                if (!isset($purchaseBySupplierStat[$supplierName])) {
                    $purchaseBySupplierStat[$supplierName] = 0;
                }
            }
        }

        return new JsonResponse([
            'supplierSpend' => array_values($supplierSpendStats),
            'purchases' => array_values($purchaseStats),
            'purchasesBySupplier' => array_values($purchaseBySupplierStats),
            'totalSupplier' => [$sumBySupplierStats],
            'totalCategory' => array_values($sumByCategoryStats),
            'topProductPurchases' => $this->getInvoiceTopProducts($invoices),
        ]);
    }

    /**
     * @param Invoice[] $invoices
     * @return array
     */
    private function getInvoiceTopProducts(array $invoices)
    {
        $variants = [];

        foreach ($invoices as $invoice) {
            foreach ($invoice->getInvoiceDeliveryNotes() as $deliveryNote) {
                /** @var InvoiceDeliveryNoteItem $item */
                foreach ($deliveryNote->getItems() as $item) {
                    /** @var ProductVariant $variant */
                    $variant = $item->getVariant();

                    if ($variant !== null) {
                        if (!isset($variants[$variant->getId()])) {
                            $variants[$variant->getId()] = [
                                'id' => $variant->getId(),
                                'name' => $variant->getFullDisplayName(),
                                'quantity' => 0,
                                'amount' => 0,
                                'supplierId' => $invoice->getSupplier()->getId()
                            ];
                        }

                        $variants[$variant->getId()]['quantity'] += $item->getQuantity();
                        $variants[$variant->getId()]['amount'] += $item->getTotalPrice();
                    }
                }
            }
        }

        usort($variants, function($a, $b) {
            return $a['amount'] < $b['amount'];
        });

        return array_slice($variants, 0, 5);
    }

    public function dashboardStatsOrdersAction(Request $request)
    {
        $dateStart = new DateTime(str_replace('/', '-', $request->get('start')));
        $dateEnd = new DateTime(str_replace('/', '-', $request->get('end')));

        $orderQueryBuilder = $this->get('sylius.repository.order')->createQueryBuilder('o');
        /** @var Order[] $orders */
        $orders = $orderQueryBuilder
            ->join('o.shoppingCart', 'sc')
            ->where('sc.restaurant = :restaurant')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->setParameter('restaurant', $this->getRestaurant()->getId())
            ->setParameter('start', $dateStart)
            ->setParameter('end', $dateEnd)
            ->orderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        $supplierSpendStats = [];
        $purchaseStats = [];
        $purchaseBySupplierStats = [];
        $sumBySupplierStats = [];
        $sumByCategoryStats = [
            Supplier::CAT_FRUITS_LEGUMES => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_FRUITS_LEGUMES),
                'amount' => 0
            ],
            Supplier::CAT_CREMERIE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_CREMERIE),
                'amount' => 0
            ],
            Supplier::CAT_BOUCHERIE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_BOUCHERIE),
                'amount' => 0
            ],
            Supplier::CAT_MAREE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_MAREE),
                'amount' => 0
            ],
            Supplier::CAT_EPICERIE => [
                'name' => Supplier::getDisplayCategory(Supplier::CAT_EPICERIE),
                'amount' => 0
            ],
            'Autres' => [
                'name' => 'Autres',
                'amount' => 0
            ]
        ];

        $orderedSuppliers = [];

        foreach ($orders as $order) {
            $orderTotal = $order->getPaymentState() === OrderPaymentStates::STATE_REFUNDED ? -$order->getTotal() : $order->getTotal();

            // Supplier spend stats
            $supplierId = $order->getShoppingCart()->getSupplier()->getId();
            $supplierName = $order->getShoppingCart()->getSupplier()->getName();

            if (!isset($supplierSpendStats[$supplierId])) {
                $supplierSpendStats[$supplierId] = [
                    'name' => $supplierName,
                    'amount' => 0
                ];
            }

            $supplierSpendStats[$supplierId]['amount'] += $orderTotal;

            // Purchase Stats
            $date = $order->getCreatedAt()->format('d/m/Y');

            if (!isset($purchaseStats[$date])) {
                $purchaseStats[$date] = [
                    'date' => $date,
                    'Montant' => 0
                ];
            }

            $purchaseStats[$date]['Montant'] += $orderTotal;

            // Purchase by supplier stats
            if (!isset($purchaseBySupplierStats[$date])) {
                $purchaseBySupplierStats[$date] = [
                    'date' => $date
                ];
            }

            $orderedSuppliers = array_unique(array_merge($orderedSuppliers, [$supplierName]));

            if (!isset($purchaseBySupplierStats[$date][$supplierName])) {
                $purchaseBySupplierStats[$date][$supplierName] = 0;
            }

            $purchaseBySupplierStats[$date][$supplierName] += $orderTotal;

            // Sum by supplier
            if (!isset($sumBySupplierStats[$supplierName])) {
                $sumBySupplierStats['date'] = 'Du ' . $dateStart->format('d/m/Y') . ' au ' . $dateEnd->format('d/m/Y');
                $sumBySupplierStats[$supplierName] = 0;
            }

            $sumBySupplierStats[$supplierName] += floatval(bcdiv($orderTotal, 100, 2));

            // Purchase by category stats
            /** @var OrderItem $item */
            foreach ($order->getItems() as $item) {
                $itemTotal = $order->getPaymentState() === OrderPaymentStates::STATE_REFUNDED ? -$item->getTotal() : $item->getTotal();

                if (ProductListService::isFruitsVegetables($item->getProduct())) {
                    $sumByCategoryStats[Supplier::CAT_FRUITS_LEGUMES]['amount'] += floatval(bcdiv($itemTotal, 100, 2));
                }

                elseif (ProductListService::isCreamery($item->getProduct())) {
                    $sumByCategoryStats[Supplier::CAT_CREMERIE]['amount'] += floatval(bcdiv($itemTotal, 100, 2));
                }

                elseif (ProductListService::isSeafood($item->getProduct())) {
                    $sumByCategoryStats[Supplier::CAT_MAREE]['amount'] += floatval(bcdiv($itemTotal, 100, 2));
                }

                elseif (ProductListService::isButchery($item->getProduct())) {
                    $sumByCategoryStats[Supplier::CAT_BOUCHERIE]['amount'] += floatval(bcdiv($itemTotal, 100, 2));
                }

                else if (ProductListService::isGrocery($item->getProduct())) {
                    $sumByCategoryStats[Supplier::CAT_EPICERIE]['amount'] += floatval(bcdiv($itemTotal, 100, 2));
                }

                else {
                    $sumByCategoryStats['Autres']['amount'] += floatval(bcdiv($itemTotal, 100, 2));
                }
            }
        }

        foreach ($supplierSpendStats as &$orderStat) {
            $orderStat['amount'] = floatval(bcdiv($orderStat['amount'], 100, 2));
        }

        foreach ($purchaseStats as &$purchaseStat) {
            $purchaseStat['Montant'] = floatval(bcdiv($purchaseStat['Montant'], 100, 2));
        }

        foreach ($purchaseBySupplierStats as &$purchaseBySupplierStat) {
            foreach ($orderedSuppliers as $supplierName) {
                if (!isset($purchaseBySupplierStat[$supplierName])) {
                    $purchaseBySupplierStat[$supplierName] = 0;
                } else {
                    $purchaseBySupplierStat[$supplierName] = floatval(bcdiv($purchaseBySupplierStat[$supplierName], 100, 2));
                }
            }
        }

        return new JsonResponse([
            'supplierSpend' => array_values($supplierSpendStats),
            'purchases' => array_values($purchaseStats),
            'purchasesBySupplier' => array_values($purchaseBySupplierStats),
            'totalSupplier' => [$sumBySupplierStats],
            'totalCategory' => array_values($sumByCategoryStats),
            'topProductPurchases' => $this->getTopProducts($orders),
        ]);
    }

    /**
     * @param Order[] $orders
     * @return array
     */
    private function getTopProducts(array $orders)
    {
        $variants = [];

        foreach ($orders as $order) {
            if ($order->getState() === OrderPaymentStates::STATE_REFUNDED) {
                continue ;
            }

            /** @var OrderItem $item */
            foreach ($order->getItems() as $item) {
                /** @var ProductVariant $variant */
                $variant = $item->getVariant();

                if ($variant !== null) {
                    if (!isset($variants[$variant->getId()])) {
                        $variants[$variant->getId()] = [
                            'id' => $variant->getId(),
                            'name' => $variant->getFullDisplayName(),
                            'quantity' => 0,
                            'amount' => 0,
                            'supplierId' => $order->getShoppingCart()->getSupplier()->getId()
                        ];
                    }

                    $variants[$variant->getId()]['quantity'] += $item->getQuantityFloat();
                    $variants[$variant->getId()]['amount'] += floatval(bcdiv($item->getTotal(), 100, 2));
                }
            }
        }

        usort($variants, function($a, $b) {
            return $a['amount'] < $b['amount'];
        });

        return array_slice($variants, 0, 5);
    }

    public function variantStatsAction(Request $request)
    {
        $id = $request->get('id');
        /** @var ProductVariant $variant */
        $variant = $this->get('sylius.repository.product_variant')->find($id);
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($this->getUser()->getCustomer()->getProspect()->getId());
        $supplierCategories = $prospect->getSuppliers();
        $productVariantHistory = $this->get('app.product_variant_history');
        $supplierProductPriceHistoryRepository = $this->get('app.repository.supplier_product_price_history');
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');

        $history = $productVariantHistory->getVariantHistory($variant, null);

        $evolution = [];
        $suppliers = [];

        foreach ($history as $variantId => $supplier) {
//            $priceQb = $supplierProductPriceRepository->createQueryBuilder('spp');
//            $latestPrices = $priceQb
//                ->where('spp.productVariant = :variant')
//                ->andWhere($priceQb->expr()->in('spp.supplier', $supplierCategories->map(
//                    function(SupplierCategory $supplierCategory) {
//                        return $supplierCategory->getSupplier()->getId();
//                    })->getValues()
//                ))
//                ->setParameter('variant', $variantId)
//                ->getQuery()
//                ->getResult()
//            ;

            $priceHistoryQb = $supplierProductPriceHistoryRepository->createQueryBuilder('spph');
            /** @var SupplierProductPriceHistory[] $historyPrices */
            $historyPrices = $priceHistoryQb
                ->where('spph.productVariant = :variant')
                ->andWhere($priceHistoryQb->expr()->in('spph.supplier', $supplierCategories->map(
                    function(SupplierCategory $supplierCategory) {
                        return $supplierCategory->getSupplier()->getId();
                    })->getValues()
                ))
                ->setParameter('variant', $variantId)
                ->orderBy('spph.createdAt', 'ASC')
                ->getQuery()
                ->getResult()
            ;

            $latestPrices = [];
            $historyPrices = array_merge($historyPrices, $latestPrices);

            foreach ($historyPrices as $historyPrice) {
                $date = $historyPrice->getCreatedAt()->format('d/m/Y');
                $supplier = $historyPrice->getSupplier();
                $supplierName = $supplier->getFacadeSupplier()->getName();

                $suppliers[$supplierName] = true;

                if (!isset($evolution[$date])) {
                    $evolution[$date] = [
                        'name' => $date,
                        'data' => []
                    ];
                }

                $evolution[$date]['data'][] = [
                    'date' => $date,
                    'supplier' => $supplierName,
                    'kg' => floatval(bcdiv($historyPrice->getKgPrice(), 100, 2)),
                    'pc' => floatval(bcdiv($historyPrice->getUnitPrice(), 100, 2)),
                ];
            }
        }

        $variantData = ['name' => $variant->getFullDisplayName()];

        usort($evolution, function($left, $right) {
            $leftDate = implode('-', array_reverse(explode('/', $left['name'])));
            $rightDate = implode('-', array_reverse(explode('/', $right['name'])));

            return new DateTime($leftDate) > new DateTime($rightDate);
        });

//        foreach ($evolution as &$evol) {
//            foreach ($suppliers as $supplierName => $ignored) {
//                $found = false;
//
//                foreach ($evol['data'] as $data) {
//                    if ($data['supplier'] == $supplierName) {
//                        $found = true;
//                        break ;
//                    }
//                }
//
//                if (!$found) {
//                    $evol['data'][] = [
//                        'date' => $evol['name'],
//                        'supplier' => $supplierName,
//                        'kg' => 0,
//                        'pc' => 0
//                    ];
//                }
//            }
//        }

        $evolution2 = [];
        $last = null;

        foreach ($evolution as $evol) {
            $evolution2[$evol['name']] = [
                'date' => $evol['name']
            ];

            foreach ($evol['data'] as $data) {
                $evolution2[$evol['name']][$data['supplier']] = $data['kg'];
            }

            foreach ($suppliers as $supplierName => $ignored) {
                if (!isset($evolution2[$evol['name']][$supplierName])) {
                    $evolution2[$evol['name']][$supplierName] = $last !== null ? $last[$supplierName] : 0;
                }
            }

            $last = $evolution2[$evol['name']];
        }

        return new JsonResponse(['variant' => $variantData, 'data' => array_values($evolution2)]);
    }

    public function variantStats2Action(Request $request)
    {
        $variantId = $request->get('id');
        $variant = $this->get('sylius.repository.product_variant')->find($variantId);
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($this->getUser()->getCustomer()->getProspect()->getId());
        $supplierCategories = $prospect->getSuppliers();
        $supplierProductPriceHistoryRepository = $this->get('app.repository.supplier_product_price_history');
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');

        $priceQb = $supplierProductPriceRepository->createQueryBuilder('spp');
        $latestPrices = $priceQb
            ->where('spp.productVariant = :variant')
            ->andWhere($priceQb->expr()->in('spp.supplier', $supplierCategories->map(
                function(SupplierCategory $supplierCategory) {
                    return $supplierCategory->getSupplier()->getId();
                })->getValues()
            ))
            ->setParameter('variant', $variant)
            ->getQuery()
            ->getResult()
        ;

        $priceHistoryQb = $supplierProductPriceHistoryRepository->createQueryBuilder('spph');
        /** @var SupplierProductPriceHistory[] $historyPrices */
        $historyPrices = $priceHistoryQb
            ->where('spph.productVariant = :variant')
            ->andWhere($priceHistoryQb->expr()->in('spph.supplier', $supplierCategories->map(
                function(SupplierCategory $supplierCategory) {
                    return $supplierCategory->getSupplier()->getId();
                })->getValues()
            ))
            ->setParameter('variant', $variant)
            ->orderBy('spph.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        $evolution = [];
        $suppliers = [];

        $historyPrices = array_merge($historyPrices, $latestPrices);

        foreach ($historyPrices as $historyPrice) {
            $date = $historyPrice->getCreatedAt()->format('d/m/Y');
            $supplier = $historyPrice->getSupplier();
            $supplierName = $supplier->getFacadeSupplier()->getName();

            $suppliers[$supplierName] = true;

            if (!isset($evolution[$date])) {
                $evolution[$date] = [
                    'date' => $date
                ];
            }

            $evolution[$date][$supplierName] = floatval(bcdiv($historyPrice->getKgPrice(), 100, 2));
        }

        $last = null;

        foreach ($evolution as $date => &$data) {
            foreach ($suppliers as $supplierName => $ignored) {
                if (!isset($data[$supplierName])) {
                    $data[$supplierName] = $last !== null ? $last[$supplierName] : 0;
                }
            }

            $last = $data;
        }

        $variantData = ['name' => $variant->getFullDisplayName()];

        return new JsonResponse(['variant' => $variantData, 'data' => array_values($evolution)]);
    }

    public function testAction(Request $request)
    {
        $id = $request->get('id');
        $productVariantHistory = $this->get('app.product_variant_history');

        /** @noinspection PhpParamsInspection */
        $history = $productVariantHistory->getVariantHistory(
            $this->get('sylius.repository.product_variant')->find($id),
            $this->get('app.repository.supplier')->find(22)
        );

        return new JsonResponse($history);
    }
}