<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Constant\PaymentMode;
use AppBundle\Entity\Email;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Services\SellAndSign\SellAndSignException;
use AppBundle\Services\SellAndSign\SellAndSignService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class SepaController extends AbstractController
{
    use ShopContext;

    public function indexAction()
    {
        return $this->responseAction([
            'supplier-account-logs' => $this->getDisplaySupplierAccountLogs(),
            'waiting-sepas' => $this->getWaitingSepas(),
            'supplier-logs' => $this->getSupplierLogs()
        ]);
    }

    public function getSupplierAccountLogsAction()
    {
        return $this->json([
            'supplier-account-logs' => $this->getDisplaySupplierAccountLogs(),
            'waiting-sepas' => $this->getWaitingSepas(),
            'supplier-logs' => $this->getSupplierLogs()
        ]);
    }

    public function checkRequirementsAction(int $supplierId)
    {
        $prospect = $this->getManagedProspect();
        /** @var SupplierAccountLogRepository $supplierAccountLogRepository */
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($supplierId);
        $logs = $supplierAccountLogRepository->getSupplierAccountLogs($prospect, $supplier);

        return $this->json([
            'requirements' => !empty($prospect->getIban()) && !empty($prospect->getRib()) && !empty($prospect->getCompanyName())
                && !empty($prospect->getMobile())
                && !empty($prospect->getAddress()) && !empty($prospect->getZipCode()) && !empty($prospect->getCity())
                && !empty($prospect->getContactName()) && !empty($prospect->getFirstName()),
            'isSigned' => !empty($logs) && $logs[0]->isSigned()
        ]);
    }

    /**
     * Passe en CB un compte qui le désire
     *
     * @param int $supplierId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function switchToCreditCardAction(int $supplierId)
    {
        $prospect = $this->getManagedProspect();
        /** @var SupplierAccountLogRepository $supplierAccountLogRepository */
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($supplierId);
        $facade = $supplier->getFacadeSupplier();

        if ($facade->isPaymentOnline() && isset($facade->getPaymentModes()[PaymentMode::CREDIT_CARD])) {
            $log = $supplierAccountLogRepository->getSupplierAccountLogs($prospect, $supplier)[0];
            $log->setStatus(SupplierAccountLog::STATUS_ORDERING);

            $supplierAccountLogRepository->updateAllAccordingTo($log);
        }

        return $this->redirectToRoute('app_front_sepa_page');
    }

    /**
     * Passe en SEPA un compte qui a déjà signé le mandat
     *
     * @param int $supplierId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function switchToDirectDebitAction(int $supplierId)
    {
        $prospect = $this->getManagedProspect();
        /** @var SupplierAccountLogRepository $supplierAccountLogRepository */
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($supplierId);
        $facade = $supplier->getFacadeSupplier();

        if (isset($facade->getPaymentModes()[PaymentMode::DIRECT_DEBIT]) && $facade->getSepaFileName() !== null) {
            $log = $supplierAccountLogRepository->getSupplierAccountLogs($prospect, $supplier)[0];

            if ($log->isSigned()) {
                $log->setStatus(SupplierAccountLog::STATUS_ACCOUNT_OPENED);
                $supplierAccountLogRepository->updateAllAccordingTo($log);
            }
        }

        return $this->redirectToRoute('app_front_sepa_page');
    }

    /**
     * Returns the list of suppliers that are still in waiting stage and that REQUIRE a sepa ONLY
     *
     * @return array
     */
    private function getWaitingSuppliersWithSepa()
    {
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        $prospect = $this->getManagedProspect();
        $waitingSuppliers = $supplierAccountLogRepository->getWaitingAccounts($prospect);
        $waitingSuppliersWithSepa = [];

        foreach ($waitingSuppliers as $waitingSupplier) {
            $supplierAccountLog = $supplierAccountLogRepository->getRelevantSupplierAccountLog($prospect, $waitingSupplier);

            if ($supplierAccountLog->isSigned()) {
                continue ;
            }

            $paymentModes = $waitingSupplier->getFacadeSupplier()->getPaymentModes();

            // If there is only the payment by direct debit option
            if (count($paymentModes) === 1 && isset($paymentModes[PaymentMode::DIRECT_DEBIT])) {
                $waitingSuppliersWithSepa[] = $waitingSupplier;
            }
        }

        return $waitingSuppliersWithSepa;
    }

    private function getSupplierLogs()
    {
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        $prospect = $this->getManagedProspect();
        $suppliers = $supplierAccountLogRepository->getSuppliers($prospect);
        $supplierLogs = [];

        foreach ($suppliers as $supplier) {
            $supplierAccountLog = $supplierAccountLogRepository->getRelevantSupplierAccountLog($prospect, $supplier);
            $supplierLogs[$supplierAccountLog->getStatus()][] = $this->getDisplaySupplierAccountLog($supplierAccountLog);
        }

        return $supplierLogs;
    }

    private function getDisplaySupplierAccountLog(SupplierAccountLog $supplierAccountLog)
    {
        $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();

        switch ($supplierAccountLog->getStatus()) {
            case SupplierAccountLog::STATUS_WAITING:
                $class = 'warning';
                break ;
            case SupplierAccountLog::STATUS_ACCOUNT_BLOCKED:
                $class = 'negative';
                break ;
            case SupplierAccountLog::STATUS_ACCOUNT_OPENED:
            case SupplierAccountLog::STATUS_ORDERING:
                $class = 'positive';
                break ;
            default:
                $class = '';
                break ;
        }

        $status = ['closed' => false, 'class' => '', 'sellsignStatus' => ''];

        if ($supplier->getFacadeSupplier()->isPaymentByDirectDebit()) {
            $status = $this->getSellAndSignStatus($supplierAccountLog);
        }

        return [
            'id' => $supplier->getId(),
            'name' => $supplier->getDisplayName(),
            'class' => $class,
            'sellsignClass' => $status['class'],
            'status' => SupplierAccountLog::getDisplayStatus($supplierAccountLog->getStatus()),
            'createdAt' => $supplierAccountLog->getCreatedAt()->format('d/m/Y H:i:s'),
            'updatedAt' => $supplierAccountLog->getUpdatedAt()->format('d/m/Y H:i:s'),
            'isSepa' => $supplier->getFacadeSupplier()->isPaymentByDirectDebit(),
            'isCB' => $supplier->getFacadeSupplier()->isPaymentOnline() && isset($supplier->getFacadeSupplier()->getPaymentModes()[PaymentMode::CREDIT_CARD]),
            'contractId' => $supplierAccountLog->getContractId(),
            'signed' => $supplierAccountLog->isSigned(),
            'closed' => $status['closed'],
            'sellsignStatus' => $status['sellsignStatus'],
            'paymentModes' => array_values($supplier->getFacadeSupplier()->getPaymentModes())
        ];
    }

    /**
     * Returns the display structures (supplier account log + sell and sign data)
     * for the suppliers that are still in waiting stage and that REQUIRE a sepa ONLY
     *
     * @return array
     */
    private function getWaitingSepas()
    {
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        $prospect = $this->getManagedProspect();
        $waitingSuppliersWithSepa = $this->getWaitingSuppliersWithSepa();
        $waitingSepas = [];

        foreach ($waitingSuppliersWithSepa as $supplier) {
            $logs = $supplierAccountLogRepository->getSupplierAccountLogs($prospect, $supplier);

            $waitingSepas[] = $this->getSellAndSignStatus($logs[0]);
        }

        return $waitingSepas;
    }

    /**
     * Returns the display structure for a single supplier account log (@see getWaitingSepas)
     *
     * @param SupplierAccountLog $supplierAccountLog
     * @return array
     */
    private function getSellAndSignStatus(SupplierAccountLog $supplierAccountLog)
    {
        $sellAndSignService = $this->get('app.service.sell_and_sign');

        if ($supplierAccountLog->getContractId() === null) {
            $contractInfos = ['status' => 'Non établi'];
        } else {
            try {
                $contractInfos = $sellAndSignService->getContract($supplierAccountLog->getContractId());
            } catch (SellAndSignException $e) {
                $contractInfos = ['status' => 'ERROR', 'message' => $e->getMessage()];
            }
        }

        $closed = false;

        switch ($contractInfos['status']) {
            case 'OPEN':
                $sellSignStatus = 'Contrat établi, non signé';
                $class = 'warning';
                break ;
            case 'SIGNED':
                $sellSignStatus = 'Signé, en attente de validation';
                $class = 'positive';
                break ;
            case 'CLOSED':
            case 'ARCHIVED':
                $sellSignStatus = 'Validé';
                $class = 'positive';
                $closed = true;
                break ;
            case 'ABANDONED':
                $sellSignStatus = "Annulé ({$contractInfos['canceledReason']})";
                $class = 'negative';
                break ;
            case 'ERROR':
                $sellSignStatus = 'Erreur';
                $class = 'negative';
                break ;
            case 'PENDING':
                $sellSignStatus = 'En cours de traitement';
                $class = '';
                break ;
            default:
                $sellSignStatus = $contractInfos['status'];
                $class = '';
                break ;
        }

        $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();

        return [
            'id' => $supplier->getId(),
            'name' => $supplier->getDisplayName(),
            'class' => $class,
            'status' => SupplierAccountLog::getDisplayStatus($supplierAccountLog->getStatus()),
            'signed' => $supplierAccountLog->isSigned(),
            'closed' => $closed,
            'contractId' => $supplierAccountLog->getContractId(),
            'sellsignStatus' => $sellSignStatus,
        ];
    }

    private function getDisplaySupplierAccountLogs()
    {
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        /** @var SupplierAccountLog[] $logs */
        $logs = $supplierAccountLogRepository->findBy(['prospect' => $this->getManagedProspect()]);
        $supplierLogs = [];

        foreach ($logs as $log) {
            $supplierLogs[$log->getSupplierCategory()->getSupplier()->getId()][] = $log;
        }

        /**
         * @var int $supplierId
         * @var SupplierAccountLog[] $_supplierLogs
         */
        foreach ($supplierLogs as $supplierId => $_supplierLogs) {
            $status = null;
            $supplierLog = null;

            foreach ($_supplierLogs as $_supplierLog) {
                if ($status === null) {
                    $status = $_supplierLog->getStatus();
                    $supplierLog = $_supplierLog;
                    continue ;
                }

                switch ($_supplierLog->getStatus()) {
                    case SupplierAccountLog::STATUS_ACCOUNT_OPENED:
                    case SupplierAccountLog::STATUS_ORDERING:
                        $status = SupplierAccountLog::STATUS_ACCOUNT_OPENED;
                        $supplierLog = $_supplierLog;
                        break ;
                    case SupplierAccountLog::STATUS_ACCOUNT_BLOCKED:
                        $status = SupplierAccountLog::STATUS_ACCOUNT_BLOCKED;
                        $supplierLog = $_supplierLog;
                        break ;
                }
            }

            $supplierLogs[$supplierId] = $this->getSellAndSignStatus($supplierLog);
        }

        return array_values($supplierLogs);
    }

    public function retrieveEvidencesAction(int $contractId)
    {
        /** @var SellAndSignService $sellAndSignService */
        $sellAndSignService = $this->get('app.service.sell_and_sign');
        $prospect = $this->getManagedProspect();

        try {
            $contractInfos = $sellAndSignService->getContract($contractId);

            if ($contractInfos === null || $contractInfos['customerNumber'] !== "P{$prospect->getId()}") {
                return $this->redirectToRoute('app_front_404_page');
            }

            $contract = $sellAndSignService->getEvidences($contractId);

            // Return a response with the contract as content
            $response = new Response($contract);

            // Create the disposition of the file
            $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                str_replace('.pdf', '.zip', $contractInfos['filename'])
            );

            // Set the content disposition
            $response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', 'application/zip');

            // Dispatch request
            return $response;
        } catch (SellAndSignException $e) {
            return $this->redirectToRoute('app_front_404_page');
        }
    }

    public function retrieveSepaAction(int $contractId, string $mode)
    {
        /** @var SellAndSignService $sellAndSignService */
        $sellAndSignService = $this->get('app.service.sell_and_sign');
        $prospect = $this->getManagedProspect();

        try {
            $contractInfos = $sellAndSignService->getContract($contractId);

            if ($contractInfos === null || $contractInfos['customerNumber'] !== "P{$prospect->getId()}") {
                return $this->redirectToRoute('app_front_404_page');
            }

            if (in_array($contractInfos['status'],  ['CLOSED', 'ARCHIVED'])) {
                $contract = $sellAndSignService->getSignedContract($contractId);
            } else {
                $contract = $sellAndSignService->getCurrentDocumentForContract($contractId);
            }

            // Return a response with the contract as content
            $response = new Response($contract);

            // Create the disposition of the file
            $disposition = $response->headers->makeDisposition(
                $mode === 'download' ? ResponseHeaderBag::DISPOSITION_ATTACHMENT : ResponseHeaderBag::DISPOSITION_INLINE,
                $contractInfos['filename']
            );

            // Set the content disposition
            $response->headers->set('Content-Disposition', $disposition);
            $response->headers->set('Content-Type', 'application/pdf');

            // Dispatch request
            return $response;
        } catch (SellAndSignException $e) {
            return $this->redirectToRoute('app_front_404_page');
        }
    }

    public function waitingSuppliersAction()
    {
        $waitingSuppliers = $this->getWaitingSuppliersWithSepa();
        $ret = [];

        foreach ($waitingSuppliers as $waitingSupplier) {
            $ret[] = ['id' => $waitingSupplier->getId(), 'name' => $waitingSupplier->getDisplayName()];
        }

        return $this->json($ret);
    }

    public function prepareContractAction(Request $request)
    {
        $prospect = $this->getManagedProspect();
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($request->get('id'));
        /** @var SellAndSignService $sellAndSignService */
        $sellAndSignService = $this->get('app.service.sell_and_sign');

        try {
            $urls = $sellAndSignService->prepareContractSingleStep($prospect, $supplier);

            if ($urls === null) {
                return $this->json(['ok' => false, 'message' => "Une erreur inattendue s'est produite"]);
            }

            return $this->json(['ok' => true, 'link' => $urls['iframe']]);
        } catch (SellAndSignException $e) {
            return $this->json(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function updateSignatureAction(Request $request)
    {
        $contractId = $request->get('contractId');
        $isSigned = $request->get('signed') == 1;
        $logs = $this->get('app.repository.supplier_account_log')->updateSignature($contractId, $isSigned);

        // On notifie hello et le grossiste
        // Edit: No !! Le SEPA est validé de manière asynchrone, donc on ne peut pas espérer une réponse immédiate...
        if ($isSigned) {
//            $this->notifyOnSigned($logs[0], $contractId);
        }

        return $this->json(['ok' => true]);
    }

    private function notifyOnSigned(SupplierAccountLog $supplierAccountLog, $contractId)
    {
        $sellAndSignService = $this->get('app.service.sell_and_sign');
        $emailFactory = $this->get('app.factory.email');
        $prospect = $supplierAccountLog->getProspect();
        $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();
        $sendinBlueManager = $this->get('app.sendinblue.manager');
        $uploaderHelper = $this->get('vich_uploader.templating.helper.uploader_helper');

        try {
            $contract = $sellAndSignService->getSignedContract($contractId);

            // On injecte le rib
            $this->get('vich_uploader.upload_handler')->inject($prospect, 'ribFile');
            $builder = $emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

            $rib = $prospect->getRibFile();

            $builder
                ->setTemplateId(50)
                ->addTo($supplier->getEmail())
                ->addCc('hello@foodomarket.com')
                ->addVariable('RESTAURANT_NAME', $prospect->getRestaurantName())
                ->addVariable('RECIPIENTS', $supplier->getDisplayName())
                ->addVariable('ADDRESS_FULL', trim("{$prospect->getAddress()} {$prospect->getZipCode()} {$prospect->getCity()}"))
                ->addVariable('DELIVERY_HOURS', $prospect->getDeliveryHours())
                ->addVariable('MANAGER_NAME', "{$prospect->getFirstName()} {$prospect->getContactName()}")
                ->addVariable('INTERLOCUTOR', "{$prospect->getFirstName()} {$prospect->getContactName()}")
                ->addVariable('TELEPHONE', "{$prospect->getPhone()} / {$prospect->getMobile()}")
                ->addVariable('EMAIL', $prospect->getEmail())
                ->addAttachment("SEPA {$prospect->getRestaurantName()}.pdf", $contract)
            ;

            $baseUrl = $this->getParameter('router.request_context.full_url');

            if ($rib !== null) {
//                    $builder->addAttachment($rib->getFilename(), file_get_contents($rib->getPathname()));

                $ribUrl = $uploaderHelper->asset($prospect, 'ribFile');

                $builder->addVariable('RIB_URL', "{$baseUrl}{$ribUrl}");
            } else {
                $builder->addVariable('RIB_URL', $baseUrl);
            }

            $email = $builder->build();

            $sendinBlueManager->addEmailToQueue($email);
        } catch (SellAndSignException $e) {
        }
    }
}
