<?php


namespace AppBundle\MiddleOffice\Controller;


use AppBundle\Entity\Prospect;
use AppBundle\Helper\CurlHelper;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Services\ProspectSourceService;
use DateTime;
use Sylius\Component\User\Security\Generator\UniqueTokenGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validation;

class WelcomeController extends AbstractController
{
    use ShopContext;

    protected $prospectRepository;

    public function __construct(ProspectRepository $prospectRepository)
    {
        $this->prospectRepository = $prospectRepository;
    }

    public function index(Request $request, UniqueTokenGenerator $tokenGenerator, ProspectSourceService $prospectSourceService)
    {
        $this->prepareUtmSession();

        $openModal = !$request->cookies->get('redirectModalOpened');

        if ($request->get('submit')) {

            $data = [
                'email' => $request->get('email'),
                'full_name' => $request->get('full_name'),
                'mobile' => $request->get('mobile'),
                'restaurant_name' => $request->get('restaurant_name'),
                'full_address' => $request->get('full_address'),
                'number' => $request->get('number'),
                'street' => $request->get('street'),
                'city' => $request->get('city'),
                'zip' => $request->get('zip'),
                'country' => $request->get('country'),
            ];

            $violations = $this->validateRegisterForm($data);

            $errors = [];
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }

            if (empty($errors)) {

                $prospect = $this->prospectRepository->findOneBy(['email' => $data['email']]);

                if(!$prospect) {
                    $prospect = new Prospect();
                }

                preg_match('/(?<zip>\d+)/', $request->get('zip'), $match);

                $prospect->setEmail($request->get('email'));
                $prospect->setFirstName($request->get('full_name'));
                $prospect->setPhone($request->get('mobile'));
                $prospect->setMobile($request->get('mobile'));
                $prospect->setRestaurantName($request->get('restaurant_name'));
                $prospect->setAddress($request->get('number') . ' ' . $request->get('street'));
                $prospect->setCity($request->get('city'));
                $prospect->setZipCode($match['zip'] ?? '');
                $prospect->setHash($tokenGenerator->generate());
                $prospect->setEnabled(false);

                $this->bindUtmParametersToProspect($prospect);

                // Ajoute welcome as source
                $prospect->addSource('welcome');

                $this->prospectRepository->add($prospect);

                // Appel au service du ProspectSource et création de l'objet ProspectSource
                $prospectSourceService->createProspectSource($prospect, "Welcome");


                // Post data to zapier url
                $curlReponse = CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o4wyqkg/', [], [
                    'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                    'FIRST_NAME' => $prospect->getFirstName(),
                    'PHONE' => $prospect->getPhone(),
                    'MAIL' => $prospect->getEmail(),
                    'DATE' => (new DateTime())->format('c'),
                    'ADDRESS' => $prospect->getZipCode()
                ]);

                $this->get('session')->set('hash', $prospect->getHash());
                $this->get('session')->set('registrationToComplete', true);

                return $this->render('@App/MiddleOffice/Welcome/home/index.html.twig', ['openModal' => $openModal, 'registrationCompleted' => true]);
            }

            return $this->render('@App/MiddleOffice/Welcome/home/index.html.twig', ['errors' => $errors, 'values' => $data, 'openModal' => $openModal, 'registrationCompleted' => false]);
        } else {
            return $this->render('@App/MiddleOffice/Welcome/home/index.html.twig', ['openModal' => $openModal, 'registrationCompleted' => false]);
        }
    }

    private function validateRegisterForm(array $data)
    {
        $validator = Validation::createValidator();

        $groups = new Assert\GroupSequence(['Default', 'custom']);

        $constraint = new Assert\Collection([
            // the keys correspond to the keys in the input array
            'email' => new Assert\Email(['message' => "L'email n'est pas valide"]),
            'full_name' => new Assert\Length(['min' => 2]),
            'mobile' => new Assert\Regex(['pattern' => '^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}^', 'message' => "Le numéro de téléphone n'est pas valide"]),
            'restaurant_name' => new Assert\NotBlank(),
            'full_address' => new Assert\NotBlank(),
            'number' => new Assert\NotBlank(),
            'street' => new Assert\NotBlank(),
            'zip' => new Assert\NotBlank(),
            'city' => new Assert\NotBlank(),
            'country' => new Assert\NotBlank(),
        ]);

        $violations = $validator->validate($data, $constraint, $groups);

        $prospect = $this->prospectRepository->findOneBy(['email' => $data['email']]);

        if ($prospect) {
            if (!$prospect->getCustomers()->isEmpty()) {
                $emailUnityViolation = new ConstraintViolation("L'email est déjà utilisé par un autre compte", "L'email est déjà utilisé par un autre compte", [], '', '[email]', '');
                $violations->add($emailUnityViolation);
            }
        }

        return $violations;
    }
}