<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Prospect;
use AppBundle\Form\Type\LiformVichFileType;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class MyAccountController extends AbstractController
{
    use ShopContext;

    public function sepaRequirementsAction(Request $request)
    {
        return $this->handleForm($request, $this->getSepaRequirementsForm());
    }

    public function bankDetailsAction(Request $request)
    {
        return $this->handleForm($request, $this->getBankDetailsForm());
    }

    /**
     * @param Request $request
     * @param FormInterface $form
     * @return JsonResponse
     */
    private function handleForm(Request $request, FormInterface $form)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->json(['ok' => true, 'form' => $this->liformize($form)]);
            } else {
                return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
            }
        }

        return $this->json(['form' => $this->liformize($form)]);
    }

    /**
     * @return FormInterface
     */
    private function getBankDetailsForm()
    {
        $prospect = $this->getManagedProspect();

        $builder = $this->createFormBuilder(
            $prospect,
            [
                'csrf_protection' => false,
                'action' => $this->generateUrl('app_front_my_account_bank_details_form'),
                'data_class' => Prospect::class,
            ])
            ->add('iban', TextType::class, [
                'label' => 'Iban',
                'constraints' => [
                    new NotBlank(),
                    new Iban()
                ]
            ])
            ->add('ribFile', LiformVichFileType::class, [
                'required' => empty($prospect->getRib()),
                'label' => 'Votre RIB',
                'download_uri' => true,
                'download_label' => 'Télécharger le RIB',
                'allow_delete' => false,
                'constraints' => empty($prospect->getRib()) ? [
                    new NotBlank()
                ] : []
            ])
        ;

        return $builder->getForm();
    }

    /**
     * @return FormInterface
     */
    private function getSepaRequirementsForm()
    {
        $prospect = $this->getManagedProspect();

        $builder = $this->createFormBuilder(
            $prospect,
            [
                'csrf_protection' => false,
                'action' => $this->generateUrl('app_front_my_account_sepa_requirements_form'),
                'data_class' => Prospect::class,
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('contactName', TextType::class, [
                'label' => 'Votre nom',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('mobile', TextType::class, [
                'label' => 'Numéro de téléphone portable',
                'liform' => ['description' => 'Vous recevrez un code de validation par SMS à ce numéro'],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('companyName', TextType::class, [
                'label' => 'Nom de votre société',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('address', TextType::class, [
                'label' => 'Rue',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'Code postal',
                'constraints' => [
                    new NotBlank(),
                    new Regex(['pattern' => '/^[\d]{5,5}$/'])
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('iban', TextType::class, [
                'label' => 'Iban',
                'constraints' => [
                    new NotBlank(),
                    new Iban()
                ]
            ])
            ->add('ribFile', LiformVichFileType::class, [
                'required' => empty($prospect->getRib()),
                'label' => 'Votre RIB',
                'download_uri' => true,
                'download_label' => 'Télécharger le RIB',
                'allow_delete' => false,
                'constraints' => empty($prospect->getRib()) ? [
                    new NotBlank()
                ] : []
            ])
        ;

        return $builder->getForm();
    }
}