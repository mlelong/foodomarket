<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Email;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\OrderPayment;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Entity\ShoppingCartItem;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\ShopUser;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Services\Arbitration\DataTransformer\ShoppingCartOptimData;
use AppBundle\Services\Arbitration\DataTransformer\ShoppingCartOptimTransformer;
use AppBundle\Services\OrderService;
use AppBundle\Services\Payment\Lyra\LyraPaymentService;
use AppBundle\Services\ShoppingCartManager;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Sylius\Component\Core\Model\ProductImage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CartController extends AbstractController
{
    use ShopContext;

    /**
     * @var LyraPaymentService
     */
    private $lyraPaymentService;

    public function __construct(LyraPaymentService $lyraPaymentService)
    {
        $this->lyraPaymentService = $lyraPaymentService;
    }

    public function index(Request $request)
    {
        $orderId = $request->get('orderId');
        $result = $request->get('vads_auth_result');
        $transactionStatus = $request->get('vads_trans_status');
        $order = null;

        if ($orderId) {
            /** @var Order $order */
            $order = $this->get('sylius.repository.order')->find($orderId);
        }

        // Bad result from Lyra, cancel order and restore state
        if ($result !== null && $result !== '00') {
            $shoppingCart = $order->getShoppingCart();

            $shoppingCart->setOrdered(false);

            $order->setState('failed');
            $order->setShoppingCart(null);

            /** @var OrderItem $item */
            foreach ($order->getItems() as $item) {
                $item->setShoppingCartItem(null);
            }

            $this->getDoctrine()->getManager()->flush();
        } elseif ($order && $result === '00' && $request->get('createAlias') == '1') {
            /** @var OrderPayment $lyraPayment */
            $lyraPayment = $order->getLyraPayments()->last();
            $this->lyraPaymentService->handleTokenWebhook($lyraPayment->getLyraId());
        }

        return $this->responseAction(['paymentResult' => $result, 'transactionStatus' => $transactionStatus]);
    }

    public function optimizeSuppliersAction()
    {
        $suppliers = $this->get('app.repository.supplier')->getByRestaurant($this->getRestaurant());
        $suppliersDisplay = [];

        foreach ($suppliers as $supplier) {
            $suppliersDisplay[] = [
                'id' => $supplier->getId(),
                'name' => $supplier->getDisplayName(),
                'checked' => true
            ];
        }


        return $this->json($suppliersDisplay);
    }

    public function optimizeResultAction(Request $request)
    {
        /** @var ShoppingCart[] $shoppingCarts */
        $shoppingCarts = $this->get('app.repository.shopping_cart')->findBy([
            'restaurant' => $this->getRestaurant(),
            'ordered' => false
        ]);

        /** @var int $maxDeliveries */
        $maxDeliveries = $request->attributes->get('deliveries');

        $shoppingCartOptimData = new ShoppingCartOptimData($request->get('algo'), $this->getRestaurant(), $maxDeliveries, $shoppingCarts, $request->get('suppliers', []));
        /** @var ShoppingCartOptimTransformer $shoppingCartOptimTransformer */
        $shoppingCartOptimTransformer = $this->get('app.arbitration.transformer.shopping_cart_optim_transformer');

        try {
            $optimum = $shoppingCartOptimTransformer->transform($shoppingCartOptimData);

            $this->get('session')->set('optim', $optimum);
        } catch (TransformationFailedException $e) {
            return $this->json(['ok' => false, 'message' => $e->getMessage(), 'data' => [], 'originalTotal' => 0, 'optimTotal' => 0]);
        }

        return $this->json($optimum);
    }

    public function optimizeAction()
    {
        $restaurant = $this->getManagedRestaurant();

        if ($restaurant === null) {
            return $this->json(['ok' => false, 'message' => 'Vous avez été déconnecté de notre plateforme, merci de vous reconnecter']);
        }

        $optim = $this->get('session')->get('optim', null);

        if ($optim === null) {
            return $this->json(['ok' => false, 'message' => 'La session a expiré. Veuillez relancer le calcul de l\'optimum avant de valider.']);
        }

        /** @var ShoppingCartOptimTransformer $shoppingCartOptimTransformer */
        $shoppingCartOptimTransformer = $this->get('app.arbitration.transformer.shopping_cart_optim_transformer');
        $optimizedShoppingCarts = $shoppingCartOptimTransformer->reverseTransform($optim);

        if (empty($optimizedShoppingCarts)) {
            return $this->json(['ok' => false, 'message' => 'Une erreur est survenue, merci de réessayer dans quelques minutes']);
        }

        $shoppingCartRepository = $this->get('app.repository.shopping_cart');
        /** @var ShoppingCart[] $shoppingCarts */
        $oldShoppingCarts = $shoppingCartRepository->findBy(['restaurant' => $this->getRestaurant(), 'ordered' => false]);

        // On supprime les anciens paniers
        foreach ($oldShoppingCarts as $shoppingCart) {
            $shoppingCartRepository->remove($shoppingCart);
        }

        // Et on les remplace par les versions optimisées
        foreach ($optimizedShoppingCarts as $shoppingCart) {
            $shoppingCart->setRestaurant($restaurant);

            $shoppingCartRepository->add($shoppingCart);
        }

        return $this->json(['ok' => true]);
    }

    public function orderHistoryAction()
    {
        $imagineCacheManager = $this->get('liip_imagine.cache.manager');
        $restaurant = $this->getRestaurant();

        $manager = $this->getDoctrine()->getManager();
        $shoppingCartRepository = $manager->getRepository(ShoppingCart::class);
        $productPictureRepository = $manager->getRepository(ProductImage::class);

        /** @var ShoppingCart[] $shoppingCarts */
        $shoppingCarts = $shoppingCartRepository->findBy(['restaurant' => $restaurant, 'ordered' => true], ['updatedAt' => 'DESC']);
        $carts = [];

        foreach ($shoppingCarts as $shoppingCart) {
            /** @var Order $order */
            $order = $shoppingCart->getOrder();

            if ($order === null) {
                continue ;
            }

            /** @var OrderItem[]|ArrayCollection $items */
            $items = $order->getItems();

            $orderDate = $order->getCheckoutCompletedAt()->format('d/m/Y');
            $orderTime = $order->getCheckoutCompletedAt()->format('H:i:s');
            $nbItems = $items->count();

            $cart = [
                'date' => $orderDate,
                'time' => $orderTime,
                'listName' => "Commande du {$orderDate}",
                'nbItems' => $nbItems,
                'nbSuppliers' => 0,
                'suppliers' => [$shoppingCart->getSupplier()->getId()],
                'products' => [],
                'cartNote' => $shoppingCart->getNote(),
                'supplierName' => $shoppingCart->getSupplier()->getName()
            ];

            $productFinder = $this->get('app.service.product_finder');

            foreach ($items as &$item) {
                $variant = $item->getVariant();

                $data = $productFinder->getProduct($variant, $shoppingCart->getSupplier(), $shoppingCart->getRestaurant());

                $pictures = $productPictureRepository->findBy(['owner' => $variant->getProduct()]);

                $quantityUnitSelected = '';
                $note = '';

                foreach ($shoppingCart->getItems() as &$shoppingCartItem) {
                    if ($shoppingCartItem->getProductVariant()->getId() == $variant->getId()) {
                        $quantityUnitSelected = $shoppingCartItem->getUnit();
                        $note = $shoppingCartItem->getNote();
                    }
                }

                $cart['products'][] = [
                    'cartId' => $shoppingCart->getId(),
                    'productId' => $variant->getId(),
                    'cartItemId' => $item->getId(),
                    'supplierId' => $shoppingCart->getSupplier()->getId(),
                    'productName' => !empty($data['productName2']) ? $data['productName2'] : $data['productName'],
                    'productSupplier' => $shoppingCart->getSupplier()->getName(),
                    'quantity' => $item->getQuantityFloat(),
                    'quantityUnitSelected' => $quantityUnitSelected,
                    'productPrice' => bcdiv($item->getUnitPrice(), 100, 2),
                    'note' => $note,
                    'amount' => bcdiv($item->getTotal(), 100, 2),
                    'productPicture' => !empty($pictures) ? $imagineCacheManager->getBrowserPath($pictures[0]->getPath(), 'front_tiny') : $imagineCacheManager->getBrowserPath('shop-default-product.jpg', 'front_tiny')
                ];
            }

            $cart['suppliers'] = array_unique($cart['suppliers']);
            $cart['nbSuppliers'] = count($cart['suppliers']);
            $cart['amount'] = bcdiv($order->getItemsTotal(), 100, 2);

            if (!isset($carts[$orderDate])) {
                $carts[$orderDate] = ['date' => $orderDate, 'orders' => []];
            }

            $carts[$orderDate]['orders'][] = $cart;
        }

        return new JsonResponse(['ok' => true, 'orders' => array_values($carts)]);
    }

    public function previouslyOrderedAction(Request $request)
    {
        $restaurant = $this->getRestaurant();

        $manager = $this->getDoctrine()->getManager();
        $shoppingCartRepository = $manager->getRepository(ShoppingCart::class);

        /** @var ShoppingCart[] $shoppingCarts */
        $shoppingCarts = $shoppingCartRepository->createQueryBuilder('sc')
            ->andWhere('sc.restaurant = :restaurant')
            ->setParameter('restaurant', $restaurant)
            ->andWhere('sc.ordered = 1')
            ->andWhere('sc.updatedAt BETWEEN :start AND :end')
            ->setParameter('start', $request->query->get('start'))
            ->setParameter('end', $request->query->get('end'))
            ->addOrderBy('sc.updatedAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;

        $carts = [];

        foreach ($shoppingCarts as $shoppingCart) {
            $order = $shoppingCart->getOrder();

            if ($order === null) {
                continue ;
            }

            $orderDate = $order->getCheckoutCompletedAt()->format('d/m/Y');

            if (isset($carts[$orderDate])) {
                $cart = $carts[$orderDate];
                $cart['suppliers'][] = $shoppingCart->getSupplier()->getId();
                $cart['supplierNames'][] = $shoppingCart->getSupplier()->getName();
            } else {
                $cart = [
                    'date' => $orderDate,
                    'listName' => "Commande #{$order->getId()} du {$orderDate}",
                    'nbItems' => 0,
                    'nbSuppliers' => 0,
                    'suppliers' => [$shoppingCart->getSupplier()->getId()],
                    'products' => [],
                    'items' => [],
                    'supplierNames' => [$shoppingCart->getSupplier()->getName()]
                ];
            }

            foreach ($shoppingCart->getItems() as $item) {
                $key = "{$item->getProductVariant()->getId()}-{$item->getUnit()}";
                $cart['items'][$key] = true;
            }

            $cart['suppliers'] = array_unique($cart['suppliers']);
            $cart['supplierNames'] = array_values(array_unique($cart['supplierNames']));
            $cart['nbSuppliers'] = count($cart['suppliers']);
            $cart['nbItems'] = count($cart['items']);

            $carts[$orderDate] = $cart;
        }

        return new JsonResponse(['ok' => true, 'carts' => array_values($carts)]);
    }

    public function previousOrderAction(Request $request)
    {
        $date = $request->get('date');
        $items = [];

        /** @var Order[] $orders */
        $orders = $this->get('sylius.repository.order')
            ->createQueryBuilder('o')
            ->join('o.shoppingCart', 'sc')
//            ->where('o.customer = :customer')
            ->where('sc.restaurant = :restaurant')
            ->andWhere('DATE(o.checkoutCompletedAt) = :date')
            ->setParameter('date', $date)
//            ->setParameter('customer', $this->getUser()->getCustomer()->getId())
            ->setParameter('restaurant', $this->getRestaurant()->getId())
            ->getQuery()
            ->getResult()
        ;

        $productFinder = $this->get('app.service.product_finder');
        $productUtils = $this->get('app.utils.product');

        foreach ($orders as $order) {
            $shoppingCart = $order->getShoppingCart();

            foreach ($shoppingCart->getItems() as $item) {
                $productFromSrv = $productFinder->getProduct(
                    $item->getProductVariant(),
                    $shoppingCart->getSupplier(),
                    $shoppingCart->getRestaurant()
                );

                if ($productFromSrv !== null) {
                    $key = "{$item->getProductVariant()->getId()}-{$item->getUnit()}";

                    if (!isset($items[$key])) {
                        $items[$key] = $productFromSrv;
                        $items[$key]['quantityUnitSelected'] = $item->getUnit();
                    }

                    $items[$key]['quantity'] += $item->getQuantity();
                } else {
                    $hasReplacement = false;
                    /** @var ProductVariant[]|ArrayCollection $variants */
                    $variants = $productUtils->getSimilarVariantsForSupplier($item->getProductVariant(), $item->getSupplier(), true);

                    foreach ($variants as $variant) {
                        $replacement = $productFinder->getProduct($variant, $item->getSupplier(), $shoppingCart->getRestaurant());

                        if ($replacement !== null) {
                            $hasReplacement = true;

                            $replacement['note'] = $item->getNote();

                            $key = "{$variant->getId()}-{$item->getUnit()}";

                            if (!isset($items[$key])) {
                                $items[$key] = $replacement;
                                $items[$key]['quantityUnitSelected'] = $item->getUnit();
                            }

                            $items[$key]['quantity'] += $item->getQuantity();
                        }
                    }

                    if (!$hasReplacement) {
                        $productFromSrv = [
                            'productId' => $item->getProductVariant()->getId(),
                            'productName' => $item->getProductVariant()->getFullDisplayName() . ' (plus commercialisé)',
                            'origin' => $item->getProductVariant()->getDisplayOrigin(),
                            'isInCatalog' => false,
                            'kgPrice' => 0,
                            'unitPrice' => 0,
                            'supplierId' => $item->getSupplier()->getId(),
                            'supplierName' => $item->getSupplier()->getName(),
                            'quantityUnit' => $item->getUnit(),
                            'quantity' => 0
                        ];

                        $key = "{$item->getProductVariant()->getId()}-{$item->getUnit()}";

                        if (!isset($items[$key])) {
                            $items[$key] = $productFromSrv;
                            $items[$key]['quantityUnitSelected'] = $item->getUnit();
                        }

                        $items[$key]['quantity'] += $item->getQuantity();
                    }
                }
            }
        }

        $date = implode('/', array_reverse(explode('-', $date)));

        return new JsonResponse(['ok' => true, 'listName' => "Commande du {$date}", 'data' => array_values($items)]);
    }

    /**
     * Returns the shopping cart associated to user and supplier
     *
     * @param Restaurant $restaurant
     * @param Supplier $supplier
     * @return ShoppingCart
     */
    private function getShoppingCart(Restaurant $restaurant, Supplier $supplier)
    {
        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();
        $shoppingCartRepository = $manager->getRepository(ShoppingCart::class);
        /** @var ShoppingCart $shoppingCart */
        $shoppingCart = $shoppingCartRepository->findOneBy(['restaurant' => $restaurant, 'supplier' => $supplier, 'ordered' => false]);

        if ($shoppingCart !== null) {
            return $shoppingCart;
        }

        $shoppingCart = new ShoppingCart();

        try {
            // Just in case the entity is not managed after an unserialize operation...
            $restaurant = $manager->find(Restaurant::class, $restaurant->getId());

            $shoppingCart->setRestaurant($restaurant);
            $shoppingCart->setSupplier($supplier);

            $manager->persist($shoppingCart);
            $manager->flush();
        } catch (ORMException $e) {
        }

        return $shoppingCart;
    }

    public function emptyCartsAction()
    {
        $restaurant = $this->getRestaurant();

        $manager = $this->getDoctrine()->getManager();
        $shoppingCartRepository = $manager->getRepository(ShoppingCart::class);
        /** @var ShoppingCart[] $shoppingCarts */
        $shoppingCarts = $shoppingCartRepository->findBy(['restaurant' => $restaurant, 'ordered' => false]);

        foreach ($shoppingCarts as $shoppingCart) {
            $manager->remove($shoppingCart);
        }

        $manager->flush();

        return new JsonResponse(['ok' => true]);
    }

    public function emptyCartAction(Request $request)
    {
        $restaurant = $this->getRestaurant();

        $manager = $this->getDoctrine()->getManager();
        $shoppingCartRepository = $manager->getRepository(ShoppingCart::class);
        /** @var ShoppingCart $shoppingCart */
        $shoppingCart = $shoppingCartRepository->findOneBy(['restaurant' => $restaurant, 'ordered' => false, 'id' => $request->get('id')]);

        $manager->remove($shoppingCart);
        $manager->flush();

        return new JsonResponse(['ok' => true]);
    }

    public function addAllToCartAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $products = $request->get('products');
        $supplierRepository = $manager->getRepository(Supplier::class);
        /** @var ProductVariantRepository $productVariantRepository */
        $productVariantRepository = $this->get('sylius.repository.product_variant');
        $restaurant = $this->getRestaurant();
        $prospect = $this->getManagedProspect();

        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');

        foreach ($products as $index => &$product) {
            /** @var ProductVariant $productVariant */
            $productVariant = $productVariantRepository->find($product['productId']);
            /** @var Supplier $supplier */
            $supplier = $supplierRepository->find($product['supplierId']);

            $canOrder = $supplierAccountLogRepository->canOrderWith($prospect, $supplier);

            if (!$canOrder) {
                unset($products[$index]);

                continue ;
            }

            $shoppingCart = $this->getShoppingCart($restaurant, $supplier);

            /** @var ShoppingCartItem $item */
            foreach ($shoppingCart->getItems() as &$item) {
                if ($item->getProductVariant()->getId() == $productVariant->getId()) {
                    $product['cartId'] = $shoppingCart->getId();
                    $product['cartItemId'] = $item->getId();

                    if (isset($product['note']) && !empty($product['note'])) {
                        $item->setNote($product['note']);
                        $product['previousNote'] = $product['note'];

                        $manager->flush();
                    } else {
                        $product['previousNote'] = $item->getNote();
                    }

                    continue 2;
                }
            }

            $shoppingCartItem = new ShoppingCartItem();

            $shoppingCartItem->setProductVariant($productVariant);
            $shoppingCartItem->setShoppingCart($shoppingCart);
            $shoppingCartItem->setQuantity($product['quantity']);
            $shoppingCartItem->setUnit(strtoupper($product['quantityUnitSelected']));

            $shoppingListItemNote = '';

            if (!isset($product['note']) || empty($product['note'])) {
                try {
                    /** @var EntityRepository $shoppingListItemRepository */
                    $shoppingListItemRepository = $manager->getRepository(RestaurantStockItem::class);
                    $qb = $shoppingListItemRepository->createQueryBuilder('rsi');

                    $shoppingListItemNote = $qb
                        ->select('rsi.note')
                        ->join('rsi.restaurantStock', 'rs')
                        ->where('rs.restaurant = :restaurant')
                        ->andWhere('rsi.productVariant = :variant')
                        ->andWhere('rsi.supplier = :supplier')
                        ->setParameter('restaurant', $restaurant)
                        ->setParameter('variant', $productVariant)
                        ->setParameter('supplier', $supplier)
                        ->getQuery()
                        ->getSingleScalarResult();

                    $shoppingCartItem->setNote($shoppingListItemNote);
                } catch (Exception $ignored) {
                }
            } else {
                $shoppingListItemNote = $product['note'];

                $shoppingCartItem->setNote($shoppingListItemNote);
            }

            $manager->persist($shoppingCartItem);
            $manager->flush();

            $product['cartId'] = $shoppingCart->getId();
            $product['cartItemId'] = $shoppingCartItem->getId();
            $product['previousNote'] = $shoppingListItemNote;
        }

        return new JsonResponse(['ok' => true, 'products' => array_values($products)]);
    }

    public function addToCartAction(Request $request) {
        $manager = $this->getDoctrine()->getManager();
        $product = $request->request->get('product');
        $supplierRepository = $manager->getRepository(Supplier::class);
        /** @var ProductVariantRepository $productVariantRepository */
        $productVariantRepository = $this->get('sylius.repository.product_variant');
        /** @var ProductVariant $productVariant */
        $productVariant = $productVariantRepository->find($product['v']);
        /** @var Supplier $supplier */
        $supplier = $supplierRepository->find($product['s']);

        /** @var Prospect $prospect */
        $prospect = $this->getManagedProspect();

        $canOrder = $this->get('app.repository.supplier_account_log')->canOrderWith($prospect, $supplier);

        if (!$canOrder) {
            return new JsonResponse(['ok' => false, 'message' => "Il faut d'abord vous ouvrir un compte chez ce fournisseur"]);
        }

        $restaurant = $this->getRestaurant();

        $shoppingCart = $this->getShoppingCart($restaurant, $supplier);

        $alreadyAdded = false;

        /** @var ShoppingCartItem $item */
        foreach ($shoppingCart->getItems() as &$item) {
            if ($item->getProductVariant()->getId() == $productVariant->getId()) {
                $alreadyAdded = true;
                break ;
            }
        }

        if ($alreadyAdded) {
            return new JsonResponse(['ok' => false, 'message' => 'Product already added']);
        }

        $shoppingCartItem = new ShoppingCartItem();

        $shoppingCartItem->setProductVariant($productVariant);
        $shoppingCartItem->setShoppingCart($shoppingCart);
        $shoppingCartItem->setQuantity($product['q']);
        $shoppingCartItem->setUnit(strtoupper($product['u']));

        $shoppingListItemNote = '';

        if (!isset($product['note']) || empty($product['note'])) {
            try {
                /** @var EntityRepository $shoppingListItemRepository */
                $shoppingListItemRepository = $manager->getRepository(RestaurantStockItem::class);
                $qb = $shoppingListItemRepository->createQueryBuilder('rsi');

                $shoppingListItemNote = $qb
                    ->select('rsi.note')
                    ->join('rsi.restaurantStock', 'rs')
                    ->where('rs.restaurant = :restaurant')
                    ->andWhere('rsi.productVariant = :variant')
                    ->andWhere('rsi.supplier = :supplier')
                    ->setParameter('restaurant', $restaurant->getId())
                    ->setParameter('variant', $productVariant->getId())
                    ->setParameter('supplier', $supplier->getId())
                    ->addOrderBy('rsi.id', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getSingleScalarResult();

                $shoppingCartItem->setNote($shoppingListItemNote);
            } catch (Exception $ignored) {
            }
        } else {
            $shoppingListItemNote = $product['note'];

            $shoppingCartItem->setNote($shoppingListItemNote);
        }

        $manager->persist($shoppingCartItem);
        $manager->flush();

        return new JsonResponse(['ok' => true, 'cartId' => $shoppingCart->getId(), 'cartItemId' => $shoppingCartItem->getId(), 'previousNote' => $shoppingListItemNote]);
    }

    public function removeFromCartAction(Request $request) {
        $manager = $this->getDoctrine()->getManager();
        $cartItemId = $request->get('itemId');
        $shoppingCartItemRepository = $manager->getRepository(ShoppingCartItem::class);

        /** @var ShoppingCartItem $shoppingCartItem */
        $shoppingCartItem = $shoppingCartItemRepository->find($cartItemId);

        if ($shoppingCartItem === null) {
            return new JsonResponse(['ok' => false, 'message' => "Unable to find shopping cart item with id `$cartItemId`"]);
        }

        $shoppingCart = $shoppingCartItem->getShoppingCart();

        $manager->remove($shoppingCartItem);
        $manager->flush();

        // Remove shopping cart if empty too
        if ($shoppingCart->getItems()->isEmpty()) {
            $manager->remove($shoppingCart);
            $manager->flush();
        }

        return new JsonResponse(['ok' => true]);
    }

    public function updateQuantityAction(Request $request) {
        $manager = $this->getDoctrine()->getManager();
        $cartItemId = $request->get('itemId');
        $shoppingCartItemRepository = $manager->getRepository(ShoppingCartItem::class);
        $quantity = $request->get('qty');
        $quantityUnit = $request->get('unit');

        if (!is_numeric($quantity) || $quantity <= 0) {
            return new JsonResponse(['ok' => false, 'message' => "Quantity must be numeric type and its value > 0"]);
        }

        /** @var ShoppingCartItem $shoppingCartItem */
        $shoppingCartItem = $shoppingCartItemRepository->find($cartItemId);

        if ($shoppingCartItem === null) {
            return new JsonResponse(['ok' => false, 'message' => "Unable to find shopping cart item with id `$cartItemId`"]);
        }

        $shoppingCartItem->setQuantity($quantity);
        $shoppingCartItem->setUnit($quantityUnit);

        $manager->flush();

        return new JsonResponse(['ok' => true]);
    }

    public function setProductNoteAction(Request $request)
    {
        $itemId = $request->get('itemId');
        $manager = $this->getDoctrine()->getManager();
        /** @var ShoppingCartItem $cartItem */
        $cartItem = $manager->getRepository(ShoppingCartItem::class)->find($itemId);

        if ($cartItem === null) {
            return new JsonResponse(['ok' => false, 'note' => "Unable to find shopping cart item with id `$itemId`"]);
        }

        $cartItem->setNote($request->get('note'));
        $manager->flush();

        return new JsonResponse(['ok' => true]);
    }

    public function setCartNoteAction(Request $request)
    {
        $cartId = $request->get('id');

        $manager = $this->getDoctrine()->getManager();
        /** @var ShoppingCart $cart */
        $cart = $manager->getRepository(ShoppingCart::class)->find($cartId);

        if ($cart === null) {
            return new JsonResponse(['ok' => false, 'note' => "Unable to find shopping cart with id `$cartId`"]);
        }

        $cart->setNote($request->get('note'));
        $manager->flush();

        return new JsonResponse(['ok' => true]);
    }

    /**
     * @param ShoppingCart $shoppingCart
     * @return Order
     */
    private function placeOrder(ShoppingCart $shoppingCart)
    {
        /** @var ShopUser $user */
        $user = $this->getUser();

        /** @var OrderService $orderService */
        $orderService = $this->get('app.service.order');

        $order = $orderService->newFloatingOrder($user, $shoppingCart);

        $shoppingCart->setOrdered(true);
        $shoppingCart->setOrder($order);

        $this->getDoctrine()->getManager()->flush();

        return $order;
    }

    public function orderAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $shoppingCartRepository = $manager->getRepository(ShoppingCart::class);
        /** @var ShoppingCart $shoppingCart */
        $shoppingCart = $shoppingCartRepository->findOneBy(['restaurant' => $this->getRestaurant(), 'id' => $request->get('id')]);

        $order = $this->placeOrder($shoppingCart);
        $ret = $this->get('app.service.order')->postCreateOrder($order);
        $orderData = $ret['orderData'];
        $emails[] = [
            'supplierName' => $orderData['supplier_name'],
            'shoppingCartId' => $order->getShoppingCart()->getId(),
            'content' => $this->renderView('@App/MiddleOffice/Mail/order.html.twig', $orderData)
        ];

        return new JsonResponse(['ok' => true, 'emails' => $emails]);
    }

    public function orderAllAction()
    {
        $restaurant = $this->getRestaurant();

        $manager = $this->getDoctrine()->getManager();
        $shoppingCartRepository = $manager->getRepository(ShoppingCart::class);
        /** @var ShoppingCart[] $shoppingCarts */
        $shoppingCarts = $shoppingCartRepository->findBy(['restaurant' => $restaurant, 'ordered' => false]);
        $emails = [];

        $orderService = $this->get('app.service.order');

        foreach ($shoppingCarts as $shoppingCart) {
            $order = $this->placeOrder($shoppingCart);
            $ret = $orderService->postCreateOrder($order);
            $orderData = $ret['orderData'];

            $emails[] = [
                'supplierName' => $shoppingCart->getSupplier()->getName(),
                'shoppingCartId' => $shoppingCart->getId(),
                'content' => $this->renderView('@App/MiddleOffice/Mail/order.html.twig', $orderData)
            ];
        }

        return new JsonResponse(['ok' => true, 'emails' => $emails]);
    }

    public function scheduleAction(Request $request)
    {
        $cartId = $request->get('id');
        $scheduledDate = $request->get('date');
        /** @var ShoppingCart $shoppingCart */
        $shoppingCart = $this->get('app.repository.shopping_cart')->findOneBy(['id' => $cartId, 'restaurant' => $this->getRestaurant()->getId()]);

        if ($shoppingCart === null) {
            return new JsonResponse(['ok' => false, 'message' => "Cannot find cart with id `{$cartId}`"]);
        }

        $shoppingCart->setScheduledDate(new DateTime($scheduledDate));

        try {
            $this->getDoctrine()->getManager()->flush();
        } catch (OptimisticLockException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['ok' => true]);
    }
}