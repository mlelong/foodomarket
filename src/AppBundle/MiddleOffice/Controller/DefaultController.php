<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\RestaurantStock;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductImage;
use Sylius\Component\Core\Model\Taxon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends AbstractController
{
    use ShopContext;

    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        /** @var MobileDetector $mobileDetector */
        $mobileDetector = $this->get('mobile_detect.mobile_detector');

        $isMobile = $mobileDetector->isMobile();
        $isTablet = $mobileDetector->isTablet();

        $user = $this->getUser();

        if ($user !== null) {
            return $this->redirectToRoute('app_front_index');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('@App/MiddleOffice/Default/index.html.twig', [
            'authToken' => false,
            'schema' => null,
            'initialStates' => [
                'frontState' => [
                    'loginError' => $error,
                    'isMobile' => $isMobile,
                    'isTablet' => $isTablet,
                    'checkPath' => $this->generateUrl('front_login_check')
                ]
            ],
            'initialValues' => [],
            'user' => null
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->responseAction([
            'firstConnection' => $request->query->get('fc', 0),
            'topProducts' => $this->getShopUtils()->getTopProducts()['products'],
            'topCategories' => $this->getShopUtils()->getTopCategories(),
            'promos' => $this->getShopUtils()->getPromos()['products'],
        ]);
    }

    public function firstConnectAction()
    {
        return $this->redirectToRoute('app_front_order_suppliers_page', ['fc' => 1]);
    }

    public function orderSuppliersAction(Request $request)
    {
        return $this->responseAction();
    }

    public function logoutAction(Request $request)
    {
        $request->cookies->remove('BEARER');
        $response = $this->redirectToRoute('app_front_index');
        $this->get('session')->remove('_foodo_switch');

        $response->headers->clearCookie('BEARER');

        return $response;
    }

    public function getShoppingListsAction(Request $request)
    {
        $terms = '%' . implode('%', explode(' ', $request->get('terms', ''))) . '%';

        $restaurant = $this->getRestaurant();

        $shoppingListManager = $this->get('app.shopping_list_manager');
        $shoppingLists = $shoppingListManager->getShoppingLists($restaurant);

        $terms = str_replace('%', '[\S\s]*', $terms);

        $shoppingLists = array_values(array_filter($shoppingLists, function($shoppingList) use ($terms) {
            return preg_match("/$terms/i", $shoppingList['listName']) > 0;
        }));

        return new JsonResponse(['data' => $shoppingLists]);
    }

    public function getShoppingListItemsAction(Request $request)
    {
        /** @var RestaurantStock $restaurantStock */
        $restaurantStock = $this->get('app.repository.restaurant_stock')->find($request->get('id'));
        $restaurant = $this->getRestaurant();

        if ($restaurantStock === null || $restaurantStock->getRestaurant()->getId() != $restaurant->getId()) {
            return new JsonResponse(['ok' => false, 'message' => 'Cannot find list']);
        }

        $shoppingListItems = $this->get('app.shopping_list_manager')->getShoppingListItems($restaurantStock);

        return new JsonResponse(['data' => $shoppingListItems, 'listName' => $restaurantStock->getName(), 'listId' => $restaurantStock->getId()]);
    }

    public function getSuppliersAction()
    {
        return new JsonResponse(['data' => $this->get('app.service.product_finder')->getRestaurantSuppliers($this->getRestaurant())]);
    }

    private function parseTerms($terms)
    {
        if (stripos($terms, ',') !== false) {
            $multipleterms = explode(',', $terms);
            $terms = $multipleterms[0];
        }

        $terms = explode(' ', $terms);

        foreach ($terms as &$term) {
            if (strlen($term) >= 3 && strripos($term, 's') !== false) {
                $term = substr_replace($term, '', -1);
            }
        }

        $terms = implode('%', $terms);

        return $terms;
    }

    public function getSupplierProductsAction(Request $request)
    {
        $promoOnly = $request->query->get('promoOnly', false) === 'true';

        $productFinder = $this->get('app.service.product_finder');
        $supplierId = $request->get('id', 0);
        $page = $request->get('page', 0);
        $limit = $request->get('limit', 50);
        $terms = $this->parseTerms($request->get('terms', ''));
        $restaurant = $this->getRestaurant();

        if ($supplierId != 0) {
            /** @var Supplier $supplier */
            $supplier = $this->getDoctrine()->getManager()->find(Supplier::class, $supplierId);
            $products = $productFinder->getRestaurantSupplierProducts($restaurant, $supplier);
        } else {
            $products = $productFinder->getRestaurantProducts($restaurant);
        }

        $terms = str_replace('%', '[\S\s]*', $terms);
        $products = array_values(array_filter($products, function($product) use ($terms, $promoOnly) {
            if ($promoOnly && !$product['promo']) {
                return false;
            }

            $a = preg_match("/$terms/i", $product['productName']);
            $b = preg_match("/$terms/i", $product['productName2']);
            $c = preg_match("/$terms/i", $product['productName3']);
            $d = preg_match("/$terms/i", $product['taxon']);

            return ($a > 0 || $b > 0 || $c > 0 || $d > 0);
        }));

        $total = count($products);
        $products = array_slice($products, $page * $limit, $limit);

        $data = json_encode([
            'items' => $products,
            'total' => $total
        ], JSON_INVALID_UTF8_IGNORE);

        return JsonResponse::fromJsonString($data);
    }

    public function getProductsAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $page = $request->get('page', 0);
        $limit = $request->get('limit', 50);

        // 0 based
        ++$page;

        $terms = $request->get('terms', '');
        $terms = implode('%', explode(' ', $terms));

        /** @var QueryBuilder $qb */
        $qb = $manager->getRepository(SupplierProductVariantInformations::class)->createQueryBuilder('pvi');

        $qb
            ->select('p.id AS productId')
            ->addSelect('pt.name AS productName')
            ->addSelect('0 AS quantity')
            ->addSelect('\'ORDER_KGPC\' AS quantityUnit')
            ->addSelect('\'KG\' AS quantityUnitSelected')
            ->addSelect('img.path AS productPicture')
            ->join('pvi.product', 'p')
            ->join('p.translations', 'pt')
            ->join('pvi.supplier', 'supp')
            ->leftJoin(ProductImage::class, 'img', 'WITH', 'img.owner = p')
            ->andWhere('pt.locale = :locale')
            ->andWhere('supp.enabled = 1')
            ->setParameter('locale', 'fr_FR')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('pt.name', ':terms'),
                $qb->expr()->like('pvi.productLabel', ':terms')
            ))
//            ->andWhere($qb->expr()->like('pt.name', ':terms'))
            ->setParameter('terms', "%$terms%")
        ;

        $qb
            ->addGroupBy('p')
            ->addGroupBy('img')
            ->orderBy('productName', 'ASC')
        ;

        $total = (new Paginator($qb))->count();

        $offset = ($page - 1) * $limit;

        $products = $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        /** @var CacheManager */
        $imagineCacheManager = $this->get('liip_imagine.cache.manager');

        foreach ($products as &$product) {
            if (!is_null($product['productPicture'])) {
                $product['productPicture'] = $imagineCacheManager->getBrowserPath($product['productPicture'], 'front_tiny');
            } else {
                $product['productPicture'] = $imagineCacheManager->getBrowserPath('shop-default-product.jpg', 'front_tiny');
            }
        }

        return new JsonResponse([
            'items' => $products,
            'total' => $total
        ]);
    }

    public function getProductsSimpleAction(Request $request)
    {
        /** @var CacheManager */
        $imagineCacheManager = $this->get('liip_imagine.cache.manager');
        $productRepository = $this->get('sylius.repository.product');

        /** @var Product[] $products */
        $products = $productRepository->findByNamePart($request->get('terms', ''), 'fr_FR');
        $ret = [];

        $defaultProductImage = $imagineCacheManager->getBrowserPath('shop-default-product.jpg', 'front_tiny');

        foreach ($products as $product) {
            $taxon = $product->getMainTaxon();

            /** @var ProductImage $image */
            $image = $product->getImages()->first();

            $description = [];

            $ancestor = $taxon;
            $description[] = $ancestor->getName();

            /** @var Taxon $ancestor */
            while ($ancestor = $ancestor->getParent()) {
                if ($ancestor->getParent() !== null) {
                    $description[] = $ancestor->getName();
                }
            }

            $ret[] = [
                'id' => $product->getId(),
                'title' => $product->getName(),
                'description' => implode(' > ', array_reverse($description)),
                'image' => $image ? $imagineCacheManager->getBrowserPath($image->getPath(), 'front_tiny') : $defaultProductImage,
            ];
        }

        return new JsonResponse($ret);
    }

    public function getProductsCategoryAction(Request $request)
    {
        /** @var CacheManager */
        $imagineCacheManager = $this->get('liip_imagine.cache.manager');
        $productRepository = $this->get('sylius.repository.product');

        /** @var Product[] $products */
        $products = $productRepository->findByNamePart($request->get('terms', ''), 'fr_FR');
        $ret = [];

        $defaultProductImage = $imagineCacheManager->getBrowserPath('shop-default-product.jpg', 'front_tiny');

        foreach ($products as $product) {
            $taxon = $product->getMainTaxon();
            $key = $taxon->getName();

            if (!isset($ret[$key])) {
                $ret[$key] = [
                    'name' => $taxon->getName(),
                    'results' => []
                ];
            }

            /** @var ProductImage $image */
            $image = $product->getImages()->first();

            $description = [];

            $ancestor = $taxon;
            $description[] = $ancestor->getName();

            /** @var Taxon $ancestor */
            while ($ancestor = $ancestor->getParent()) {
                if ($ancestor->getParent() !== null) {
                    $description[] = $ancestor->getName();
                }
            }

            $ret[$key]['results'][] = [
                'id' => $product->getId(),
                'title' => $product->getName(),
                'description' => implode(' > ', array_reverse($description)),
                'image' => $image ? $imagineCacheManager->getBrowserPath($image->getPath(), 'front_tiny') : $defaultProductImage,
            ];
        }

        return new JsonResponse($ret);
    }
}
