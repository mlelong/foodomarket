<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\ProductPricesStatistics;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Repository\ProductPricesStatisticsRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use Doctrine\ORM\NonUniqueResultException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sylius\Component\Core\Model\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractController
{
    use ShopContext;

    public function indexAction(Request $request)
    {
        $slug = $request->get('slug');
        $productData = $this->getProductData($slug);

        if ($productData === null) {
            $slugs = $this->getShopUtils()->getProductsSlugs($this->getContextRestaurantId());

            $bestScore = PHP_INT_MIN;
            $bestSlug = null;

            foreach ($slugs as $productId => $productSlug) {
                similar_text($productSlug, $slug, $score);

                if ($score > $bestScore) {
                    $bestScore = $score;
                    $bestSlug = $productSlug;
                }
            }

            if ($bestSlug !== null) {
                return $this->redirectToRoute('app_front_product_page', ['slug' => urlencode($bestSlug)], 301);
            } else {
                return $this->redirectToRoute('app_front_404_page');
            }
        }

        return $this->responseAction(['product' => $productData]);
    }

    public function productAction(Request $request)
    {
        return new JsonResponse(['product' => $this->getProductData($request->get('slug'))]);
    }

    private function getProductData($slug)
    {
        /** @var Product $product */
        try {
            $product = $this->get('sylius.repository.product')->createQueryBuilder('p')
                ->innerJoin('p.translations', 't')
                ->where('t.slug = :slug')
                ->andWhere('t.locale = :locale')
                ->andWhere('p.enabled = 1')
                ->setParameter('slug', urldecode($slug))
                ->setParameter('locale', 'fr_FR')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $product = null;
        }

        if ($product === null) {
            return null;
        }

        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        /** @var CacheManager $liipImagineCacheManager */
        $liipImagineCacheManager = $this->get('liip_imagine.cache.manager');

        $taxon = $product->getMainTaxon();

        $taxons = [
            [
                'id' => $taxon->getId(),
                'name' => $taxon->getName(),
                'slug' => $taxon->getSlug()
            ]
        ];

        while ($taxon = $taxon->getParent()) {
            $taxons[] = [
                'id' => $taxon->getId(),
                'name' => $taxon->getName(),
                'slug' => $taxon->getSlug()
            ];
        }

        $supplierRepository = $this->get('app.repository.supplier');
        $restaurant = $this->getRestaurant();

        if ($restaurant === null) {
            $prices = $supplierProductPriceRepository->getPrices(
                $supplierRepository->getPartners(),
                [$product],
                null,
                [4],
                false
            );
        } else {
            $suppliers = $supplierRepository->getByRestaurant($restaurant);
            $suppliers = array_merge($suppliers, $supplierRepository->getFacadesByRestaurant($restaurant));

            $prices = $supplierProductPriceRepository->getRestaurantPrices(
                $suppliers,
                [$product],
                null,
                $restaurant,
                true
            );
        }

        $suppliers = [];
        $variants = [];

        $minPrice = null;
        $maxPrice = null;
        $isPromo = false;

        $bioCounter = $noBioCounter = 0;

        foreach ($prices as $price) {
            $variant = $price->getProductVariant();
            $suppliers[$price->getSupplier()->getId()] = $price->getSupplier();
            $variants[$variant->getId()] = $variant;
            $bioOptionValue = $variant->getOptionBio();

            if (!$bioOptionValue || $bioOptionValue->getValue() === 'NON') {
                ++$noBioCounter;
            } else {
                ++$bioCounter;
            }

            if ($price->isPromo()) {
                $isPromo = true;
            }
        }

        $productFinder = $this->get('app.service.product_finder');

        $productPricesStatisticsRepository = $this->getDoctrine()->getRepository(ProductPricesStatistics::class);
        $isBio = $bioCounter >= $noBioCounter;

        if ($isBio) {
            /** @var ProductPricesStatistics[] $pricesTrend */
            $pricesTrend = $productPricesStatisticsRepository->findBy(['product' => $product, 'isBio' => 1], ['updatedAt' => 'DESC'], 1);
        } else {
            /** @var ProductPricesStatistics[] $pricesTrend */
            $pricesTrend = $productPricesStatisticsRepository->findBy(['product' => $product, 'isBio' => 0], ['updatedAt' => 'DESC'], 1);
        }

        if (!empty($pricesTrend)) {
            $pricesTrend = $pricesTrend[0];
        } else {
            $pricesTrend = null;
        }

        $priceEvolution = $this->getPriceEvolution($product, $variants);

        $productArray = [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'picture' => $liipImagineCacheManager->getBrowserPath($product->getImages()->isEmpty() ? 'shop-default-product.jpg' : $product->getImages()[0]->getPath(), 'front_big'),
            'taxons' => array_reverse($taxons),
            'promo' => $isPromo,
            'priceEvolution' => $priceEvolution,
            'isBio' => $isBio,
            'prices' => [
                'kg' => [
                    'min' => $priceEvolution['unit'] === 'kg' ? ($pricesTrend ? $pricesTrend->getMinimumKgPrice() : 0) : 0,
                    'avg' => $priceEvolution['unit'] === 'kg' ? ($pricesTrend ? $pricesTrend->getAverageKgPrice() : 0) : 0,
                    'max' => $priceEvolution['unit'] === 'kg' ? ($pricesTrend ? $pricesTrend->getMaximumKgPrice() : 0) : 0
                ],
                'unit' => [
                    'min' => $priceEvolution['unit'] === 'pc' ? ($pricesTrend ? $pricesTrend->getMinimumUnitPrice() : 0) : 0,
                    'avg' => $priceEvolution['unit'] === 'pc' ? ($pricesTrend ? $pricesTrend->getAverageUnitPrice() : 0) : 0,
                    'max' => $priceEvolution['unit'] === 'pc' ? ($pricesTrend ? $pricesTrend->getMaximumUnitPrice() : 0) : 0
                ]
            ],
            'variants' => array_values(array_map(function(ProductVariant $variant) use($prices, $productFinder) {
                $saleUnit = $variant->getOptionSupplierSalesUnit()->getValue();

                return [
                    'id' => $variant->getId(),
                    'name' => $variant->getFullDisplayName(),
                    'offers' => array_values(array_reduce($prices, function($carry, SupplierProductPrice $price) use($variant, $saleUnit, $productFinder) {
                        if ($price->getProductVariant()->getId() === $variant->getId()) {
                            $supplier = $price->getSupplier();

                            if ($supplier->getFacadeSupplier() !== null) {
                                $supplierName = $supplier->getFacadeSupplier()->getName();
                            } else {
                                $supplierName = $supplier->getName();
                            }

                            $orderLineData = $productFinder->getProduct($variant, $supplier, $this->getRestaurant());

                            $carry[] = [
                                'supplierId' => $supplier->getId(),
                                'supplierName' => $supplierName,
                                'saleUnit' => $saleUnit,
                                'price' => $saleUnit === 'PC'
                                    ? bcdiv($price->getUnitPrice(), 100, 2)
                                    : bcdiv($price->getKgPrice(), 100, 2),
                                'orderLineData' => $orderLineData
                            ];
                        }

                        return $carry;
                    }, []))
                ];
            }, $variants))
        ];

        return $productArray;
    }

    /**
     * @param Product $product
     * @param ProductVariant[] $variants
     * @return array
     */
    private function getPriceEvolution(Product $product, array $variants)
    {
        /** @var ProductPricesStatisticsRepository $productPricesStatisticsRepository */
        $productPricesStatisticsRepository = $this->getDoctrine()->getRepository(ProductPricesStatistics::class);
        /** @var ProductPricesStatistics[] $noBio */
        $noBio = $productPricesStatisticsRepository->findBy(['product' => $product, 'isBio' => 0], ['updatedAt' => 'ASC']);
        /** @var ProductPricesStatistics[] $isBio */
        $isBio = $productPricesStatisticsRepository->findBy(['product' => $product, 'isBio' => 1], ['updatedAt' => 'ASC']);

        $evolution = [
            'nobio' => [],
            'bio' => [],
        ];

        $kgPcStats = array_reduce($variants, function($carry, ProductVariant $variant) {
            switch ($variant->getOptionSupplierSalesUnit()->getValue()) {
                case 'PC':
                    ++$carry['pc'];
                    break ;
                default:
                    ++$carry['kg'];
                    break ;
            }

            return $carry;
        }, ['kg' => 0, 'pc' => 0]);

        $useKg = false;

        if ($kgPcStats['kg'] >= $kgPcStats['pc']) {
            $useKg = true;
        }

        $callback = function(ProductPricesStatistics $productPricesStatistics) use($useKg) {
            if ($productPricesStatistics->getMinimumKgPrice() == 0 || $productPricesStatistics->getMaximumKgPrice() >= 999) {
                return null;
            }

            return [
                'date' => $productPricesStatistics->getUpdatedAt()->format('d/m/Y'),
                'min' => $useKg ? $productPricesStatistics->getMinimumKgPrice() : $productPricesStatistics->getMinimumUnitPrice(),
                'avg' => $useKg ? $productPricesStatistics->getAverageKgPrice() : $productPricesStatistics->getAverageUnitPrice(),
                'max' => $useKg ? $productPricesStatistics->getMaximumKgPrice() : $productPricesStatistics->getMaximumUnitPrice(),
            ];
        };

        foreach ($noBio as $productPricesStatistics) {
            $data = $callback($productPricesStatistics);

            if ($data !== null) {
                $evolution['nobio'][$data['date']] = $data;
            }
        }

        foreach ($isBio as $productPricesStatistics) {
            $data = $callback($productPricesStatistics);

            if ($data !== null) {
                $evolution['bio'][$data['date']] = $data;
            }
        }

        $evolution['nobio'] = array_values($evolution['nobio']);
        $evolution['bio'] = array_values($evolution['bio']);
        $evolution['unit'] = $useKg ? 'kg' : 'pc';

        return $evolution;
    }
}