<?php

namespace AppBundle\MiddleOffice\Controller\Traits;

use AppBundle\Form\Liform\Serializer\Normalizer\InitialValuesNormalizer;
use Limenius\Liform\Liform;
use Limenius\Liform\Serializer\Normalizer\FormErrorNormalizer;
use Symfony\Component\Form\FormInterface;

trait FormHelper
{
    /** @var Liform */
    private $liform;

    /** @var InitialValuesNormalizer */
    private $initialValuesNormalizer;

    /** @var FormErrorNormalizer */
    private $formErrorNormalizer;

    /**
     * @param Liform $liform
     * @param InitialValuesNormalizer $initialValuesNormalizer
     * @param FormErrorNormalizer $formErrorNormalizer
     * @required
     */
    public function setLiformSerializers(Liform $liform, InitialValuesNormalizer $initialValuesNormalizer, FormErrorNormalizer $formErrorNormalizer)
    {
        $this->liform = $liform;
        $this->initialValuesNormalizer = $initialValuesNormalizer;
        $this->formErrorNormalizer = $formErrorNormalizer;
    }

    protected function liformize(FormInterface $form)
    {
        return [
            'schema' => $this->getLiformSchema($form),
            'values' => $this->getLiformInitialValues($form),
            'errors' => $this->getLiformErrors($form)
        ];
    }

    protected function getLiformSchema(FormInterface $form)
    {
        return $this->liform->transform($form);
    }

    protected function getLiformInitialValues(FormInterface $form)
    {
        return $this->initialValuesNormalizer->normalize($form);
    }

    protected function getLiformErrors(FormInterface $form)
    {
        return $this->formErrorNormalizer->normalize($form);
    }
}