<?php

namespace AppBundle\MiddleOffice\Controller\Traits;

use AppBundle\Entity\AdminUser;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShopUser;
use AppBundle\Entity\Supplier;
use AppBundle\Form\Type\CreateAccountType;
use AppBundle\Form\Type\PasswordResetType;
use AppBundle\Repository\InvoiceRepository;
use AppBundle\Repository\LitigationRepository;
use AppBundle\Repository\OrderRepository;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\RestaurantStockRepository;
use AppBundle\Repository\ShoppingCartRepository;
use AppBundle\Repository\ShopUserRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Repository\WebHookRepository;
use AppBundle\Services\LitigationService;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueV3Manager;
use AppBundle\Services\OrderService;
use AppBundle\Services\Product\ProductFinderInterface;
use AppBundle\Services\ProductUtils;
use AppBundle\Services\ProspectSourceService;
use AppBundle\Services\RedisCacheManager;
use AppBundle\Services\SellAndSign\SellAndSignService;
use AppBundle\Services\ShoppingCartManager;
use AppBundle\Services\ShoppingListManager;
use AppBundle\Services\ShopUserService;
use AppBundle\Services\ShopUtils;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;
use Sylius\Component\User\Security\Generator\UniqueTokenGenerator;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use GeoIp2\Database\Reader;

trait ShopContext
{
    use FormHelper;

    /** @var MobileDetector */
    protected $mobileDetector;

    /**
     * @required
     * @param MobileDetector $mobileDetector
     */
    public function setMobileDetector(MobileDetector $mobileDetector)
    {
        $this->mobileDetector = $mobileDetector;
    }

    public static function getSubscribedServices()
    {
        $services = parent::getSubscribedServices();

        return array_merge(
            $services,
            [
                'sylius.shop_user.token_generator.password_reset' => UniqueTokenGenerator::class,
                'app.repository.shop_user' => ShopUserRepository::class,
                'sylius.repository.product' => ProductRepository::class,
                'liip_imagine.cache.manager' => CacheManager::class,
                'app.repository.prospect' => ProspectRepository::class,
                'app.repository.supplier' => SupplierRepository::class,
                'app.repository.supplier_product_price' => SupplierProductPriceRepository::class,
                'app.service.product_finder' => ProductFinderInterface::class,
                'app.service.shop_utils' => ShopUtils::class,
                'sylius.shop_user.token_generator.email_verification' => UniqueTokenGenerator::class,
                'app.service.prospect.source' => ProspectSourceService::class,
                'app.service.shop_user' => ShopUserService::class,
                'lexik_jwt_authentication.jwt_manager' => JWTManager::class,
                'event_dispatcher' => EventDispatcher::class,
                'app.factory.email' => EmailFactory::class,
                'app.sendinblue.manager' => SendInBlueV3Manager::class,
                'sylius.repository.product_variant' => ProductVariantRepository::class,
                'app.repository.supplier_account_log' => SupplierAccountLogRepository::class,
                'app.repository.restaurant' => RestaurantRepository::class,
                'app.shopping_cart_manager' => ShoppingCartManager::class,
                'app.shopping_list_manager' => ShoppingListManager::class,
                'sylius.repository.order' => OrderRepository::class,
                'app.service.order' => OrderService::class,
                'app.repository.invoice' => InvoiceRepository::class,
                'app.redis.cache.manager' => RedisCacheManager::class,
                'app.repository.restaurant_stock' => RestaurantStockRepository::class,
                'app.repository.shopping_cart' => ShoppingCartRepository::class,
                'app.service.sell_and_sign' => SellAndSignService::class,
                'app.utils.product' => ProductUtils::class,
                'app.service.litigation' => LitigationService::class,
                'app.repository.litigation' => LitigationRepository::class,
                'app.repository.web_hook' => WebHookRepository::class
            ]
        );
    }

    /**
     * @param Request $request
     * @return Prospect
     */
    protected function getContextProspect(Request $request)
    {
        // Takes the hash from either the url or the cookie
        // Preference is for the url
        $hash = $request->get('h', $request->cookies->get('_hash'));
        $cookieRestaurant = $request->cookies->get('_restaurant');
        $cookieEmail = $request->cookies->get('_email');
        $prospect = null;
        $prospectRepository = $this->get('app.repository.prospect');

        // Try to find prospect by hash string
        if (!empty($hash)) {
            $prospect = $prospectRepository->getProspect(['hash' => $hash]);
        }

        // Else try by cookie emailpaymentResult
        if ($prospect === null && !empty($cookieEmail)) {
            /** @var Prospect $prospect */
            $prospect = $prospectRepository->getProspect(['email' => $cookieEmail]);
        }

        // First time visit or cookies cleared
        if ($prospect === null) {
            $prospect = new Prospect();

            $prospect->setEmail($cookieEmail);
            $prospect->setRestaurantName($cookieRestaurant);
            $prospect->setEnabled(false);
        }

        $this->bindUtmParametersToProspect($prospect);

        return $prospect;
    }

    protected function bindUtmParametersToProspect(Prospect $prospect)
    {
        $session = $this->get('session');

        $prospect->setIp($session->get('client_ip'));
        $prospect->setUtmEmail($session->get('email'));

        if (!empty($session->get('utm_source'))) {
            $prospect->setUtmSource($session->get('utm_source'));
        }
        if (!empty($session->get('gclid'))) {
            $prospect->addSource('adwords');
        }
        if (!empty($session->get('fbclid'))) {
            $prospect->addSource('facebook');
        }

        $prospect->setUtmMedium($session->get('utm_medium'));
        $prospect->setUtmCampaign($session->get('utm_campaign'));
        $prospect->setUtmTerm($session->get('utm_term'));
        $prospect->setUtmContent($session->get('utm_content'));

        if (empty($prospect->getUtmSource())) {
            $prospect->addSource('inscription');
        } else {
            $prospect->addSource($prospect->getUtmSource());
        }
    }

    /**
     * @return ShopUser|AdminUser|null
     */
    protected function getUser()
    {
        return parent::getUser();
    }

    /**
     * @return Restaurant|null
     */
    protected function getRestaurant(): ?Restaurant
    {
        $user = $this->getUser();

        if ($user === null) {
            return null;
        }

        if ($user instanceof AdminUser) {
            return $this->get('app.repository.restaurant')->find(4);
        }

        $restaurants = $user->getCustomer()->getRestaurants();

        if ($restaurants->isEmpty()) {
            return null;
        }

        return $this->get('session')->get('restaurant', $restaurants->first());
    }

    protected function getManagedRestaurant(): ?Restaurant
    {
        $restaurant = $this->getRestaurant();

        if ($restaurant === null) {
            return null;
        }

        return $this->get('app.repository.restaurant')->find($restaurant->getId());
    }

    protected function getManagedProspect(): ?Prospect
    {
        $user = $this->getUser();

        if ($user === null) {
            return null;
        }

        if ($user instanceof AdminUser) {
            return $this->get('app.repository.prospect')->findOneBy(['email' => 'juliefarhi@hotmail.com']);
        } else {
            $prospect = $user->getCustomer()->getProspect();
        }

        return $this->get('app.repository.prospect')->refresh($prospect);
    }

    protected function getContextRestaurantId()
    {
        $restaurant = $this->getRestaurant();

        if ($restaurant === null) {
            return 4;
        }

        return $restaurant->getId();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function switchRestaurantAction(Request $request): Response
    {
        $restaurantId = $request->attributes->get('restaurantId');
        $restaurant = $this->get('app.repository.restaurant')->find($restaurantId);

        if ($restaurant !== null) {
            $this->get('session')->set('restaurant', $restaurant);
        }

        return $this->redirectToRoute('app_front_index');
    }

    /**
     * @return ShopUtils
     */
    protected function getShopUtils()
    {
        $shopUtils = $this->get('app.service.shop_utils');

        $shopUtils->setRestaurant($this->getContextRestaurantId());

        return $shopUtils;
    }

    /**
     * 404 page, renders a dummy response but it has a url to point to it ;)
     *
     * @return Response
     */
    public function error404Action()
    {
        $response = $this->responseAction([
            'topCategories' => $this->getShopUtils()->getTopCategories(),
            'topProducts' => $this->getShopUtils()->getTopProducts()['products'],
            'promos' => $this->getShopUtils()->getPromos()['products'],
        ]);

        $response->setStatusCode(404);

        return $response;
    }

    /**
     * 500 page, renders a dummy response but it has a url to point to it ;)
     *
     * @param Request $request
     * @return Response
     */
    public function error500Action(Request $request)
    {
        $response = $this->responseAction([
            'topCategories' => $this->getShopUtils()->getTopCategories(),
            'topProducts' => $this->getShopUtils()->getTopProducts()['products'],
            'promos' => $this->getShopUtils()->getPromos()['products'],
            'exception' => $request->get('exception', null)
        ]);

        $response->setStatusCode(500);

        return $response;
    }

    protected function prepareUtmSession()
    {
        $request = $this->get('request_stack')->getMasterRequest();

        if ($request !== null) {
            $session = $this->get('session');

            $session->set('client_ip', $request->getClientIp());
            $session->set('utm_source', $request->get('utm_source', $session->get('utm_source')));
            $session->set('utm_medium', $request->get('utm_medium', $session->get('utm_medium')));
            $session->set('utm_campaign', $request->get('utm_campaign', $session->get('utm_campaign')));
            $session->set('utm_content', $request->get('utm_content', $session->get('utm_content')));
            $session->set('utm_term', $request->get('utm_term', $session->get('utm_term')));
            $session->set('email', $request->get('email', $session->get('email')));
            $session->set('gclid', $request->get('gclid', $session->get('gclid')));
            $session->set('fbclid', $request->get('fbclid', $session->get('fbclid')));

            if (!empty($session->get('utm_source'))) {
                $session->set('computed_source', $session->get('utm_source'));
            } else {
                if (!empty($session->get('gclid'))) {
                    $session->set('computed_source', 'adwords');
                } elseif (!empty($session->get('fbclid'))) {
                    $session->set('computed_source', 'facebook');
                }
            }
        }
    }

    /**
     * @param array $staticContext
     * @return Response
     */
    public function responseAction($staticContext = array())
    {
        $this->prepareUtmSession();

        /** @var Request $request */
        $request = $this->get('request_stack')->getMasterRequest();

        try {

            // Détermine le numéro de téléphone à afficher selon l'adresse ip de l'utilisateur
            // Récupération de l'adresse ip
            //$ip = $request->getClientIp();

            $geoipDatatabasePath = $this->getParameter('kernel.project_dir') . '/var/Database_GeoIp2/GeoLite2-City.mmdb';

            // chemin de la base de données de GeoIP2 (Var/Database_GeoIp2)
            $reader = new Reader($geoipDatatabasePath);
            $record = $reader->city('86.195.33.31');
            $codePostal = $record->postal->code;

            $pays = $record->country->name;

            // Code postaux dans la zone des numéros en 05
            $codesSud = ["17000", "16000", "87000", "23000", "33000", "24000", "19000", "40000", "47000", "46000",
                "64000", "32000", "82000", "65000", "31000", "81000", "12000", "09000"];

            // Essai ici pour le bandeau uniquement en idf
            $codesIdf = ['92', '78', '75'];
            $codePostalFirstNumber = substr($codePostal, 0, 2);


            // Déjà, vérifier que la personne est en France
            if ($pays == "France") {
                // Si le département fait parti des codesSud, alors la personne est dans le Sud-Ouest
                if (in_array($codePostal, $codesSud)) {
                    // La personne se trouve dans le Sud-Ouest
                    $phoneNumber = "05 54 54 27 70";
                    $isAvalaible = false;
                } elseif(in_array($codePostalFirstNumber, $codesIdf) && strlen($codePostal) === 5) {
                    // La personne ne se trouve pas dans le Sud-Ouest
                    // Mettre le numéro de Paris
                    $phoneNumber = "01 87 66 11 90";
                    // Variable qui sera ajoutée pour savoir s'il faut ajouter le bandeau exceptionnel
                    // Seulement true ici
                    $isAvalaible = true;
                } else {
                    $isAvalaible = false;
                    $phoneNumber = "01 87 66 11 90";
                }
            } else {
                // La personne n'habite pas en France
                // Mettre par défaut le numéro de Paris
                $phoneNumber = "01 87 66 11 90";
                $isAvalaible = false;
            }

        } catch (\Exception $e) {
            // Gère si l'ip récupéré dans $ip = $request->getClientIp(); est null
            // Gère aussi si le chemin à la database de geoIp2 n'est pas trouvée
            // Retourne toujours le numéro de Paris en cas d'exception levée
            $phoneNumber = "01 87 66 11 90";
            $isAvalaible = false;
        }


        $isMobile = $this->mobileDetector->isMobile();
        $isTablet = $this->mobileDetector->isTablet();

        if (!isset($staticContext['taxons'])) {
            $staticContext['taxons'] = $this->getShopUtils()->getShopTaxons($this->getContextRestaurantId());
        }

        if (!isset($staticContext['request-password-reset-form'])) {
            $staticContext['request-password-reset-form'] = $this->liformize($this->getRequestPasswordResetForm());
        }

        $isAppDev = $this->getParameter('kernel.environment') === 'dev';

        try {
            /** @var ShopUser|AdminUser $user */
            $user = $this->getUser();

            if ($user === null) {
//                $prospect = $this->getContextProspect($request);

//                $registerForm = array_merge($this->liformize($this->getRegisterForm($prospect)), ['step' => $prospect->getRegistrationStep()]);

//                $staticContext['register-form'] = $registerForm;

                return $this->render('@App/MiddleOffice/Default/index.html.twig', [
                    'authToken' => false,
                    'schema' => null,
                    'initialStates' => [
                        'debugState' => $this->getDebugState($request),
                        'shoppingCarts' => [],
                        'frontState' => [
                            'registrationToComplete' => $this->get('session')->get('registrationToComplete'),
                            'loginError' => '',
                            'user' => [
                                'username' => '',
                                'id' => ''
                            ],
                            'isMobile' => $isMobile,
                            'isTablet' => $isTablet,
                            'isAndroidChrome' => $this->mobileDetector->isAndroidOS() && $this->mobileDetector->isChrome(),
                            'isAppDev' => $isAppDev,
                            'phoneNumber' => $phoneNumber,
                            'isAvalaible' => $isAvalaible
                        ]
                    ],
                    'initialValues' => $staticContext,
                ]);
            }

            $shoppingCartManager = $this->get('app.shopping_cart_manager');
            $productFinder = $this->get('app.service.product_finder');

            $restaurant = $this->getRestaurant();

            if ($user instanceof AdminUser) {
                $userName = $user->getFirstName();
                $restaurants = [
                    [
                        'key' => $restaurant->getId(),
                        'value' => $restaurant->getId(),
                        'text' => $restaurant->getName(),
                        'selected' => true
                    ]
                ];
            } else {
                if ($user->getCustomer()->getProspect()->getType() === Prospect::TYPE_INDIVIDUAL) {
                    return $this->redirect('https://app.foodomarket.com');
                }

                $userName = $user->getCustomer()->getFirstName();
                $restaurants = $user->getCustomer()->getRestaurants()->map(function (Restaurant $userRestaurant) use ($restaurant) {
                    return [
                        'key' => $userRestaurant->getId(),
                        'value' => $userRestaurant->getId(),
                        'text' => $userRestaurant->getName(),
                        'selected' => $restaurant !== null && $restaurant->getId() == $userRestaurant->getId()
                    ];
                })->toArray();
            }

            $prospect = $this->getManagedProspect();

            $restaurantSuppliers = $productFinder->getRestaurantSuppliers($this->getRestaurant());

            foreach ($restaurantSuppliers as &$restaurantSupplier) {
                if ($restaurantSupplier['supplierId'] == 0) {
                    continue ;
                }

                /** @var Supplier $supplier */
                $supplier = $this->get('app.repository.supplier')->find($restaurantSupplier['supplierId']);
                $creditCardOnline = false;
                $restaurantSupplier['order_enabled'] = $this->get('app.repository.supplier_account_log')->canOrderWith($prospect, $supplier, $creditCardOnline);
                $restaurantSupplier['payment_method'] = $creditCardOnline ? 'credit_card_online' : 'default';
            }

            return $this->render('@App/MiddleOffice/Default/index.html.twig', [
                'authToken' => true,
                'schema' => null,
                'initialStates' => [
                    'debugState' => $this->getDebugState($request),
                    'shoppingCarts' => $shoppingCartManager->getShoppingCarts($restaurant),
                    'frontState' => [
                        'registrationToComplete' => $this->get('session')->get('registrationToComplete'),
                        'loginError' => '',
                        'user' => [
                            'username' => $userName ? $userName : '',
                            'id' => $user !== null ? $user->getId() : '',
                            'restaurant' => $restaurant !== null ? $restaurant->getName() : '',
                            'email' => $user !== null ? $user->getEmail() : '',
                            'arbitration' => $user !== null ? $prospect->isCanUseArbitration() : false,
                        ],
                        'isMobile' => $isMobile,
                        'isTablet' => $isTablet,
                        'restaurants' => $restaurants,
                        'restaurant' => $restaurant->getId(),
                        'suppliers' => $restaurantSuppliers,
                        'isAppDev' => $isAppDev,
                        'phoneNumber' => $phoneNumber
                    ]
                ],
                'initialValues' => $staticContext,
            ]);
        } catch (\Exception $exception) {
            return $this->render('@App/MiddleOffice/Default/index.html.twig', [
                'authToken' => false,
                'schema' => null,
                'initialStates' => [
                    'frontState' => [
                        'loginError' => $exception->getMessage(),
                        'isMobile' => $isMobile,
                        'isTablet' => $isTablet,
                        'isAppDev' => $isAppDev,
                        'phoneNumber' => $phoneNumber
                    ],
                    'shoppingCarts' => [],
                    'debugState' => $this->getDebugState($request)
                ],
                'initialValues' => $staticContext,
                'user' => null
            ]);
        }
    }

    /**
     * @param Prospect $prospect
     * @return FormInterface
     */
    protected function getRegisterForm(Prospect $prospect)
    {
        return $this->createForm(CreateAccountType::class, $prospect, ['validation_groups' => "step{$prospect->getRegistrationStep()}"]);
    }

    /**
     * @return FormInterface
     */
    protected function getPasswordResetForm()
    {
        return $this->createForm(PasswordResetType::class);
    }

    /**
     * @return FormInterface
     */
    protected function getRequestPasswordResetForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('email', EmailType::class, ['mapped' => false, 'label' => 'Email', 'liform' => ['widget' => 'email'], 'constraints' => [new NotBlank()]])
            ->getForm()
            ;
    }

    private function getDebugState(Request $request)
    {
        $debugParams = $request->get('debug', []);
        $state = [];

        foreach ($debugParams as $debugParam) {
            $state[$debugParam] = true;
        }

        return $state;
    }

    protected function getParameter(string $name)
    {
        if ($this->container->has('parameter_bag')) {
            return parent::getParameter($name);
        }

        return $this->container->getParameter($name);
    }

    protected function json($data, int $status = 200, array $headers = [], array $context = []) : JsonResponse
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $data = $serializer->encode($data, 'json');

        $response = new JsonResponse(null, $status, $headers);
        $response->setContent($data);

        return $response;
    }
}
