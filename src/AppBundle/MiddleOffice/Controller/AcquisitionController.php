<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Email;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\WebHook;
use AppBundle\Form\Type\LiformVichFileType;
use AppBundle\Form\Type\RegisterStepOneType;
use AppBundle\Form\Type\RegisterStepTwoType;
use AppBundle\Helper\CurlHelper;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Services\Mailer\EmailBuilder;
use DateTime;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AcquisitionController extends AbstractController
{
    use ShopContext;

    public function finalizeRegistrationAction(Request $request)
    {
        /** @var Prospect|null $prospect */
        $prospect = $this->get('app.repository.prospect')->findOneBy(['hash' => $request->attributes->get('token')]);

        if (!$prospect) {
            $this->get('session')->remove('hash');
            return $this->redirectToRoute('app_front_404_page');
        }

        // Il faut set ici la session, mettre le hash suivant dans la session
        // Pour pouvoir réafficher les données renseignées quand retour au premier formulaire
        $this->get('session')->set('hash', $request->attributes->get('token'));

        // Appel du formulaire
        $form = $this->createForm(RegisterStepTwoType::class, $prospect);

        return $this->responseAction([
            'acquisition-reg-final-form' => $this->liformize($form),
        ]);
    }

    // Ce formulaire ne sert plus pour l'instant
    private function getFinalRegistrationForm(Prospect $prospect, int $step = 1)
    {
        return $this->createFormBuilder($prospect, ['csrf_protection' => false, 'data_class' => Prospect::class, 'validation_groups' => "step$step"])
            // Step 1
            ->add('firstname', TextType::class, [
                'mapped' => false,
                'label' => 'Prénom',
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            ->add('lastname', TextType::class, [
                'mapped' => false,
                'label' => 'Nom',
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'label' => 'Créez votre mot de passe',
                'liform' => ['widget' => 'password'],
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            ->add('password_again', PasswordType::class, [
                'mapped' => false,
                'label' => 'Confirmez votre mot de passe',
                'liform' => ['widget' => 'repeatedpassword'],
                'constraints' => [
                    new NotBlank(['groups' => ['step1']])
                ]
            ])
            // Step 2
            ->add('address', TextType::class, ['label' => "Adresse", 'constraints' => [new NotBlank(['groups' => 'step2'])], 'liform' => ['widget' => 'places-autocomplete']])
            ->add('zipCode', TextType::class, ['label' => 'Code postal', 'constraints' => [new NotBlank(['groups' => 'step2'])]])
            ->add('city', TextType::class, ['label' => 'Ville', 'constraints' => [new NotBlank(['groups' => 'step2'])]])
//            // Step 2
//            ->add('categories', ChoiceType::class, [
//                'label' => "Catégories de produits",
//                'choices' => [
//                    Supplier::getDisplayCategory(Supplier::CAT_FRUITS_LEGUMES) => Supplier::CAT_FRUITS_LEGUMES,
//                    Supplier::getDisplayCategory(Supplier::CAT_CREMERIE) => Supplier::CAT_CREMERIE,
//                    Supplier::getDisplayCategory(Supplier::CAT_BOUCHERIE) => Supplier::CAT_BOUCHERIE,
//                    Supplier::getDisplayCategory(Supplier::CAT_MAREE) => Supplier::CAT_MAREE,
//                    Supplier::getDisplayCategory(Supplier::CAT_EPICERIE) => Supplier::CAT_EPICERIE,
//                ],
//                'multiple' => true,
//                'expanded' => true,
//                'liform' => ['widget' => 'category-multiple-choice'],
//                'constraints' => [
//                    new NotBlank(['groups' => ['step2']])
//                ]
//            ])
//            // Step 3
//            ->add('currentSuppliers', ChoiceType::class, [
//                'mapped' => false,
//                'label' => 'Vos fournisseurs actuels',
//                'multiple' => true,
//                'expanded' => true,
//                'choices' => [
//                    "Halles Prestige" => "Halles Prestige",
//                    "Alazard" => "Alazard",
//                    "Bermudes" => "Bermudes",
//                    "Chassineau" => "Chassineau",
//                    "Daumesnil" => "Daumesnil",
//                    "Distri-Resto" => "Distri-Resto",
//                    "EpiSaveurs" => "EpiSaveurs",
//                    "Demarne" => "Demarne",
//                    "Domafrais" => "Domafrais",
//                    "Halles Trottemant" => "Halles Trottemant",
//                    "Jacob" => "Jacob",
//                    "Halles Mandar" => "Halles Mandar",
//                    "Metro" => "Metro",
//                    "Terre Azur" => "Terre Azur",
//                    "Transgourmet" => "Transgourmet",
//                    "Verger Etoile" => "Verger Etoile",
//                    "Reynaud" => "Reynaud",
//                    "Primeurs Passion" => "Primeurs Passion",
//                    "Eurodis" => "Eurodis",
//                    "Activia" => "Activia",
//                    "Fory Viandes" => "Fory Viandes",
//                    "Martin" => "Martin",
//                    "Huguenin" => "Huguenin",
//                    "Prodal" => "Prodal",
//                    "J'Oceane" => "J'Oceane",
//                    "L'Ecrevisse" => "L'Ecrevisse",
//                    "Buisson" => "Buisson",
//                    "Delon" => "Delon",
//                    "ABC Peyraud" => "ABC Peyraud",
//                    "Ugalait" => "Ugalait",
//                    "Placet" => "Placet",
//                    "Daguerre" => "Daguerre",
//                    "Autre",
//                    "Autre crémier",
//                    "Autre boucher",
//                    "Autre épicier",
//                    "Autre primeur",
//                    "Autre poissonnier",
//                ],
//                'liform' => ['widget' => 'supplier-multiple-choice'],
//                'constraints' => [
//                    new NotBlank(['groups' => ['step3']])
//                ]
//            ])
            // Step 3
            ->add('deliveryHours', TextareaType::class, [
                'label' => 'Horaires de livraison',
                'liform' => ['widget' => 'timeslot'],
                'required' => true,
                'empty_data' => '08:00-10:00'
            ])
            ->add('kbisFile', LiformVichFileType::class, [
                'label' => 'Extrait de KBis',
                'constraints' => [
//                    new NotBlank(['groups' => ['step4']])
                    new NotBlank(['groups' => ['step3']])
                ]
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                /** @var Prospect|null $data */
                $data = $event->getData();

                if ($data && $data->getKbis()) {
                    $event->getForm()->remove('kbisFile');
                }
            })
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                /** @var Prospect|null $data */
                $data = $event->getData();
                $form = $event->getForm();

                if ($data) {
                    $form->get('firstname')->setData($data->getFirstName());
                    $form->get('lastname')->setData($data->getContactName());
//                    $form->get('currentSuppliers')->setData(explode(',', $data->getCurrentSupplier()));

                    $event->setData($data);
                }
            })
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                /** @var Prospect $data */
                $data = $event->getData();
                $form = $event->getForm();

                $data->setFirstName($form->get('firstname')->getData());
                $data->setContactName($form->get('lastname')->getData());
                $data->setPlainPassword($form->get('password')->getData());
//                $data->setCurrentSupplier(implode(',', $form->get('currentSuppliers')->getData()));
            })
            ->getForm();
    }

    public function registerHookAction(Request $request)
    {
        // Saves the data in webhook table. If something goes wrong we will be able to process the data from webhook table
        $content = $request->getContent();

        $webhook = new WebHook();
        $webhook->setHookName('acquisition-register');
        $webhook->setHookData($content);

        $this->get('app.repository.web_hook')->add($webhook);

        // Get data as json for the prospect registering
        $data = json_decode($content, true);

        $prospectRepository = $this->get('app.repository.prospect');
        /** @var Prospect|null $prospect */
        $prospect = $prospectRepository->findOneBy(['email' => $data['MAIL']]);
        $tokenGenerator = $this->get('sylius.shop_user.token_generator.email_verification');

        if ($prospect) {
            if (!$prospect->getCustomers()->isEmpty()) {
                return $this->json(['ok' => false, 'message' => 'Already client']);
            }
        } else {
            $prospect = new Prospect();
        }

        preg_match('/(?<zip>\d+)/', $data['RESTAURANT_ZIP'], $match);

        $prospect->setEmail($data['MAIL']);
        $prospect->setFirstName($data['FIRST_NAME']);
        $prospect->setPhone($data['PHONE']);
        $prospect->setMobile($data['PHONE']);
        $prospect->setJob($data['POSITION']);
        $prospect->setRestaurantName($data['RESTAURANT_NAME']);
        $prospect->setAddress('');
        $prospect->setCity('');
        $prospect->setZipCode($match['zip'] ?? '');
        $prospect->setHash($tokenGenerator->generate());
        $prospect->setEnabled(false);

        // Ajoute welcome as source
        $prospect->addSource('welcome');

        $prospectRepository->add($prospect);
        $source = $prospect->getSource();

        // Appel au service du ProspectSource et création de l'objet ProspectSource
        $prospectSourceService = $this->get('app.service.prospect.source');
        $prospectSourceService->createProspectSource($prospect, "Welcome", 'facebook');

        $this->sendVerificationEmail($prospect, 'facebook');

        // hubspot : log transaction
//        $this->get('app.services.hubspot')->createContactAndPartialSubscriptionHoppy($prospect);

        // Post data to zapier url
        $curlReponse = CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o4wyqkg/', [], [
            'RESTAURANT_NAME' => $prospect->getRestaurantName(),
            'FIRST_NAME' => $prospect->getFirstName(),
            'PHONE' => $prospect->getPhone(),
            'MAIL' => $prospect->getEmail(),
            'DATE' => (new DateTime())->format('c'),
            'ADDRESS' => $prospect->getZipCode()
        ]);

        return $this->json(['ok' => true]);
    }

    // Fonction du nouveau formulaire d'inscription
    private function sendVerificationEmail(Prospect $prospect, string $source = '')
    {
        $this->get('app.sendinblue.manager')->addEmailToQueue(
            $this->get('app.factory.email')
                ->createBuilder(Email::MANAGER_SENDINBLUE)
                ->setTemplateId(17)
                ->addTo($prospect->getEmail(), $prospect->getFirstName())
                ->addVariable('FIRSTNAME', $prospect->getFirstName())
                ->addVariable('URL', $this->generateUrl('app_front_finalize_registration_page', ['token' => $prospect->getHash(), 'utm_source' => $source], UrlGeneratorInterface::ABSOLUTE_URL))
                ->build()
        );
    }

    public function individualRegistrationAction(Request $request)
    {
        $data = [
            'full_name' => $request->request->get('full_name'),
            'email' => $request->request->get('email'),
            'mobile' => $request->request->get('mobile'),
            'password' => $request->request->get('password'),
            'full_address' => $request->request->get('full_address'),
            'street_number' => $request->request->get('number'),
            'street' => $request->request->get('street'),
            'zip' => $request->request->get('zip'),
            'city' => $request->request->get('city'),
            'country' => $request->request->get('country'),
            'floor' => $request->request->get('floor'),
            'delivery_instructions' => $request->request->get('delivery_instructions'),
            'amplitude_device_id' => $request->request->get('amplitude_device_id'),
            'callback' => $request->request->get('callback', '0')
        ];

        /** @var ProspectRepository $prospectRepository */
        $prospectRepository = $this->get('app.repository.prospect');

        $prospect = $prospectRepository->findOneBy(['email' => $data['email']]);

        if ($prospect !== null) {
            if (!$prospect->getCustomers()->isEmpty()) {
                // Error
                return $this->render('@App/MiddleOffice/Acquisition/sorry.html.twig', ['data' => 'Cet email existe déjà dans notre base de données. Veuillez réessayer avec un autre email ou vous connecter directement sur <a href="https://app.foodomarket.com">FoodoMarket</a>']);
            }
        } else {
            $prospect = new Prospect();
        }

        if (empty($data['zip'])) {
            if (preg_match('/(?<zip>\d{5})/', $data['full_address'], $match)) {
                $data['zip'] = $match['zip'];
            } else {
                return $this->render('@App/MiddleOffice/Acquisition/sorry.html.twig', ['data' => "Nous n'avons pas pu déterminer le code postal. Veuillez modifier votre adresse."]);
            }
        }

        $prospect->setEnabled(true);
        $prospect->setFirstName($data['full_name']);
        $prospect->setContactName('');
        $prospect->setMobile($data['mobile']);
        $prospect->setPhone($data['mobile']);
        $prospect->setPlainPassword($data['password']);

        if (!empty($data['street_number']) && !empty($data['street'])) {
            $prospect->setAddress("{$data['street_number']} {$data['street']}");
        } else {
            $prospect->setAddress($data['full_address']);
        }

        $prospect->setZipCode(trim($data['zip']));
        $prospect->setCity($data['city'] ?? '');
        $prospect->setDeliveryHours('');
        $prospect->setEmail($data['email']);
        $prospect->setType(Prospect::TYPE_INDIVIDUAL);
        $prospect->setCivility('MONSIEUR');

        $address = implode(', ', array_filter([$prospect->getAddress(), $data['floor'], $data['delivery_instructions']], function($value) {
            return !empty(trim($value));
        }));

        $prospect->setAddress($address);

        $prospectRepository->add($prospect);

        // Appel au service du ProspectSource et créer l'objet ProspectSource
        $prospectSourceService = $this->get('app.service.prospect.source');
        $prospectSourceService->createProspectSource($prospect, "Inscription particulier");

        $shopUser = $this->get('app.service.shop_user')->createRestaurantAndShopUser($prospect);

        if ($shopUser === null) {
            return $this->render('@App/MiddleOffice/Acquisition/sorry.html.twig', ['data' => 'Une erreur est survenue durant la création du compte shop']);
        }

        // Post data to zapier url
        CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o56phvf/', [], $data);

        /**
         * Notify hello@foodomarket.com
         * Edit: Not anymore cause of spam
         * @var EmailBuilder $emailBuilder
         */
//        $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);
//
//        $boProspectUrl = $this->generateUrl('app_admin_admin_prospect_show', ['id' => $prospect->getId()], Router::ABSOLUTE_URL);
//
//        $emailBuilder
//            ->addTo('hello@foodomarket.com')
//            ->addFrom('olivier@foodomarket.com')
//            ->setSubject("Inscription complète du particulier {$prospect->getFirstName()}")
//            ->setHtmlBody("<p>Bonjour<br/><br/>Voila une nouvelle inscription sur foodomarket : <a href='{$boProspectUrl}' target='_blank'>{$prospect->getRestaurantName()} à {$prospect->getCity()} ({$prospect->getZipCode()})</a><br/>{$prospect->getAddress()}</p>");
//
//        $this->get('app.sendinblue.manager')->addEmailToQueue($emailBuilder->build());

        if ($prospect->getSuppliers()->isEmpty()) {
            return $this->render('@App/MiddleOffice/Acquisition/sorry.html.twig', ['data' => "Nous sommes désolés, nous ne sommes pas en mesure de servir le {$prospect->getZipCode()} à l'heure actuelle. Nous vous recontacterons par email dès que la situation évolue. Merci de votre compréhension"]);
        }

        $forwardAppUrl = $this->getParameter('app_url');

        return $this->redirect("$forwardAppUrl/?amplitude_device_id={$data['amplitude_device_id']}");
    }

    // L'inscription finale qui devient la deuxième step après la première (registerFormAction)
    public function registerFormAction(Request $request)
    {
        $this->prepareUtmSession();

        $prospectRepository = $this->get('app.repository.prospect');
        /** @var Prospect $prospect */

        // Ici il faut vérifier si la session est remplie des données ou déjà vide
        // Dans le cas où on clique sur le bouton retour du second formulaire
        if($this->get('session')->get('hash')){

            // Récupération du prospect du form 1
            $prospect = $this->get('app.repository.prospect')->findOneBy(['hash' => $this->get('session')->get('hash')]);

            // Récupération du formType
            $form = $this->createForm(RegisterStepOneType::class, $prospect);
        }else{
            $prospect = null;
            $form = $this->createForm(RegisterStepOneType::class, $prospect);
        }

        $form->handleRequest($request);

        // Validation du formulaire
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var Prospect $data */
                $data = $form->getData();
                /** @var Prospect|null $prospect */
                $prospect = $prospectRepository->findOneBy(['email' => $data->getEmail()]);
                $tokenGenerator = $this->get('sylius.shop_user.token_generator.email_verification');

                // Vérification si le prospect existe déjà
                if ($prospect) {
                    if (!$prospect->getCustomers()->isEmpty()) {
                        $form->get('email')->addError(new FormError("Un compte avec cet email existe déjà."));

                        return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
                    }
                } else {
                    $prospect = $data;
                }

                // Génération du token
                $prospect->setHash($tokenGenerator->generate());

                $this->bindUtmParametersToProspect($prospect);

                $prospectRepository->add($prospect);

                // Appel au service du ProspectSource et créer l'objet ProspectSource
                $prospectSourceService = $this->get('app.service.prospect.source');
                $prospectSourceService->createProspectSource($prospect, "Pré-inscription");

                // Post data to zapier url
                $curlReponse = CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o4wyqkg/', [], [
                    'FIRST_NAME' => $prospect->getFirstName(),
                    'MAIL' => $prospect->getEmail(),
                    'PHONE' => $prospect->getMobile()
                ]);
                return $this->json(['ok' => true, 'continue_url' => $this->generateUrl('app_front_finalize_registration_page', ['token' => $prospect->getHash()])]);
            } else {
                return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
            }
        }
        return $this->json(['form' => $this->liformize($form)]);
    }


    public function finalizeRegistrationFormAction(Request $request)
    {
        $this->get('session')->remove('hash');

        /** @var Prospect|null $prospect */
        $prospect = $this->get('app.repository.prospect')->findOneBy(['hash' => $request->attributes->get('token')]);

        if (!$prospect) {
            $this->get('session')->remove('hash');
        }

        // Il faut set ici la session, mettre le hash suivant dans la session
        $this->get('session')->set('hash', $request->attributes->get('token'));

        // Appel du formulaire
        $form = $this->createForm(RegisterStepTwoType::class, $prospect);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //The registration has been completed
                $this->get('session')->remove('registrationToComplete');

                $this->getDoctrine()->getManager()->flush();

                $shopUser = $this->get('app.service.shop_user')->createRestaurantAndShopUser($prospect);

                $response = $this->json(['ok' => true]);

                /** @var JWTManager $jwtManager */
                $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
                /** @var EventDispatcher $dispatcher */
                $dispatcher = $this->get('event_dispatcher');

                $jwt = $jwtManager->create($shopUser);
                $event = new AuthenticationSuccessEvent(array('token' => $jwt), $shopUser, $response);

                $dispatcher->dispatch(Events::AUTHENTICATION_SUCCESS, $event);
                $response->setData(array_merge(['ok' => true], $event->getData()));

                $cookieRegistered = new Cookie('_reg', true, (new DateTime())->modify('+29 day'), '/', null, false, false);
                $response->headers->setCookie($cookieRegistered);

                // Post data to zapier url
                $curlReponse = CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o4nk7u2/', [], [
                    'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                    'FIRST_NAME' => $prospect->getFirstName(),
                    'ADDRESS' => "{$prospect->getAddress()} {$prospect->getZipCode()} {$prospect->getCity()}",
                    'PHONE' => $prospect->getMobile(),
                    'MAIL' => $prospect->getEmail(),
                    'DATE' => (new DateTime())->format('c'),

                ]);

                /**
                 * Notify hello@foodomarket.com
                 */

                $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

                $boProspectUrl = $this->generateUrl('app_admin_admin_prospect_show', ['id' => $prospect->getId()], Router::ABSOLUTE_URL);

                $emailBuilder
                    ->addTo('hello@foodomarket.com')
                    ->addFrom('olivier@foodomarket.com')
                    ->setSubject("Inscription complète de {$prospect->getRestaurantName()}")
                    ->setHtmlBody("<p>Bonjour<br/><br/>Voila une nouvelle inscription sur foodomarket : <a href='{$boProspectUrl}' target='_blank'>{$prospect->getRestaurantName()} à {$prospect->getCity()} ({$prospect->getZipCode()})</a><br/>{$prospect->getAddress()}</p>");

                $this->get('app.sendinblue.manager')->addEmailToQueue($emailBuilder->build());

                // Removes the hash used as token, this page is now useless
                $prospect->setHash(null);
                $prospect->setEnabled(true);
                $this->get('session')->remove('hash');

                $this->getDoctrine()->getManager()->flush();

                /// Appel au service du ProspectSource et création de l'objet ProspectSource
                $prospectSourceService = $this->get('app.service.prospect.source');
                $prospectSourceService->createProspectSource($prospect, "Inscription finale");

                return $response;

            }else{
                return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
            }
        }

        return $this->json(['form' => $this->liformize($form)]);

    }

    // Ne sert plus
    public function registerFormActionOld(Request $request)
    {
        $this->prepareUtmSession();

        $prospectRepository = $this->get('app.repository.prospect');
        /** @var Prospect $prospect */
        $prospect = null;

        if ($email = $request->cookies->get('_email')) {
            $prospect = $prospectRepository->findOneBy(['email' => $email]);
        }

//        $form = $this->createForm(RegisterType::class);

        $form = $this->createFormBuilder(null, ['csrf_protection' => false])
            // Step 1
            ->add('firstname', TextType::class, ['label' => 'Votre prénom', 'constraints' => [new NotBlank()]])
            ->add('email', EmailType::class, ['label' => 'Votre email', 'constraints' => [new NotBlank()]])
            ->add('telephone', TextType::class, ['label' => 'Votre téléphone', 'constraints' => [new NotBlank()], 'attr' => ['pattern' => '^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$']])
            // Step 2
            ->add('restaurantName', TextType::class, ['label' => 'Le nom de votre restaurant', 'constraints' => [new NotBlank()]])
            ->add('restaurantAddress', TextType::class, ['label' => 'Son adresse', 'constraints' => [new NotBlank()], 'liform' => ['widget' => 'places-autocomplete']])
            ->add('zipCode', HiddenType::class)
            ->add('city', HiddenType::class)
            // Adds event listener
            ->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use($request, $prospect) {
                $event->setData([
                    'restaurantName' => $prospect ? $prospect->getRestaurantName() : $request->cookies->get('_restaurant', null),
                    'email' => $prospect ? $prospect->getEmail() : $request->cookies->get('_email', null),
                    'firstname' => $prospect ? $prospect->getFirstName() : null,
                    'telephone' => $prospect ? $prospect->getPhone() : null,
                    'restaurantAddress' => $prospect ? $prospect->getAddress() : null,
                    'zipCode' => $prospect ? $prospect->getZipCode() : null,
                    'city' => $prospect ? $prospect->getCity() : null
                ]);
            })
            // Build form
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                /** @var Prospect|null $prospect */
                $prospect = $prospectRepository->findOneBy(['email' => $data['email']]);
                $tokenGenerator = $this->get('sylius.shop_user.token_generator.email_verification');

                if ($prospect) {
                    if (!$prospect->getCustomers()->isEmpty()) {
                        $form->get('email')->addError(new FormError("Un compte avec cet email existe déjà."));

                        return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
                    }
                } else {
                    $prospect = new Prospect();
                }

                $prospect->setEmail($data['email']);
                $prospect->setFirstName($data['firstname']);
                $prospect->setPhone($data['telephone']);
                $prospect->setMobile($data['telephone']);
                $prospect->setRestaurantName($data['restaurantName']);
                $prospect->setAddress($data['restaurantAddress']);
                $prospect->setCity($data['city']);
                $prospect->setZipCode($data['zipCode']);
                $prospect->setHash($tokenGenerator->generate());
                $prospect->setEnabled(false);

                $this->bindUtmParametersToProspect($prospect);

                $prospectRepository->add($prospect);

//                $this->sendVerificationEmail($prospect);

                // Appel au service du ProspectSource et créer l'objet ProspectSource
                $prospectSourceService = $this->get('app.service.prospect.source');
                $prospectSourceService->createProspectSource($prospect, "Pré-inscription");

                // hubspot : log transaction
//                $this->get('app.services.hubspot')->createContactAndPartialSubscriptionHoppy($prospect);

                // Post data to zapier url
                $curlReponse = CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o4wyqkg/', [], [
                    'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                    'FIRST_NAME' => $prospect->getFirstName(),
                    'PHONE' => $prospect->getPhone(),
                    'MAIL' => $prospect->getEmail(),
                    'DATE' => (new DateTime())->format('c'),
                    'ADDRESS' => $prospect->getAddress()
                ]);

                return $this->json(['ok' => true, 'continue_url' => $this->generateUrl('app_front_finalize_registration_page', ['token' => $prospect->getHash()])]);
            } else {
                return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
            }
        }

        return $this->json(['form' => $this->liformize($form)]);
    }


    // Ne sert plus
    public function finalizeRegistrationFormActionOld(Request $request)
    {
        /** @var Prospect|null $prospect */
        $prospect = $this->get('app.repository.prospect')->findOneBy(['hash' => $request->attributes->get('token')]);
        $step = $request->query->get('step');
        $form = $this->getFinalRegistrationForm($prospect, $step);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                if ($step == 3) {
                    $shopUser = $this->get('app.service.shop_user')->createRestaurantAndShopUser($prospect);

                    $response = $this->json(['ok' => true]);

                    /** @var JWTManager $jwtManager */
                    $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
                    /** @var EventDispatcher $dispatcher */
                    $dispatcher = $this->get('event_dispatcher');

                    $jwt = $jwtManager->create($shopUser);
                    $event = new AuthenticationSuccessEvent(array('token' => $jwt), $shopUser, $response);

                    $dispatcher->dispatch($event, Events::AUTHENTICATION_SUCCESS);
                    $response->setData(array_merge(['ok' => true], $event->getData()));

                    $cookieRegistered = new Cookie('_reg', true, (new DateTime())->modify('+29 day'), '/', null, false, false);
                    $response->headers->setCookie($cookieRegistered);

                    // Creates a complete subscription in hubspot lifecycle
//                    $this->get('app.services.hubspot')->createContactAndCompleteSubscriptionHoppy($prospect);

                    // Post data to zapier url
                    $curlReponse = CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o4nk7u2/', [], [
                        'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                        'FIRST_NAME' => $prospect->getFirstName(),
                        'ADDRESS' => "{$prospect->getAddress()} {$prospect->getZipCode()} {$prospect->getCity()}",
                        'PHONE' => $prospect->getMobile(),
                        'MAIL' => $prospect->getEmail(),
                        'DATE' => (new DateTime())->format('c'),
                        'DELIVERY_HOURS' => $prospect->getDeliveryHours(),
                    ]);

                    /**
                     * Notify hello@foodomarket.com
                     */
                    $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

                    $boProspectUrl = $this->generateUrl('app_admin_admin_prospect_show', ['id' => $prospect->getId()], Router::ABSOLUTE_URL);

                    $emailBuilder
                        ->addTo('hello@foodomarket.com')
                        ->addFrom('olivier@foodomarket.com')
                        ->setSubject("Inscription complète de {$prospect->getRestaurantName()}")
                        ->setHtmlBody("<p>Bonjour<br/><br/>Voila une nouvelle inscription sur foodomarket : <a href='{$boProspectUrl}' target='_blank'>{$prospect->getRestaurantName()} à {$prospect->getCity()} ({$prospect->getZipCode()})</a><br/>{$prospect->getAddress()}</p>")
                    ;

                    $this->get('app.sendinblue.manager')->addEmailToQueue($emailBuilder->build());

                    // Removes the hash used as token, this page is now useless
                    $prospect->setHash(null);
                    $prospect->setEnabled(true);

                    $this->getDoctrine()->getManager()->flush();

                    /// Appel au service du ProspectSource et création de l'objet ProspectSource
                    $prospectSourceService = $this->get('app.service.prospect.source');
                    $prospectSourceService->createProspectSource($prospect, "Inscription finale");

                    return $response;
                }

                return $this->json(['ok' => true, 'form' => $this->liformize($form)]);
            } else {
                return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
            }
        }

        return $this->json(['form' => $this->liformize($form)]);
    }

    public function timeoutModalFormAction(Request $request)
    {
        $form = $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('firstname', TextType::class, ['label' => 'Votre prénom'])
            ->add('restaurant', TextType::class, ['label' => 'Le nom de votre restaurant'])
            ->add('email', EmailType::class, ['label' => 'Votre email'])
            ->add('telephone', TextType::class, ['label' => 'Votre numéro de téléphone'])
            ->getForm()
        ;

        $this->prepareUtmSession();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();

                // Creates (or update) the prospect
                $prospect = $this->get('app.repository.prospect')
                    ->addProspect($data['firstname'], $data['restaurant'], $data['email'], $data['telephone'], $this->get('session')->get('computed_source'))
                ;

                // Appel au service du ProspectSource et créer l'objet ProspectSource
                $prospectSourceService = $this->get('app.service.prospect.source');
                $prospectSourceService->createProspectSource($prospect, "Pop-up");

                // hubspot : record transaction
//                $this->get('app.services.hubspot')->createContactAndCallbackHoppy($prospect);

                // Post data to zapier url
                $curlReponse = CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/o4dixj8/', [], [
                    'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                    'FIRST_NAME' => $prospect->getFirstName(),
                    'PHONE' => $prospect->getPhone(),
                    'MAIL' => $prospect->getEmail(),
                    'DATE' => (new DateTime())->format('c')
                ]);

                // Sets the cookies `_restaurant` and `_email` to expire in 29 days
                $response = $this->json(['ok' => true]);
                $expiresAt = (new DateTime())->modify('+29 day');
                $cookieRestaurant = new Cookie('_restaurant', $prospect->getRestaurantName(), $expiresAt, '/', null, false, false);
                $cookieEmail = new Cookie('_email', $prospect->getEmail(), $expiresAt, '/', null, false, false);

                $response->headers->setCookie($cookieRestaurant);
                $response->headers->setCookie($cookieEmail);

                return $response;
            } else {
                return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
            }
        }

        return $this->json(['form' => $this->liformize($form)]);
    }
}
