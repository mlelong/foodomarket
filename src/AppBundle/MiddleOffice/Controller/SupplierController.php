<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Supplier;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Services\RedisCacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SupplierController extends AbstractController
{
    use ShopContext;

    public function indexAction(Request $request)
    {
        $restaurant = $this->getRestaurant();

        // TODO: Get the list of partner suppliers according to their delivery ability for the restaurant address !
        $supplierRepository = $this->get('app.repository.supplier');
        $partnerSuppliers = $supplierRepository->getPartners();

        $filters = ['categories' => [
            'key' => 'categories',
            'name' => 'Catégorie de produit',
            'values' => []
        ]];

        $suppliers = [];

        $liipImagineCacheManager = $this->get('liip_imagine.cache.manager');

        /** @var Supplier $partnerSupplier */
        foreach ($partnerSuppliers as $partnerSupplier) {
            $nbProducts = $supplierRepository->countActiveProducts($partnerSupplier);

            if ($nbProducts == 0) {
                continue ;
            }

            foreach ($partnerSupplier->getCategories() as $category) {
                $filters['categories']['values'][] = [
                    'id' => $category,
                    'name' => Supplier::getDisplayCategory($category)
                ];
            }

            $suppliers[] = [
                'id' => $partnerSupplier->getId(),
                'name' => $partnerSupplier->getFacadeSupplier()->getName(),
                'logo' => $liipImagineCacheManager->getBrowserPath(!empty($partnerSupplier->getLogoFileName()) ? $partnerSupplier->getLogoFileName() : 'shop-default-product.jpg', 'front_big'),
                'products' => $nbProducts,
                'filters' => ['categories' => $partnerSupplier->getCategories()],
                'categories' => array_reduce($partnerSupplier->getCategories(), function(string $carry, string $category) {
                    $displayCategory = Supplier::getDisplayCategory($category);

                    return empty($carry) ? $displayCategory : "$carry, $displayCategory";
                }, '')
            ];
        }

        foreach ($filters as $filterKey => &$filter) {
            $filter['values'] = array_values(array_unique($filter['values'], SORT_REGULAR));
        }

        return new JsonResponse(['ok' => true, 'suppliers' => $suppliers, 'filters' => array_values($filters)]);
    }

    public function catalogAction(Request $request)
    {
        $supplierId = $request->get('id');
        $productFilterService = $this->get('app.service.product_filter');
        $cacheManager = $this->get('app.redis.cache.manager');
        $supplierRepository = $this->get('app.repository.supplier');

        $ret = $cacheManager->get(
            str_replace('{supplierId}', $supplierId, RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCTS),
            null,
            function() use($supplierId, $productFilterService, $supplierRepository) {
                $products = $supplierRepository->getActiveProducts($supplierId);

                return $productFilterService->getProductsAndFilters($products, null, [$supplierId]);
            }
        );

        return new JsonResponse([
            'ok' => true,
            'supplierName' => $this->get('app.repository.supplier')->find($supplierId)->getFacadeSupplier()->getName(),
            'products' => array_values($ret['products']),
            'filters' => array_values($ret['filters'])
        ]);
    }
}