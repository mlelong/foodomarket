<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Email;
use AppBundle\Entity\Litigation;
use AppBundle\Entity\LitigationComment;
use AppBundle\Entity\LitigationCommentPicture;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Form\Type\LitigationCommentType;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class LitigationController extends AbstractController
{
    use ShopContext;

    public function indexAction()
    {
        return $this->responseAction([
            'litigations' => $this->get('app.service.litigation')->getLitigationsByRestaurant($this->getRestaurant())
        ]);
    }

    public function showAction(int $id)
    {
        $litigationService = $this->get('app.service.litigation');

        return $this->responseAction([
            'litigation' => $litigationService->getLitigation($id),
            'litigation-comment-form' => $this->liformize($litigationService->getLitigationCommentForm())
        ]);
    }

    public function newAction()
    {
        return $this->responseAction([
            'litigation-form' => $this->liformize($this->getLitigationForm())
        ]);
    }

    public function getLitigationFormAction()
    {
        return $this->json($this->liformize($this->getLitigationForm()));
    }

    public function getLitigationAction(int $id)
    {
        $litigationService = $this->get('app.service.litigation');

        return $this->json([
            'litigation' => $litigationService->getLitigation($id),
            'litigation-comment-form' => $this->liformize($litigationService->getLitigationCommentForm())
        ]);
    }

    public function getLitigationCommentsAction(int $id, int $offset, int $limit)
    {
        return $this->json(['comments' => $this->get('app.service.litigation')->getLitigationComments($id, $offset, $limit)]);
    }

    public function addAction(Request $request)
    {
        $form = $this->getLitigationForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Litigation $litigation */
            $litigation = $form->getData();
            $prospect = $this->getManagedProspect();

            // Manually adds the prospect as it is not fillable by user
            $litigation->setProspect($prospect);

            $createdBy = Prospect::class . ":{$prospect->getId()}";

            foreach ($litigation->getComments() as $comment) {
                foreach ($comment->getPictures() as $picture) {
                    if ($picture->getPictureFile() === null) {
                        $comment->getPictures()->removeElement($picture);
                    }
                }

                $comment->setCreatedBy($createdBy);
                $comment->setUpdatedBy($createdBy);
            }

            $litigation->setHash(uniqid("litigation-{$prospect->getId()}", true));

            $this->get('app.repository.litigation')->add($litigation);

            $address = "{$litigation->getRestaurant()->getName()}<br>{$litigation->getRestaurant()->getStreet()}<br>{$litigation->getRestaurant()->getPostcode()} {$litigation->getRestaurant()->getCity()}";

            $email = $this
                ->get('app.factory.email')
                ->createBuilder(Email::MANAGER_SENDINBLUE)
                ->addTo($litigation->getSupplier()->getEmail())
                ->addBcc('olivier@foodomarket.com')
                ->addReplyTo($litigation->getProspect()->getEmail())
                ->addReplyTo('olivier@foodomarket.com')
                ->addVariable('URL', $this->generateUrl('app_seller_litigation_show', ['hash' => $litigation->getHash()], Router::ABSOLUTE_URL))
                ->addVariable('FIRSTNAME', $litigation->getSupplier()->getName())
                ->addVariable('RESTAURANT', $litigation->getRestaurant()->getName())
                ->addVariable('ADDRESS', $address)
                ->addVariable('PHONE', $litigation->getProspect()->getPhone())
                ->addVariable('PROBLEM', $litigation->getProblemLabel())
                ->addVariable('DEMAND', $litigation->getDemandLabel())
                ->addVariable('MESSAGE', nl2br($litigation->getComments()->first()->getContent()))
                ->addVariable('ASK_TO_BE_CALLED', $litigation->isAskToBeCalled() ? 'Oui' : 'Non')
                ->setTemplateId(40)
                ->build()
            ;

            $this->get('app.sendinblue.manager')->addEmailToQueue($email);

            return $this->json(['ok' => true, 'litigationId' => $litigation->getId()]);
        }

        return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
    }

    public function addCommentAction(Request $request)
    {
        $litigationId = $request->get('id');
        $litigationService = $this->get('app.service.litigation');

        $form = $litigationService->getLitigationCommentForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var LitigationComment $litigationComment */
            $litigationComment = $form->getData();

            $litigationService->addComment($litigationId, $litigationComment, $this->getManagedProspect());
            $litigation = $litigationComment->getLitigation();

            $address = "{$litigation->getRestaurant()->getName()}<br>{$litigation->getRestaurant()->getStreet()}<br>{$litigation->getRestaurant()->getPostcode()} {$litigation->getRestaurant()->getCity()}";

            // sent the new response to supplier
            $email = $this
                ->get('app.factory.email')
                ->createBuilder(Email::MANAGER_SENDINBLUE)
                ->addTo($litigation->getSupplier()->getEmail())
                ->addBcc('olivier@foodomarket.com')
                ->addReplyTo($litigation->getProspect()->getEmail())
                ->addReplyTo('olivier@foodomarket.com')
                ->addVariable('URL', $this->generateUrl('app_seller_litigation_show', ['hash' => $litigation->getHash()], Router::ABSOLUTE_URL))
                ->addVariable('FIRSTNAME', $litigation->getSupplier()->getName())
                ->addVariable('RESTAURANT', $litigation->getRestaurant()->getName())
                ->addVariable('ADDRESS', $address)
                ->addVariable('PHONE', $litigation->getProspect()->getPhone())
                ->addVariable('PROBLEM', $litigation->getProblemLabel())
                ->addVariable('DEMAND', $litigation->getDemandLabel())
                ->addVariable('MESSAGE', nl2br($litigation->getComments()->first()->getContent()))
                ->addVariable('NEWMESSAGE', nl2br($litigationComment->getContent()))
                ->addVariable('ASK_TO_BE_CALLED', $litigation->isAskToBeCalled() ? 'Oui' : 'Non')
                ->setTemplateId(41)
                ->build()
            ;

            $this->get('app.sendinblue.manager')->addEmailToQueue($email);

            // Resets the form for a clear redraw
            $form = $litigationService->getLitigationCommentForm();

            return $this->json(['ok' => true, 'form' => $this->liformize($form), 'comment' => $litigationService->getLitigationComment($litigationComment)]);
        }

        return $this->json(['ok' => false, 'form' => $this->liformize($form)]);
    }

    public function getLitigationsAction() {
        return $this->json([
            'litigations' => $this->get('app.service.litigation')->getLitigationsByRestaurant($this->getRestaurant())
        ]);
    }

    public function closeLitigationAction(int $id)
    {
        /** @var Litigation $litigation */
        $litigation = $this->get('app.repository.litigation')->find($id);

        $litigation->setStatus(Litigation::STATUS_CLOSED);

        $this->getDoctrine()->getManager()->flush();

        return $this->json(['ok' => true]);
    }

    private function getLitigationForm(?Litigation $litigation = null)
    {
        if ($litigation === null) {
            $litigation = new Litigation();
        }

        // Ensures we get at least to see 1 comment section
        if ($litigation->getComments()->isEmpty()) {
            $litigationComment = new LitigationComment();

            // Prepare 3 photos
            $litigationComment->addPicture(new LitigationCommentPicture());
            $litigationComment->addPicture(new LitigationCommentPicture());
            $litigationComment->addPicture(new LitigationCommentPicture());

            $litigation->addComment($litigationComment);
        }

        $form = $this->createFormBuilder($litigation, [
            'action' => $this->generateUrl('app_front_litigation_add'),
            'csrf_protection' => false,
            'data_class' => Litigation::class
        ])
            ->add('supplier', EntityType::class,
                [
                    'required' => true,
                    'label' => 'Grossiste',
                    'class' => Supplier::class,
                    'choices' => $this->get('app.repository.supplier')->getFacadesByRestaurant($this->getRestaurant()),
                    'choice_label' => 'name',
                ]
            )
            ->add('restaurant', EntityType::class,
                [
                    'required' => true,
                    'label' => 'Restaurant',
                    'class' => Restaurant::class,
                    'choices' => $this->get('app.repository.prospect')->getProspectRestaurants($this->getManagedProspect()),
                    'choice_label' => 'name',
                ]
            )
            ->add('problem', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Quel est le problème ?',
                    'choices' => array_flip(Litigation::PROBLEM_LABEL)
                ]
            )
            ->add('demand', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Que voulez vous ?',
                    'choices' => array_flip(Litigation::DEMAND_LABEL)
                ]
            )
            ->add('askToBeCalled', ChoiceType::class,
                [
                    'required' => true,
                    'label' => 'Voulez vous être rappelé ?',
                    'choices' => ['Oui' => true,
                        'Non' => false
                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add('comments', CollectionType::class,
                [
                    'entry_type' => LitigationCommentType::class,
                    'allow_add' => false,
                    'by_reference' => false,
                    'label' => 'Réponses'
                ]
            )
            ->getForm()
        ;

        return $form;
    }
}