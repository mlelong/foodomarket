<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CatalogController extends AbstractController
{
    use ShopContext;

    public function taxonsAction()
    {
        return new JsonResponse(['taxons' => $this->getShopUtils()->getShopTaxons($this->getContextRestaurantId())]);
    }

    public function indexAction(Request $request)
    {
        $slug = $request->get('slug');
        $shopUtils = $this->getShopUtils();
        $ret = $shopUtils->getCatalogProductsByTaxonSlug($slug, $this->getContextRestaurantId());

        if ($ret === null) {
            $slugs = $shopUtils->getTaxonsSlugs($this->getContextRestaurantId());

            $bestScore = PHP_INT_MIN;
            $bestSlug = null;

            foreach ($slugs as $taxonId => $taxonSlug) {
                similar_text($taxonSlug, $slug, $score);

                if ($score > $bestScore) {
                    $bestScore = $score;
                    $bestSlug = $taxonSlug;
                }
            }

            if ($bestSlug !== null) {
                return $this->redirectToRoute('app_front_catalog_page', ['slug' => urlencode($bestSlug)], 301);
            } else {
                return $this->redirectToRoute('app_front_404_page');
            }
        }

        return $this->responseAction(['catalog' => $ret]);
    }

    public function searchAction(Request $request)
    {
        $terms = $request->get('terms');
        $splittedTerms = explode(' ', trim($terms));
        $isSearchingByVariantId = true;

        foreach ($splittedTerms as $splittedTerm) {
            if (!empty($splittedTerm) && !is_numeric($splittedTerm)) {
                $isSearchingByVariantId = false;
                break ;
            }
        }

        if (!$isSearchingByVariantId) {
            $ret = $this->getShopUtils()->getCatalogProductsByTerms($terms, $this->getContextRestaurantId());
        } else {
            $ret = $this->getShopUtils()->getCatalogProductsByVariantIds($splittedTerms);
        }

        return $this->responseAction(['catalog' => $ret]);
    }

    public function productsAction(Request $request)
    {
        $ret = $this->getShopUtils()->getCatalogProductsByTaxonSlug($request->get('slug'), $this->getContextRestaurantId());

        if ($ret === null) {
            return new JsonResponse(['ok' => false, 'message' => 'Server error']);
        }

        return new JsonResponse($ret);
    }

    public function productsByTermsAction(Request $request)
    {
        $terms = $request->get('terms');
        $splittedTerms = explode(' ', trim($terms));
        $isSearchingByVariantId = true;

        foreach ($splittedTerms as $splittedTerm) {
            if (!empty($splittedTerm) && !is_numeric($splittedTerm)) {
                $isSearchingByVariantId = false;
                break ;
            }
        }

        if (!$isSearchingByVariantId) {
            $ret = $this->getShopUtils()->getCatalogProductsByTerms($terms, $this->getContextRestaurantId());
        } else {
            $ret = $this->getShopUtils()->getCatalogProductsByVariantIds($splittedTerms);
        }

        return new JsonResponse($ret);
    }

    public function productsByPromoAction()
    {
        return $this->json($this->getShopUtils()->getCatalogProductsInPromo($this->getContextRestaurantId()));
    }

    public function promoAction()
    {
        $shopUtils = $this->getShopUtils();
        $ret = $shopUtils->getCatalogProductsInPromo($this->getContextRestaurantId());

        return $this->responseAction(['catalog-promos' => $ret]);
    }

    public function topProductsAction()
    {
        $ret = $this->getShopUtils()->getTopProducts();

        return new JsonResponse($ret['products']);
    }

    public function promosAction()
    {
        $ret = $this->getShopUtils()->getPromos();

        return new JsonResponse($ret['products']);
    }

    public function topSellProductsAction(int $limit, string $orderBy)
    {
        $ret = $this->getShopUtils()->getTopSellProducts($limit, $orderBy);

        return new JsonResponse($ret['products']);
    }

    public function topCategoriesAction()
    {
        return new JsonResponse(['ok' => true, 'topCategories' => $this->getShopUtils()->getTopCategories()]);
    }
}