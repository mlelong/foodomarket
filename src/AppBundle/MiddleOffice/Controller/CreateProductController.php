<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Services\RedisCacheManager;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Sylius\Component\Product\Model\ProductOption;
use Sylius\Component\Product\Model\ProductOptionValue;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CreateProductController extends AbstractController
{
    use ShopContext;

    public function createProductAction(Request $request)
    {
        $supplierId = $request->get('supplierId');
        $productId = $request->get('productId');
        $name = $request->get('name');
        $price = $request->get('price');
        $unit = $request->get('unit');

        if (!is_numeric($supplierId) || $supplierId <= 0) {
            return new JsonResponse(['ok' => false, 'message' => 'Please select a supplier']);
        }

        if (empty($name)) {
            return new JsonResponse(['ok' => false, 'message' => 'Please specify the product name']);
        }

        if (!is_numeric($price)) {
            return new JsonResponse(['ok' => false, 'message' => 'Please specify a correct price']);
        }

        if ($unit != 'KG' && $unit != 'PC' && $unit != 'L') {
            return new JsonResponse(['ok' => false, 'message' => 'Product price unit should be either KG or PC or L']);
        }

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            /** @var Restaurant $restaurant */
            $restaurant = $manager->find(Restaurant::class, $this->getRestaurant()->getId());
            /** @var SupplierProductPrice $price */
            $price = $manager->transactional(function (EntityManager $manager) use($restaurant, $supplierId, $productId, $name, $price, $unit) {
                $supplier = $manager->getRepository(Supplier::class)->find($supplierId);
                $product = null;

                if ($productId !== null) {
                    $product = $this->get('sylius.repository.product')->find($productId);
                }

                if ($product === null) {
                    $product = $this->get('sylius.factory.product')->createNew();
                    $product->setCode(md5(time()));
                    $product->setName($name);
                    $product->setEnabled(true);
                    $product->setSlug(Slugify::create()->slugify($name));

                    $manager->persist($product);
//                    $manager->getConnection()->exec("INSERT INTO sylius_product_channels SET product_id = '{$product->getId()}', channel_id = '{$supplier->getChannel()->getId()}';");
                }

                // Create variant
                $variant = $this->get('sylius.factory.product_variant')->createForProduct($product);
                $variant->setName('-');
                $variant->setWeight(1);

                $variant->addOptionValue($this->getUCOptionValue($unit));
                $variant->addOptionValue($this->getUMOptionValue($unit));

                $variant->setCode(md5(time()));

                $manager->persist($variant);

//                $prospect = $manager->find(Prospect::class, $this->getUser()->getCustomer()->getProspect()->getId());

//                $partnerSupplier = null;
//                /** @var Supplier $_partnerSupplier */
//                foreach ($supplier->getPartnerSuppliers() as $_partnerSupplier) {
//                    foreach ($prospect->getSuppliers() as $supplierCategory) {
//                        if ($_partnerSupplier->getId() === $supplierCategory->getSupplier()->getId()) {
//                            $partnerSupplier = $_partnerSupplier;
//                            break ;
//                        }
//                    }
//
//                    if ($partnerSupplier !== null) {
//                        break ;
//                    }
//                }

                $infos = new SupplierProductVariantInformations();

                $infos->setProduct($product);
                $infos->setProductVariant($variant);
//                $infos->setSupplier($partnerSupplier);
                $infos->setSupplier($supplier);
                $infos->setProductLabel($name);
                $infos->setDisplayName($name);
                $infos->setSalesUnit($unit);
                $infos->setVariantLabel('-');
                $infos->setReference('');
                $infos->setWeight(1);
                $infos->setContent(1);
                $infos->setUnitQuantity(1);
                $infos->setManuallyCreated(true);

                $manager->persist($infos);

                $supplierProductPrice = new SupplierProductPrice();

                $supplierProductPrice->setChannelCode($supplier->getChannel()->getCode());
                $supplierProductPrice->setRestaurant($restaurant);
                $supplierProductPrice->setProduct($product);
                $supplierProductPrice->setProductVariant($variant);
                $supplierProductPrice->setSupplier($supplier);

                switch ($unit) {
                    case 'KG':
                        $supplierProductPrice->setUnitPrice(0);
                        $supplierProductPrice->setKgPrice($price * 100);
                        break ;
                    case 'PC':
                        $supplierProductPrice->setKgPrice(0);
                        $supplierProductPrice->setUnitPrice($price * 100);
                        break ;
                    case 'L':
                        $supplierProductPrice->setUnitPrice($price * 100);
                        $supplierProductPrice->setKgPrice($price * 100);
                        break ;
                }

                $manager->persist($supplierProductPrice);

                return $supplierProductPrice;
            });

            $this->get('app.redis.cache.manager')->deleteItemsByPattern(RedisCacheManager::CACHE_KEY_PREFIX_SHOP . "restaurant-{$restaurant->getId()}-*");

            return new JsonResponse(['ok' => true, 'data' => [
                'productId' => $price->getProductVariant()->getId(),
                'supplierId' => $price->getSupplier()->getId(),
                'productPrice' => $price->getKgPrice(),
                'productPriceUnit' => $price->getUnitPrice(),
                'productName' => $name,
                'productSupplier' => $price->getSupplier()->getName(),
                'quantity' => 1,
                'quantityUnit' => $unit,
                'quantityUnitSelected' => $unit,
                'weight' => 1,
                'productPicture' => ''
            ]]);
        } catch (\Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @param $value
     * @return mixed|null|ProductOptionValue
     */
    private function getUMOptionValue($value)
    {
        $productOptionRepository = $this->get('sylius.repository.product_option');
        /** @var ProductOption $umOption */
        $umOption = $productOptionRepository->findOneBy(['code' => 'UM']);

        /** @var ProductOptionValue $umValue */
        foreach ($umOption->getValues() as $umValue) {
            if ($umValue->getValue() == $value) {
                return $umValue;
            }
        }

        return null;
    }

    /**
     * @param $value
     * @return mixed|null|ProductOptionValue
     */
    private function getUCOptionValue($value)
    {
        $productOptionRepository = $this->get('sylius.repository.product_option');
        /** @var ProductOption $ucOption */
        $ucOption = $productOptionRepository->findOneBy(['code' => 'UC']);

        /** @var ProductOptionValue $ucValue */
        foreach ($ucOption->getValues() as $ucValue) {
            if ($ucValue->getValue() == $value) {
                return $ucValue;
            }
        }

        return null;
    }
}