<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InvoiceController extends AbstractController
{
    use ShopContext;

    private function getUploadForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('invoice', FileType::class, ['required' => true])
            ->getForm()
        ;
    }

    public function indexAction(Request $request)
    {
        $dateStart = new \DateTime(str_replace('/', '-', $request->get('start')));
        $dateEnd = new \DateTime(str_replace('/', '-', $request->get('end')));
        $prospect = $this->getUser()->getCustomer()->getProspect();
        $invoiceRepository = $this->get('app.repository.invoice');
        /** @var Invoice[] $invoices */
        $invoices = $invoiceRepository->createQueryBuilder('invoice')
            ->where('invoice.prospect = :prospect')
            ->andWhere('invoice.date BETWEEN :start AND :end')
            ->orderBy('invoice.date', 'DESC')
            ->setParameter('prospect',  $prospect->getId())
            ->setParameter('start', $dateStart)
            ->setParameter('end', $dateEnd)
            ->getQuery()
            ->getResult();

        $invoicesArray = [];

        foreach ($invoices as $invoice) {
            $invoiceArray = [
                'id' => $invoice->getId(),
                'number' => $invoice->getNumber(),
                'date' => $invoice->getDate()->format('d/m/Y'),
                'total' => $invoice->getTotal(),
                'clientCode' => $invoice->getClientCode(),
                'file' => $invoice->getInvoicePath(),
                'supplier' => $invoice->getSupplier()->getName(),
                'deliveryNotes' => $invoice->getInvoiceDeliveryNotes()->map(function(InvoiceDeliveryNote $deliveryNote) {
                    return [
                        'number' => $deliveryNote->getNumber(),
                        'date' => $deliveryNote->getDate()->format('d/m/Y'),
                        'total' => $deliveryNote->getTotal(),
                        'items' => $deliveryNote->getItems()->map(function(InvoiceDeliveryNoteItem $item) {
                            $variant = $item->getVariant();
                            $price = $item->getPrice();

                            $unitQuantity = 1;
                            $saleUnit = 'KG';

                            if ($variant !== null) {
                                $unitQuantityOption = $variant->getOptionUnitQuantity();
                                $saleUnitOption = $variant->getOptionSupplierSalesUnit();

                                if ($unitQuantityOption !== false) {
                                    $unitQuantity = intval($unitQuantityOption->getValue());
                                }

                                if ($saleUnitOption !== false) {
                                    $saleUnit = $saleUnitOption->getValue();
                                }
                            }

                            $unitPrice = $price !== null ? floatval(
                                bcdiv($saleUnit === 'PC'
                                    ? $price->getUnitPrice() / $unitQuantity
                                    : $price->getKgPrice(), 100, 2))
                                : 0
                            ;

                            return [
                                'original' => [
                                    'reference' => $item->getReference(),
                                    'name' => $item->getName(),
                                    'quantity' => $item->getQuantity(),
                                    'unitPrice' => $item->getUnitPrice(),
                                    'totalPrice' => $item->getTotalPrice(),
                                ],
                                'computed' => [
                                    'name' => $item->getVariant() !== null ? $item->getVariant()->getFullDisplayName() : '',
                                    'unitPrice' => $unitPrice,
                                    'totalPrice' => floatval($unitPrice) * $item->getQuantity()
                                ]
                            ];
                        })->toArray()
                    ];
                })->toArray()
            ];

            $invoicesArray[] = $invoiceArray;
        }

        return new JsonResponse(['ok' => true, 'invoices' => $invoicesArray]);
    }

    public function downloadAction(Request $request)
    {
        /** @var Invoice $invoice */
        $invoice = $this->get('app.repository.invoice')->find($request->get('id'));

        if ($invoice === null || $invoice->getProspect()->getId() != $this->getUser()->getCustomer()->getProspect()->getId()) {
            return new Response("Facture introuvable");
        }

        $invoicePath = $invoice->getInvoicePath();
        $invoiceFileName = substr($invoicePath, strrpos($invoicePath, '/') + 1);

        return $this->file($invoicePath, $invoiceFileName);
    }
}