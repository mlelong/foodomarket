<?php

namespace AppBundle\MiddleOffice\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Email;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShopUser;
use AppBundle\MiddleOffice\Controller\Traits\ShopContext;
use AppBundle\Repository\SupplierCategoryRepository;
use DateTime;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Sylius\Component\User\Security\Generator\GeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AccountController extends AbstractController
{
    use ShopContext;

    const CREATE_USER = false;
    const AUTO_AFFECT_SUPPLIERS = false;

    public function registerPageAction(Request $request)
    {
        $response = $this->responseAction();

        if (!empty($request->get('token', $request->get('h')))) {
            $expiresAt = (new \DateTime())->modify('+29 day');
            $cookieHash = new Cookie('_hash', $request->get('token', $request->get('h')), $expiresAt);
            $response->headers->setCookie($cookieHash);
        }

        return $response;
    }

    public function registerFormAction(Request $request)
    {
        $prospect = $this->getContextProspect($request);

        return $this->json(array_merge($this->liformize($this->getRegisterForm($prospect)), ['step' => $prospect->getRegistrationStep()]));
    }

    public function verifyAction(Request $request)
    {
        /** @var ShopUser $user */
        $user = $this->get('app.repository.shop_user')->findOneBy(['emailVerificationToken' => $request->get('token')]);

        if ($user === null) {
            return $this->responseAction(['account-verified' => false]);
        }

        $user->setVerifiedAt(new DateTime());
        $user->setEmailVerificationToken(null);
        $user->enable();

        $prospect = $user->getCustomer()->getProspect();

        $prospect->setEnabled(true);

        $this->getDoctrine()->getManager()->flush();

        return $this->responseAction(['account-verified' => true]);
    }

    public function handleRegisterFormSubmitAction(Request $request)
    {
        $prospect = $this->getContextProspect($request);
        $step = $request->get('step', $prospect->getRegistrationStep());

        // Allow override of prospects, so that reusing an email address is possible and does not trigger insert error
        if ($step == 1 && $request->request->has('form')) {
            $formDataSubmitted = $request->request->get('form');

            if (isset($formDataSubmitted['email'])) {
                $newEmail = $formDataSubmitted['email'];

                // Si email différent (l'utilisateur l'a changé dans le champs input)
                // ou si ils sont identiques mais l'id actuel du prospect est null (prospect non persisté)
                if ($prospect->getEmail() !== $newEmail || $prospect->getId() === null) {
                    // On recherche ce prospect par email
                    $prospectByEmail = $this->get('app.repository.prospect')->getProspect(['email' => $newEmail]);

                    // Si il n'est pas null, alors on a trouvé une version persistée correspondant, on doit l'utiliser sinon
                    // on aura un insert error email unique !
                    if ($prospectByEmail !== null) {
                        $prospect = $prospectByEmail;
                    }
                }
            }
        }

        // If user go back on the form and then resubmit, we should advance the step accordingly
        // instead of just incrementing it blindly, let's restart the cursor to the current position
        // if specified in the request, otherwise default prospect step will be used
        $prospect->setRegistrationStep($step);

        $form = $this->getRegisterForm($prospect);
        $errors = [];

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager = $this->getDoctrine()->getManager();

                // Update registration step manually
                $prospect->setRegistrationStep($prospect->getRegistrationStep() + 1);

                if (empty($prospect->getHash())) {
                    $prospect->setHash(uniqid("", true));
                }

                $manager->persist($prospect);
                $manager->flush();

                // Final step
                if ($prospect->getRegistrationStep() == 5) {
                    $shopUser = $this->createRestaurantAndShopUser($prospect);

                    // Persists restaurant, customer and shopuser
                    $manager->flush();

                    $verifyUrl = $this->generateUrl('app_front_register_verify_page', ['token' => $shopUser->getEmailVerificationToken()], Router::ABSOLUTE_URL);

                    /**
                     * Sends Confirmation Email to client
                     *
                     * if we are in app_dev, the url will be generated in app_dev...
                     *  everyone uses the BO in app_dev
                     *  so sending the verification email will show in app_dev
                     *  let's replace this manually
                     */
                    $verifyUrl = str_replace('app_dev.php/', '', $verifyUrl);

                    $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

                    $emailBuilder->setTemplateId(17);
                    $emailBuilder->addTo($prospect->getEmail());
                    $emailBuilder->addVariable('FIRSTNAME', $shopUser->getCustomer()->getFirstName());
                    $emailBuilder->addVariable('URL', $verifyUrl);
                    $emailBuilder->addVariable('LOGIN', $shopUser->getEmail());
                    $emailBuilder->addVariable('PASSWORD', $prospect->getPlainPassword());

                    $sendinBlueManager = $this->get('app.sendinblue.manager');

                    $sendinBlueManager->addEmailToQueue($emailBuilder->build());

                    /**
                     * Notify hello@foodomarket.com
                     */
                    $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

                    $boProspectUrl = $this->generateUrl('app_admin_admin_prospect_show', ['id' => $prospect->getId()], Router::ABSOLUTE_URL);

                    $emailBuilder
                        ->addTo('hello@foodomarket.com')
                        ->addFrom('olivier@foodomarket.com')
                        ->setSubject("Inscription complète de {$prospect->getRestaurantName()}")
                        ->setHtmlBody("<p>Bonjour<br/><br/>Voila une nouvelle inscription sur foodomarket : <a href='{$boProspectUrl}' target='_blank'>{$prospect->getRestaurantName()} à {$prospect->getCity()} ({$prospect->getZipCode()})</a><br/>{$prospect->getAddress()}</p>")
                    ;

                    $sendinBlueManager->addEmailToQueue($emailBuilder->build());

                    $response = $this->json(['ok' => true]);
                    $response->headers->clearCookie('_hash');

                    $cookieRegistered = new Cookie('_reg', true, (new DateTime())->modify('+29 day'), '/', null, false, false);
                    $response->headers->setCookie($cookieRegistered);

                    // Creates a complete subscription in hubspot lifecycle
                    $this->get('app.services.hubspot')->createContactAndCompleteSubscriptionHoppy($prospect);

                    return $response;
                }
            } else {
                $formErrors = $form->getErrors(true);

                foreach ($formErrors as $error) {
                    $errors[] = $error->getMessage();
                }

                return $this->json(['ok' => false, $errors]);
            }
        }

        $response = $this->json(['ok' => true]);

        $expiresAt = (new DateTime())->modify('+29 day');

        if (!empty($prospect->getRestaurantName())) {
            $cookieRestaurant = new Cookie('_restaurant', $prospect->getRestaurantName(), $expiresAt, '/', null, false, false);
            $response->headers->setCookie($cookieRestaurant);
        }

        if (!empty($prospect->getEmail())) {
            $cookieEmail = new Cookie('_email', $prospect->getEmail(), $expiresAt, '/', null, false, false);
            $response->headers->setCookie($cookieEmail);
        }

        if (!empty($prospect->getHash())) {
            $cookieHash = new Cookie('_hash', $prospect->getHash(), $expiresAt);
            $response->headers->setCookie($cookieHash);
        }

        return $response;
    }

    public function updatePasswordFormAction(Request $request)
    {
        $form = $this->getUpdatePasswordForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();

                $a = 1;
            }
        }

        return $this->json($this->liformize($form));
    }

//    public function updatePasswordAction(Request $request)
//    {
//
//    }

    private function getUpdatePasswordForm()
    {
        return $this->createFormBuilder(null, ['csrf_protection' => false])
            ->add('email', EmailType::class, ['data' => $this->getUser()->getEmail(), 'label' => 'Email', 'attr' => ['readonly' => true, 'disabled' => true], 'liform' => ['widget' => 'email']])
            ->add('oldPassword', PasswordType::class, ['label' => 'Ancien mot de passe', 'liform' => ['widget' => 'password']])
            ->add('newPassword', PasswordType::class, ['label' => 'Nouveau mot de passe', 'liform' => ['widget' => 'password']])
            ->getForm()
        ;
    }

    public function registerAction(Request $request)
    {
        $restaurantName = $request->request->get('restaurantName');
        $restaurantAddress = $request->request->get('restaurantAddress');
        $restaurantZipCode = $request->request->get('restaurantZipCode');
        $restaurantCity = $request->request->get('restaurantCity');
        $managerLastname = $request->request->get('managerLastname', $request->request->get('lastname'));
        $managerFirstname = $request->request->get('managerFirstname', $request->request->get('firstname'));
        $company = $request->request->get('company');
        $siren = $request->request->get('siren');
        $telephone = $request->request->get('telephone');
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        if (self::CREATE_USER) {
            if (empty($restaurantName) || empty($restaurantAddress) || empty($restaurantZipCode) || empty($restaurantCity)
                || empty($managerFirstname) || empty($managerLastname) || empty($company) || empty($siren) || empty($telephone)
                || empty($email) || empty($password)
            ) {
                return new JsonResponse(['ok' => false, 'message' => 'Tous les champs sont obligatoires']);
            }
        } else {
            if (empty($restaurantName) || empty($restaurantAddress) || empty($restaurantZipCode) || empty($restaurantCity)
                || empty($managerFirstname) || empty($managerLastname) || empty($company) || empty($email) || empty($telephone)
            ) {
                return new JsonResponse(['ok' => false, 'message' => 'Tous les champs sont obligatoires']);
            }
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse(['ok' => false, 'message' => "L'adresse email est invalide"]);
        }

        $prospect = $this->get('app.repository.prospect')->findOneBy(['email' => $email]);

//        if ($prospect !== null) {
//            return new JsonResponse(['ok' => false, 'message' => 'Cette adresse email est déjà utilisée']);
//        }

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            /** @var ShopUser|Prospect $shopUser */
            $shopUserOrProspect = $manager->transactional(function (EntityManager $manager) use (
                $email, $managerFirstname, $managerLastname, $restaurantName, $restaurantAddress,
                $restaurantZipCode, $restaurantCity, $telephone, $company, $password, $siren, $prospect) {

                if ($prospect === null) {
                    $prospect = new Prospect();
                }

                /**
                 * BEGIN: Save utm params
                 */
                $ip = $this->get('request_stack')->getMasterRequest()->getClientIp();

                $session = $this->get('session');

                $prospect->setIp($ip);
                $prospect->setUtmEmail($session->get('email'));

                if (!empty($session->get('utm_source'))) {
                    $prospect->setUtmSource($session->get('utm_source'));
                } elseif (!empty($session->get('gclid'))) {
                    $prospect->setUtmSource('adwords');
                } elseif (!empty($session->get('fbclid'))) {
                    $prospect->setUtmSource('facebook');
                }

                $prospect->setUtmMedium($session->get('utm_medium'));
                $prospect->setUtmCampaign($session->get('utm_campaign'));
                $prospect->setUtmTerm($session->get('utm_term'));
                $prospect->setUtmContent($session->get('utm_content'));

                if (empty($prospect->getUtmSource())) {
                    $prospect->setSource('landing');
                } else {
                    $prospect->setSource($prospect->getUtmSource());
                }
                /**
                 * END: Save utm params
                 */

                $prospect->setEmail($email);
                $prospect->setFirstName($managerFirstname);
                $prospect->setContactName($managerLastname);
                $prospect->setCompanyName($company);
                $prospect->setRestaurantName($restaurantName);
                $prospect->setAddress($restaurantAddress);
                $prospect->setCity($restaurantCity);
                $prospect->setZipCode($restaurantZipCode);
                $prospect->setPhone($telephone);
                $prospect->setSiren($siren);

                $manager->persist($prospect);

                $this->get('app.services.hubspot')->createContactAndCallbackHoppy($prospect);

                if (self::CREATE_USER) {
                    $restaurant = new Restaurant();

                    $restaurant->setName($restaurantName);
                    $restaurant->setCity($restaurantCity);
                    $restaurant->setPostcode($restaurantZipCode);
                    $restaurant->setStreet($restaurantAddress);
                    $restaurant->setCountryCode('FR');
                    $restaurant->setEmail($email);
                    $restaurant->setEnabled(true);

                    $manager->persist($restaurant);

                    $customer = new Customer();

                    $customer->setEmail($email);
                    $customer->setProspect($prospect);
                    $customer->setFirstName($managerFirstname);
                    $customer->setLastName($managerLastname);
                    /** @noinspection PhpParamsInspection */
                    $customer->setGroup($this->get('sylius.repository.customer_group')->findOneBy(['code' => 'restaurant']));
                    $customer->addRestaurant($restaurant);

                    $manager->persist($customer);

                    $shopUser = new ShopUser();

                    $shopUser->setCustomer($customer);
                    $shopUser->setEmail($email);
                    $shopUser->setPlainPassword($password);
                    $shopUser->setPhone($telephone);
                    $shopUser->setEnabled(true);

                    $manager->persist($shopUser);

                    return $shopUser;
                }

                return $prospect;
            });

            $response = new JsonResponse(['ok' => true]);

            if (self::CREATE_USER) {
                /** @var JWTManager $jwtManager */
                $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
                /** @var EventDispatcher $dispatcher */
                $dispatcher = $this->get('event_dispatcher');

                $jwt = $jwtManager->create($shopUserOrProspect);
                $event = new AuthenticationSuccessEvent(array('token' => $jwt), $shopUserOrProspect, $response);

                $dispatcher->dispatch(Events::AUTHENTICATION_SUCCESS, $event);
                $response->setData(array_merge(['ok' => true], $event->getData()));
            }

            return $response;
        } catch (\Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    private function createRestaurantAndShopUser(Prospect $prospect)
    {
        $manager = $this->getDoctrine()->getManager();

        $restaurant = new Restaurant();

        $restaurant->setName($prospect->getRestaurantName());
        $restaurant->setCity($prospect->getCity());
        $restaurant->setPostcode($prospect->getZipCode());
        $restaurant->setStreet($prospect->getAddress());
        $restaurant->setCountryCode('FR');
        $restaurant->setEmail($prospect->getEmail());
        $restaurant->setEnabled(true);

        $manager->persist($restaurant);

        $customer = $manager->getRepository(Customer::class)->findOneBy(['email' => $prospect->getEmail()]);

        if ($customer === null) {
            $customer = new Customer();
        }

        $customer->setEmail($prospect->getEmail());
        $customer->setProspect($prospect);
        $customer->setFirstName($prospect->getFirstName());
        $customer->setLastName($prospect->getContactName());
        /** @noinspection PhpParamsInspection */
        $customer->setGroup($this->get('sylius.repository.customer_group')->findOneBy(['code' => 'restaurant']));
        $customer->addRestaurant($restaurant);

        $manager->persist($customer);

        $shopUser = $manager->getRepository(ShopUser::class)->findOneBy(['email' => $prospect->getEmail()]);

        if ($shopUser === null) {
            $shopUser = new ShopUser();
        }

        $shopUser->setCustomer($customer);
        $shopUser->setEmail($prospect->getEmail());
        $shopUser->setPlainPassword($prospect->getPlainPassword());
        $shopUser->setPhone($prospect->getMobile());
        $shopUser->setEnabled(false);
        $shopUser->setVerifiedAt(null);

        $shopUser->setEmailVerificationToken($this->get('sylius.shop_user.token_generator.email_verification')->generate());

        $manager->persist($shopUser);

        $prospect->setEnabled(false);

        if (self::AUTO_AFFECT_SUPPLIERS) {
            $suppliers = explode(',', $prospect->getCurrentSupplier());

            $suppliersMap = [
                'Jacob' => 37,
                'Halles Prestige' => 18,
                'Daumesnil' => 22,
                'Reynaud' => 24,
                'Alazard' => 21,
                'Domafrais' => 39,
                "Wines n' Roses" => 35
            ];

            /** @var SupplierCategoryRepository $supplierCategoryRepository */
            $supplierCategoryRepository = $this->get('app.repository.supplier_category');

            foreach ($suppliersMap as $supplierName => $supplierId) {
                if (!in_array($supplierName, $suppliers)) {
                    $supplierCategories = $supplierCategoryRepository->findBySupplier($supplierId);

                    foreach ($supplierCategories as $supplierCategory) {
                        $prospect->addSupplier($supplierCategory);
                    }
                }
            }
        }

        return $shopUser;
    }

    public function noProductFoundRegisterAction(Request $request)
    {
        return $this->simpleRegister(
            $request->request->get('restaurantName'),
            $request->request->get('email'),
            'no_product_found'
        );
    }

    public function customOfferRequestRegisterAction(Request $request)
    {
        return $this->simpleRegister(
            $request->request->get('restaurantName'),
            $request->request->get('email'),
            'request_custom_offer'
        );
    }

    public function timeoutModalRegisterAction(Request $request)
    {
        return $this->simpleRegister(
            $request->request->get('restaurantName'),
            $request->request->get('email'),
            'landing'
        );
    }

    public function updateProspectTelephoneAction(Request $request)
    {
        $this->prepareUtmSession();

        $email = $request->cookies->get('_email');
        $telephone = $request->request->get('telephone');
        $shouldCreateHubspotCallbackHoppy = true;

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = 'too_fast@' . microtime() . '.com';
            $shouldCreateHubspotCallbackHoppy = false;
        }

        $prospectRepository = $this->get('app.repository.prospect');
        $prospect = $prospectRepository->findOneBy(['email' => $email]);

        if ($prospect === null) {
            $prospect = new Prospect();

            $prospect->setEmail($email);
            $prospectRepository->add($prospect);
        }

        $prospect->setPhone($telephone);

        $this->bindUtmParametersToProspect($prospect);

        // Appel au service du ProspectSource et créer l'objet ProspectSource
        $prospectSourceService = $this->get('app.service.prospect.source');
        $prospectSourceService->createProspectSource($prospect,  "Téléphone");

        $this->getDoctrine()->getManager()->flush();

        if ($shouldCreateHubspotCallbackHoppy) {
            $this->get('app.services.hubspot')->createContactAndCallbackHoppy($prospect);
        }

        return new JsonResponse(['ok' => true]);
    }


    private function simpleRegister(?string $restaurantName, ?string $email, string $registerSource)
    {
        if (empty($restaurantName) || empty($email)) {
            return new JsonResponse(['ok' => false, 'message' => 'Tous les champs sont obligatoires']);
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse(['ok' => false, 'message' => "L'adresse email est invalide"]);
        }

        $this->prepareUtmSession();

        $prospectRepository = $this->get('app.repository.prospect');

        /** @var Prospect|null $prospect */
        $prospect = $prospectRepository->findOneBy(['email' => $email]);

        if ($prospect === null) {
            $prospect = new Prospect();

            $prospect->setEnabled(false);
            $prospect->setEmail($email);
            $prospect->setRestaurantName($restaurantName);
            $prospect->setSource($registerSource);
            $prospect->setNewsletterPrices(false);
            $prospect->setEmailSubscribed(false);

            $this->bindUtmParametersToProspect($prospect);

            $prospectRepository->add($prospect);
        } else {
            $prospect->setRestaurantName($restaurantName);
            $prospect->addSource($registerSource);

            $this->bindUtmParametersToProspect($prospect);

            try {
                $prospect->setUpdatedAt(new DateTime());
            } catch (\Exception $e) {
                $prospect->setSource($prospect->getSource() . ', forceupdate');
            }

            try {
                $this->get('doctrine.orm.entity_manager')->flush();
            } catch (\Exception $e) {
                return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
            }
        }

        // Appel au service du ProspectSource et créer l'objet ProspectSource
        $prospectSourceService = $this->get('app.service.prospect.source');
        $prospectSourceService->createProspectSource($prospect,  "Simple register");

        // hubspot : record transaction
        $this->get('app.services.hubspot')->createContactAndSubscriptionHoppy($prospect);

        $response = new JsonResponse(['ok' => true]);

        $expiresAt = (new DateTime())->modify('+29 day');

        $cookieRestaurant = new Cookie('_restaurant', $restaurantName, $expiresAt, '/', null, false, false);
        $response->headers->setCookie($cookieRestaurant);

        $cookieEmail = new Cookie('_email', $email, $expiresAt, '/', null, false, false);

        $response->headers->setCookie($cookieEmail);

        return $response;
    }

    public function checkEmailAction($email)
    {
        if ($this->get('app.repository.prospect')->getClient(['email' => $email])) {
            return $this->json(['ok' => false, 'message' => 'Cet email est déjà utilisé.']);
        }

        return $this->json(['ok' => true]);
    }

    public function requestPasswordResetAction(Request $request)
    {
        $form = $this->getRequestPasswordResetForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $email = $form->get('email')->getData();
                $shopUserRepository = $this->get('app.repository.shop_user');
                /** @var ShopUser $shopUser */
                $shopUser = $shopUserRepository->findOneByEmail($email);

                if ($shopUser !== null) {
                    /** @var GeneratorInterface $generator */
                    $generator = $this->get('sylius.shop_user.token_generator.password_reset');

                    $shopUser->setPasswordResetToken($generator->generate());
                    $shopUser->setPasswordRequestedAt(new DateTime());

                    $this->getDoctrine()->getManager()->flush();

                    $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

                    $email = $emailBuilder
                        ->setTemplateId(30)
                        ->addTo($shopUser->getEmail())
                        ->addVariable('INTERLOCUTOR', $shopUser->getCustomer()->getFirstName())
                        ->addVariable('EMAIL', $shopUser->getEmail())
                        ->addVariable('URL', $this->get('router')->generate('app_front_reset_password_page', ['token' => $shopUser->getPasswordResetToken()], Router::ABSOLUTE_URL))
                        ->build()
                    ;

                    $this->get('app.sendinblue.manager')->addEmailToQueue($email);
                } else {
                    return $this->json(['ok' => false]);
                }
            } else {
                return $this->json(['ok' => false]);
            }
        }

        return $this->json(['ok' => true]);
    }

    public function resetPasswordAction(Request $request)
    {
        $token = $request->get('token');
        $shopUserRepository = $this->get('app.repository.shop_user');
        /** @var ShopUser $shopUser */
        $shopUser = $shopUserRepository->findOneBy(['passwordResetToken' => $token]);
        $form = $this->getPasswordResetForm();

        if ($shopUser === null) {
            return $this->redirectToRoute('app_front_404_page');
        }

        if ($request->isXmlHttpRequest()) {
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $shopUser->setPlainPassword($form->get('password')->getData());
                    $shopUser->setPasswordResetToken(null);
                    $shopUser->setPasswordRequestedAt(null);
//                    $shopUser->enable();

                    $this->getDoctrine()->getManager()->flush();

                    return $this->json(['ok' => true]);
                } else {
                    return $this->json(['ok' => false]);
                }
            }
        }

        return $this->responseAction(['password-reset-form' => $this->liformize($form)]);
    }
}