<?php

namespace AppBundle\Services;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Email;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShopUser;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Entity\SupplierCategory;
use AppBundle\Helper\CurlHelper;
use AppBundle\Repository\CustomerRepository;
use AppBundle\Repository\MercurialeRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\ShopUserRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierCategoryRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\Hubspot\HubspotService;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueV3Manager;
use AppBundle\Util\SupplierUtil;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Throwable;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ShopUserService
{
    const AUTO_AFFECT_SUPPLIERS = true;

    private $manager;
    private $shopUserRepository;
    private $customerRepository;
    private $customerGroupRepository;
    private $restaurantRepository;
    private $supplierCategoryRepository;
    private $supplierAccountLogRepository;
    private $emailFactory;
    private $emailManager;
    private $mercurialeRepository;
    private $uploaderHelper;
    private $hubspotService;

    private $kbisPath;
    private $webDir;
    private $baseUrl;

    private $emailNotificationEnabled = true;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    public function __construct(
        EntityManager $manager,
        ShopUserRepository $shopUserRepository,
        CustomerRepository $customerRepository,
        EntityRepository $customerGroupRepository,
        RestaurantRepository $restaurantRepository,
        SupplierCategoryRepository $supplierCategoryRepository,
        SupplierAccountLogRepository $supplierAccountLogRepository,
        EmailFactory $emailFactory,
        SendInBlueV3Manager $emailManager,
        MercurialeRepository $mercurialeRepository,
        UploaderHelper $uploaderHelper,
        HubspotService $hubspotService,
        SupplierRepository $supplierRepository,
        string $kbisPath,
        string $webDir,
        string $baseUrl)
    {
        $this->manager = $manager;
        $this->shopUserRepository = $shopUserRepository;
        $this->customerRepository = $customerRepository;
        $this->customerGroupRepository = $customerGroupRepository;
        $this->restaurantRepository = $restaurantRepository;
        $this->supplierCategoryRepository = $supplierCategoryRepository;
        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
        $this->emailFactory = $emailFactory;
        $this->emailManager = $emailManager;
        $this->mercurialeRepository = $mercurialeRepository;
        $this->uploaderHelper = $uploaderHelper;
        $this->hubspotService = $hubspotService;
        $this->kbisPath = $kbisPath;
        $this->webDir = $webDir;
        $this->baseUrl = $baseUrl;
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * @param Prospect $prospect
     * @return ShopUser|null
     */
    function createRestaurantAndShopUser(Prospect $prospect)
    {
        try {
            $shopUser = $this->manager->transactional(function (EntityManager $em) use ($prospect) {
                $restaurant = new Restaurant();

                $restaurantName = $prospect->getType() === Prospect::TYPE_PROFESSIONNAL ? $prospect->getRestaurantName() : $prospect->getFirstName();

                $restaurant->setName($restaurantName);
                $restaurant->setCity(!empty($prospect->getCity()) ? $prospect->getCity() : '');
                $restaurant->setPostcode(!empty($prospect->getZipCode()) ? $prospect->getZipCode() : '');
                $restaurant->setStreet($prospect->getAddress());
                $restaurant->setCountryCode('FR');
                $restaurant->setEmail($prospect->getEmail());
                $restaurant->setEnabled(true);

                $em->persist($restaurant);

                $customer = $this->customerRepository->findOneBy(['email' => $prospect->getEmail()]);

                if ($customer === null) {
                    $customer = new Customer();
                }

                $customer->setEmail($prospect->getEmail());
                $customer->setProspect($prospect);
                $customer->setFirstName($prospect->getFirstName());
                $customer->setLastName($prospect->getContactName());
                /** @noinspection PhpParamsInspection */
                $customer->setGroup($this->customerGroupRepository->findOneBy(['code' => 'restaurant']));
                $customer->addRestaurant($restaurant);

                $em->persist($customer);

                $shopUser = $this->shopUserRepository->findOneBy(['username' => $prospect->getEmail()]);

                if ($shopUser === null) {
                    $shopUser = new ShopUser();
                }

                $shopUser->setCustomer($customer);
                $shopUser->setEmail($prospect->getEmail());
                $shopUser->setPlainPassword($prospect->getPlainPassword());
                $shopUser->setPhone($prospect->getMobile());
                $shopUser->setEnabled(true);
                $shopUser->setVerifiedAt(new DateTime());

                $em->persist($shopUser);

                $prospect->setEnabled(true);

                return $shopUser;
            });
        } catch (Throwable $e) {
            $shopUser = null;
        }

        $zipCode = $prospect->getZipCode();

        if (self::AUTO_AFFECT_SUPPLIERS && $prospect->getType() === Prospect::TYPE_PROFESSIONNAL) {
//            if (preg_match('/^(75|78|92)/', $zipCode)) {
//                // Jacob
//                $this->openSupplierAccount($prospect, 37, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            if (preg_match('/^(75|92)/', $zipCode)) {
//                // Verger de Souama
//                $this->openSupplierAccount($prospect, 45, SupplierAccountLog::STATUS_ACCOUNT_OPENED);
//            }
//
//            if (preg_match('/^(75|78|92|93|91|95|77)/', $zipCode)) {
//                // Daumesnil
//                $this->openSupplierAccount($prospect, 22, SupplierAccountLog::STATUS_WAITING);
//
//                // Halles prestige - SP
//                $this->openSupplierAccount($prospect, 23, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            if (preg_match('/^(75|91|92)/', $zipCode)) {
//                // Beaugrain
//                $this->openSupplierAccount($prospect, 54, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            if (preg_match('/^(33|24|47)/', $zipCode)) {
//                // Sainfruit
//                $this->openSupplierAccount($prospect, 76, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            if (preg_match('/^(64|40)/', $zipCode)) {
//                // Primadour
//                $this->openSupplierAccount($prospect, 70, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            if (preg_match('/^(31|82|81|09|32|65)/', $zipCode)) {
//                // Garonne Fruits
//                $this->openSupplierAccount($prospect, 69, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            // MTC
//            $this->openSupplierAccount($prospect, 56, SupplierAccountLog::STATUS_WAITING);
//
//            if (preg_match('/^(33|40|64|31)/', $zipCode)) {
//                // Estiveau
//                $this->openSupplierAccount($prospect, 68, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            if (preg_match('/^(09|12|33|40|24|31|47|64|81|82)/', $zipCode)) {
//                // Sobomar
//                $this->openSupplierAccount($prospect, 72, SupplierAccountLog::STATUS_WAITING);
//            }
//
//            $groupeLeSaint = [
//                100 => '/^(85)/', // Armor Fruits
//                101 => '/^(22|35)/', // Bourguignon
//                102 => '/^(53|72)/', // DC Primeurs
//                103 => '/^(14|50|61|76)/', // Foissier
//                104 => '/^(44|49)/', // Fruidis
//                105 => '/^(29)/', // Lesaint
//                106 => '/^(17|79|16)/', // Ponsprimeurs
//                107 => '/^(09|11|34|66)/', // Sudprimeurs
//                108 => '/^(18|28|36|37|41|45|86)/', // Valifruit
//                110 => '/^(85)/', // Devaud
//            ];
//
//            foreach ($groupeLeSaint as $supplierId => $regexp) {
//                if (preg_match($regexp, $zipCode)) {
//                    $this->openSupplierAccount($prospect, $supplierId, SupplierAccountLog::STATUS_WAITING);
//                }
//            }

            $partners = $this->supplierRepository->getPartners();

            foreach ($partners as $partner) {
                $facade = $partner->getFacadeSupplier();

                // If the supplier is boundable
                if ($facade->isAutoBind() && SupplierUtil::getDeliveryCost($facade, $zipCode) !== null) {
                    // If it is HP, then auto bound only the SP grid
                    if ($facade->getId() == 11 && $partner->getId() != 23) {
                        continue ;
                    }

                    // Verger de Souama: Ouvert direct quoi qu'il arrive
                    if ($facade->getId() == 44) {
                        $this->openSupplierAccount(
                            $prospect,
                            $partner->getId(),
                            SupplierAccountLog::STATUS_ACCOUNT_OPENED
                        );
                    } else {
                        $this->openSupplierAccount(
                            $prospect,
                            $partner->getId(),
                            $facade->isPaymentOnline() ? SupplierAccountLog::STATUS_ORDERING : SupplierAccountLog::STATUS_WAITING
                        );
                    }
                }
            }
        } elseif (self::AUTO_AFFECT_SUPPLIERS && $prospect->getType() === Prospect::TYPE_INDIVIDUAL) {
            $partners = $this->supplierRepository->getIndividualsPartners();

            foreach ($partners as $partner) {
                if (SupplierUtil::getDeliveryCost($partner, $zipCode) !== null) {
                    $this->openSupplierAccount($prospect, $partner->getId(), SupplierAccountLog::STATUS_ACCOUNT_OPENED);
                }
            }
        }

        return $shopUser;
    }

    /**
     * @param Prospect $prospect
     * @param Supplier|int $supplier
     * @param string $status
     * @return bool
     */
    public function openSupplierAccount(Prospect $prospect, $supplier, string $status = SupplierAccountLog::STATUS_WAITING)
    {
        $supplierCategories = $this->supplierCategoryRepository->findBySupplier($supplier);
        $categories = [];

        foreach ($supplierCategories as $supplierCategory) {
            $supplierAccountLog = new SupplierAccountLog();

            $supplierAccountLog->setProspect($prospect);
            $supplierAccountLog->setSupplierCategory($supplierCategory);
            $supplierAccountLog->setStatus($status);

            try {
                $supplierAccountLog->setHash(substr(strtr(base64_encode(bin2hex(random_bytes(32))), '+', '.'), 0, 44));
            } catch (Exception $e) {
                $supplierAccountLog->setHash(uniqid());
            }

            try {
                $this->manager->persist($supplierAccountLog);
                $prospect->addSupplier($supplierCategory);
            } catch (ORMException $e) {
                return false;
            }

            $categories[] = Supplier::getDisplayCategory($supplierCategory->getCategory());
        }

        try {
            $this->manager->flush();
        } catch (OptimisticLockException $e) {
            return false;
        } catch (ORMException $e) {
            return false;
        }

        if ($this->isEmailNotificationEnabled() && $prospect->getType() === Prospect::TYPE_PROFESSIONNAL) {
            $this->notifySupplier($supplierCategories[0]->getSupplier(), $prospect, $categories);
            $this->notifyClient($prospect, $supplierCategories);
        }

        if ($status === SupplierAccountLog::STATUS_ACCOUNT_OPENED && $prospect->getType() === Prospect::TYPE_PROFESSIONNAL) {
            try {
                CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/oto6093/', [], [
                    'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                    'FIRST_NAME' => $prospect->getFirstName(),
                    'PHONE' => $prospect->getPhone(),
                    'MAIL' => $prospect->getEmail(),
                    'DATE' => (new DateTime())->format('c'),
                    'ADDRESS' => $prospect->getZipCode()
                ]);
            } catch (Exception $ignored) {
            }
        }

        return true;
    }

    /**
     * @param Prospect $prospect
     * @param SupplierCategory[] $supplierCategories
     */
    private function notifyClient(Prospect $prospect, $supplierCategories)
    {
        $partnerSupplier = $supplierCategories[0]->getSupplier();
        $emailBuilder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

        $emailBuilder
            ->setTemplateId(29)
            ->addTo($prospect->getEmail(), $prospect->getFirstName())
            ->addVariable('FIRSTNAME', $prospect->getFirstName())
            ->addVariable('SUPPLIER', $partnerSupplier->getDisplayName())
            ->addVariable('ORDER_MODALITY', nl2br($partnerSupplier->getFacadeSupplier()->getOrderModality()))
            ->addVariable('ORDER_ADDITIONAL_ELEMENTS', nl2br($partnerSupplier->getFacadeSupplier()->getOrderAdditionalElements()))
//                ->addVariable('INFORMATIONS', $partnerSupplier->getFacadeSupplier()->getHowToOrder())
        ;

        $mercuriales = [];
        $categories = [];

        foreach ($supplierCategories as $supplierCategory) {
            $categories[] = Supplier::getDisplayCategory($supplierCategory->getCategory());
            $mercuriales[Supplier::getDisplayCategory($supplierCategory->getCategory())] = $this->mercurialeRepository->findOneBy(['category' => $supplierCategory->getCategory(), 'supplier' => $supplierCategory->getSupplier()]);
        }

        $mercurialeUrls = '';
        foreach ($mercuriales as $categorie => $mercuriale) {
            if ($mercuriale !== null) {
                $mercurialeUrls .= sprintf("<a href='%s'>Mercuriale %s</a><br>", $this->baseUrl . $this->uploaderHelper->asset($mercuriale, 'file'), $categorie);
            }
        }

        // TODO: Handle the facade and partner correctly next time...
//        if (!empty($partnerSupplier->getSepaFileName())) {
//            $sepaFile = null;
//            $sepaPath = $this->webDir . $this->uploaderHelper->asset($partnerSupplier,'sepa');
//            $sepaExtension = substr($sepaPath, strrpos($sepaPath, '.') + 1);
//
//            if (file_exists($sepaPath)) {
//                $sepaFile = file_get_contents($sepaPath);
//
//                $emailBuilder->addAttachment("SEPA.$sepaExtension", $sepaFile);
//            }
//        } elseif (!empty($partnerSupplier->getFacadeSupplier()->getSepaFileName())) {
//            $sepaFile = null;
//            $sepaPath = $this->webDir . $this->uploaderHelper->asset($partnerSupplier->getFacadeSupplier(),'sepa');
//            $sepaExtension = substr($sepaPath, strrpos($sepaPath, '.') + 1);
//
//            if (file_exists($sepaPath)) {
//                $sepaFile = file_get_contents($sepaPath);
//
//                $emailBuilder->addAttachment("SEPA.$sepaExtension", $sepaFile);
//            }
//        }

        $emailBuilder->addVariable('CATEGORIES', implode(', ', $categories));
        $emailBuilder->addVariable('MERCURIALE_URLS', $mercurialeUrls);

        // TODO: HACK : We should not send an email to prospect for Jacob at this stage (needs to be redone, in a better way)
//        if ($partnerSupplier->getId() != 37) {
            $this->emailManager->addEmailToQueue($emailBuilder->build());
//        }

        $this->hubspotService->createContactAndGridSentAccountNotOpenedHoppy($prospect);
    }

    private function notifySupplier(Supplier $supplier, Prospect $prospect, $categories)
    {
        $emailBuilder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

        foreach ($supplier->getFacadeSupplier()->getCommercialEmails() as $email) {
            $emailBuilder->addTo($email);
        }

        $emailBuilder->addVariable('RECIPIENTS', $supplier->getFacadeSupplier()->getName());

        $categories = implode(', ', $categories);

        if ($supplier->getId() == 17) {
            $gridName = "Grille 3 {$categories}";
        } elseif ($supplier->getId() == 18) {
            $gridName = "Grille 4 {$categories}";
        } elseif ($supplier->getId() == 23) {
            $gridName = "Grille SP {$categories}";
        } else {
            $gridName = $categories;
        }

        // Send email to supplier
        $emailBuilder
            ->setTemplateId(14)
            ->addVariable('PRICE_GRID', $gridName)
        ;

        $emailBuilder
            ->addVariable('RESTAURANT_NAME', $prospect->getRestaurantName())
            ->addVariable('COMPANY', $prospect->getCompanyName())
            ->addVariable('SIREN', $prospect->getSiren())
            ->addVariable('ADDRESS_FULL', trim("{$prospect->getAddress()} {$prospect->getZipCode()} {$prospect->getCity()}"))
            ->addVariable('MANAGER_NAME', "{$prospect->getFirstName()} {$prospect->getContactName()}")
            ->addVariable('INTERLOCUTOR', "{$prospect->getFirstName()} {$prospect->getContactName()}")
            ->addVariable('TELEPHONE', implode(' - ', array_filter([$prospect->getPhone(), $prospect->getMobile()])))
            ->addVariable('EMAIL', $prospect->getEmail())
            ->addVariable('DELIVERY_HOURS', $prospect->getDeliveryHours())
        ;

        $kbisUrl = $this->uploaderHelper->asset($prospect, 'kbisFile');

        if (!empty($kbisUrl)) {
            $kbisUrl = explode('/', $kbisUrl);
            $kbisUrl[count($kbisUrl) - 1] = rawurlencode($kbisUrl[count($kbisUrl) - 1]);
            $kbisUrl = implode('/', $kbisUrl);

            $emailBuilder->addVariable('KBIS_URL', "{$this->baseUrl}{$kbisUrl}");
        } else {
            $emailBuilder->addVariable('KBIS_URL', "{$this->baseUrl}");
        }

//        $kbis = $prospect->getKbis();
//
//        if (!empty($kbis)) {
//            $kbisFilePath = $this->kbisPath . '/' . $kbis;
//
//            if (file_exists($kbisFilePath)) {
//                $emailBuilder->addAttachment($kbis, file_get_contents($kbisFilePath));
//            }
//        }

        // TODO: HACK : We should not send an email to Domafrais at this stage (needs to be redone, in a better way)
        if ($supplier->getId() != 39) {
            $this->emailManager->addEmailToQueue($emailBuilder->build());
        }
    }

    /**
     * @return bool
     */
    public function isEmailNotificationEnabled(): bool
    {
        return $this->emailNotificationEnabled;
    }

    /**
     * @param bool $emailNotificationEnabled
     */
    public function setEmailNotificationEnabled(bool $emailNotificationEnabled): void
    {
        $this->emailNotificationEnabled = $emailNotificationEnabled;
    }

    /**
     * @param Prospect $prospect
     * @param Supplier[] $suppliers
     */
    public function sendNewlyBoundNotification(Prospect $prospect, $suppliers)
    {
        $supplierDisplay = [];

        foreach ($suppliers as $supplier) {
            $supplierDisplay[] = $supplier->getDisplayName();
        }

        $email = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE)
            ->addTo($prospect->getEmail())
            ->setTemplateId(75)
            ->addVariable('FIRSTNAME', $prospect->getFirstName())
            ->addVariable('SUPPLIERS', implode(', ', $supplierDisplay))
            ->build();

        $this->emailManager->addEmailToQueue($email);
    }
}
