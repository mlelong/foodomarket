<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use AppBundle\Services\RedisCacheManager;
use AppBundle\Entity\ProductPricesStatistics;
use AppBundle\Entity\Import;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductPriceHistory;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\SupplierProductPriceHistoryRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Product\Model\ProductOptionValueInterface;


class ProductUtils
{
    const CHEAPEST = true;
    const MOST_EXPENSIVE = false;

    protected $entityManager;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ProductVariantRepository
     */
    private $productVariantRepository;

    /**
     * @var SupplierProductPriceRepository
     */
    private $priceRepository;

    /**
     * @var SupplierProductPriceHistoryRepository
     */
    private $priceHistoryRepository;

    /**
     * @var SupplierProductVariantInformationsRepository
     */
    private $variantInfoRepository;

    /**
     * @var RedisCacheManager
     */
    private $cacheManager;

    public function __construct(
                                EntityManager $entityManager,
                                RedisCacheManager $cacheManager,
                                ProductRepository $productRepository,
                                ProductVariantRepository $productVariantRepository,
                                SupplierProductPriceRepository $priceRepository,
                                SupplierProductPriceHistoryRepository $priceHistoryRepository,
                                SupplierProductVariantInformationsRepository $variantInfoRepository)
    {
        $this->entityManager = $entityManager;
        $this->cacheManager = $cacheManager;
        $this->productRepository = $productRepository;
        $this->productVariantRepository = $productVariantRepository;
        $this->priceRepository = $priceRepository;
        $this->priceHistoryRepository = $priceHistoryRepository;
        $this->variantInfoRepository = $variantInfoRepository;
    }

    /**
     * @param ProductVariant $variant
     * @param Restaurant $restaurant
     * @param Supplier $supplier
     * @return SupplierProductPrice|null|object
     */
    public function getVariantPrice(ProductVariant $variant, Restaurant $restaurant, Supplier $supplier)
    {
        return $this->priceRepository->findOneBy(['productVariant' => $variant->getId(), 'restaurant' => $restaurant->getId(), 'supplier' => $supplier->getId()]);
    }

    /**
     * @param ProductVariant $variant
     * @return SupplierProductPrice
     */
    public function getCheapestPriceForVariant(ProductVariant $variant)
    {
        /** @var SupplierProductPrice[] $prices */
        $prices = $this->priceRepository->findBy(['productVariant' => $variant->getId()]);
        $price = null;
        $minPrice = PHP_INT_MAX;

        $salesUnit = $variant->getOptionSupplierSalesUnit();

        if ($salesUnit === false) {
            $salesUnit = 'KG';
        } else {
            switch ($salesUnit->getCode()) {
                case 'KG':
                case 'L':
                    $salesUnit = 'KG';
                    break ;
                default:
                    $salesUnit = 'PC';
                    break ;
            }
        }

        foreach ($prices as $candidate) {
            $candidatePrice = $salesUnit == 'KG' ? $candidate->getKgPrice() : $candidate->getUnitPrice();

            if ($minPrice > $candidatePrice) {
                $minPrice = $candidatePrice;
                $price = $candidate;
            }
        }

        return $price;
    }

    /**
     * @param ProductVariant $variant
     * @param Restaurant $restaurant
     * @return SupplierProductPrice
     */
    public function getCheapestPriceForVariantAndRestaurant(ProductVariant $variant, Restaurant $restaurant)
    {
        /** @var SupplierProductPrice[] $prices */
        $prices = $this->priceRepository->findBy(['productVariant' => $variant->getId(), 'restaurant' => $restaurant->getId()]);
        $price = null;
        $minPrice = PHP_INT_MAX;

        $salesUnit = $variant->getOptionSupplierSalesUnit();

        if ($salesUnit === false) {
            $salesUnit = 'KG';
        } else {
            switch ($salesUnit->getCode()) {
                case 'KG':
                case 'L':
                    $salesUnit = 'KG';
                    break ;
                default:
                    $salesUnit = 'PC';
                    break ;
            }
        }

        foreach ($prices as $candidate) {
            $candidatePrice = $salesUnit == 'KG' ? $candidate->getKgPrice() : $candidate->getUnitPrice();

            if ($minPrice > $candidatePrice) {
                $minPrice = $candidatePrice;
                $price = $candidate;
            }
        }

        return $price;
    }

    /**
     * @param ProductVariant $variant
     * @param Restaurant $restaurant
     * @param Supplier $supplier
     * @param $date
     * @return SupplierProductPriceHistory|null
     */
    public function getVariantPriceForDate(ProductVariant $variant, Restaurant $restaurant, Supplier $supplier, $date)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->priceHistoryRepository->createQueryBuilder('s');

        try {
            return $qb
                ->where('s.productVariant = :variant')
                ->andWhere('s.restaurant = :restaurant')
                ->andWhere('s.supplier = :supplier')
                ->andWhere('s.createdAt <= :date')
                ->orderBy('s.createdAt', 'DESC')
                ->setParameter('variant', $variant->getId())
                ->setParameter('restaurant', $restaurant->getId())
                ->setParameter('supplier', $supplier->getId())
                ->setParameter('date', $date)
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param ProductVariant $variant
     * @param Restaurant $restaurant
     * @param Supplier $supplier
     * @param $date
     * @return SupplierProductPriceHistory|null
     */
    public function getEffectiveVariantPriceForDate(ProductVariant $variant, Restaurant $restaurant, Supplier $supplier, $date)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->priceHistoryRepository->createQueryBuilder('s');

        try {
            return $qb
                ->join(Import::class, 'i', 'WITH', 'i.restaurant = :restaurant AND i.supplier = :supplier')
                ->andWhere('i.createdAt <= :date')
                ->andWhere('s.productVariant = :variant')
                ->andWhere('s.restaurant = :restaurant')
                ->andWhere('s.supplier = :supplier')
                ->andWhere('s.createdAt = CONCAT(i.pricesDate, \' 00:00:00\')')
                ->orderBy('s.createdAt', 'DESC')
                ->setParameter('variant', $variant->getId())
                ->setParameter('restaurant', $restaurant->getId())
                ->setParameter('supplier', $supplier->getId())
                ->setParameter('date', $date)
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param ProductVariant $variant
     * @return ArrayCollection
     */
    public function getSimilarVariants(ProductVariant $variant)
    {
        /** @var ProductVariant[] $variants */
        $variants = $this->productVariantRepository->findBy(['product' => $variant->getProduct()->getId(), 'enabled' => true]);
        $similarVariants = new ArrayCollection();
        $isBio = $this->isBio($variant);

        foreach ($variants as $v) {
            if ($this->isBio($v) == $isBio) {
                $similarVariants->add($v);
            }
        }

        if (!$similarVariants->contains($variant)) {
            $similarVariants->add($variant);
        }

        return $similarVariants;
    }

    public function getSimilarVariantsForSupplier(ProductVariant $variant, Supplier $supplier, $activeOnly = false)
    {
        $similarVariants = new ArrayCollection();
        $isBio = $this->isBio($variant);
        $qb = $this->productVariantRepository->createQueryBuilder('variant');

        if ($activeOnly) {
            $qb->join(SupplierProductPrice::class, 'price', 'WITH', 'price.supplier = :supplier AND price.productVariant = variant');
        }

        $variants = $qb
//            ->join(SupplierProductVariantInformations::class, 'info', 'WITH', 'info.supplier = :supplier AND info.product = variant.product AND info.manuallyCreated = 0')
            ->join(SupplierProductVariantInformations::class, 'info', 'WITH', 'info.supplier = :supplier AND info.product = variant.product')
            ->where('variant.product = :product')
            ->setParameter('product', $variant->getProduct()->getId())
            ->setParameter('supplier', $supplier->getId())
            ->getQuery()
            ->getResult()
        ;

        foreach ($variants as $v) {
            if ($this->isBio($v) == $isBio) {
                $similarVariants->add($v);
            }
        }

        if (!$similarVariants->contains($variant)) {
            $similarVariants->add($variant);
        }

        return $similarVariants;
    }

    /**
     * @param ProductVariant $variant
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getCheapestSimilarVariant(ProductVariant $variant, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariant($variant, self::CHEAPEST, $includeManuallyCreated);
    }

    /**
     * @param ProductVariant $variant
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getMostExpensiveSimilarVariant(ProductVariant $variant, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariant($variant, self::MOST_EXPENSIVE, $includeManuallyCreated);
    }

    /**
     * @param ProductVariant $variant
     * @param Supplier|int $supplier
     * @param Restaurant|int $restaurant
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getCheapestSimilarVariantForSupplierAndRestaurant(ProductVariant $variant, $supplier, $restaurant, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariantForSupplierAndRestaurant($variant, $supplier, $restaurant, self::CHEAPEST, $includeManuallyCreated);
    }

    /**
     * @param ProductVariant $variant
     * @param Supplier|int $supplier
     * @param Restaurant|int $restaurant
     * @return SupplierProductPrice
     */
    public function getCheapestSimilarVariantForSupplierAndRestaurantIso(ProductVariant $variant, $supplier, $restaurant)
    {
        /** @var ProductVariant[] $similars */
        $similars = $this->getSimilarVariantsForSupplier($variant, $supplier, true);
        /** @var ProductVariant[] $isoSimilars */
        $isoSimilars = [];

        foreach ($similars as $similar) {
            if ($similar->getId() === $variant->getId()) {
                $isoSimilars[] = $similar;
            } elseif ($variant->getWeight() == $similar->getWeight()) {
                $similarContentQuantity = $similar->getOptionUnitContentQuantity();
                $similarContentMeasurement = $similar->getOptionUnitContentMeasurement();
                $variantContentQuantity = $variant->getOptionUnitContentQuantity();
                $variantContentMeasurement = $variant->getOptionUnitContentMeasurement();

                if (!$similarContentQuantity) {
                    $similarContentQuantity = 1;
                } else {
                    $similarContentQuantity = floatval(str_replace(',', '.', $similarContentQuantity->getValue()));
                }

                if (!$variantContentQuantity) {
                    $variantContentQuantity = 1;
                } else {
                    $variantContentQuantity = floatval(str_replace(',', '.', $variantContentQuantity->getValue()));
                }

                $similarContentMeasurement = $similarContentMeasurement ? $similarContentMeasurement->getValue() : 'KG';
                $variantContentMeasurement = $variantContentMeasurement ? $variantContentMeasurement->getValue() : 'KG';

                // On ramène tout au kilo
                if (in_array($similarContentMeasurement, ['GR', 'G', 'ML'])) {
                    $similarContentQuantity /= 1000;
                }
                if (in_array($variantContentMeasurement, ['GR', 'G', 'ML'])) {
                    $variantContentQuantity /= 1000;
                }
                if (in_array($similarContentMeasurement, ['CL'])) {
                    $similarContentQuantity /= 100;
                }
                if (in_array($variantContentMeasurement, ['CL'])) {
                    $variantContentQuantity /= 100;
                }

                if ($similarContentQuantity == $variantContentQuantity) {
                    $isoSimilars[] = $similar;
                } else {
                    if (in_array($similarContentMeasurement, ['PIECE', 'UNITE', 'KG']) && $similarContentQuantity == 1) {
                        $isoSimilars[] = $similar;
                    }
                }
            }
        }

        /** @var SupplierProductPrice $retainedPrice */
        $retainedPrice = null;

        $suppliers = [$supplier, $supplier->getFacadeSupplier()];

        foreach ($isoSimilars as $isoSimilar) {
            $prices = $this->priceRepository->getRestaurantPrices($suppliers, [$isoSimilar->getProduct()], [$isoSimilar], $restaurant, false);

            $price = !empty($prices) ? $prices[0] : null;

            if ($retainedPrice === null) {
                $retainedPrice = $price;
            } elseif ($price !== null) {
                if ($price->getKgPrice() < $retainedPrice->getKgPrice()) {
                    $retainedPrice = $price;
                }
            }
        }

        return $retainedPrice;
    }

    /**
     * @param ProductVariant $variant
     * @param Supplier|int $supplier
     * @param $restaurant
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getMostExpensiveSimilarVariantForSupplierAndRestaurant(ProductVariant $variant, $supplier, $restaurant, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariantForSupplierAndRestaurant($variant, $supplier, $restaurant, self::MOST_EXPENSIVE, $includeManuallyCreated);
    }

    /**
     * @param ProductVariant $variant
     * @param Supplier|int $supplier
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getCheapestSimilarVariantForSupplier(ProductVariant $variant, $supplier, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariantForSupplier($variant, $supplier, self::CHEAPEST, $includeManuallyCreated);
    }

    /**
     * @param ProductVariant $variant
     * @param Supplier|int $supplier
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getMostExpensiveSimilarVariantForSupplier(ProductVariant $variant, $supplier, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariantForSupplier($variant, $supplier, self::MOST_EXPENSIVE, $includeManuallyCreated);
    }

    /**
     * @param ProductVariant $variant
     * @param Restaurant|int $restaurant
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getCheapestSimilarVariantForRestaurant(ProductVariant $variant, $restaurant, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariantForRestaurant($variant, $restaurant, self::CHEAPEST, $includeManuallyCreated);
    }

    /**
     * @param ProductVariant $variant
     * @param Restaurant|int $restaurant
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice|null
     */
    public function getMostExpensiveSimilarVariantForRestaurant(ProductVariant $variant, $restaurant, bool $includeManuallyCreated = false)
    {
        return $this->getOneSimilarVariantForRestaurant($variant, $restaurant, self::MOST_EXPENSIVE, $includeManuallyCreated);
    }

    private function getOneSimilarVariant(ProductVariant $variant, bool $direction, bool $includeManuallyCreated) {
        $variants = $this->getSimilarVariants($variant);
        $variantIds = [];
        foreach ($variants as $tmp) {
            $variantIds[] = $tmp->getId();
        }

        /** @var QueryBuilder $qb */
        $qb = $this->priceRepository->createQueryBuilder('s');
        /** @var SupplierProductPrice[] $prices */
        $prices = $qb
            ->join(SupplierProductVariantInformations::class, 'i', 'WITH', 'i.productVariant = s.productVariant' . (!$includeManuallyCreated ? ' AND i.manuallyCreated = 0' : ''))
            ->where($qb->expr()->in('s.productVariant', $variantIds))
            ->getQuery()
            ->getResult()
        ;
        $comparePrice = $direction == self::CHEAPEST ? PHP_INT_MAX : PHP_INT_MIN;
        $retainedPrice = null;

        foreach ($prices as $price) {
            $tmpPrice = $price->getKgPrice();
//            $tmpPrice = $price->getUnitPrice();

//            if ($variant->getWeight() !== null && $variant->getWeight() > 0
//                && $price->getProductVariant()->getWeight() !== null && $price->getProductVariant()->getWeight() > 0) {
//                $tmpPrice = $tmpPrice * $variant->getWeight() / $price->getProductVariant()->getWeight();
//            }

            if (($comparePrice > $tmpPrice && $direction == self::CHEAPEST)
                || ($comparePrice < $tmpPrice && $direction == self::MOST_EXPENSIVE)) {
                $comparePrice = $tmpPrice;
                $retainedPrice = $price;
            }
        }

        return $retainedPrice;
    }

    private function getOneSimilarVariantForSupplierAndRestaurant(ProductVariant $variant, $supplier, $restaurant, bool $direction, bool $includeManuallyCreated)
    {
        $variants = $this->getSimilarVariants($variant);
        $variantIds = [];
        foreach ($variants as $tmp) {
            $variantIds[] = $tmp->getId();
        }

        /** @var QueryBuilder $qb */
        $qb = $this->priceRepository->createQueryBuilder('s');
        /** @var SupplierProductPrice[] $prices */
        $prices = $qb
            ->join(SupplierProductVariantInformations::class, 'i', 'WITH', 'i.productVariant = s.productVariant' . (!$includeManuallyCreated ? ' AND i.manuallyCreated = 0' : ''))
            ->where($qb->expr()->in('s.productVariant', $variantIds))
            ->andWhere('s.supplier = :supplier')
            ->andWhere('s.restaurant = :restaurant')
            ->setParameter('supplier', $supplier instanceof Supplier ? $supplier->getId() : $supplier)
            ->setParameter('restaurant', $restaurant instanceof Restaurant ? $restaurant->getId() : $restaurant)
            ->getQuery()
            ->getResult()
        ;
        $minPrice = $direction == self::CHEAPEST ? PHP_INT_MAX : PHP_INT_MIN;
        $retainedPrice = null;

        foreach ($prices as $price) {
            $tmpPrice = $price->getKgPrice();
//            $tmpPrice = $price->getUnitPrice();

//            if ($variant->getWeight() !== null && $variant->getWeight() > 0
//                && $price->getProductVariant()->getWeight() !== null && $price->getProductVariant()->getWeight() > 0) {
//                $tmpPrice = $tmpPrice * $variant->getWeight() / $price->getProductVariant()->getWeight();
//            }

            if (($minPrice > $tmpPrice && $direction == self::CHEAPEST) || ($minPrice < $tmpPrice && $direction == self::MOST_EXPENSIVE)) {
                $minPrice = $tmpPrice;
                $retainedPrice = $price;
            }
        }

        return $retainedPrice;
    }

    private function getOneSimilarVariantForSupplier(ProductVariant $variant, $supplier, bool $direction, bool $includeManuallyCreated)
    {
        $variants = $this->getSimilarVariants($variant);
        $variantIds = [];
        foreach ($variants as $tmp) {
            $variantIds[] = $tmp->getId();
        }

        /** @var QueryBuilder $qb */
        $qb = $this->priceRepository->createQueryBuilder('s');
        /** @var SupplierProductPrice[] $prices */
        $prices = $qb
            ->join(SupplierProductVariantInformations::class, 'i', 'WITH', 'i.productVariant = s.productVariant' . (!$includeManuallyCreated ? ' AND i.manuallyCreated = 0' : ''))
            ->where($qb->expr()->in('s.productVariant', $variantIds))
            ->andWhere('s.supplier = :supplier')
            ->setParameter('supplier', $supplier instanceof Supplier ? $supplier->getId() : $supplier)
            ->getQuery()
            ->getResult()
        ;
        $minPrice = $direction == self::CHEAPEST ? PHP_INT_MAX : PHP_INT_MIN;
        $retainedPrice = null;

        foreach ($prices as $price) {
            $tmpPrice = $price->getKgPrice();
//            $tmpPrice = $price->getUnitPrice();

//            if ($variant->getWeight() !== null && $variant->getWeight() > 0
//                && $price->getProductVariant()->getWeight() !== null && $price->getProductVariant()->getWeight() > 0) {
//                $tmpPrice = $tmpPrice * $variant->getWeight() / $price->getProductVariant()->getWeight();
//            }

            if (($minPrice > $tmpPrice && $direction == self::CHEAPEST) || ($minPrice < $tmpPrice && $direction == self::MOST_EXPENSIVE)) {
                $minPrice = $tmpPrice;
                $retainedPrice = $price;
            }
        }

        return $retainedPrice;
    }

    private function getOneSimilarVariantForRestaurant(ProductVariant $variant, $restaurant, bool $direction, bool $includeManuallyCreated)
    {
        $variants = $this->getSimilarVariants($variant);
        $variantIds = [];
        foreach ($variants as $tmp) {
            $variantIds[] = $tmp->getId();
        }

        /** @var QueryBuilder $qb */
        $qb = $this->priceRepository->createQueryBuilder('s');
        /** @var SupplierProductPrice[] $prices */
        $prices = $qb
            ->join(SupplierProductVariantInformations::class, 'i', 'WITH', 'i.productVariant = s.productVariant' . (!$includeManuallyCreated ? ' AND i.manuallyCreated = 0' : ''))
            ->where($qb->expr()->in('s.productVariant', $variantIds))
            ->andWhere('s.restaurant = :restaurant')
            ->setParameter('restaurant', $restaurant instanceof Restaurant ? $restaurant->getId() : $restaurant)
            ->getQuery()
            ->getResult()
        ;
        $minPrice = $direction == self::CHEAPEST ? PHP_INT_MAX : PHP_INT_MIN;
        $retainedPrice = null;

        foreach ($prices as $price) {
            $tmpPrice = $price->getKgPrice();
//            $tmpPrice = $price->getUnitPrice();

//            if ($variant->getWeight() !== null && $variant->getWeight() > 0
//                && $price->getProductVariant()->getWeight() !== null && $price->getProductVariant()->getWeight() > 0) {
//                $tmpPrice = $tmpPrice * $variant->getWeight() / $price->getProductVariant()->getWeight();
//            }

            if (($minPrice > $tmpPrice && $direction == self::CHEAPEST) || ($minPrice < $tmpPrice && $direction == self::MOST_EXPENSIVE)) {
                $minPrice = $tmpPrice;
                $retainedPrice = $price;
            }
        }

        return $retainedPrice;
    }

    /**
     * @param ProductVariant $productVariant
     * @return bool
     */
    private function isBio(ProductVariant $productVariant) {
        /** @var ProductOptionValueInterface $biOptionValue */
        $biOptionValue = $productVariant->getOptionValuesByOptionCode('BI');

        if (!$biOptionValue instanceof ProductOptionValueInterface)
            return false;

        $isBio = $biOptionValue->getValue();

        if ($isBio == 'NON' || $isBio == 'NOBIO')
            return false;

        return $isBio;
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getProductStatistics($productId = null, $variantId = null, $refreshCache = false, $flush = true)
    {

        if (is_null($productId) && !is_null($variantId)) {
            $variant = $this->productVariantRepository->findOneBy(['id' => $variantId]);
            $product = $variant->getProduct();
            $productId = $product->getId();
        }

        $cacheKey = RedisCacheManager::CACHE_KEY_PREFIX_PRODUCT_STATISTICS . $productId;

        if (!$refreshCache) {

            $stats = $this->cacheManager->get($cacheKey);
            if (!is_null($stats)) {
                return $stats;
            }
        }

        $currentPeriodEnd = new \DateTime();
        $currentPeriodBegin = new \DateTime();
        $currentPeriodBegin->modify('-8 day');
        $oneYearBegin = new \DateTime();
        $oneYearBegin->modify('-360 day');


        $lastMonthPeriodEnd = new \DateTime();
        $lastMonthPeriodBegin = new \DateTime();
        $lastMonthPeriodEnd->modify('-25 day');
        $lastMonthPeriodBegin->modify('-35 day');

        $statistics = [];
        $statistics['bio'] = [
            'averageKgPrice' => 0,
            'averageUnitPrice' => 0,
            'previousPeriodAverageKgPrice' => 0,
            'pricePercentEvolution' => 0,
            'minimumKgPrice' => 999,
            'maximumKgPrice' => 0,
            'minimumUnitPrice' => 999,
            'maximumUnitPrice' => 0,

        ];

        $statistics['nobio'] = [
            'averageKgPrice' => 0,
            'averageUnitPrice' => 0,
            'previousPeriodAverageKgPrice' => 0,
            'pricePercentEvolution' => 0,
            'minimumKgPrice' => 999,
            'maximumKgPrice' => 0,
            'minimumUnitPrice' => 999,
            'maximumUnitPrice' => 0,
        ];

        // Prices about current week
        $qb = $this->priceHistoryRepository->createQueryBuilder('history');
        $currentOtherPrices = $qb
            ->andwhere('history.product = :product')
            ->andWhere('history.createdAt BETWEEN :date_start AND :date_end')
            ->andWhere('history.unitPrice > 0')
            ->setParameter('product', $productId)
            ->setParameter('date_start', $currentPeriodBegin)
            ->setParameter('date_end', $currentPeriodEnd)
            ->getQuery()
            ->getResult();

        // Prices about one current year
        if (count($currentOtherPrices) == 0) {
            $currentOtherPrices = $qb
                ->andwhere('history.product = :product')
                ->andWhere('history.createdAt BETWEEN :date_start AND :date_end')
                ->andWhere('history.unitPrice > 0')
                ->setParameter('product', $productId)
                ->setParameter('date_start', $oneYearBegin)
                ->setParameter('date_end', $currentPeriodEnd)
                ->getQuery()
                ->getResult();
        }

        $totalNoBioKgPrice = 0;
        $totalBioKgPrice = 0;
        $countNoBioKgPrices = 0;
        $countBioKgPrices = 0;

        $totalNoBioUnitPrice = 0;
        $totalBioUnitPrice = 0;
        $countNoBioUnitPrices = 0;
        $countBioUnitPrices = 0;

        foreach ($currentOtherPrices as $entry) {

            $kgPrice = bcdiv($entry->getKgPrice(), 100, 2);

            if ($entry->getProductVariant()->getOptionBio() && $entry->getProductVariant()->getOptionBio()->getCode() == 'BIO') {
                $totalBioKgPrice += $kgPrice;
                $countBioKgPrices++;
                if ($statistics['bio']['maximumKgPrice'] < $kgPrice) {
                    $statistics['bio']['maximumKgPrice'] = $kgPrice;
                }
                if ($statistics['bio']['minimumKgPrice'] > $kgPrice) {
                    $statistics['bio']['minimumKgPrice'] = $kgPrice;
                }
            } else {
                $totalNoBioKgPrice += $kgPrice;
                $countNoBioKgPrices++;
                if ($statistics['nobio']['maximumKgPrice'] < $kgPrice) {
                    $statistics['nobio']['maximumKgPrice'] = $kgPrice;
                }
                if ($statistics['nobio']['minimumKgPrice'] > $kgPrice) {
                    $statistics['nobio']['minimumKgPrice'] = $kgPrice;
                }
            }

            if ($entry->getProductVariant()->getOptionSupplierSalesUnit()->getCode() == 'PC') {

                $unitUnitPrice = bcdiv(bcdiv($entry->getUnitPrice(), $entry->getProductVariant()->getOptionUnitQuantity()->getValue()), 100, 2);

                if ($entry->getProductVariant()->getOptionBio() && $entry->getProductVariant()->getOptionBio()->getCode() == 'BIO') {
                    $totalBioUnitPrice += $unitUnitPrice;
                    $countBioUnitPrices++;
                    if ($statistics['bio']['maximumUnitPrice'] < $unitUnitPrice) {
                        $statistics['bio']['maximumUnitPrice'] = $unitUnitPrice;
                    }
                    if ($statistics['bio']['minimumUnitPrice'] > $unitUnitPrice) {
                        $statistics['bio']['minimumUnitPrice'] = $unitUnitPrice;
                    }
                } else {
                    $totalNoBioUnitPrice += $unitUnitPrice;
                    $countNoBioUnitPrices++;
                    if ($statistics['nobio']['maximumUnitPrice'] < $unitUnitPrice) {
                        $statistics['nobio']['maximumUnitPrice'] = $unitUnitPrice;
                    }
                    if ($statistics['nobio']['minimumUnitPrice'] > $unitUnitPrice) {
                        $statistics['nobio']['minimumUnitPrice'] = $unitUnitPrice;
                    }
                }

            }
        }

        if ($countBioKgPrices) {
            $statistics['bio']['averageKgPrice'] = bcdiv($totalBioKgPrice, $countBioKgPrices, 2);
        }
        if ($countNoBioKgPrices) {
            $statistics['nobio']['averageKgPrice'] = bcdiv($totalNoBioKgPrice, $countNoBioKgPrices, 2);
        }

        if ($countBioUnitPrices) {
            $statistics['bio']['averageUnitPrice'] = bcdiv($totalBioUnitPrice, $countBioUnitPrices, 2);
        }
        if ($countNoBioUnitPrices) {
            $statistics['nobio']['averageUnitPrice'] = bcdiv($totalNoBioUnitPrice, $countNoBioUnitPrices, 2);
        }

        // Prices about last month
        $qb = $this->priceHistoryRepository->createQueryBuilder('history');
        $oldOtherPrices = $qb
            ->andwhere('history.product = :product')
            ->andWhere('history.createdAt BETWEEN :date_start AND :date_end')
            ->andWhere('history.unitPrice > 0')
            ->setParameter('product', $productId)
            ->setParameter('date_start', $lastMonthPeriodBegin)
            ->setParameter('date_end', $lastMonthPeriodEnd)
            ->getQuery()
            ->getResult();

        // Prices about the whole year
        if (count($oldOtherPrices) == 0) {
            $oldOtherPrices = $qb
                ->andwhere('history.product = :product')
                ->andWhere('history.createdAt BETWEEN :date_start AND :date_end')
                ->andWhere('history.unitPrice > 0')
                ->setParameter('product', $productId)
                ->setParameter('date_start', $oneYearBegin)
                ->setParameter('date_end', $lastMonthPeriodEnd)
                ->getQuery()
                ->getResult();
        }

        $totalNoBioKgPrice = 0;
        $totalBioKgPrice = 0;
        $countNoBioKgPrices = 0;
        $countBioKgPrices = 0;

        foreach ($oldOtherPrices as $entry) {

            $kgPrice = bcdiv($entry->getKgPrice(), 100, 2);

            if ($entry->getProductVariant()->getOptionBio() && $entry->getProductVariant()->getOptionBio()->getCode() == 'BIO') {
                $totalBioKgPrice += $kgPrice;
                $countBioKgPrices++;
            } else {
                $totalNoBioKgPrice += $kgPrice;
                $countNoBioKgPrices++;
            }
        }

        if ($countBioKgPrices) {
            $statistics['bio']['previousPeriodAverageKgPrice'] = bcdiv($totalBioKgPrice, $countBioKgPrices, 2);
        }

        if ($countNoBioKgPrices) {
            $statistics['nobio']['previousPeriodAverageKgPrice'] = bcdiv($totalNoBioKgPrice, $countNoBioKgPrices, 2);
        }

        if ($statistics['bio']['previousPeriodAverageKgPrice'] && $statistics['bio']['previousPeriodAverageKgPrice'] > 0) {
            $statistics['bio']['pricePercentEvolution'] = bcsub(bcdiv(bcmul(100, $statistics['bio']['averageKgPrice']), $statistics['bio']['previousPeriodAverageKgPrice'], 2), 100, 2);
        }

        if ($statistics['nobio']['previousPeriodAverageKgPrice'] && $statistics['nobio']['previousPeriodAverageKgPrice'] > 0) {
            $statistics['nobio']['pricePercentEvolution'] = bcsub(bcdiv(bcmul(100, $statistics['nobio']['averageKgPrice']), $statistics['nobio']['previousPeriodAverageKgPrice'], 2), 100, 2);
        }

        // if stats are null, put some clean values
        // KG
        if ($statistics['bio']['minimumKgPrice'] == 999) {
            $statistics['bio']['minimumKgPrice'] = $statistics['bio']['averageKgPrice'];
        }
        if ($statistics['nobio']['minimumKgPrice'] == 999) {
            $statistics['nobio']['minimumKgPrice'] = $statistics['nobio']['averageKgPrice'];
        }
        if ($statistics['bio']['maximumKgPrice'] == 0) {
            $statistics['bio']['maximumKgPrice'] = $statistics['bio']['averageKgPrice'];
        }
        if ($statistics['nobio']['maximumKgPrice'] == 0) {
            $statistics['nobio']['maximumKgPrice'] = $statistics['nobio']['averageKgPrice'];
        }

        // PC
        if ($statistics['bio']['minimumUnitPrice'] == 999) {
            $statistics['bio']['minimumUnitPrice'] = $statistics['bio']['averageUnitPrice'];
        }
        if ($statistics['nobio']['minimumUnitPrice'] == 999) {
            $statistics['nobio']['minimumUnitPrice'] = $statistics['nobio']['averageUnitPrice'];
        }
        if ($statistics['bio']['maximumUnitPrice'] == 0) {
            $statistics['bio']['maximumUnitPrice'] = $statistics['bio']['averageUnitPrice'];
        }
        if ($statistics['nobio']['maximumUnitPrice'] == 0) {
            $statistics['nobio']['maximumUnitPrice'] = $statistics['nobio']['averageUnitPrice'];
        }

        $this->cacheManager->set($cacheKey, $statistics);

        if (!is_null($productId)) {
            $product = $this->productRepository->findOneBy(['id' => $productId]);
        }

        $productPricesStatisticsBio = new ProductPricesStatistics();
        $productPricesStatisticsBio->setProduct($product)
            ->setIsBio(1)
            ->setAverageKgPrice($statistics['bio']['averageKgPrice'])
            ->setMinimumKgPrice($statistics['bio']['minimumKgPrice'])
            ->setMaximumKgPrice($statistics['bio']['maximumKgPrice'])
            ->setAverageUnitPrice($statistics['bio']['averageUnitPrice'])
            ->setMinimumUnitPrice($statistics['bio']['minimumUnitPrice'])
            ->setMaximumUnitPrice($statistics['bio']['maximumUnitPrice']);

        $this->entityManager->persist($productPricesStatisticsBio);

        $productPricesStatisticsNoBio = new ProductPricesStatistics();
        $productPricesStatisticsNoBio->setProduct($product)
            ->setIsBio(0)
            ->setAverageKgPrice($statistics['nobio']['averageKgPrice'])
            ->setMinimumKgPrice($statistics['nobio']['minimumKgPrice'])
            ->setMaximumKgPrice($statistics['nobio']['maximumKgPrice'])
            ->setAverageUnitPrice($statistics['nobio']['averageUnitPrice'])
            ->setMinimumUnitPrice($statistics['nobio']['minimumUnitPrice'])
            ->setMaximumUnitPrice($statistics['nobio']['maximumUnitPrice']);

        $this->entityManager->persist($productPricesStatisticsNoBio);

        if ($flush) {
            $this->entityManager->flush();
        }

        return $statistics;

    }


}