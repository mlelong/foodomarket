<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\SupplierProductPrice;

class ProductVariantUtils
{
    public static function getNameForVariant(ProductVariant $variant)
    {
        return self::getName(
            $variant->getProduct()->getName(),
            self::getOptionValue($variant->getOptionCaliber()),
            self::getOptionValue($variant->getOptionCategory()),
            self::getOptionValue($variant->getOptionVariety()),
            self::getOptionValue($variant->getOptionValuesByOptionCode('MQ')),
            self::getOptionValue($variant->getOptionBio()),
            self::getOptionValue($variant->getOptionValuesByOptionCode('AP'))
        );
    }

    public static function getExplicitContentForVariant(ProductVariant $variant)
    {
        return self::getExplicitContent(
            self::getOptionValue($variant->getOptionOrderUnit()),
            self::getOptionValue($variant->getOptionConditionning()),
            self::getOptionValue($variant->getOptionUnitQuantity(), 1),
            self::getOptionValue($variant->getOptionUnitContentQuantity(), 1),
            self::getOptionValue($variant->getOptionUnitContentMeasurement())
        );
    }

    public static function getPriceDetailFromSupplierProductPrice(SupplierProductPrice $supplierProductPrice)
    {
        $variant = $supplierProductPrice->getProductVariant();

        return self::getPriceDetail(
            self::getOptionValue($variant->getOptionOrderUnit()),
            self::getOptionValue($variant->getOptionSupplierSalesUnit()),
            self::getOptionValue($variant->getOptionUnitQuantity(), 1),
            self::getOptionValue($variant->getOptionConditionning()),
            $variant->getWeight() !== null ? $variant->getWeight() : 1,
            $supplierProductPrice->getKgPrice(),
            $supplierProductPrice->getUnitPrice()
        );
    }

    public static function getName(
        string $productName,
        string $caliber,
        string $category,
        string $variety,
        string $brand,
        string $bio,
        string $appellation
    ) {
        if ($bio != 'NON') {
            $productName .= " $bio";
        }

        if ($caliber && stripos($productName, ' CAL ') === false) {
            if (stripos($caliber, 'CAL') === false) {
                $productName .= " CAL";
            }

            if (stripos($productName, $caliber) === false) {
                $productName .= " $caliber";
            }
        }

        if ($brand) {
            if (stripos($productName, $brand) === false) {
                $productName .= " $brand";
            }
        }

        if ($variety) {
            if (stripos($productName, $variety) === false) {
                $productName .= " $variety";
            }
        }

        if ($category) {
            if (stripos($category, 'CAT') === false) {
                $productName .= " CAT";
            }
            $productName .= " $category";
        }

        if ($appellation) {
            if (stripos($productName, $appellation) === false) {
                $productName .= " $appellation";
            }
        }

        return trim($productName);
    }

    /**
     * This method returns the explicit content based on inputs
     *
     * @param string $orderUnit
     * @param string $conditionning
     * @param string $unitQuantity
     * @param string $unitContentQuantity
     * @param string $unitContentMeasurement
     * @return string
     */
    public static function getExplicitContent(
        string $orderUnit = 'KG',
        string $conditionning = '',
        string $unitQuantity = '1',
        string $unitContentQuantity = '1',
        string $unitContentMeasurement = 'KG')
    {
        $explicitContent = '';

        if ($orderUnit === 'PC') {
            switch ($conditionning) {
                case '':
                case 'VRAC':
                    if ($unitQuantity > 1) {
                        $explicitContent = '[unitQuantity]x[unitContentQuantity] [unitContentMeasurement]';
                    } elseif ($unitQuantity == 1) {
                        $explicitContent = '[unitContentQuantity] [unitContentMeasurement]';
                    }
                    if ($unitQuantity > 1) {
                        $explicitContent = '[unitQuantity]x[unitContentQuantity] [unitContentMeasurement]';
                    } elseif ($unitQuantity == 1) {
                        $explicitContent = '[unitContentQuantity] [unitContentMeasurement]';
                    }
                    break ;
                default:
                    if ($unitQuantity > 1) {
                        $explicitContent = '[conditionning] de [unitQuantity]x[unitContentQuantity] [unitContentMeasurement]';
                    } elseif ($unitContentQuantity == 1 && in_array(strtoupper($unitContentMeasurement), ['UNITE', 'PIECE', 'UNITES', 'PIECES'])) {
                        $explicitContent = '[unitQuantity] [conditionning]';
                    } else {
                        if (!in_array(strtoupper($unitContentMeasurement), ['UNITE', 'PIECE', 'UNITES', 'PIECES'])) {
                            $explicitContent = '[conditionning] de [unitContentQuantity] [unitContentMeasurement]';
                        } else {
                            $explicitContent = '[conditionning] de [unitContentQuantity]';
                        }
                    }
                    break ;
            }
        } else if ($orderUnit === 'COLIS') {
            if ($unitQuantity == 1) {
                if ($conditionning === '') {
                    $explicitContent = '[unitContentQuantity] [unitContentMeasurement]';
                } else {
                    if (in_array(strtoupper($unitContentMeasurement), ['UNITE', 'PIECE', 'UNITES', 'PIECES'])) {
                        $explicitContent = '[unitContentQuantity] [conditionning]';
                    } else {
                        $explicitContent = '[conditionning] de [unitContentQuantity] [unitContentMeasurement]';
                    }
                }
            } elseif ($unitQuantity > 1) {
                if ($conditionning === '') {
                    $explicitContent = 'Colis de [unitQuantity]x[unitContentQuantity] [unitContentMeasurement]';
                } elseif (strtoupper($conditionning) !== 'COLIS') {
                    if (in_array(strtoupper($unitContentMeasurement), ['UNITE', 'PIECE', 'UNITES', 'PIECES']) && $unitContentQuantity > 1) {
                        $explicitContent = 'Colis de [unitQuantity]x[unitContentQuantity] [conditionning]';
                    } elseif (in_array(strtoupper($unitContentMeasurement), ['UNITE', 'PIECE', 'UNITES', 'PIECES']) && $unitContentQuantity == 1) {
                        $explicitContent = 'Colis de [unitQuantity] [conditionning]';
                    } else {
                        $explicitContent = 'Colis de [unitQuantity] [conditionning] de [unitContentQuantity] [unitContentMeasurement]';
                    }
                } elseif ($unitContentQuantity > 1) {
                    $explicitContent = 'Colis de [unitQuantity]x[unitContentQuantity] [unitContentMeasurement]';
                } elseif (!in_array(strtoupper($unitContentMeasurement), ['UNITE', 'PIECE', 'UNITES', 'PIECES'])) {
                    $explicitContent = 'Colis de [unitQuantity] [unitContentMeasurement]';
                } else {
                    $explicitContent = 'Colis de [unitQuantity]';
                }
            }
        }

        $explicitContent = str_replace(
            [
                '[unitQuantity]',
                '[conditionning]',
                '[unitContentQuantity]',
                '[unitContentMeasurement]'
            ],
            [
                $unitQuantity,
                $conditionning,
                $unitContentQuantity,
                $unitContentMeasurement
            ],
            $explicitContent
        );

        $explicitContent = explode(' ', $explicitContent);
        $pluralizeNextWord = false;

        foreach ($explicitContent as &$word) {
            $wordLength = strlen($word);

            if ($wordLength === 1) {
                $word = strtoupper($word);
            } else {
                $word = ucfirst(strtolower($word));
            }

            if (in_array($word, ['De', 'Ml', 'Gr', 'Cl'])) {
                $word = strtolower($word);
            } else if (is_numeric($word) && $word > 1) {
                $pluralizeNextWord = true;
            } else if ($pluralizeNextWord && $wordLength > 2 && substr($word, -1) !== 's') {
                if(substr($word, -3) === 'eau' || substr($word, -2) === 'au') {
                    $word .= 'x';
                }
                else {
                    $word .= 's';
                }
                $pluralizeNextWord = false;
            }
        }

        $explicitContent = implode(' ', $explicitContent);
        $explicitContent = str_replace('x1 ', ' ', $explicitContent);
        $explicitContent = str_replace('Piece', 'Pièce', $explicitContent);
        $explicitContent = str_replace('Pièce de 1 Pièce', 'Pièce', $explicitContent);

        return $explicitContent;
    }

    public static function getPriceDetail(
        string $orderUnit = 'KG',
        string $salesUnit = 'KG',
        int $unitQuantity = 1,
        string $conditionning = '',
        float $weight = 1.0,
        int $kgPrice = 0,
        int $unitPrice = 0)
    {
        $priceDetail = '';
        $unit = '';

        switch ($orderUnit) {
            case 'L':
            case 'ORDER_L':
                $price = bcdiv($kgPrice, 100, 2);
                $unit = 'L';
                break ;
            case 'PC':
            case 'ORDER_PC':
                $price = bcdiv($unitPrice, 100, 2);

                // TODO : Rule REMOVED, so that kg price is displayed everytime
                // if ($weight != 1) {
                    $pricePerUnit = bcdiv($kgPrice, 100, 2);
                    $priceDetail = " (soit {$pricePerUnit}€/Kg)";
                //}

                break ;
            case 'CO':
            case 'ORDER_CO':
            case 'COLIS':
            case 'ORDER_COLIS':
                $price = bcdiv($unitPrice, 100, 2);

                if ($salesUnit === 'PC') {
                    $pricePerUnit = bcdiv($unitPrice / $unitQuantity, 100, 2);

                    if (!empty($conditionning) && strtoupper($conditionning) !== 'COLIS') {
                        $unitOfPricePerUnit = ucfirst(strtolower($conditionning));
                    } else {
                        $unitOfPricePerUnit = 'Pièce';
                    }
                } else if ($salesUnit === 'L') {
                    $pricePerUnit = bcdiv($kgPrice, 100, 2);
                    $unitOfPricePerUnit = 'L';
                } else {
                    $pricePerUnit = bcdiv($kgPrice, 100, 2);
                    $unitOfPricePerUnit = 'Kg';
                }

                $priceDetail = " (soit {$pricePerUnit}€/{$unitOfPricePerUnit})";
                break ;
            case 'KGPC':
            case 'ORDER_KGPC':
                $price = bcdiv($unitPrice, 100, 2);

                // TODO : Rule REMOVED, so that kg price is displayed everytime
                // if ($weight != 1) {
                    $pricePerUnit = bcdiv($kgPrice, 100, 2);
                    $priceDetail = " (soit {$pricePerUnit}€/Kg)";
                //}
                break ;
            case 'KG':
            case 'ORDER_KG':
            default:
                $price = bcdiv($kgPrice, 100, 2);
                $unit = 'kg';
                break ;
        }

        return [
            'price' => $price,
            'unit' => $unit,
            'detail' => $priceDetail,
            'full' => "{$price}€" . (!empty($unit) ? "/$unit" : '') . $priceDetail
        ];
    }

    /**
     * @param ProductOptionValue|false $optionValue
     * @param string $default
     * @return string
     */
    private static function getOptionValue($optionValue, $default = '')
    {
        if (!$optionValue) {
            return $default;
        }

        return !empty($optionValue->getValue()) ? $optionValue->getValue() : $default;
    }

    /**
     * @param ProductOptionValue|false $optionValue
     * @return string
     */
    private static function getOptionCode($optionValue)
    {
        if (!$optionValue) {
            return '';
        }

        return $optionValue->getCode();
    }
}