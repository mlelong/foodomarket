<?php

namespace AppBundle\Services\Shipping\Calculator;

use AppBundle\Entity\Order;
use AppBundle\Entity\Prospect;
use AppBundle\Util\SupplierUtil;
use Sylius\Component\Core\Model\Shipment;
use Sylius\Component\Shipping\Calculator\CalculatorInterface;
use Sylius\Component\Shipping\Model\ShipmentInterface;

class SupplierShippingCalculator implements CalculatorInterface
{
    /**
     * @param ShipmentInterface $subject
     * @param array $configuration
     *
     * @return int
     */
    public function calculate(ShipmentInterface $subject, array $configuration): int
    {
        if ($subject instanceof Shipment) {
            /** @var Order $order */
            $order = $subject->getOrder();
            /** @var Prospect $prospect */
            $prospect = $order->getCustomer()->getProspect();
            $supplier = $order->getShoppingCart()->getSupplier();

            if ($prospect->getType() === Prospect::TYPE_INDIVIDUAL) {
                $deliveryCost = (int)(SupplierUtil::getDeliveryCost($supplier, $prospect->getZipCode()) * 100);

                if (in_array($supplier->getId(), [79, 83]) && $deliveryCost > 0) {
                    $totalWeight = $order->getTotalWeight();
                    $nbPackages = (int)ceil($totalWeight / 25);

                    return ($nbPackages * $deliveryCost);
                }

                return 0;
            } else {
                $shoppingCart = $order->getShoppingCart();

                if ($shoppingCart !== null) {
                    $supplier = $shoppingCart->getSupplier();

                    if ($supplier->getEnabled()) {
                        $supplier = $supplier->getPartnerSuppliers()->first();
                    }

                    $total = floatval(bcdiv($order->getItemsTotal(), 100, 2));

                    return $total >= $supplier->getFreePort() ? 0 : intval(bcmul($supplier->getShippingCost(), 100));
                }
            }
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'supplier';
    }
}