<?php

namespace AppBundle\Services;

use Vaites\ApacheTika\Clients\CLIClient;

class ApacheTikaClient extends CLIClient
{
    protected function getArguments($type, $file = null)
    {
        $arguments = parent::getArguments($type, $file);

        // Force utf8 output
        $arguments[] = '--encoding=utf8';

        return $arguments;
    }
}
