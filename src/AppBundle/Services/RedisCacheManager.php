<?php

namespace AppBundle\Services;

use Predis\Client;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class RedisCacheManager
{
    /**
     * Cache Key Prefixes (general, shop, front, onboarding and admin)
     */
    const CACHE_KEY_PREFIX                              = 'foodomarket-';
    const CACHE_KEY_PREFIX_SHOP                         = self::CACHE_KEY_PREFIX . 'middle-shop-';
    const CACHE_KEY_PREFIX_FRONT                        = self::CACHE_KEY_PREFIX . 'front-';
    const CACHE_KEY_PREFIX_ONBOARDING                   = self::CACHE_KEY_PREFIX . 'onboarding-';
    const CACHE_KEY_PREFIX_PRODUCT_STATISTICS           = self::CACHE_KEY_PREFIX . 'products-statistics-';
    const CACHE_KEY_PREFIX_ADMIN                        = self::CACHE_KEY_PREFIX . 'admin-';

    /**
     * Cache Key Patterns for the shop
     */
    // Cache key for the restaurant suppliers in the logged shop (/commander)
    const CACHE_KEY_SHOP_USER_SUPPLIERS                 = self::CACHE_KEY_PREFIX_SHOP . 'suppliers-restaurant-{restaurantId}';
    // Cache key for the products of a restaurant for a supplier
    const CACHE_KEY_SHOP_USER_SUPPLIER_PRODUCTS         = self::CACHE_KEY_PREFIX_SHOP . 'products-supplier-{supplierId}-restaurant-{restaurantId}';
    // Cache key for all products of a restaurant (Tous les fournisseurs)
    const CACHE_KEY_SHOP_USER_PRODUCTS                  = self::CACHE_KEY_PREFIX_SHOP . 'products-restaurant-{restaurantId}';
    // Cache key for shopping list information (list + items)
    const CACHE_KEY_SHOP_USER_SHOPPING_LISTS            = self::CACHE_KEY_PREFIX_SHOP . 'shopping-lists-restaurant-{restaurantId}';

    // Cache key for the variant detail (by supplier and restaurant)
    const CACHE_KEY_SHOP_VARIANT                        = self::CACHE_KEY_PREFIX_SHOP . 'variant-{variantId}-supplier-{supplierId}-restaurant-{restaurantId}';

    // Cache key for the product listing by restaurant
    const CACHE_KEY_SHOP_CATALOG_PRODUCTS               = self::CACHE_KEY_PREFIX_SHOP . 'catalog-restaurant-{restaurantId}-taxon-{taxonId}-products';
    // Cache key for a supplier catalog (unused for the moment)
    const CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCTS      = self::CACHE_KEY_PREFIX_SHOP . 'catalog-supplier-{supplierId}-products';
    // Cache key for the search page
    const CACHE_KEY_SHOP_CATALOG_SEARCH_PRODUCTS        = self::CACHE_KEY_PREFIX_SHOP . 'catalog-search-{terms}-products';
    // Cache key for the product and filters for a single product (never used)
    const CACHE_KEY_SHOP_CATALOG_PRODUCT                = self::CACHE_KEY_PREFIX_SHOP . 'catalog-product-{productId}';
    // Cache key for the product and filters for a single product and some suppliers (foodomarket partners or restaurant partners + facades)
    const CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCT       = self::CACHE_KEY_PREFIX_SHOP . 'catalog-supplier-{supplierId}-product-{productId}-restaurant-{restaurantId}';
    const CACHE_KEY_SHOP_CATALOG_VARIANT_PATTERN        = self::CACHE_KEY_PREFIX_SHOP . 'catalog-supplier-{supplierId}-variant-{variantId}';
    const CACHE_KEY_SHOP_CATALOG_PRODUCTS_SLUGS         = self::CACHE_KEY_PREFIX_SHOP . 'catalog-supplier-{supplierId}-products-slugs';
    const CACHE_KEY_SHOP_CATALOG_TAXONS_SLUGS           = self::CACHE_KEY_PREFIX_SHOP . 'catalog-supplier-{supplierId}-taxons-slugs';
    const CACHE_KEY_SHOP_CATALOG_TAXONS_TREE            = self::CACHE_KEY_PREFIX_SHOP . 'catalog-supplier-{supplierId}-taxons-tree';

    /**
     * Cache Keys for the sendinblue manager
     */
    const CACHE_KEY_SENDINBLUE_TEMPLATES                = self::CACHE_KEY_PREFIX_FRONT . 'sendinblue-templates';

    /**
     * Cache Keys for the front
     */
    const CACHE_KEY_FOOD_TYPES                          = self::CACHE_KEY_PREFIX_FRONT . 'food-types';

    /**
     * Cache Key Patterns for the front
     */
    const CACHE_KEY_FRONT_PRICES_PATTERN                = self::CACHE_KEY_PREFIX_FRONT . 'prices-{category}-{bio}';
    const CACHE_KEY_FRONT_MERCURIAL_PATTERN             = self::CACHE_KEY_PREFIX_FRONT . 'prices-{supplierId}-all';

    /**
     * Cache Key Patterns for onboarding component
     */
    const CACHE_KEY_ONBOARDING_PRODUCTS_PATTERN         = self::CACHE_KEY_PREFIX_ONBOARDING . 'products-{category}';

    /**
     * Cache Key Patterns for admin
     */
    const CACHE_KEY_ADMIN_OPTIM_PRODUCTS_PATTERN        = self::CACHE_KEY_PREFIX_ADMIN . 'optim-{restaurantId}-products';
    const CACHE_KEY_ADMIN_TAXONS                        = self::CACHE_KEY_PREFIX_ADMIN . 'taxons';
    const CACHE_KEY_ADMIN_PRODUCTS                      = self::CACHE_KEY_PREFIX_ADMIN . 'products';
    const CACHE_KEY_ADMIN_PRODUCT_OPTION_VALUES         = self::CACHE_KEY_PREFIX_ADMIN . 'option-{code}-values';
    const CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_LIST         = self::CACHE_KEY_PREFIX_ADMIN . 'customer-success-list';
    const CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_ITEM         = self::CACHE_KEY_PREFIX_ADMIN . 'customer-success-{prospectId}';

    const CACHE_KEY_ADMIN_PRODUCT_MATCHING_LIST         = self::CACHE_KEY_PREFIX_ADMIN . 'product-matching-{supplierId}-list';

    /** @var Client */
    private $predisClient;

    /** @var RedisAdapter */
    private $adapter;

    /** @var string */
    private $redisNamespace;

    /**
     * RedisCacheManager constructor.
     * @param Client $predisClient
     * @param string $redisNamespace
     */
    public function __construct(Client $predisClient, string $redisNamespace)
    {
        $this->predisClient = $predisClient;
        $this->adapter = new RedisAdapter($this->predisClient);
        $this->redisNamespace = $redisNamespace;
    }

    /**
     * @param string $pattern
     * @return string[]
     */
    public function getKeys(string $pattern)
    {
        return $this->predisClient->keys("{$this->redisNamespace}$pattern");
    }

    /**
     * @return string
     */
    public function getRedisNamespace(): string
    {
        return $this->redisNamespace;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->predisClient;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function exists(string $key): bool
    {
        try {
            $item = $this->adapter->getItem("{$this->redisNamespace}$key");

            return $item->isHit();
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * @param string $key
     * @param null $expiresAfter
     * @param null|callable $callable
     * @return mixed|null
     */
    public function get(string $key, $expiresAfter = null, callable $callable = null)
    {
        try {
            $item = $this->adapter->getItem("{$this->redisNamespace}$key");

            if ($item->isHit()) {
                return $item->get();
            }

            if ($callable !== null) {
                $data = $callable();
                $item->set($data);

                if ($expiresAfter !== null) {
                    $item->expiresAfter($expiresAfter);
                }

                $this->adapter->save($item);

                return $data;
            }

            return null;
        } catch (InvalidArgumentException $e) {
            return null;
        }
    }

    /**
     * @param string $key The key
     * @param mixed $what What to store
     * @param null|int $expiresAfter Seconds of validity (or forever)
     * @return mixed|bool
     */
    public function set(string $key, $what, $expiresAfter = null)
    {
        try {
            $item = $this->adapter->getItem("{$this->redisNamespace}$key");
            $item->set($what);

            if ($expiresAfter !== null) {
                $item->expiresAfter($expiresAfter);
            }

            $this->adapter->save($item);
        } catch (InvalidArgumentException $e) {
            // TODO: log why it didn't work
            return false;
        }

        return $what;
    }

    /**
     * @param string $key
     * @param bool $useNamespace
     * @return bool
     */
    public function deleteItem(string $key, $useNamespace = true)
    {
        return $this->adapter->deleteItem($useNamespace ? "{$this->redisNamespace}$key" : $key);
    }

    /**
     * @param array $keys
     * @param bool $useNamespace
     * @return bool
     */
    public function deleteItems(array $keys, $useNamespace = true)
    {
        return $this->adapter->deleteItems($useNamespace ? array_map(function($key) {
            return "{$this->redisNamespace}$key";
        }, $keys) : $keys);
    }

    /**
     * @param string $pattern
     * @param bool $useNamespace
     * @return bool
     */
    public function deleteItemsByPattern(string $pattern, $useNamespace = true)
    {
        return $this->adapter->deleteItems($useNamespace ? $this->getKeys($pattern) : $this->predisClient->keys($pattern));
    }
}