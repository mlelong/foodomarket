<?php

namespace AppBundle\Services\Invoice;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\InvoiceDeliveryNoteRepository;
use AppBundle\Repository\InvoiceRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Services\ApacheTikaClient;
use Exception;

class InvoiceParserService
{
    /** @var ApacheTikaClient */
    private $apacheTikaClient;

    /** @var InvoiceParser[] */
    private $parsers;

    /** @var InvoiceRepository */
    protected $invoiceRepository;

    /** @var InvoiceDeliveryNoteRepository */
    protected $invoiceDeliveryNoteRepository;

    /** @var SupplierAccountLogRepository */
    private $supplierAccountLogRepository;

    public function __construct(
        ApacheTikaClient $apacheTikaClient,
        InvoiceRepository $invoiceRepository,
        InvoiceDeliveryNoteRepository $invoiceDeliveryNoteRepository,
        SupplierAccountLogRepository $supplierAccountLogRepository)
    {
        $this->apacheTikaClient = $apacheTikaClient;
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceDeliveryNoteRepository = $invoiceDeliveryNoteRepository;
        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
    }

    /**
     * Ajoute un parser spécifique à notre liste de parsers
     * Cette méthode est appelée automatiquement par DI (see AppBundle\DependencyInjection\Compiler\InvoiceParserCompilerPass)
     *
     * @param string $alias
     * @param InvoiceParser $parser
     */
    public function addParser(string $alias, InvoiceParser $parser)
    {
        $this->parsers[$alias] = $parser;
    }

    /**
     * @param string $fileContent
     * @return InvoiceParser|null
     */
    public function getParser(string &$fileContent) : ?InvoiceParser
    {
        foreach ($this->parsers as $parser) {
            if ($parser->supports($fileContent)) {
                return $parser;
            }
        }

        return null;
    }

    /**
     * @param string $file
     * @param string|null $fileContent
     * @return InvoiceParser|null
     */
    public function getParserForFile(string $file, string &$fileContent = null)
    {
        try {
            $fileContent = $this->apacheTikaClient->getText($file);

            return $this->getParser($fileContent);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Retourne un array:
     * - Invoice si nouvelles datas
     * - true si aucune nouvelle data
     * - false si une erreur survient ou si on n'a rien pu faire du tout
     *
     * @param string $filePath
     * @return array [
     *  'invoice' => Invoice,
     *  'hasDiff' => bool
     * ]
     * @throws Exception
     */
    final public function parseFile(string $filePath = null)
    {
        try {
            $invoiceString = $this->apacheTikaClient->getText($filePath);
        } catch (Exception $e) {
            return ['invoice' => null, 'hasDiff' => false];
        }

        // On parcourt les parsers dispos pour en trouver un qui supporte(rait) le fichier en question
        foreach ($this->parsers as $parser) {
            if ($parser->supports($invoiceString)) {
                // On extrait les infos (1 seule facture, potentiellement avec plusieurs bons de livraisons)
                $parsedInvoice = $parser->parse($invoiceString);

                $hasDiff = false;

                // On tente d'aggréger les données à une facture existante
                $invoice = $this->aggregate($parsedInvoice, $hasDiff);

                // Si on a déja ces datas en base, on a terminé
                if (!$hasDiff) {
                    return ['invoice' => $invoice, 'hasDiff' => $hasDiff];
                }

                // On va chercher le client concerné si besoin
                if ($invoice->getProspect() === null) {
                    $prospect = $this->getProspectByClientCode($invoice->getClientCode(), $parser->getSupplier());

                    if ($prospect !== null) {
                        $invoice->setProspect($prospect);
                    } else {
                        // On lève une exception si on ne trouve pas de prospect avec le code client qui figure sur la facture
                        throw new Exception("Impossible de trouver un prospect qui correspond au code client `{$invoice->getClientCode()}`", 404);
                    }
                }

                // Sinon on retourne l'objet muté
                return ['invoice' => $invoice, 'hasDiff' => $hasDiff];
            }
        }

        return ['invoice' => null, 'hasDiff' => false];
    }

    /**
     * Retourne un objet Invoice
     * (db ou l'objet passé en paramètre si non trouvé en base)
     * Si une modification a été faite, alors le paramètre hasDiff vaudra true
     *
     * Si Invoice est retourné, alors il faudra le persister
     *
     * @param Invoice $parsedInvoice
     * @param bool $hasDiff
     * @return Invoice
     */
    private function aggregate(Invoice $parsedInvoice, bool &$hasDiff = false) : Invoice
    {
        // On vérifie si on a pas déjà une facture à ce numéro en base
        $invoice = $this->getInvoiceByNumber($parsedInvoice->getNumber(), $parsedInvoice->getSupplier());

        // Apparemment non, alors c'est une toute nouvelle facture, on retourne l'objet passé
        if ($invoice === null) {
            $hasDiff = true;

            return $parsedInvoice;
        } else {
            $hasDiff = false;

            // On parcourt tous les bons de livraison
            foreach ($parsedInvoice->getInvoiceDeliveryNotes() as $deliveryNote) {
                // On vérifie si on a pas deja un bon de livraison avec ce numéro en base
                $deliveryNoteDb = $this->getInvoiceDeliveryNoteByNumber($deliveryNote->getNumber(), $invoice);

                // Si oui
                if ($deliveryNoteDb) {
                    // On parcourt les items parsés du fichier
                    foreach ($deliveryNote->getItems() as $deliveryNoteItem) {
                        $found = false;

                        // Et on vérifie si on a pas déjà cet item dans le BL
                        foreach ($deliveryNoteDb->getItems() as $deliveryNoteItemDb) {
                            if ($deliveryNoteItem->getName() === $deliveryNoteItemDb->getName()) {
                                $found = true;
                                break ;
                            }
                        }

                        // Si on ne l'a pas trouvé, alors c'est nouveau, on le rajoute dans le BL
                        if (!$found) {
                            $deliveryNoteDb->addItem($deliveryNoteItem);
                            $hasDiff = true;
                        }
                    }
                } else {
                    $invoice->addInvoiceDeliveryNote($deliveryNote);
                    $hasDiff = true;
                }
            }

            if ($hasDiff) {
                // On recalcule le montant total de la facture
                $invoice->setTotal(0.0);

                foreach ($invoice->getInvoiceDeliveryNotes() as $deliveryNote) {
                    $invoice->setTotal($invoice->getTotal() + $deliveryNote->getTotal());
                }
            }

            return $invoice;
        }
    }

    /**
     * @param string $number
     * @param Supplier|int $supplier
     * @return Invoice|object|null
     */
    private function getInvoiceByNumber(string $number, $supplier) : ?Invoice
    {
        return $this->invoiceRepository->findOneBy(['number' => $number, 'supplier' => $supplier]);
    }

    /**
     * @param string $number
     * @param Invoice|int $invoice
     * @return InvoiceDeliveryNote|object|null
     */
    private function getInvoiceDeliveryNoteByNumber(string $number, $invoice) : ?InvoiceDeliveryNote
    {
        return $this->invoiceDeliveryNoteRepository->findOneBy(['number' => $number, 'invoice' => $invoice]);
    }

    /**
     * @param string $clientCode
     * @param Supplier $supplier
     * @return Prospect|null
     */
    protected function getProspectByClientCode(string $clientCode, Supplier $supplier) : ?Prospect
    {
        $qb = $this->supplierAccountLogRepository->createQueryBuilder('o');
        /** @var SupplierAccountLog[] $supplierAccountLogs */
        $supplierAccountLogs = $qb
            ->where($qb->expr()->like('o.clientCodes', ':code'))
            ->setParameter('code', "%\"$clientCode\"%")
            ->getQuery()
            ->getResult()
        ;

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            if ($supplierAccountLog->getSupplierCategory()->getSupplier()->getFacadeSupplier()->getId() === $supplier->getId()) {
                return $supplierAccountLog->getProspect();
            }
        }

        return null;
    }
}
