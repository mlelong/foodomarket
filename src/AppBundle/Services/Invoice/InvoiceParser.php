<?php

namespace AppBundle\Services\Invoice;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\SupplierRepository;
use DateTime;
use Exception;

abstract class InvoiceParser
{
    /** @var SupplierRepository */
    protected $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * @param string $number
     * @return float
     */
    protected function parseNumber(string $number)
    {
        return floatval(str_replace([',', ' ', '€'], ['.', '', ''], $number));
    }

    /**
     * Transforme le format d/m/Y en Y-m-d
     *
     * @param string $date
     * @return DateTime|null
     */
    protected function parseDate(string $date)
    {
        try {
            return new DateTime(implode('-', array_reverse(explode('/', $date))));
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param string $invoiceString
     * @return bool
     */
    abstract public function supports(string &$invoiceString) : bool;

    /**
     * @return Supplier
     */
    abstract public function getSupplier() : Supplier;

    /**
     * @param string $invoiceString
     * @return string|null
     */
    abstract public function getInvoiceNumber(string &$invoiceString) : ?string;

    /**
     * @param string $invoiceString
     * @return string|null
     */
    abstract public function getClientCode(string &$invoiceString) : ?string;

    /**
     * @param string $invoiceString
     * @return Invoice|null
     * @throws Exception
     */
    abstract public function parse(string &$invoiceString) : ?Invoice;
}