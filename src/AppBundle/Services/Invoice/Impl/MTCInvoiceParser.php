<?php

namespace AppBundle\Services\Invoice\Impl;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use AppBundle\Entity\Supplier;
use AppBundle\Services\Invoice\InvoiceParser;
use Exception;

class MTCInvoiceParser extends InvoiceParser
{

    /**
     * @inheritDoc
     */
    public function supports(string &$invoiceString): bool
    {
        return
            (stripos($invoiceString, 'MEAT TRADING COMPANY') !== false && stripos($invoiceString, 'FACTURE') !== false)
                || (stripos($invoiceString, 'CONDITIONS GENERALES DE VENTE') !== false && stripos($invoiceString, 'M.T.C') !== false);
    }

    /**
     * @inheritDoc
     */
    public function getSupplier(): Supplier
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->supplierRepository->find(55);
    }

    /**
     * @inheritDoc
     */
    public function parse(string &$invoiceString): ?Invoice
    {
        // Numéro de la facture
        $invoiceNum = $this->getInvoiceNumber($invoiceString);
        // Numéro du client
        $clientCode = $this->getClientCode($invoiceString);
        // Date de la facture
        $hasDate = preg_match('/FACTURE N°\s+([\S]+)\s+du\s+(?<invoice_date>[\S]+)/',$invoiceString, $invoiceDate) > 0;

        // Récupération des entêtes de bon de livraison
        preg_match_all('/BL N°\s+(?<num>[\S]+)\s+du\s+(?<date>[\S]+)/i', $invoiceString, $deliveryNoteDatesNumbers, PREG_OFFSET_CAPTURE);
        // Récupération de la liste des produits
        $nbProducts = preg_match_all('/[\s]+(?<ref>\d{6})(?<product>(\s\w.+\s+|[\. \n \r \: \w]+))\s+Lot:\s\w+(?<origin>\s\w.+|[\. \n \r \: \s\w]+)(?<unit>\s\d+)(?<quantity> \d+.\d+)(?<pu> \d+,\d+)[\s](?<amount>\d+.\d+)/',
            $invoiceString, $products, PREG_OFFSET_CAPTURE);

        // Met KG en valeur par défaut à la colonne unit
        foreach($products['unit'] as &$unit){
            foreach($unit as &$item){
                $item = "KG";
            }
        }

        // Enlève les parties superflues récupérées dans la colonnes Product
        foreach($products['product'] as &$value){
            foreach($value as &$item){
                $trimmed = trim($item);
                $item = $trimmed;
                $replaced = str_replace("PRIX PREFERENTIEL DATE COURTE", "", $item);
                $item = $replaced;
            }
        }

        if (!$hasDate || $invoiceNum === null || $clientCode === null || $nbProducts === 0) {
            throw new Exception("Pas de numéro de facture trouvé ou aucun produit trouvé.", 500);
        }

        // Création de la nouvelle facture
        $invoice = new Invoice();
        $invoice->setNumber($invoiceNum);
        $invoice->setClientCode($clientCode);
        $invoice->setDate($this->parseDate($invoiceDate['invoice_date']));
        $invoice->setSupplier($this->getSupplier());

        /** @var InvoiceDeliveryNote[] $deliveryNotes */
        $deliveryNotes = [];

        // Créer un bl à chaque date détectée
        foreach ($deliveryNoteDatesNumbers['date'] as $i => $deliveryNoteDateNumber) {

            $deliveryNote = new InvoiceDeliveryNote();

            $deliveryNote->setDate($this->parseDate($deliveryNoteDateNumber[0]));
            $deliveryNote->setNumber($deliveryNoteDatesNumbers['num'][$i][0]);

            $deliveryNotes[] = $deliveryNote;
        }

        // Va servir à determiner à quel BL ajouter les produits parsés (il peut y avoir plusieurs BL par page, et le BL peut apparaitre après les produits )
        $maxOffset = PHP_INT_MIN;
        $i = 0;

        foreach ($products[0] as $productIndex => $product) {
            // Si l'offset du produit est supérieur au maxOffset (qui vaut initialement la position où l'on a trouvé la première date de livraison)
            // alors on incrémente l'index du tableau de deliveryNotes afin d'ajouter le produit au bon BL
            // On met aussi le maxOffset au prochain endroit où l'on trouve le BL, ou infini s'il n'y en a plus, de sorte que l'on incrémente plus jamais l'index
            if ($product[1] > $maxOffset) {
                $maxOffset = count($deliveryNoteDatesNumbers['date']) > ($i + 1) ? $deliveryNoteDatesNumbers['date'][$i + 1][1] : PHP_INT_MAX;
                ++$i;
            }

            // Création de chacun des produits trouvés
            $item = new InvoiceDeliveryNoteItem();

            $item->setReference($products['ref'][$productIndex][0]);
            $item->setName($products['product'][$productIndex][0]);
            $item->setQuantity($this->parseNumber($products['quantity'][$productIndex][0]));
            $item->setUnit($products['unit'][$productIndex][0]);
            $item->setUnitPrice($this->parseNumber($products['pu'][$productIndex][0]));
            $item->setTotalPrice($this->parseNumber($products['amount'][$productIndex][0]));

            $deliveryNotes[$i - 1]->addItem($item);
        }

        // Ajoute les bl dans la facture
        foreach ($deliveryNotes as $deliveryNote) {
            $invoice->addInvoiceDeliveryNote($deliveryNote);
        }

        return $invoice;
    }

    /**
     * @inheritDoc
     */
    public function getInvoiceNumber(string &$invoiceString): ?string
    {
        if (preg_match('/FACTURE N°\s+(?<invoice_num>[\S]+)/', $invoiceString, $invoiceNum)) {
            return $invoiceNum['invoice_num'];
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getClientCode(string &$invoiceString): ?string
    {
        $clientCode = preg_match('/client N°\s+(?<client>[\d]+)/', $invoiceString, $client);
        //$clientCode = replace(")", "", $clientCode);
        if ($clientCode) {
            return $client['client'];
        }

        return null;
    }

}