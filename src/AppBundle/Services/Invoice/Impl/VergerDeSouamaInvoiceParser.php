<?php


namespace AppBundle\Services\Invoice\Impl;


use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use AppBundle\Entity\Supplier;
use AppBundle\Services\Invoice\InvoiceParser;
use Exception;


class VergerDeSouamaInvoiceParser extends InvoiceParser
{
    /**
     * @inheritDoc
     */
    public function supports(string &$invoiceString): bool
    {
        return stripos($invoiceString, 'E.U.R.L. VERGER DE SOUAMA') !== false
            && stripos($invoiceString, 'Facture') !== false
        ;
    }

    /**
     * @inheritDoc
     */
    public function getSupplier(): Supplier
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->supplierRepository->find(44);
    }

    /**
     * @inheritDoc
     */
    public function parse(string &$invoiceString): ?Invoice
    {
        $hasDate = preg_match('/Date\s+(?<invoice_date>[\S]+)/',$invoiceString, $invoiceDate) > 0;
        $nbProducts = preg_match_all('/\n(?!\*)(?<ref>(\S+\s+\d+|\S+))\s+(?<product>[\w\d.\/\n\' ]+)\s+(?<unit>[\S]+)\s+(?<quantity>[\d.,]+)\s+(?<pu>[\d.,]+)\s+(?<amount>[\d.,]+)\s+2(?:\n|$)/', $invoiceString, $products);

        $invoiceNum = $this->getInvoiceNumber($invoiceString);
        $clientCode = $this->getClientCode($invoiceString);

        if (!$hasDate || $invoiceNum === null || $clientCode === null || $nbProducts === 0) {
            throw new Exception("Pas de numéro de facture trouvé !", 500);
        }

        $invoice = new Invoice();

        $invoice->setSupplier($this->getSupplier());
        $invoice->setNumber($invoiceNum);
        $invoice->setClientCode($clientCode);
        $invoice->setDate($this->parseDate($invoiceDate['invoice_date']));

        // Il n'y a pas de notion de bons de livraisons multiples pour verger de souama, alors on crée un BL avec les mêmes infos dans ce cas
        $deliveryNote = new InvoiceDeliveryNote();

        $deliveryNote->setNumber($invoiceNum);
        $deliveryNote->setDate($this->parseDate($invoiceDate['invoice_date']));

        for ($i = 0; $i < $nbProducts; ++$i) {
            $item = new InvoiceDeliveryNoteItem();

            $productName = trim(preg_replace('/\s{2,}/', ' ', $products['product'][$i]));

            $item->setName($productName);
            $item->setReference($products['ref'][$i]);
            $item->setQuantity($this->parseNumber($products['quantity'][$i]));
            $item->setUnit($products['unit'][$i]);
            $item->setUnitPrice($this->parseNumber($products['pu'][$i]));
            $item->setTotalPrice($this->parseNumber($products['amount'][$i]));

            $deliveryNote->addItem($item);
        }

        $invoice->addInvoiceDeliveryNote($deliveryNote);

        return $invoice;
    }

    /**
     * @inheritDoc
     */
    public function getInvoiceNumber(string &$invoiceString): ?string
    {
        if (preg_match('/Facture N°\s+(?<invoice_num>[\S]+)/', $invoiceString, $invoiceNum)) {
            return $invoiceNum['invoice_num'];
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getClientCode(string &$invoiceString): ?string
    {
        if (preg_match('/Client\s+(?<client>[\S ]+)/', $invoiceString, $client)) {
            return $client['client'];
        }

        return null;
    }
}