<?php


namespace AppBundle\Services\Invoice\Impl;


use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use AppBundle\Entity\Supplier;
use AppBundle\Services\Invoice\InvoiceParser;
use Exception;


class JacobInvoiceParser extends InvoiceParser
{
    /**
     * @inheritDoc
     */
    public function supports(string &$invoiceString): bool
    {
        return stripos($invoiceString, 'Jacob SA') !== false
            && stripos($invoiceString, 'Facture') !== false
        ;
    }

    /**
     * @inheritDoc
     */
    public function getSupplier(): Supplier
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->supplierRepository->find(36);
    }

    /**
     * @inheritDoc
     */
    public function parse(string &$invoiceString): ?Invoice
    {
        $hasDate = preg_match('/Facture\s+\S+\s+(?<invoice_date>\d+\/\d+\/\d+)/',$invoiceString, $invoiceDate) > 0;

        if (!$hasDate || !preg_match('/^\d+\/\d+\/\d+$/', $invoiceDate['invoice_date'])) {
            $hasDate = preg_match('/Facture\s+\S+\s+(?<client_code>[\S ]+\s+|[\S ]+\s+[\S ]+\s+)(?<invoice_date>\d+\/\d+\/\d+)/',$invoiceString, $invoiceDate) > 0;
        }

        $nbProducts = preg_match_all('/(?<product>[\S ]+)\s+(?<quantity>[\d.,]+)\s+(?<unit>[\w]+)\s+(?<cond>[\S ]+)\s+(?<pu>[\d.,]+)\s+€\s+(?<amount>[\d.,]+)\s+€/', $invoiceString, $products);

        $invoiceNum = $this->getInvoiceNumber($invoiceString);
        $clientCode = $this->getClientCode($invoiceString);

        if (!$hasDate || $invoiceNum === null || $clientCode === null || $nbProducts === 0 || !preg_match('/^\d+\/\d+\/\d+$/', $invoiceDate['invoice_date'])) {
            throw new Exception("Pas de numéro de facture/date/code client ou produits trouvé(s) !", 500);
        }

        $invoice = new Invoice();

        $invoice->setSupplier($this->getSupplier());
        $invoice->setNumber($invoiceNum);
        $invoice->setClientCode($clientCode);
        $invoice->setDate($this->parseDate($invoiceDate['invoice_date']));

        // Il n'y a pas de notion de bons de livraisons multiples pour verger de souama, alors on crée un BL avec les mêmes infos dans ce cas
        $deliveryNote = new InvoiceDeliveryNote();

        $deliveryNote->setNumber($invoiceNum);
        $deliveryNote->setDate($this->parseDate($invoiceDate['invoice_date']));

        for ($i = 0; $i < $nbProducts; ++$i) {
            $item = new InvoiceDeliveryNoteItem();

            $productName = trim(preg_replace('/\s{2,}/', ' ', $products['product'][$i]));

            $item->setName($productName);
            $item->setQuantity($this->parseNumber($products['quantity'][$i]));
            $item->setUnit($products['unit'][$i]);
            $item->setUnitPrice($this->parseNumber($products['pu'][$i]));
            $item->setTotalPrice($this->parseNumber($products['amount'][$i]));

            $deliveryNote->addItem($item);
        }

        $invoice->addInvoiceDeliveryNote($deliveryNote);

        return $invoice;
    }

    /**
     * @inheritDoc
     */
    public function getInvoiceNumber(string &$invoiceString): ?string
    {
        if (preg_match('/Facture\s+(?<invoice_num>[\S]+)/', $invoiceString, $invoiceNum)) {
            return trim($invoiceNum['invoice_num']);
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getClientCode(string &$invoiceString): ?string
    {
        if (preg_match('/Facture\s+\S+\s+(?<client_code>[\S ]+\s+|[\S ]+\s+[\S ]+\s+)(?<invoice_date>\d+\/\d+\/\d+)/', $invoiceString, $client)) {
            if (preg_match('/^(?<code>\d+)/', $client['client_code'], $match)) {
                return $match['code'];
            }

            return trim($client['client_code']);
        }

        if (preg_match('/Adresse de livraison :\s+(?<client>[\S ]+)\n/', $invoiceString, $client)) {
            return trim($client['client']);
        }

        return null;
    }
}