<?php


namespace AppBundle\Services\Invoice\Impl;


use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use AppBundle\Entity\Supplier;
use AppBundle\Services\Invoice\InvoiceParser;
use Exception;

class HallesPrestigeInvoiceParser extends InvoiceParser
{
    /**
     * @inheritDoc
     */
    public function supports(string &$invoiceString): bool
    {
        return (stripos($invoiceString, 'halles prestige') !== false
            && stripos($invoiceString, 'facture') !== false)
            || stripos($invoiceString, 'HTQtéDescription TVA') !== false
        ;
    }

    /**
     * @inheritDoc
     */
    public function getSupplier(): Supplier
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->supplierRepository->find(11);
    }

    /**
     * @inheritDoc
     */
    public function parse(string &$invoiceString): ?Invoice
    {
        $clientCodes = $invoiceDates = $invoiceNumbers = $deliveries = $products = [];

        preg_match('/client[\s]*(?<code>CL[\d]+)/i', $invoiceString, $clientCodes);
        preg_match('/Date[\s]*(?<date>[\d\/]+)/i', $invoiceString, $invoiceDates);
        $hasInvoiceNumber = preg_match('/Num[\S]*ro[\s]*(?<num>[\S]+)/i', $invoiceString, $invoiceNumbers) > 0;
        $nbProducts = preg_match_all('/(?!0)(?<tva>[\d]+,[\d]{2})(?<amount>[\d]+,[\d]{2})(?<pu>[\d]+,[\d]{2})(?<quantity>[\d]+,[\d]{2})(?<product>[\S ]+)/i', $invoiceString,$products, PREG_OFFSET_CAPTURE);
        $nbDeliveries = preg_match_all('/(?<num>BL[\d]+)[^0-9]+(?<date>[\d\/]+)\./', $invoiceString, $deliveries, PREG_OFFSET_CAPTURE);

        if ($hasInvoiceNumber) {
            $invoice = new Invoice();

            $invoice->setSupplier($this->getSupplier());
            $invoice->setClientCode($clientCodes['code']);
            $invoice->setNumber($invoiceNumbers['num']);
            $invoice->setDate($this->parseDate($invoiceDates['date']));
        } else {
            throw new Exception("Pas de numéro de facture trouvé !", 500);
        }

        $deliveryNotes = [];
        $deliveryNoteIndex = 0;

        // Pour halles prestige, on peut avoir des bons de livraison dans une facture, ou pas !
        if ($nbDeliveries === 0) {
            // S'il n'y en a pas, c'est le cas le plus simple, on rajoute un BL avec le meme numéro que la facture
            $deliveryNote = new InvoiceDeliveryNote();

            $deliveryNote->setNumber($invoiceNumbers['num']);
            $deliveryNote->setDate($this->parseDate($invoiceDates['date']));

            $deliveryNotes[] = $deliveryNote;

            $maxOffset = PHP_INT_MAX;
        }
        // Sinon et bien...
        else {
            // On met donc le maxOffset à l'endroit ou on trouve le PROCHAIN BL
            $maxOffset = isset($deliveries['date'][1]) ? $deliveries['date'][1][1] : PHP_INT_MAX;
            $deliveryNoteIndex = 1;

            for ($i = 0; $i < $nbDeliveries; ++$i) {
                $deliveryNote = new InvoiceDeliveryNote();

                $deliveryNote->setNumber($deliveries['num'][$i][0]);
                $deliveryNote->setDate($this->parseDate($deliveries['date'][$i][0]));

                $deliveryNotes[] = $deliveryNote;
            }
        }

        $index = 0;

        for ($i = 0; $i < $nbProducts; ++$i) {
            // L'index du produit est supérieur à l'index du prochain bon de livraison
            if ($products['product'][$i][1] > $maxOffset) {
                $maxOffset = isset($deliveries['date'][$deliveryNoteIndex + 1]) ? $deliveries['date'][$deliveryNoteIndex + 1][1] : PHP_INT_MAX;
                ++$index;
                ++$deliveryNoteIndex;
            }

            $product = [
                'product' => $products['product'][$i][0],
                'quantity' => floatval(str_replace(',', '.', $products['quantity'][$i][0])),
                'pu' => floatval(str_replace(',', '.', $products['pu'][$i][0])),
                'amount' => floatval(str_replace(',', '.', $products['amount'][$i][0])),
            ];

            $item = new InvoiceDeliveryNoteItem();

            $item->setName($product['product']);
            $item->setQuantity($product['quantity']);
            $item->setUnitPrice($product['pu']);
            $item->setTotalPrice($product['amount']);

            $deliveryNotes[$index]->addItem($item);
        }

        foreach ($deliveryNotes as $deliveryNote) {
            $invoice->addInvoiceDeliveryNote($deliveryNote);
        }

        return $invoice;
    }

    /**
     * @inheritDoc
     */
    public function getInvoiceNumber(string &$invoiceString): ?string
    {
        if (preg_match('/Num[\S]*ro[\s]*(?<num>[\S]+)/i', $invoiceString, $invoiceNumber)) {
            return $invoiceNumber['num'];
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getClientCode(string &$invoiceString): ?string
    {
        if (preg_match('/client[\s]*(?<code>CL[\d]+)/i', $invoiceString, $clientCode)) {
            return $clientCode['code'];
        }

        return null;
    }
}