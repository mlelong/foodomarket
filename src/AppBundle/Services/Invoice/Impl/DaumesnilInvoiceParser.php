<?php


namespace AppBundle\Services\Invoice\Impl;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use AppBundle\Entity\Supplier;
use AppBundle\Services\Invoice\InvoiceParser;


class DaumesnilInvoiceParser extends InvoiceParser
{
    /**
     * @inheritDoc
     */
    function parse(string &$invoiceString) : ?Invoice
    {
        $clientCodes = $invoiceDatesNumbers = $deliveryNoteDatesNumbers = $products = [];

        preg_match('/ECHEANCE AU (?<due_date>[\S]+)\s*(?<invoice_type>[a-zA-Z]+)?\s*(?<invoice_num>[\d]+)/i', $invoiceString, $invoiceDatesNumbers);
        preg_match('/(?<code>[\d]+)[\s]+(?<invoice_date>[\d\/]+)[\s]+(?:PREL|VIREMENT|CHEQUE|ESP)/i', $invoiceString, $clientCodes);
        preg_match_all('/Livraison du (?<date>[\d\/]+)[\s]N[°?]+[\s]*(?<num>[\d]+)/i', $invoiceString, $deliveryNoteDatesNumbers, PREG_OFFSET_CAPTURE);
        preg_match_all('/(?=[\d]+[\S]?[\s]+[a-zA-Z]+)(?<ref>[\d]+[\S]?)[\s]+(?<product>[\S ]+)[\s]+(?<pu>[\d.]+)[\s]+(?<amount>[\d.-]+)[\s]+(?<tax>[\d]+[\s]+[a-zA-Z]+)[\s]+(?<quantity>[\d.]+)\s+(?<unit>[A-Z]+)/i', $invoiceString, $products, PREG_OFFSET_CAPTURE);

        $invoice = new Invoice();

        $invoice->setNumber($invoiceDatesNumbers['invoice_num']);
        $invoice->setClientCode($clientCodes['code']);
        $invoice->setDate($this->parseDate($clientCodes['invoice_date']));
        $invoice->setSupplier($this->getSupplier());

        /** @var InvoiceDeliveryNote[] $deliveryNotes */
        $deliveryNotes = [];

        foreach ($deliveryNoteDatesNumbers['date'] as $i => $deliveryNoteDateNumber) {
            $deliveryNote = new InvoiceDeliveryNote();

            $deliveryNote->setDate($this->parseDate($deliveryNoteDateNumber[0]));
            $deliveryNote->setNumber($deliveryNoteDatesNumbers['num'][$i][0]);

            $deliveryNotes[] = $deliveryNote;
        }

        // Va servir à determiner à quel BL ajouter les produits parsés (il peut y avoir plusieurs BL par page, et le BL peut apparaitre après les produits )
        $maxOffset = PHP_INT_MIN;
        $i = 0;

        foreach ($products[0] as $productIndex => $product) {
            // Si l'offset du produit est supérieur au maxOffset (qui vaut initialement la position où l'on a trouvé la première date de livraison)
            // alors on incrémente l'index du tableau de deliveryNotes afin d'ajouter le produit au bon BL
            // On met aussi le maxOffset au prochain endroit où l'on trouve le BL, ou infini s'il n'y en a plus, de sorte que l'on incrémente plus jamais l'index
            if ($product[1] > $maxOffset) {
                $maxOffset = count($deliveryNoteDatesNumbers['date']) > ($i + 1) ? $deliveryNoteDatesNumbers['date'][$i + 1][1] : PHP_INT_MAX;
                ++$i;
            }

            $item = new InvoiceDeliveryNoteItem();

            $item->setReference($products['ref'][$productIndex][0]);
            $item->setName($products['product'][$productIndex][0]);
            $item->setQuantity($this->parseNumber($products['quantity'][$productIndex][0]));
            $item->setUnit($products['unit'][$productIndex][0]);
            $item->setUnitPrice($this->parseNumber($products['pu'][$productIndex][0]));
            $item->setTotalPrice($this->parseNumber($products['amount'][$productIndex][0]));

            $deliveryNotes[$i - 1]->addItem($item);
        }

        foreach ($deliveryNotes as $deliveryNote) {
            $invoice->addInvoiceDeliveryNote($deliveryNote);
        }

        return $invoice;
    }

    /**
     * @inheritDoc
     */
    public function getSupplier(): Supplier
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->supplierRepository->find(27);
    }

    /**
     * @inheritDoc
     */
    public function supports(string &$invoiceString) : bool
    {
        // Les avoirs daumesnil ressemblent à des factures, à ceci près que le invoice_type est 'AVOIR', j'espère qu'il n'y a(ura) pas d'autres cas de figures du style...
        $has = preg_match('/ECHEANCE AU (?<due_date>[\S]+)\s*(?<invoice_type>[a-zA-Z]+)?\s*(?<invoice_num>[\d]+)/i', $invoiceString, $invoiceDatesNumbers);

        if ($has) {
            if ($invoiceDatesNumbers['invoice_type'] === 'AVOIR') {
                return false;
            }
        }

        return $has && stripos($invoiceString, 'facture') !== false
            && stripos($invoiceString, '389 438 722') !== false
            && preg_match('/TAUX TVA MONTANT HT MONTANT TVA MONTANT EXONERE TOTAL HT/', $invoiceString) > 0
            && stripos('RELEVE N', $invoiceString) === false
        ;
    }

    /**
     * @inheritDoc
     */
    public function getInvoiceNumber(string &$invoiceString): ?string
    {
        if (preg_match('/ECHEANCE AU (?<due_date>[\S]+)\s*(?<invoice_type>[a-zA-Z]+)?\s*(?<invoice_num>[\d]+)/i', $invoiceString, $invoiceDatesNumber)) {
            return $invoiceDatesNumber['invoice_num'];
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getClientCode(string &$invoiceString): ?string
    {
        if (preg_match('/(?<code>[\d]+)[\s]+(?<invoice_date>[\d\/]+)[\s]+(?:PREL|VIREMENT|CHEQUE)/i', $invoiceString, $clientCode)) {
            return $clientCode['code'];
        }

        return null;
    }
}