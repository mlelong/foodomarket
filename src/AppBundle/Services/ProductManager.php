<?php

namespace AppBundle\Services;

use AppBundle\Entity\ChannelPricing;
use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductInformations;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductPriceHistory;
use AppBundle\Factory\ChannelPricingFactory;
use AppBundle\Repository\ImportLineRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierProductInformationsRepository;
use AppBundle\Services\Fetcher\Fetcher;
use AppBundle\Repository\ProductRepository;
use Cocur\Slugify\Slugify;
use League\ISO3166\ISO3166;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductTaxonRepository;
use Sylius\Bundle\ProductBundle\Doctrine\ORM\ProductOptionRepository;
use Sylius\Bundle\ProductBundle\Doctrine\ORM\ProductVariantRepository;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository;
use Sylius\Component\Attribute\AttributeType\SelectAttributeType;
use Sylius\Component\Attribute\Model\AttributeInterface;
use Sylius\Component\Attribute\Model\AttributeValueInterface;
use Sylius\Bundle\ChannelBundle\Doctrine\ORM\ChannelRepository;
use Sylius\Component\Core\Model\ProductTaxonInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Product\Factory\ProductVariantFactory;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Product\Model\ProductAttribute;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValue;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\Intl\Intl;

class ProductManager
{
    protected $entityManager;

    protected $productRepository;

    protected $supplierProductInformationsRepository;

    protected $taxonRepository;

    protected $channelRepository;

    protected $productTaxonRepository;

    protected $productTaxonFactory;

    protected $productAttributeRepository;

    protected $productAttributeFactory;

    protected $productAttributeValueFactory;

    protected $productVariantRepository;

    protected $productVariantFactory;

    protected $productOptionRepository;

    protected $productOptionFactory;

    protected $productOptionValueRepository;

    protected $productOptionValueFactory;

    protected $channelPricingFactory;

    protected $supplierProductPriceRepository;

    protected $importLineRepository;

    protected $createTaxon;

    protected $fetchers = [];

    protected $optionValueCache = [];
    protected $supplierProductVariantInformationsCache = [];

    public function __construct(
        EntityManager $entityManager,
        EntityRepository $importLineRepository,
        SupplierProductInformationsRepository $supplierProductInformationsRepository,
        SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository,
        SupplierProductPriceRepository $supplierProductPriceRepository,
        ProductRepository $productRepository,
        TaxonRepository $taxonRepository,
        ChannelRepository $channelRepository,
        ProductTaxonRepository $productTaxonRepository,
        FactoryInterface $productTaxonFactory,
        RepositoryInterface $productAttributeRepository,
        FactoryInterface $productAttributeFactory,
        FactoryInterface $productAttributeValueFactory,
        ProductVariantRepository $productVariantRepository,
        ProductVariantFactory $productVariantFactory,
        ProductOptionRepository $productOptionRepository,
        FactoryInterface $productOptionFactory,
        RepositoryInterface $productOptionValueRepository,
        FactoryInterface $productOptionValueFactory,
        ChannelPricingFactory $channelPricingFactory,
        $createTaxon
    )
    {
        $this->entityManager = $entityManager;
        $this->importLineRepository = $importLineRepository;
        $this->supplierProductInformationsRepository = $supplierProductInformationsRepository;
        $this->supplierProductVariantInformationsRepository = $supplierProductVariantInformationsRepository;
        $this->productRepository = $productRepository;
        $this->taxonRepository = $taxonRepository;
        $this->channelRepository = $channelRepository;
        $this->productTaxonRepository = $productTaxonRepository;
        $this->productTaxonFactory = $productTaxonFactory;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->productAttributeFactory = $productAttributeFactory;
        $this->productAttributeValueFactory = $productAttributeValueFactory;
        $this->productVariantRepository = $productVariantRepository;
        $this->productVariantFactory = $productVariantFactory;
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionFactory = $productOptionFactory;
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->productOptionValueFactory = $productOptionValueFactory;
        $this->channelPricingFactory = $channelPricingFactory;
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
        $this->createTaxon = $createTaxon;
    }

    public function addFetcher($alias, Fetcher $fetcher)
    {
        $this->fetchers[$alias] = $fetcher;
    }

    protected function getFetcher($alias)
    {
        $alias = strtolower($alias);

        if (!array_key_exists($alias, $this->fetchers)) {
            throw new InvalidArgumentException('Fetcher "' . $alias . '" doesn\'t exist (Please check the services conf)');
        }

        return $this->fetchers[$alias];
    }

    public function import($fetcherName, $filetype, $filename, $pricesDate, $supplier, $restaurant, $mercurialeId)
    {
        $import = new Import();
        $import
            ->setPricesDate($pricesDate)
            ->setFileType(Import::MERCURIAL)
            ->setFilename($filename)
            ->setFetcher($fetcherName)
            ->setSupplier($supplier)
            ->setRestaurant($restaurant)
            ->setMercurialeId($mercurialeId)
            ->setChannel($supplier->getChannel())
        ;

        $fetcher = $this->getFetcher($fetcherName);
        $products = $fetcher->getProducts($filename, $supplier);

        foreach ($products as $product) {

            // check in "matching tables" if we know that line // TODO : search by all options
            $options = [];
            /** @var SupplierProductVariantInformations $informations */

            if ($supplier->getId() == 39) {
                $weightToCheck = 0;
            } else {
                $weightToCheck = $product['weight'];
            }

            $informations = $this->getSupplierVariantInformations($supplier, $product['supplierName'], $product['reference'], $weightToCheck, $options);

            $line = new ImportLine();

            if ($informations != false) {

                $product['variantName']     = $informations->getVariantLabel();
                $product['weight']          = $informations->getWeight();
                $product['salesUnit']       = $informations->getSalesUnit();
                $product['content']         = $informations->getContent();
                $product['unitQuantity']    = $informations->getUnitQuantity();

                $product['taxon']   = '';
                if ($informations->getProduct()) {
                    $taxon = $this->getLastLevelTaxon($informations->getProduct());
                    if ($taxon) {
                        $product['taxon'] = $taxon->getCode();
                    }
                }
                switch($informations->getSalesUnit()) {
                    case 'PC' :
                        $product['priceVariant'] = bcmul($product['price'], $informations->getUnitQuantity(), 0);
                        break;
                    case 'L'  :
                    case 'KG' :
                        $product['priceVariant'] = bcmul($product['price'], $informations->getWeight(), 0);
                        break;
                    case 'CO' :
                        $product['priceVariant'] = $product['price'];
                        break;
                }
                if ($informations->getWeight()) {
                    $product['priceKg'] = bcdiv($product['priceVariant'], $informations->getWeight(), 0);
                } else {
                    $product['priceKg'] = 99900;
                }

                $line->setProduct($informations->getProduct());
                $line->setProductVariant($informations->getProductVariant());
            } else {
                // product not found
            }

            // normalize country
            if ($country = $this->getMatchingCountry($product['originCountry'])) {
                $product['originCountry'] = ucfirst($country);
            }

            $line->setContent(serialize($product));

            if ($taxon = $this->getMatchingTaxon($product['taxon'])) {
                $line->setTaxon($taxon);
            }
            if ($line->getProduct() && $line->getTaxon() && $product['weight'] > 0) {
                $line->setStatus(ImportLine::STATUS_MATCHED_WITH_CATALOG);
            }

            $line->setLine(isset($product['line']) ? $product['line'] : null);

            if ($line->getProductVariant() !== null) {
                $line->contentToProperties();
                $line->loadOptionsFromProductVariant();
            }

            $import->addLine($line);
        }

        $this->entityManager->persist($import);
        $this->entityManager->flush();

        return $import;
    }

    public function isImportReady(Import $import)
    {
        $errors = [];

        if (!$this->createTaxon) {
            foreach ($import->getLines() as $line) {
                if (!$line->getProduct() && !$line->getTaxon()) {
                    $infos = unserialize($line->getContent());
                    $errors[] = $infos['productName'];
                }
            }
        }

        if (count($errors)) {
            return $errors;
        }

        return true;
    }

    public function matchWithProduct(Import $import)
    {
        foreach ($import->getLines() as $line) {
            if (!$line->getProduct()) {
                $data = unserialize($line->getContent());

                $product = $this->getMatchingProduct($import->getSupplier(), $data['supplierName']);

                if ($product) {
                    $line->setProduct($product);
                }
            }
        }
        $this->entityManager->flush();

        return true;
    }

    public function getSupplierVariantInformations(Supplier $supplier, $name, $reference, $weight, $options)
    {
        return $this->supplierProductVariantInformationsRepository->getInformationsBySupplierNameWeightOptions($supplier, $name, $reference, $weight, $options);
    }

    public function getMatchingProduct(Supplier $supplier, $name)
    {
        return $this->supplierProductInformationsRepository->getProductBySupplierInformationName($supplier, $name);
    }


    public function getMatchingProductInformations(Supplier $supplier, $name)
    {
        return $this->supplierProductInformationsRepository->findOneBy(['supplier' => $supplier, 'label' => $name]);
    }

    public function getMatchingVariantInformations(Supplier $supplier, $name)
    {
        return $this->supplierProductVariantInformationsRepository->findOneBy(['supplier' => $supplier, 'productLabel' => $name]);
    }

    public function getMatchingVariant(Product $product, $weight, $options, $optionsVariantOptionFork)
    {
        // we will try to find a matching variant for $options
        // we will try to find a matching variant for $optionsVariantOptionFork
        // we will take the second one, if we find one

        $variantB = $this->getMatchingVariantByOptions($product, $weight, array_merge($options, $optionsVariantOptionFork));
        if ($variantB) {
            return $variantB;
        }

        $variantA = $this->getMatchingVariantByOptions($product, $weight, $options);
        if ($variantA) {
            return $variantA;
        }

        return null;

    }

    public function getMatchingVariantByOptions(Product $product, $weight, $options)
    {

        foreach ($product->getVariants() as $variant) {
            if ($variant->getWeight() != $weight) {
                continue 1;
            }
            foreach($options as $code => $value) {
                $optionValue = $variant->getOptionValuesByOptionCode($code);

                if ($optionValue === false && $value != '') {
                    continue 2;
                }
                if ($optionValue && $optionValue->getCode() != $value) {
                    continue 2;
                }
            }
            return $variant;
        }
        return null;
    }

    public function findMatchingProducts($taxonCode, $origin)
    {
        return $this->productRepository->findMatching($taxonCode);
    }

    public function findAll()
    {
        return $this->productRepository->findAll();
    }

    public function getMatchingTaxon($code)
    {
        return $this->taxonRepository->findOneByCode($code);
    }

    public function getMatchingCountry($code)
    {
        if ($code == '') {
            return '';
        }

        $code = strtoupper(trim($code));

        if (strlen($code) === 3) {
            try {
                $data = (new ISO3166)->alpha3($code);
                $code = $data['alpha2'];
            } catch (\League\ISO3166\Exception\OutOfBoundsException $ignored) {}
        }

        switch($code) {
            case 'HOL':
            case 'HOLL':
            case 'HOLLANDE':
                $code = 'NL'; break;
            case 'THAI':
                $code = 'TH'; break;
        }

        \Locale::setDefault('fr');

        $country = Intl::getRegionBundle()->getCountryName($code);
        if ($country) {
            return $country;
        }

        $countries = Intl::getRegionBundle()->getCountryNames();

        foreach ($countries as $country) {
            if (stripos($country, $code) !== FALSE) {
                return $country;
            }
        }

        return $code;
    }

    public function getMatchingProductAttribute($code, $value)
    {
        /** @var ProductAttribute $productAttribute */
        $productAttribute = $this->productAttributeRepository->findOneByCode($code);

        if ($productAttribute && array_key_exists($value, $productAttribute->getConfiguration()['choices'])) {
            return $value;
        }
    }


    /*
     * Create if needed product & variant
     *
     *
     */
    public function importProduct(Import $import, ImportLine $line)
    {

        $infos = unserialize($line->getContent());
        //$infos['salesUnit'] = strtolower($infos['salesUnit']);

        $options = [];

        // release 0.5.0
        // todo : this array contain the new options unit_content_qty / unit_content_measurement
        // and should match old variant (with no option) & new variant (with option)
        $optionsVariantOptionFork = [];

        // todo : we can remove the array_key_exists when the form import_line will be updated with all data
        if (array_key_exists('originCountry', $infos)) {
            $options['OP'] = $infos['originCountry'];
        }
        if (array_key_exists('origin', $infos)) {
            $options['OV'] = $infos['origin'];
        }
        if (array_key_exists('caliber', $infos)) {
            $options['CA'] = $infos['caliber'];
        }
        if (array_key_exists('conditioning', $infos) && $infos['conditioning'] != '') {
            $options['CO'] = $infos['conditioning'];
        }
        if (array_key_exists('naming', $infos)) {
            $options['AP'] = $infos['naming'];
        }
        if (array_key_exists('variety', $infos)) {
            $options['VA'] = $infos['variety'];
        }
        if (array_key_exists('salesUnit', $infos)) {
            $options['UM'] = $infos['salesUnit'];
        }
        if (array_key_exists('category', $infos)) {
            $options['CC'] = $infos['category'];
        }
        if (array_key_exists('bio', $infos) && $infos['bio'] != '') {
            $options['BI'] = $infos['bio'];
        } else {
            $options['BI'] = 'NOBIO';
        }
        if (array_key_exists('orderUnit', $infos)) {
            $options['UC'] = $infos['orderUnit'];
        }
        if (array_key_exists('brand', $infos)) {
            $options['MQ'] = $infos['brand'];
        }
        if (array_key_exists('unitQuantity', $infos)) {
            $optionsVariantOptionFork['UQ'] = $infos['unitQuantity'];
        }
        if (array_key_exists('unitContentQuantity', $infos)) {
            $optionsVariantOptionFork['UCQ'] = $infos['unitContentQuantity'];
        }
        if (array_key_exists('unitContentMeasurement', $infos)) {
            $optionsVariantOptionFork['UCM'] = $infos['unitContentMeasurement'];
        }

        $product = $line->getProduct();
        $variant = $line->getProductVariant();

        // if we already know the variant, use the variant to surcharge the form informations
        if ($infos['weight'] == 0) {
            $infos['weight'] = $variant->getWeight();
        }
        if ($infos['priceKg'] == 99900) {
            $infos['priceKg'] = bcdiv($infos['priceVariant'], $variant->getWeight(), 0);
        }
        if ($variant) {
            $infos['unitQuantity'] = $variant->getOptionUnitQuantity()->getValue();
            $infos['salesUnit'] = $variant->getOptionSupplierSalesUnit()->getValue();
        }

        // search for a matching product in the product catalogue
        if (!$product && !$variant) {
            if (array_key_exists($infos['productName'], $this->supplierProductVariantInformationsCache)) {
                $product = $this->supplierProductVariantInformationsCache[$infos['productName']];
            } else {
                $product = $this->getMatchingProduct($import->getSupplier(), $infos['supplierName']);
            }
        }

        // search for a matching variant in the product variants
        if ($product && !$variant) {
            $variant = $this->getMatchingVariant($product, $infos['weight'], $options, $optionsVariantOptionFork);
        }

        // create the product if we have no one to match
        if (!$product) {

            $slugify = Slugify::create();
            $product = new Product();
            $product->setCode($slugify->slugify($infos['productName']));
            $product->setCurrentLocale('fr_FR');
            $product->setName($infos['productName']);
            $product->setSlug($slugify->slugify($infos['productName']));
            $product->addChannel($import->getChannel());

            if ($line->getTaxon()) {
                $this->categorizeProduct($product, $line->getTaxon());
            }
            $this->addSupplierProductInformations($import->getSupplier(), $product, $infos['supplierName']);
            $this->supplierProductVariantInformationsCache[$infos['productName']] = $product;

            $this->entityManager->persist($product);
        } else {
            $informations = $this->getMatchingProductInformations($import->getSupplier(), $infos['supplierName']);
            if (is_null($informations)) {
                $this->addSupplierProductInformations($import->getSupplier(), $product, $infos['supplierName']);
            }
        }

        if (!$variant) {
            /** @var ProductVariantInterface $variant */
            $variant = $this->productVariantFactory->createNew();

            $variant->setName($infos['variantName']);
            $variant->setCode(rand());
            $variant->setWeight($infos['weight']);
            $product->addVariant($variant);

            // à la pièce ou kilo // pays ....
            $this->addProductOptions($variant, array_merge($options, $optionsVariantOptionFork));

            // insert or update the matching informations line
            $informations = $this->getSupplierVariantInformations($import->getSupplier(), $infos['supplierName'], $infos['reference'], $infos['weight'], []);
            if ($informations === false) {
                $this->addSupplierVariantInformations($import->getSupplier(), $product, $variant,
                    $infos['supplierName'], $infos['variantName'], $infos['weight'], $infos['unitQuantity'], $infos['salesUnit'], $infos['reference']);
            } else {
                $informations->setProductVariant($variant);
            }

            $this->entityManager->persist($variant);
        } else {
            // update the variant with missing values
            $this->addProductOptions($variant, array_merge($options, $optionsVariantOptionFork));
            // insert or update the matching informations line
            $informations = $this->getMatchingVariantInformations($import->getSupplier(), $infos['supplierName']);
            if (is_null($informations)) {
                $this->addSupplierVariantInformations($import->getSupplier(), $product, $variant,
                    $infos['supplierName'], $infos['variantName'], $infos['weight'], $infos['unitQuantity'], $infos['salesUnit'], $infos['reference']);
            } else {
                // Dont remember why this line is there.
                // Remove the SET if the column is already filled.
                if (is_null($informations->getProductVariant())) {
                    $informations->setProductVariant($variant);
                }
            }
        }

        $this->addSupplierPrice($product, $variant, $import->getSupplier(), $import->getRestaurant(), $import->getChannel(),
            $infos['priceKg'], $infos['priceVariant'], $import->getPricesDate(), $import->getMercurialeId(), $import->isPromo());

        $line->setProduct($product);
        $line->setProductVariant($variant);
        $line->setStatus(ImportLine::STATUS_IMPORTED);

        $this->entityManager->flush();

        return $product;
    }

    private function addSupplierProductInformations(Supplier $supplier, Product $product, $information)
    {
        $supplierInformation = new SupplierProductInformations();
        $supplierInformation->setProduct($product);
        $supplierInformation->setLabel($information);
        $supplierInformation->setSupplier($supplier);

        $this->entityManager->persist($supplierInformation);
    }

    private function addSupplierVariantInformations(Supplier $supplier, Product $product, ProductVariant $variant, $supplierName, $variantName, $weight, $unitQuantity, $salesUnit, $reference)
    {
        $supplierVariantInformation = new SupplierProductVariantInformations();
        $supplierVariantInformation->setProduct($product);
        $supplierVariantInformation->setProductVariant($variant);
        $supplierVariantInformation->setSupplier($supplier);
        $supplierVariantInformation->setProductLabel($supplierName);
        $supplierVariantInformation->setDisplayName($supplierName);
        $supplierVariantInformation->setVariantLabel($variantName);
        $supplierVariantInformation->setWeight($weight);
        $supplierVariantInformation->setContent(0);
        $supplierVariantInformation->setUnitQuantity($unitQuantity);

        if ($salesUnit === 'COLIS') {
            $salesUnit = 'CO';
        }

        $supplierVariantInformation->setSalesUnit($salesUnit);
        $supplierVariantInformation->setReference($reference);

        $this->entityManager->persist($supplierVariantInformation);
    }

    protected function getFirstLevelTaxon(Taxon $taxon)
    {
        if ($taxon->getLevel() == 1) {
            return $taxon;
        }

        if ($taxon->getParent()) {
            return $this->getFirstLevelTaxon($taxon->getParent());
        }

        return null;
    }

    protected function getLastLevelTaxon(Product $product)
    {
        $productTaxons = $product->getProductTaxons();
        $minLevel = 0;
        foreach ($productTaxons as $taxon) {
            if ($taxon->getTaxon()->getLevel() > $minLevel) {
                $minLevel = $taxon->getTaxon()->getLevel();
            }
        }

        foreach ($productTaxons as $taxon) {
            if ($taxon->getTaxon()->getLevel() == $minLevel) {
                return $taxon->getTaxon();
            }
        }

        return $product->getMainTaxon();
    }

    protected function categorizeProduct(Product $product, Taxon $taxon)
    {
        $productTaxon = $this->productTaxonRepository->findOneByProductCodeAndTaxonCode($product->getCode(), $taxon->getCode());

        if (!$productTaxon) {
            $this->addProductTaxon($product, $taxon);
        }

        if (!$product->getMainTaxon()) {
            $product->setMainTaxon($taxon);
        }
    }

    protected function addProductTaxon(Product $product, Taxon $taxon)
    {
        /** @var ProductTaxonInterface $productTaxon */
        $productTaxon = $this->productTaxonFactory->createNew();

        $productTaxon->setTaxon($taxon);
        $productTaxon->setProduct($product);

        $product->addProductTaxon($productTaxon);
        $this->entityManager->persist($productTaxon);

        if ($taxon->getLevel() > 0 && $taxon->getParent()) {
            $this->addProductTaxon($product, $taxon->getParent());
        }
    }

    protected function addAttributeValueForCode($product, $code, $value)
    {
        if ($value == '') {
            return ;
        }

        $attribute = $this->productAttributeRepository->findOneByCode($code);

        if (!$attribute) {// && !$this->attribute) {
            /** @var AttributeInterface $attribute */
            $attribute = $this->productAttributeFactory->createTyped(SelectAttributeType::TYPE);

            $attribute->setName($code);
            $attribute->setCode($code);
            $attribute->setConfiguration([
                'multiple' => false,
                'choices' => [],
            ]);

            $this->entityManager->persist($attribute);
        }

        if (!($product->hasAttributeByCodeAndLocale($code, 'fr_FR') && in_array($value, $product->getAttributeByCodeAndLocale($code, 'fr_FR')->getValue()))) {
            /** @var AttributeValueInterface $attributeValue */
            $attributeValue = $this->productAttributeValueFactory->createNew();

            $attributeValue->setLocaleCode('fr_FR');
            $attributeValue->setAttribute($attribute);
            $attributeValue->setValue([$value]);

            $configuration = $attribute->getConfiguration();
            $configuration['choices'][$value] = $value;

            $attribute->setConfiguration($configuration);

            $product->addAttribute($attributeValue);

            $this->entityManager->persist($attributeValue);
        }
    }

    protected function addProductOptions(ProductVariant $variant, $options)
    {



        foreach($options as $code => $value) {
            if ($value != '') {
                $this->addProductOption($variant, $code, $value);
            }
        }
    }

    protected function updateProductOptions(ProductVariant $variant, $options)
    {
        foreach($options as $code => $value) {
            if ($value != '') {
                $this->addProductOption($variant, $code, $value);
            }
        }
    }

    protected function addProductOption(ProductVariant $variant, $code, $value)
    {
        // check if the variant have already the option
        $optionValue = $variant->getOptionValuesByOptionCode($code);

        if (!$optionValue) {
            // get the option
            $option = $this->productOptionRepository->findOneByCode($code);

            if (!$option) {
                /** @var ProductOptionInterface $option */
                $option = $this->productOptionFactory->createNew();

                $option->setName($code);
                $option->setCode($code);

                $this->entityManager->persist($option);
            }

            if (array_key_exists("$code-$value", $this->optionValueCache)) {
                $optionValue =  $this->optionValueCache["$code-$value"];
            } else {
                // get existing option value or create new one
                $optionValue = $this->productOptionValueRepository->findOneBy(['option' => $option, 'code' => $value]);
                if (!$optionValue) {
                    /** @var ProductOptionValue $optionValue */
                    $optionValue = $this->productOptionValueFactory->createNew();

                    $optionValue->setOption($option);
                    $optionValue->setValue($value);
                    $optionValue->setCode($value);
                    $this->entityManager->persist($optionValue);
                    $this->optionValueCache["$code-$value"] = $optionValue;
                }
            }

            $variant->addOptionValue($optionValue);
            $variant->getProduct()->addOption($option);
        }
    }

    protected function addChannelPricing(ProductVariant $variant, Restaurant $restaurant, $channel, $price)
    {

        $channelPricing = $variant->getChannelPricingForChannelAndRestaurant($channel, $restaurant);

        if (!$channelPricing) {
            /** @var ChannelPricing $channelPricing */
            $channelPricing = $this->channelPricingFactory->createNew();

            $channelPricing->setChannelCode($channel->getCode());
            // set Price for a specific restaurant make login mandatory to see it
            $channelPricing->setRestaurant($restaurant);

            $variant->addChannelPricing($channelPricing);

            $this->entityManager->persist($channelPricing);
        }

        $channelPricing->setPrice($price);
    }

    protected function addSupplierPrice($product, ProductVariant $variant, $supplier, $restaurant, $channel, $priceKg, $priceVariant, $date, $mercurialeId, $promo)
    {
        $supplierProductPrice = $this->supplierProductPriceRepository->findOneBy(['product' => $product, 'productVariant' => $variant, 'supplier' => $supplier, 'channelCode' => $supplier->getChannel()->getCode(), 'restaurant' => $restaurant]);

        if (!$supplierProductPrice) {
            $supplierProductPrice = new SupplierProductPrice();
            $supplierProductPrice->setSupplier($supplier);
            $supplierProductPrice->setRestaurant($restaurant);
            $supplierProductPrice->setProduct($product);
            $supplierProductPrice->setProductVariant($variant);
            $supplierProductPrice->setChannelCode($supplier->getChannel()->getCode());
            $supplierProductPrice->setCreatedAt($date);
            $supplierProductPrice->setUpdatedAt($date);
            $supplierProductPrice->setPromo($promo);

            $this->entityManager->persist($supplierProductPrice);
        }

        $now = new \DateTime();
        $now->setTime(0, 0, 0);

        if ($date >= $supplierProductPrice->getCreatedAt()) {
            $supplierProductPrice->setKgPrice($priceKg);
            $supplierProductPrice->setUnitPrice($priceVariant);
            $supplierProductPrice->setMercurialeId($mercurialeId);
            $supplierProductPrice->setCreatedAt($date);
            $supplierProductPrice->setUpdatedAt($date);
            $supplierProductPrice->setPromo($promo);

            $this->addChannelPricing($variant, $restaurant, $channel, $priceVariant);
        }

        $supplierProductPriceHistory = new SupplierProductPriceHistory();
        $supplierProductPriceHistory->setSupplier($supplier);
        $supplierProductPriceHistory->setRestaurant($restaurant);
        $supplierProductPriceHistory->setProduct($product);
        $supplierProductPriceHistory->setProductVariant($variant);
        $supplierProductPriceHistory->setChannelCode($supplier->getChannel()->getCode());
        $supplierProductPriceHistory->setKgPrice($priceKg);
        $supplierProductPriceHistory->setUnitPrice($priceVariant);
        $supplierProductPriceHistory->setCreatedAt($date);
        $supplierProductPriceHistory->setUpdatedAt($date);

        $this->entityManager->persist($supplierProductPriceHistory);
    }

    public function refreshTaxonAndProduct($importId)
    {
        /** @var ImportLine[] $linesWithMissingTaxons */
        $qb = $this->importLineRepository->createQueryBuilder('i');
        $linesWithMissingInformations = $qb
            ->where('i.import = :import')
            ->andWhere($qb->expr()->orX($qb->expr()->isNull('i.taxon'), $qb->expr()->isNull('i.product')))
            ->setParameter('import', $importId)
            ->getQuery()
            ->getResult()
        ;

        foreach ($linesWithMissingInformations as $line) {
            $productNameExploded = explode(' ', $line->getProductName());

            while (!empty($productNameExploded)) {
                $productName = join(' ', $productNameExploded);

                $products = $this->productRepository->findByName($productName, 'fr_FR');

                if (!empty($products)) {
                    /** @var ProductInterface $product */
                    $product = $products[0];
                    $line->setProduct($product);
                    $line->setTaxon($product->getMainTaxon());
                    break ;
                }

                unset($productNameExploded[count($productNameExploded) - 1]);
            }

            if ($line->getProduct() != null || $line->getTaxon() != null)
                continue ;

            $productNameExploded = explode(' ', $line->getProductName());

            while (!empty($productNameExploded)) {
                $productName = join(' ', $productNameExploded);

                $taxons = $this->taxonRepository->findByName($productName, 'fr_FR');

                if (!empty($taxons)) {
                    $taxon = $this->findBestMatching($productName, $taxons);
                    $line->setTaxon($taxon);
                    break ;
                }

                unset($productNameExploded[count($productNameExploded) - 1]);
            }
        }

        foreach ($linesWithMissingInformations as $line) {
            if ($line->getProduct() && $line->getTaxon() && $line->getWeight() > 0) {
                $line->setStatus(ImportLine::STATUS_MATCHED_WITH_CATALOG);
            }
        }

        return true;
    }

    /**
     * @param string $match
     * @param TaxonInterface[] $taxons
     * @return null|TaxonInterface
     */
    private function findBestMatching(string $match, $taxons) {
        $score = 0;
        $bestTaxon = null;

        foreach ($taxons as $taxon) {
            similar_text($match, $taxon->getName(), $newScore);

            if ($newScore > $score) {
                $score = $newScore;
                $bestTaxon = $taxon;
            }
        }

        return $bestTaxon;
    }
}
