<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\SupplierProductPriceHistoryRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\TaxonInterface;

class ProductListService
{
    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var SupplierProductPriceRepository */
    private $supplierProductPriceRepository;

    /** @var SupplierProductPriceHistoryRepository */
    private $supplierProductPriceHistoryRepository;

    /** @var SupplierProductVariantInformationsRepository */
    private $supplierProductVariantInformationsRepository;

    /** @var CacheManager */
    private $liipImagineCacheManager;

    /** @var ProductUtils */
    private $productUtils;

    /**
     * ProductListService constructor.
     * @param SupplierRepository $supplierRepository
     * @param SupplierProductPriceRepository $supplierProductPriceRepository
     * @param SupplierProductPriceHistoryRepository $supplierProductPriceHistoryRepository
     * @param SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository
     * @param CacheManager $liipImagineCacheManager
     * @param ProductUtils $productUtils
     */
    public function __construct(SupplierRepository $supplierRepository, SupplierProductPriceRepository $supplierProductPriceRepository, SupplierProductPriceHistoryRepository $supplierProductPriceHistoryRepository, SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository, CacheManager $liipImagineCacheManager, $productUtils)
    {
        $this->supplierRepository = $supplierRepository;
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
        $this->supplierProductPriceHistoryRepository = $supplierProductPriceHistoryRepository;
        $this->supplierProductVariantInformationsRepository = $supplierProductVariantInformationsRepository;
        $this->liipImagineCacheManager = $liipImagineCacheManager;
        $this->productUtils = $productUtils;
    }

    public function getPriceList($isBio = false, $category = 'fruits-legumes', $supplierId = null)
    {
        if ($supplierId === null) {
            switch ($category) {
                case 'cremerie':
                    $category = [2];
                    $supplierCategory = Supplier::CAT_CREMERIE;
                    break;
                case 'maree':
                    $category = [11];
                    $supplierCategory = Supplier::CAT_MAREE;
                    break ;
                case 'boucherie':
                    $category = [34];
                    $supplierCategory = Supplier::CAT_BOUCHERIE;
                    break ;
                case 'charcuterie':
                    $category = [507];
                    $supplierCategory = Supplier::CAT_BOUCHERIE;
                    break ;
                case 'cave':
                    $category = [503];
                    $supplierCategory = Supplier::CAT_CAVE;
                    break ;
                case 'fruits-legumes':
                default:
                    $category = [69];
                    $supplierCategory = Supplier::CAT_FRUITS_LEGUMES;
                    break;
            }

            $qb = $this->supplierRepository->createQueryBuilder('s');
            $suppliers = $qb
                ->where('s.enabled = 0')
                ->andWhere($qb->expr()->like('s.categories', ':categories'))
                ->setParameter('categories', "%$supplierCategory%")
                ->getQuery()
                ->getResult();

            foreach ($suppliers as $index => $supplier) {
                if ($supplier instanceof Supplier) {
                    $suppliers[$index] = $supplier->getId();
                }
            }
        } else {
            $suppliers = [$supplierId];
            $category = null;
        }

        $qb = $this->supplierProductVariantInformationsRepository->createQueryBuilder('spvi');

        /** @var SupplierProductVariantInformations[] $spvi */
        $spvi = $qb
            ->where($qb->expr()->in('spvi.supplier', $suppliers))
            ->andWhere('spvi.manuallyCreated = 0')
            ->andWhere('spvi.product is not null')
            ->andWhere('spvi.productVariant is not null')
            ->getQuery()
            //->setMaxResults(30)
            ->getResult()
        ;

        $prices = [];

        foreach ($spvi as $info) {
            $product = $info->getProduct();
            $variant = $info->getProductVariant();

            if (!$product->isEnabled() || !$variant->isEnabled() || ($category !== null && !self::isProductOfTaxons($product, $category))) {
                continue ;
            }

            if ($isBio && $variant->getOptionBio() && $variant->getOptionBio()->getCode() == 'NOBIO') {
                continue;
            }

            $spp = $this->supplierProductPriceRepository->findOneBy(['product' => $product, 'productVariant' => $variant, 'supplier' => $info->getSupplier(), 'restaurant' => 4]);

            if ($spp === null) {
                continue;
            }

            $qb = $this->supplierProductPriceHistoryRepository->createQueryBuilder('history');
            $pricesHistory = $qb
                ->andwhere('history.product = :product')
                ->andwhere('history.productVariant = :variant')
                ->andwhere('history.supplier = :supplier')
                ->andwhere('history.restaurant = :restaurant')
                ->andwhere('history.unitPrice > 0')
                ->orderBy('history.createdAt', 'DESC')
                ->setMaxResults(4)
                ->setParameter('product', $product)
                ->setParameter('variant', $variant)
                ->setParameter('supplier', $info->getSupplier())
                ->setParameter('restaurant', 4)
                ->getQuery()
                ->getResult()
            ;

            $oldKgPrice = $currentKgPrice = $spp->getKgPrice();

            if ($currentKgPrice === 0) {
                continue ;
            }

            $evolution7j = '<i class="grey arrow right icon">';
            if (is_array($pricesHistory) && array_key_exists(1, $pricesHistory)) {
                $oldKgPrice = $pricesHistory[1]->getKgPrice();
            }

            $percent7j = $oldKgPrice > 0 ? bcsub(bcdiv(bcmul(100, $currentKgPrice), $oldKgPrice), 100) : 0;

            if ($percent7j > 0) {
                $evolution7j = '<span style="color : red; font-weight: bold">+'.$percent7j.'%</span>';
            } else if ($percent7j < 0) {
                $evolution7j = '<span style="color : green; font-weight: bold">'.$percent7j.'%</span>';
            }

            $evolution30j = '<i class="grey arrow right icon">';
            if (is_array($pricesHistory) && array_key_exists(3, $pricesHistory)) {
                $oldKgPrice = $pricesHistory[3]->getKgPrice();
            }

            $percent30j = $oldKgPrice > 0 ? bcsub(bcdiv(bcmul(100, $currentKgPrice), $oldKgPrice), 100) : 0;

            if ($percent30j > 0) {
                $evolution30j = '<span style="color : red; font-weight: bold">+'.$percent30j.'%</span>';
            } else if ($percent7j < 0) {
                $evolution30j = '<span style="color : green; font-weight: bold">'.$percent30j.'%</span>';
            }

            $ucOptionValue = $variant->getOptionOrderUnit();

            switch ($ucOptionValue) {
                case 'ORDER_KG':
                    $orderUnit = 'KG';
                    $primitivePrice = $spp->getKgPrice();
                    break ;
                case 'ORDER_PC':
                    $orderUnit = 'PC';
                    $primitivePrice = $spp->getUnitPrice();
                    break ;
                case 'ORDER_KGPC':
                default:
                    $umOptionValue = $variant->getOptionSupplierSalesUnit();

                    switch ($umOptionValue) {
                        case 'KG':
                        case 'L':
                            $orderUnit = 'KG';
                            $primitivePrice = $spp->getKgPrice();
                            break ;
                        case 'PC':
                        case 'COLIS':
                        default:
                            $orderUnit = 'PC';
                            $primitivePrice = $spp->getUnitPrice();
                            break ;
                    }

                    break ;
            }

            $productName = $product->getName();

            $productIsBio = false;
            if ($variant->getOptionBio() && $variant->getOptionBio()->getCode() == 'BIO') {
                $productName .= ' BIO';
                $productIsBio = true;
            }

            if ($orderUnit == 'PC') {
                $coOptionValue = $variant->getOptionConditionning();

                $hasVrac = false;

                if ($coOptionValue instanceof ProductOptionValue) {
                    $hasVrac = stripos($coOptionValue->getValue(), 'VRAC') !== false;
                }

                if ($coOptionValue !== false && !$hasVrac) {
                    if (in_array($coOptionValue->getValue(), ['BQ', 'BARQ', 'BARQUETTE'])) {
                        $coOptionValue = 'BARQUETTE';
                    } else if (in_array($coOptionValue->getValue(), ['CO_BOITE'])) {
                        $coOptionValue = 'BOITE';
                    } else {
                        $coOptionValue = $coOptionValue->getValue();
                    }
                    $productName .= " - {$coOptionValue}";
                }

                $uqOptionValue = $variant->getOptionUnitQuantity();

                if ($uqOptionValue !== false && $uqOptionValue->getValue() > 0) {
                    $primitivePrice /= $uqOptionValue->getValue();
                }

                $weight = bcmul(1000, $variant->getWeight(),0);
                $productName .= " <span class='weight-indication'>({$weight}g)</span>";
            }

            $supplierName = $info->getSupplier()->getName();

            $imagePath = 'shop-default-product.jpg';
            if (!$product->getImages()->isEmpty()) {
                $imagePath = $product->getImages()->first()->getPath();
            }
            $imagePath = $this->liipImagineCacheManager->getBrowserPath($imagePath, 'front_tiny');

            $marketStatistics = $this->productUtils->getProductStatistics($product->getId());
            if ($productIsBio) {
                $statsMarketKgPrice = $marketStatistics['bio']['averageKgPrice'];
                $statsEvolutionPercentMarket = $marketStatistics['bio']['pricePercentEvolution'];
                if ($ucOptionValue == 'PC') {
                    $statsMinKgPrice = $marketStatistics['bio']['minimumUnitPrice'];
                    $statsMaxKgPrice = $marketStatistics['bio']['maximumUnitPrice'];
                } else {
                    $statsMinKgPrice = $marketStatistics['bio']['minimumKgPrice'];
                    $statsMaxKgPrice = $marketStatistics['bio']['maximumKgPrice'];
                }
            } else {
                $statsMarketKgPrice = $marketStatistics['nobio']['averageKgPrice'];
                $statsEvolutionPercentMarket = $marketStatistics['nobio']['pricePercentEvolution'];
                if ($ucOptionValue == 'PC') {
                    $statsMinKgPrice = $marketStatistics['nobio']['minimumUnitPrice'];
                    $statsMaxKgPrice = $marketStatistics['nobio']['maximumUnitPrice'];
                } else {
                    $statsMinKgPrice = $marketStatistics['nobio']['minimumKgPrice'];
                    $statsMaxKgPrice = $marketStatistics['nobio']['maximumKgPrice'];
                }
            }

            if ($statsMarketKgPrice == 0) {
                $statsMarketKgPrice = bcdiv($spp->getKgPrice(), 100, 2);
            }

            $evolutionPercentMarket = '<i class="grey arrow right icon">';
            if ($statsEvolutionPercentMarket > 0) {
                $evolutionPercentMarket = '<span style="color : red; font-weight: bold">+'.$statsEvolutionPercentMarket.'%</span>';
            } else if ($statsEvolutionPercentMarket < 0) {
                $evolutionPercentMarket = '<span style="color : green; font-weight: bold">'.$statsEvolutionPercentMarket.'%</span>';
            }

            $price = [
                'id'              => $product->getId().'.'.$variant->getId(),
                'productImage'    => $imagePath,
                'product'         => $productName,
                'price'           => bcdiv($primitivePrice, 100, 2),
                'priceKg'         => bcdiv($spp->getKgPrice(), 100, 2),
                'orderUnit'       => $orderUnit,
                'supplier'        => $supplierName,
                'variantId'       => $variant->getId(),
                'evolution7j'     => $evolution7j,
                'evolution30j'    => $evolution30j,
                'marketKgPrice'   => $statsMarketKgPrice,
                'marketEvolution' => $evolutionPercentMarket,
                'marketMin'       => $statsMinKgPrice,
                'marketMax'       => $statsMaxKgPrice,
            ];

            if (!isset($prices[$product->getName()]) || $prices[$product->getName()]['priceKg'] > $price['priceKg']) {
                $prices[$product->getName()] = $price;
            }
        }

        usort($prices, function($a, $b) {
            return strcmp($a['product'], $b['product']);
        });

        return $prices;
    }

    /**
     * @param ProductInterface $product
     * @param int $taxonId
     * @return bool
     */
    public static function isProductOfTaxon(ProductInterface $product, int $taxonId)
    {
        return self::hasTaxonAncestor($product->getMainTaxon(), $taxonId);
    }

    /**
     * @param ProductInterface $product
     * @param int[] $taxonIds
     * @return bool
     */
    public static function isProductOfTaxons(ProductInterface $product, array $taxonIds)
    {
        foreach ($taxonIds as $taxonId) {
            if (self::hasTaxonAncestor($product->getMainTaxon(), $taxonId)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param null|TaxonInterface $taxon
     * @param int $taxonId
     * @return bool
     */
    private static function hasTaxonAncestor(?TaxonInterface $taxon, int $taxonId)
    {
        if ($taxon === null) {
            return false;
        }

        if ($taxon->getId() == $taxonId) {
            return true;
        }

        /** @noinspection PhpParamsInspection */
        return self::hasTaxonAncestor($taxon->getParent(), $taxonId);
    }

    public static function isFruitsVegetables(ProductInterface $product)
    {
        return self::isProductOfTaxons($product, [69]);
    }

    public static function isCreamery(ProductInterface $product)
    {
        return self::isProductOfTaxons($product, [2]);
    }

    public static function isButchery(ProductInterface $product)
    {
        return self::isProductOfTaxons($product, [34, 507]);
    }

    public static function isSeafood(ProductInterface $product)
    {
        return self::isProductOfTaxons($product, [11]);
    }

    public static function isGrocery(ProductInterface $product)
    {
        return self::isProductOfTaxons($product, [9, 10]);
    }
}