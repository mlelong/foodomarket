<?php

namespace AppBundle\Services\Suggestions;

use AppBundle\Entity\ShoppingItemInterface;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use Sylius\Component\Core\Model\ProductVariantInterface;

class SuggestionItem implements ShoppingItemInterface
{
    /**
     * @var ProductVariantInterface
     */
    private $originalVariant;

    /**
     * @var ProductVariantInterface
     */
    private $productVariant;

    /**
     * @var string
     */
    private $unit;

    /**
     * @var float
     */
    private $quantity = 0.0;

    /**
     * @var SupplierProductPrice
     */
    private $price;

    /**
     * @var float
     */
    private $originalPrice = 0.0;

    /**
     * @return ProductVariantInterface
     */
    public function getProductVariant(): ProductVariantInterface
    {
        return $this->productVariant;
    }

    /**
     * @return string {"KG", "PC"}
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param ProductVariantInterface $productVariant
     */
    public function setProductVariant(ProductVariantInterface $productVariant): void
    {
        $this->productVariant = $productVariant;
    }

    /**
     * @param string $unit
     */
    public function setUnit(string $unit): void
    {
        $this->unit = $unit;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return SupplierProductPrice
     */
    public function getPrice(): ?SupplierProductPrice
    {
        return $this->price;
    }

    /**
     * @param SupplierProductPrice $price
     */
    public function setPrice(SupplierProductPrice $price): void
    {
        $this->price = $price;
    }

    /**
     * @return ProductVariantInterface
     */
    public function getOriginalVariant(): ProductVariantInterface
    {
        return $this->originalVariant;
    }

    /**
     * @param ProductVariantInterface $originalVariant
     */
    public function setOriginalVariant(ProductVariantInterface $originalVariant): void
    {
        $this->originalVariant = $originalVariant;
    }

    public function getPriceForUnit()
    {
        if ($this->price === null)
            return 0;

        switch ($this->unit) {
            case 'PC':
//                return $this->price->getUnitPrice();
                return $this->price->getUnitContentPrice();
            case 'KG':
            default:
                return $this->price->getKgPrice();
        }
    }

    /**
     * @return float
     */
    public function getOriginalPrice(): float
    {
        return $this->originalPrice;
    }

    /**
     * @param float $originalPrice
     */
    public function setOriginalPrice(float $originalPrice): void
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier()
    {
        return $this->price !== null ? $this->price->getSupplier() : null;
    }
}