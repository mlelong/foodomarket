<?php

namespace AppBundle\Services\Suggestions;

use AppBundle\Entity\Import;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShoppingCartItem;
use AppBundle\Entity\ShoppingItemInterface;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Services\ProductUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SuggestionsService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    private $supplierRepository;

    private $suppliers;

    /** @var ProductUtils */
    private $productUtils;

    /**
     * @var float
     *
     * Percentage of missing product that can be tolerated (default 50%)
     */
    private $missingThreshold = 50.0;

    /**
     * SuggestionsService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->supplierRepository = $this->container->get('doctrine')->getRepository(Supplier::class);
        $this->productUtils = $this->container->get('app.utils.product');
    }

    /**
     * @param float $missingThreshold
     */
    public function setMissingThreshold(float $missingThreshold): void
    {
        $this->missingThreshold = $missingThreshold;
    }

    /**
     * @param ShoppingCartItem[]|ArrayCollection $items
     * @param Restaurant|null $restaurant
     * @return ArrayCollection|Suggestion[]
     */
    public function getSuggestions($items, Restaurant $restaurant = null)
    {
        $this->loadSuppliers($restaurant);

        $itemsBySuppliers = $this->splitBySuppliers($items)->map(function(ArrayCollection $items) {
            return $this->reduceItems($items);
        });

        $suggestions = new ArrayCollection();

        foreach ($this->suppliers as &$suggestedSupplier) {
            $suggestion = new Suggestion();
            $suggestion->setSupplier($suggestedSupplier);

            $missing = 0;
            $nbProducts = 0;

            foreach ($itemsBySuppliers as $key => &$itemsBySupplier) {
//                $supplier = $this->getSupplier(explode('-', $key)[0]);

                /** @var SuggestionItem $item */
                foreach ($itemsBySupplier as $item) {
                    ++$nbProducts;

                    /** @noinspection PhpParamsInspection */
                    $cheapestVariant = $this->productUtils->getCheapestSimilarVariantForSupplierAndRestaurant(
                        $item->getProductVariant(), $suggestedSupplier, $restaurant
                    );

                    if ($cheapestVariant !== null) {
                        $suggestedItem = new SuggestionItem();

                        $suggestedItem->setOriginalVariant($item->getProductVariant());
                        /** @noinspection PhpParamsInspection */
                        $suggestedItem->setProductVariant($cheapestVariant->getProductVariant());
                        $suggestedItem->setUnit($item->getUnit());
                        $suggestedItem->setPrice($cheapestVariant);
                        $suggestedItem->setQuantity($this->getCorrectQuantity($item, $cheapestVariant));

                        $suggestion->addVariant($suggestedItem);
                    } else {
                        ++$missing;

                        $suggestedItem = new SuggestionItem();

                        $suggestedItem->setOriginalVariant($item->getProductVariant());
                        /** @noinspection PhpParamsInspection */
                        $suggestedItem->setProductVariant($item->getProductVariant());
                        $suggestedItem->setUnit($item->getUnit());

                        $price = new SupplierProductPrice();

                        $price->setProduct($item->getProductVariant()->getProduct());
                        $price->setProductVariant($item->getProductVariant());
                        $price->setKgPrice($item->getOriginalPrice());
                        $price->setUnitPrice($item->getOriginalPrice());

                        $suggestedItem->setPrice($price);
                        $suggestedItem->setQuantity($this->getCorrectQuantity($item, $price));

                        $suggestion->addVariant($suggestedItem);
                    }
                }
            }

            if ($missing === 0 || ($missing * 100 / $nbProducts) <= $this->missingThreshold) {
                $suggestions->set("{$suggestion->getSupplier()->getId()}-{$suggestion->getSupplier()->getName()}", $suggestion);
            }
        }

        return $suggestions;
    }

    /**
     * @param ShoppingItemInterface $item
     * @param SupplierProductPrice $price
     * @return float|int
     */
    private function getCorrectQuantity(ShoppingItemInterface $item, SupplierProductPrice $price) {
        $orderedWeight = $item->getProductVariant()->getWeight();
        $suggestedWeight = $price->getProductVariant()->getWeight();

        switch ($item->getUnit()) {
            case 'PC':
                if ($orderedWeight !== null && $orderedWeight > 0
                    && $suggestedWeight !== null && $suggestedWeight > 0) {
                    return $item->getQuantity() * $orderedWeight / $suggestedWeight;
                }

                return $item->getQuantity();
            case 'KG':
            case 'L':
            default:
                return $item->getQuantity();
        }
    }

    /**
     * @param ShoppingCartItem[]|ArrayCollection $items
     * @return ArrayCollection
     */
    public function splitBySuppliers($items) {
        $splittedItems = new ArrayCollection();

        foreach ($items as $item) {
            $supplier = $item->getShoppingCart()->getSupplier();
            $key = "{$supplier->getId()}-{$supplier->getName()}";

            if (null === ($splittedItem = $splittedItems->get($key))) {
                $splittedItem = new ArrayCollection();

                $splittedItems->set($key, $splittedItem);
            }

            $splittedItem->add($item);
        }

        return $splittedItems;
    }

    /**
     * @param ShoppingItemInterface[]|ArrayCollection $items
     * @return ArrayCollection|ShoppingItemInterface[]
     */
    private function reduceItems($items)
    {
        $reducedItems = new ArrayCollection();

        foreach ($items as $item) {
            $key = "{$item->getProductVariant()->getId()}-{$item->getUnit()}";

            if (null === ($reducedItem = $reducedItems->get($key))) {
                $reducedItem = new SuggestionItem();

                $reducedItem->setProductVariant($item->getProductVariant());
                $reducedItem->setUnit($item->getUnit());
                $reducedItem->setOriginalPrice(bcdiv(bcmul($item->getPrice(), 100), $item->getQuantity(), 2));

                $reducedItems->set($key, $reducedItem);
            }

            $reducedItem->setQuantity($reducedItem->getQuantity() + $item->getQuantity());
        }

        return $reducedItems;
    }

    private function loadSuppliers(?Restaurant $restaurant = null): void
    {
        if ($restaurant !== null) {
            /** @var QueryBuilder $qb */
            $qb = $this->supplierRepository->createQueryBuilder('s');
            /** @var Supplier[] $suppliers */
            $suppliers = $qb
                ->join(Import::class, 'i', 'WITH', 'i.supplier = s AND i.restaurant = :restaurant')
                ->where('s.enabled = 0')
                ->setParameter('restaurant', $restaurant)
                ->getQuery()
                ->getResult()
            ;
        } else {
            $suppliers = $this->supplierRepository->findBy(['enabled' => false]);
        }

        foreach ($suppliers as &$supplier) {
            $this->suppliers[$supplier->getId()] = $supplier;
        }
    }

    /**
     * @param int $supplierId
     * @return Supplier
     */
    private function getSupplier(int $supplierId)
    {
        if (!isset($this->suppliers[$supplierId])) {
            $this->suppliers[$supplierId] = $this->supplierRepository->find($supplierId);
        }

        return $this->suppliers[$supplierId];
    }
}