<?php

namespace AppBundle\Services\Suggestions;

use AppBundle\Entity\Supplier;
use Doctrine\Common\Collections\ArrayCollection;

class Suggestion
{
    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * @var ArrayCollection|SuggestionItem[]
     */
    private $variants;

    /**
     * @var float
     */
    private $amount = 0.0;

    public function __construct()
    {
        $this->variants = new ArrayCollection();
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     * @return Suggestion
     */
    public function setSupplier(Supplier $supplier): Suggestion
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function addVariant(SuggestionItem $variant): Suggestion
    {
        $this->variants->add($variant);

        switch (strtoupper($variant->getUnit())) {
            case 'KG':
                $this->amount += ($variant->getQuantity() * $variant->getPrice()->getKgPrice());
                break ;
            case 'PC':
//                $this->amount += ($variant->getQuantity() * $variant->getPrice()->getUnitPrice());
                $this->amount += ($variant->getQuantity() * $variant->getPrice()->getUnitContentPrice());
                break ;
            case 'L':
                $this->amount += ($variant->getQuantity() * $variant->getPrice()->getUnitContentPrice());
                break ;
        }

        return $this;
    }

    /**
     * @return SuggestionItem[]|ArrayCollection
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}