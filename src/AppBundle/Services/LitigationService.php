<?php

namespace AppBundle\Services;

use AppBundle\Entity\AdminUser;
use AppBundle\Entity\Litigation;
use AppBundle\Entity\LitigationComment;
use AppBundle\Entity\LitigationCommentPicture;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Form\Type\LitigationCommentPictureType;
use AppBundle\Form\Type\LitigationCommentType;
use AppBundle\Repository\AdminUserRepository;
use AppBundle\Repository\LitigationCommentRepository;
use AppBundle\Repository\LitigationRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierRepository;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormFactory;

class LitigationService
{
    /** @var LitigationRepository */
    private $litigationRepository;

    /** @var LitigationCommentRepository */
    private $litigationCommentRepository;

    /** @var ProspectRepository */
    private $prospectRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var AdminUserRepository */
    private $adminUserRepository;

    /** @var CacheManager */
    private $liipImagineCacheManager;

    /** @var FormFactory */
    private $formFactory;

    /** @var string */
    private $routerBaseUrl;

    public function __construct(
        LitigationRepository $litigationRepository,
        LitigationCommentRepository $litigationCommentRepository,
        ProspectRepository $prospectRepository,
        SupplierRepository $supplierRepository,
        AdminUserRepository $adminUserRepository,
        CacheManager $liipImagineCacheManager,
        FormFactory $formFactory,
        string $routerBaseUrl)
    {
        $this->litigationRepository = $litigationRepository;
        $this->litigationCommentRepository = $litigationCommentRepository;
        $this->prospectRepository = $prospectRepository;
        $this->supplierRepository = $supplierRepository;
        $this->adminUserRepository = $adminUserRepository;
        $this->liipImagineCacheManager = $liipImagineCacheManager;
        $this->formFactory = $formFactory;
        $this->routerBaseUrl = $routerBaseUrl;
    }

    /**
     * @param Restaurant $restaurant
     * @return array
     */
    public function getLitigationsByRestaurant(Restaurant $restaurant)
    {
        return $this->getLitigations(
            $this->litigationRepository->findBy(['restaurant' => $restaurant], ['createdAt' => 'DESC'])
        );
    }

    /**
     * @param Supplier $supplier
     * @return array
     */
    public function getLitigationsBySupplier(Supplier $supplier)
    {
        return $this->getLitigations(
            $this->litigationRepository->findBy(['supplier' => $supplier], ['createdAt' => 'DESC'])
        );
    }

    /**
     * @param Litigation[] $litigations
     * @return array
     */
    public function getLitigations(array $litigations)
    {
        $ret = [];

        foreach($litigations as $litigation) {

            switch ($litigation->getStatus()) {
                case Litigation::STATUS_OPEN:
                    $class = 'negative';
                    break ;
                case Litigation::STATUS_CLOSED:
                    $class = 'positive';
                    break ;
                default:
                    $class = '';
                    break ;
            }

            $ret[] = [
                'id' => $litigation->getId(),
                'hash' => $litigation->getHash(),
                'class' => $class,
                'created' => $litigation->getCreatedAt()->format('d/m/Y H:i:s'),
                'updated' => $litigation->getUpdatedAt()->format('d/m/Y H:i:s'),
                'supplier' => $litigation->getSupplier()->getName(),
                'restaurant' => $litigation->getRestaurant()->getName(),
                'status' => $litigation->getStatusLabel(),
                'statusId' => $litigation->getStatus(),
                'problem' => $litigation->getProblemLabel(),
                'demand' => $litigation->getDemandLabel(),
            ];
        }

        return $ret;
    }

    /**
     * @param Litigation|int $litigation
     * @return array
     */
    public function getLitigation($litigation)
    {
        if (!$litigation instanceof Litigation) {
            $litigation = $this->litigationRepository->find($litigation);
        }

        $ret = [
            'hash' => $litigation->getHash(),
            'restaurant' => $litigation->getRestaurant()->getName(),
            'supplier' => $litigation->getSupplier()->getDisplayName(),
            'problem' => $litigation->getProblemLabel(),
            'demand' => $litigation->getDemandLabel(),
            'comments' => $this->getLitigationComments($litigation->getId(), 1, $litigation->getComments()->count()),
            'status' => $litigation->getStatus(),
            'ask_to_be_called' => $litigation->isAskToBeCalled()
        ];

        return $ret;
    }

    /**
     * @param LitigationComment|int $litigationComment
     * @return array
     */
    public function getLitigationComment($litigationComment)
    {
        if (!$litigationComment instanceof LitigationComment) {
            $litigationComment = $this->litigationCommentRepository->find($litigationComment);
        }

        $createdBy = explode(':', $litigationComment->getCreatedBy());
        $creator = null;
        $creatorType = null;

        /**
         * There can be 3 main creators of comments: the client, the supplier and foodomarket
         * Client: AppBundle\Entity\Prospect
         * Supplier: AppBundle\Entity\Supplier
         * FoodoMarket: AppBundle\Entity\AdminUser
         */
        switch ($createdBy[0]) {
            case Prospect::class:
                /** @var Prospect $entity */
                $entity = $this->prospectRepository->find($createdBy[1]);
                $creatorType = 'client';
                $creator = trim("{$entity->getFirstName()} {$entity->getContactName()}");
                break ;
            case Supplier::class:
                /** @var Supplier $entity */
                $entity = $this->supplierRepository->find($createdBy[1]);
                $creatorType = 'supplier';
                $creator = $entity->getDisplayName();
                break ;
            case AdminUser::class:
                /** @var AdminUser $entity */
                $entity = $this->adminUserRepository->find($createdBy[1]);
                $creator = $entity->getFirstName();
                $creatorType = 'foodomarket';
                break ;
        }

        return [
            'from' => $creator,
            'fromType' => $creatorType,
            'content' => nl2br($litigationComment->getContent()),
            'date' => $litigationComment->getCreatedAt()->format('d/m/Y H:i:s'),
            'photos' => array_values($litigationComment->getPictures()->map(function(LitigationCommentPicture $picture) {
                return [
                    'thumbnail_mobile' => $this->liipImagineCacheManager->getBrowserPath($picture->getPicture(), 'litigation_thumbnail_mobile'),
                    'thumbnail' => $this->liipImagineCacheManager->getBrowserPath($picture->getPicture(), 'litigation_thumbnail'),
                    'original' => "{$this->routerBaseUrl}/litigations/{$picture->getPicture()}"
                ];
            })->toArray())
        ];
    }

    public function getLitigationComments(int $litigationId, int $offset, int $limit)
    {
        /** @var Pagerfanta $paginator */
        $paginator = $this->litigationCommentRepository->createPaginator([
            'litigation' => $litigationId
        ]);

        $paginator->setCurrentPage($offset);
        $paginator->setMaxPerPage($limit);

        /** @var \ArrayIterator $litigationComments */
        $litigationComments = $paginator->getCurrentPageResults();
        $comments = [];

        /** @var LitigationComment $litigationComment */
        foreach ($litigationComments as $litigationComment) {
            $comments[] = $this->getLitigationComment($litigationComment);
        }

        return $comments;
    }

    /**
     * @param Litigation|int $litigation
     * @param LitigationComment $litigationComment
     * @param Prospect|Supplier|AdminUser $createdBy
     */
    public function addComment($litigation, LitigationComment $litigationComment, $createdBy)
    {
        // Strip off the litigation comment picture without picture (cause we force to 3 the number of pictures able to be set, so they are all sent here)
        foreach ($litigationComment->getPictures() as $picture) {
            if ($picture->getPictureFile() === null) {
                $litigationComment->getPictures()->removeElement($picture);
            }
        }

        $createdBy = str_replace('Proxies\__CG__\\', '', get_class($createdBy)) . ":{$createdBy->getId()}";

        if (!$litigation instanceof Litigation) {
            $litigation = $this->litigationRepository->find($litigation);
        }

        $litigationComment->setLitigation($litigation);
        $litigationComment->setCreatedBy($createdBy);
        $litigationComment->setUpdatedBy($createdBy);

        $this->litigationCommentRepository->add($litigationComment);
    }

    public function getLitigationCommentForm()
    {
        $litigationComment = new LitigationComment();

        $litigationComment->addPicture(new LitigationCommentPicture());
        $litigationComment->addPicture(new LitigationCommentPicture());
        $litigationComment->addPicture(new LitigationCommentPicture());

        $form = $this->formFactory->create(LitigationCommentType::class, $litigationComment, [
            'csrf_protection' => false,
        ]);

        $form
            ->remove('pictures')
            ->add('pictures', CollectionType::class, [
                'required' => false,
                'entry_type' => LitigationCommentPictureType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => false,
                'label' => 'Photos',
                'liform' => [
                    'widget' => 'photo-array'
                ]
            ])
        ;

        return $form;
    }
}