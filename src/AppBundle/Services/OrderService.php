<?php

namespace AppBundle\Services;

use ApiBundle\Dto\Interlocutor;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Email;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\Message;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\OrderStatus;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Entity\ShoppingCartItem;
use AppBundle\Repository\MessageRepository;
use AppBundle\Services\Mailer\EmailBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Exception;
use SM\Factory\Factory;
use SM\SMException;
use Sylius\Component\Core\Factory\CartItemFactory;
use Sylius\Component\Core\Model\AddressInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Core\Model\ShipmentInterface;
use Sylius\Component\Core\Model\ShopUser;
use Sylius\Component\Core\OrderCheckoutStates;
use Sylius\Component\Core\OrderPaymentStates;
use Sylius\Component\Core\OrderPaymentTransitions;
use Sylius\Component\Core\OrderShippingStates;
use Sylius\Component\Core\OrderShippingTransitions;
use Sylius\Component\Core\Repository\OrderRepositoryInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Processor\CompositeOrderProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class OrderService
{
    const DEFAULT_PAYMENT_METHOD_CODE = 'cash_on_delivery';

    private $paymentMethodCode = self::DEFAULT_PAYMENT_METHOD_CODE;

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $service
     * @return object
     */
    protected function get($service)
    {
        return $this->container->get($service);
    }

    /**
     * @param string $parameter
     * @return mixed
     */
    protected function getParameter($parameter)
    {
        return $this->container->getParameter($parameter);
    }

    /**
     * Sets the payment method code to be used as order payment
     *
     * @param string $code
     */
    public function setPaymentMethodCode($code)
    {
        $this->paymentMethodCode = $code;
    }

    /**
     * Creates a new order
     *
     * @param ShopUser $user
     * @param ShoppingCart $shoppingCart
     * @param null $when
     * @return Order
     */
    public function newFloatingOrder(ShopUser $user, ShoppingCart $shoppingCart, $when = null)
    {
        if (is_null($when)) {
            $when = new \DateTime('now');
        }

        /** @var ShopUser $user */
        $user = $this->get('app.repository.shop_user')->find($user->getId());

        /** @var Customer $customer */
        $customer = $user->getCustomer();

        $supplier = $shoppingCart->getSupplier();

        $restaurant = $shoppingCart->getRestaurant();

        /** @var ChannelInterface $channel */
        $channel = $supplier->getChannel();

        /** @var Order $order */
        $order = $this->get('sylius.factory.order')->createNew();
        /** @var string $localeCode */
        $localeCode = 'fr_FR';
        $currencyCode = 'EUR';
        /** @var CartItemFactory $orderItemFactory */
        $orderItemFactory = $this->get('sylius.factory.order_item');
        /** @var CompositeOrderProcessor $orderProcessor */
        $orderProcessor = $this->get('sylius.order_processing.order_processor');
        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $this->get('sylius.repository.order');
        /** @var EntityManagerInterface $orderManager */
        $orderManager = $this->get('sylius.manager.order');
        /** @var Factory $stateMachineFactory */
        $stateMachineFactory = $this->get('sm.factory');

        $order->setCurrencyCode($currencyCode);

        $order->setChannel($channel);
        $order->setLocaleCode($localeCode);
        $order->setCurrencyCode($currencyCode);
        $order->setCustomer($customer);
        $order->setCreatedAt($when);
        $order->setCheckoutCompletedAt($when);

        foreach ($shoppingCart->getItems() as $item) {
            /** @var ProductVariantInterface $productVariant */
            $productVariant = $item->getProductVariant();
            /** @var int $productVariantQty */
            $productVariantQty = $item->getQuantity();
            /** @var OrderItem $orderItem */
            $orderItem = $orderItemFactory->createNew();

            $orderItem->setVariant($productVariant);
            $orderItem->setQuantity($productVariantQty);
            $orderItem->setShoppingCartItem($item);

            $order->addItem($orderItem);
        }

        $order->setShoppingCart($shoppingCart);

        $orderRepository->add($order);

        $shippingAddress = $customer->getDefaultAddress();

        if ($shippingAddress === null) {
            /** @var AddressInterface $shippingAddress */
            $shippingAddress = $this->get('sylius.factory.address')->createNew();

            $shippingAddress->setCustomer($customer);
            $shippingAddress->setStreet($restaurant->getStreet());
            $shippingAddress->setCountryCode($restaurant->getCountryCode());
            $shippingAddress->setCity($restaurant->getCity());
            $shippingAddress->setPostcode($restaurant->getPostcode());
            $shippingAddress->setFirstName($customer->getFirstName());
            $shippingAddress->setLastName($customer->getLastName() ?? "");
        }

        $order->setShippingAddress($shippingAddress);
        $order->setBillingAddress($shippingAddress);

        /** @var ShipmentInterface $shipment */
        $shipment = $this->get('sylius.factory.shipment')->createNew();
        $shippingMethod = $this->get('sylius.repository.shipping_method')->findOneBy(['code' => 'supplier']);

        $shipment->setMethod($shippingMethod);

        $order->addShipment($shipment);

        try {
            $stateMachine = $stateMachineFactory->get($order, OrderShippingTransitions::GRAPH);

            $stateMachine->apply(OrderShippingTransitions::TRANSITION_REQUEST_SHIPPING);
//            $stateMachine->apply(OrderShippingTransitions::TRANSITION_SHIP);
        } catch (SMException $e) {
        }

        /** @var PaymentInterface $payment */
        $payment = $this->get('sylius.factory.payment')->createNew();

        $payment->setMethod($this->get('sylius.repository.payment_method')->findOneBy(['code' => $this->paymentMethodCode]));
        $payment->setCurrencyCode($currencyCode);

        $order->addPayment($payment);

        try {
            $stateMachine = $stateMachineFactory->get($order, OrderPaymentTransitions::GRAPH);

            $stateMachine->apply(OrderPaymentTransitions::TRANSITION_REQUEST_PAYMENT);
//            $stateMachine->apply(OrderPaymentTransitions::TRANSITION_PAY);
        } catch (SMException $e) {
        }

        $order->setState('new');
        $order->setCheckoutState('completed');
        $order->setNumber(str_pad($order->getId(), 9, '0', STR_PAD_LEFT));

        $orderProcessor->process($order);
        $orderManager->flush();

        // Adds a new history entry
        $orderStatus = new OrderStatus();
        $orderStatus->setOrder($order);
        $orderStatus->setStatus('new');

        $this->get('app.repository.order_status')->add($orderStatus);

        return $order;
    }

    /**
     * Sends email and set a message and order status
     * @param Order $order
     * @return array
     */
    public function postCreateOrder(Order $order)
    {
        $shoppingCart = $order->getShoppingCart();
        $customer = $order->getCustomer();
        $orderData = $this->get('app.shopping_cart_manager')->getOrderData($shoppingCart, $customer);
        /** @var MessageRepository $messageRepository */
        $messageRepository = $this->get('app.repository.message');
        /** @var Environment $twig */
        $twig = $this->get('twig');

        // Renders the order content to be sent via email

        /** @noinspection PhpUnhandledExceptionInspection */
        $orderData['order_content'] = $twig->render('@App/MiddleOffice/Mail/order_content.html.twig', [
            'items' => $orderData['items'],
            'note' => $shoppingCart->getNote(),
            'type' => $orderData['type'],
            'amount' => $orderData['amount'],
            'delivery_cost' => $orderData['delivery_cost']
        ]);

        $orderData['order_ref'] = $order->getNumber();

        $trials = 0;
        $tokenToUse = null;

        while ($trials < 10) {
            try {
                $token = bin2hex(random_bytes(64));

                if ($messageRepository->count(['token' => $token]) === 0) {
                    $tokenToUse = $token;
                    break ;
                }
            } catch (Exception $e) {
                ++$trials;
            }
        }

        $orderData['token'] = $tokenToUse;

        $isPaid = false;

        foreach ($order->getLyraPayments() as $lyraPayment) {
            if (in_array($lyraPayment->getLyraOrderStatus(), ['PENDING', 'SUCCEEDED'])) {
                $isPaid = true;
                break ;
            }
        }

        $email = $this->sendMail($orderData, true, $isPaid);

        $message = new Message();

        $message->setEmail($email);
        $message->setToken($tokenToUse);
        $message->setRecipientType(Interlocutor::TYPE_SUPPLIER);
        $message->setRecipient($shoppingCart->getSupplier());
        $message->setSenderType(Interlocutor::TYPE_PROSPECT);
        $message->setSender($customer->getProspect());
        $message->setOrder($order);
        $message->setChannel(Message::CHANNEL_EMAIL);
        $message->setChannelStatus(Message::CHANNEL_STATUS_WAITING);
        $message->setStatus(Message::MESSAGE_STATUS_WAITING);
        /** @noinspection PhpUnhandledExceptionInspection */
        $message->setMessage(
            $twig->render('@App/MiddleOffice/Mail/order_message.html.twig', [
                'supplier' => $shoppingCart->getSupplier()->getDisplayName(),
                'date' => $shoppingCart->getScheduledDate(),
                'items' => $orderData['items'],
                'note' => $shoppingCart->getNote()
            ])
        );

        $messageRepository->add($message);

        return ['message' => $message, 'orderData' => $orderData];
    }

    /**
     * @param array $orderData
     * @param bool $isAppOrder
     * @param bool $isPaid
     * @return Email
     */
    public function sendMail(array $orderData, bool $isAppOrder = false, bool $isPaid = false)
    {
        $sendinBlueProvider = $this->get('app.sendinblue.manager');

        /** @var EmailBuilder $emailClientBuilder */
        $emailClientBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);
        /** @var EmailBuilder $emailSupplierBuilder */
        $emailSupplierBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

        $emailClientBuilder->addTo($orderData['customer_email']);
        $emailClientBuilder->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com');

        foreach($orderData['supplier']->getOrderEmails() as $email) {
            $emailSupplierBuilder->addTo($email);
            $emailSupplierBuilder->addVariable('EMAIL', $email);
        }

        /** @var RouterInterface $router */
        $router = $this->container->get('router');

        // Supplier order confirmation

        //$emailSupplierBuilder->setTemplateId($isAppOrder ? 74 : 74);
        $emailSupplierBuilder->setTemplateId(74);

        $customerType = $orderData['customer']->getProspect()->getType() === Prospect::TYPE_INDIVIDUAL ? 'particulier' : 'professionnel';

        $emailSupplierBuilder->addVariable('CUSTOMER_TYPE', $customerType);
        $emailSupplierBuilder->addVariable('PAID', $isPaid ? 'oui' : 'non');
        $emailSupplierBuilder->addVariable('VALIDATE_ORDER_URL', $router->generate('api_confirm_order', ['id' => $orderData['id']], RouterInterface::ABSOLUTE_URL));
        $emailSupplierBuilder->addVariable('CONTACT_CLIENT', $router->generate('api_supplier_message_index', ['token' => $orderData['token']], RouterInterface::ABSOLUTE_URL));
        
        $phone = $orderData['customer']->getProspect()->getPhone();
        $mobile = $orderData['customer']->getProspect()->getMobile();
        $telephones = "$mobile - $phone";

        $emailSupplierBuilder->addVariable('FIRSTNAME', $orderData['customer']->getFirstName());
        $emailSupplierBuilder->addVariable('ORDER_REF', $orderData['order_ref']);
        $emailSupplierBuilder->addVariable('SUPPLIER_NAME', $orderData['supplier_name']);
        $emailSupplierBuilder->addVariable('RESTAURANT_NAME', $orderData['restaurant_name']);
        $emailSupplierBuilder->addVariable('RESTAURANT_ADDRESS', $orderData['address']);
        $emailSupplierBuilder->addVariable('CUSTOMER_NAME', $orderData['customer']->getFullName());
        $emailSupplierBuilder->addVariable('CUSTOMER_EMAIL', $orderData['customer_email']);
        $emailSupplierBuilder->addVariable('ORDER_CONTENT', $orderData['order_content']);
        $emailSupplierBuilder->addVariable('DATE', $orderData['date']);
        $emailSupplierBuilder->addVariable('RESTAURANT_PHONE', $telephones);
        $emailSupplierBuilder->addVariable('DELIVERY_HOURS', $orderData['customer']->getProspect()->getDeliveryHours());
        $emailSupplierBuilder->addFrom('commande@foodomarket.com');
        $emailSupplierBuilder->addBcc('commande@foodomarket.com');

        $emailSupplierBuilder->addReplyTo($isAppOrder ? 'no-reply@foodomarket.com' : $orderData['customer_email']);

        //Customer order confirmation

        $emailClientBuilder->setTemplateId(11);
        $emailClientBuilder->addVariable('EMAIL', $orderData['customer_email']);
        $emailClientBuilder->addVariable('FIRSTNAME', $orderData['customer']->getFirstName());
        $emailClientBuilder->addVariable('SUPPLIER_NAME', $orderData['supplier_name']);
        $emailClientBuilder->addVariable('RESTAURANT_NAME', $orderData['restaurant_name']);
        $emailClientBuilder->addVariable('RESTAURANT_ADDRESS', $orderData['address']);
        $emailClientBuilder->addVariable('CUSTOMER_NAME', $orderData['customer']->getFullName());
        $emailClientBuilder->addVariable('CUSTOMER_EMAIL', $orderData['customer']->getEmail());
        $emailClientBuilder->addVariable('ORDER_CONTENT', $orderData['order_content']);
        $emailClientBuilder->addVariable('DATE', $orderData['date']);
        $emailClientBuilder->addFrom('commande@foodomarket.com');
        $emailClientBuilder->addBcc('commande@foodomarket.com');
        $emailClientBuilder->addReplyTo('hello@foodomarket.com');

        $emailSupplier = $emailSupplierBuilder->build();

        $sendinBlueProvider->addEmailToQueue($emailSupplier);
        $sendinBlueProvider->addEmailToQueue($emailClientBuilder->build());

        return $emailSupplier;
    }
}