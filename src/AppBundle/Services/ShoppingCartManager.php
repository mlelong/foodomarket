<?php

namespace AppBundle\Services;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\ShoppingCartItemRepository;
use AppBundle\Repository\ShoppingCartRepository;
use AppBundle\Services\Product\ProductFinderInterface;
use AppBundle\Util\PriceModifier;
use AppBundle\Util\UserUtil;
use IntlDateFormatter;
use DateTime;



class ShoppingCartManager
{
    /** @var ShoppingCartRepository */
    private $shoppingCartRepository;

    /** @var ShoppingCartItemRepository */
    private $shoppingCartItemRepository;

    /** @var ProductFinderInterface */
    private $productFinder;


    public function __construct(
        ShoppingCartRepository $shoppingCartRepository,
        ShoppingCartItemRepository $shoppingCartItemRepository,
        ProductFinderInterface $productFinder)
    {
        $this->shoppingCartRepository = $shoppingCartRepository;
        $this->shoppingCartItemRepository = $shoppingCartItemRepository;
        $this->productFinder = $productFinder;
    }

    public function getShoppingCarts(Restaurant $restaurant)
    {
        /** @var ShoppingCart[] $shoppingCarts */
        $shoppingCarts = $this->shoppingCartRepository->findBy(['restaurant' => $restaurant, 'ordered' => false]);
        $shoppingCartsArray = [];

        foreach ($shoppingCarts as $shoppingCart) {
            $shoppingCartsArray[$shoppingCart->getSupplier()->getId()] = [
                'id' => $shoppingCart->getId(),
                'note' => $shoppingCart->getNote(),
                'supplierName' => $shoppingCart->getSupplier()->getName(),
                'supplierId' => $shoppingCart->getSupplier()->getId(),
                'items' => $this->getShoppingCartItems($shoppingCart),
                'scheduledDate' => $shoppingCart->getScheduledDate() !== null ? $shoppingCart->getScheduledDate()->format('d/m/Y') : ''
            ];
        }

        return $shoppingCartsArray;
    }

    public function getShoppingCartsItems(Restaurant $restaurant)
    {
        $shoppingCarts = $this->getShoppingCarts($restaurant);
        $shoppingCartsItems = [];

        foreach ($shoppingCarts as $shoppingCart) {
            foreach ($shoppingCart['items'] as $shoppingCartItem) {
                $shoppingCartsItems[] = $shoppingCartItem;
            }
        }

        usort($shoppingCartsItems, function($a, $b) {
            return strcasecmp($a['productName'], $b['productName']);
        });

        return $shoppingCartsItems;
    }

    public function getShoppingCartItems(ShoppingCart $shoppingCart)
    {
        $shoppingCartItems = $shoppingCart->getItems();
        $shoppingCartItemsArray = [];

        if ($shoppingCart->getScheduledDate() !== null && $shoppingCart->getScheduledDate() <= new \DateTime()) {
            $shoppingCart->setScheduledDate(null);
            $this->shoppingCartRepository->update($shoppingCart);
        }

        $scheduledDate = $shoppingCart->getScheduledDate() !== null ? $shoppingCart->getScheduledDate()->format('d/m/Y') : '';

        foreach ($shoppingCartItems as $shoppingCartItem) {
            $shoppingCartItemArray = $this->productFinder->getProduct(
                $shoppingCartItem->getProductVariant(),
                $shoppingCartItem->getSupplier(),
                $shoppingCart->getRestaurant()
            );

            if ($shoppingCartItemArray === null) {
                continue ;
            }

            $shoppingCartItemArray['cartId'] = $shoppingCart->getId();
            $shoppingCartItemArray['cartNote'] = $shoppingCart->getNote();
            $shoppingCartItemArray['scheduledDate'] = $scheduledDate;
            $shoppingCartItemArray['cartItemId'] = $shoppingCartItem->getId();
            $shoppingCartItemArray['quantity'] = $shoppingCartItem->getQuantity();
            $shoppingCartItemArray['quantityUnitSelected'] = $shoppingCartItem->getUnit();
            $shoppingCartItemArray['note'] = $shoppingCartItem->getNote();

            $shoppingCartItemsArray[$shoppingCartItemArray['productId']] = $shoppingCartItemArray;
        }

        uasort($shoppingCartItemsArray, function($a, $b) {
            return strcasecmp($a['productName'], $b['productName']);
        });

        return $shoppingCartItemsArray;
    }

    // Fonction pour formater une date (e.g : mercredi 11 novembre)
    public function dateFormat($date){
        $formatter = new IntlDateFormatter('fr_FR', IntlDateFormatter::NONE, IntlDateFormatter::NONE);
        $formatter->setPattern('EEEE d MMMM');

        return $formatter->format($date);
    }

    /**
     * @param ShoppingCart $shoppingCart
     * @return string
     */
    public function buildDate(ShoppingCart $shoppingCart)
    {
        // Cette fonction est appelée à la confirmation d'une commande
        return $this->_buildDate($shoppingCart->getSupplier()->getClosingDays(),$shoppingCart->getScheduledDate());
    }


    public function _buildDate($closingDays, ?DateTime $date = null)
    {
        // Si la personne sélectionne directement la date, aucun problème
        if ($date === null) {
            // Création d'une nouvelle date
            $date = new DateTime();
        }

        // Récupération de l'heure d'aujourd'hui
        $hour = (int)$date->format('H');

        // Si on est pas entre minuit et 2h du matin, c'est pour demain. Sinon aujourd'hui même.
        if (!($hour >=0 && $hour <= 2)) {
            $date->modify('+1 day');
        }

        // Parcours du tableau des jours fériés du fournisseur
        // Si le jour d'après est dans le planning des jours fériés
        while(in_array((int)$date->format('w'), $closingDays)){
            // Le lendemain est un jour férié, alors la livraison est avancée d'un autre jour
            $date->modify('+1 day');
        }

        return $date;
    }


    public function getOrderData(ShoppingCart $shoppingCart, Customer $customer)
    {
        $productFinder = $this->productFinder;
        $restaurant = $shoppingCart->getRestaurant();
        $restaurantName = $restaurant->getName();
        $supplier = $shoppingCart->getSupplier();
        $supplierName = $supplier->getName();
        $address = "{$restaurant->getName()}<br>{$restaurant->getStreet()}<br>{$restaurant->getPostcode()} {$restaurant->getCity()}";
        $dateNoFormatted = $this->buildDate($shoppingCart);

        $shoppingCart->setScheduledDate($dateNoFormatted);
        $this->shoppingCartRepository->update($shoppingCart);

        // Transforme le DateTime en string formatté (e.g -> mardi 12 novembre) grâce à la fonction dateFormat()
        $date = $this->dateFormat($dateNoFormatted);
        $deliveryCost = $shoppingCart->getOrder()->getShippingTotal();
        $deliveryCost = $deliveryCost && $deliveryCost > 0 ? floatval(bcdiv($shoppingCart->getOrder()->getShippingTotal(), 100, 2)) : 'Aucun';
        $prospect = $customer->getProspect();

        $isOnlinePayment = $prospect->getType() === 'individual'
            || $prospect->getAccountStatus($shoppingCart->getSupplier()) === SupplierAccountLog::STATUS_ORDERING
        ;

        $orderData = [
            'id' => $shoppingCart->getOrder()->getId(),
            'restaurant_name' => $restaurantName,
            'order_date' => $shoppingCart->getOrder()->getCheckoutCompletedAt()->format('d/m/Y H:i:s'),
            'supplier_name' => $supplierName,
            'supplier'  => $supplier,
            'total_amount' => bcdiv($shoppingCart->getOrder()->getTotal(), 100, 2),
            'customer_email' => $customer->getEmail(),
            'customer' => $customer,
            'note' => $shoppingCart->getNote(),
            'address' => $address,
            'date' => $date,
            'type' => $isOnlinePayment ? 'ttc' : 'ht',
            'delivery_cost' => $deliveryCost
        ];

        $items = [];
        $amount = 0;

        foreach ($shoppingCart->getItems() as $item) {
            $variant = $item->getProductVariant();
            $productData = $productFinder->getProduct($variant, $supplier, $restaurant);

            if ($productData === null) {
                continue ;
            }

            $productName = $productData['productName'];
            $quantity = $item->getQuantity();
            $quantityDetailed = '';
            $unit = $item->getUnit();

            if (!empty($productData['origin'])) {
                $productName .= " {$productData['origin']}";
            }

            $productNameExtra = $productData['explicitContent'];
            $priceDisplay = $productData['priceDetail']['price'] . '€';

            if (!empty($productData['priceDetail']['unit'])) {
                $priceDisplay .= "/{$productData['priceDetail']['unit']}";
            }

            if (!empty($productData['priceDetail']['detail'])) {
                $priceDisplay .= $productData['priceDetail']['detail'];
            }

            // {priceDetail.price}€<span className="price-unit">{priceDetail.unit.length > 0 && `/${priceDetail.unit}`}{priceDetail.detail.length > 0 && priceDetail.detail}</span>

            switch ($unit) {
                case 'CO':
                    $unit = 'COLIS';
                    if ($variant->getOptionUnitQuantity()->getValue() > 1) {
                        $quantityDetailed .= $variant->getOptionUnitQuantity()->getValue().'x';
                    }
                    $quantityDetailed .= $variant->getOptionUnitContentQuantity()->getValue().' '.$variant->getOptionUnitContentMeasurement()->getValue();
                    if (strlen($quantityDetailed)) {
                        $quantityDetailed = ' ('.$quantityDetailed.')';
                    }

                    $unitPrice = $productData['productPriceUnit'];
                    break ;
                case 'PC':
                    $unitPrice = $productData['productPriceUnit'];
                    break ;
                default:
                    $unitPrice = $productData['productPrice'];
                    break ;
            }

            $unitPrice = floatval(bcdiv($unitPrice, 100, 2));

            if ($orderData['type'] === 'ttc') {
                $prospect = UserUtil::getProspect($customer);
                $unitPrice = PriceModifier::modify($unitPrice, $prospect, $shoppingCart->getRestaurant()->getPostcode(), $shoppingCart->getSupplier());
            }

            $totalPrice = $quantity * $unitPrice;
            $amount += $totalPrice;

            $items[] = [
                'quantity' => $quantity,
                'unit' => $unit.$quantityDetailed,
                'reference' => $productData['supplierReference'],
                'foodomarket_name' => $productName,
                'foodomarket_name_extra' => $productNameExtra,
                'unitPrice' => $priceDisplay,
                'totalPrice' => number_format($totalPrice, 2, ',', ' '),
                'note' => $item->getNote(),
            ];
        }
        $orderData['items'] = $items;
        $orderData['amount'] = $amount;

//        $orderData['order_content'] = $this->renderView('@App/MiddleOffice/Mail/order_content.html.twig', ['items' => $items, 'note' => $shoppingCart->getNote()]);

        return $orderData;

    }
}