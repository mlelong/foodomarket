<?php

namespace AppBundle\Services;

class RedisCache extends RedisCacheManager
{
    /**
     * Removes the restaurant suppliers cache (the suppliers listed in logged shop -> Order -> ...)
     *
     * /!\ By default it removes the cache for all restaurants
     *
     * @param string $restaurant
     */
    public function removeRestaurantSuppliers($restaurant = '*')
    {
        $this->deleteItemsByPattern(
            str_replace('{restaurantId}', $restaurant, RedisCacheManager::CACHE_KEY_SHOP_USER_SUPPLIERS)
        );
    }
    /**
     * Removes the supplier products cache for a restaurant and supplier (logged shop -> Order -> Supplier X)
     *
     *  /!\ By default it removes the cache for all restaurants and suppliers
     *
     * @param string $supplier
     * @param string $restaurant
     */
    public function removeRestaurantSupplierProductsCache($supplier = '*', $restaurant = '*')
    {
        $this->deleteItemsByPattern(
            str_replace(['{supplierId}', '{restaurantId}'], [$supplier, $restaurant], RedisCacheManager::CACHE_KEY_SHOP_USER_SUPPLIER_PRODUCTS)
        );
    }

    /**
     * Removes the products cache for a restaurant (logged shop -> Order -> All Supplier)
     *
     *  /!\ By default it removes the cache for all restaurants
     *
     * @param string $restaurant
     */
    public function removeRestaurantProductsCache($restaurant = '*')
    {
        $this->deleteItemsByPattern(
            str_replace('{restaurantId}', $restaurant, RedisCacheManager::CACHE_KEY_SHOP_USER_PRODUCTS)
        );
    }

    /**
     * Removes the shopping lists cache for a restaurant
     *
     * /!\ By default it removes the cache for all restaurants
     *
     * @param string $restaurant
     */
    public function removeRestaurantShoppingListsCache($restaurant = '*')
    {
        $this->deleteItemsByPattern(
            str_replace('{restaurantId}', $restaurant, RedisCacheManager::CACHE_KEY_SHOP_USER_SHOPPING_LISTS)
        );
    }

    /**
     * Removes the cache for a restaurant's supplier's variant (offers from product page and items in logged shop product listing pages)
     *
     * /!\ By default it removes everyhing from cache
     *
     * @param string $restaurant
     * @param string $supplier
     * @param string $variant
     */
    public function removeRestaurantSupplierVariantCache($restaurant = '*', $supplier = '*', $variant = '*')
    {
        $this->deleteItemsByPattern(
            str_replace(['{variantId}', '{supplierId}', '{restaurantId}'], [$variant, $supplier, $restaurant], RedisCacheManager::CACHE_KEY_SHOP_VARIANT)
        );
    }

    /**
     * Removes the catalog products cache for a restaurant and/or taxon
     *
     * /!\ By default it removes the cache for all restaurants and taxons
     *
     * @param string $restaurant
     * @param string $taxon
     */
    public function removeRestaurantCatalogProductsCache($restaurant = '*', $taxon = '*')
    {
        $this->deleteItemsByPattern(
            str_replace(['{restaurantId}', '{taxonId}'], [$restaurant, $taxon], RedisCacheManager::CACHE_KEY_SHOP_CATALOG_PRODUCTS)
        );
    }

    /**
     * Removes the catalog cache for a supplier (unused at the moment)
     *
     * /!\ By defaults it removes the cache for all supplier
     *
     * @param string $supplier
     */
    public function removeSupplierCatalogProductsCache($supplier = '*')
    {
        $this->deleteItemsByPattern(str_replace('{supplierId}', $supplier, RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCTS));
    }

    /**
     * Removes the catalog search cache
     */
    public function removeCatalogSearchCache()
    {
        $this->deleteItemsByPattern(str_replace('{terms}', '*', RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SEARCH_PRODUCTS));
    }

    /**
     * Removes the prices listing (mercuriale or prices by category)
     */
    public function removePricesListingCache()
    {
        $this->deleteItemsByPattern(str_replace(['{category}', '{bio}'], '*', RedisCacheManager::CACHE_KEY_FRONT_PRICES_PATTERN));
        $this->deleteItemsByPattern(str_replace(['{supplierId}'], '*', RedisCacheManager::CACHE_KEY_FRONT_MERCURIAL_PATTERN));
    }

    /**
     * Removes the catalog product cache (data of a product card, for all suppliers, unused at the moment)
     *
     * /!\ By default it removes the cache for all products
     *
     * @param string $product
     */
    public function removeCatalogProductCache($product = '*')
    {
        $this->deleteItemsByPattern(str_replace('{productId}', $product, RedisCacheManager::CACHE_KEY_SHOP_CATALOG_PRODUCT));
    }

    /**
     * Removes the catalog product cache for a supplier and product (foodomarket supplier product when non logged, and restaurant supplier product when logged)
     *
     * /!\ By default it removes the cache for all suppliers and products
     *
     * @param string $supplier
     * @param string $product
     */
    public function removeCatalogSupplierProductCache($supplier = '*', $product = '*')
    {
        $this->deleteItemsByPattern(
            str_replace(
                ['{supplierId}', '{productId}', '{restaurantId}'],
                [$supplier, $product, '*'],
                RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCT
            )
        );
    }

    /**
     * Removes the shop cache completely
     *
     */
    public function removeShopCache()
    {
        $this->removeRestaurantSuppliers();
        $this->removeRestaurantSupplierProductsCache();
        $this->removeRestaurantProductsCache();
        $this->removeRestaurantShoppingListsCache();
        $this->removeRestaurantSupplierVariantCache();
    }

    /**
     * Removes the catalog cache completely
     */
    public function removeCatalogCache()
    {
        $this->removeRestaurantCatalogProductsCache();
        $this->removeSupplierCatalogProductsCache();
        $this->removeCatalogProductCache();
        $this->removeCatalogSupplierProductCache();
        $this->removeCatalogSearchCache();
    }
}