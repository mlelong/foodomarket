<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\SupplierProductPriceHistoryRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;

class ProductVariantHistory
{
    /** @var ProductUtils */
    private $productUtils;

    /** @var SupplierProductPriceRepository */
    private $supplierProductPriceRepository;

    /** @var SupplierProductPriceHistoryRepository */
    private $supplierProductPriceHistoryRepository;

    /** @var SupplierProductVariantInformationsRepository */
    private $supplierProductVariantInformationsRepository;

    public function __construct(
        ProductUtils $productUtils,
        SupplierProductPriceRepository $supplierProductPriceRepository,
        SupplierProductPriceHistoryRepository $supplierProductPriceHistoryRepository,
        SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository
    )
    {
        $this->productUtils = $productUtils;
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
        $this->supplierProductPriceHistoryRepository = $supplierProductPriceHistoryRepository;
        $this->supplierProductVariantInformationsRepository = $supplierProductVariantInformationsRepository;
    }

    /**
     * Returns the variant history (structured as followed:
     *
     * {
     *  variant id,
     *  date start,
     *  date end
     * }
     * in ASC order of dates
     * )
     *
     * @param ProductVariant $variant
     * @param Supplier $supplier
     * @return array
     */
    public function getVariantHistory(ProductVariant $variant, ?Supplier $supplier)
    {
        $history = [];
//        $similarVariants = $this->pickRealSimilars($variant, $supplier);
//
//        /** @var ProductVariant $similarVariant */
//        foreach ($similarVariants as $similarVariant) {
//            $variantInfos = $this->supplierProductVariantInformationsRepository->findBy(
//                ['productVariant' => $similarVariant, 'supplier' => $supplier],
//                ['id' => 'DESC']
//            );
//
//            $history[] = [
//                'id' => $similarVariant->getId(),
//                'createdAt' => $variantInfos[0]->getCreatedAt() !== null ? $variantInfos[0]->getCreatedAt()->format('d/m/Y') : ''
//            ];
//        }
//
//        usort($history, function($left, $right) {
//            return $left['id'] > $right['id'];
//        });

        /** @var SupplierProductVariantInformations[] $spvis */
        $spvis = $this->supplierProductVariantInformationsRepository->findBy(['product' => $variant->getProduct()], ['id' => 'ASC']);//, 'supplier' => $supplier], ['id' => 'ASC', 'createdAt' => 'ASC']);

        foreach ($spvis as $spvi) {
            if ($spvi->getProductVariant() === null)
                continue ;

            $history[$spvi->getProductVariant()->getId()][] = [
                'supplierId' => $spvi->getSupplier()->getId(),
                'supplierName' => $spvi->getSupplier()->getName()
            ];
        }

        return $history;
    }

    private function pickRealSimilars(ProductVariant $variant, Supplier $supplier)
    {
        /** @var ProductVariant[] $variants */
        $variants = $this->productUtils->getSimilarVariants($variant);
        $similarVariants = [];

        foreach ($variants as $candidateVariant) {
            if (!empty($this->supplierProductVariantInformationsRepository->findBy(['productVariant' => $candidateVariant, 'supplier' => $supplier]))) {
//              if ($this->dist($variant, $candidateVariant) >= 50) {
                    $similarVariants[] = $candidateVariant;
//              }
            }
        }

        return $similarVariants;
    }

    /**
     * Returns the percentage of similarities between 2 variants, calculated with the variant options
     *
     * @param ProductVariant $left
     * @param ProductVariant $right
     * @return float|int
     */
    private function dist(ProductVariant $left, ProductVariant $right)
    {
        $nbCriteria = max($left->getOptionValues()->count(), $right->getOptionValues()->count());
        $nbSimilarities = 0;
        $nbDifferences = 0;

        /** @var ProductOptionValue $leftOptionValue */
        foreach ($left->getOptionValues() as $leftOptionValue) {
            $found = false;

            /** @var ProductOptionValue $rightOptionValue */
            foreach ($right->getOptionValues() as $rightOptionValue) {
                if ($leftOptionValue->getOptionCode() === $rightOptionValue->getOptionCode()) {
                    if ($leftOptionValue->getCode() === $rightOptionValue->getCode()) {
                        ++$nbSimilarities;
                    } else {
                        ++$nbDifferences;
                    }

                    $found = true;
                    break ;
                }
            }

            if (!$found) {
                if (stripos($leftOptionValue->getCode(), 'vrac') > -1) {
                    ++$nbSimilarities;
                } elseif ($leftOptionValue->getCode() === 'NOBIO') {
                    ++$nbSimilarities;
                } else {
                    ++$nbDifferences;
                }
            }
        }

        $coeff = $nbSimilarities * 100 / $nbCriteria;

        return $coeff;
    }
}