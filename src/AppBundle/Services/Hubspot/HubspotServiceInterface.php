<?php

namespace AppBundle\Services\Hubspot;

use AppBundle\Entity\Prospect;

interface HubspotServiceInterface
{
    /**
     * @param string $terms
     * @param bool $refresh
     * @return array
     */
    function searchContacts(string $terms, bool $refresh = false): ?array;

    /**
     * @param int|string $dealId
     * @param bool $refresh
     * @return array
     */
    function getDealDetails($dealId, bool $refresh = false): ?array;

    /**
     * @param bool $refresh
     * @return array
     */
    function getLifecycleStages(bool $refresh = false): ?array;

    /**
     * @param int|string $vid
     * @param bool $refresh
     * @return array
     */
    function getUser($vid, bool $refresh = false): ?array;

    /**
     * @param Prospect $prospect
     * @return array
     */
    function createUser(Prospect $prospect): ?array;

    /**
     * @param int|string $vid
     * @param Prospect $prospect
     * @return array
     */
    function updateUser($vid, Prospect $prospect): ?array;

    /**
     * @param int|string $vid
     * @param bool $refresh
     * @return array
     */
    function getUserAssociatedTransactions($vid, bool $refresh = false): ?array;

    /**
     * @param int|string $vid
     * @param bool $refresh
     * @return array
     */
    function getCompanyAssociatedTransactions($vid, bool $refresh = false): ?array;

    /**
     * @param bool $refresh
     * @return array
     */
    function getDealPropertyGroups(bool $refresh = false): ?array;

    /**
     * @param string $propertyGroup
     * @param bool $refresh
     * @return array
     */
    function getDealPropertyGroup(string $propertyGroup, bool $refresh = false): ?array;

    /**
     * @param int|string $companyId
     * @param array|int[]|string[] $userIds
     * @param array $dealDetails
     * @return array
     */
    function createDeal($companyId, array $userIds, array $dealDetails): ?array;

    /**
     * @param int|string $dealId
     * @param array $dealDetails
     * @return array
     */
    function updateDeal($dealId, array $dealDetails): ?array;
}