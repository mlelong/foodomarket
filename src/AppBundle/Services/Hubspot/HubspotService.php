<?php

namespace AppBundle\Services\Hubspot;

use AppBundle\Entity\Prospect;
use AppBundle\Services\RedisCacheManager;

class HubspotService extends HubspotServiceImpl implements HubspotServiceInterface
{
    /** @var RedisCacheManager */
    private $cacheManager;

    public function __construct(RedisCacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    public function searchContacts(string $terms, bool $refresh = false): ?array
    {
        $key = 'hubspot-contacts-search-' . md5($terms);

        if ($refresh) {
            $this->pruneCache($key);
        }

        return $this->loadFromCache($key, ['parent', 'searchContacts'], [$terms], 120);
    }

    public function removeSearchContactCache() {
        $this->pruneCache('hubspot-contacts-search-*');
    }

    public function getDealDetails($dealId, bool $refresh = false): ?array
    {
        $key = "hubspot-deal-detail-$dealId";

        if ($refresh) {
            $this->pruneCache($key);
        }

        return $this->loadFromCache($key, ['parent', 'getDealDetails'], [$dealId], 120);
    }

    public function getLifeCycleStages(bool $refresh = false): ?array
    {
        $key = 'hubspot-lifecyclestages';

        if ($refresh) {
            $this->pruneCache($key);
        }

        $ret = $this->loadFromCache($key, ['parent', 'getLifeCycleStages']);

        if ($ret !== null && isset($ret['stages'])) {
            return $ret['stages'];
        }

        return null;
    }

    public function getUser($vid, bool $refresh = false): ?array
    {
        $key = "hubspot-user-$vid";

        if ($refresh) {
            $this->pruneCache($key);
        }

        return $this->loadFromCache($key, ['parent', 'getUser'], [$vid], 120);
    }

    function getUserAssociatedTransactions($vid, bool $refresh = false): ?array
    {
        $key = "hubspot-user-{$vid}-associated-transactions";

        if ($refresh) {
            $this->pruneCache($key);
        }

        return $this->loadFromCache($key, ['parent', 'getUserAssociatedTransactions'], [$vid], 120);
    }

    function getCompanyAssociatedTransactions($vid, bool $refresh = false): ?array
    {
        $key = "hubspot-company-{$vid}-associated-deals";

        if ($refresh) {
            $this->pruneCache($key);
        }

        return $this->loadFromCache($key, ['parent', 'getCompanyAssociatedTransactions'], [$vid], 120);
    }

    function getDealPropertyGroups(bool $refresh = false): ?array
    {
        $key = 'hubspot-deal-property-groups';

        if ($refresh) {
            $this->pruneCache($key);
        }

        return $this->loadFromCache($key, ['parent', 'getDealPropertyGroups']);
    }

    function getDealPropertyGroup(string $propertyGroup, bool $refresh = false): ?array
    {
        $key ="hubspot-deal-property-group-$propertyGroup";

        if ($refresh) {
            $this->pruneCache($key);
        }

        return $this->loadFromCache($key, ['parent', 'getDealPropertyGroup'], [$propertyGroup]);
    }

    function createDeal($companyId, array $userIds, array $dealDetails): ?array
    {
        $ret = parent::createDeal($companyId, $userIds, $dealDetails);

        if ($ret === null) {
            return null;
        }

        if ($ret[1] == 200) {
            if ($companyId !== null) {
                $this->pruneCompanyAssociatedTransactions($companyId);
            }

            foreach ($userIds as $userId) {
                $this->pruneUserAssociatedTransactions($userId);
            }
        }

        return $ret;
    }

    function createUser(Prospect $prospect): ?array
    {
        $ret = parent::createUser($prospect);

        if ($ret === null) {
            return null;
        }

        if ($ret[1] == 200) {
            $vid = json_decode($ret[0], true)['vid'];

            return $this->getUser($vid, true);
        } else if ($ret[1] == 409) {
            $vid = json_decode($ret[0], true)['identityProfile']['vid'];

            return $this->getUser($vid, true);
        }

        return [];
    }

    /**
     * @param int|string $vid
     * @param Prospect $prospect
     * @return array
     */
    function updateUser($vid, Prospect $prospect): ?array
    {
        $ret = parent::updateUser($vid, $prospect);

        if ($ret === null) {
            return null;
        }

        if ($ret[1] == 204) {
            return $this->getUser($vid, true);
        }

        return [];
    }

    /**
     * @param int|string $dealId
     * @param array $dealDetails
     * @return array
     */
    function updateDeal($dealId, array $dealDetails): ?array
    {
        $ret = parent::updateDeal($dealId, $dealDetails);

        if ($ret === null) {
            return null;
        }

        if ($ret[1] == 200) {
            $this->pruneDealCache($dealId);
        }

        return $ret;
    }

    /**
     * @param string $key
     * @param callable $callable
     * @param array $callableArgs
     * @param null|int $expiresAfter Time before expiration (in seconds)
     * @param int $defaultExpiresAfter
     * @return array
     */
    private function loadFromCache($key, callable $callable, $callableArgs = [], $expiresAfter = null, $defaultExpiresAfter = 604800)
    {
        if ($this->cacheManager->exists($key)) {
            return json_decode($this->cacheManager->get($key)[0], true);
        } else {
            $data = call_user_func_array($callable, $callableArgs);

            if ($expiresAfter === null) {
//                preg_match('/max-age=(?P<maxAge>[\d]+)/', $data[2]['Expect-CT'], $matches);
                preg_match('/max-age=(?P<maxAge>[\d]+)/', isset($data[2]['expect-ct']) ? $data[2]['expect-ct'] : $data[2]['Expect-CT'], $matches);

                if (isset($matches['maxAge'])) {
                    $expiresAfter = intval($matches['maxAge']);
                } else {
                    $expiresAfter = $defaultExpiresAfter;
                }
            }

            return json_decode($this->cacheManager->set($key, $data, $expiresAfter)[0], true);
        }
    }

    public function pruneDealCache($dealId) {
        return $this->pruneCache("hubspot-deal-detail-$dealId");
    }

    public function pruneCompanyAssociatedTransactions($companyId) {
        return $this->pruneCache("hubspot-company-{$companyId}-associated-deals");
    }

    public function pruneUserAssociatedTransactions($userId) {
        return $this->pruneCache("hubspot-user-{$userId}-associated-transactions");
    }

    /**
     * @param string $key
     * @return bool
     */
    private function pruneCache(string $key) {
        return $this->cacheManager->deleteItemsByPattern($key);
    }

    /**
     * @param Prospect $prospect
     * @return bool
     */
    public function createContactAndCallbackHoppy(Prospect $prospect)
    {
        return true;
//        return $this->createContactAndTransaction($prospect, 'appointmentscheduled');
    }

    /**
     * @param Prospect $prospect
     * @return bool
     */
    public function createContactAndSubscriptionHoppy(Prospect $prospect)
    {
        return true;
//        return $this->createContactAndTransaction($prospect, 'e1a6da88-7e46-4694-80f1-9f304c577413');
    }

    /**
     * @param Prospect $prospect
     * @return bool
     */
    public function createContactAndPartialSubscriptionHoppy(Prospect $prospect)
    {
        return true;
//        return $this->createContactAndTransaction($prospect, '890755');
    }

    /**
     * @param Prospect $prospect
     * @return bool
     */
    public function createContactAndCompleteSubscriptionHoppy(Prospect $prospect)
    {
        return true;
//        return $this->createContactAndTransaction($prospect, 'f41ae32f-a582-490c-9a8f-60b7f5e872f3');
    }

    /**
     * @param Prospect $prospect
     * @return bool
     */
    public function createContactAndGridSentAccountNotOpenedHoppy(Prospect $prospect)
    {
        return true;
//        return $this->createContactAndTransaction($prospect, '0a261202-3e11-4e57-ba22-71f503e7d4d3');
    }

    /**
     * @param Prospect $prospect
     * @return bool
     */
    public function createContactAndFirstOrderHoppy(Prospect $prospect)
    {
        return $this->createContactAndTransaction($prospect, 'decisionmakerboughtin');
    }

    /**
     * @param Prospect $prospect
     * @return bool
     */
    public function createContactAndAccountOpenedHoppy(Prospect $prospect)
    {
        return true;
//        return $this->createContactAndTransaction($prospect, 'presentationscheduled');
    }

    /**
     * @param Prospect $prospect
     * @param string $dealStage
     * @return bool
     */
    public function createContactAndTransaction(Prospect $prospect, string $dealStage)
    {
//        return true;

        if ($prospect->getEmail() === null) {
            return false;
        }

        $hubspotContacts = $this->searchContacts($prospect->getEmail(), true);

        if ($hubspotContacts === null || empty($hubspotContacts) || !isset($hubspotContacts['contacts']) || empty($hubspotContacts['contacts'])) {
            $contact = $this->createUser($prospect);
        } else {
            $contact = $hubspotContacts['contacts'][0];
        }

        if ($contact !== null && !empty($contact)) {
            $existingDeals = $this->getUserAssociatedTransactions($contact['vid'], true);

            // Create the deal
            if ($existingDeals === null || empty($existingDeals) || !isset($existingDeals['deals']) || empty($existingDeals['deals'])) {
                if (strlen($prospect->getRestaurantName())) {
                    $value = $prospect->getRestaurantName();
                } else {
                    $value = $prospect->getEmail();
                }

                $ret = $this->createDeal(null, [$contact['vid']], ['properties' => [
                    [
                        'name' => 'dealname',
                        'value' => $value
                    ],
                    [
                        'name' => 'dealstage',
                        'value' => $dealStage
                    ],
                    [
                        'name' => 'current_supplier',
                        'value' => $prospect->getCurrentSupplier()
                    ],
                    [
                        'name' => 'restaurant_name',
                        'value' => $prospect->getRestaurantName()
                    ],
                    [
                        'name' => 'phone',
                        'value' => $prospect->getMobile()
                    ],
                    [
                        'name' => 'restaurant_address',
                        'value' => $prospect->getAddress()
                    ]
                ]]);

                return $ret[1] == 200;
            }
            // Update the deal, if possible
            else {
                // We can update a deal only in ascending order (we can not put a deal to a former stage in the pipeline)
                // but we can move to any next deal stage
                $stages = $this->getLifeCycleStages();

                // Let's mak sure the returned deal stages are sorted by their display order
                usort($stages, function($aStage, $bStage) {
                    return $aStage['displayOrder'] > $bStage['displayOrder'];
                });

                $stageMap = [];

                foreach ($stages as $stage) {
                    $stageMap[$stage['stageId']] = $stage['displayOrder'];
                }

                $dealId = null;

                foreach ($existingDeals['deals'] as $deal) {
                    if (isset($deal['properties']['dealstage']['value'])) {
                        $_dealId = $deal['dealId'];
                        $_dealStage = $deal['properties']['dealstage']['value'];

                        if ($stageMap[$_dealStage] <= $stageMap[$dealStage]) {
                            $dealId = $_dealId;
                            break ;
                        }
                    }
                }

                if ($dealId !== null) {
                    $ret = $this->updateDeal($dealId, ['properties' => [
                        [
                            'name' => 'dealstage',
                            'value' => $dealStage
                        ],
                        [
                            'name' => 'current_supplier',
                            'value' => $prospect->getCurrentSupplier()
                        ],
                        [
                            'name' => 'restaurant_name',
                            'value' => $prospect->getRestaurantName()
                        ],
                        [
                            'name' => 'phone',
                            'value' => $prospect->getPhone()
                        ],
                        [
                            'name' => 'email',
                            'value' => $prospect->getEmail()
                        ],
                        [
                            'name' => 'restaurant_address',
                            'value' => $prospect->getAddress()
                        ]
                    ]]);

                    return $ret[1] == 200;
                }
            }
        }

        return false;
    }
}