<?php

namespace AppBundle\Services\Hubspot;

use AppBundle\Entity\Prospect;
use AppBundle\Helper\CurlClient;

abstract class HubspotServiceImpl implements HubspotServiceInterface
{
    const HUBSPOT_API_URL_BASE = 'https://api.hubapi.com';
    const API_KEY = 'd7d1f7ee-1aa9-4934-a98b-c05130ce8ec1';
    const SALE_PIPELINE_ID = 'default';
    const ENDPOINT_CONTACT_SEARCH = self::HUBSPOT_API_URL_BASE . '/contacts/v1/search/query?hapikey=' . self::API_KEY;
    const ENDPOINT_DEAL_PIPELNE = self::HUBSPOT_API_URL_BASE . '/deals/v1/pipelines/' . self::SALE_PIPELINE_ID . '?hapikey=' . self::API_KEY;
    const ENDPOINT_CONTACT = self::HUBSPOT_API_URL_BASE . '/contacts/v1/contact/vid/:vid/profile?hapikey=' . self::API_KEY;
    const ENDPOINT_COMPANY = self::HUBSPOT_API_URL_BASE . '/companies/v2/companies/:companyId?hapikey=' . self::API_KEY;
    const ENDPOINT_ASSOCIATED_DEALS = self::HUBSPOT_API_URL_BASE . '/deals/v1/deal/associated/:objectType/:objectId/paged?properties=dealstage&hapikey=' . self::API_KEY;
    const ENDPOINT_DEAL = self::HUBSPOT_API_URL_BASE . '/deals/v1/deal/:dealId?hapikey=' . self::API_KEY;
    const ENDPOINT_DEAL_PROPERTY_GROUPS = self::HUBSPOT_API_URL_BASE . '/properties/v1/deals/groups?hapikey=' . self::API_KEY;
    const ENDPOINT_DEAL_PROPERTY_GROUP = self::HUBSPOT_API_URL_BASE . '/properties/v1/deals/groups/named/:property_group?includeProperties=true&hapikey=' . self::API_KEY;
    const ENDPOINT_DEAL_UPDATE = self::HUBSPOT_API_URL_BASE . '/deals/v1/deal/:dealId?hapikey=' . self::API_KEY;
    const ENDPOINT_DEAL_CREATE = self::HUBSPOT_API_URL_BASE . '/deals/v1/deal/?hapikey=' . self::API_KEY;
    const ENDPOINT_CONTACT_CREATE = self::HUBSPOT_API_URL_BASE . '/contacts/v1/contact?hapikey=' . self::API_KEY;
    const ENDPOINT_CONTACT_UPDATE = self::HUBSPOT_API_URL_BASE . '/contacts/v1/contact/vid/:vid/profile?hapikey=' . self::API_KEY;

    public function searchContacts(string $terms, bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            self::ENDPOINT_CONTACT_SEARCH . '&q=' . urlencode($terms),
            [],
            [],
            false
        );
    }

    public function getDealDetails($dealId, bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            str_replace(':dealId', $dealId, self::ENDPOINT_DEAL),
            [],
            [],
            false
        );
    }

    public function getLifeCycleStages(bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            self::ENDPOINT_DEAL_PIPELNE,
            [],
            [],
            false
        );
    }

    public function getUser($vid, bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            str_replace(':vid', $vid, self::ENDPOINT_CONTACT),
            [],
            [],
            false
        );
    }

    /**
     * @param int|string $vid
     * @param bool $refresh
     * @return array
     */
    function getUserAssociatedTransactions($vid, bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            str_replace([':objectType', ':objectId'], ['CONTACT', $vid], self::ENDPOINT_ASSOCIATED_DEALS),
            [],
            [],
            false
        );
    }

    /**
     * @param int|string $vid
     * @param bool $refresh
     * @return array
     */
    function getCompanyAssociatedTransactions($vid, bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            str_replace([':objectType', ':objectId'], ['COMPANY', $vid], self::ENDPOINT_ASSOCIATED_DEALS),
            [],
            [],
            false
        );
    }

    /**
     * @param bool $refresh
     * @return array
     */
    function getDealPropertyGroups(bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            self::ENDPOINT_DEAL_PROPERTY_GROUPS,
            [],
            [],
            false
        );
    }

    /**
     * @param string $propertyGroup
     * @param bool $refresh
     * @return array
     */
    function getDealPropertyGroup(string $propertyGroup, bool $refresh = false): ?array
    {
        return CurlClient::instance()->request(
            'GET',
            str_replace(':property_group', $propertyGroup, self::ENDPOINT_DEAL_PROPERTY_GROUP),
            [],
            [],
            false
        );
    }

    /**
     * @param null $companyId
     * @param array $userIds
     * @param array $dealDetails
     * @return array
     */
    function createDeal($companyId, array $userIds, array $dealDetails): ?array
    {
        $dealDetails['associations'] = [];

        if ($companyId !== null) {
            $dealDetails['associations']['associatedCompanyIds'] = [$companyId];
        }

        if (!empty($userIds)) {
            $dealDetails['associations']['associatedVids'] = $userIds;
        }

        $data = json_encode($dealDetails);

        return CurlClient::instance()->request(
            'POST',
            self::ENDPOINT_DEAL_CREATE,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ],
            $data,
            false
        );
    }

    /**
     * @param int|string $dealId
     * @param array $dealDetails
     * @return array
     */
    function updateDeal($dealId, array $dealDetails): ?array
    {
        $data = json_encode($dealDetails);

        return CurlClient::instance()->request(
            'PUT',
            str_replace(':dealId', $dealId, self::ENDPOINT_DEAL_UPDATE),
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ],
            $data,
            false
        );
    }

    function createUser(Prospect $prospect): ?array
    {
        $firstname = '';
        $lastname = '';
        $compagny = '';

        if (strlen($prospect->getRestaurantName())) {
            $firstname = $prospect->getRestaurantName();
        } else {
            $firstname = $prospect->getFirstName();
            $lastname = $prospect->getContactName();
        }

        $company = $prospect->getCompanyName();

        $data = json_encode([
            'properties' => [
                [
                    'property' => 'email',
                    'value' => $prospect->getEmail()
                ],
                [
                    'property' => 'firstname',
                    'value' => $firstname
                ],
                [
                    'property' => 'lastname',
                    'value' => $lastname
                ],
                [
                    'property' => 'company',
                    'value' => "{$company}"
                ],
                [
                    'property' => 'phone',
                    'value' => $prospect->getPhone()
                ],
                [
                    'property' => 'address',
                    'value' => $prospect->getAddress()
                ],
                [
                    'property' => 'fournisseur_actuel',
                    'value' => $prospect->getCurrentSupplier()
                ],
                [
                    'property' => 'nom_du_contact',
                    'value' => $lastname
                ],
                [
                    'property' => 'pr_nom_du_contact',
                    'value' => $firstname
                ],
            ]
        ]);

        return CurlClient::instance()->request(
            'POST',
            self::ENDPOINT_CONTACT_CREATE,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ],
            $data,
            false
        );
    }

    public function updateUser($vid, Prospect $prospect): ?array
    {
        $firstname = '';
        $lastname = '';
        $compagny = '';

        if (strlen($prospect->getRestaurantName())) {
            $firstname = $prospect->getRestaurantName();
        } else {
            $firstname = $prospect->getFirstName();
            $lastname = $prospect->getContactName();
        }

        $company = $prospect->getCompanyName();

        $data = ['properties' => []];

        $data['properties'][] = [
            'property' => 'firstname',
            'value' => $firstname
        ];
        $data['properties'][] = [
            'property' => 'lastname',
            'value' => $lastname
        ];
        $data['properties'][] = [
            'property' => 'company',
            'value' => $company
        ];

        if ($prospect->getEmail() !== null) {
            $data['properties'][] = [
                'property' => 'email',
                'value' => $prospect->getEmail()
            ];
        }

        if ($prospect->getFirstName() !== null) {
            $data['properties'][] = [
                'property' => 'pr_nom_du_contact',
                'value' => $prospect->getFirstName()
            ];
        }

        if ($prospect->getContactName() !== null) {
            $data['properties'][] = [
                'property' => 'nom_du_contact',
                'value' => $prospect->getContactName()
            ];
        }

        if ($prospect->getPhone() !== null) {
            $data['properties'][] = [
                'property' => 'phone',
                'value' => $prospect->getPhone()
            ];
        }

        if ($prospect->getAddress() !== null) {
            $data['properties'][] = [
                'property' => 'address',
                'value' => $prospect->getAddress()
            ];
        }

        $data['properties'][] = [
            'property' => 'fournisseur_actuel',
            'value' => $prospect->getCurrentSuppliers()
        ];

        $data = json_encode($data);

        return CurlClient::instance()->request(
            'POST',
            str_replace(':vid', $vid, self::ENDPOINT_CONTACT_UPDATE),
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ],
            $data,
            false
        );
    }
}