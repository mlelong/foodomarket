<?php

namespace AppBundle\Services\Fetcher;

use AppBundle\Entity\Supplier;
use AppBundle\Services\MercurialeParser\DomafraisParser;

class DomafraisMercurialCsvFetcher extends Fetcher
{
    /** @var DomafraisParser */
    private $domafraisParser;

    protected function extractLines($filename)
    {
        $this->domafraisParser = new DomafraisParser($this->importPath . $filename);

        if ($this->domafraisParser->parse()) {
            return $this->domafraisParser->getProducts();
        }

        return [];
    }


    /**
     * @param string $filename
     * @param Supplier $supplier
     *
     * @return array
     */
    public function getProducts(string $filename, Supplier $supplier)
    {
        $lines = $this->extractLines($filename);
        $products = [];

        foreach ($lines as $line) {
            $products[] = $this->format($line);
        }

        return $products;
    }

    private function format(array $line)
    {
        $copy = $line;
        unset($copy['_features']);

        return [
            'line' => implode(' | ', $copy),
            'reference' => $line['code'],
            'supplierName' => $line['label'],
            'productName' => $line['_features']['productName'],
            'variantName' => '',
            'taxon' => '',
            'weight' => $line['_features']['weight'],
            'price' => bcmul($line['price'], 100, 0),
            'priceKg' => bcmul($line['_features']['kgPrice'], 100, 0),
            'priceUnit' => bcmul($line['_features']['unitPrice'], 100, 0),
            'priceVariant' => bcmul($line['_features']['unitPrice'], 100, 0),
            'originCountry' => $this->getOptionValueCodeByOptionValue('OP', $line['_features']['originCountry']),
            'origin' => $this->getOptionValueCodeByOptionValue('OV', $line['_features']['origin']),
            'salesUnit' => $this->getOptionValueCodeByOptionValue('UM', $line['_features']['salesUnit']),
            'orderUnit' => $this->getOptionValueCodeByOptionValue('UC', $line['_features']['orderUnit']),
            'conditioning' => $this->getOptionValueCodeByOptionValue('CO', $line['_features']['conditioning']),
            'category' => $this->getOptionValueCodeByOptionValue('CC', $line['_features']['category']),
            'caliber' => $this->getOptionValueCodeByOptionValue('CA', $line['_features']['caliber']),
            'naming' => $this->getOptionValueCodeByOptionValue('AP', $line['_features']['naming']),
            'bio' => $this->getOptionValueCodeByOptionValue('BI', $line['_features']['bio']),
            'brand' => $this->getOptionValueCodeByOptionValue('MQ', $line['_features']['brand']),
            'content' => 0,
            'unitQuantity' => $line['_features']['uq'],
            'unitContentQuantity' => $line['_features']['ucq'],
            'unitContentMeasurement' => $this->getOptionValueCodeByOptionValue('UCM', $line['_features']['ucm']),
        ];
    }
}