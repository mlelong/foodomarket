<?php

namespace AppBundle\Services\Fetcher;

class LegerMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $countOfColumnsByProduct[3] = 3; // Used value FILET de BOEUF 2,5 KG
        $countOfColumnsByProduct[4] = 3; // 3 ou 4 // Used value TOURNEDOS FILET  160/180/200gr
        $countOfColumnsByProduct[6] = 3; // Used value BAVETTE DE FLANCHET S/P
        $countOfColumnsByProduct[7] = 3; // Used value
        $countOfColumnsByProduct[8] = 4; // Used value BROCHETTE de POULET Maison - Filet
        $countOfColumnsByProduct[9] = 4; // Used value
        $cutAt = 0;

        $products = [];
        foreach ($lines as $key => $line) {

            if ($line == '') {
                continue;
            }
            $infosLine = explode("\t", $line);
            array_shift($infosLine);

            if (! in_array(count($infosLine), array_keys($countOfColumnsByProduct))) {
                continue;
            }

            $cutAt = $countOfColumnsByProduct[count($infosLine)];
            if (array_key_exists($cutAt-1, $infosLine) && in_array(strtoupper(trim($infosLine[$cutAt-1])), ['KG', 'BTE', 'PCE'])) {
                $cutAt++;
            }

            $insertionkey = 0;
            $productLine = [];

            foreach ($infosLine as $productInfosKey => $info) {
                if ($productInfosKey == $cutAt && $info == '') {
                    continue;
                }
                $productLine[] = $info;
                $insertionkey++;

                if ($insertionkey % $cutAt == 0) {

                    $insertionkey = 0;
                    $extracts = $this->format($productLine);
                    if (is_array($extracts)) {
                        foreach($extracts as $extract) {
                            $products[] = $extract;
                        }
                    }
                    $productLine = [];
                }
            }
        }
        return $products;
    }

    protected function format($line)
    {

        if ($line[0] == "" ||
            $line[1] == "" ||
            $line[2] == "" ||
            in_array($line[0], ['Code'])
        ) {
            return;
        }

        $columnForPrice = count($line)-1;

        $name = $line[1];
        // remove multiple space
        $name = preg_replace('/\s+/', ' ', $name);
        $name = preg_replace('/\*\s/', '', $name);

        $country = '';
        $conditioning = '';
        $content = 1;
        $weight = 1;
        $variant = '1 KG'; // REPLACE BY $unitContentQuantity / $unitContentMeasurement
        $unitQuantity = 1;
        $unitContentQuantity = 1;
        $unitContentMeasurement = 'KG';

        $variantWeight = [];
        $salesUnit = 'KG';
        $KgOrGr = 'KG';

        $patternWeight = "/([\/.,\d\s]+\s*(KG|GR|G))/i";
        preg_match($patternWeight, $name, $variantWeight);

        if (array_key_exists(1, $variantWeight) && intval($variantWeight[1]) > 0) {
            if (array_key_exists(2, $variantWeight)) {
                $KgOrGr = strtoupper($variantWeight[2]);
                if ($KgOrGr == 'G') {
                    $KgOrGr = 'GR';
                }
            }
            $weight = str_ireplace(['KG', 'GR', 'G', ' '], '', $variantWeight[1]);
            $weight = str_replace(',', '.', $weight);
            $salesUnit = 'PC';
            $variant = '1 Pièce de '.$weight.' '.$KgOrGr;
            $unitContentQuantity = $weight;
            $unitContentMeasurement = $KgOrGr;
        }

        $price = 0;
        $line[$columnForPrice] = str_replace([' ', '€'], '', $line[$columnForPrice]);
        if (floatval($line[$columnForPrice] > 0)) {
            $price = floatval($line[$columnForPrice]*100);
        }

        if ($KgOrGr == 'GR') {
            $weights = explode('/', $weight);
            $weightsKg = [];
            $variantNames = [];
            $unitContentQuantities = [];
            $unitContentMeasurements = [];
            foreach($weights as & $weightgr) {
                if ($weightgr > 0) {
                    $variantNames[] = '1 Pièce de ' . $weightgr . ' ' . $KgOrGr;
                    $unitContentQuantities[] = $weightgr;
                    $unitContentMeasurements[] = $KgOrGr;
                    $weightsKg[] = $weightgr / 1000;
                }
            }
            $weight = implode('|', $weightsKg);
            $variant = implode('|', $variantNames);
            $unitContentQuantity = implode('|', $unitContentQuantities);
            $unitContentMeasurement = implode('|', $unitContentMeasurements);
        }

        $containing = "( )?(BOTTE|BOX|BARQUETTE|BARQ|BARQ\.|CAISSE|SAC(HET)?|PLATEAU|PCS|PIECES|VRAC|CAISSE|COLIS)";
        $pattern = '/' . $containing . '/';
        preg_match($pattern, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? 'CO_'.$conditioning[0] : null;

        $taxon = explode(' ', $name)[0];
        if (in_array($taxon, ['HF', 'BIO', 'MINI'])) {
            $taxon = explode(' ', $name)[1];
        }

        $priceKg = $price;
        $priceVariant = $price;
        // some prices calculation with some array for special cases
        if ($salesUnit == 'PC') {
            $priceUnits = [];
            $weights = explode('|', $weight);
            foreach($weights as & $weightgr) {
                $priceUnits[] = bcmul($price, $weightgr, 0);
            }
            $priceVariant = implode('|', $priceUnits);
        }

        $returnLines = [];

        $commonInfos = [
            'reference'    => '',
            'supplierName' => $line[1],
            'productName' => trim($name),
            'taxon' => $taxon,
            'price' => $price,
            'priceKg' => $priceKg,
            'originCountry' => '',
            'salesUnit' => $salesUnit,
            'orderUnit' => 'ORDER_'.$salesUnit,
            'conditioning' => trim($conditioning),
            ];

        if (strstr($variant, '|') !== false) {

            $names = explode('|', $variant);
            $weights = explode('|', $weight);
            $priceVariants = explode('|', $priceVariant);
            $unitContentQuantities = explode('|', $unitContentQuantity);
            $unitContentMeasurements = explode('|', $unitContentMeasurement);

                foreach($names as $key => $variantName) {
                    $unitContent = [
                        'unitQuantity' => 1,
                        'unitContentQuantity' => $unitContentQuantities[$key],
                        'unitContentMeasurement' => 'UCM_'.$unitContentMeasurements[$key],
                    ];

                    $returnLines[] = array_merge($commonInfos, $unitContent,
                        [
                            'variantName' => $names[$key],
                            'weight' => $weights[$key],
                            'priceVariant' => $priceVariants[$key],
                            'content' => $weights[$key],
                        ]
                        );
            }
        } else {

            $unitContent = [
                'unitQuantity' => 1,
                'unitContentQuantity' => $unitContentQuantity,
                'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
            ];

            $returnLines[] = array_merge($commonInfos, $unitContent,
                    [
                        'variantName' => $variant,
                        'weight' => $weight,
                        'priceVariant' => $priceVariant,
                        'content' => $weight,
                    ]
                    );
        }

        return $returnLines;
    }
}
