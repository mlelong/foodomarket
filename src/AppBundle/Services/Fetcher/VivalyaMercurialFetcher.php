<?php

namespace AppBundle\Services\Fetcher;

class VivalyaMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {
            if ($key < 2) {
                continue;
            }

            $product = $this->format($line);
            if ($product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($line)
    {
        $infos = explode("\t", $line);

        $name = trim(substr($infos[1], 0, -2));
        $weight = floatval(str_replace(',', '.', $infos[2]));
        $price = array_key_exists(3, $infos) ? str_replace(',', '.', $infos[3]) : null;
        $originCountry = substr($infos[1], -2);

        if (!$price) {
            return;
        }

        $price = floatval(str_replace(',', '.', $price)) * 100;

        // remove multiple space
        $name = preg_replace('/\s+/', ' ', $name);
        $name = preg_replace('/\*\s/', '', $name);

        $containing = "( )?(BOTTE|BOX|BARQUETTE|BARQ|BARQ\.|CAISSE|SAC(HET)?|PLATEAU|PCS|PIECES|VRAC|CAISSE|COLIS)";
        $pattern = '/' . $containing . '/';
        preg_match($pattern, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? $conditioning[0] : null;

        if ($weight == 1) {
            $salesUnit = 'KG';
        } else {
            $salesUnit = 'PC';
        }

        $priceVariant = bcmul($price, $weight, 0);
        $priceKg = $price;

        return [
            'reference'    => '',
            'supplierName' => $infos[1],
            'productName' => $name,
            'variantName' => $weight.' KGS',
            'taxon' => explode(' ', $name)[0],
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceVariant' => $priceVariant,
            'originCountry' => $originCountry,
            'salesUnit' => trim($salesUnit),
            'orderUnit' => trim($salesUnit),
            'conditioning' => $conditioning,
            'content' => '1'
        ];
    }
}
