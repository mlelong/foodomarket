<?php

namespace AppBundle\Services\Fetcher;

class DemarneMercurialFetcher extends Fetcher
{

    protected $format = 'text';

    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);
        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {
            if ($key < 11) {
                continue;
            }

            $aProducts = $this->format($line);
            foreach($aProducts as $product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($line)
    {
        $line = strval($line);
        $line = str_replace('*', '* ', $line);

        $pattern = "/(\w{3,7})\s(.+)\s(pièce|piece|pi|[\/,\d]*(kg|gr|pi|ml|g))\s([,\d]+\s€|Cours|rupture)\s(KG|UN|PI)/Ui";
        preg_match_all($pattern, $line, $multipleInfos, PREG_SET_ORDER);

        if (count($multipleInfos) == 0) {
            return [];
        }


        /*
            array:7 [
              0 => "8459 FILET DE MERLU bretagne 3KG 9,90 € KG"
              1 => "8459"
              2 => "FILET DE MERLU bretagne"
              3 => "3KG"
              4 => "KG"
              5 => "9,90 €"
              6 => "KG"
            ]
         */

        $aProducts = [];
        foreach($multipleInfos as $infos) {

            $reference = $infos[1];
            $name = $infos[2];
            $conditioning = $infos[3];
            $salesUnit = strtoupper($infos[6]);
            $originCountry = '';
            $price = intval(floatval(str_replace(',', '.', $infos[5])) * 100);
            $variant = '';
            $unitQuantity = 1;
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';

            switch ($conditioning) {
                case 'pi' :
                case 'piece' :
                    $conditioning = 'pièce';
                    break;
            }

            if ($conditioning == 'pièce') {
                $conditioning = 'CO_PIECE';
                $unitContentQuantity = 1;
                $unitContentMeasurement = 'PIECE';
            } else {
                $conditioning = '';
            }

            if ($salesUnit == 'PI') {
                $salesUnit = 'PC';
            }
            if ($salesUnit == 'UN') {
                $salesUnit = 'PC';
            }

            // remove multiple space
            $name = preg_replace('/\s+/', ' ', $name);
            $name = preg_replace('/\*\s/', '', $name);

            $patternCountry = "/(NORVÈGE|ECOSSE|HOLLANDE|FRANCE|Argentine)/i";
            preg_match($patternCountry, $name, $country);
            $originCountry = array_key_exists(0, $country) ? $country[0] : '';
            $name = trim(str_replace($originCountry, '', $name));

            $weight = 0;
            $content = 1;

            $KgOrGr = 'KG';
            if (array_key_exists(4, $infos) && $infos[4] != '') {
                $KgOrGr = strtoupper($infos[4]);
                if ($KgOrGr == 'GRS') {
                    $KgOrGr = 'GR';
                }
                if ($KgOrGr == 'G') {
                    $KgOrGr = 'GR';
                }

                $weight = floatval(str_replace(',', '.', $infos[3]));

                $unitContentQuantity = $weight;
                $unitContentMeasurement = $KgOrGr;

                $variant = $weight.' '.$KgOrGr;

                if ($content > 1) {
                    $variant = $content. ' X '.$variant;
                }
                if ($KgOrGr == 'GR') {
                    $weight = bcdiv($weight, 1000, 3);
                }
                if ($KgOrGr == 'PI') {
                    $weight = 0;
                }
            }

            if ($variant == '') {
                if ($salesUnit == 'KG') {
                    $variant = '1 KG';
                } else {
                    if ($content == 1) {
                        $variant = 'LA PIECE';
                    } else {
                        $variant = 'PAR '.$content;
                    }
                }
            }

            $totalWeight = bcmul($weight, $content, 3);

            if ($salesUnit == 'KG') {
                $priceKg = $price;
                $priceVariant = bcmul($weight, $priceKg, 0);
            } else {
                $priceKg = 99900;
                $priceVariant = $price;
                if ($totalWeight) {
                    $priceKg = bcdiv($priceVariant, $totalWeight, 0);
                }
            }

            $aProducts[] = [
                'reference'    => $reference,
                'supplierName' => $infos[2],
                'productName' => strtoupper($this->normalizeSpaces($name)),
                'variantName' => '-',
                'taxon' => explode(' ', $name)[0],
                'weight' => $totalWeight,
                'price' => $price,
                'priceKg' => $priceKg,
                'priceVariant' => $priceVariant,
                'originCountry' => $this->normalizeSpaces($originCountry),
                'salesUnit' => trim($salesUnit),
                'orderUnit' => 'ORDER_'.trim($salesUnit),
                'conditioning' => $conditioning,
                'category' => '',
                'caliber' => '',
                'naming' => '',
                'bio' => '',
                'brand' => '',
                'content' => 0,
                'unitQuantity' => $unitQuantity,
                'unitContentQuantity' => $unitContentQuantity,
                'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
            ];
        }

        return $aProducts;
    }
}
