<?php

namespace AppBundle\Services\Fetcher;

use Exception;

class JoceaneMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $products = [];

        try {
            $content = $this->client->{'get'.ucfirst($this->format)}($this->importPath . $filename);
            $nbProducts = preg_match_all('/(?<product>[ \S]+)\t(?<caliber>[ \S]*)\t(?<price>[\d.]+)/', $content, $matches);

            for ($i = 0; $i < $nbProducts; ++$i) {
                $products[$i] = $this->extractFeatures($matches['product'][$i], $matches['caliber'][$i], $matches['price'][$i]);
            }
        } catch (Exception $ignored) {
        }

        return $products;
    }

    private function extractFeatures($product, $caliber, $price)
    {
        $product = $this->normalizeSpaces($product);
        $caliber = $this->normalizeSpaces($caliber);

        // Weight can be extracted from name, or caliber
        if (preg_match('/(?<weight>\d+\s*(?:KG|G))/', $product, $matches)) {
            $rawWeight = $matches['weight'];
        } elseif (preg_match('/(?<weight>\d+\s*(?:KG|G))/', $caliber, $matches)) {
            $rawWeight = $matches['weight'];
        } else {
            $rawWeight = '1';
        }

        preg_match('/\d+/', $rawWeight, $matches);
        $weight = (float)$matches[0];
        $unit = trim(str_replace($rawWeight, '', $weight));

        if ($unit == 'G') {
            $weight /= 1000;
        }

        $tmpPrice = $price;
        $price *= 100;
        $priceKg = $price / $weight;
        $priceUnit = $price * $weight;
        $priceVariant = $weight == 1 ? $priceKg : $priceUnit;

        if (strpos($product, '*')  !== false) {
            $salesUnit = 'PC';
        } else {
            if (!$unit) {
                $salesUnit = 'KG';
            } else {
                $salesUnit = 'PC';
            }
        }

        $orderUnit = $salesUnit;

        return [
            'reference'    => '',
            'supplierName' => "$product $caliber",
            'productName' => strtoupper($product),
            'variantName' => '-',
            'taxon' => '',
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => '',
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($orderUnit),
            'conditioning' => '',
            'category' => '',
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => 'NOBIO',
            'brand' => '',
            'content' => 0,
            'unitQuantity' => 1,
            'unitContentQuantity' => 1,
            'unitContentMeasurement' => $unit ? 'UCM_'.strtoupper($unit) : 'KG',
        ];
    }
}

