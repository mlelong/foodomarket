<?php


namespace AppBundle\Services\Fetcher;


class CentraleDuFraisMercurialCsvFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $content = $this->getContentAsText($filename);

        if (empty($content)) {
            return null;
        }

        if ($nbProducts = preg_match_all("/(?<product>[\\\\\/a-zA-Z0-9+()\- ]+);(?<price>[\d.]+);/", $content, $matches)) {
            $products = [];

            for ($i = 0; $i < $nbProducts; ++$i) {
                $products[] = $this->getProduct($matches['product'][$i], $matches['price'][$i]);
            }

            return $products;
        }

        return null;
    }

    private function getProduct(string $product, string $price)
    {
        $productNormalized = $this->normalizeSpaces($product);
        $price100 = bcmul($price, 100);

        return [
            'reference'    => '',
            'supplierName' => $productNormalized,
            'productName' => $productNormalized,
            'variantName' => '-',
            'taxon' => '',
            'weight' => 1,
            'price' => $price100,
            'priceKg' => $price100,
            'priceUnit' => $price100,
            'priceVariant' => $price100,
            'originCountry' => '',
            'originCity' => '',
            'salesUnit' => 'KG',
            'orderUnit' => "ORDER_KG",
            'conditioning' => '',
            'category' => '',
            'caliber' => '',
            'naming' => '',
            'bio' => 'NOBIO',
            'brand' => '',
            'content' => 0,
            'unitQuantity' => 1,
            'unitContentQuantity' => 1,
            'unitContentMeasurement' => 'KG',
            'line' => "$product | $price €"
        ];
    }
}