<?php

namespace AppBundle\Services\Fetcher;

class HallesPrestigeMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {

            if ($key < 10) {
                continue;
            }

            $line = iconv('UTF-8', 'ASCII//TRANSLIT', $line);

            $line = trim(str_replace([
                'LES PRIX SONT INDICATIFS',
                'ILS PEUVENT ETRE MODIFIES',
                'SUIVANT LE COURS DU MARCHE TOMATE',
                'PRODUITS DE SAISON',
                'NOTRE ADRESSE:',
                '102 RUE DE CARPENTRA',
                'BAT D3       PLA 312 94612',
                'RUNGIS CEDEX',
            ], '', $line));

            // extract the products from pdf line
            $pattern = "/([a-z]{1}(?:\w|\s|\'|’|\+|\.|\/)+)(KG|BQ|PCS|PC|COLIS|BTTE|BTE|JG|NC)+\s([\d,]+)/i";
            preg_match_all($pattern, $line, $infos);

            if (strpos($line, "\t") != false) {
                continue;
            }

            if (count($infos[0]) == 0) {
                continue;
            }

            $productToImport = [];
            for($i = 0; $i < count($infos[0]); $i++) {
                $productToImport[] =  $infos[1][$i];
                $productToImport[] =  $infos[2][$i];
                $productToImport[] =  $infos[3][$i];

                $product = $this->format($productToImport);

                if ($product) {
                    $products[] = $product;
                }
                $productToImport = [];
            }
        }

        return $products;
    }

    protected function format($line)
    {
        if ($line[0] == "" ||
            $line[1] == "" ||
            $line[2] == "" ||
            in_array($line[0], ['FRUITS', 'LEGUMES', 'CHAMPIGNONS'])
        ) {
            return;
        }

        $name = $line[0];
        $name = trim(str_replace('au kg', '', $name));
        $conditioning = '';
        $salesUnit = '';
        $unitQuantity = 1;
        $unitContentQuantity = 0;
        $unitContentMeasurement = 'KG';

        switch(strtoupper($line[1])) {
            case 'KG':
                $salesUnit = 'KG';
                break;
            case 'COLIS':
                $salesUnit = 'CO';
                break;
            default:
                $salesUnit = 'PC';
                break;
        }

        $patternCountry = "/(fr|France|ghana|costa rica|esp|espagne|martinique|chine|Italie|Allemagne|all|peru|hollande|holl|hol(l)?|bresil|colombie|usa|belg|kenya|maroc|chilie|isr|israel|afr sud|afr du sud|portugal|thai|reunion|pol|pologne)$/i";
        preg_match($patternCountry, $name, $country);
        $originCountry = array_key_exists(0, $country) ? $country[0] : '';
        $name = preg_replace($patternCountry, '', $name, 1);

        if ($salesUnit == 'PC') {
            $patternContent = "/(X |X)[0-9]+/";
            preg_match($patternContent, $name, $unitQuantity);
            $unitQuantity = array_key_exists(0, $unitQuantity) ? str_replace('X', '', $unitQuantity[0]) : '1';
        }

        $caliber = '';
        $patternCaliber = '/(\d+\/\d+)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $patternCaliber = '/CAL (\d+)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $name = str_replace($caliber, '', $name);

        $patternConditioning = "( )?(PLQ|BOTTE|BOX|BQ|BARQUETTE|BARQ|BARQ\.|CAISSE|SAC(HET)?|PLATEAU|PCS|PIECES|VRAC|CAISSE|COLIS)";
        $patternConditioning = '/' . $patternConditioning . '/i';
        preg_match($patternConditioning, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? trim($conditioning[0]) : '';
        switch (strtoupper($line[1])) {
            case "BQ" :     $conditioning = 'BARQUETTE'; break;
            case "BTTE" :   $conditioning = 'BOTTE'; break;
            case "BTE" :    $conditioning = 'BOITE'; break;
            case "COLIS" :  $conditioning = 'COLIS'; break;
        }
        $name = str_replace($conditioning, '', $name);
        if (!empty($conditioning)) {
            $conditioning = 'CO_'.$conditioning;
        }

        if (empty($conditioning)) {
            if ($salesUnit == 'KG') {
                $conditioning = 'CO_VRAC';
            }
        }

        $taxon = explode(' ', $name)[0];
        if (in_array($taxon, ['HF', 'BIO', 'MINI'])) {
            $taxon = explode(' ', $name)[1];
        }

        $weight = 0;
        $price = floatval(str_replace(',', '.', $line[2])) * 100;

        if ($salesUnit == 'KG') {
            $weight = 1;
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';
        } else {
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'PIECE';
        }

        $patternWeight = "/([\/.,\d\s]+\s*(KG|GR))/i";
        preg_match($patternWeight, $name, $variantWeight);

        if (array_key_exists(1, $variantWeight)) {
            $unitContentMeasurement = 'KG';
            if (array_key_exists(2, $variantWeight)) {
                $unitContentMeasurement = strtoupper($variantWeight[2]);
            }
            $weight = str_ireplace(['KG', 'GR', ' '], '', $variantWeight[1]);
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $weight));

            if ($unitContentMeasurement == 'GR') {
                $weight = bcdiv($weight, 1000, 3);
            }
        }

        if ($salesUnit == 'PC') {
            if ($weight > 0) {
                $priceKg = bcdiv($price, $weight, 0);
            } else {
                $priceKg = 0;
            }
            $priceUnit = $price;
            $priceVariant = $price;
        } else {
            $priceKg = $price;
            $priceUnit = bcmul($price, $weight, 0);
            $priceVariant = bcmul($price, $weight, 0);
        }

        return [
            'reference'    => '',
            'supplierName' => $this->normalizeSpaces($line[0]),
            'productName' => strtoupper($this->normalizeSpaces($name)),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => $this->normalizeSpaces($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($salesUnit),
            'conditioning' => trim($conditioning),
            'category' => '',
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => '',
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
