<?php

namespace AppBundle\Services\Fetcher;

class LalauzeMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {

        $products = [];

        foreach ($lines as $key => $line) {

            if ($key == 0 || $key == 1) {
                continue;
            }

            // if column 8 is empty, then add it.
            $search = ["€\tkilo", "€\tà la pièce", "€\tlitre"];
            $replace = ["€\t\tkilo", "€\t\tà la pièce", "€\t\\tlitre"];
            $line = str_replace($search, $replace, $line);

            $infosLine = explode("\t", $line);

            // sometimes, there is an empty column A that should be removed
            if ($infosLine[0] == '') {
                array_shift($infosLine);
            }

            $product = $this->format($infosLine);
            //dump('PRODUCT', $product);
            $products[] = $product;
        }

        return $products;
    }

    protected function format($line)
    {
        //dump('LINE', $line);

        if ($line[0] == "Ref." || $line[0] == "Feuil1") {
            return;
        }

        $reference = $line[0];
        $name = $line[5];
        $taxon = $line[3];
        $originCountry = strtoupper($line[10]);

        $content = 0;
        $salesUnit = '';
        $orderUnit = 'ORDER_CO';
        $unitQuantity = 1;
        $unitContentQuantity = 0;
        $unitContentMeasurement = 'KG';

        // country
        if ($originCountry == 'UE') {
            $originCountry = 'Europe';
        }
        if ($originCountry == 'UK/Irlande') {
            $originCountry = 'Irlande';
        }
        if ($originCountry == 'France/Allemagne') {
            $originCountry = 'France';
        }

        // taxon
        if ($taxon == 'Saucisson, lard, jambon') {
            $taxon = $line[4];
        }

        $search = [
            'Viande bovine',
            ];
        $replace = [
            'Boeuf',
            ];
        $taxon = str_replace($search, $replace, $taxon);
        $taxon = explode(' ', $taxon)[0];

        switch(strtoupper($line[8])) {
            case 'KILO':
                $salesUnit = 'KG';
                break;
            case 'LITRE':
                $salesUnit = 'L';
                break;
            case 'COLIS':
                $salesUnit = 'COLIS';
                break;
            case 'A LA PIECE':
                $salesUnit = 'PC';
                break;
            default:
                $salesUnit = 'PC';
                break;
        }

        $caliber = '';
        $conditioning = '';

        $totalWeight = floatval(str_replace(',', '.', $line[9]));
        $price = floatval(str_replace(',', '.', $line[6])) * 100;

        // 5 filets 180g - sous vide => 5 !!
        $patternContent = "/^[0-9]+/i";
        preg_match($patternContent, $name, $unitQuantity);
        $unitQuantity = array_key_exists(0, $unitQuantity) ? $unitQuantity[0] : '1';

        $patternWeight = "/([\/.,\d\s]+\s*(KG|GR|G))/i";
        preg_match($patternWeight, $name, $variantWeight);

        if (array_key_exists(1, $variantWeight) && strlen($variantWeight[1]) > 3) {
            $unitContentMeasurement = 'KG';
            if (array_key_exists(2, $variantWeight)) {
                $unitContentMeasurement = strtoupper($variantWeight[2]);
            }
            $weight = str_ireplace(['KG', 'GR', 'G', ' '], '', $variantWeight[1]);
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $weight));

            if (in_array($unitContentMeasurement, ['GR', 'G'])) {
                $weight = bcdiv($weight, 1000, 3);
            }
        }

        if ($unitContentMeasurement == 'G') {
            $unitContentMeasurement = 'GR';
        }

        if ($unitContentQuantity == 0) {
            $unitContentQuantity = $totalWeight;
        }

        $priceVariant = 0;
        if ($salesUnit == 'KG') {
            $priceKg = $price;
            $priceUnit = bcmul($price, $totalWeight, 0);
            $priceVariant = bcmul($price, $totalWeight, 0);
        }

        if ($salesUnit == 'PC') {
            $priceUnit = $price;
            $priceVariant = bcmul($unitQuantity, $price, 0);
            if ($totalWeight) {
                $priceKg = bcdiv($priceVariant, $totalWeight, 0);
            } else {
                $priceKg = 0;
            }
        }

        if ($salesUnit == 'CO' || $salesUnit == 'COLIS') {
            if ($totalWeight) {
                $priceKg = bcdiv($price, $totalWeight, 0);
            } else {
                $priceKg = 0;
            }
            $priceUnit = $price;
            $priceVariant = $price;
        }

        if ($salesUnit == 'L') {
            $priceUnit = $price;
            $priceVariant = bcmul($unitQuantity, $price, 0);
            if ($weight) {
                $priceKg = bcdiv($priceVariant, $totalWeight, 0);
            } else {
                $priceKg = 0;
            }
        }

        $bio = 'NOBIO';

        foreach($this->optionsValues as $option => $value) {
            ${$option} = $value;
        }

        if ($priceVariant == 0) {
            return false;
        }
        
        return [
            'reference'    => $reference,
            'supplierName' => $this->normalizeSpaces($line[5]),
            'productName' => strtoupper($this->normalizeSpaces($name)),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $totalWeight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => $this->normalizeSpaces($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => $orderUnit,
            'conditioning' => trim($conditioning),
            'category' => '',
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
