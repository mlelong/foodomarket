<?php

namespace AppBundle\Services\Fetcher;

class EpisaveursMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {
            if ($key < 7) {
                continue;
            }
            $product = $this->format($line);
            if ($product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($line)
    {
        $infos = explode("\t", $line);

        if (count($line) < 5 && !floatval($line[4])) {
            return;
        }

        $reference = $infos[1];
        $name = $infos[3];
        $conditioning = $infos[2];
        $weight = 0;
        $price = floatval(bcmul($infos[4], 100, 0));
        $salesUnit = 'PC';
        $KgOrGr = '';
        $unitQuantity = 1;
        $unitContentQuantity = 0;
        $unitContentMeasurement = 'KG';

        // remove multiple space
        $name = preg_replace('/\s+/', ' ',$name);
        $name = preg_replace('/\*\s/', '',$name);

        switch ($conditioning) {
            case "PAQ" :     $conditioning = 'PAQUET'; break;
            case "BQ" :     $conditioning = 'BARQUETTE'; break;
            case "BTTE" :   $conditioning = 'BOTTE'; break;
            case "BTE" :    $conditioning = 'BOITE'; break;
            case "COLIS" :  $conditioning = 'COLIS'; break;
            case "COL" :    $conditioning = 'COLIS'; break;
        }
        $conditioning = 'CO_'.$conditioning;

        /*
        $patternContent = "/(X |X)(\d+)/i";
        preg_match($patternContent, $name, $variantContent);
        if (array_key_exists(2, $variantContent)) {
            $unitQuantity = $variantContent[2];
        }
        */

        $variant = $unitQuantity.' '.$conditioning;

        $patternWeight = "/([.,\d]+(KG|GR|G|L|U|F))(x(\d+))?/i";
        preg_match($patternWeight, $name, $variantWeight);
        if (array_key_exists(1, $variantWeight)) {
            if (array_key_exists(2, $variantWeight)) {
                $unitContentMeasurement = strtoupper($variantWeight[2]);
            }
            $weight = str_ireplace($unitContentMeasurement, '', $variantWeight[1]);
            $weight = str_replace(',', '.', $weight);
            $variant = $unitQuantity.' '.$conditioning.'(s) de '.$weight.' '.$KgOrGr;
            $unitContentQuantity = $weight;

            if (in_array($unitContentMeasurement, ['GR', 'G'])) {
                $weight = bcdiv($weight, 1000, 3);
            }
            if (in_array($unitContentMeasurement, ['GR', 'G', 'KG'])) {
                $weight = bcmul($weight, $unitQuantity, 3);
            } else {
                $weight = 0;
            }
        }

        $priceVariant = bcmul($price, $unitQuantity, 0);
        $priceKg = 0;
        if ($weight > 0) {
            $priceKg = bcdiv($priceVariant, $weight, 0);
        }

        return [
            'reference'    => $reference,
            'supplierName' => $this->normalizeSpaces($infos[3]),
            'productName' => $this->normalizeSpaces($name),
            'variantName' => $variant,
            'taxon' => explode(' ', $name)[0],
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceVariant' => $priceVariant,
            'originCountry' => '',
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($salesUnit),
            'conditioning' => trim($conditioning),
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
