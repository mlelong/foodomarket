<?php

namespace AppBundle\Services\Fetcher;

class ReynaudMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $products = [];
        $content = $this->getContentAsText($filename);

        if ($content !== null) {
            $nbProducts = preg_match_all('/(?<ref>[\d\/]*)\s+(?<product>[\S ]+)\s+(?<cond>[\S ]+)\s+(?<price>[\d.,]+)(\s*€|\/\s*kg)/', $content, $matches);

            for ($i = 0; $i < $nbProducts; ++$i) {
                $reference = $matches['ref'][$i];

                if (strpos($reference, '/') !== false) {
                    $references = explode('/', $reference);

                    preg_match('/^(?<prefix>[\S ]*)\s+(?<p1>[a-zA-Z]+)\/(?<p2>[a-zA-Z]+)\s+(?<suffix>[\S ]+)$/i', $matches['product'][$i], $splittedProduct);

                    if (empty($splittedProduct['p1']) || empty($splittedProduct['p2'])) {
                        break ;
                    }

                    $products[] = $this->getProduct($references[0], "{$splittedProduct['prefix']} {$splittedProduct['p1']} {$splittedProduct['suffix']}", $matches['cond'][$i], $matches['price'][$i]);
                    $products[] = $this->getProduct($references[1], "{$splittedProduct['prefix']} {$splittedProduct['p2']} {$splittedProduct['suffix']}", $matches['cond'][$i], $matches['price'][$i]);

                    continue ;
                }

                $products[] = $this->getProduct($matches['ref'][$i], $matches['product'][$i], $matches['cond'][$i], $matches['price'][$i]);
            }
        }

        return $products;
    }

    private function getProduct(string $reference, string $productName, string $conditionning, string $price)
    {
        $supplierName = !empty($reference) ? "$productName ($reference)" : $productName;
        $price = floatval(str_replace([',', ' '], ['.', ''], $price));
        $priceKg = $price;
        $priceUnit = $price;
        $priceVariant = $price;

        preg_match('/^x?\s*(?<weight>[\d.,\/]*)\s*(?<unit>[\S]+)/', $conditionning, $cond);

        $weight = 1;
        $unitQuantity = 1;
        $unitContentQuantity = 1;
        $unitContentMeasurement = 'KG';

        switch (strtolower($cond['unit'])) {
            case 'l':
                $salesUnit = 'L';
                $orderUnit = 'PC';
                $weight = isset($cond['weight']) ? floatval(str_replace(',', '.', $cond['weight'])) : 1;
                $unitContentQuantity = $weight;
                $unitContentMeasurement = 'L';
                break ;
            case 'colis':
                $salesUnit = 'CO';
                $orderUnit = 'CO';
                $unitContentMeasurement = 'PC';
                break ;
            case 'kg':
                $salesUnit = 'KG';
                $orderUnit = 'PC';
                $weight = floatval(str_replace(',', '.', $cond['weight']));
                $unitContentQuantity = $weight;
                break ;
            case 'g':
            case 'gr':
                $weight = floatval(str_replace(',', '.', $cond['weight'])) / 1000;
                $orderUnit = 'PC';
                $salesUnit = 'PC';
                $unitContentQuantity = floatval(str_replace(',', '.', $cond['weight']));
                $unitContentMeasurement = 'GR';
                break ;
            default:
                $salesUnit = 'PC';
                $unitContentMeasurement = 'PC';

                if (empty($cond['weight']) || $cond['weight'] == 1) {
                    $orderUnit = 'PC';
                } else {
                    $orderUnit = 'CO';
                    $unitQuantity = (int)$cond['weight'];
                }

                break ;
        }

        if (preg_match('/\s+BIO(\s|$)/i', $supplierName)) {
            $bio = 'BIO';
        } else {
            $bio = 'NOBIO';
        }

        if ($salesUnit === 'KG') {
            $priceUnit = bcmul($price, $weight, 3);
            $priceVariant = $priceUnit;
        } else {
            $priceKg = bcdiv($price, $weight, 3);
        }

        $priceUnit = bcmul($priceUnit, $unitQuantity, 3);
        $priceVariant = bcmul($priceVariant, $unitQuantity, 3);

        $originalPrice = $price;
        $price = bcmul($price, 100);
        $priceKg = bcmul($priceKg, 100);
        $priceUnit = bcmul($priceUnit, 100);
        $priceVariant = bcmul($priceVariant, 100);

        return [
            'reference'    => $reference,
            'supplierName' => $supplierName,
            'productName' => $productName,
            'variantName' => '-',
            'taxon' => '',
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => '',
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($orderUnit),
            'conditioning' => '',
            'category' => '',
            'caliber' => '',
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
            'line' => "{$reference} {$productName} {$conditionning} {$originalPrice}€"
        ];
    }
}

