<?php

namespace AppBundle\Services\Fetcher;

class DynamisMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];
        foreach ($lines as $key => $line) {

            if ($key < 30 || $line == '') {
                continue;
            }

            // extract the products from pdf line
            $pattern = "/(.+)\s(BIO|DEMETER)(.*)\s(\w{2,3})\s(\d{1,2},\d{1,2})\s(.*)\s(\d{1,2},\d{1,2})\s(Kg|Co|Un)\w*/i";
            preg_match($pattern, $line, $infos);

            $product = $this->format($infos);

            if ($product) {dump($product);
                $products[] = $product;
            }
            $productToImport = [];
        }

        return $products;
    }

    protected function format($line)
    {
        if (
            count($line) == 0 ||
            $line[0] == "" ||
            $line[1] == ""
        ) {
            return;
        }

        $name = $line[1];
        $originCountry = $line[4];
        $conditioning = 'CO_COLIS';
        $taxon = explode(' ', $name)[0];
        $unitQuantity = 1;
        $unitContentQuantity = 1;
        $unitContentMeasurement = 'KG';
        $weight = str_replace(',', '.', $line[5]);
        $price = floatval(str_replace(',', '.', $line[7])) * 100;

        $salesUnit = '';
        switch(strtoupper($line[8])) {
            case 'KG':
                $salesUnit = 'KG';
                break;
            case 'UN':
                $salesUnit = 'PC';
                break;
            case 'CO':
                $salesUnit = 'CO';
                break;
        }

        $searchContent = [];
        $patternContent = "/([0-9]+( X|X))/i";
        preg_match($patternContent, $line[3], $searchContent);

        if (array_key_exists(1, $searchContent)) {
            $unitQuantity = str_ireplace('X', '', $searchContent[1]);
        } else {
            $patternContent = "/\s*([0-9]+)\s\w+/i";
            preg_match($patternContent, $line[3], $searchContent);
            if (array_key_exists(1, $searchContent)) {
                $unitQuantity = $searchContent[1];
            } else {
                $patternContent = "/([0-9]+P)/i";
                preg_match($patternContent, $line[3], $searchContent);
                if (array_key_exists(1, $searchContent)) {
                    $unitQuantity = str_ireplace('P', '', $searchContent[1]);
                }
            }
        }

        $KgOrGr = '';
        $unitWeight = 0;
        $patternWeight = "/([\/.,\d\s]+\s*(KG|GR|G))/i";
        preg_match($patternWeight, $line[3], $variantWeight);
        if (array_key_exists(1, $variantWeight)) {
            if (array_key_exists(2, $variantWeight)) {
                $unitContentMeasurement = $KgOrGr = strtoupper($variantWeight[2]);
            }
            $unitWeight = str_ireplace(['KG', 'GR', 'G', ' '], '', $variantWeight[1]);
            $unitContentQuantity = $unitWeight = floatval(str_replace(',', '.', $unitWeight));

            if ($KgOrGr == 'GR' || $KgOrGr == 'G') {
                $unitContentMeasurement = 'GR';
                $unitWeight = bcdiv($unitWeight, 1000, 3);
            }
        } else {
            if ($salesUnit == 'KG') {
                $unitQuantity = $weight;
                $unitContentQuantity = 1;
                $unitContentMeasurement = 'KG';
            } else {
                $unitContentQuantity = 1;
                $unitContentMeasurement = 'PIECE';
            }

        }

        if ($weight > 0 && $unitWeight > 0) {
            $unitQuantity =  bcdiv($weight, $unitWeight, 3);
        }

        $caliber = '';
        $patternCaliber = '/(\d+\/\d+)/i';
        if (preg_match($patternCaliber, $line[3], $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $patternCaliber = '/^\s*((?:>|\+)*\s*\d+(?:>|\+)*)$/i';
        if (preg_match($patternCaliber, $line[3], $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }

        if ($salesUnit == 'PC') {
            $priceUnit = bcmul($price, $unitQuantity, 0);
            $priceVariant = bcmul($price, $unitQuantity, 0);
            $priceKg = bcdiv($priceVariant, $weight, 0);
        } else if ($salesUnit == 'KG') {
            $priceKg = $price;
            $priceUnit = bcmul($price, $weight, 0);
            $priceVariant = bcmul($price, $weight, 0);
        } else if ($salesUnit == 'CO') {
            $priceKg = bcdiv($price, $weight, 0);
            $priceUnit = $price;
            $priceVariant = $price;
        }

        if ($line[2] == 'BIO') {
            $bio = 'BIO';
        } else {
            $bio = 'DEM';
        }

        return [
            'reference'    => '',
            'supplierName' => $this->normalizeSpaces($line[1]),
            'productName' => strtoupper($this->normalizeSpaces($name)),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => $this->normalizeSpaces($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_PC',
            'conditioning' => trim($conditioning),
            'category' => '',
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
