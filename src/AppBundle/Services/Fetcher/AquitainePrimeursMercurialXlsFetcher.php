<?php


namespace AppBundle\Services\Fetcher;


class AquitainePrimeursMercurialXlsFetcher extends Fetcher
{
    public function getProducts(string $filename)
    {
        $products = [];

        try {
            $output = $this->client->getText($this->importPath . $filename);

            if ($nbProducts = preg_match_all('/\t(?<product>[\S ]+)\t+(?<price>[\d.,]+)\t+(?<unit>[\S]+)\n/', $output, $matches)) {
                for ($i = 0; $i < $nbProducts; ++$i) {
                    $unit = $matches['unit'][$i];
                    $salesUnit = $unit === 'KG' ? 'KG' : 'PC';
                    $orderUnit = "ORDER_$salesUnit";
                    $price = $matches['price'][$i];
                    $price100 = bcmul(str_replace(',', '.', $price), 100);

                    $products[] = [
                        'reference'    => '',
                        'supplierName' => $matches['product'][$i],
                        'productName' => $matches['product'][$i],
                        'variantName' => '-',
                        'taxon' => '',
                        'weight' => 1,
                        'price' => $price100,
                        'priceKg' => $price100,
                        'priceUnit' => $price100,
                        'priceVariant' => $price100,
                        'originCountry' => '',
                        'originCity' => '',
                        'salesUnit' => $salesUnit,
                        'orderUnit' => $orderUnit,
                        'conditioning' => !in_array($unit, ['KG', 'PC', 'PIECE']) ? $unit : '',
                        'category' => '',
                        'caliber' => '',
                        'naming' => '',
                        'bio' => 'NOBIO',
                        'brand' => '',
                        'content' => 0,
                        'unitQuantity' => 1,
                        'unitContentQuantity' => 1,
                        'unitContentMeasurement' => $salesUnit,
                        'line' => "{$matches['product'][$i]} | {$matches['price'][$i]} | {$matches['unit'][$i]}"
                    ];
                }
            }
        } catch (\Exception $e) {
        }

        return $products;
    }
}