<?php


namespace AppBundle\Services\Fetcher;


use Exception;

class SainfruitMercurialXlsFetcher extends Fetcher
{
    public function getProducts(string $filename)
    {
        $products = [];

        try {
            $output = $this->client->getText($this->importPath . $filename);

            if ($nbProducts = preg_match_all('/(?<ref>[\S]+)\s+(?<product>[\S ]+)\s+(?<unit>[\S]{1})\s+(?<price>[\d.,]+)\s+(?<weight>[\d.,]+)/', $output, $matches)) {
                for ($i = 0; $i < $nbProducts; ++$i) {
                    $ref = trim($matches['ref'][$i]);
                    $product = $matches['product'][$i];
                    $unit = trim(strtoupper($matches['unit'][$i]));
                    $price = trim($matches['price'][$i]);
                    $weight = floatval(str_replace([',', ' '], ['.', ''], $matches['weight'][$i]));

                    $price100 = bcmul(str_replace(',', '.', $price), 100);

                    switch ($unit) {
                        case 'P':
                            $um = 'PC';
                            $uc = 'ORDER_PC';
                            break ;
                        case 'C':
                            $um = 'COLIS';
                            $uc = 'ORDER_COLIS';
                            break ;
                        case 'N':
                        default:
                            $um = 'KG';
                            $uc = 'ORDER_KG';
                            break ;
                    }

                    $products[] = [
                        'reference'    => $ref,
                        'supplierName' => $this->normalizeSpaces($product),
                        'productName' => strtoupper($this->normalizeSpaces($product)),
                        'variantName' => '-',
                        'taxon' => '',
                        'weight' => $weight,
                        'price' => $price100,
                        'priceKg' => $price100,
                        'priceUnit' => $price100,
                        'priceVariant' => $price100,
                        'originCountry' => '',
                        'salesUnit' => $um,
                        'orderUnit' => $uc,
                        'conditioning' => '',
                        'category' => '',
                        'caliber' => '',
                        'naming' => '',
                        'bio' => '',
                        'brand' => '',
                        'content' => 0,
                        'unitQuantity' => 1,
                        'unitContentQuantity' => 1,
                        'unitContentMeasurement' => $um === 'PC' ? 'UCM_PC' : 'UCM_KG',
                        'line' => "$ref $product $unit $price"
                    ];
                }
            }
        } catch (Exception $e) {
        }

        return $products;
    }
}
