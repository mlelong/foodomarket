<?php

namespace AppBundle\Services\Fetcher;

use AppBundle\Entity\ProductOptionValue;
use Cocur\Slugify\Slugify;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductOptionRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Vaites\ApacheTika\Client;

abstract class Fetcher
{
    protected $format = 'text';

    protected $optionsValues = [];

    protected $client;

    protected $importPath;

    /** @var ProductOptionRepository */
    private $productOptionRepository;

    /** @var EntityRepository */
    private $productOptionValueRepository;

    public function __construct(Client $client, $importPath, ProductOptionRepository $productOptionRepository, EntityRepository $productOptionValueRepository)
    {
        $this->client = $client;
        $this->importPath = $importPath;
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionValueRepository = $productOptionValueRepository;
    }

    public function forceOptionsValues($values) {
        $this->optionsValues = $values;
    }

    protected function extractLines($filename)
    {
        $extract = $this->client->{'get'.ucfirst($this->format)}($this->importPath . $filename);

        $lines = explode("\n", $extract);

        return $lines;
    }

    /**
     * @param string $filename
     * @return string|null
     */
    protected function getContentAsText(string $filename)
    {
        try {
            return $this->client->getText($this->importPath . $filename);
        } catch (\Exception $e) {
            return null;
        }
    }

    protected function normalizeSpaces($str) {
        return trim(preg_replace('/[\s]{2,}/', ' ' , $str));
    }

    protected function getOptionValueCodeByOptionValue(string $optionCode, string $optionValue)
    {
        if (empty($optionValue)) {
            return '';
        }

        static $staticCodes = [
            'UCM' => [
                'Kilogramme' => 'UCM_KG',
                'Gramme' => 'UCM_GR',
                'Litre' => 'UCM_L',
                'Centilitre' => 'UCM_CL',
                'Millilitre' => 'UCM_ML',
                'Piece' => 'UCM_PIECE',
                'Bouteille' => 'UCM_BOUTEILLE'
            ],
            'UM' => [
                'KG' => 'KG',
                'L' => 'L',
                'PC' => 'PC',
                'COLIS' => 'CO'
            ],
            'UC' => [
                'KG' => 'ORDER_KG',
                'KGPC' => 'ORDER_KGPC',
                'L' => 'ORDER_L',
                'PC' => 'ORDER_PC',
                'COLIS' => 'ORDER_CO'
            ]
        ];

        if (isset($staticCodes[$optionCode][$optionValue])) {
            return $staticCodes[$optionCode][$optionValue];
        }

        $qb = $this->productOptionValueRepository->createQueryBuilder('pov');

        /** @var ProductOptionValue[] $optionValues */
        $optionValues = $qb
            ->innerJoin('pov.option', 'po')
            ->innerJoin('pov.translations', 't')
            ->where('po.code = :option_code')
            ->setParameter('option_code', $optionCode)
            ->andWhere('t.value = :option_value')
            ->setParameter('option_value', "$optionValue")
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (!empty($optionValues)) {
            return $optionValues[0]->getCode();
        }

        $qb = $this->productOptionValueRepository->createQueryBuilder('pov');

        /** @var ProductOptionValue[] $optionValues */
        $optionValues = $qb
            ->innerJoin('pov.option', 'po')
            ->innerJoin('pov.translations', 't')
            ->where('po.code = :option_code')
            ->setParameter('option_code', $optionCode)
            ->andWhere('t.value LIKE :option_value')
            ->setParameter('option_value', "%$optionValue%")
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (!empty($optionValues)) {
            return $optionValues[0]->getCode();
        }

        try {
            // Create the option value then...
            $pov = new ProductOptionValue();
            $pov->setCurrentLocale('fr_FR');

            $optionValueCode = strtoupper((new Slugify())->slugify($optionValue));

            $pov->setValue($optionValue);
            /** @noinspection PhpParamsInspection */
            $pov->setOption($this->productOptionRepository->findOneBy(['code' => $optionCode]));
            $pov->setCode("{$optionCode}_$optionValueCode");

            $this->productOptionValueRepository->add($pov);

            return $pov->getCode();
        } catch (\Exception $e) {
            return $optionValue;
        }
    }
}
