<?php

namespace AppBundle\Services\Fetcher;

class HallesPrestigeMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {
            $infosLine = explode("\t", $line);

            // sometimes, there is an empty column A that should be removed
            if (count($infosLine) == 14) {
                array_shift($infosLine);
            }

            // remove the first exploding column
            array_shift($infosLine);
            if (!count($infosLine)) {
                continue;
            }

            $key = 0;
            $productLine = [];
            foreach ($infosLine as $info) {
                $productLine[] = $info;
                $key++;
                if (($key % 4) == 0) {
                    if ($key == 4) {
                        $key = 0;
                        $product = $this->format($productLine);

                        if ($product) {
                            $products[] = $product;
                        }
                    }
                    $productLine = [];
                }
            }
        }

        return $products;
    }

    protected function format($line)
    {
        if ($line[0] == "" ||
            $line[2] == "" ||
            $line[3] == "" ||
            in_array($line[0], ['FRUITS', 'LEGUMES', 'CHAMPIGNONS'])
        ) {
            return;
        }

        $name = $line[0];

        $content = 0;
        $salesUnit = '';
        $unitQuantity = 1;
        $unitContentQuantity = 0;
        $unitContentMeasurement = 'KG';

        switch(strtoupper($line[2])) {
            case '0':
            case 'JG':
            case 'KG':
                $salesUnit = 'KG';
                break;
            case 'COLIS':
                $salesUnit = 'CO';
                break;
            default:
                $salesUnit = 'PC';
                break;
        }

        $patternCountry = "/(fr|France|ghana|costa rica|esp|espagne|martinique|chine|Italie|Allemagne|all|peru|hollande|holl|hol(l)?|bresil|colombie|usa|belg|kenya|maroc|chilie|isr|israel|afr sud|afr du sud|portugal|thai|reunion|pol|pologne)$/i";
        preg_match($patternCountry, $name, $country);
        $originCountry = array_key_exists(0, $country) ? $country[0] : '';
        $originCountry = ''; // Ignore the country in Halles Prestige, because its mainly false
        $name = preg_replace($patternCountry, '', $name, 1);

        if ($salesUnit == 'PC') {
            $patternContent = "/(X |X)[0-9]+/";
            preg_match($patternContent, $name, $unitQuantity);
            $unitQuantity = array_key_exists(0, $unitQuantity) ? str_replace('X', '', $unitQuantity[0]) : '1';
        }

        $caliber = '';
        $patternCaliber = '/(\d+\/\d+)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $patternCaliber = '/CAL (\d+)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $name = str_replace($caliber, '', $name);

        $patternConditioning = "( )?(PLQ|BOTTE|BOX|BQ|BARQUETTE|BARQ|BARQ\.|CAISSE|SAC(HET)?|PLATEAU|PCS|PIECES|VRAC|CAISSE|COLIS)";
        $patternConditioning = '/' . $patternConditioning . '/i';
        preg_match($patternConditioning, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? trim($conditioning[0]) : '';
        switch (strtoupper($line[2])) {
            case "BQ" :     $conditioning = 'BARQUETTE'; break;
            case "BTTE" :   $conditioning = 'BOTTE'; break;
            case "BTE" :    $conditioning = 'BOITE'; break;
            case "COLIS" :  $conditioning = 'COLIS'; break;
        }
        $name = str_replace($conditioning, '', $name);
        if (!empty($conditioning)) {
            $conditioning = 'CO_'.$conditioning;
        }

        if (empty($conditioning)) {
            if ($salesUnit == 'KG') {
                $conditioning = 'CO_VRAC';
            }
        }

        $taxon = explode(' ', $name)[0];
        if (in_array($taxon, ['HF', 'BIO', 'MINI'])) {
            $taxon = explode(' ', $name)[1];
        }

        $weight = 0;
        $price = floatval(str_replace(',', '.', $line[3])) * 90;

        if ($salesUnit == 'KG') {
            $weight = 1;
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';
        } else {
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'PIECE';
        }

        $patternWeight = "/([\/.,\d\s]+\s*(KG|GR))/i";
        preg_match($patternWeight, $name, $variantWeight);

        if (array_key_exists(1, $variantWeight) && strlen($variantWeight[1]) > 3) {
            $unitContentMeasurement = 'KG';
            if (array_key_exists(2, $variantWeight)) {
                $unitContentMeasurement = strtoupper($variantWeight[2]);
            }
            $weight = str_ireplace(['KG', 'GR', ' '], '', $variantWeight[1]);
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $weight));

            if ($unitContentMeasurement == 'GR') {
                $weight = bcdiv($weight, 1000, 3);
            }
        }

        if ($salesUnit == 'PC') {
            if ($weight > 0) {
                $priceKg = bcdiv($price, $weight, 0);
            } else {
                $priceKg = 0;
            }
            $priceUnit = $price;
            $priceVariant = $price;
        } else {
            $priceKg = $price;
            $priceUnit = bcmul($price, $weight, 0);
            $priceVariant = bcmul($price, $weight, 0);
        }

        $bio = 'NOBIO';

        foreach($this->optionsValues as $option => $value) {
            ${$option} = $value;
        }

        if ($priceVariant == 0) {
            return false;
        }
        
        return [
            'reference'    => '',
            'supplierName' => $this->normalizeSpaces($line[0]),
            'productName' => strtoupper($this->normalizeSpaces($name)),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => $this->normalizeSpaces($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($salesUnit),
            'conditioning' => trim($conditioning),
            'category' => '',
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
