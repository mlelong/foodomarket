<?php

namespace AppBundle\Services\Fetcher;

class DaumesnilMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {

            $infosLine = explode("\t", $line);
            array_shift($infosLine); // remove the empty column A

            if (! in_array(count($infosLine), [4, 5, 6]) || $infosLine[0] == 'Article') {
                continue;
            }

            $product = $this->format($infosLine);

            if ($product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($line)
    {

        if (
            $line[2] == ""
        ) {
            return;
        }

        $reference = $line[0];
        $name = $line[1];

        $salesUnit = '';
        $unitQuantity = 1;
        $unitContentQuantity = 0;
        $unitContentMeasurement = 'KG';

        switch(strtoupper($line[3])) {
            case 'K':
                $salesUnit = 'KG';
                break;
            case 'C':
                $salesUnit = 'CO';
                break;
            case 'P':
                $salesUnit = 'PC';
                break;
            default:
                $salesUnit = 'PC';
                break;
        }

        $patternCountry = "/(france|ghana|costa rica|esp|espagne|martinique|chine|italie|allemagne|hollande|bresil|colombie|usa|belgique|kenya|maroc|chili|israel|portugal|thailand|reunion|pologne)$/i";
        preg_match($patternCountry, $name, $country);
        $originCountry = array_key_exists(0, $country) ? $country[0] : '';
        $name = preg_replace($patternCountry, '', $name, 1);

        if ($salesUnit == 'PC') {
            $patternContent = "/(X |X)\s+[0-9]+/";
            preg_match($patternContent, $name, $unitQuantity);
            $unitQuantity = array_key_exists(0, $unitQuantity) ? str_replace(['X', ' '], '', $unitQuantity[0]) : '1';
        }

        $caliber = '';
        $patternCaliber = '/(\d+\/\d+)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $patternCaliber = '/CAL (\d+)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $name = str_replace($caliber, '', $name);

        $category = '';
        $patternCategory = '/\s+CAT[\s]*([\S]+)/i';
        if (preg_match($patternCategory, $name, $variantCategory)) {
            $category = str_replace('.', '', $variantCategory[1]);
            $name = str_replace($variantCategory[0], '', $name);
        }

        $bio = '';
        $bioPattern = '/(\s+BIO\s+)/i';
        if (preg_match($bioPattern, $name, $variantBio)) {
            $bio = 'BIO';
        } else {
            $bio = 'NOBIO';
        }

        $patternContent = "/(X |X)[0-9]+ P/";
        preg_match($patternContent, $name, $content);
        $unitQuantity = array_key_exists(0, $content) ? str_replace(['X', ' ', 'P'], '', $content[0]) : '1';

        $patternConditioning = "( )?(PLQ|BOTTE|BOX|BQ|BQT|BARQUETTE|BARQ|BARQ\.|CAISSE|SAC(HET)?|PLATEAU|PCS|PIECES|VRAC|CAISSE|COLIS)";
        $patternConditioning = '/' . $patternConditioning . '/i';
        preg_match($patternConditioning, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? trim($conditioning[0]) : '';
        switch (strtoupper($line[2])) {
            case "BQ" :     $conditioning = 'BARQUETTE'; break;
            case "BTTE" :   $conditioning = 'BOTTE'; break;
            case "BTE" :    $conditioning = 'BOITE'; break;
            case "COLIS" :  $conditioning = 'COLIS'; break;
        }
        $name = str_replace($conditioning, '', $name);
        if (!empty($conditioning)) {
            $conditioning = 'CO_'.$conditioning;
        }

        if (empty($conditioning)) {
            if ($salesUnit == 'KG') {
                $conditioning = 'CO_VRAC';
            }
        }

        $taxon = explode(' ', $name)[0];
        if (in_array($taxon, ['HF', 'BIO', 'MINI'])) {
            $taxon = explode(' ', $name)[1];
        }

        $weight = 0;
        $price = floatval(str_replace(',', '.', $line[2])) * 100;

        if ($salesUnit == 'KG') {
            $weight = 1;
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';
        } else {
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'PIECE';
        }

        $patternWeight = "/([.,\d\s]+\s*(K|KG|G|GR))\s/i";
        preg_match($patternWeight, $name, $variantWeight);

        if (array_key_exists(1, $variantWeight)) {
            $unitContentMeasurement = 'KG';
            if (array_key_exists(2, $variantWeight)) {
                switch(strtoupper($variantWeight[2])) {
                    case 'K':
                    case 'KG':
                        $unitContentMeasurement = 'KG';
                        break;
                    case 'G':
                    case 'GR':
                        $unitContentMeasurement = 'GR';
                        break;
                }
            }
            $weight = str_ireplace(['KG', 'K', 'GR', 'G', ' '], '', $variantWeight[1]);
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $weight));

            if (in_array($unitContentMeasurement, ['GR'])) {
                $weight = bcdiv($weight, 1000, 3);
            }
        }

        if ($salesUnit == 'PC') {
            $priceUnit = bcmul($price, $unitQuantity, 0);
            $priceVariant = bcmul($price, $unitQuantity, 0);
            if ($weight > 0) {
                $priceKg = bcdiv($priceVariant, $weight, 0);
            } else {
                $priceKg = 0;
            }
        } else if ($salesUnit == 'KG') {
            $priceKg = $price;
            $priceUnit = bcmul($price, $weight, 0);
            $priceVariant = bcmul($price, $weight, 0);
        } else if ($salesUnit == 'CO') {
            $priceUnit = $price;
            $priceVariant = $price;
            if ($weight > 0) {
                $priceKg = bcdiv($priceVariant, $weight, 0);
            } else {
                $priceKg = 0;
            }
        }

        return [
            'reference'    => $reference,
            'supplierName' => $this->normalizeSpaces($line[1]),
            'productName' => strtoupper($this->normalizeSpaces($name)),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => $this->normalizeSpaces($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($salesUnit),
            'conditioning' => trim($conditioning),
            'category' => $category,
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
