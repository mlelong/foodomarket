<?php


namespace AppBundle\Services\Fetcher;


class ReynaudNewMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $content = $this->getContentAsText($filename);

        if ($content === null) {
            return null;
        }

        $products = [];

        if ($nbProducts = preg_match_all('/(?<ref>(?:à intégrer|[\d\/]+))\t+(?<product>[\S ]+)\t+(?<uv>[\S ]+)\t+(?<origin>[\S ]+)\t+(?<price>(?:indispo|\d+\.\d+ €))/', $content, $matches)) {
            for ($i = 0; $i < $nbProducts; ++$i) {
                if ($matches['price'][$i] === 'indispo') {
                    continue ;
                }

                $price = floatval(str_replace(' €', '', $matches['price'][$i]));

                if ($price <= 0.0) {
                    continue ;
                }

                if (stripos($matches['ref'][$i], '/') === false) {
                    $products[] = $this->getProductData($matches['ref'][$i], $matches['product'][$i], $matches['uv'][$i], $matches['origin'][$i], $price);
                } else {
                    $refs = explode('/', $matches['ref'][$i]);
                    $nbRefs = count($refs);

                    // Capture $nbRefs first '/' in the product name
                    $_products = explode('/', $matches['product'][$i]);
                    $_nbProducts = count($_products);

                    // It is not good if we don't find enough separators in the product name
                    if ($_nbProducts < $nbRefs) {
                        continue ;
                    }

                    $lastSpaceInFirstSlice = strrpos($_products[0], ' ');
                    $productNameFirstPart = substr($_products[0], 0, $lastSpaceInFirstSlice + 1);
                    $firstRealProduct = substr($_products[0], $lastSpaceInFirstSlice + 1);
                    $firstSpaceInLastSlice = strpos($_products[$nbRefs - 1], ' ');
                    $productNameLastPart = substr($_products[$nbRefs - 1], $firstSpaceInLastSlice ? $firstSpaceInLastSlice : strlen($_products[$nbRefs - 1]));
                    $lastRealProduct = substr($_products[$nbRefs - 1], 0, $firstSpaceInLastSlice);

                    if ($_nbProducts > $nbRefs) {
                        $productNameLastPart .= '/';
                    }

                    $productNameLastPart .= implode('/', array_slice($_products, $nbRefs));

                    for ($j = 0; $j < $nbRefs; ++$j) {
                        if ($j === 0) {
                            $productName = "{$productNameFirstPart}$firstRealProduct{$productNameLastPart}";
                        } elseif ($j === $nbRefs - 1) {
                            $productName = "{$productNameFirstPart}$lastRealProduct{$productNameLastPart}";
                        } else {
                            $productName = "{$productNameFirstPart}{$_products[$j]}{$productNameLastPart}";
                        }

                        $products[] = $this->getProductData($refs[$j], $productName, $matches['uv'][$i], $matches['origin'][$i], $price);
                    }
                }
            }
        }

        return $products;
    }

    private function getProductData(string $ref, string $product, string $uv, string $origin, string $price)
    {
//        $supplierName = !empty($ref) ? "$product $uv $origin ($ref)" : "$product $uv $origin";
        $supplierName = "$product $uv $origin";
        $priceKg = $price;
        $priceUnit = $price;
        $priceVariant = $price;

        preg_match('/^x?\s*(?<weight>[\d.,\/]*)\s*(?<unit>[\S]+)/', $uv, $cond);

        $weight = 1;
        $unitQuantity = 1;
        $unitContentQuantity = 1;
        $unitContentMeasurement = 'KG';

        switch (strtolower($cond['unit'])) {
            case 'l':
                $salesUnit = 'L';
                $orderUnit = 'PC';
                $weight = isset($cond['weight']) ? floatval(str_replace(',', '.', $cond['weight'])) : 1;
                $unitContentQuantity = $weight;
                $unitContentMeasurement = 'L';
                break ;
            case 'colis':
                $salesUnit = 'CO';
                $orderUnit = 'CO';
                $unitContentMeasurement = 'PC';
                break ;
            case 'kg':
                $salesUnit = 'KG';
                $orderUnit = 'PC';
                $weight = floatval(str_replace(',', '.', $cond['weight']));
                $unitContentQuantity = $weight;
                break ;
            case 'g':
            case 'gr':
                $weight = floatval(str_replace(',', '.', $cond['weight'])) / 1000;
                $orderUnit = 'PC';
                $salesUnit = 'PC';
                $unitContentQuantity = floatval(str_replace(',', '.', $cond['weight']));
                $unitContentMeasurement = 'GR';
                break ;
            default:
                $salesUnit = 'PC';
                $unitContentMeasurement = 'PC';

                if (empty($cond['weight']) || $cond['weight'] == 1) {
                    $orderUnit = 'PC';
                } else {
                    $orderUnit = 'CO';
                    $unitQuantity = (int)$cond['weight'];
                }

                break ;
        }

        if (preg_match('/\s+BIO(\s|$)/i', $supplierName)) {
            $bio = 'BIO';
        } else {
            $bio = 'NOBIO';
        }

        if ($salesUnit === 'KG') {
            $priceUnit = bcmul($price, $weight, 3);
            $priceVariant = $priceUnit;
        } else {
            $priceKg = bcdiv($price, $weight, 3);
        }

        $priceUnit = bcmul($priceUnit, $unitQuantity, 3);
        $priceVariant = bcmul($priceVariant, $unitQuantity, 3);

        $originalPrice = $price;
        $price = bcmul($price, 100);
        $priceKg = bcmul($priceKg, 100);
        $priceUnit = bcmul($priceUnit, 100);
        $priceVariant = bcmul($priceVariant, 100);

        return [
            'reference'    => $ref,
            'supplierName' => $supplierName,
            'productName' => $product,
            'variantName' => '-',
            'taxon' => '',
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => '',
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($orderUnit),
            'conditioning' => '',
            'category' => '',
            'caliber' => '',
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
            'line' => "{$ref} {$product} {$uv} {$origin} {$originalPrice} €"
        ];
    }
}
