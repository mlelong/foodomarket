<?php


namespace AppBundle\Services\Fetcher;

use Exception;

class TopAtlantiqueMercurialPdfFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $products = [];

        try {
            $output = $this->client->getText($this->importPath . $filename);

            if ($nbProducts = preg_match_all('/\n(?<product>[\S ]+\n?[\S ]*)\s+(?<price>[\d]+,[\d]+)[ ]*(?:\n|$)/', $output, $matches)) {
                for ($i = 0; $i < $nbProducts; ++$i) {
                    $products[] = $this->getProduct($matches['product'][$i], $matches['price'][$i]);
                }
            }
        } catch (Exception $e) {
        }

        return $products;
    }

    private function getProduct(string $product, string $price)
    {
        $product = $this->normalizeSpaces(str_replace(["\r\n", "\n"], [' ', ' '], $product));
        $normalizedPrice = (int)$this->normalizePrice($price);

        return [
            'reference' => '',
            'supplierName' => $product,
            'productName' => $product,
            'variantName' => '-',
            'taxon' => '',
            'weight' => 0,
            'price' => $normalizedPrice,
            'priceKg' => $normalizedPrice,
            'priceUnit' => $normalizedPrice,
            'priceVariant' => $normalizedPrice,
            'originCountry' => '',
            'originCity' => '',
            'salesUnit' => 'KG',
            'orderUnit' => "ORDER_KG",
            'conditioning' => '',
            'category' => '',
            'caliber' => '',
            'naming' => '',
            'bio' => 'NOBIO',
            'brand' => '',
            'content' => 0,
            'unitQuantity' => 1,
            'unitContentQuantity' => 1,
            'unitContentMeasurement' => 'KG',
            'line' => "$product | {$price}€"
        ];
    }

    private function normalizePrice(string $price)
    {
        return bcmul(str_replace([',', ' '], ['.', ''], $price), 100);
    }
}
