<?php


namespace AppBundle\Services\Fetcher;


use Exception;

class Sun7FruitsMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $products = [];

        try {
            $output = $this->client->getText($this->importPath . $filename);

            $nbHeaders = preg_match_all("/(?<col1>[ \S]+)\t(?<col2>[ \S]*)\t(?<col3>[ \S]*)\t(?<col4>[ \S]*)\t(?<col5>[ a-zA-Z]+)/", $output, $headers, PREG_OFFSET_CAPTURE);

            $offsetFromWhichItIsNoLongerTheOriginInTheSecondColumn = PHP_INT_MAX;

            for ($i = 0; $i < $nbHeaders; ++$i) {
                if (!in_array(strtolower(trim($headers['col2'][$i][0])), ['origine', ''])) {
                    $offsetFromWhichItIsNoLongerTheOriginInTheSecondColumn = $headers['col2'][$i][1];
                    break ;
                }
            }

            if (($nbProducts = preg_match_all("/(?<product>[ \S]+)\t(?<origin>[ \S]*)\t(?<weight>[ \S]+)\t(?<um>[ \S]+)\t(?<price>[\d.,\/]+)/", $output, $matches, PREG_OFFSET_CAPTURE))) {
                for ($i = 0; $i < $nbProducts; ++$i) {
                    $line = [
                        'product' => $this->normalizeSpaces($matches['product'][$i][0]),
                        'origin' => $matches['origin'][$i][1] < $offsetFromWhichItIsNoLongerTheOriginInTheSecondColumn ? $this->normalizeSpaces($matches['origin'][$i][0]) : '',
                        'conditionning' => $matches['origin'][$i][1] > $offsetFromWhichItIsNoLongerTheOriginInTheSecondColumn ? $this->normalizeSpaces($matches['origin'][$i][0]) : '',
                        'weight' => $this->normalizeSpaces($matches['weight'][$i][0]),
                        'um' => $this->normalizeSpaces($matches['um'][$i][0]),
                        'price' => $this->normalizeSpaces($matches['price'][$i][0])
                    ];

                    if (strpos($line['price'], '/') !== false) {
                        $multipleProducts = explode('/', $line['product']);
                        $multiplePrices = explode('/', $line['price']);

                        foreach ($multipleProducts as $index => $product) {
                            $product = $this->normalizeSpaces($product);
                            $price = $this->normalizePrice($multiplePrices[0]);

                            if (isset($multiplePrices[$index])) {
                                $price = $this->normalizePrice($multiplePrices[$index]);
                            }

                            $lineCopy = $line;

                            $lineCopy['product'] = $product;
                            $lineCopy['price'] = $price;

                            $products[] = $this->getProduct($lineCopy);
                        }
                    } elseif (strpos($line['weight'], '/') || strpos($line['weight'], '-')) {
                        $line['weight'] = str_replace('-', '/', $line['weight']);
                        $weights = explode('/', $line['weight']);

                        foreach ($weights as $weight) {
                            $lineCopy = $line;
                            $lineCopy['weight'] = $this->normalizeSpaces($weight);

                            $products[] = $this->getProduct($lineCopy);
                        }
                    } else {
                        $products[] = $this->getProduct($line);
                    }
                }
            }
        } catch (Exception $e) {
        }

        return $products;
    }

    private function getProduct($line)
    {
        $product = $line['product'];
        $price = $this->normalizePrice($line['price']);

        $hasWeight = preg_match('/(?<weight>[\d.,]+)/', $line['weight'], $match) > 0;

        return [
            'reference' => '',
            'supplierName' => $product,
            'productName' => $product,
            'variantName' => '-',
            'taxon' => '',
            'weight' => $hasWeight ? $match['weight'] : 0,
            'price' => $price,
            'priceKg' => $price,
            'priceUnit' => $price,
            'priceVariant' => $price,
            'originCountry' => $line['origin'],
            'originCity' => '',
            'salesUnit' => 'KG',
            'orderUnit' => "ORDER_KG",
            'conditioning' => $line['conditionning'],
            'category' => '',
            'caliber' => '',
            'naming' => '',
            'bio' => 'NOBIO',
            'brand' => '',
            'content' => 0,
            'unitQuantity' => 1,
            'unitContentQuantity' => 1,
            'unitContentMeasurement' => 'KG',
            'line' => implode(' | ', $line)
        ];
    }

    private function normalizePrice(string $price)
    {
        return bcmul(str_replace([',', ' '], ['.', ''], $price), 100);
    }
}
