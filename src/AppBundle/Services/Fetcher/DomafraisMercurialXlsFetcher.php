<?php

namespace AppBundle\Services\Fetcher;

class DomafraisMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {

            $infosLine = explode("\t", $line);

            if ($key < 3) {
                continue;
            }

            $product = $this->format($infosLine);

            if ($product) {
                $products[] = $product;
            }

        }

        return $products;
    }

    protected function format($line)
    {

        if (count($line) < 5 || $line[1] == '') {
            return;
        }

        $reference = $line[1];
        $name = $line[2];
        $salesUnit = $line[3];
        $price = floatval(str_replace(',', '.', $line[4])) * 100;
        if (array_key_exists(6, $line)) {
            $fileConditionning = $conditioning = $line[5];
        } else {
            $fileConditionning = '';
        }

        $taxon = explode(' ', $name)[0];
        if (in_array($taxon, ['HF', 'BIO', 'MINI'])) {
            $taxon = explode(' ', $name)[1];
        }
        $taxon = 'Divers';

        $content = 0;
        $unitQuantity = 1;
        $unitContentQuantity = 0;
        $unitContentMeasurement = 'KG';

        switch(strtoupper($salesUnit)) {
            case 'BA': // Barquette
                $conditioning = 'BARQUETTE';
                break;
            case 'BT': // Barquette ?
                $conditioning = 'BARQUETTE';
                break;
            case 'PO': // Pot
                $conditioning = 'PORTION';
                break;
            case 'SA': // Sachet
                $conditioning = 'SACHET';
                break;
            case 'SO': // Seaux
                $conditioning = 'SEAUX';
                break;
            case 'CO': // colis
                $conditioning = 'COLIS';
                break;
            case 'LO': // Lot
                $conditioning = 'LOT';
                break;
            case 'PT': // Plateau
                $conditioning = 'PLATEAU';
                break;
            default:
                $conditioning = '';
        }

        switch(strtoupper($salesUnit)) {
            case 'BA': // Barquette
            case 'BT': // Barquette ?
            case 'PO': // Pot
            case 'SA': // Sachet
            case 'SO': // Seaux
            case 'PI': // Piece
                $salesUnit = 'PC';
                break;
            case 'L':   // Litre
                $salesUnit = 'L';
                break;
            case 'KG':
                $salesUnit = 'KG';
                break;
            case 'CO': // colis
            case 'LO': // Lot
            case 'PT': // Plateau
                $salesUnit = 'CO';
                break;
            default:
                $salesUnit = 'PC';
                break;
        }

        $originCountry = '';
        $caliber = '';

        // explore conditionning

        $patternContent = "/(\d*)\s*x\s*(\d*K*\d)\s*(portions|pièces|pieces|piece|boites|poches|carton|sachets|barquettes|barquette|seaux|plaques|kilos|kilo|kg|k|gr|g)*/ui";
        preg_match($patternContent, $fileConditionning, $content);
        $unitQuantity = array_key_exists(1, $content) ? $content[1] : 1;

        $unitContentQuantity = array_key_exists(2, $content) ? $content[2] : '1';
        $unitContentMeasurement = array_key_exists(3, $content) ? strtoupper($content[3]) : 'PIECE';

        if ($unitContentMeasurement == 'PIECES' || $salesUnit == 'PC') {
            $unitContentMeasurement = 'PIECE';

            if ($unitQuantity == 1) {
                $unitQuantity = $unitContentQuantity;
                $unitContentQuantity = 1;
            }
        }

        if ($salesUnit == 'L') {
            $unitContentMeasurement = 'L';
        }

        $weight = 0;
        if (in_array($unitContentMeasurement, ['k', 'kg', 'kilo'])) {

            $unitContentMeasurement = 'KG';
            $weight = $unitQuantity * $unitContentQuantity;
        }

        if (in_array($unitContentMeasurement, ['g', 'gr'])) {

            $unitContentMeasurement = 'GR';
            $weight = $unitQuantity * bcdiv($unitContentQuantity, 1000, 3);
        }

        // search weight in name
/*
        $patternContent = "/(\d*)\s*(\d*K*\d)*\s*(kg|k|gr|g)/ui";
        preg_match($patternContent, $name, $content);
        if (count($content)) {

            $weightInName = '';
            $weightInName = array_key_exists(1, $content) && $content[1] ? $content[1] : $weightInName;
            $weightInName = array_key_exists(2, $content) && $content[2] ? $content[2] : $weightInName;

            $unitContentMeasurementInName = array_key_exists(3, $content) && $content[3] ? strtoupper($content[3]) : '';

            $unitWeight = $weightInName;
            if (in_array($unitContentMeasurementInName, ['GR', 'G'])) {
                $unitWeight = bcdiv($unitWeight, 1000, 3);
            }

            $unitContentQuantity = bcdiv($weight, $unitWeight, 3);

        }
        dump('WEIGHT NAME', $content, $weightInName, $unitContentMeasurementInName);
*/

        if (!empty($conditioning)) {
            $conditioning = 'CO_'.$conditioning;
        }

        $priceVariant = 0;
        if ($salesUnit == 'KG') {
            $priceKg = $price;
            $priceUnit = bcmul($price, $weight, 0);
            $priceVariant = bcmul($price, $weight, 0);
        }

        if ($salesUnit == 'PC') {
            $priceUnit = $price;
            $priceVariant = bcmul($unitQuantity, $price, 0);
            if ($weight) {
                $priceKg = bcdiv($priceVariant, $weight, 0);
            } else {
                $priceKg = 0;
            }
        }

        if ($salesUnit == 'CO') {
            if ($weight) {
                $priceKg = bcdiv($price, $weight, 0);
            } else {
                $priceKg = 0;
            }
            $priceUnit = $price;
            $priceVariant = $price;
        }

        if ($salesUnit == 'L') {
            $priceUnit = $price;
            $priceVariant = bcmul($unitQuantity, $price, 0);
            if ($weight) {
                $priceKg = bcdiv($priceVariant, $weight, 0);
            } else {
                $priceKg = 0;
            }
        }

        $bio = '';
        $bioPattern = '/(\s+BIO(?!\S+))/i';
        if (preg_match($bioPattern, $name, $content)) {
            $bio = 'BIO';
        } else {
            $bio = 'NOBIO';
        }

        foreach($this->optionsValues as $option => $value) {
            ${$option} = $value;
        }

        $orderUnit = 'ORDER_CO';
        if ($unitQuantity == 1 && $unitContentQuantity == 1) {
            $orderUnit = 'ORDER_PC';
        }

        return [
            'reference'    => $reference,
            'supplierName' => $this->normalizeSpaces($line[2]),
            'productName' => strtoupper($this->normalizeSpaces($name)),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => $this->normalizeSpaces($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => $orderUnit,
            'conditioning' => trim($conditioning),
            'category' => '',
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
