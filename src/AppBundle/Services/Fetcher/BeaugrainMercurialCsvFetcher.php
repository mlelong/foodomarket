<?php


namespace AppBundle\Services\Fetcher;

use Exception;

class BeaugrainMercurialCsvFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        return $this->extractLines($filename);
    }

    protected function extractLines($filename)
    {
        $handle = fopen($this->importPath . $filename, 'r');
        $products = [];

        // Skip the first line
        fgetcsv($handle);

        while ($line = fgetcsv($handle)) {
            if (!isset($line[3]) || empty($line[3])) {
                continue ;
            }

            $um = $line[7];
            $uc = str_replace(' ', '', $line[8]);
            $kgPrice = $line[6];
            $pcPrice = $line[5];

            $kgPrice = (int)(floatval(str_replace([',', ' €'], ['.', ''], $kgPrice)) * 100);
            $pcPrice = (int)(floatval(str_replace([',', ' €'], ['.', ''], $pcPrice)) * 100);

            $products[] = [
                'reference'    => '',
                'supplierName' => $line[3],
                'productName' => $line[3],
                'variantName' => '-',
                'taxon' => '',
                'weight' => $line[4],
                'price' => $um === 'KG' ? $kgPrice : $pcPrice,
                'priceKg' => $kgPrice,
                'priceUnit' => $pcPrice,
                'priceVariant' => $um === 'KG' ? $kgPrice : $pcPrice,
                'originCountry' => $line[9],
                'originCity' => $line[10],
                'salesUnit' => $um,
                'orderUnit' => "ORDER_$uc",
                'conditioning' => $line[13],
                'category' => $line[15],
                'caliber' => $this->normalizeSpaces($line[12]),
                'naming' => $line[14],
                'bio' => $line[11] === 'BIO' ? 'BIO' : 'NOBIO',
                'brand' => $line[17],
                'content' => 0,
                'unitQuantity' => 1,
                'unitContentQuantity' => 1,
                'unitContentMeasurement' => 'KG',
            ];
        }

        return $products;
    }
}
