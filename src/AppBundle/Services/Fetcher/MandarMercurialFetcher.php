<?php

namespace AppBundle\Services\Fetcher;

use Cocur\Slugify\Slugify;

class MandarMercurialFetcher extends Fetcher
{

    protected $format = 'text';

    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {
            if ($key < 10) {
                continue;
            }

            $product = $this->format($line);
            if ($product) {
                $countries = explode('/', $product['originCountry']);

                foreach ($countries as $country) {
                    $product['originCountry'] = trim($country);
                    $products[] = $product;
                }
            }
        }

        return $products;
    }

    protected function format($line)
    {
        $pattern = "/(\d{8})\s(.*)\s(KG|PI)+\s(\S*)\s([.,\d]+)\s€/i";
        preg_match($pattern, $line, $infos);

        if (count($infos) == 0) {
            return;
        }

        $reference = $infos[1];
        $name = $infos[2];
        $salesUnit = $infos[3];
        $originCountry = $infos[4];
        $price = intval(floatval(str_replace(',', '.', $infos[5])) * 100);
        $variant = '';
        $unitContentQuantity = 1;
        $unitContentMeasurement = 'KG';

        if ($salesUnit == 'PI') {
            $salesUnit = 'PC';
        }

        if ($salesUnit == 'KG') {
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';
        } else {
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'PIECE';
        }

        // remove 4 G
        $name = str_replace(' 4 G X ', ' ', $name);
        // remove multiple space
        $name = preg_replace('/\s+/', ' ', $name);
        $name = preg_replace('/\*\s/', '', $name);

        $patternConditioning = "( )?(PLQ|POT|BOTTE|BOX|BQ|BARQUETTE|BARQ|BARQ\.|CAISSE|SAC(HET)?|PLATEAU|VRAC|CAISSE|COLIS)";
        $patternConditioning = '/' . $patternConditioning . '/i';
        preg_match($patternConditioning, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? trim($conditioning[0]) : '';

        $unitQuantity = 1;
        $patternContent = "/([\d\s]+)\s*PIECES/i";
        preg_match($patternContent, $name, $matchQuantity);
        if (array_key_exists(1, $matchQuantity)) {
            $unitContentQuantity = intval($matchQuantity[1]);
            $unitContentMeasurement = 'PIECE';
        }

        $patternContent = "/X ([\d\s]+)\s[^P]/i";
        preg_match($patternContent, $name, $matchQuantity);
        if (array_key_exists(1, $matchQuantity)) {
            $unitQuantity = intval($matchQuantity[1]);
        }

        $patternContent = "/X ([\d\s]+)$/i";
        preg_match($patternContent, $name, $matchQuantity);
        if (array_key_exists(1, $matchQuantity)) {
            $unitQuantity = intval($matchQuantity[1]);
        }

        $weight = 0;
        $patternWeight = "/([\/.,\d\s]+)\s*(KG|GR|G)(?!\S+)/i";
        preg_match($patternWeight, $name, $variantWeight);

        $KgOrGr = 'KG';
        if (array_key_exists(1, $variantWeight) && $variantWeight[1] > 0) {
            if (array_key_exists(2, $variantWeight)) {
                $KgOrGr = strtoupper($variantWeight[2]);
                if ($KgOrGr == 'G') {
                    $KgOrGr = 'GR';
                }
                $unitContentMeasurement = $KgOrGr;
            }
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $variantWeight[1]));

            if ($KgOrGr == 'GR') {
                $weight = bcdiv($weight, 1000, 3);
            }
            if ($weight == 0) {
                $weight = 1;
            }
        }

        $category = '';

        $patternCategory = '/\s+CAT[\s]*([\S]+)/i';
        if (preg_match($patternCategory, $name, $variantCategory)) {
            $category = $variantCategory[1];
        }

        $caliber = '';
        $patternCaliber = '/(?:[\s]+CAL(?!IBRE)[\s]*)(?P<cal>[\S]+[\s]*(?:[\d\s]*MM[\s+]*)*(?:N\d+)*)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber['cal']);
        }

        $totalWeight = bcmul($weight, $unitQuantity, 3);
        $priceVariant = $price;

        if ($salesUnit == 'KG') {
            $priceKg = $price;
            $totalWeight = 1;
        } else {
            $priceKg = 99900;
            if ($totalWeight) {
                $priceKg = bcdiv($priceVariant, $totalWeight, 0);
            }
        }

        $bio = '';
        $bioPattern = '/(?P<naming>\s+BIO(?!\S+))/i';
        if (preg_match($bioPattern, $name, $variantNaming)) {
            $bio = 'BIO';
        } else {
            $bio = 'NOBIO';
        }

        if (empty($conditioning)) {
            if (preg_match('/KG/i', $name)) {
                $conditioning = 'VRAC';
            }
        }

        if (empty($conditioning)) {
            if ($salesUnit == 'KG') {
                $conditioning = 'VRAC';
            }
        }

        if (!empty($conditioning)) {
            $conditioning = 'CO_'.$conditioning;
        }

        $nameAfter = $this->normalizeSpaces(preg_replace([
            $patternWeight,
            $patternConditioning,
            $patternContent,
            $patternCaliber,
            $patternCategory,
            $bioPattern
        ], '', $name));

        $name = $nameAfter;

        $orderUnit = '';
        if ($salesUnit == 'KG') {
            $orderUnit = 'ORDER_KG';
        } else {
            $orderUnit = 'ORDER_PC';
        }

        return [
            'reference'    => $reference,
            'supplierName' => $infos[2],
            'productName' => $this->normalizeSpaces($name),
            'variantName' => '-',
            'taxon' => explode(' ', $name)[0],
            'weight' => $totalWeight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceVariant' => $priceVariant,
            'originCountry' => trim($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => trim($orderUnit),
            'conditioning' => $this->normalizeSpaces($conditioning),
            'category' => $this->slugifyOption('CC', $category),
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }

    protected function slugifyOption($code, $value) {
        $code = strtolower(trim($code));
        $value = strtolower(trim($value));

        return Slugify::create()->slugify("{$code}_{$value}");
    }
}
