<?php


namespace AppBundle\Services\Fetcher;


class JacobMercurialCsvFetcher extends Fetcher
{
    public function getProducts($fileName)
    {
        $handle = fopen($this->importPath . $fileName, 'rb');
        $products = [];

        if ($handle) {
            // Strip the header
            fgetcsv($handle);

            while ($line = fgetcsv($handle)) {
                $salesUnit = $line[6] === 'Kilogramme' ? 'KG' : 'PC';
                $price = str_replace(',', '.', $line[7]);
                $price = bcmul($price, 100);

                if ($price == 0) {
                    continue ;
                }

                // Les oeufs, le prix est à la centaine
                if (in_array($line[3], [2119, 2121, 249, 2346, 2122, 2124, 2126, 2114, 2118, 2125, 2114, 2124, 2126, 2115, 2116])) {
                    $price = bcdiv($price, 100, 2);
                }

                $ref = "{$line[0]}-{$line[3]}";
                $ref = substr($ref, 1);

                $products[] = [
                    'reference'    => $ref,
                    'supplierName' => $this->normalizeSpaces($line[2] . ' ' . $line[6] . ' ' . $line[4]),
                    'productName' => strtoupper($this->normalizeSpaces($line[2])),
                    'variantName' => '-',
                    'taxon' => '',
                    'weight' => 1,
                    'price' => $price,
                    'priceKg' => $salesUnit === 'KG' ? $price : 0,
                    'priceUnit' => $salesUnit !== 'KG' ? $price : 0,
                    'priceVariant' => $salesUnit !== 'KG' ? $price : 0,
                    'originCountry' => '',
                    'salesUnit' => $salesUnit,
                    'orderUnit' => 'ORDER_'.$salesUnit,
                    'conditioning' => '',
                    'category' => '',
                    'caliber' => '',
                    'naming' => '',
                    'bio' => '',
                    'brand' => '',
                    'content' => 0,
                    'unitQuantity' => 1,
                    'unitContentQuantity' => 1,
                    'unitContentMeasurement' => 'UCM_KG'
                ];
            }
        }

        return $products;
    }
}