<?php


namespace AppBundle\Services\Fetcher;


class BruyerreMercurialXlsFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $content = $this->getContentAsText($filename);

        if (empty($content)) {
            return null;
        }

        if ($nbProducts = preg_match_all('/(?<ref>\d+)\t(?<product>[\S ]+)\t(?<cond>[\S ]*)\t(?<quantity>[\d.,]+)\t(?<price>[\d.,]+)\t(?<um>[\S]+)\t(?<uc>[\S]+)/', $content,$matches)) {
            $products = [];

            for ($i = 0; $i < $nbProducts; ++$i) {
                $products[] = $this->getProduct($matches['ref'][$i], $matches['product'][$i], $matches['cond'][$i], $matches['quantity'][$i], $matches['price'][$i], $matches['um'][$i], $matches['uc'][$i]);
            }

            return $products;
        }

        return null;
    }

    private function guessUnitContents(string $product, string $cond, int &$uq = 1, float &$ucq = 1, string &$ucm = 'KG')
    {
        if ($count = preg_match_all('/(?<uq>\d+)\s*X\s*(?<ucq>[\d.,]+)\s*(?<ucm>[a-z]+)/i', $cond, $pMatch, PREG_OFFSET_CAPTURE)) {
            $uq = $pMatch['uq'][$count - 1][0];
            $ucq = $pMatch['ucq'][$count - 1][0];
            $ucm = $pMatch['ucm'][$count - 1][0];
        } elseif ($count = preg_match_all('/(?<ucq>[\d.,]+)\s*(?<ucm>[a-z]+)/i', $cond, $pMatch, PREG_OFFSET_CAPTURE)) {
            $ucq = $pMatch['ucq'][$count - 1][0];
            $ucm = $pMatch['ucm'][$count - 1][0];
        } elseif ($count = preg_match_all('/(?<uq>\d+)\s*X\s*(?<ucq>[\d.,]+)\s*(?<ucm>[a-z]+)/i', $product, $pMatch, PREG_OFFSET_CAPTURE)) {
            $uq = $pMatch['uq'][$count - 1][0];
            $ucq = $pMatch['ucq'][$count - 1][0];
            $ucm = $pMatch['ucm'][$count - 1][0];
        } elseif ($count = preg_match_all('/(?<ucq>[\d.,]+)\s*(?<ucm>[a-z]+)/i', $product, $pMatch, PREG_OFFSET_CAPTURE)) {
            $ucq = $pMatch['ucq'][$count - 1][0];
            $ucm = $pMatch['ucm'][$count - 1][0];
        }

        $uq = intval($uq);
        $ucq = floatval(str_replace(',', '.', $ucq));
        $ucm = strtoupper($ucm);
    }

    private function guessWeight(int $uq, float $ucq, string $ucm)
    {
        if (in_array($ucm, ['KG', 'LITRE', 'L'])) {
            return $uq * $ucq;
        }

        if (in_array($ucm, ['G', 'ML'])) {
            return ($uq * $ucq) / 1000;
        }

        return 1.0;
    }

    private function getProduct(string $ref, string $product, string $cond, string $quantity, string $price, string $um, string $uc)
    {
        $conditionning = '';

        if ($um === 'KG') {
            $salesUnit = 'KG';
        } elseif ($um === 'LITRE') {
            $salesUnit = 'L';
        } elseif ($um === 'COLIS') {
            $salesUnit = 'COLIS';
        } else {
            $salesUnit = 'PC';
            $conditionning = $um;
        }

        $price = str_replace(',', '.', $price);
        $price100 = bcmul($price, 100);
        $productNormalized = $this->normalizeSpaces($product);

        $uq = 1;
        $ucq = 1;
        $ucm = '';

        $this->guessUnitContents($product, $cond, $uq, $ucq, $ucm);
        $weight = 1;//$this->guessWeight($uq, $ucq, $ucm);

        return [
            'reference'    => $ref,
            'supplierName' => $productNormalized,
            'productName' => $productNormalized,
            'variantName' => '-',
            'taxon' => '',
            'weight' => $weight,
            'price' => $price100,
            'priceKg' => $price100,
            'priceUnit' => $price100,
            'priceVariant' => $price100,
            'originCountry' => '',
            'originCity' => '',
            'salesUnit' => $salesUnit,
            'orderUnit' => "ORDER_$salesUnit",
            'conditioning' => $conditionning,
            'category' => '',
            'caliber' => '',
            'naming' => '',
            'bio' => 'NOBIO',
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $uq,
            'unitContentQuantity' => $ucq,
            'unitContentMeasurement' => $ucm,
            'line' => "$ref | $product | $cond | $quantity | $price € | $um | $uc"
        ];
    }
}