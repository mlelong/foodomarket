<?php

namespace AppBundle\Services\Fetcher;

class LauranceMercurialXlsFetcher extends Fetcher
{

    protected $format = 'text';

    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        $products = $this->extractProducts($lines);

        return $products;
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {

            //dump('-------------------- FICHIER ',$line);

            if ($key < 13) {
                continue;
            }

            $infosLine = explode("\t", $line);
            //dump('-------------------------------- LINE', $infosLine);

            if (count($infosLine) != 5) {
                continue;
            }

            $product = $this->format($infosLine);
            //dump('-------------------------------- PRODUCT', $product);

            if ($product) {
                //dump('ADD PROD', $product);
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($infos)
    {

        $reference = $infos[1];
        $name = $infos[2];
        $salesUnit = $infos[4];
        $originCountry = '';
        $price = intval(floatval(str_replace(',', '.', $infos[3])) * 100);
        $weight = 0;

        switch($salesUnit) {
            case 'K':
                $salesUnit = 'KG';
                $weight = 1;
                $unitContentQuantity = 1;
                $unitContentMeasurement = 'KG';
                break;
            case 'C':
                $salesUnit = 'CO';
                $unitContentQuantity = 1;
                $unitContentMeasurement = 'COLIS';
                break;
            default:
                $salesUnit = 'PC';
                $unitContentQuantity = 1;
                $unitContentMeasurement = 'PIECE';
                break;
        }

        $taxon = explode(' ', $name)[0];
        if (in_array($taxon, ['HF', 'BIO', 'MINI'])) {
            $taxon = explode(' ', $name)[1];
        }

        $patternCountry = "/(france|francais|corse|italie|martinique|guadeloupe)/i";
        preg_match($patternCountry, $name, $country);
        $originCountry = array_key_exists(0, $country) ? $country[0] : '';
        $name = preg_replace($patternCountry, '', $name, 1);

        $patternConditioning = "( )?(BOTTES|COLIS|BQ|BARQ|BARQUETTE|CAISSE|SAC(HET)?|PLATEAU|VRAC)";
        $patternConditioning = '/' . $patternConditioning . '/i';
        preg_match($patternConditioning, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? $conditioning[0] : '';

        switch (strtoupper($conditioning)) {
            case "BQ" :
            case "BARQ" :
            case "BARQUETTE" :
                $conditioning = 'BARQUETTE'; break;
        }

        $unitQuantity = 1;

        $patternWeight = "/([\/.,\d\s]+\s*(KG|GR))/i";
        preg_match($patternWeight, $name, $variantWeight);

        if (array_key_exists(1, $variantWeight) && strlen($variantWeight[1]) > 3) {
            $unitContentMeasurement = 'KG';
            if (array_key_exists(2, $variantWeight)) {
                $unitContentMeasurement = strtoupper($variantWeight[2]);
            }
            $weight = str_ireplace(['KG', 'GR', ' '], '', $variantWeight[1]);
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $weight));

            if ($unitContentMeasurement == 'GR') {
                $weight = bcdiv($weight, 1000, 3);
            }
        }


        //$totalWeight = bcmul($weight, $unitQuantity, 3);
        $priceVariant = $price;
        $priceKg = 0;

        if ($salesUnit == 'PC') {
            $priceUnit = bcmul($price, $unitQuantity, 0);
            $priceVariant = bcmul($price, $unitQuantity, 0);
            if ($weight > 0) {
                $priceKg = bcdiv($priceVariant, $weight, 0);
            } else {
                $priceKg = 0;
            }
        } else if ($salesUnit == 'KG') {
            $priceKg = $price;
            $priceUnit = bcmul($price, $weight, 0);
            $priceVariant = bcmul($price, $weight, 0);
        } else if ($salesUnit == 'CO') {
            if ($weight > 0) {
                $priceKg = bcdiv($priceVariant, $weight, 0);
            } else {
                $priceKg = 0;
            }
            $priceUnit = $price;
            $priceVariant = $price;
        }

        $bio = 'NOBIO';

        // remove multiple space
        $name = preg_replace('/\s+/', ' ', $name);
        $name = preg_replace('/\*\s/', '', $name);

        return [
            'reference'    => $reference,
            'supplierName' => $this->normalizeSpaces($infos[2]),
            'productName' => strtoupper($this->normalizeSpaces($name)),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $weight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceUnit' => $priceUnit,
            'priceVariant' => $priceVariant,
            'originCountry' => $this->normalizeSpaces($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => 'ORDER_'.trim($salesUnit),
            'conditioning' => trim($conditioning),
            'category' => '',
            'caliber' => '',
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }
}
