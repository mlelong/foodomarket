<?php

namespace AppBundle\Services\Fetcher;

class ButcheryMercurialCsvFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $handle = fopen($this->importPath . $filename, 'r');
        $products = [];
        $found = false;

        while ($line = fgetcsv($handle)) {
            if ($line[0] === 'FAMILLE') {
                $found = true;
                continue ;
            }

            // Tant qu'on a pas trouvé "FAMILLE" dans la première colonne
            // ou si la première colonne est vide
            // ou si la colonne prix est vide
            if (!$found || empty($line[0]) || empty($line[10])) {
                continue ;
            }

            if (preg_match('/(?<weight>[\d\.,]+)/', $line[4], $matches)) {
                $weight = floatval(str_replace(',', '.', $matches['weight']));
            } else {
                $weight = 1;
            }

            $price = bcmul(str_replace([',', ' ', '€'], ['.', '', ''], $line[3]), 100);

            $products[] = [
                'reference'    => $line[1],
                'supplierName' => $line[2],
                'productName' => $line[2],
                'variantName' => '-',
                'taxon' => '',
                'weight' => $weight,
                'price' => $price,
                'priceKg' => $price,
                'priceUnit' => $price * $weight,
                'priceVariant' => $price * $weight,
                'originCountry' => $line[5],
                'originCity' => '',
                'salesUnit' => 'KG',
                'orderUnit' => "ORDER_KG",
                'conditioning' => '',
                'category' => '',
                'caliber' => '',
                'naming' => $line[7],
                'bio' => 'NOBIO',
                'brand' => $line[6],
                'content' => 0,
                'unitQuantity' => 1,
                'unitContentQuantity' => 1,
                'unitContentMeasurement' => 'KG',
                'line' => implode(' | ', $line)
            ];
        }

        return $products;
    }
}
