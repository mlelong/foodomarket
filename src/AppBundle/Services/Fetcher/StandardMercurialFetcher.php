<?php

namespace AppBundle\Services\Fetcher;

class StandardMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {
            if ($key < 2) {
                continue;
            }

            if (stripos($line, "\t") !== FALSE) {
                $infosLine = explode("\t", $line);
                array_shift($infosLine);
            }
            else if (stripos($line, ";") !== FALSE) {
                $infosLine = explode(";", $line);
            }

            if (count($infosLine) < 13) {
                continue;
            }

            $product = $this->format($infosLine);

            if ($product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($line)
    {

        if ($line[0] == "") {
            return;
        }

        if (in_array($line[8], ['PI', 'PIECE'])) {
            $line[8] = 'PC';
        }

        if (in_array($line[10], ['PI', 'PIECE'])) {
            $line[10] = 'PC';
        }

        if (in_array($line[10], ['COLIS'])) {
            $line[10] = 'CO';
        };

        if (!in_array($line[10], ['KG', 'PC', 'L', 'CO'])) {
            $line[10] = 'KG';
        };
        if (!array_key_exists(12, $line)) {
            $line[12] = '';
        }
        if (!array_key_exists(13, $line)) {
            $line[13] = 'NOBIO';
        } else {
            if (!in_array($line[13], ['OUI', '1', 'BIO', 'DEMETER'])) {
                $line[13] = 'NOBIO';
            }
        }
        if (!array_key_exists(14, $line)) {
            $line[14] = '';
        }
        if (!array_key_exists(15, $line)) {
            $line[15] = '';
        }
        if (!array_key_exists(17, $line)) {
            $line[17] = '';
        }



        $infos['price']                  = intval(floatval(str_replace([','], '.',$line[11]))* 100);
        $infos['taxon']                  = $line[0];
        $infos['reference']              = $line[1];
        $infos['supplierName']           = $this->normalizeSpaces($line[2]);
        $infos['productName']            = $this->normalizeSpaces($line[3]);
        $infos['variantName']            = $line[4];
        $infos['weight']                 = floatval(str_replace(',', '.', $line[9]));
        $infos['priceKg']                = 0;
        $infos['priceUnit']              = 0;
        $infos['priceVariant']           = 0;
        $infos['originCountry']          = $line[14];
        $infos['origin']                 = $line[15];
        $infos['salesUnit']              = $line[10];
        $infos['orderUnit']              = 'ORDER_' . $line[5];
        $infos['conditioning']           = $this->normalizeSpaces(strtoupper($line[12]));
        $infos['category']               = '';
        $infos['caliber']                = '';
        $infos['naming']                 = $line[16];
        $infos['bio']                    = $line[13];
        $infos['brand']                  = $line[17];
        $infos['unitQuantity']           = floatval(str_replace(',', '.', $line[6]));
        $infos['unitContentQuantity']    = floatval(str_replace(',', '.', $line[7]));
        $infos['unitContentMeasurement'] = 'UCM_' . $line[8];

        if (empty($infos['variantName'])) {
            $infos['variantName'] = '-';
        }

        if (!empty($infos['conditioning'])) {
            $infos['conditioning'] = 'CO_'.$infos['conditioning'];
        }

        if ($infos['bio'] == 'NON') {
            $infos['bio'] = 'NOBIO';
        }
        if ($infos['bio'] == 'OUI') {
            $infos['bio'] = 'BIO';
        }

        if (!empty($infos['origin'])) {
            $infos['origin'] = 'OV_'.$infos['origin'];
        }

        if (!empty($infos['naming'])) {
            $infos['naming'] = 'AP_'.$infos['naming'];
        }

        if ($infos['salesUnit'] == 'KG') {
            $infos['priceKg'] = $infos['price'];
            $infos['priceUnit'] = bcmul($infos['price'], $infos['weight'], 0);
            if ($infos['weight']) {
                $infos['priceVariant'] = bcmul($infos['price'], $infos['weight'], 0);
            }
        }

        if ($infos['salesUnit'] == 'PC') {
            $infos['priceUnit'] = $infos['price'];
            $infos['priceVariant'] = bcmul($infos['unitQuantity'], $infos['price'], 0);
            if ($infos['weight']) {
                $infos['priceKg'] = bcdiv($infos['priceVariant'], $infos['weight'], 0);
            }
        }

        if ($infos['salesUnit'] == 'CO') {
            $infos['priceKg'] = bcdiv($infos['price'], $infos['weight'], 0);
            $infos['priceUnit'] = $infos['price'];
            $infos['priceVariant'] = $infos['price'];
        }

        if ($infos['salesUnit'] == 'L') {
            $infos['priceUnit'] = $infos['price'];
            $infos['priceVariant'] = bcmul($infos['unitQuantity'], $infos['price'], 0);
            if ($infos['weight']) {
                $infos['priceKg'] = bcdiv($infos['priceVariant'], $infos['weight'], 0);
            }
        }

        return $infos;
    }
}