<?php

namespace AppBundle\Services\Fetcher;

class TerreAzurMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $this->alreadyImportedProduct = [];
        $products = [];

        foreach ($lines as $key => $line) {
            $infosLine = explode("\t", $line);
            array_shift($infosLine);
            if (!count($infosLine)) {
                continue;
            }

            $key = 0;
            $productLine = [];
            foreach ($infosLine as $info) {
                $productLine[] = $info;
                $key++;
                if (($key % 7) == 0) {
                    if ($key == 7) {
                        $key = 0;

                        $product = $this->format($productLine);

                        // TMP
                        if ($product['taxon'] == 'SAL') {
                            $product['taxon'] = 'SALADE';
                        }
                        if ($product['taxon'] == 'POIREAUX') {
                            $product['taxon'] = 'POIREAU';
                        }
                        if ($product['taxon'] == 'CHAMP') {
                            $product['taxon'] = 'CHAMPIGNON';
                        }
                        if ($product['taxon'] == 'ARTICH.') {
                            $product['taxon'] = 'ARTICHAUT';
                        }
                        if ($product['taxon'] == 'ASP') {
                            $product['taxon'] = 'asperge';
                        }
                        if ($product['taxon'] == 'PDT') {
                            $product['taxon'] = 'pomme-de-terre';
                        }
                        if ($product['taxon'] == 'POIR') {
                            $product['taxon'] = 'poire';
                        }
                        if ($product['taxon'] == 'POM') {
                            $product['taxon'] = 'pomme';
                        }
                        if ($product['taxon'] == 'CHOU') {
                            $product['taxon'] = 'choux';
                        }
                        if ($product['taxon'] == 'OR') {
                            $product['taxon'] = 'orange';
                        }
                        if ($product['taxon'] == 'CLEM') {
                            $product['taxon'] = 'clementine';
                        }
                        if ($product['taxon'] == 'FRAMBOISES') {
                            $product['taxon'] = 'framboise';
                        }
                        if ($product['taxon'] == 'FRITE') {
                            $product['taxon'] = 'frites';
                        }
                        // TMP

                        if ($product) {
                            $products[] = $product;
                        }
                    }
                    $productLine = [];
                }
            }
        }

        return $products;
    }

    protected function format($line)
    {

        if ($line[0] == "" ||
            $line[1] == "" ||
            $line[2] == "" ||
            $line[3] == "" ||
            $line[4] == "" ||
            $line[3] == "0.00" ||
            $line[4] == "0,00 €" ||
            $line[4] == "0.00 €" ||
            in_array($line[1], ['PRODUIT'])
        ) {
            return;
        }

        if (array_key_exists($line[0], $this->alreadyImportedProduct)) {
            return;
        }
        
        $this->alreadyImportedProduct[$line[0]] = 1;

        /*
         * Les fruits sont en barquette de 250 Gr
         * Les produits au kilo avec colis de x kgs sont achetables par 1 kg
         * Les produits en x barquette sont achetables à la barquette
         *
         */

        $unitQuantity = 1;
        $unitContentQuantity = 1;
        $unitContentMeasurement = 'KG';

        $reference = $line[0];
        $name = $line[1];

        // for better country indentification
        $name = str_replace('C1', ' C1', $name);
        // remove multiple space
        $name = preg_replace('/\s+/', ' ', $name);
        $name = preg_replace('/\*\s/', '', $name);

        $caliber = '';
        $patternCaliber = '/(\d+\/\d+)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
        }
        $patternCaliber = '/CAL (\d+\/?\d?)/i';
        if (preg_match($patternCaliber, $name, $variantCaliber)) {
            $caliber = str_replace('-', '/', $variantCaliber[1]);
            $name = str_replace('CAL ', '', $name);
        }
        $name = str_replace($caliber, '', $name);

        $country = '';
        $patternCountry = "/\S?(MADAGASCAR|ANTILLES|EQUATEUR|VIET NAM|COLOMBIE|MARTINIQ|THAIL|ISRAEL|GREC|MAURICE|TURQUIE|COSTA RICA|PORTUGAL|PORT|BENIN|MEX|BELGIQUE|BRESIL|BRES|HOLL|POL|MAROC|KEN|ZIMB|ITAL|ITALIE|FRANCE|ESPAGNE|ESP|HOL|HO)$/";
        preg_match($patternCountry, $name, $country);
        $originCountry = array_key_exists(0, $country) ? $country[0] : '';
        $name = str_replace($originCountry, '', $name);

        $containing = "( )?(BOTTE|BOX|BARQUETTE|BARQ|BQ|BARQ\.|CAISSE|SAC(HET)?|PLATEAU|PCS|PIECES|VRAC|CAISSE|COLIS)";
        $pattern = '/' . $containing . '/';
        preg_match($pattern, $name, $conditioning);
        $conditioning = array_key_exists(0, $conditioning) ? $conditioning[0] : null;
        $name = str_replace($conditioning, '', $name);
        if (!empty($conditioning)) {
            $conditioning = 'CO_'.$conditioning;
        }

        $patternContent = "/(X |X)[0-9]+/";
        preg_match($patternContent, $name, $content);
        $unitQuantity = array_key_exists(0, $content) ? str_replace('X', '', $content[0]) : '1';

        $salesUnit = strtoupper(str_replace('/', '', $line[5])) == 'K' ? 'KG' : 'PC';

        $patternContent = "/([0-9]+)(KG|K|G)*/";
        preg_match($patternContent, $name, $unitWeight);
        if (array_key_exists(2, $unitWeight)) {
            $name = str_replace($unitWeight[0], '', $name);
            $unitContentQuantity = $unitWeight[1];
            if ($unitWeight[2] == 'G') {
                $unitContentMeasurement = 'GR';
                $unitWeight = bcdiv($unitWeight[1], 1000, 3);
            } else {
                $unitContentMeasurement = 'KG';
                $unitWeight = $unitWeight[1];
            }
        } else {
            if ($salesUnit == 'KG') {
                $unitWeight = 1;
            } else {
                $unitWeight = 0;
            }
        }

        $taxon = explode(' ', $line[1])[0];
        if (in_array($taxon, ['HF', 'BIO', 'MINI'])) {
            $taxon = explode(' ', $line[1])[1];
        }

        $weight = floatval(str_replace(',', '.', $line[3]));
        $price = floatval(str_replace(',', '.', $line[4])) * 100;

        if ($salesUnit == 'KG' || $unitWeight == 1) {
            $variant = '1 KG';
            $priceKg = $price;
            $priceVariant = $price;
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';
        } else {
            $variant = 'LA PIECE';
            if ($unitWeight) {
                $priceKg = bcdiv($price, $unitWeight, 0);
            } else {
                $priceKg = 999;
            }
            $priceVariant = $price;
            if (!$unitWeight) {
                $unitContentQuantity = 1;
                $unitContentMeasurement = 'PIECE';
            }
        }

        $orderUnit = '';
        if ($salesUnit == 'KG') {
            $orderUnit = 'ORDER_KG';
        } else {
            $orderUnit = 'ORDER_PC';
        }

        $bio = '';
        $bioPattern = '/BIO$/i';
        if (preg_match($bioPattern, $name, $bioMatches)) {
            $bio = 'BIO';
        } else {
            $bio = 'NOBIO';
        }

        $return = [
            'reference'    => '',
            'supplierName' => $line[1],
            'productName' => $this->normalizeSpaces($name),
            'variantName' => '-',
            'taxon' => $taxon,
            'weight' => $unitWeight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceVariant' => $priceVariant,
            'originCountry' => $originCountry,
            'salesUnit' => trim($salesUnit),
            'orderUnit' => trim($orderUnit),
            'conditioning' => $this->normalizeSpaces($conditioning),
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];

        return $return;
    }
}
