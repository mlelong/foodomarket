<?php

namespace AppBundle\Services\Fetcher;

use Cocur\Slugify\Slugify;

class ChassineauMercurialFetcher extends Fetcher
{

    protected $format = 'text';

    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {
            //dump($line);
            if (stripos($line, ',') === false) {
                continue;
            }

            $product = $this->format($line);
            //dump($product);
            if ($product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($line)
    {
        $pattern = "/(\d{3}-\d{1}-\d{2})\s(.*)\s(\d+,\d{2})/i";
        preg_match($pattern, $line, $infos);

        if (count($infos) == 0) {
            return;
        }

        //echo "---------------------------------------------------------------------------------------------\n";
        //dump($infos);

        $reference = $infos[1];
        $name = $infos[2];
        $salesUnit = 'KG';
        $price = intval(floatval(str_replace(',', '.', $infos[3])) * 100);
        $variant = '';
        $weight = 0;
        $unitQuantity = 1;
        $unitContentQuantity = 1;
        $unitContentMeasurement = 'KG';

        if (stripos($name, ' PC') !== false) {
            $salesUnit = 'PC';
        }

        if ($salesUnit == 'KG') {
            $weight = 1;
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';
        } else {
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'PIECE';
        }

        $patternCountry = "/(NORVÈGE|ECOSSE|HOLLANDE|FRANCE|ARGENTINE|IRLANDE|IRLANDAISE)/i";
        preg_match($patternCountry, $name, $country);
        $originCountry = array_key_exists(0, $country) ? $country[0] : '';
        $name = trim(str_replace($originCountry, '', $name));
        $name = preg_replace('/ origine /', '', $name);

        $name = preg_replace('/\s+/', ' ', $name);
        $name = preg_replace('/\*\s/', '', $name);

        $conditioning = '';

        $patternWeight = "/([\/.,\d\s]+)\s*(K|KG|KGS|GR|G)/i";
        preg_match($patternWeight, $name, $variantWeight);

        $KgOrGr = 'KG';
        if (array_key_exists(1, $variantWeight) && $variantWeight[1] > 0) {
            if (array_key_exists(2, $variantWeight)) {
                $KgOrGr = strtoupper($variantWeight[2]);
                if ($KgOrGr == 'G') {
                    $KgOrGr = 'GR';
                }
                $unitContentMeasurement = $KgOrGr;
            }
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $variantWeight[1]));

            if ($KgOrGr == 'GR') {
                $weight = bcdiv($weight, 1000, 3);
            }
            if ($weight == 0) {
                $weight = 1;
            }
        }

        $category = '';
        $caliber = '';

        $totalWeight = bcmul($weight, $unitQuantity, 3);
        $priceVariant = bcmul($weight, $price);

        if ($salesUnit == 'KG') {
            $priceKg = $price;
        } else {
            $priceKg = 99900;
            if ($totalWeight) {
                $priceKg = bcdiv($priceVariant, $totalWeight, 0);
            }
        }

        $bio = 'NOBIO';

        $orderUnit = '';
        if ($salesUnit == 'KG') {
            $orderUnit = 'ORDER_KG';
        } else {
            $orderUnit = 'ORDER_PC';
        }

        if (stripos($name, 'piécé') !== false || stripos($name, 'piece') !== false) {
            $orderUnit = 'ORDER_PC';
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'PIECE';
        }

        return [
            'reference'    => $reference,
            'supplierName' => $infos[2],
            'productName' => $this->normalizeSpaces($name),
            'variantName' => '-',
            'taxon' => explode(' ', $name)[0],
            'weight' => $totalWeight,
            'price' => $price,
            'priceKg' => $priceKg,
            'priceVariant' => $priceVariant,
            'originCountry' => trim($originCountry),
            'salesUnit' => trim($salesUnit),
            'orderUnit' => trim($orderUnit),
            'conditioning' => $this->normalizeSpaces($conditioning),
            'category' => $this->slugifyOption('CC', $category),
            'caliber' => $this->normalizeSpaces($caliber),
            'naming' => '',
            'bio' => $bio,
            'brand' => '',
            'content' => 0,
            'unitQuantity' => $unitQuantity,
            'unitContentQuantity' => $unitContentQuantity,
            'unitContentMeasurement' => 'UCM_'.$unitContentMeasurement,
        ];
    }

    protected function slugifyOption($code, $value) {
        if ($value == '') {
            return '';
        }
        $code = strtolower(trim($code));
        $value = strtolower(trim($value));

        return Slugify::create()->slugify("{$code}_{$value}");
    }
}
