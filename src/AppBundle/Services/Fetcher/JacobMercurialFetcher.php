<?php

namespace AppBundle\Services\Fetcher;

class JacobMercurialFetcher extends Fetcher
{
    public function getProducts($filename)
    {
        $lines = $this->extractLines($filename);

        return $this->extractProducts($lines);
    }

    protected function extractProducts($lines)
    {
        $products = [];

        foreach ($lines as $key => $line) {

            if ($key < 2) {
                continue;
            }

            if (stripos($line, "\t") !== FALSE) {
                $infosLine = explode("\t", $line);
                array_shift($infosLine);
            }
            else if (stripos($line, ";") !== FALSE) {
                $infosLine = explode(";", $line);
            }

            if (count($infosLine) < 3) {
                continue;
            }

            $product = $this->format($infosLine);

            if ($product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    protected function format($line)
    {

        if ($line[0] == 'product-id') {
            return;
        }

        if (in_array($line[4], ['Kilogramme'])) {
            $infos['salesUnit'] = 'KG';
        } else if (in_array($line[4], ['Unité'])) {
            $infos['salesUnit'] = 'PC';
        } else {
            $infos['salesUnit'] = 'CO';
        }

        $infos['price'] = 0;
        if (array_key_exists(7, $line) && $line[7] != '') {
            $infos['price']  = intval(floatval(str_replace([','], '.', $line[7]))* 100);
        }
        $infos['taxon']                  = explode(' ', $line[2])[0];
        $infos['reference']              = $line[3];
        $infos['supplierName']           = $this->normalizeSpaces($line[2]);
        $infos['productName']            = $this->normalizeSpaces($line[2]);
        $infos['variantName']            = '';

        $unitQuantity = 1;
        if ($line[4] == 'Kilogramme') {
            $weight = 1;
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'KG';
        } else {
            $unitContentQuantity = 1;
            $unitContentMeasurement = 'PIECE';
        }

        $weight = 0;
        $patternWeight = "/([\/.,\d]+\s*(KG|GR|G|L|ML))/i";
        preg_match($patternWeight, $line[4], $variantWeight);

        if (array_key_exists(1, $variantWeight) && strlen($variantWeight[1]) > 1) {
            $unitContentMeasurement = 'KG';
            if (array_key_exists(2, $variantWeight)) {
                $unitContentMeasurement = strtoupper($variantWeight[2]);
            }
            $weight = str_ireplace(['KG', 'GR', 'G', 'L', 'ML'], '', $variantWeight[1]);
            $unitContentQuantity = $weight = floatval(str_replace(',', '.', $weight));

            if ($unitContentMeasurement == 'GR' || $unitContentMeasurement == 'G') {
                $unitContentMeasurement = 'GR';
                $weight = bcdiv($weight, 1000, 3);
            }
            if ($unitContentMeasurement == 'ML') {
                $weight = bcdiv($weight, 1000, 3);
            }
        }

        if ($infos['salesUnit'] == 'PC') {
            $patternContent = "/(X |X)([0-9]+)/i";
            preg_match($patternContent, $line[4], $pregUnitQuantity);
            if (array_key_exists(2, $pregUnitQuantity)) {
                $unitQuantity = $pregUnitQuantity[2];
            }

            $patternContent = "/([0-9]+)\spièces/i";
            preg_match($patternContent, $line[4], $pregUnitQuantity);
            if (array_key_exists(1, $pregUnitQuantity)) {
                $unitQuantity = $pregUnitQuantity[1];
            }
        }

        $infos['weight']                 = floatval($weight);
        $infos['priceKg']                = 0;
        $infos['priceUnit']              = 0;
        $infos['priceVariant']           = 0;
        $infos['originCountry']          = '';
        //$infos['salesUnit']              = $line[1];
        $infos['orderUnit']              = 'ORDER_' . $infos['salesUnit'];
        $infos['conditioning']           = '';
        $infos['category']               = '';
        $infos['caliber']                = '';
        $infos['naming']                 = '';
        $infos['bio']                    = 'NOBIO';
        $infos['brand']                  = '';
        $infos['unitQuantity']           = $unitQuantity;
        $infos['unitContentQuantity']    = $unitContentQuantity;
        $infos['unitContentMeasurement'] = 'UCM_' . $unitContentMeasurement;

        if (empty($infos['variantName'])) {
            $infos['variantName'] = '-';
        }

        if (!empty($infos['conditioning'])) {
            $infos['conditioning'] = 'CO_'.$infos['conditioning'];
        }

        if ($infos['salesUnit'] == 'KG') {

            if ($infos['weight'] == 0) {
                $infos['weight'] = 1;
            }

            $infos['priceKg'] = $infos['price'];
            $infos['priceUnit'] = bcmul($infos['price'], $infos['weight'], 0);
            $infos['priceVariant'] = bcmul($infos['price'], $infos['weight'], 0);
        }

        if ($infos['salesUnit'] == 'PC') {
            $infos['priceUnit'] = $infos['price'];
            $infos['priceVariant'] = bcmul($infos['unitQuantity'], $infos['price'], 0);
            if ($infos['weight']) {
                $infos['priceKg'] = bcdiv($infos['priceVariant'], $infos['weight'], 0);
            } else {
                $infos['priceKg'] =  0;
            }
        }

        if ($infos['salesUnit'] == 'L') {
            $infos['priceUnit'] = $infos['price'];
            $infos['priceVariant'] = bcmul($infos['unitQuantity'], $infos['price'], 0);
            if ($infos['weight']) {
                $infos['priceKg'] = bcdiv($infos['priceVariant'], $infos['weight'], 0);
            } else {
                $infos['priceKg'] =  0;
            }
        }

        if ($infos['salesUnit'] == 'CO') {
            $infos['priceUnit'] = $infos['price'];
            $infos['priceVariant'] = $infos['price'];
            if ($infos['weight']) {
                $infos['priceKg'] = bcdiv($infos['priceVariant'], $infos['weight'], 0);
            } else {
                $infos['priceKg'] = 0;
            }
        }

        return $infos;
    }
}