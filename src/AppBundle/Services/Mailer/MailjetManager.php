<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Email;
use Mailjet\Resources;

/**
 * Class MailjetManager
 * @package AppBundle\Services\Mailer
 */
class MailjetManager extends AbstractMailManager
{
    /** @var \Mailjet\Client */
    private $client30;
    /** @var \Mailjet\Client  */
    private $client31;

    /**
     * MailjetManager constructor.
     * @param string $apiKey
     * @param string $apiSecret
     */
    public function __construct(string $apiKey, string $apiSecret)
    {
        $this->client30 = new \Mailjet\Client($apiKey, $apiSecret,true, ['version' => 'v3']);
        $this->client31 = new \Mailjet\Client($apiKey, $apiSecret,true, ['version' => 'v3.1']);
    }
    /**
     * @return Template[]
     */
    function getTemplates(): array
    {
        $response = $this->client30->get(Resources::$Template, [
            'filters' => [
                'OwnerType' => 'user'
            ]
        ]);

        $templates = [];

        if ($response->success()) {
            $data = $response->getData();

            foreach ($data as $record) {
                $detailContent = $this->client30->get(Resources::$TemplateDetailcontent, ['id' => $record['ID']]);

                if ($detailContent->success()) {
                    $detailData = $detailContent->getData();

                    $template = new Template();

                    $template->setTemplateId($record['ID']);
                    $template->setCampaignName($record['Name']);
                    $template->setSubject($detailData[0]['Headers']['Subject']);
                    $template->setContent($detailData[0]['Html-part']);

                    if (preg_match_all('/{{[\S\s]*:(?P<params>[\S\s]+):[\S\s]*}}/', $detailData[0]['Headers']['Subject'], $matches)) {
                        $template->setSubjectVars($matches['params']);
                    }

                    if (preg_match_all('/{{[\S\s]*:(?P<params>[\S\s]+):[\S\s]*}}/', $detailData[0]['Html-part'], $matches)) {
                        $template->setContentVars($matches['params']);
                    }

                    $templates[] = $template;
                }
            }
        }

        return $templates;
    }

    private function buildRecipients(array $recipients)
    {
        $recipientsArray = [];

        foreach ($recipients as $email => $name) {
            if (filter_var($email,FILTER_VALIDATE_EMAIL)) {
                $recipientsArray[] = ['Email' => $email, 'Name' => $name];
            } elseif (filter_var($name, FILTER_VALIDATE_EMAIL)) {
                $recipientsArray[] = ['Email' => $name, 'Name' => $name];
            }
        }

        return $recipientsArray;
    }

    /**
     * /!\ Must not exceed 15MB
     *
     * @param array $attachments
     * @return array
     */
    private function buildAttachments(array &$attachments) {
        $attachmentsArray = [];

        foreach ($attachments as $attachmentName => $attachmentContent) {
            $attachmentsArray[] = [
                'Filename' => $attachmentName,
                'Base64Content' => $attachmentContent,
                'ContentType' => $this->getMimeType($attachmentName)
            ];
        }

        return $attachmentsArray;
    }

    function sendEmail(Email $email)
    {
        $data = ['Messages' => [[]]];

        $from = $this->buildRecipients($email->getFrom());
        $replyTo = $this->buildRecipients($email->getFrom());
        $to = $this->buildRecipients($email->getTo());
        $cc = $this->buildRecipients($email->getCc());
        $bcc = $this->buildRecipients($email->getBcc());
        $attachments = $email->getAttachments();

        /**
         * Only 1 From email address is allowed for Mailjet's API
         */
        if (!empty($from)) {
            $data['Messages'][0]['From'] = $from[0];
        }

        /**
         * Only 1 ReplyTo email address is allowed for Mailjet's API
         */
        if (!empty($replyTo)) {
            $data['Messages'][0]['ReplyTo'] = $replyTo[0];
        }

        if (!empty($to)) {
            $data['Messages'][0]['To'] = $to;
        }

        if (!empty($cc)) {
            $data['Messages'][0]['Cc'] = $cc;
        }

        if (!empty($bcc)) {
            $data['Messages'][0]['Bcc'] = $bcc;
        }

        if (!empty($email->getSubject())) {
            $data['Messages'][0]['Subject'] = $email->getSubject();
        }

        if (!empty($attachments)) {
            $data['Messages'][0]['Attachments'] = $this->buildAttachments($attachments);
        }

        if ($email->getTemplateId() !== null) {
            $data['Messages'][0]['TemplateID'] = (int)$email->getTemplateId();
            $data['Messages'][0]['TemplateLanguage'] = true;
            $data['Messages'][0]['Variables'] = $email->getVariables();
        } else {
            if (!empty($email->getHtmlBody())) {
                $data['Messages'][0]['HTMLPart'] = $email->getHtmlBody();
            }

            if (!empty($email->getTextBody())) {
                $data['Messages'][0]['TextPart'] = $email->getTextBody();
            }
        }

        $response = $this->client31->post(Resources::$Email, ['body' => $data]);

        if ($response->success()) {
            $this->logResponse($email, [
                'status' => $response->getStatus(),
                'message' => $response->getReasonPhrase(),
                'body' => $response->getBody()
            ]);

            return true;
        } else {
            $this->logError($email, [
                'status' => $response->getStatus(),
                'message' => $response->getReasonPhrase(),
                'body' => $response->getBody()
            ]);

            return false;
        }
    }
}