<?php

namespace AppBundle\Services\Mailer;

class Template
{
    /**
     * @var string
     */
    private $templateId;

    /**
     * @var string
     */
    private $campaignName;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var array
     */
    private $subjectVars = [];

    /**
     * @var array
     */
    private $contentVars = [];

    /**
     * @return string
     */
    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    /**
     * @param string $templateId
     */
    public function setTemplateId(string $templateId): void
    {
        $this->templateId = $templateId;
    }

    /**
     * @return string
     */
    public function getCampaignName(): string
    {
        return $this->campaignName;
    }

    /**
     * @param string $campaignName
     */
    public function setCampaignName(string $campaignName): void
    {
        $this->campaignName = $campaignName;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getSubjectVars(): array
    {
        return $this->subjectVars;
    }

    /**
     * @param array $subjectVars
     */
    public function setSubjectVars(array $subjectVars): void
    {
        $this->subjectVars = $subjectVars;
    }

    /**
     * @return array
     */
    public function getContentVars(): array
    {
        return $this->contentVars;
    }

    /**
     * @param array $contentVars
     */
    public function setContentVars(array $contentVars): void
    {
        $this->contentVars = $contentVars;
    }
}