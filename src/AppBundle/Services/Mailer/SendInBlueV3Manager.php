<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Email;
use SendinBlue\Client\Api\SMTPApi;
use AppBundle\Services\RedisCacheManager;
use SendinBlue\Client\Model\CreateSmtpEmail;
use SendinBlue\Client\Model\GetSmtpTemplateOverview;
use SendinBlue\Client\Model\GetSmtpTemplates;
use SendinBlue\Client\Model\SendEmail;
use SendinBlue\Client\Model\SendEmailAttachment;
use SendinBlue\Client\Model\SendSmtpEmail;
use SendinBlue\Client\Model\SendSmtpEmailAttachment;
use SendinBlue\Client\Model\SendSmtpEmailBcc;
use SendinBlue\Client\Model\SendSmtpEmailCc;
use SendinBlue\Client\Model\SendSmtpEmailReplyTo;
use SendinBlue\Client\Model\SendSmtpEmailSender;
use SendinBlue\Client\Model\SendSmtpEmailTo;
use SendinBlue\Client\Model\SendTemplateEmail;

/**
 * Class SendInBlueManager
 * @package AppBundle\Services\Mailer
 */
class SendInBlueV3Manager extends AbstractMailManager
{
    /**
     * @var SMTPApi
     */
    private $client;

    /**
     * SendInBlueManager constructor.
     * @param SMTPApi $client
     */
    public function __construct(SMTPApi $client)
    {
        $this->client = $client;
    }

    /**
     * @return Template[]
     */
    function getTemplates(): array
    {
        $templates = $this->cacheManager->get(
            RedisCacheManager::CACHE_KEY_SENDINBLUE_TEMPLATES,
            86400,
            function () {
                /** @var GetSmtpTemplates $result */
                $result =  $this->client->getSmtpTemplates();
                $templates = [];

                /** @var GetSmtpTemplateOverview $record */
                foreach ($result->getTemplates() as $record) {
                    $template = new Template();

                    $template->setTemplateId($record->getId());
                    $template->setCampaignName($record->getName());
                    $template->setSubject($record->getSubject());
                    $template->setContent($record->getHtmlContent());

                    if (preg_match_all('/%(?P<params>[a-zA-Z_]+)%/u', $record->getSubject(), $matches)) {
                        $template->setSubjectVars($matches['params']);
                    }

                    if (preg_match_all('/%(?P<params>[a-zA-Z_]+)%/u', html_entity_decode($record->getHtmlContent()), $matches)) {
                        $template->setContentVars(array_unique($matches['params']));
                    }

                    $templates[] = $template;
                }

                return $templates;
            }
        );

        return $templates;
    }

    /**
     * @param array $recipients
     * @return array
     */
    private function buildRecipients(array $recipients)
    {
        $recipientsArray = [];

        foreach ($recipients as $email => $name) {
            if (filter_var($email,FILTER_VALIDATE_EMAIL)) {
                $recipientsArray[] = $email;
            } elseif (filter_var($name, FILTER_VALIDATE_EMAIL)) {
                $recipientsArray[] = $name;
            }
        }

        return array_unique($recipientsArray);
    }

    function sendEmail(Email $email)
    {
        try {
            if ($email->getTemplateId() !== null) {
                $sendEmail = new SendEmail();

                if (!empty($email->getVariables())) {
                    $sendEmail->setAttributes($email->getVariables());
                }

                $sendEmail->setEmailTo($this->buildRecipients($email->getTo()));

                if (!empty($email->getBcc())) {
                    $sendEmail->setEmailBcc($this->buildRecipients($email->getBcc()));
                }

                if (!empty($email->getCc())) {
                    $sendEmail->setEmailCc($this->buildRecipients($email->getCc()));
                }

                $replyTo = $this->buildRecipients($email->getReplyTo());

                if (!empty($replyTo)) {
                    $sendEmail->setReplyTo($replyTo[0]);
                }

                $sendEmailAttachments = [];

                foreach ($email->getAttachments() as $attachmentName => $attachment) {
                    $sendEmailAttachment = new SendEmailAttachment(['name' => $attachmentName, 'content' => $attachment]);
                    $sendEmailAttachments[] = $sendEmailAttachment;
                }

                if (!empty($sendEmailAttachments)) {
                    $sendEmail->setAttachment($sendEmailAttachments);
                }

                /** @var SendTemplateEmail $response */
                $response = $this->client->sendTemplate($email->getTemplateId(), $sendEmail);
            } else {
                $sendEmail = new SendSmtpEmail();

                $sendEmail->setSubject($email->getSubject());

                $to = [];

                foreach ($this->buildRecipients($email->getTo()) as $dst) {
                    $sendSmtpEmailTo = new SendSmtpEmailTo(['email' => $dst]);
                    $to[] = $sendSmtpEmailTo;
                }

                $sendEmail->setTo($to);

                $cc = [];

                foreach ($this->buildRecipients($email->getCc()) as $dst) {
                    $sendSmtpEmailCc = new SendSmtpEmailCc(['email' => $dst]);
                    $cc[] = $sendSmtpEmailCc;
                }

                if (!empty($cc)) {
                    $sendEmail->setCc($cc);
                }

                $bcc = [];

                foreach ($this->buildRecipients($email->getBcc()) as $dst) {
                    $sendSmtpEmailBcc = new SendSmtpEmailBcc(['email' => $dst]);
                    $bcc[] = $sendSmtpEmailBcc;
                }

                if (!empty($bcc)) {
                    $sendEmail->setBcc($bcc);
                }

                $from = $this->buildRecipients($email->getFrom());
                $replyTo = $this->buildRecipients($email->getReplyTo());

                if (!empty($from)) {
                    $sendEmail->setSender(new SendSmtpEmailSender(['email' => $from[0]]));
                }

                if (!empty($replyTo)) {
                    $sendEmail->setReplyTo(new SendSmtpEmailReplyTo(['email' => $replyTo[0]]));
                }

                if ($email->getHtmlBody() !== null) {
                    $sendEmail->setHtmlContent($email->getHtmlBody());
                }

                if ($email->getTextBody() !== null) {
                    $sendEmail->setTextContent($email->getTextBody());
                }

                $sendEmailAttachments = [];

                foreach ($email->getAttachments() as $attachmentName => $attachment) {
                    $sendEmailAttachment = new SendSmtpEmailAttachment(['name' => $attachmentName, 'content' => $attachment]);
                    $sendEmailAttachments[] = $sendEmailAttachment;
                }

                if (!empty($sendEmailAttachments)) {
                    $sendEmail->setAttachment($sendEmailAttachments);
                }

                /** @var CreateSmtpEmail $response */
                $response = $this->client->sendTransacEmail($sendEmail);
            }

            if (!$response->valid()) {
                $response = [
                    'valid' => $response->valid(),
                    'message' => $response->getMessageId(),
                    'errors' => $response->listInvalidProperties()
                ];

                $this->logError($email, $response);
                return false;
            } else {
                $this->logResponse($email, ['valid' => $response->valid(), 'message' => $response->getMessageId()]);
                return true;
            }
        }

        catch (\Exception $e) {
            $response = ['code' => 'failure', 'message' => $e->getMessage()];
            $this->logError($email, $response);

            return false;
        }
    }
}