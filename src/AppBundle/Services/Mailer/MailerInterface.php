<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Email;

interface MailerInterface
{
    /**
     * @return Template[]
     */
    function getTemplates(): array;

    function sendEmail(Email $email);

    function addEmailToQueue(Email $email);
}