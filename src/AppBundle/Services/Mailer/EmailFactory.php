<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Email;
use Sylius\Component\Resource\Factory\FactoryInterface;

class EmailFactory implements FactoryInterface
{
    /**
     * @return Email
     */
    public function createNew(): Email
    {
        return new Email();
    }

    /**
     * @param string $mailManager
     * @return EmailBuilder
     */
    public function createBuilder(string $mailManager): EmailBuilder
    {
        return (new EmailBuilder($this->createNew()))->setMailManagerName($mailManager);
    }
}