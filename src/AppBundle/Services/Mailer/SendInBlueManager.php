<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Email;
use SendinBlue\SendinBlueApiBundle\Wrapper\Mailin;
use AppBundle\Services\RedisCacheManager;

/**
 * Class SendInBlueManager
 * @package AppBundle\Services\Mailer
 */
class SendInBlueManager extends AbstractMailManager
{
    /**
     * @var Mailin
     */
    private $client;

    /**
     * SendInBlueManager constructor.
     * @param Mailin $client
     */
    public function __construct(Mailin $client)
    {
        $this->client = $client;
    }

    /**
     * @return Template[]
     */
    function getTemplates(): array
    {

        $templates = $this->cacheManager->get(
            RedisCacheManager::CACHE_KEY_SENDINBLUE_TEMPLATES,
            86400,
            function () {
                $result =  $this->client->get_campaigns_v2(['type' => 'template', 'status' => 'temp_active']);
                $templates = [];

                if ($result['code'] == 'success') {
                    foreach ($result['data']['campaign_records'] as $record) {
                        $template = new Template();

                        $template->setTemplateId($record['id']);
                        $template->setCampaignName($record['campaign_name']);
                        $template->setSubject($record['subject']);
                        $template->setContent($record['html_content']);

                        if (preg_match_all('/%(?P<params>[a-zA-Z_]+)%/u', $record['subject'], $matches)) {
                            $template->setSubjectVars($matches['params']);
                        }

                        if (preg_match_all('/%(?P<params>[a-zA-Z_]+)%/u', html_entity_decode($record['html_content']), $matches)) {
                            $template->setContentVars(array_unique($matches['params']));
                        }

                        $templates[] = $template;
                    }
                }

                return $templates;
            }
        );

        return $templates;

    }

    /**
     * @param array $recipients
     * @return string
     */
    private function buildRecipientsString(array $recipients)
    {
        $recipientsString = '';

        foreach ($recipients as $email => $name) {
            if (filter_var($email,FILTER_VALIDATE_EMAIL)) {
                $recipientsString .= "{$email}|";
            } elseif (filter_var($name, FILTER_VALIDATE_EMAIL)) {
                $recipientsString .= "{$name}|";
            }
        }

        if (!empty($recipientsString)) {
            return substr($recipientsString, 0, -1);
        }

        return $recipientsString;
    }

    /**
     * @param array $recipients
     * @return array
     */
    private function buildRecipients(array $recipients)
    {
        $recipientsArray = [];

        foreach ($recipients as $email => $name) {
            if (filter_var($email,FILTER_VALIDATE_EMAIL)) {
                $recipientsArray[$email] = $name;
            } elseif (filter_var($name, FILTER_VALIDATE_EMAIL)) {
                $recipientsArray[$name] = $name;
            }
        }

        return $recipientsArray;
    }

    function sendEmail(Email $email)
    {
        $data = [
            'headers' => array("Content-Type"=> "text/html;charset=iso-8859-1"),
        ];

        if (!empty($email->getAttachments())) {
            $data['attachment'] = $email->getAttachments();
        }

        if (!empty($email->getSubject())) {
            $data['subject'] = $email->getSubject();
        }

        try {
            if ($email->getTemplateId() !== null) {
                $data['id'] = $email->getTemplateId();
                $data['attr'] = $email->getVariables();

                $from = $this->buildRecipientsString($email->getFrom());
                $to = $this->buildRecipientsString($email->getTo());
                $replyTo = $this->buildRecipients($email->getReplyTo());
                $cc = $this->buildRecipientsString($email->getCc());
                $bcc = $this->buildRecipientsString($email->getBcc());

                if (!empty($from)) {
                    $data['from'] = $from;
                }

                if (!empty($to)) {
                    $data['to'] = $to;
                }

                if (!empty($replyTo)) {
                    $data['replyto'] = array_keys($replyTo)[0];
                }

                if (!empty($cc)) {
                    $data['cc'] = $cc;
                }

                if (!empty($bcc)) {
                    $data['bcc'] = $bcc;
                }

                $response = $this->client->send_transactional_template($data);
            } else {
                $from = $this->buildRecipients($email->getFrom());
                $to = $this->buildRecipients($email->getTo());
                $replyTo = $this->buildRecipients($email->getReplyTo());
                $cc = $this->buildRecipients($email->getCc());
                $bcc = $this->buildRecipients($email->getBcc());

                if (!empty($from)) {
                    $data['from'] = array_keys($from);
                }

                if (!empty($to)) {
                    $data['to'] = $to;
                }

                if (!empty($replyTo)) {
                    $data['replyto'] = $replyTo;
                }

                if (!empty($cc)) {
                    $data['cc'] = $cc;
                }

                if (!empty($bcc)) {
                    $data['bcc'] = $bcc;
                }

                if ($email->getHtmlBody() !== null) {
                    $data['html'] = $email->getHtmlBody();
                }

                if ($email->getTextBody() !== null) {
                    $data['text'] = $email->getTextBody();
                }

                $response = $this->client->send_email($data);
            }

            if (isset($response['code']) && $response['code'] == 'failure') {
                throw new \Exception($response['message'], -1);
            }

            $this->logResponse($email, $response);
            return true;
        }

        catch (\Exception $e) {
            $response['code'] = 'failure';
            $response['message'] = $e->getMessage();

            $this->logError($email, $response);
            return false;
        }
    }
}