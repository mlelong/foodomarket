<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Email;

class EmailBuilder
{
    /**
     * @var Email
     */
    private $email;

    /**
     * EmailBuilder constructor.
     * @param Email $email
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * @param string $email
     * @param null|string $name
     * @return $this
     */
    public function addTo(string $email, ?string $name = null)
    {
        if (strlen($email) < 3) {
            return $this;
        }

        $this->email->setTo($this->addAssociativeEmail($this->email->getTo(), $email, $name));

        return $this;
    }

    /**
     * @param string $email
     * @param null|string $name
     * @return $this
     */
    public function addReplyTo(string $email, ?string $name = null)
    {
        if (strlen($email) < 3) {
            return $this;
        }

        $this->email->setReplyTo($this->addAssociativeEmail($this->email->getReplyTo(), $email, $name));

        return $this;
    }

    /**
     * @param string $email
     * @param null|string $name
     * @return $this
     */
    public function addCc(string $email, ?string $name = null)
    {
        if (strlen($email) < 3) {
            return $this;
        }

        $this->email->setCc($this->addAssociativeEmail($this->email->getCc(), $email, $name));

        return $this;
    }

    /**
     * @param string $email
     * @param null|string $name
     * @return $this
     */
    public function addBcc(string $email, ?string $name = null)
    {
        if (strlen($email) < 3) {
            return $this;
        }

        $this->email->setBcc($this->addAssociativeEmail($this->email->getBcc(), $email, $name));

        return $this;
    }

    /**
     * @param string $email
     * @param null|string $name
     * @return $this
     */
    public function addFrom(string $email, ?string $name = null)
    {
        if (strlen($email) < 3) {
            return $this;
        }

        $this->email->setFrom($this->addAssociativeEmail($this->email->getFrom(), $email, $name));

        return $this;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject(string $subject)
    {
        $this->email->setSubject($subject);

        return $this;
    }

    /**
     * @param string $mailManagerName
     * @return $this
     */
    public function setMailManagerName(string $mailManagerName)
    {
        if ($mailManagerName === Email::MANAGER_SENDINBLUE || $mailManagerName === Email::MANAGER_MAILJET) {
            $this->email->setMailManagerName($mailManagerName);
        }

        return $this;
    }

    /**
     * @param string $templateId
     * @return $this
     */
    public function setTemplateId(string $templateId)
    {
        $this->email->setTemplateId($templateId);

        return $this;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setHtmlBody(string $body)
    {
        $this->email->setHtmlBody($body);

        return $this;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setTextBody(string $body)
    {
        $this->email->setTextBody($body);

        return $this;
    }

    /**
     * @param string $name
     * @param $value
     * @return $this
     */
    public function addVariable(string $name, $value)
    {
        $variables = $this->email->getVariables();
        $variables[$name] = $value;

        $this->email->setVariables($variables);

        return $this;
    }

    /**
     * @param string $attachmentName
     * @param string $attachmentContent
     * @return $this
     */
    public function addAttachment(string $attachmentName, string $attachmentContent)
    {
        $attachments = $this->email->getAttachments();
        $attachments[$attachmentName] = base64_encode($attachmentContent);

        $this->email->setAttachments($attachments);

        return $this;
    }

    /**
     * @param array $src
     * @param string $email
     * @param null|string $name
     * @return array
     */
    private function addAssociativeEmail(array $src, string $email, ?string $name = null)
    {
        if (isset($src[$email]) || in_array($email, $src)) {
            return $src;
        }

        if ($name !== null) {
            $src[$email] = $name;
        } else {
            $src[] = $email;
        }

        return $src;
    }

    /**
     * @return Email
     */
    public function build()
    {
        return $this->email;
    }
}