<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Repository\RestaurantStockItemRepository;
use AppBundle\Repository\RestaurantStockRepository;
use AppBundle\Services\Product\ProductFinderInterface;
use AppBundle\Services\ProductUtils;
use AppBundle\Services\RedisCacheManager;
use Doctrine\Common\Collections\ArrayCollection;

class ShoppingListManager
{
    /** @var RestaurantStockRepository */
    private $restaurantStockRepository;

    /** @var RestaurantStockItemRepository */
    private $restaurantStockItemRepository;

    /** @var ProductFinderInterface */
    private $productFinder;

    /** @var RedisCacheManager */
    private $cacheManager;

    /** @var ProductUtils */
    private $productUtils;

    /**
     * ShoppingListManager constructor.
     * @param RestaurantStockRepository $restaurantStockRepository
     * @param RestaurantStockItemRepository $restaurantStockItemRepository
     * @param ProductFinderInterface $productFinder
     * @param RedisCacheManager $cacheManager
     * @param ProductUtils $productUtils
     */
    public function __construct(
        RestaurantStockRepository $restaurantStockRepository,
        RestaurantStockItemRepository $restaurantStockItemRepository,
        ProductFinderInterface $productFinder,
        RedisCacheManager $cacheManager,
        ProductUtils $productUtils)
    {
        $this->restaurantStockRepository = $restaurantStockRepository;
        $this->restaurantStockItemRepository = $restaurantStockItemRepository;
        $this->productFinder = $productFinder;
        $this->cacheManager = $cacheManager;
        $this->productUtils = $productUtils;
    }

    /**
     * @param Restaurant $restaurant
     * @return array|null
     */
    public function getShoppingLists(Restaurant $restaurant)
    {
        return $this->cacheManager->get(
            str_replace('{restaurantId}', $restaurant->getId(), RedisCacheManager::CACHE_KEY_SHOP_USER_SHOPPING_LISTS),
            null,
            function() use ($restaurant) {
                /** @var RestaurantStock[] $shoppingLists */
                $shoppingLists = $this->restaurantStockRepository->findBy([
                    'restaurant' => $restaurant->getId(),
                    'deleted' => 0
                ]);

                $data = [];

                foreach ($shoppingLists as $shoppingList) {
                    $items = $shoppingList->getItems();
                    $suppliers = [];
                    $supplierNames = [];

                    foreach ($items as $item) {
                        $suppliers[] = $item->getSupplier()->getId();
                        $supplierNames[] = $item->getSupplier()->getName();
                    }

                    $data[] = [
                        'id' => $shoppingList->getId(),
                        'listName' => $shoppingList->getName(),
                        'nbItems' => $items->count(),
                        'nbSuppliers' => count(array_unique($suppliers)),
                        'supplierNames' => array_values(array_unique($supplierNames)),
                        'items' => $this->getShoppingListItems($shoppingList)
                    ];
                }

                return $data;
            }
        );
    }

    /**
     * @param RestaurantStock $shoppingList
     * @return array
     */
    public function getShoppingListItems(RestaurantStock $shoppingList)
    {
        $shoppingListItems = $shoppingList->getItems();
        $shoppingListItemsArray = [];

        foreach ($shoppingListItems as $shoppingListItem) {
            $shoppingListItemArray = $this->productFinder->getProduct($shoppingListItem->getProductVariant(), $shoppingListItem->getSupplier(), $shoppingList->getRestaurant());

            if ($shoppingListItemArray === null) {
                continue ;
            }

            $shoppingListItemArray['sid'] = $shoppingListItem->getId();
            $shoppingListItemArray['note'] = $shoppingListItem->getNote();
            $shoppingListItemArray['quantity'] = $shoppingListItem->getStock();
            $shoppingListItemArray['quantityUnitSelected'] = $shoppingListItem->getUnit();

            if (!$shoppingListItemArray['hasPrice']) {
                $supplier = $shoppingListItem->getSupplier();

                foreach ($shoppingList->getRestaurant()->getCustomers() as $customer) {
                    foreach ($customer->getProspect()->getSuppliers() as $supplierCategory) {
                        $partnerSupplier = $supplierCategory->getSupplier();

                        if ($partnerSupplier->getFacadeSupplier() === null) {
                            continue;
                        }

                        if ($partnerSupplier->getFacadeSupplier()->getId() === $supplier->getId()) {
                            $supplier = $partnerSupplier;
                            break 2;
                        }
                    }
                }

                $hasReplacement = false;
                /** @var ProductVariant[]|ArrayCollection $variants */
                $variants = $this->productUtils->getSimilarVariantsForSupplier($shoppingListItem->getProductVariant(), $supplier, true);

                // TODO: find
                foreach ($variants as $variant) {
                    $replacement = $this->productFinder->getProduct($variant, $shoppingListItem->getSupplier(), $shoppingList->getRestaurant());

                    if ($replacement !== null && $replacement['hasPrice']) {
                        $hasReplacement = true;

                        $replacement['note'] = $shoppingListItem->getNote();
                        $replacement['quantity'] = $shoppingListItem->getStock();
                        $replacement['quantityUnitSelected'] = $shoppingListItem->getUnit();

                        $shoppingListItemsArray[] = $replacement;
                    }
                }

                if (!$hasReplacement) {
                    $shoppingListItemArray['productName'] = $shoppingListItemArray['productName'] . ' (plus commercialisé)';
                    $shoppingListItemArray['isInCatalog'] = false;
                    $shoppingListItemsArray[] = $shoppingListItemArray;
                }
            } else {
                $shoppingListItemsArray[] = $shoppingListItemArray;
            }
        }

        usort($shoppingListItemsArray, function($a, $b) {
            return strcasecmp($a['productName'], $b['productName']);
        });

        /**
         * Let's remove the doublons
         */
        $existing = [];

        $shoppingListItemsArray = array_filter($shoppingListItemsArray, function($shoppingListItemArray) use(&$existing) {
            $key = "{$shoppingListItemArray['productId']}-{$shoppingListItemArray['supplierId']}";

            if (!isset($existing[$key])) {
                $existing[$key] = true;
                return true;
            }

            return false;
        });

        return array_values($shoppingListItemsArray);
    }
}