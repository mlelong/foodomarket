<?php

namespace AppBundle\Services\SellAndSign;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

class SellAndSignException extends Exception
{
    /** @var Prospect */
    protected $prospect;

    /** @var Supplier */
    protected $supplier;

    public function __construct(GuzzleException $previous, Prospect $prospect = null, Supplier $supplier = null)
    {
        parent::__construct($previous->getMessage(), $previous->getCode(), $previous);

        $this->prospect = $prospect;
        $this->supplier = $supplier;
    }

    public function __toString()
    {
        $messages = [];

        if ($this->prospect !== null) {
            $messages[] = "Prospect ({$this->prospect->getId()})";
        }

        if ($this->supplier !== null) {
            $messages[] = "Supplier ({$this->supplier->getId()})";
        }

        $messages[] = $this->getMessage();

        return implode(' : ', $messages);
    }
}