<?php

namespace AppBundle\Services\SellAndSign;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\SupplierAccountLogRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class SellAndSignService
{
    /** @var Client */
    private $client;

    /** @var SupplierAccountLogRepository */
    private $supplierAccountLogRepository;

    /** @var UploaderHelper */
    private $uploaderHelper;

    /** @var string */
    private $webDir;

    /** @var string */
    private $host;

    /** @var int */
    private $contractDefinitionId;

    /** @var int */
    private $actorId;

    /** @var string */
    private $jToken;

    /** @var string */
    private $vendorEmail;

    /**
     * Durée de vie du token sell&sign pour la signature d'un contrat (1 jour, en millisecondes)
     * @var int
     */
    private $tokenLifetime;

    /**
     * Returns the number of milliseconds ellapsed since epoch
     * @return int
     */
    public static function getCurrentMilliseconds()
    {
        $microtime = explode(' ', microtime());

        return (int) ($microtime[1] * 1000 + $microtime[0] * 1000);
    }

    /**
     * Enlève les accents et autres caractères non utf-8 d'une chaîne de caractère
     *
     * @param $string
     * @return bool|false|string
     */
    private static function asciiTranslit($string)
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    }

    public function __construct(
        SupplierAccountLogRepository $supplierAccountLogRepository,
        UploaderHelper $uploaderHelper,
        string $webDir,
        string $host,
        int $contractDefinitionId,
        int $actorId,
        string $jToken,
        string $vendorEmail,
        int $tokenLifetime
        )
    {
        $this->client = new Client([
            'timeout'  => 30.0,
        ]);

        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
        $this->uploaderHelper = $uploaderHelper;
        $this->webDir = $webDir;

        // Sell & Sign api requirements
        $this->host = $host;
        $this->contractDefinitionId = $contractDefinitionId;
        $this->actorId = $actorId;
        $this->jToken = $jToken;
        $this->vendorEmail = $vendorEmail;
        $this->tokenLifetime = $tokenLifetime;
    }

    /**
     * Retourne les urls de signature du contrat
     * iframe: l'url à utiliser dans une iframe (dans notre cas on utilisera toujours celle là)
     * page  : l'url à utiliser dans le cas d'une redirection (la signature se fait sur le site de sell&sign)
     *
     * @param int $contractId
     * @param string $customerNumber
     * @param string $jToken
     * @return array
     */
    public function getSigningUrls(int $contractId, string $customerNumber, string $jToken)
    {
        $urls = [
            'iframe' => "$this->host/calinda/s/sign_contract_index.html?direct_contract=1&cd_id={contract_definition_id}&c_id={contract_id}&page=1&no_ui=true&customer_number={customer_number}&j_token={j_token}",
            'page' => "$this->host/calinda/hub/selling/sign_contract.html?cd_id={contract_definition_id}&c_id={contract_id}&page=1&customer_number={customer_number}&j_token={j_token}"
        ];

        foreach ($urls as &$url) {
            $url = str_replace(
                ['{contract_definition_id}', '{contract_id}', '{customer_number}', '{j_token}'],
                [$this->contractDefinitionId, $contractId, urlencode($customerNumber), urlencode($jToken)],
                $url
            );
        }

        return $urls;
    }

    /**
     * Créé un contrat en une seule étape (créé le client, le signataire, upload le pdf et passe en signature)
     *
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @return array|null
     * @throws SellAndSignException
     */
    public function sendCommandPacket(Prospect $prospect, Supplier $supplier)
    {
        $supplier = $supplier->getFacadeSupplier();
        $filename = 'SEPA_' . str_replace(' ', '-', $supplier->getDisplayName()) . '_' . str_replace(' ', '-', $prospect->getCompanyName()) . '.pdf';
        $sepaPath = $this->webDir . $this->uploaderHelper->asset($supplier,'sepa');

        try {
            $response = $this->client->request(
                'POST',
                "$this->host/calinda/hub/selling/do?m=sendCommandPacket",
                [
                    RequestOptions::HEADERS => ['j_token' => $this->jToken],
                    RequestOptions::MULTIPART => [
                        [
                            'name'      => 'adhoc_light.sellsign',
                            'contents'  => \GuzzleHttp\json_encode([
                                'customer' => [
                                    'number' => "P{$prospect->getId()}",
                                    'mode' => 1,
                                    'contractor_id' => -1,
                                    'vendor' => $this->vendorEmail,
                                    'fields' => [
                                        ['key' => 'firstname', 'value' => self::asciiTranslit($prospect->getFirstName())],
                                        ['key' => 'lastname', 'value' => self::asciiTranslit($prospect->getContactName())],
                                        ['key' => 'civility', 'value' => self::asciiTranslit($prospect->getCivility())],
                                        ['key' => 'email', 'value' => $prospect->getEmail()],
                                        ['key' => 'cellPhone', 'value' => $prospect->getMobile()],
                                        ['key' => 'address1', 'value' => self::asciiTranslit($prospect->getAddress())],
                                        ['key' => 'postalCode', 'value' => $prospect->getZipCode()],
                                        ['key' => 'city', 'value' => self::asciiTranslit($prospect->getCity())],
                                        ['key' => 'country', 'value' => 'FR'],
                                        ['key' => 'companyName', 'value' => self::asciiTranslit($prospect->getCompanyName())],
                                        ['key' => 'registrationNumber', 'value' => $prospect->getSiren()],
                                        ['key' => 'phone', 'value' => $prospect->getMobile()]
                                    ]
                                ],
                                'contractors' => [],
                                'contract' => [
                                    'contract_definition_id' => $this->contractDefinitionId,
                                    'pdf_file_path' => $filename,
                                    'contract_id' => -1,
                                ],
                                'contract_properties' => [
                                    [
                                        'key' => 'Iban',
                                        'value' => $prospect->getIban(),
                                        'to_fill_by_user' => empty($prospect->getIban())
                                    ]
                                ],
                                'files' => [],
                                'options' => [],
                                'to_sign' => 1
                            ]),
                            'filename'  => 'adhoc_light.sellsign',
                            'headers'   => [
                                'Content-type' => 'application/json'
                            ],
                        ],
                        [
                            'name' => $filename,
                            'contents' => fopen($sepaPath, 'rb'),
                            'filename' => $filename,
                            'headers' => [
                                'Content-type' => 'application/pdf'
                            ]
                        ]
                    ]
                ]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e, $prospect, $supplier);
        }
    }

    /**
     * @param int $contractId
     * @return array|null
     * @throws SellAndSignException
     */
    public function getContractorsAbout(int $contractId)
    {
        $uriParams = $this->getUriStringParameters([
            'contract_id' => $contractId,
            'offset' => 0,
            'size' => 1
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/model/contractorsforcontract/list?action=getContractorsAbout&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @return array|null
     * @throws SellAndSignException
     */
    public function prepareContractSingleStep(Prospect $prospect, Supplier $supplier)
    {
        $logs = $this->supplierAccountLogRepository->getSupplierAccountLogs($prospect, $supplier);

        if ($logs[0]->getContractId() !== null) {
            if ($logs[0]->getToken() !== null && $logs[0]->getTokenExpiration() !== null && $logs[0]->getTokenExpiration() > self::getCurrentMilliseconds()) {
                return $this->getSigningUrls($logs[0]->getContractId(), "P{$prospect->getId()}", $logs[0]->getToken());
            }

            $ret = ['contract_id' => $logs[0]->getContractId(), 'customer_number' => "P{$prospect->getId()}"];
        } else {
            $ret = $this->sendCommandPacket($prospect, $supplier);
        }

        if ($ret) {
            $this->supplierAccountLogRepository->updateLog($prospect, $supplier, $ret['contract_id']);

            $contractorsAbout = $this->getContractorsAbout($ret['contract_id']);

            if ($contractorsAbout) {
                $temporaryToken = $this->createTemporaryToken($ret['customer_number'], $contractorsAbout['elements'][0]['contractId'], $contractorsAbout['elements'][0]['contractorId']);

                if ($temporaryToken) {
                    if (isset($temporaryToken['token']['Succeed']) && $temporaryToken['token']['Succeed'] === true) {
                        $this->supplierAccountLogRepository->updateLog($prospect, $supplier, $contractorsAbout['elements'][0]['contractId'], $temporaryToken['token']['token'], self::getCurrentMilliseconds() + $this->tokenLifetime);

                        return $this->getSigningUrls($contractorsAbout['elements'][0]['contractId'], $ret['customer_number'], $temporaryToken['token']['token']);
                    }
                }
            }
        }

        return null;
    }

    /**
     * All in one function that creates everything needed for the contract between a prospect and a supplier
     * If success, returns an array with 2 urls that can be used to sign the contract: 'iframe' and 'page' as key of the array
     * If failure, returns false
     *
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @return array|bool
     * @throws SellAndSignException
     */
    public function prepareContract(Prospect $prospect, Supplier $supplier)
    {
        $customer = $this->createOrUpdateCustomer($prospect);
        $contractor = $this->getOrCreateContractor($customer);
        $contract = $this->createContract($customer['number'], $prospect, $supplier);
        $uploadContract = $this->uploadContract($contract['id'], $supplier);
        $addContractor = $this->addContractorTo($contract['id'], $contractor['id']);
        $setIban = $this->setIbanOnContract($contract['id'], $prospect->getIban());
        $contractReady = $this->contractReady($contract['id']);
        $temporaryToken = $this->createTemporaryToken($customer['number'], $contract['id'], $contractor['id']);

        if (isset($temporaryToken['token']['Succeed']) && $temporaryToken['token']['Succeed'] === true) {
            return $this->getSigningUrls($contract['id'], $customer['number'], $temporaryToken['token']['token']);
        }

        return false;
    }

    /**
     * Créé / met à jour un client
     *
     * @param Prospect $prospect
     * @return array|null
     * @throws SellAndSignException
     */
    public function createOrUpdateCustomer(Prospect $prospect)
    {
        $uriParams = $this->getUriStringParameters([
            'number' => "P{$prospect->getId()}",
            'civility' => $prospect->getCivility(),
            'firstname' => $prospect->getFirstName(),
            'lastname' => $prospect->getContactName(),
            'email' => $prospect->getEmail(),
            'cell_phone' => $prospect->getMobile(),
            'address_1' => $prospect->getAddress(),
            'postal_code' => $prospect->getZipCode(),
            'city' => $prospect->getCity(),
            'country' => 'FR',
            'customer_code' => $prospect->getId(),
            'company_name' => $prospect->getCompanyName(),
            'registration_number' => $prospect->getSiren(),
            'phone' => $prospect->getPhone(),
            'address_2' => '',
            'job_title' => '',
            'birthdate' => '',
            'birthplace' => ''
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/model/customer/update?action=selectOrCreateCustomer&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e, $prospect);
        }
    }

    /**
     * Retourne un client depuis sell&sign
     *
     * @param string $number
     * @return array|null
     * @throws SellAndSignException
     */
    public function getCustomer(string $number)
    {
        $uriParams = $this->getUriStringParameters(['number' => $number]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/model/customer/read?action=getCustomer&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Créé un signataire
     *
     * @param array $customer
     * @return array|null
     * @throws SellAndSignException
     */
    public function getOrCreateContractor(array $customer)
    {
        $uriParams = $this->getUriStringParameters([
            'customer_number' => $customer['number'],
            'id' => -1,
            'civility' => $customer['civility'],
            'firstname' => $customer['firstname'],
            'lastname' => $customer['lastname'],
            'email' => $customer['email'],
            'address_1' => $customer['address1'],
            'postal_code' => $customer['postalCode'],
            'city' => $customer['city'],
            'country' => $customer['country'],
            'cell_phone' => $customer['cellPhone'],
            'phone' => $customer['phone'],
            'company_name' => $customer['companyName'],
            'is_default' => true,
            'registration_number' => $customer['registrationNumber'],
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/model/contractor/create?action=getOrCreateContractor&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Créé un contrat
     *
     * @param string $customerNumber
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @return array|null
     * @throws SellAndSignException
     */
    public function createContract(string $customerNumber, Prospect $prospect, Supplier $supplier)
    {
        $filename = 'SEPA_' . str_replace(' ', '-', $supplier->getDisplayName()) . '_' . str_replace(' ', '-', $prospect->getCompanyName()) . '.pdf';

        $uriParams = $this->getUriStringParameters([
            'date' => self::getCurrentMilliseconds(),
            'vendor_email' => $this->vendorEmail,
            'customer_number' => $customerNumber,
            'contract_definition_id' => $this->contractDefinitionId,
            'closed' => 0,
            'filename' => $filename,
            'keep_on_move' => 0,
            'customer_entity_id' => -1
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/model/contract/create?action=createContract&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e, $prospect, $supplier);
        }
    }

    /**
     * Upload le contrat chez sell&sign (le pdf)
     *
     * @param int $contractId
     * @param Supplier $supplier
     * @return array|null
     * @throws SellAndSignException
     */
    public function uploadContract(int $contractId, Supplier $supplier)
    {
        $uriParams = $this->getUriStringParameters([
            'id' => $contractId
        ]);

        $sepaPath = $this->webDir . $this->uploaderHelper->asset($supplier,'sepa');

        try {
            $response = $this->client->request(
                'POST',
                "$this->host/calinda/hub/selling/do?m=uploadContract&$uriParams",
                [
                    RequestOptions::HEADERS => ['j_token' => $this->jToken],
                    'multipart' => [
                        [
                            'name' => $supplier->getSepaFileName(),
                            'contents' => fopen($sepaPath, 'r'),
                            'filename' => $supplier->getSepaFileName(),
                            RequestOptions::HEADERS => [
                                'Content-type' => 'application/pdf'
                            ]
                        ]
                    ]
                ]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e, null, $supplier);
        }
    }

    /**
     * Ajoute un signataire au contrat
     *
     * @param int $contractId
     * @param int $contractorId
     * @return array|null
     * @throws SellAndSignException
     */
    public function addContractorTo(int $contractId, int $contractorId)
    {
        $uriParams = $this->getUriStringParameters([
            'contract_id' => $contractId,
            'contractor_id' => $contractorId,
            'signature_mode' => 1,
            'signature_status' => 'NONE'
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/model/contractorsforcontract/insert?action=addContractorTo&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Change le mode de signature du contrat pour un signataire désigné (dans notre cas c'est toujours le client)
     *
     * @param int $contractorId
     * @return array|null
     * @throws SellAndSignException
     */
    public function changeSignatureMode(int $contractorId)
    {
        $uriParams = $this->getUriStringParameters([
            'cfc_id' => $contractorId,
            'rank' => 0,
            'mode' => 1
        ]);

        try {
            $response = $this->client->request(
                'POST',
                "$this->host/calinda/hub/selling/do?m=changeSignatureMode&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Ajoute l'information Iban sur un contrat existant
     *
     * @param int $contractId
     * @param string $iban
     * @return array|null
     * @throws SellAndSignException
     */
    public function setIbanOnContract(int $contractId, ?string $iban)
    {
        try {
            $response = $this->client->request(
                'POST',
                "$this->host/calinda/hub/selling/model/contractproperty/insert?action=addContractProperty",
                [
                    RequestOptions::HEADERS => ['j_token' => $this->jToken],
                    RequestOptions::JSON => [
                        'key' => 'Iban',
                        'value' => $iban,
                        'to_fill_by_user' => empty($iban),
                        'contract_id' => $contractId
                    ]
                ]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Passe le contrat en signature
     *
     * @param int $contractId
     * @return array|null
     * @throws SellAndSignException
     */
    public function contractReady(int $contractId)
    {
        $uriParams = $this->getUriStringParameters([
            'c_id' => $contractId
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/do?m=contractReady&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Créé un token d'authentification temporaire pour signer le contrat depuis une iframe ou page web avec redirection
     *
     * @param string $customerNumber
     * @param int $contractId
     * @param int $contractorId
     * @return array|null
     * @throws SellAndSignException
     */
    public function createTemporaryToken(string $customerNumber, int $contractId, int $contractorId)
    {
        try {
            $response = $this->client->request(
                'POST',
                "$this->host/calinda/hub/createTemporaryToken.action",
                [
                    RequestOptions::HEADERS => ['j_token' => $this->jToken],
                    RequestOptions::JSON => [
                        'actorId' => $this->actorId,
                        'time' => self::getCurrentMilliseconds() + $this->tokenLifetime,
                        'profile' => 'sign_contract',
                        'parameters' => [
                            'contract_definition_id' => $this->contractDefinitionId,
                            'customer_id' => $customerNumber,
                            'contract_id' => $contractId,
                            'contractor_id' => $contractorId
                        ]
                    ]
                ]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Récupération des informations du contrat
     *
     * @param int $contractId
     * @return array|null
     * @throws SellAndSignException
     */
    public function getContract(int $contractId)
    {
        $uriParams = $this->getUriStringParameters(['contract_id' => $contractId]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/model/contract/read?action=getContract&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Récupération du document signé
     *
     * @param int $contractId
     * @return string|null
     * @throws SellAndSignException
     */
    public function getSignedContract(int $contractId)
    {
        $uriParams = $this->getUriStringParameters(['contract_id' => $contractId]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/do?m=getSignedContract&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            return $response->getBody()->getContents();
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * Récupération du document PDF quelque soit le statut du contrat
     *
     * @param int $contractId
     * @return mixed
     * @throws SellAndSignException
     */
    public function getCurrentDocumentForContract(int $contractId)
    {
        $uriParams = $this->getUriStringParameters(['id' => $contractId]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/do?m=getCurrentDocumentForContract&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            return $response->getBody()->getContents();
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }


    /**
     * Retourne le dossier de preuves
     *
     * @param int $contractId
     * @return string|null
     * @throws SellAndSignException
     */
    public function getEvidences(int $contractId)
    {
        $uriParams = $this->getUriStringParameters(['contract_id' => $contractId]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/calinda/hub/selling/do?m=getEvidences&$uriParams",
                [RequestOptions::HEADERS => ['j_token' => $this->jToken]]
            );

            return $response->getBody()->getContents();
        } catch (GuzzleException $e) {
            throw new SellAndSignException($e);
        }
    }

    /**
     * @param array $parameters
     * @return string
     */
    private function getUriStringParameters(array $parameters)
    {
        $params = [];

        foreach ($parameters as $key => $value) {
            $params[] = "$key=" . urlencode(self::asciiTranslit($value));
        }

        return implode('&', $params);
    }
}