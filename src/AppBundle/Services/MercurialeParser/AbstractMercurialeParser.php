<?php

namespace AppBundle\Services\MercurialeParser;

abstract class AbstractMercurialeParser
{
    /** @var string */
    private $mercurialePath;
    /** @var resource|bool */
    protected $file;
    /** @var array */
    protected $header = [];
    /** @var array */
    protected $products = [];
    /** @var Dictionary */
    protected $dictionary;

    public function __construct(string $mercurialePath)
    {
        $this->mercurialePath = $mercurialePath;
        $this->file = fopen($this->mercurialePath, 'rb');
        $this->dictionary = new Dictionary();
    }

    public function __destruct()
    {
        if ($this->file) {
            fclose($this->file);
        }
    }

    /**
     * Load the header into memory (consumes)
     * @return mixed
     */
    abstract protected function readHeader();

    /**
     * Loads the raw mercuriale products into memory (consumes)
     * @return void
     */
    abstract protected function readProducts();

    /**
     * Override this method to exclude fields from pre processing (do not: fix spelling, reorder units, etc... with these fields)
     *
     * @return array
     */
    protected function getExcludeFieldsFromPreProcess()
    {
        return [];
    }

    /**
     * Extract all product features from a "cleaned" product
     * (ie: spelling fixed + numbers and multipliers formatted as per the dictionary)
     *
     * @param array $product
     * @return MercurialeProduct
     * @throws \Exception
     */
    abstract protected function extractProductFeatures(array $product);

    /**
     * @param string $unit
     * @return int
     * @throws \Exception
     */
    protected function getCoeffForUnit(string $unit)
    {
        switch ($unit) {
            case 'Gramme':
            case 'GR':
            case 'Millilitre':
            case 'ML':
                return 1000;
            case 'Centilitre':
            case 'CL':
                return 100;
            case 'Kilogramme':
            case 'Litre':
            case 'KG':
            case 'L':
                return 1;
            default:
                throw new \Exception("Unknown unit `{$unit}`");
        }
    }

    protected function getWeightsFromString(string $label)
    {
        $weights = [];

        foreach ($this->dictionary->getUnitMeasurementWords() as $word) {
            if (preg_match_all("/(?:([\d]+[\.]*[\d]*)[\s]+)*({$word->getText()})/", $label, $matches)) {
                foreach ($matches[1] as $i => $result) {
                    if (empty($result)) {
                        $result = 1;
                    }

                    $weights[] = [
                        'raw' => $matches[0][$i],
                        'unit' => $matches[2][$i],
                        'value' => $result,
                        'valueInKg' => $result / $this->getCoeffForUnit($matches[2][$i])
                    ];
                }
            }
        }

        usort($weights, function ($a, $b) {
            return $a['valueInKg'] > $b['valueInKg'];
        });

        return $weights;
    }

    protected function getWeightMultipliersFromString(string $label, array $weights = [])
    {
        /**
         * Trims the weights to avoid incoherences
         *
         * Eg: BEURRE 100 * 10 Grammes -> would yield "100, 10" but we just want "100"
         */
        foreach ($weights as $weight) {
            $label = str_replace($weight['raw'], '', $label);
        }

        /**
         * There are 7 supported formats for weight multipliers:
         *
         * - NUM CONDITIONING_OR_UNIT * NUM CONDITIONNING_OR_UNIT: 3 Lots * 5 Kilogrammes
         * - NUM CONDITIONING_OR_UNIT * NUM: 3 Pieces * 24
         * - NUM CONDITIONING_OR_UNIT: 3 Pieces
         * - NUM * NUM CONDITIONING_OR_UNIT: 2 * 5 Bouteilles
         * - NUM * NUM: 1 * 1
         * - NUM * NUM * NUM CONDITIONING_OR_UNIT: 2 * 3 * 18 Pieces
         * - NUM * NUM * NUM: 2 * 3 * 18
         *
         * In all cases we need to get all the relevant numbers and multiply them together to get the final multiplier
         */
        $conditioningOrUnitPattern = $this->dictionary->getConditioningOrUnitPattern(false);
        $numberPattern = '([\d]+[\.]*[\d]*)';

        $label = explode(' ', $label);
        $count = count($label);
        $multipliers = [];

        if ($count === 1) {
            if (is_numeric($label[0])) {
                return [$label[0]];
            }

            if (preg_match("/$conditioningOrUnitPattern/", $label[0])) {
                return [1];
            }
        }

        for ($i = 0; $i < $count; ++$i) {
            $fragment = $label[$i];

            if (is_numeric($fragment)) {
                if ($i > 0 && $label[$i - 1] === '*') {
                    $multipliers[] = $fragment;
                    continue ;
                }

                if ($i < $count - 1) {
                    if (preg_match("/(?:[\*]|$numberPattern|$conditioningOrUnitPattern)+/", $label[$i + 1])) {
                        $multipliers[] = $fragment;
                    }
                } else {
                    if ($i === $count - 1 && preg_match("/(?:[\*]|$conditioningOrUnitPattern)+/", $label[$i - 1])) {
                        $multipliers[] = $fragment;
                    }
                }
            }
        }

        return $multipliers;
    }

    private function sanitize(string &$value)
    {
        $value = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $value));
    }

    protected function fixSpelling(string &$value)
    {
        foreach ($this->dictionary->getWords() as $word) {
            $value = preg_replace_callback("/{$word->buildPattern()}/", function($match) use($word) {
                return str_replace($match[$word->getCode()], $word->getText(), $match[0]);
            }, $value);
        }
    }

    private function fixDecimalPoints(string &$value)
    {
        $value = preg_replace_callback('/([\d]+),([\d]+)/', function($match) {
            return "{$match[1]}.{$match[2]}";
        }, $value);
    }

    private function reorderUnitMeasurements(string &$value)
    {
        $conditioningOrUnitPattern = $this->dictionary->getConditioningOrUnitPattern(true);
        $numberPattern = '([\d]+[\.]*[\d]*)';

        foreach ($this->dictionary->getUnitMeasurementWords() as $word) {
            $value = preg_replace_callback("/([\d]+[\d\.]*)[\s]*({$word->getText()})([\d]+[\d\.]*)/", function($match) {
                return "{$match[1]}.{$match[3]}{$match[2]}";
            }, $value);
        }

        $value = preg_replace_callback_array([
            "/$numberPattern$conditioningOrUnitPattern/" => function($match) {
                return "{$match[1]} {$match[2]}";
            },
            '/x([\d]+)/' => function($match) {
                return "* {$match[1]}";
            },
            '/([\S]+)\*/' => function($match) {
                return "{$match[1]} *";
            }
        ], $value);
    }

    protected function roundApproximateWeights(string &$value)
    {
        $conditioningOrUnitPattern = $this->dictionary->getConditioningOrUnitPattern(true);
        $numberPattern = '([\d]+[\.]*[\d]*)';

        $value = preg_replace_callback("/$numberPattern+\/{$numberPattern}+[\s]*$conditioningOrUnitPattern+/", function($match) {
            if ($match[1] == 1 && $match[2] == 2) {
                return '';
            }

            $len1 = strlen(intval($match[1]));
            $len2 = strlen(intval($match[2]));

            if ($len1 < $len2) {
                $match[1] = str_pad($match[1], $len2, '0');
            } else if ($len1 > $len2) {
                $match[2] = str_pad($match[2], $len1, '0');
            }

            return (($match[1] + $match[2]) / 2) . " $match[3]";
        },$value);
    }

    protected function extractBrand(string $string)
    {
        return $this->extractFeature($this->dictionary->getBrandWords(), $string);
    }

    protected function extractOrigin(string $string)
    {
        return $this->extractFeature($this->dictionary->getOriginWords(), $string);
    }

    protected function extractConditioning(string $string)
    {
        return $this->extractFeature($this->dictionary->getConditionningWords(), $string);
    }

    protected function extractFeature(array $words, string $string)
    {
        foreach ($words as $word) {
            if (preg_match("/{$word->getText()}/", $string)) {
                return $word->getText();
            }
        }

        return '';
    }

    private function preProcess(array $product)
    {
        $exludedFields = $this->getExcludeFieldsFromPreProcess();

        foreach ($product as $key => &$value) {
            if (!in_array($key, $exludedFields)) {
                $this->sanitize($value);
                $this->fixSpelling($value);
                $this->fixDecimalPoints($value);
                $this->reorderUnitMeasurements($value);
                $this->roundApproximateWeights($value);
            }
        }

        return $product;
    }

    /**
     * @return bool
     */
    public function parse()
    {
        if ($this->file) {
            $this->readHeader();
            $this->readProducts();

            foreach ($this->products as &$product) {
                $cleanProduct = $this->preProcess($product);

                try {
                    $features = $this->extractProductFeatures($cleanProduct);
                    $product['_features'] = $features->getData();
                } catch (\Exception $e) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Outputs to dst filename, as csv
     * @param string $dst
     */
    public function output(string $dst)
    {
        if ($output = fopen($dst, 'w')) {

            $i = 0;
            foreach ($this->products as $product) {
                if (!$i++) {
                    fputcsv($output, array_merge($this->header, array_keys($product['_features'])));
                }

                $copy = $product;

                unset($copy['_features']);

                fputcsv($output, array_merge($copy, $product['_features']));
            }

            fclose($output);
        }
    }
}