<?php

namespace AppBundle\Services\MercurialeParser;

class Word
{
    /** @var string */
    private $code;
    /** @var string */
    private $text;
    /** @var array */
    private $similars = [];

    public function __construct(string $code = '', string $text = '', array $similars = [])
    {
        $this->code = $code;
        $this->text = $text;
        $this->similars = $similars;
    }

    /**
     * @return string
     */
    public function buildPattern()
    {
        return "(?:[^a-zA-Z]|^)(?<$this->code>" . implode('|', $this->similars) . ')(?:[^a-zA-Z\.]|x[\s]*[\d\.]+|[\.]+$|$)';
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return array
     */
    public function getSimilars(): array
    {
        return $this->similars;
    }

    /**
     * @param array $similars
     */
    public function setSimilars(array $similars): void
    {
        $this->similars = [];

        foreach ($similars as $similar) {
            $this->similars[] = strtolower($similar);
        }
    }

    /**
     * @param string $similar
     */
    public function addSimilar(string $similar): void
    {
        $similar = strtolower($similar);

        if (!in_array($similar, $this->similars)) {
            $this->similars[] = $similar;
        }
    }
}