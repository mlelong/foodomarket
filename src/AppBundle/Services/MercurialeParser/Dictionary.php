<?php

namespace AppBundle\Services\MercurialeParser;

class Dictionary
{
    /** @var Word[] */
    private $words = [];
    /** @var Word[] */
    private $conditionningWords = [];
    /** @var Word[] */
    private $unitMeasurementWords = [];
    /** @var Word[] */
    private $extraWords = [];
    /** @var Word[] */
    private $brandWords = [];
    /** @var Word[] */
    private $originWords = [];

    public function __construct()
    {
        $this->buildConditioningDictionary();
        $this->buildUnitMeasurementDictionary();
        $this->buildExtraDictionary();
        $this->buildBrandDictionary();
        $this->buildOriginDictionary();
    }

    private function buildConditioningDictionary()
    {
        $this->conditionningWords = [
            new Word('BARQUETTE', 'Barquette', ['barquettes', 'barquette', 'barq\.', 'barq']),
            new Word('BOITE', 'Boite', ['boites', 'boite', 'bte', 'btes']),
            new Word('PIECE', 'Piece', ['pieces', 'piece', 'pc', 'pces', 'pers', 'p']),
            new Word('PLATEAU', 'Plateau', ['plateaux', 'plateau', 'plat\.']),
            new Word('SACHET', 'Sachet', ['sachets', 'sachet']),
            new Word('SAC', 'Sac', ['sacs', 'sacs\.', 'sac']),
            new Word('CARTON', 'Carton', ['cartons', 'carton', 'cart\.', 'ct']),
            new Word('PLAQUETTE', 'Plaquette', ['plaquettes', 'plaquette', 'plaq\.', 'plaq', 'pl']),
            new Word('PLAQUE', 'Plaque', ['plaques', 'plaque']),
            new Word('MOTTE', 'Motte', ['mottes', 'motte']),
            new Word('BOUTEILLE', 'Bouteille', ['bouteilles', 'bouteille', 'bout\.']),
            new Word('AEROSOL', 'Aerosol', ['aerosols', 'aerosol']),
            new Word('POT', 'Pot', ['pots', 'pot']),
            new Word('PORTION', 'Portion', ['portions', 'portion', 'port\.']),
            new Word('ROULEAU', 'Rouleau', ['rouleaux', 'rouleau', 'roul\.', 'roul']),
            new Word('SEAU', 'Seau', ['seaux', 'seau']),
            new Word('BIDON', 'Bidon', ['bidons', 'bidon']),
            new Word('DOSETTE', 'Dosette', ['dosettes', 'dosette']),
            new Word('DOSE', 'Dose', ['doses', 'dose']),
            new Word('STICK', 'Stick', ['sticks', 'stick']),
            new Word('CAISSE', 'Caisse', ['caisses', 'caisse']),
            new Word('BAC', 'Bac', ['bacs', 'bac']),
            new Word('ASSIETTE', 'Assiette', ['assiettes', 'assiette']),
            new Word('FLACON', 'Flacon', ['flacons', 'flacon']),
            new Word('POCHE', 'Poche', ['poches', 'poche']),
            new Word('BOCAL', 'Bocal', ['bocaux', 'bocal']),
            new Word('TRANCHE', 'Tranche', ['tranches', 'tranche', 'tr\.', 'tr']),
        ];

        $this->words = array_merge($this->words, $this->conditionningWords);
    }

    private function buildUnitMeasurementDictionary()
    {
        $this->unitMeasurementWords = [
            new Word('KG', 'Kilogramme', ['kilos', 'kilo', 'kgs', 'kg', 'k']),
            new Word('L', 'Litre', ['litres', 'litre', 'l']),
            new Word('CL', 'Centilitre', ['cl']),
            new Word('ML', 'Millilitre', ['ml']),
            new Word('GR', 'Gramme', ['gr', 'g']),
        ];

        $this->words = array_merge($this->words, $this->unitMeasurementWords);
    }

    private function buildExtraDictionary()
    {
        $this->extraWords = [
            new Word('LOT', 'Lot', ['lots', 'lot'])
        ];

        $this->words = array_merge($this->words, $this->extraWords);
    }

    private function buildBrandDictionary()
    {
        $this->brandWords = [
            new Word('PRESIDENT', 'President', ['president', 'presid\.', 'presid', 'presd\.', 'presd', 'pres\.', 'pres']),
            new Word('ECHIRE', 'Echire', ['echire']),
            new Word('DEBIC', 'Debic', ['debic']),
            new Word('MEGGLE', 'Meggle', ['meggle']),
            new Word('ISIGNY', 'Isigny', ['isigny', 'isig']),
            new Word('ELLE_VIRE', 'Elle & Vire', ['elle & vire', 'e \.vire', 'e\.vire', 'ev', 'cuisinel']),
            new Word('LA_CONVIETTE', 'La Conviette', ['la conviette', 'conviette', 'viette']),
            new Word('VERNEUIL', 'Verneuil', ['verneuil']),
            new Word('BEILLEVAIRE', 'Beillevaire', ['beillevaire']),
            new Word('LIGUEIL', 'Ligueil', ['ligueil']),
            new Word('GRAND_FERMAGE', 'Grand Fermage', ['grand fermage', 'g\. fermage', 'g\.fermage']),
            new Word('BOURG_FLEURI', 'Bourg Fleuri', ['bourg fleuri', 'b\. fleuri', 'b\.fleuri', 'b fleuri']),
            new Word('FLORY', 'Flory', ['flory']),
            new Word('BRIDELICE', 'Bridelice', ['bridelice']),
            new Word('MONTEBOURG', 'Montebourg', ['montebourg', 'monteb\.', 'monteb']),
            new Word('CAMPAGNE_DE_FRANCE', 'Campagne de France', ['campagne de france', 'cdf', 'tdf']),
            new Word('CANDIA', 'Candia', ['candia']),
            new Word('HAXAIRE', 'Haxaire', ['haxaire']),
            new Word('AGOUR', 'Agour', ['agour']),
            new Word('ISTARA', 'Istara', ['istara']),
            new Word('PAPILLON', 'Papillon', ['papillon']),
            new Word('ONETIK', 'Onetik', ['onetik']),
            new Word('AMBROSI', 'Ambrosi', ['ambrosi', 'ambr\.', 'ambr']),
            new Word('GILLARD', 'Bernard Gillard', ['bernard gillard', 'gillard']),
            new Word('ROUZAIRE', 'Rouzaire', ['rouzaire']),
            new Word('RIANS', 'Rians', ['rians']),
            new Word('SOIGNON', 'Soignon', ['soignon']),
            new Word('LACTEL', 'Lactel', ['lactel']),
            new Word('GALBANI', 'Galbani', ['galbani']),
            new Word('VALCO', 'Valco', ['valco']),
            new Word('CORMAN', 'Corman', ['corman']),
            new Word('OVIPAC', 'Ovipac', ['ovipac']),
            new Word('COCOTINE', 'Cocotine', ['cocotine']),
            new Word('MAMIE_NOVA', 'Mamie Nova', ['mamie nova', 'm\.nova', 'm\. nova']),
            new Word('NOVA', 'Nova', ['nova']),
            new Word('DANONE', 'Danone', ['danone']),
            new Word('ANDROS', 'Andros', ['andros']),
            new Word('NESTLE', 'Nestle', ['nestle']),
            new Word('YOPLAIT', 'Yoplait', ['Yoplait']),
            new Word('LA_BRESSANE', 'La Bressane', ['la bressane', 'bressane', 'bressan\.', 'bressan']),
            new Word('ENTREMONT', 'Entremont', ['entremont', 'entr.']),
            new Word('CODIPAL', 'Codipal', ['codipal']),
            new Word('PATURAGES_COMTOIS', 'Paturages Comtois', ['paturages comtois']),
            new Word('FERME_DU_PRE', 'Ferme du Pre', ['ferme du pre', 'ferme pre', 'f\.pre', 'f\. du pre', 'f du pre']),
            new Word('BARILLA', 'Barilla', ['barilla']),
            new Word('LOTUS', 'Lotus', ['lotus']),
            new Word('PASQUIER', 'Pasquier', ['pasquier', 'pasq.']),
            new Word('BOUVARD', 'Bouvard', ['bouvard']),
            new Word('PTIT_GOURMAND', "P'tit Gourmand", ['p\.gourmand', 'p\.gourm', 'p\. gourmand', 'p\. gourm']),
            new Word('KER_CADELAC', "Ker Cadelac", ['ker cadelac', 'k\. cadelac', 'k\.cadelac', 'cadelac']),
            new Word('CASA_AZZURRA', "Casa Azzurra", ['casa azzurra', 'c\. azzurra', 'c\.azzurra', 'azzurra']),
        ];

        $this->words = array_merge($this->words, $this->brandWords);
    }

    private function buildOriginDictionary()
    {
        $this->originWords = [
            new Word('TRIBEHOU', 'Tribehou', ['tribehou', 'trib\.', 'trib']),
            new Word('PLEUMEUR_GAUTIER', 'Pleumeur-Gautier', ['pleumeur\-gautier', 'pleumeur gautier', 'p\. gautier', 'p\.gautier', 'p gautier', 'gautier'])
        ];

        $this->words = array_merge($this->words, $this->originWords);
    }

    /**
     * @return Word[]
     */
    public function getWords(): array
    {
        return $this->words;
    }

    /**
     * @return Word[]
     */
    public function getConditionningWords(): array
    {
        return $this->conditionningWords;
    }

    /**
     * @return Word[]
     */
    public function getUnitMeasurementWords(): array
    {
        return $this->unitMeasurementWords;
    }

    /**
     * @return Word[]
     */
    public function getExtraWords(): array
    {
        return $this->extraWords;
    }

    /**
     * @return Word[]
     */
    public function getBrandWords(): array
    {
        return $this->brandWords;
    }

    /**
     * @return Word[]
     */
    public function getOriginWords(): array
    {
        return $this->originWords;
    }

    /**
     * @param bool $capture
     * @param string $captureName
     * @return mixed
     */
    public function getConditioningOrUnitPattern(bool $capture = false, string $captureName = '')
    {
        /** @var Word[] $words */
        $words = array_merge($this->conditionningWords, $this->unitMeasurementWords, $this->extraWords);

        return $this->getTextPattern($words, $capture, $captureName);
    }

    /**
     * @param Word[] $words
     * @param bool $capture
     * @param string $captureName
     * @return mixed
     */
    public function getTextPattern(array $words, bool $capture = false, string $captureName = '')
    {
        if ($capture) {
            $pattern = !empty($captureName) ? "(?<$captureName>" : '(';
        } else {
            $pattern = '(?:';
        }

        foreach ($words as $word) {
            $pattern .= $word->getText() . '|';
        }

        return substr_replace($pattern, ')', -1);
    }
}