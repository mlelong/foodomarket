<?php

namespace AppBundle\Services\MercurialeParser;

class MercurialeProduct
{
    /** @var string */
    private $productName = '';
    /** @var string */
    private $salesUnit = '';
    /** @var string */
    private $orderUnit = '';
    /** @var float */
    private $kgPrice = 0.0;
    /** @var float */
    private $unitPrice = 0.0;
    /** @var float */
    private $weight = 0.0;
    /** @var int */
    private $uq = 0;
    /** @var float */
    private $ucq = 0.0;
    /** @var string */
    private $ucm = '';
    /** @var string */
    private $conditioning = '';
    /** @var string */
    private $brand = '';
    /** @var string */
    private $category = '';
    /** @var string */
    private $origin = '';
    /** @var string */
    private $originCountry = '';
    /** @var string */
    private $naming = '';
    /** @var string */
    private $bio = '';
    /** @var string */
    private $caliber = '';
    /** @var string */
    private $variety = '';

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     */
    public function setProductName(string $productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return string
     */
    public function getSalesUnit(): string
    {
        return $this->salesUnit;
    }

    /**
     * @param string $salesUnit
     */
    public function setSalesUnit(string $salesUnit): void
    {
        $this->salesUnit = $salesUnit;
    }

    /**
     * @return string
     */
    public function getOrderUnit(): string
    {
        return $this->orderUnit;
    }

    /**
     * @param string $orderUnit
     */
    public function setOrderUnit(string $orderUnit): void
    {
        $this->orderUnit = $orderUnit;
    }

    /**
     * @return float
     */
    public function getKgPrice(): float
    {
        return $this->kgPrice;
    }

    /**
     * @param float $kgPrice
     */
    public function setKgPrice(float $kgPrice): void
    {
        $this->kgPrice = $kgPrice;
    }

    /**
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice(float $unitPrice): void
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getUq(): int
    {
        return $this->uq;
    }

    /**
     * @param int $uq
     */
    public function setUq(int $uq): void
    {
        $this->uq = $uq;
    }

    /**
     * @return float
     */
    public function getUcq(): float
    {
        return $this->ucq;
    }

    /**
     * @param float $ucq
     */
    public function setUcq(float $ucq): void
    {
        $this->ucq = $ucq;
    }

    /**
     * @return string
     */
    public function getUcm(): string
    {
        return $this->ucm;
    }

    /**
     * @param string $ucm
     */
    public function setUcm(string $ucm): void
    {
        $this->ucm = $ucm;
    }

    /**
     * @return string
     */
    public function getConditioning(): string
    {
        return $this->conditioning;
    }

    /**
     * @param string $conditioning
     */
    public function setConditioning(string $conditioning): void
    {
        $this->conditioning = $conditioning;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getOrigin(): string
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin(string $origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getOriginCountry(): string
    {
        return $this->originCountry;
    }

    /**
     * @param string $originCountry
     */
    public function setOriginCountry(string $originCountry): void
    {
        $this->originCountry = $originCountry;
    }

    /**
     * @return string
     */
    public function getNaming(): string
    {
        return $this->naming;
    }

    /**
     * @param string $naming
     */
    public function setNaming(string $naming): void
    {
        $this->naming = $naming;
    }

    /**
     * @return string
     */
    public function getBio(): string
    {
        return $this->bio;
    }

    /**
     * @param string $bio
     */
    public function setBio(string $bio): void
    {
        $this->bio = $bio;
    }

    /**
     * @return string
     */
    public function getCaliber(): string
    {
        return $this->caliber;
    }

    /**
     * @param string $caliber
     */
    public function setCaliber(string $caliber): void
    {
        $this->caliber = $caliber;
    }

    /**
     * @return string
     */
    public function getVariety(): string
    {
        return $this->variety;
    }

    /**
     * @param string $variety
     */
    public function setVariety(string $variety): void
    {
        $this->variety = $variety;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'productName' => $this->productName,
            'salesUnit' => $this->salesUnit,
            'orderUnit' => $this->orderUnit,
            'kgPrice' => $this->kgPrice,
            'unitPrice' => $this->unitPrice,
            'weight' => $this->weight,
            'uq' => $this->uq,
            'ucq' => $this->ucq,
            'ucm' => $this->ucm,
            'conditioning' => $this->conditioning,
            'brand' => $this->brand,
            'category' => $this->category,
            'origin' => $this->origin,
            'originCountry' => $this->originCountry,
            'naming' => $this->naming,
            'bio' => $this->bio,
            'caliber' => $this->caliber,
            'variety' => $this->variety
        ];
    }
}