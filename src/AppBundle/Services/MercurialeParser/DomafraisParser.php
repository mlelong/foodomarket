<?php

namespace AppBundle\Services\MercurialeParser;

class DomafraisParser extends AbstractMercurialeParser
{
    /**
     * Load the header into memory (consumes)
     * @return void
     */
    protected function readHeader()
    {
        fgetcsv($this->file);
        fgetcsv($this->file);

        $this->header = ['code', 'label', 'unit', 'price', 'cond', 'type'];
    }

    /**
     * Loads the raw mercuriale products into memory (consumes)
     * @return void
     */
    protected function readProducts()
    {
        while ($line = fgetcsv($this->file)) {
            $price = floatval(str_replace([',', ' '], ['.', ''], $line[3]));

            if ($price == 0) {
                continue ;
            }

            $this->products[] = [
                'code' => $line[0],
                'label' => $line[1],
                'unit' => strtoupper($line[2]),
                'price' => $price,
                'cond' => $line[4],
                'type' => $line[5]
            ];
        }
    }

    protected function getExcludeFieldsFromPreProcess()
    {
        return ['unit', 'price', 'type', 'code'];
    }

    /**
     * Extract all product features from a "cleaned" product
     * (ie: spelling fixed + numbers and multipliers formatted as per the dictionary)
     *
     * @param array $product
     * @return MercurialeProduct
     * @throws \Exception
     */
    protected function extractProductFeatures(array $product)
    {
        $mercurialeProduct = new MercurialeProduct();

        $mercurialeProduct->setProductName($product['label']);
        $mercurialeProduct->setConditioning($product['cond']);

        $mercurialeProduct->setBrand($this->extractBrand($product['label']));
        $mercurialeProduct->setSalesUnit($this->extractSalesUnit($product['unit']));
        $mercurialeProduct->setOrigin($this->extractOrigin($product['label']));

        $conditioning = $this->extractConditioning($product['label']);

//        if (empty($conditioning)) {
//            $conditioning = $this->extractConditioning($product['cond']);
//        }

        $mercurialeProduct->setConditioning($conditioning);

        $labelWeights = $this->getWeightsFromString($product['label']);
        $labelWeightsMultipliers = $this->getWeightMultipliersFromString($product['label'], $labelWeights);
        $labelWeightMultiplier = array_reduce($labelWeightsMultipliers, function($carry, $value) {
            return $carry * $value;
        }, 1);

        $conditioningWeights = $this->getWeightsFromString($product['cond']);
        $conditioningWeightMultipliers = $this->getWeightMultipliersFromString($product['cond'], $conditioningWeights);
        $conditioningWeightMultiplier = array_reduce($conditioningWeightMultipliers, function($carry, $value) {
            return $carry * $value;
        }, 1);

        $mercurialeProduct->setOrderUnit(
            $this->extractOrderUnit(
                $mercurialeProduct->getSalesUnit(),
                $conditioningWeights,
                $conditioningWeightMultiplier,
                $labelWeights,
                $labelWeightMultiplier,
                $product['cond']
            )
        );

        if ($mercurialeProduct->getOrderUnit() === 'KG') {
            $mercurialeProduct->setUq(1);
            $mercurialeProduct->setUcq(1);
            $mercurialeProduct->setUcm('Kilogramme');
            $mercurialeProduct->setWeight(1);
        } else if (!empty($labelWeights)) {
            $smallestUnitWeight = $labelWeights[0]['valueInKg'];
            $totalSmallestUnitWeight = $smallestUnitWeight * $labelWeightMultiplier;

            // Prevents stuffs...
            if ($totalSmallestUnitWeight > 50) {
                $totalSmallestUnitWeight = $smallestUnitWeight;
                $labelWeightMultiplier = 1;
            }

            $fullConditioningWeight = 0;

            if (!empty($conditioningWeights)) {
                $fullConditioningWeight = $conditioningWeightMultiplier * $conditioningWeights[0]['valueInKg'];
            } else if (($count = count($labelWeights)) > 1) {
                $fullConditioningWeight = $labelWeights[$count - 1]['valueInKg'];
            }

            $commonMultipliers = [];
            $aggregatedMultiplier = 1;
            $done = [];

            foreach ($labelWeightsMultipliers as $labelWeightsMultiplier2) {
                foreach ($conditioningWeightMultipliers as $conditioningWeightMultiplier2) {
                    if ($labelWeightsMultiplier2 == $conditioningWeightMultiplier2 && !isset($done[$conditioningWeightMultiplier2])) {
                        $commonMultipliers[] = $conditioningWeightMultiplier2;
                        $aggregatedMultiplier *= $conditioningWeightMultiplier2;
                        $done[$conditioningWeightMultiplier2] = true;
                    }
                }
            }

            $aggregatedMultiplier = max(1, $aggregatedMultiplier);
            $weightMultiplier = $conditioningWeightMultiplier * $labelWeightMultiplier / $aggregatedMultiplier;

            // There is a weight in the conditionning column, it must be the real one
            if ($fullConditioningWeight != 0) {
                $totalWeight = $fullConditioningWeight;
                $unitQuantity = $totalWeight / $labelWeights[0]['valueInKg'];
            } else {
                $totalWeight = $weightMultiplier * $smallestUnitWeight;
                $unitQuantity = $weightMultiplier;

                if ($totalWeight > 50) {
                    $totalWeight = $totalSmallestUnitWeight * $labelWeightMultiplier;
                    $unitQuantity = $labelWeightMultiplier;
                }
            }

            // TODO: check for incoherences and mark warning

            $mercurialeProduct->setUq($unitQuantity);
            $mercurialeProduct->setUcq($labelWeights[0]['value']);
            $mercurialeProduct->setUcm($labelWeights[0]['unit']);
            $mercurialeProduct->setWeight($totalWeight);
        } else if (!empty($conditioningWeights)) {
            $conditioningWeight = $conditioningWeights[0]['valueInKg'];
            $fullConditioningWeight = $conditioningWeightMultiplier * $conditioningWeight;

            $mercurialeProduct->setUq($conditioningWeightMultiplier);
            $mercurialeProduct->setUcq($conditioningWeights[0]['value']);
            $mercurialeProduct->setUcm($conditioningWeights[0]['unit']);
            $mercurialeProduct->setWeight($fullConditioningWeight);
        } else {
            if ($labelWeightMultiplier === $conditioningWeightMultiplier) {
                $mercurialeProduct->setUq($labelWeightMultiplier);
                $mercurialeProduct->setUcq(1);
            } else if ($labelWeightMultiplier > 1 || $conditioningWeightMultiplier > 1) {
                $mercurialeProduct->setUq($conditioningWeightMultiplier);
                $mercurialeProduct->setUcq($labelWeightMultiplier);
            } else {
                $mercurialeProduct->setUq(1);
                $mercurialeProduct->setUcq(1);
            }

            $conditioningOrUnitPattern = $this->dictionary->getConditioningOrUnitPattern(true);

            if (preg_match("/$conditioningOrUnitPattern/", $product['cond'], $matches)) {
                $mercurialeProduct->setUcm($matches[1]);

                if (preg_match("/{$this->dictionary->getTextPattern($this->dictionary->getUnitMeasurementWords(), false)}/", $matches[1])) {
                    $coeff = $this->getCoeffForUnit($matches[1]);
                    $mercurialeProduct->setWeight($mercurialeProduct->getUq() * $mercurialeProduct->getUcq() / $coeff);
                }
            } else {
                $mercurialeProduct->setUcm('Kilogramme');
                $mercurialeProduct->setWeight($mercurialeProduct->getUq() * $mercurialeProduct->getUcq());
            }
        }

        // Calculate KG Price
        if ($mercurialeProduct->getOrderUnit() === 'KG') {
            $mercurialeProduct->setKgPrice($product['price']);
            $mercurialeProduct->setUnitPrice($product['price']);
        } else {
            switch ($mercurialeProduct->getSalesUnit()) {
                case 'KG':
                case 'L':
                    if ($mercurialeProduct->getWeight() > 0) {
                        $mercurialeProduct->setUnitPrice($product['price'] * $mercurialeProduct->getWeight());
                    } else {
                        $mercurialeProduct->setUnitPrice($product['price']);
                    }

                    $mercurialeProduct->setKgPrice($product['price']);
                    break ;
                case 'COLIS':
                    if ($mercurialeProduct->getWeight() > 0) {
                        $mercurialeProduct->setKgPrice($product['price'] / $mercurialeProduct->getWeight());
                    }

                    $mercurialeProduct->setUnitPrice($product['price']);
                    break ;
                case 'PC':
                    if (in_array($product['unit'], ['PT', 'BA', 'SA'])) {
                        $multiplier = PHP_INT_MAX;

                        foreach ($conditioningWeightMultipliers as $_multiplier) {
                            if ($_multiplier != 1) {
                                $multiplier = min($_multiplier, $multiplier);
                            }
                        }

                        if ($multiplier === PHP_INT_MAX) {
                            $multiplier = $conditioningWeightMultiplier != $labelWeightMultiplier ? $conditioningWeightMultiplier : 1;
                        }
                    } else {
                        $multiplier = $mercurialeProduct->getUq();
                    }

                    $mercurialeProduct->setUnitPrice($product['price'] * $multiplier);

                    if ($mercurialeProduct->getWeight() > 0) {
                        $mercurialeProduct->setKgPrice($mercurialeProduct->getUnitPrice() / $mercurialeProduct->getWeight());
                    }
                    break ;
                default:
                    throw new \Exception("Unknown sale unit `{$mercurialeProduct->getSalesUnit()}`");
            }
        }

        return $mercurialeProduct;
    }

    /**
     * @param string $string
     * @return string
     * @throws \Exception
     */
    private function extractSalesUnit(string $string)
    {
        switch ($string) {
            case 'KG':
                return 'KG';
            case 'PI': // Pièce, ...
            case 'PO': // Pot, ...
            case 'LO': // Lot, ...
            case 'BT': // Boîte, ...
            case 'BA': // Barquette, ...
            case 'SO': // Seau, ...
            case 'PT': // Plateau, ...
            case 'SA': // Sachet, ...
                return 'PC';
            case 'L':
                return 'L';
            case 'CO':
                return 'COLIS';
            case 'B':
                return 'PC';
            default:
                throw new \Exception("Unit \"$string\" has no association yet");
        }
    }

    private function extractOrderUnit(
        string $salesUnit,
        array $conditionningWeights,
        float $conditioningWeightMultiplier,
        array $labelWeights,
        float $labelWeightMultiplier,
        string $conditioning)
    {
        if ($salesUnit === 'COLIS' || $conditioningWeightMultiplier > 1 || $labelWeightMultiplier > 1) {
            return 'COLIS';
        }

        if (!empty($labelWeights) && !empty($conditionningWeights)) {
            if ($labelWeights[0]['valueInKg'] != $conditionningWeights[0]['valueInKg']) {
                return 'COLIS';
            }

            return 'PC';
        }

        if (!empty($conditionningWeights) || !empty($labelWeights)) {
            return 'PC';
        }

        foreach ($this->dictionary->getConditionningWords() as $word) {
            if (preg_match("/{$word->getText()}/", $conditioning)) {
                return 'PC';
            }
        }

        return $salesUnit;
    }
}