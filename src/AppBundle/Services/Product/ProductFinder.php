<?php

namespace AppBundle\Services\Product;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ProductVariantUtils;
use AppBundle\Services\RedisCacheManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;

/**
 * Class ProductFinder
 * @package AppBundle\Services
 */
class ProductFinder implements ProductFinderInterface
{
    const USE_CACHE = true;

    /** @var ProductVariantRepository */
    private $productVariantRepository;
    /** @var SupplierRepository */
    private $supplierRepository;
    /** @var RestaurantRepository */
    private $restaurantRepository;
    /** @var SupplierProductPriceRepository */
    private $supplierProductPriceRepository;
    /** @var SupplierProductVariantInformationsRepository */
    private $supplierProductVariantInformationsRepository;
    /** @var CacheManager */
    private $liipImagineCacheManager;
    /** @var RedisCacheManager */
    private $predisCacheManager;

    /**
     * Search constructor.
     *
     * @param ProductVariantRepository $productVariantRepository
     * @param SupplierRepository $supplierRepository
     * @param RestaurantRepository $restaurantRepository
     * @param SupplierProductPriceRepository $supplierProductPriceRepository
     * @param SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository
     * @param CacheManager $liipImagineCacheManager
     * @param RedisCacheManager $predisCacheManager
     */
    public function __construct(
        ProductVariantRepository $productVariantRepository,
        SupplierRepository $supplierRepository,
        RestaurantRepository $restaurantRepository,
        SupplierProductPriceRepository $supplierProductPriceRepository,
        SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository,
        CacheManager $liipImagineCacheManager,
        RedisCacheManager $predisCacheManager
    )
    {
        $this->productVariantRepository = $productVariantRepository;
        $this->supplierRepository = $supplierRepository;
        $this->restaurantRepository = $restaurantRepository;
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
        $this->supplierProductVariantInformationsRepository = $supplierProductVariantInformationsRepository;
        $this->liipImagineCacheManager = $liipImagineCacheManager;
        $this->predisCacheManager = $predisCacheManager;
    }

    private function _getRestaurantSuppliers(Restaurant $restaurant)
    {
        $restaurantSuppliers = $this->supplierRepository->getByRestaurant($restaurant);
        $restaurantFacades = $this->supplierRepository->getFacadesByRestaurant($restaurant);
        $concernedSuppliers = array_merge($restaurantSuppliers, $restaurantFacades);

        $suppliers = [0 => [
            'supplierId' => 0,
            'supplierName' => 'Tous mes fournisseurs',
            'supplierPicture' => '',
            'supplierProducts' => !empty($concernedSuppliers)
                ? $this->supplierProductPriceRepository->countPrices($concernedSuppliers, null, null, [$restaurant, 4], true)
                : 0
        ]];

        foreach ($restaurantSuppliers as $supplier) {
            $facade = $supplier->getFacadeSupplier();
            $supplierId = $facade->getId();

            $logoFileName = $facade->getLogoFileName();
            $supplierPicture = '';

            if ($logoFileName !== null && !empty($logoFileName)) {
                $supplierPicture = $this->liipImagineCacheManager->getBrowserPath("supplier/$logoFileName", 'front_tiny');
            }

            $suppliers[$supplierId] = [
                'supplierId' => $supplierId,
                'supplierName' => $facade->getName(),
                'supplierPicture' => $supplierPicture,
                'minimumOrder' => $facade->getMinimumOrder(),
                'closingDays' => $facade->getClosingDays(),
                'freePort' => $facade->getFreePort(),
                'shippingCost' => $facade->getShippingCost(),
                'orderLimitTime' => $facade->getOrderLimitTime(),
                'chronofreshDelivery' => $facade->isChronofreshDelivery(),
                'supplierProducts' => $this->supplierProductPriceRepository->countPrices([$supplier, $facade], null, null, [$restaurant, 4], true)
            ];
        }

        return array_values($suppliers);
    }

    public function getRestaurantSuppliers(Restaurant $restaurant)
    {
        $callback = function() use($restaurant) {
            return $this->_getRestaurantSuppliers($restaurant);
        };

        if (!self::USE_CACHE) {
            return $callback();
        }

        $key = str_replace(
            '{restaurantId}',
            $restaurant->getId(),
            RedisCacheManager::CACHE_KEY_SHOP_USER_SUPPLIERS
        );

        return $this->predisCacheManager->get(
            $key,
            null,
            $callback
        );
    }

    /**
     * @param ProductVariant|int $variant
     * @param Supplier|int $supplier
     * @param Restaurant|int|null $restaurant
     * @return mixed
     */
    public function getProductCacheKey($variant, $supplier, $restaurant = null)
    {
        return str_replace(
            ['{supplierId}', '{restaurantId}', '{variantId}'],
            [
                $supplier instanceof Supplier ? $supplier->getId() : $supplier,
                $restaurant !== null ? ($restaurant instanceof Restaurant ? $restaurant->getId() : $restaurant) : 4,
                $variant instanceof ProductVariant ? $variant->getId() : $variant
            ],
            RedisCacheManager::CACHE_KEY_SHOP_VARIANT
        );
    }

    /**
     * Returns the product data array, based on the parameters given
     *
     * @param ProductVariant|null $variant
     * @param Supplier|null $supplier
     * @param Restaurant|null $restaurant
     * @return mixed|null
     */
    public function getProduct(?ProductVariant $variant, ?Supplier $supplier, ?Restaurant $restaurant = null)
    {
        if ($variant === null || $supplier === null) {
            return null;
        }

        $callback = function() use($variant, $supplier, $restaurant) {
            return $this->_getProduct($variant, $supplier, $restaurant);
        };

        if (!self::USE_CACHE) {
            return $callback();
        }

        $key = $this->getProductCacheKey($variant, $supplier, $restaurant);

        return $this->predisCacheManager->get($key, null, $callback);
    }

    /**
     * Returns the product data array, based on the given key.
     * If the key is not in redis cache, then we extract the key's data necessary to fetch the data from DB
     *
     * @param string $key
     * @return mixed|null
     */
    public function getProductFromKey(string $key)
    {
        $callback = function() use($key) {
            $keyRegexp = str_replace(
                ['{variantId}', '{supplierId}', '{restaurantId}'],
                ['(?<variant>[\d]+)', '(?<supplier>[\d]+)', '(?<restaurant>[\d]+)'],
                RedisCacheManager::CACHE_KEY_SHOP_VARIANT
            );

            preg_match("/$keyRegexp/", $key, $keyParams);

            /** @var ProductVariant|null $variant */
            $variant = $this->productVariantRepository->find($keyParams['variant']);
            /** @var Supplier|null $supplier */
            $supplier = $this->supplierRepository->find($keyParams['supplier']);
            /** @var Restaurant|null $restaurant */
            $restaurant = $this->restaurantRepository->find($keyParams['restaurant']);

            return $this->_getProduct($variant, $supplier, $restaurant);
        };

        if (!self::USE_CACHE) {
            return $callback();
        }

        return $this->predisCacheManager->get($key, null, $callback);
    }

    /**
     * Returns the product data array from a SupplierProductPrice
     * This is the *real* entry point to fetch a product data
     *
     * @param SupplierProductPrice $price
     * @return array
     */
    public function _getProductFromPrice(SupplierProductPrice $price)
    {
        $supplier = $price->getSupplier();
        $product = $price->getProduct();
        $variant = $price->getProductVariant();

        /** @var SupplierProductVariantInformations $spvi */
        $spvi = $this->supplierProductVariantInformationsRepository->findOneBy([
            'product' => $product, 'productVariant' => $variant, 'supplier' => $supplier
        ]);

        // TODO: check for promo
        // TODO: check for blocked price

        $image = $product->getImages()->first();
        $salesUnit = $variant->getOptionSupplierSalesUnit();
        $orderUnit = $variant->getOptionOrderUnit();

        $unitQuantity = $variant->getOptionUnitQuantity();
        $unitContentQuantity = $variant->getOptionUnitContentQuantity();
        $unitContentMeasurement = $variant->getOptionUnitContentMeasurement();

        if ($orderUnit === false) {
            $orderUnitSelected = 'KG';
        } else {
            switch ($orderUnit->getCode()) {
                case 'ORDER_PC':
                case 'ORDER_KGPC':
                case 'KGPC':
                    $orderUnitSelected = 'PC';
                    break;
                case 'ORDER_CO':
                case 'ORDER_COLIS':
                    $orderUnitSelected = 'CO';
                    break ;
                case 'ORDER_L':
                    $orderUnitSelected = 'L';
                    break;
                case 'ORDER_KG':
                default:
                    $orderUnitSelected = 'KG';
                    break;
            }
        }

        $ancestors = array_merge([$product->getMainTaxon()], $product->getMainTaxon()->getAncestors()->toArray());
        $tree = [];

        foreach ($ancestors as $taxon) {
            $tree[] = [
                'id' => $taxon->getId(),
                'name' => $taxon->getName()
            ];
        }

        $tree = array_reverse($tree);

        return [
            'pid' => $product->getId(),
            'productId' => $variant->getId(),
            'weight' => $variant->getWeight(),
            'supplierId' => !$supplier->getEnabled() && $supplier->getFacadeSupplier() !== null ? $supplier->getFacadeSupplier()->getId() : $supplier->getId(),
            'supplierReference' => $spvi !== null ? $spvi->getReference() : '',
            'salesUnit' => $salesUnit !== false ? $salesUnit->getCode() : 'KG',
            'productPrice' => $price !== null ? $price->getKgPrice() : 0,
            'productPriceUnit' => $price !== null ? $price->getUnitPrice() : 0,
            'productName' => $variant->getFullDisplayName(),
            'productName2' => $spvi !== null ? $spvi->getProductLabel() : '',
            'productName3' => $product->getName(),
            'origin' => $variant->getDisplayOrigin(),
            'productSupplier' => !$supplier->getEnabled() && $supplier->getFacadeSupplier() !== null ? $supplier->getFacadeSupplier()->getName() : $supplier->getName(),
            'quantity' => 0,
            'quantityUnit' => $orderUnit->getCode(),
            'quantityUnitSelected' => $orderUnitSelected,
            'productPicture' => $this->liipImagineCacheManager->getBrowserPath($image ? $image->getPath() : 'shop-default-product.jpg', 'front_tiny'),
            'productPictureLarge' => $this->liipImagineCacheManager->getBrowserPath($image ? $image->getPath() : 'shop-default-product.jpg', 'front_big'),
            'taxon' => $product->getMainTaxon() !== null ? $product->getMainTaxon()->getName() : '',
            'hasPrice' => $price !== null,
            'unitQuantity' => $unitQuantity ? $unitQuantity->getValue() : 1,
            'unitContentQuantity' => $unitContentQuantity ? $unitContentQuantity->getValue() : 1,
            'unitContentMeasurement' => $unitContentMeasurement ? $unitContentMeasurement->getValue() : 'KG',
            'explicitContent' => $variant->getExplicitContent(),
            'priceDetail' => $this->getPriceDetail($price),
            'promo' => $price->isPromo(),
            'tree' => $tree,
            'manuallyCreated' => $spvi !== null ? $spvi->isManuallyCreated() : false
        ];
    }

    /**
     * Generate the price precision in the form: (soit X€/Unit)
     *
     * @param SupplierProductPrice|null $supplierProductPrice
     * @return array
     */
    private function getPriceDetail(?SupplierProductPrice $supplierProductPrice)
    {
        $emptyData = ['price' => 0, 'unit' => '', 'detail' => '', 'full' => ''];

        if ($supplierProductPrice === null || $supplierProductPrice->getProductVariant() === null) {
            return $emptyData;
        }

        return ProductVariantUtils::getPriceDetailFromSupplierProductPrice($supplierProductPrice);
    }

    private function _getProduct(ProductVariant $variant, Supplier $supplier, Restaurant $restaurant = null)
    {
        $prices = [];

        if ($restaurant === null || $restaurant->getId() === 4) {
            if ($supplier->getEnabled()) {
                return null;
            }
        } else if ($supplier->getEnabled()) {
            // Try to find manually created price (facade + manually created)
            $prices = $this->supplierProductPriceRepository->getPrices([$supplier], [$variant->getProduct()], [$variant], [$restaurant], true);
        }

        if (empty($prices)) {
            if ($supplier->getEnabled()) {
                $supplier = $this->supplierRepository->getRestaurantPartnerForFacade($restaurant, $supplier);
            }

            // Try to find a price for this partner and restaurant foodomarket
            $prices = $this->supplierProductPriceRepository->getPrices([$supplier], [$variant->getProduct()], [$variant], [4], false);
        }

        return !empty($prices) ? $this->_getProductFromPrice($prices[0]) : null;
    }

    private function _getRestaurantSupplierProducts(Restaurant $restaurant, Supplier $supplier)
    {
        $partnerSupplier = $this->supplierRepository->getRestaurantPartnerForFacade($restaurant, $supplier);
        $prices = $this->supplierProductPriceRepository->getRestaurantPricesForSuppliers($restaurant, $supplier, $partnerSupplier);
        $productKeys = [];

        foreach ($prices as $price) {
            $productKeys[] = $this->getProductCacheKey($price->getProductVariant(), $price->getSupplier(), $price->getRestaurant());
        }

        return $productKeys;
    }

    public function getRestaurantSupplierProducts(Restaurant $restaurant, Supplier $supplier)
    {
        $callback = function() use($restaurant, $supplier) {
            return $this->_getRestaurantSupplierProducts($restaurant, $supplier);
        };

        if (!self::USE_CACHE) {
            $productKeys = $callback();
        } else {
            $key = str_replace(
                ['{restaurantId}', '{supplierId}'],
                [$restaurant->getId(), $supplier->getId()],
                RedisCacheManager::CACHE_KEY_SHOP_USER_SUPPLIER_PRODUCTS
            );

            $productKeys = $this->predisCacheManager->get(
                $key,
                null,
                $callback
            );
        }

        $products = $this->getProductsFromKeys($productKeys);

        $this->sortProducts($products);

        return $products;
    }

    private function _getRestaurantProducts(Restaurant $restaurant)
    {
        $suppliers = $this->supplierRepository->getByRestaurant($restaurant);
        $facades = $this->supplierRepository->getFacadesByRestaurant($restaurant);

        if (empty($suppliers) && empty($facades)) {
            return [];
        }

        $prices = $this->supplierProductPriceRepository->getRestaurantPricesForSuppliers($restaurant, ...$suppliers, ...$facades);
        $productKeys = [];

        foreach ($prices as $price) {
            $productKeys[] = $this->getProductCacheKey($price->getProductVariant(), $price->getSupplier(), $price->getRestaurant());
        }

        return $productKeys;
    }

    public function getRestaurantProducts(Restaurant $restaurant)
    {
        if (!self::USE_CACHE) {
            $productKeys = $this->_getRestaurantProducts($restaurant);
        } else {
            $key = str_replace(
                '{restaurantId}',
                $restaurant->getId(),
                RedisCacheManager::CACHE_KEY_SHOP_USER_PRODUCTS
            );

            $productKeys = $this->predisCacheManager->get(
                $key,
                null,
                function () use ($restaurant) {
                    return $this->_getRestaurantProducts($restaurant);
                }
            );
        }

        $products = $this->getProductsFromKeys($productKeys);

        $this->sortProducts($products);

        return $products;
    }

    private function getProductsFromKeys($productKeys)
    {
        $products = [];

        foreach ($productKeys as $productKey) {
            $product = $this->getProductFromKey($productKey);

            if ($product !== null) {
                $products[] = $product;
            }
        }

        return $products;
    }

    private function sortProducts(&$products)
    {
        usort($products, function($p1, $p2) {
            if (class_exists('Collator')) {
                return (new \Collator('fr_FR'))->compare($p1['productName'], $p2['productName']);
            }

            return strcasecmp($p1['productName'], $p2['productName']);
        });
    }

    /**
     * @return CacheManager
     */
    public function getLiipImagineCacheManager(): CacheManager
    {
        return $this->liipImagineCacheManager;
    }
}