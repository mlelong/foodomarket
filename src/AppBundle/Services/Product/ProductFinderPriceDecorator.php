<?php


namespace AppBundle\Services\Product;


use ApiBundle\Traits\TokenAwareTrait;
use AppBundle\Constant\PaymentMode;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\SupplierRepository;

class ProductFinderPriceDecorator implements ProductFinderInterface
{
    use TokenAwareTrait;

    /** @var ProductFinder */
    private $decorated;

    /** @var SupplierRepository */
    private $supplierRepository;

    private $paymentMethodCache = [];

    public function __construct(ProductFinder $decorated, SupplierRepository $supplierRepository)
    {
        $this->decorated = $decorated;
        $this->supplierRepository = $supplierRepository;
    }

    public function getProduct(?ProductVariant $variant, ?Supplier $supplier, ?Restaurant $restaurant = null)
    {
        $product = $this->decorated->getProduct($variant, $supplier, $restaurant);

        if ($product === null) {
            return null;
        }

        if ($this->isPaymentByCreditCardOnline($supplier)) {
            $multiplier = $supplier->getFacadeSupplier()->getCreditCardPriceMultiplier();

            $this->applyPriceModification($product, $multiplier);
        }

        return $product;
    }

    public function getRestaurantSupplierProducts(Restaurant $restaurant, Supplier $supplier)
    {
        $products = $this->decorated->getRestaurantSupplierProducts($restaurant, $supplier);

        if ($this->isPaymentByCreditCardOnline($supplier)) {
            $multiplier = $supplier->getFacadeSupplier()->getCreditCardPriceMultiplier();

            foreach ($products as &$product) {
                if ($product !== null) {
                    $this->applyPriceModification($product, $multiplier);
                }
            }
        }

        return $products;
    }

    public function getRestaurantProducts(Restaurant $restaurant)
    {
        $products = $this->decorated->getRestaurantProducts($restaurant);
        $suppliers = [];

        foreach ($products as &$product) {
            if ($product === null) {
                continue ;
            }

            $supplierId = $product['supplierId'];

            if (!isset($suppliers[$supplierId])) {
                $suppliers[$supplierId] = $this->supplierRepository->find($supplierId);
            }

            /** @var Supplier $supplier */
            $supplier = $suppliers[$supplierId];

            if ($this->isPaymentByCreditCardOnline($supplier)) {
                $this->applyPriceModification($product, $supplier->getCreditCardPriceMultiplier());
            }
        }

        return $products;
    }

    public function __call($name, $arguments)
    {
        return $this->decorated->{$name}(...$arguments);
    }

    private function applyPriceModification(array &$product, float $multiplier)
    {
        $product['productPrice'] = (int)($product['productPrice'] * $multiplier);
        $product['productPriceUnit'] = (int)($product['productPriceUnit'] * $multiplier);

        foreach ($product['priceDetail'] as &$detail) {
            $detail = preg_replace_callback('/[\d.,]+/', function ($match) use($multiplier) {
                return number_format(floatval($match[0]) *  $multiplier, 2, '.', '');
            }, $detail);
        }
    }

    private function isPaymentByCreditCardOnline(?Supplier $supplier)
    {
        if ($supplier === null) {
            return false;
        }

        if (isset($this->paymentMethodCache[$supplier->getId()])) {
            return $this->paymentMethodCache[$supplier->getId()];
        }

        $this->paymentMethodCache[$supplier->getId()] = false;

        $shopUser = $this->getUser();

        if ($shopUser) {
            $prospect = $shopUser->getCustomer()->getProspect();

            if ($prospect->getAccountStatus($supplier) === SupplierAccountLog::STATUS_ORDERING) {
                $this->paymentMethodCache[$supplier->getId()] = true;
            }
        } else {
            $this->paymentMethodCache[$supplier->getId()] = $supplier->getFacadeSupplier()->isPaymentOnline()
                && isset($supplier->getFacadeSupplier()->getPaymentModes()[PaymentMode::CREDIT_CARD]);
        }

        return $this->paymentMethodCache[$supplier->getId()];
    }
}
