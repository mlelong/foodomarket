<?php


namespace AppBundle\Services\Product;


use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;

interface ProductFinderInterface
{
    /**
     * Returns the product data array, based on the parameters given
     *
     * @param ProductVariant|null $variant
     * @param Supplier|null $supplier
     * @param Restaurant|null $restaurant
     * @return mixed|null
     */
    public function getProduct(?ProductVariant $variant, ?Supplier $supplier, ?Restaurant $restaurant = null);

    /**
     * @param Restaurant $restaurant
     * @param Supplier $supplier
     * @return mixed
     */
    public function getRestaurantSupplierProducts(Restaurant $restaurant, Supplier $supplier);

    /**
     * @param Restaurant $restaurant
     * @return mixed
     */
    public function getRestaurantProducts(Restaurant $restaurant);
}