<?php

namespace AppBundle\Services\Catalog;

use AppBundle\Entity\Supplier;
use AppBundle\Repository\ProductTaxonRepository;
use AppBundle\Repository\TaxonRepository;
use AppBundle\Services\ProductFilterService;
use Sylius\Component\Core\Model\Taxon;

class Catalog
{
    /** @var ProductTaxonRepository */
    private $productTaxonRepository;
    /** @var TaxonRepository */
    private $taxonRepository;
    /** @var int[]|Supplier[] */
    private $suppliers;
    /** @var ProductFilterService */
    private $productFilterService;

    public function __construct(ProductTaxonRepository $productTaxonRepository, TaxonRepository $taxonRepository, ProductFilterService $productFilterService)
    {
        $this->productTaxonRepository = $productTaxonRepository;
        $this->taxonRepository = $taxonRepository;
        $this->productFilterService = $productFilterService;
    }

    public function setSuppliers(array $suppliers)
    {
        $this->suppliers = $suppliers;
    }

    public function getTaxonTree()
    {
        return $this->taxonRepository->getShopTaxons($this->suppliers, -1);
    }

    public function getTreeByTaxonSlug(string $taxonSlug)
    {
        $taxonSlug = urldecode($taxonSlug);
        /** @var Taxon $taxon */
        $taxon = $this->taxonRepository->findOneBySlug($taxonSlug, 'fr_FR');

        if ($taxon === null) {
            return null;
        }

        $taxonId = $taxon->getId();
        $taxons = [$taxonId];

        // Ajout de force de chartuterie pour la boucherie
        if ($taxonId === 34) {
            $taxons[] = 507;
        }
        // Ajout de force de l'épicerie sucrée pour l'épicerie salée
        elseif ($taxonId === 9) {
            $taxons[] = 10;
        }

        $products = $this->productTaxonRepository->getProductsForSuppliersAndCategories($this->suppliers, ...$taxons);

        $ret = [
            'taxons' => $this->taxonRepository->getTaxonTree($taxon, $this->suppliers),
            'products' => $this->productFilterService->getProductsAndFilters($products, $taxon, $this->suppliers, 'front_thumbnail')
        ];

        $ret['taxons'] = $this->taxonRepository->cleanTaxonTree($ret['taxons']);

        usort($ret['products']['products'], function($a, $b) {
            if (class_exists('Collator')) {
                return (new \Collator('fr_FR'))->compare($a['name'], $b['name']);
            }

            return strcasecmp($a['name'], $b['name']);
        });

        return $ret;
    }
}