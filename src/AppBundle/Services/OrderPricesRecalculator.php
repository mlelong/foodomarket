<?php

declare(strict_types=1);

namespace AppBundle\Services;

use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\ShoppingCart;
use Sylius\Component\Core\Calculator\ProductVariantPriceCalculatorInterface;
use Sylius\Component\Order\Model\OrderInterface as BaseOrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;
use Webmozart\Assert\Assert;

final class OrderPricesRecalculator implements OrderProcessorInterface
{
    /**
     * @var ProductVariantPriceCalculatorInterface
     */
    private $productVariantPriceCalculator;

    /**
     * @param ProductVariantPriceCalculatorInterface $productVariantPriceCalculator
     */
    public function __construct(ProductVariantPriceCalculatorInterface $productVariantPriceCalculator)
    {
        $this->productVariantPriceCalculator = $productVariantPriceCalculator;
    }

    /**
     * {@inheritdoc}
     */
    public function process(BaseOrderInterface $order): void
    {
        /** @var Order $order */
        Assert::isInstanceOf($order, Order::class);
        $channel = $order->getChannel();

        /** @var ShoppingCart $shoppingCart */
        $shoppingCart = $order->getShoppingCart();

        /** @var OrderItem $item */
        foreach ($order->getItems() as $item) {
            if ($item->isImmutable()) {
                continue;
            }

            $unitPrice = $this->productVariantPriceCalculator->calculate(
                $item->getVariant(),
                [
                    'channel' => $channel,
                    'supplier' => $shoppingCart->getSupplier(),
                    'restaurant' => $shoppingCart->getRestaurant(),
                    'unit' => $item->getShoppingCartItem()->getUnit()
                ]
            );

            $item->setUnitPrice($unitPrice);
        }
    }
}
