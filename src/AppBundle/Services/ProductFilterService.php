<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductTaxonRepository;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductTaxon;
use Sylius\Component\Core\Model\Taxon;

class ProductFilterService
{
    /** @var RedisCacheManager */
    private $redisCacheManager;

    /** @var CacheManager */
    private $liipImagineCacheManager;

    /** @var SupplierProductVariantInformationsRepository */
    private $supplierProductVariantInformationsRepository;

    /** @var SupplierProductPriceRepository */
    private $supplierProductPriceRepository;

    /** @var ProductTaxonRepository */
    private $productTaxonRepository;

    /** @var Restaurant|int */
    private $restaurant;

    public function __construct(
        RedisCacheManager $redisCacheManager,
        CacheManager $liipImagineCacheManager,
        SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository,
        SupplierProductPriceRepository $supplierProductPriceRepository,
        ProductTaxonRepository $productTaxonRepository
    ) {
        $this->redisCacheManager = $redisCacheManager;
        $this->liipImagineCacheManager = $liipImagineCacheManager;
        $this->supplierProductVariantInformationsRepository = $supplierProductVariantInformationsRepository;
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
        $this->productTaxonRepository = $productTaxonRepository;
    }

    /**
     * @param Restaurant|int $restaurant
     */
    public function setRestaurant($restaurant): void
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @param Product[] $products
     * @param Taxon|null $excludedTaxon
     * @param Supplier[] $restrictToSuppliers
     * @param string $liipFilter
     * @return array
     */
    public function getProductsAndFilters(&$products, ?Taxon $excludedTaxon = null, array $restrictToSuppliers = [], string $liipFilter = 'front_thumbnail')
    {
        $ret = [
            'products' => [],
            'filters' => []
        ];

        foreach ($products as $product) {
            if (!isset($ret['products'][$product->getId()])) {
                if (empty($restrictToSuppliers)) {
                    $cacheKey = str_replace('{productId}', $product->getId(), RedisCacheManager::CACHE_KEY_SHOP_CATALOG_PRODUCT);
                } else {
                    $suppliersParam = array_reduce($restrictToSuppliers, function($carry, $supplier) {
                        if ($supplier instanceof Supplier) {
                            $supplier = $supplier->getId();
                        }

                        return empty($carry) ? $supplier : "{$carry}_{$supplier}";
                    }, '');

                    $cacheKey = str_replace(
                        ['{productId}', '{supplierId}', '{restaurantId}'],
                        [$product->getId(), $suppliersParam, $this->restaurant],
                        RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCT
                    );
                }

                $data = $this->redisCacheManager->get(
                    $cacheKey,
                    86400,
                    function() use($product, $liipFilter, $restrictToSuppliers) {
                        return $this->productToArray($product, $liipFilter, $restrictToSuppliers);
                    }
                );

                $productArray = $data;
                $filters = $this->productToFilters($product, $excludedTaxon, $restrictToSuppliers);

                if (empty($productArray['variants'])) {
                    continue ;
                }

                $ret['products'][$product->getId()] = $productArray;
                $ret['products'][$product->getId()]['filters'] = $filters['product'];
                $ret['filters'] = array_merge_recursive($ret['filters'], $filters['global']);
            }
        }

        $ret['filters'] = $this->array_unique_recursive($ret['filters']);

        foreach ($ret['filters'] as $key => &$filter) {
            if (empty($filter['values'])) {
                unset($ret['filters'][$key]);

                continue ;
            }

            $filter['key'] = is_array($filter['key']) ? $filter['key'][0] : $filter['key'];
            $filter['name'] = is_array($filter['name']) ? $filter['name'][0] : $filter['name'];

            usort($filter['values'], function($a, $b) {
                if (class_exists('Collator')) {
                    return (new \Collator('fr_FR'))->compare($a['name'], $b['name']);
                }

                return strcasecmp($a['name'], $b['name']);
            });
        }

        $ret['products'] = array_values($ret['products']);
        $ret['filters'] = array_values($ret['filters']);

        return $ret;
    }

    private function array_unique_recursive($array)
    {
        $array = array_unique($array, SORT_REGULAR);

        foreach ($array as $key => $elem) {
            if (is_array($elem)) {
                $array[$key] = $this->array_unique_recursive($elem);
            }
        }

        return $array;
    }

    /**
     * @param SupplierProductPrice[] $prices
     * @return SupplierProductPrice|null
     */
    private function getStartingPriceForProduct2(array $prices)
    {
        /** @var SupplierProductPrice|null $minPrice */
        $minPrice = null;
        $minKgPrice = PHP_FLOAT_MAX;

        foreach ($prices as $price)
        {
            $tmp = $price->getKgPrice() * $price->getSupplier()->getFacadeSupplier()->getCreditCardPriceMultiplier();

            if ($tmp < $minKgPrice) {
                $minKgPrice = $tmp;
                $minPrice = $price;
            }
        }

        return $minPrice;
    }

    /**
     * @param Product $product
     * @param string $liipFilter
     * @param Supplier[] $restrictToSuppliers
     * @return array
     */
    private function productToArray(Product $product, string $liipFilter, array $restrictToSuppliers = [])
    {
        $picturePath = $this->liipImagineCacheManager->getBrowserPath(
            $product->getImages()->isEmpty() ? 'shop-default-product.jpg' : $product->getImages()[0]->getPath(),
            $liipFilter
        );

        $prices = $this->supplierProductPriceRepository->getRestaurantPrices($restrictToSuppliers, [$product], null, $this->restaurant, false);

        $suppliers = [];
        $isPromo = false;

        foreach ($prices as $price) {
            $suppliers[$price->getSupplier()->getId()] = $price->getSupplier();

            if ($price->isPromo()) {
                $isPromo = true;
            }
        }

        $startingPrice = $this->getStartingPriceForProduct2($prices);
        $startingPriceDisplay = $startingPriceOnline = '';
        $startingExplicitContent = $startingExplicitContentOnline = '';
        $bestSupplier = 0;

        if ($startingPrice !== null) {
            $bestSupplier = $startingPrice->getSupplier()->getFacadeSupplier()->getId();
            $variant = $startingPrice->getProductVariant();
//            $orderUnit = $variant->getOptionOrderUnit();

//            if ($orderUnit !== false) {
//                switch ($orderUnit->getCode()) {
//                    case 'ORDER_PC':
//                    case 'PC':
//                        $startingPriceDisplay = bcdiv($startingPrice->getUnitPrice(), 100, 2) . "€/{$variant->getExplicitContent()}";
//                        break ;
//                    case 'ORDER_COLIS':
//                    case 'ORDER_CO':
//                    case 'CO':
//                    case 'COLIS':
//                        $startingPriceDisplay = bcdiv($startingPrice->getUnitPrice(), 100, 2) . "€/{$variant->getExplicitContent()}";
//                        break ;
//                    case 'ORDER_L':
//                    case 'L':
//                        $startingPriceDisplay = bcdiv($startingPrice->getKgPrice(), 100, 2) . '€/L';
//                        break ;
//                    case 'ORDER_KG':
//                    case 'KG':
//                    default:
//                        $startingPriceDisplay = bcdiv($startingPrice->getKgPrice(), 100, 2) . '€/kg';
//                        break ;
//                }
//            } else {
//                $startingPriceDisplay = bcdiv($startingPrice->getKgPrice(), 100, 2) . '€/kg';
//            }

            // Le prix online (avec un coeff)
            $coeff = $startingPrice->getSupplier()->getFacadeSupplier()->getCreditCardPriceMultiplier();
            $kgPrice = (int)($startingPrice->getKgPrice() * $coeff);
            $unitPrice = (int)($startingPrice->getUnitPrice() * $coeff);

            $tmp = ProductVariantUtils::getPriceDetail(
                $variant->getOptionOrderUnit()->getValue(),
                $variant->getOptionSupplierSalesUnit()->getValue(),
                $variant->getOptionUnitQuantity() ? $variant->getOptionUnitQuantity()->getValue() : 1,
                $variant->getOptionConditionning() ? $variant->getOptionConditionning()->getValue() : '',
                $variant->getWeight(),
                $kgPrice,
                $unitPrice
            );

            $startingPriceOnline = $tmp['full'];
            $startingExplicitContentOnline = ProductVariantUtils::getExplicitContentForVariant($variant);

            // Le prix "standard" (sans coeff..)
            $tmp = ProductVariantUtils::getPriceDetail(
                $variant->getOptionOrderUnit()->getValue(),
                $variant->getOptionSupplierSalesUnit()->getValue(),
                $variant->getOptionUnitQuantity() ? $variant->getOptionUnitQuantity()->getValue() : 1,
                $variant->getOptionConditionning() ? $variant->getOptionConditionning()->getValue() : '',
                $variant->getWeight(),
                $startingPrice->getKgPrice(),
                $startingPrice->getUnitPrice()
            );

            $startingPriceDisplay = $tmp['full'];
            $startingExplicitContent = ProductVariantUtils::getExplicitContentForVariant($variant);
        }

        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'slug' => $product->getSlug(),
            'picture' => $picturePath,
            'promo' => $isPromo,
            'suppliers' => array_values(array_map(function(Supplier $supplier) {
                return [
                    'id' => $supplier->getId(),
                    'name' => $supplier->getName()
                ];
            }, $suppliers)),
            'variants' => array_values(array_map(function(SupplierProductPrice $price) {
                $variant = $price->getProductVariant();

                return [
                    'id' => $variant->getId(),
                    'name' => $variant->getFullDisplayName(),
                ];
            }, $prices)),
            'startingPrice' => $startingPriceDisplay,
            'startingExplicitContent' => $startingExplicitContent,
            'startingPriceOnline' => $startingPriceOnline,
            'startingExplicitContentOnline' => $startingExplicitContentOnline,
            'bestSupplier' => $bestSupplier
        ];
    }

    private function productToFilters(Product $product, ?Taxon $excludedTaxon, array $restrictedSuppliers = [])
    {
        $excludedTaxons = null;

        if ($excludedTaxon !== null) {
            $excludedTaxons = $excludedTaxon->getAncestors();

            $excludedTaxons->add($excludedTaxon);
        }

        $qb = $this->productTaxonRepository->createQueryBuilder('pt')
            ->where('pt.product = :product')
            ->setParameter('product', $product)
        ;

        if ($excludedTaxons !== null) {
            $qb
                ->andWhere("pt.taxon NOT IN (:taxons)")
                ->setParameter('taxons', $excludedTaxons)
            ;
        }

        /** @var ProductTaxon[] $productTaxons */
        $productTaxons = $qb
            ->getQuery()
            ->getResult()
        ;

        $filters = [
            'categories' => [
                'key' => 'categories',
                'name' => 'Catégories',
                'values' => $excludedTaxon !== null ? array_map(function(ProductTaxon $productTaxon) {
                    return [
                        'id' => $productTaxon->getTaxon()->getId(),
                        'name' => $productTaxon->getTaxon()->getName()
                    ];
                }, $productTaxons) : [[
                    'id' => $product->getMainTaxon()->getId(),
                    'name' => $product->getMainTaxon()->getName(),
                ]]
            ]
        ];

        $activeVariants = $product->getVariants()->filter(function(ProductVariant $variant) use($restrictedSuppliers) {
            if (!$variant->isEnabled()) {
                return false;
            }

            $criterias = ['productVariant' => $variant];

            if (!empty($restrictedSuppliers)) {
                $criterias['supplier'] = $restrictedSuppliers;
            }

            return $this->supplierProductPriceRepository->count($criterias) > 0;
        });

        foreach ($activeVariants as $variant) {
            /** @var ProductOptionValue[] $interestingOptions */
            $interestingOptions = $variant->getOptionValues()->filter(function (ProductOptionValue $optionValue) {
                return !in_array($optionValue->getOptionCode(), ['UC', 'UM', 'UQ', 'UCQ', 'UCM']);
            });

            foreach ($interestingOptions as $optionValue) {
                if (!isset($filters[$optionValue->getOptionCode()])) {
                    $filters[$optionValue->getOptionCode()] = [
                        'key' => $optionValue->getOptionCode(),
                        'name' => $optionValue->getOption()->getName(),
                        'values' => []
                    ];
                }

                $filters[$optionValue->getOptionCode()]['values'][] = [
                    'id' => $optionValue->getId(),
                    'name' => $optionValue->getValue()
                ];
            }
        }

        $productFilters = $filters;

        array_walk($productFilters, function(&$productFilter) {
            $productFilter = array_map(function($filter) {
                return $filter['id'];
            }, $productFilter['values']);
        });

        return ['global' => $filters, 'product' => $productFilters];
    }
}