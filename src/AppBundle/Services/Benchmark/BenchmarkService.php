<?php

namespace AppBundle\Services\Benchmark;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShoppingCartItem;
use AppBundle\Entity\ShoppingItemInterface;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Services\ProductUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BenchmarkService
{
    /** @var ContainerInterface */
    private $container;

    /** @var ProductUtils */
    private $productUtils;

    /**
     * BenchmarkService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->productUtils = $this->container->get('app.utils.product');
    }

    /**
     * @param ShoppingCartItem[]|ArrayCollection $items
     * @param $userTotal float
     * @return array
     */
    public function getBenchmark($items, $userTotal)
    {
        $benchmark = $this->benchmark($items, $userTotal);

        return $benchmark;
    }

    /**
     * @return \Doctrine\Bundle\DoctrineBundle\Registry|object
     */
    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    /**
     * @param ShoppingItemInterface[] $items
     * @param $total
     * @return array
     */
    private function benchmark(array $items, $total)
    {
        $shoppingItems = $items;

        $benchmark = [
            'best'          => PHP_INT_MIN, // GAUGE Right
            'worst'         => PHP_INT_MAX, // GAUGE Left
            'average'       => 0,           // GAUGE Middle
            'quartileDown'  => 0,           // GAUGE lower quartile
            'quartileUp'    => 0            // GAUGE upper quartile
        ];

        $best = 0;
        $worst = 0;

        /**
         * @var $shoppingItem ShoppingItemInterface
         */
        foreach ($shoppingItems as &$shoppingItem) {
            /** @noinspection PhpParamsInspection */
            $bestPrice = $this->productUtils->getCheapestSimilarVariant($shoppingItem->getProductVariant());
            /** @noinspection PhpParamsInspection */
            $worstPrice = $this->productUtils->getMostExpensiveSimilarVariant($shoppingItem->getProductVariant());

            $best += $this->getCorrectPrice($shoppingItem, $bestPrice);
            $worst += $this->getCorrectPrice($shoppingItem, $worstPrice);
        }

        $benchmark['best'] = bcdiv($best, 100, 2);
        $benchmark['worst'] = bcdiv($worst, 100, 2);

        /** @var EntityRepository $supplierProductPrice */
        $supplierProductPrice = $this->getDoctrine()->getRepository(SupplierProductPrice::class);
        $points = [];

        $distinctRestaurants = $supplierProductPrice->createQueryBuilder('s')->select('DISTINCT(s.restaurant) AS restaurant')->getQuery()->getScalarResult();
        $possibleRestaurants = [];

        foreach ($distinctRestaurants as $distinctRestaurant) {
            $distinctSuppliers = $supplierProductPrice->createQueryBuilder('s')
                ->select('DISTINCT(s.supplier) AS supplier')
                ->where('s.restaurant = :restaurant')
                ->setParameter('restaurant', $distinctRestaurant['restaurant'])
                ->getQuery()
                ->getScalarResult()
            ;
            $possibleSuppliers = [];

            foreach ($distinctSuppliers as $distinctSupplier) {
                $possibleSuppliers[] = $distinctSupplier['supplier'];
            }

            $possibleRestaurants[$distinctRestaurant['restaurant']] = $possibleSuppliers;
        }

        $trace = [];

        foreach ($possibleRestaurants as $restaurantId => $suppliers) {
            foreach ($suppliers as $supplierId) {
                $supplier = $this->getDoctrine()->getRepository(Supplier::class)->find($supplierId);
                $restaurant = $this->getDoctrine()->getRepository(Restaurant::class)->find($restaurantId);

                $key = "Restaurant {$restaurant->getName()} Fournisseur {$supplier->getName()}";
                $points[$key] = 0;
                $trace[$key] = [];

                foreach ($shoppingItems as &$shoppingItem) {
                    /** @noinspection PhpParamsInspection */
                    $bestPrice = $this->productUtils->getCheapestSimilarVariantForSupplierAndRestaurant($shoppingItem->getProductVariant(), $supplier, $restaurant);

                    if ($bestPrice === null) {
                        // No replacement found for this item
                        $trace[$key][$shoppingItem->getProductVariant()->getId()] = null;
                        $points[$key] += $shoppingItem->getPrice() * 100;
                        continue ;
                    }

                    $finalPrice = $this->getCorrectPrice($shoppingItem, $bestPrice);

                    $trace[$key][$shoppingItem->getProductVariant()->getId()] = [
                        'original' => [
                            'id' => $shoppingItem->getProductVariant()->getId(),
                            'name' => $shoppingItem->getProductVariant()->getProduct()->getName(),
                            'price' => $shoppingItem->getPrice(),
                            'unit' => strtolower($shoppingItem->getUnit())
                        ],
                        'replacement' => [
                            'id' => $bestPrice->getProductVariant()->getId(),
                            'name' => $bestPrice->getProductVariant()->getProduct()->getName(),
                            'price' => bcdiv($finalPrice, 100, 2),
                            'supplier' => $bestPrice->getSupplier(),
                            'unit' => strtolower($shoppingItem->getUnit())
                        ]
                    ];

                    $points[$key] += $finalPrice;
                }
            }
        }

        foreach ($points as $key => $point) {
            foreach ($trace[$key] as $id => $value) {
                if (null === $value) {
                    unset($points[$key]);
                    continue 2;
                }
            }
        }

        foreach ($points as &$point) {
            $point = bcdiv($point, 100, 2);
        }

        $benchmark['points'] = $points;
        $benchmark['average'] = $this->computeAverage($benchmark['points']);
        $benchmark['quartileUp'] = $this->computeUpperQuartile($benchmark['average'], $benchmark['points']);
        $benchmark['quartileDown'] = $this->computeLowerQuartile($benchmark['average'], $benchmark['points']);

        // Un seul point de data...
        if ($benchmark['best'] == $benchmark['worst']) {
            if ($total <= $benchmark['best']) {
                $benchmark['cursor'] = 100;
            } else {
                $benchmark['cursor'] = 0;
            }
        } else {
            if ($total >= $benchmark['best'] && $total < $benchmark['quartileUp']) {
                $benchmark['cursor'] = 75 + (floatval($total) - floatval($benchmark['best'])) * 25 / (floatval($benchmark['quartileUp']) - floatval($benchmark['best']));
            } else if ($total >= $benchmark['quartileUp'] && $total < $benchmark['average']) {
                $benchmark['cursor'] = 50 + (floatval($total) - floatval($benchmark['quartileUp'])) * 25 / (floatval($benchmark['average']) - floatval($benchmark['quartileUp']));
            } else if ($total >= $benchmark['average'] && $total < $benchmark['quartileDown']) {
                $benchmark['cursor'] = 25 + (floatval($total) - floatval($benchmark['average'])) * 25 / (floatval($benchmark['quartileDown']) - floatval($benchmark['average']));
            } else if ($total >= $benchmark['quartileDown'] && $total <= $benchmark['worst']) {
                $benchmark['cursor'] = 0 + (floatval($total) - floatval($benchmark['quartileDown'])) * 25 / (floatval($benchmark['worst']) - floatval($benchmark['quartileDown']));
            } else {
                $benchmark['cursor'] = ($total >= $benchmark['worst']) ? 0 : 100;
            }
        }

        $benchmark['trace'] = $trace;

        return $benchmark;
    }

    private function computeAverage($points)
    {
        if (empty($points)) {
            return 0;
        }

        return array_sum($points) / count($points);
    }

    private function computeUpperQuartile($average, $points)
    {
        $data = array_filter($points, function($point) use ($average) {
            return $point >= $average;
        });

        if (empty($data)) {
            return 0;
        }

        return array_sum($data) / count($data);
    }

    private function computeLowerQuartile($average, $points)
    {
        $data = array_filter($points, function($point) use ($average) {
            return $point <= $average;
        });

        if (empty($data)) {
            return 0;
        }

        return array_sum($data) / count($data);
    }

    private function getCorrectPrice(ShoppingItemInterface $item, SupplierProductPrice $price) {
        switch ($item->getUnit()) {
            case 'PC':
            case 'L':
                $productVariant = $item->getProductVariant();
                $variant = $price->getProductVariant();

                $intPrice = $price->getUnitPrice();

                /** @noinspection PhpUndefinedMethodInspection */
                if ($productVariant->getWeight() !== null && $productVariant->getWeight() > 0
                    && $variant->getWeight() !== null && $variant->getWeight() > 0) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $intPrice = $intPrice * $productVariant->getWeight() / $variant->getWeight();
                }

                /** @var ProductVariant $variant */
                $variant = $item->getProductVariant();
                $unitQuantity = $variant->getOptionUnitQuantity();

                if ($unitQuantity === false) {
                    $unitQuantity = 1;
                } else {
                    $unitQuantity = $unitQuantity->getValue();
                }

                return $item->getQuantity() * $intPrice / $unitQuantity;
            case 'KG':
            default:
                return $item->getQuantity() * $price->getKgPrice();
        }
    }
}