<?php

namespace AppBundle\Services\Benchmark;

use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Services\Suggestions\SuggestionItem;
use Doctrine\Common\Collections\ArrayCollection;

class Benchmark
{
    /** @var Restaurant */
    protected $restaurant;

    /** @var Supplier */
    protected $supplier;

    /** @var SuggestionItem[]|ArrayCollection */
    protected $products;

    /** @var float */
    protected $total = 0.0;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return Restaurant
     */
    public function getRestaurant(): Restaurant
    {
        return $this->restaurant;
    }

    /**
     * @param Restaurant $restaurant
     */
    public function setRestaurant(Restaurant $restaurant): void
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return SuggestionItem[]|ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param SuggestionItem[]|ArrayCollection $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

    /**
     * @param SuggestionItem $product
     */
    public function addProduct(SuggestionItem $product)
    {
        $this->products->add($product);
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total): void
    {
        $this->total = $total;
    }
}