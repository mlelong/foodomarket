<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductVariant;
use AppBundle\Services\Product\ProductFinderInterface;
use Sylius\Component\Core\Calculator\ProductVariantPriceCalculatorInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Webmozart\Assert\Assert;

class ProductVariantPriceCalculator implements ProductVariantPriceCalculatorInterface
{
    /**
     * @var ProductFinderInterface
     */
    private $productFinder;

    /**
     * ProductVariantPriceCalculator constructor.
     * @param ProductFinderInterface $productFinder
     */
    public function __construct(ProductFinderInterface $productFinder)
    {
        $this->productFinder = $productFinder;
    }

    /**
     * @param ProductVariantInterface|ProductVariant $productVariant
     * @param array $context
     *
     * @return int
     *
     */
    public function calculate(ProductVariantInterface $productVariant, array $context): int
    {
        Assert::keyExists($context, 'supplier');
        Assert::keyExists($context, 'restaurant');
        Assert::keyExists($context, 'unit');

        /** @noinspection PhpParamsInspection */
        $product = $this->productFinder->getProduct($productVariant, $context['supplier'], $context['restaurant']);

        if ($product !== null) {
            switch ($context['unit']) {
//                case 'PC':
//                    $unitQuantity = $productVariant->getOptionUnitQuantity();
//                    $unitQuantity = $unitQuantity === false ? 1 : $unitQuantity->getValue();
//
//                    if ($unitQuantity == 0) {
//                        $unitQuantity = 1;
//                    }
//
//                    return $product['productPriceUnit'] / $unitQuantity;
                case 'CO':
                case 'PC':
                    return $product['productPriceUnit'];
                case 'KG':
                case 'L':
                default:
                    return $product['productPrice'];
            }
        }

        return 0;
    }
}
