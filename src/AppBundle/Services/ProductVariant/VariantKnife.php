<?php

namespace AppBundle\Services\ProductVariant;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductPriceHistory;
use AppBundle\Repository\SupplierProductPriceHistoryRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use Doctrine\Common\Collections\ArrayCollection;

class VariantKnife
{
    /**
     * @var SupplierProductPriceRepository
     */
    private $priceRepository;

    /**
     * @var SupplierProductPriceHistoryRepository
     */
    private $priceHistoryRepository;

    public function __construct(
        SupplierProductPriceRepository $supplierProductPriceRepository,
        SupplierProductPriceHistoryRepository $supplierProductPriceHistoryRepository
    ) {
        $this->priceRepository = $supplierProductPriceRepository;
        $this->priceHistoryRepository = $supplierProductPriceHistoryRepository;
    }

    /**
     * @param ProductVariant $variant
     * @param Supplier $supplier
     * @param ProductOptionValue[]|null $optionValues
     * @return ProductVariant|null
     */
    public function getReplacementForVariant(ProductVariant $variant, Supplier $supplier, array $optionValues = null)
    {
        $prices = $this->priceRepository->getPrices([$supplier], [$variant->getProduct()], null, [4], false);

        // First, if there is still an active price for this variant and supplier, then there is no replacement but the variant itself
        foreach ($prices as $price) {
            if ($price->getProductVariant()->getId() == $variant->getId()) {
                return $variant;
            }
        }

        if (empty($optionValues)) {
            $originalOptionsToMatch = $this->getInterestingOptionsForComparison($variant);
        } else {
            $originalOptionsToMatch = $optionValues;
        }

        // We haven't found the variant, so there can be a replacement (not mandatory !)
        foreach ($prices as $price) {
            if (empty($optionValues)) {
                $optionsToCompare = $this->getInterestingOptionsForComparison($price->getProductVariant());
                $optionCodes = $this->getInterestingOptionCodes();
            } else {
                $optionsToCompare = $this->getMatchingOptionsForComparison($price->getProductVariant(), $optionValues);
                $optionCodes = array_map(function(ProductOptionValue $optionValue) {
                    return $optionValue->getOptionCode();
                }, $optionValues);
            }

            if ($this->compareOptions($originalOptionsToMatch, $optionsToCompare, $optionCodes)) {
                return $price->getProductVariant();
            }
        }

        return null;
    }

    /**
     * @param ProductVariant $variant
     * @param ProductOptionValue[] $optionValues
     * @return ProductOptionValue[]
     */
    private function getMatchingOptionsForComparison(ProductVariant $variant, array $optionValues = [])
    {
        /** @var ProductOptionValue[]|ArrayCollection $variantOptionValues */
        $variantOptionValues = $variant->getOptionValues();
        $matchingOptions = [];

        foreach ($optionValues as $key => $optionValue) {
            $optionValues[$optionValue->getOptionCode()] = $optionValue;
            unset($optionValues[$key]);
        }

        foreach ($variantOptionValues as $variantOptionValue) {
            if (isset($optionValues[$variantOptionValue->getOptionCode()])) {
                $matchingOptions[$variantOptionValue->getOptionCode()] = $variantOptionValue;
            }
        }

        return $matchingOptions;
    }

    public function getVariantHistorySynthesis(ProductVariant $variant, Supplier $supplier)
    {
        $history = $this->getVariantHistoryPrices($variant, $supplier);
        $currentPrice = $history[0];
        $lastKnownVariantId = $currentPrice->getProductVariant()->getId();
        $synthesis = [
            0 => [
                'from' => $currentPrice->getCreatedAt()->format('d/m/Y'),
                'to' => $currentPrice->getCreatedAt()->format('d/m/Y'),
                'id' => $lastKnownVariantId,
                'name' => $variant->getInternalName(),
                'history' => []
            ]
        ];
        $tmp = 0;

        foreach ($history as $price) {
            if ($price === null) {
                ++$tmp;

                $synthesis[$tmp] = [
                    'to' => '',
                    'from' => '',
                    'id' => '',
                    'name' => ''
                ];
            }

            $date = $price->getCreatedAt()->format('d/m/Y');

            if ($lastKnownVariantId == $price->getProductVariant()->getId()) {
                $synthesis[$tmp]['from'] = $date;
                $synthesis[$tmp]['history'][] = $price;
            } else {
                /** @var ProductVariant $variant */
                $variant = $price->getProductVariant();
                $lastKnownVariantId = $variant->getId();
                ++$tmp;

                $synthesis[$tmp] = [
                    'to' => $price->getCreatedAt()->format('d/m/Y'),
                    'from' => $price->getCreatedAt()->format('d/m/Y'),
                    'id' => $lastKnownVariantId,
                    'name' => $variant->getInternalName(),
                    'history' => [$price]
                ];
            }
        }

        return $synthesis;
    }

    /**
     * @param ProductVariant $variant
     * @param Supplier $supplier
     * @return SupplierProductPriceHistory[]
     */
    public function getVariantHistoryPrices(ProductVariant $variant, Supplier $supplier)
    {
        /** @var SupplierProductPrice $lastPrice */
        $lastPrice = $this->priceRepository->findOneBy(['product' => $variant->getProduct(), 'productVariant' => $variant, 'supplier' => $supplier, 'restaurant' => 4]);
        /** @var SupplierProductPriceHistory[] $productHistory */
        $productHistory = $this->priceHistoryRepository->findBy(['product' => $variant->getProduct(), 'supplier' => $supplier, 'restaurant' => 4], ['createdAt' => 'DESC']);
        $history = [];
        $historyByDate = [];

        if ($lastPrice !== null) {
            $productHistory[] = $lastPrice;
        }

        usort($productHistory, function($a, $b) {
            return $a->getCreatedAt() <= $b->getCreatedAt();
        });

        $lastPrice = $productHistory[0];

        foreach ($productHistory as $price)
        {
            $date = $price->getCreatedAt()->format('d/m/Y');
            $historyByDate[$date][] = $price;
        }

        foreach ($historyByDate as $prices) {
            $found = false;
            $candidate = null;

            /** @var SupplierProductPriceHistory $price */
            foreach ($prices as $price) {
                if ($price->getProductVariant()->getId() == $lastPrice->getProductVariant()->getId()) {
                    $history[] = $price;
                    $lastPrice = $price;
                    $found = true;
                    break ;
                } elseif (!$candidate && $this->compareOptions(
                    $price->getProductVariant()->getOptionValues()->toArray(),
                    $variant->getOptionValues()->toArray())) {
                    $candidate = $price;
                }
            }

            if (!$found) {
                if ($candidate) {
                    $lastPrice = $candidate;
                    $history[] = $lastPrice;
                } else {
//                    $history[] = null;
                }
            }
        }

        return $history;
    }

    /**
     * @param ProductOptionValue[] $aOptions
     * @param ProductOptionValue[] $bOptions
     * @param array $optionCodes
     * @return bool
     */
    private function compareOptions(array $aOptions, array $bOptions, array $optionCodes = [])
    {
        if (empty($optionCodes)) {
            $optionCodes = $this->getInterestingOptionCodes();
        }

        $similar = true;

        foreach ($aOptions as $option) {
            $aOptions[$option->getOptionCode()] = $option;
        }

        foreach ($bOptions as $option) {
            $bOptions[$option->getOptionCode()] = $option;
        }

        foreach ($optionCodes as $optionCode) {
            if (isset($aOptions[$optionCode]) && isset($bOptions[$optionCode])) {
                $similar = $similar && $aOptions[$optionCode]->getCode() == $bOptions[$optionCode]->getCode();
            } elseif (isset($aOptions[$optionCode])) {
                switch ($optionCode) {
                    case 'BI':
                        $similar = $similar && $aOptions[$optionCode]->getCode() === 'NOBIO';
                        break ;
                    default:
                        $similar = false;
                        break ;
                }
            } elseif (isset($bOptions[$optionCode])) {
                switch ($optionCode) {
                    case 'BI':
                        $similar = $similar && $bOptions[$optionCode]->getCode() === 'NOBIO';
                        break ;
                    default:
                        $similar = false;
                        break ;
                }
            }
        }

        return $similar;
    }

    private function getInterestingOptionsForComparison(ProductVariant $variant)
    {
        $interestingOptionCodes = $this->getInterestingOptionCodes();
        $optionValues = [];

        /** @var ProductOptionValue $optionValue */
        foreach ($variant->getOptionValues() as $optionValue) {
            if (in_array($optionValue->getOptionCode(), $interestingOptionCodes)) {
                $optionValues[$optionValue->getOptionCode()] = $optionValue;
            }
        }

        return $optionValues;
    }

    private function getInterestingOptionCodes()
    {
        static $interestingOptionCodes = ['UM', 'UC', 'UQ', 'UCQ', 'UCM', 'BI', 'MQ'];

        return $interestingOptionCodes;
    }
}