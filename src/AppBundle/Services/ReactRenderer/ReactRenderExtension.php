<?php


namespace AppBundle\Services\ReactRenderer;


class ReactRenderExtension extends \Limenius\ReactRenderer\Twig\ReactRenderExtension
{
    /**
     * @param string $storeName
     * @param array  $props
     *
     * @return string
     */
    public function reactReduxStore($storeName, $props)
    {
        $propsString = is_array($props) ? json_encode($props, JSON_INVALID_UTF8_IGNORE) : $props;
        $this->registeredStores[$storeName] = $propsString;


        $reduxStoreTag = sprintf(
            '<script type="application/json" data-js-react-on-rails-store="%s">%s</script>',
            $storeName,
            $propsString
        );

        return $this->renderContext().$reduxStoreTag;
    }
}
