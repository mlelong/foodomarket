<?php


namespace AppBundle\Services\ReactRenderer;


use Limenius\ReactRenderer\Context\ContextProviderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class SymfonyContextProvider implements ContextProviderInterface
{
    /** @var RequestStack */
    private $requestStack;

    /** @var RouterInterface */
    private $router;

    /** @var string */
    private $graphqlUri;

    public function __construct(RequestStack $requestStack, RouterInterface $router, ?string $graphqlUri = null)
    {
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->graphqlUri = $graphqlUri;
    }

    /**
     * getContext
     *
     * @param boolean $serverSide whether is this a server side context
     * @return array the context information
     */
    public function getContext($serverSide)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        $context = [
            'serverSide' => $serverSide,
            'href' => $request->getSchemeAndHttpHost().$request->getRequestUri(),
            'location' => $request->getRequestUri(),
            'scheme' => $request->getScheme(),
            'host' => $request->getHost(),
            'port' => $request->getPort(),
            'base' => $request->getBaseUrl(),
            'pathname' => $request->getPathInfo(),
            'search' => $request->getQueryString(),
            'graphqlUri' => $this->graphqlUri ? $this->graphqlUri : $this->router->generate('api_graphql_entrypoint', [], RouterInterface::ABSOLUTE_URL)
        ];

        if ($serverSide) {
            $cookie = "";

            foreach ($request->cookies->all() as $key => $value) {
                $cookie .= "$key=$value; ";
            }

            $cookie = substr($cookie, 0, -2);

            $context['cookie'] = $cookie;
        }

        return $context;
    }
}
