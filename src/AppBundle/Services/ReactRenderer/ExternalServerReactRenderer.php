<?php


namespace AppBundle\Services\ReactRenderer;


class ExternalServerReactRenderer extends \Limenius\ReactRenderer\Renderer\ExternalServerReactRenderer
{
    public function render($componentName, $propsString, $uuid, $registeredStores = array(), $trace)
    {
        $windows = defined('PHP_WINDOWS_VERSION_MAJOR');
        $inputContent = $this->wrap($componentName, $propsString, $uuid, $registeredStores, $trace)."\0";

        if ($windows) {
            if (strpos($this->serverSocketPath, "\\pipe\\xsock\\") === false) {
                $this->serverSocketPath = "\\\\.\\pipe\\xsock\\" . str_replace('/', '\\', $this->serverSocketPath);
            }

            if (!$sock = fopen($this->serverSocketPath, 'w+')) {
                throw new \RuntimeException("Unable to open windows pipe");
            }

            fwrite($sock, $inputContent);
        } else {
            if (strpos($this->serverSocketPath, '://') === false) {
                $this->serverSocketPath = 'unix://' . $this->serverSocketPath;
            }

            if (!$sock = stream_socket_client($this->serverSocketPath, $errno, $errstr)) {
                throw new \RuntimeException($errstr);
            }

            stream_socket_sendto($sock, $inputContent);
        }

        $contents = '';

        while (!feof($sock)) {
            $contents .= fread($sock, 8192);
        }
        fclose($sock);

        $result = json_decode($contents, true);
        if ($result['hasErrors']) {
            $this->logErrors($result['consoleReplayScript']);
            if ($this->failLoud) {
                $this->throwError($result['consoleReplayScript'], $componentName);
            }
        }

        return [
            'evaluated' => $result['html'],
            'consoleReplay' => $result['consoleReplayScript'],
            'hasErrors' => $result['hasErrors']
        ];
    }

    /**
     * @param string $name
     * @param string $propsString
     * @param string $uuid
     * @param array  $registeredStores
     * @param bool   $trace
     *
     * @return string
     */
    protected function wrap($name, $propsString, $uuid, $registeredStores = array(), $trace)
    {
        $traceStr = $trace ? 'true' : 'false';
        $context = json_encode($this->contextProvider->getContext(true));
        $initializedReduxStores = $this->initializeReduxStores($registeredStores, $context);
        $wrapperJs = <<<JS
(function() {
  $initializedReduxStores
  var props = $propsString;
  return ReactOnRailsPatch.serverRenderReactComponent({
    name: '$name',
    domNodeId: '$uuid',
    props: props,
    trace: $traceStr,
    railsContext: $context,
  }, handleResponse);
})();
JS;

        return $wrapperJs;
    }
}
