<?php

namespace AppBundle\Services;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\ProspectSource;
use AppBundle\Repository\ProspectSourceRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class ProspectSourceService
{
    /** @var ProspectSourceRepository  */
    private $prospectSourceRepository;

    /** @var Session */
    private $session;

    /**
     * ProspectSourceService constructor.
     * @param ProspectSourceRepository $prospectSourceRepository
     * @param Session $session
     */
    public function __construct(
        ProspectSourceRepository $prospectSourceRepository, Session $session
    ){
        $this->prospectSourceRepository = $prospectSourceRepository;
        $this->session = $session;
    }

    /**
     * @param Prospect $prospect
     * @param string $action
     * @param string|null $sourceOverride
     */
    public function createProspectSource($prospect, $action, ?string $sourceOverride = null){

        // PROBLEME ICI

        // Problème avec la source : Pas de source qui va dans la BDD  :( :(

        // Changement de la valeur de la source selon la valeur de la session

        if ($sourceOverride !== null) {
            $source = $sourceOverride;
        } else {
            if (!empty($this->session->get('utm_source'))) {
                $source = $this->session->get('utm_source');
            } elseif (!empty($this->session->get('gclid'))) {
                $source = "adwords";
            } elseif (!empty($this->session->get('fbclid'))) {
                $source = "facebook";
            } else {
                $source = null;
            }
        }

        // Création de l'objet et initialisation des valeurs
        $prospectSource = new ProspectSource();
        $prospectSource->setProspect($prospect);
        // Vérifie si la source est vide
        if(!empty($source)){
            $prospectSource->setSource($source);
        }
        $prospectSource->setAction($action);

        // Persist et flush
        $this->prospectSourceRepository->add($prospectSource);
    }
}