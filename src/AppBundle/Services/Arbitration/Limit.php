<?php

namespace AppBundle\Services\Arbitration;

interface Limit
{
    const SURPLUS = 1;
    const ERROR = -1;
    const NONE = 0;
}