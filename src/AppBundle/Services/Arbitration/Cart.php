<?php

namespace AppBundle\Services\Arbitration;

class Cart
{
    /** @var int */
    private $nbProduct;

    /** @var int */
    private $nbSupplier;

    /** @var float[][] */
    private $prices = [];

    /** @var float[][] */
    private $thresholds = [];

    /** @var int[] */
    private $request = [];

    /** @var int[] */
    private $interval = [];

    /** @var int[][] */
    private $steps = [];

    /** @var int[][] */
    private $cart = [];

    /** @var float[] */
    private $totalSupplier = [];

    /** @var int[] */
    private $surplus = [];

    /** @var int[] */
    private $state = [];

    /**
     * Cart constructor.
     * @param float[][] $prices
     * @param int $nbSupplier
     * @param int $nbProduct
     * @param float[][] $thresholds
     * @param int[] $request
     * @param int[] $interval
     * @param int[][] $steps
     * @param int[][] $cart
     * @param float[] $totalSupplier
     * @param int[] $surplus
     * @param int[] $state
     */
    public function __construct(
        array $prices, int $nbSupplier, int $nbProduct, array $thresholds, array $request,
        array $interval, array $steps, array $cart, array $totalSupplier, array $surplus, array $state)
    {
        $this->prices = $prices;
        $this->nbSupplier = $nbSupplier;
        $this->nbProduct = $nbProduct;
        $this->thresholds = $thresholds;
        $this->request = $request;
        $this->interval = $interval;
        $this->steps = $steps;
        $this->cart = $cart;
        $this->totalSupplier = $totalSupplier;
        $this->surplus = $surplus;
        $this->state = $state;
    }

    /**
     * Copie le panier et renvoie cette copie avec le nouvel état enregistré
     *
     * @param int $state
     * @return Cart
     */
    function copyCart(int $state)
    {
        return new Cart(
            $this->prices, $this->nbSupplier, $this->nbProduct, $this->thresholds, $this->request,
            $this->interval, $this->steps, $this->cart, $this->totalSupplier, $this->surplus, $this->computeState($state)
        );
    }

    /**
     * Calcule le maximum de produit qu'on peut enlever dans le fournisseur sans dépasser le minimum de commande
     *
     * @param int $supplier
     * @param int $product
     * @return int
     */
    private function maxProductToRemove(int $supplier, int $product)
    {
        switch ($this->state[$supplier]) {
            case 0: // On doit rester au dessus du minimum de commande
                $res = (int) floor(($this->totalSupplier[$supplier] - $this->thresholds[$supplier][0]) / $this->prices[$supplier][$product]);
                $resMod = $res % $this->steps[$supplier][$product];
                $res -= $resMod;

                return $res;
            case 1: // On doit au moins avoir 1 élément dans le panier
                if ($this->totalSupplier[$supplier] == $this->prices[$supplier][$product] * $this->cart[$supplier][$product]) {
//                    return $this->cart[$supplier][$product] - 1;
                    return $this->cart[$supplier][$product] - $this->steps[$supplier][$product];
                } else {
                    return $this->cart[$supplier][$product];
                }
            default: // On doit tout vider
                return $this->cart[$supplier][$product];
        }
    }

    /**
     * Calcule le maximum de produit qu'on peut rajouter dans le fournisseur sans dépasser le minimum de commande
     *
     * @param int $supplier
     * @param int $product
     * @return int
     */
    private function maxProductToAdd(int $supplier, int $product)
    {
        switch ($this->state[$supplier]) {
            case 0: // Aucune limite supérieure
                return PHP_INT_MAX;
            case 1: // On doit rester en dessous du minimum de commande
                $res = (int) floor(($this->thresholds[$supplier][0] - 1 - $this->totalSupplier[$supplier]) / $this->prices[$supplier][$product]);
                $resMod = $res % $this->steps[$supplier][$product];
                $res -= $resMod;

                return $res;
            default: // On ne peut rien ajouter
                return 0;
        }
    }

    public function add(int $supplier, int $product, int $quantity)
    {
        $this->cart[$supplier][$product] += $quantity;
        $this->totalSupplier[$supplier] += ($quantity * $this->prices[$supplier][$product]);
        $this->surplus[$product] += $quantity;
    }

    public function remove(int $supplier, int $product, int $quantity)
    {
        $this->cart[$supplier][$product] -= $quantity;
        $this->totalSupplier[$supplier] -= ($quantity * $this->prices[$supplier][$product]);
        $this->surplus[$product] -= $quantity;
    }

    function empty(int $supplier, int $product)
    {
        $quantity = min(
            $this->surplus[$product],
            min($this->cart[$supplier][$product], $this->maxProductToRemove($supplier, $product))
        );

        if ($quantity > 0) {
            $this->remove($supplier, $product, $quantity);
        }
    }

    function transfer(int $supplier1, int $supplier2, int $product)
    {
        $quantity = min(
            $this->maxProductToRemove($supplier1, $product),
            min($this->cart[$supplier1][$product], $this->maxProductToAdd($supplier2, $product))
        );

        if ($quantity > 0) {
            $this->remove($supplier1, $product, $quantity);
            $this->add($supplier2, $product, $quantity);
        }
    }

    /**
     * Savoir si le panier est valide
     *
     * @return int
     */
    function checkValidity()
    {
        $res = Limit::NONE;

        for ($product = 0; $product < $this->nbProduct; ++$product) {
            if ($this->surplus[$product] < 0 || $this->surplus[$product] > $this->interval[$product]) {
                $res = Limit::SURPLUS;
                break ;
            }
        }

        for ($supplier = 0; $supplier < $this->nbSupplier; ++$supplier) {
            switch ($this->state[$supplier]) {
                case 0: // Cas impossible
                    if ($this->totalSupplier[$supplier] < $this->thresholds[$supplier][0]) {
                        $res = Limit::ERROR;
                    }
                    break ;
                case 1:
                    if ($this->totalSupplier[$supplier] >= $this->thresholds[$supplier][0]) {
                        $res = Limit::ERROR;
                    } else if ($this->totalSupplier[$supplier] == 0) {
                        $res = Limit::ERROR;
                    }
                    break ;
                default:
                    if ($this->totalSupplier[$supplier] > 0) {
                        $res = Limit::ERROR;
                    }
                    break ;
            }
        }

        return $res;
    }

    /**
     * @param int $state
     * @return int[]|array
     */
    function computeState(int $state)
    {
        $states = array_fill(0, $this->nbSupplier, 0);
        $supplier = 0;

        while ($state > 0) {
            $states[$supplier] = $state % 3;
            $state = (int)($state / 3);
            ++$supplier;
        }

        return $states;
    }

    function computePrice()
    {
        $price = 0;

        for ($i = 0; $i < $this->nbSupplier; ++$i) {
            $price += $this->totalSupplier[$i];

            if ($this->totalSupplier[$i] > 0 && $this->totalSupplier[$i] < $this->thresholds[$i][0]) {
                $price += $this->thresholds[$i][1];
            }
        }

        return $price;
    }

    function computeIntitialState()
    {
        for ($i = 0; $i < $this->nbSupplier; ++$i) {
            if ($this->totalSupplier[$i] == 0) {
                $this->state[$i] = 2;
            } elseif ($this->totalSupplier[$i] <= $this->thresholds[$i][0]) {
                $this->state[$i] = 1;
            } else {
                $this->state[$i] = 0;
            }
        }
    }

    /**
     * @return array|int[][]
     */
    function getCart()
    {
        return $this->cart;
    }
}