<?php

namespace AppBundle\Services\Arbitration;

class Optimizer
{
    /** @var int */
    private $nbProduct;

    /** @var int */
    private $nbSupplier;

    /** @var float[][] */
    private $prices = [];

    /** @var float[][] */
    private $thresholds = [];

    /** @var int[] */
    private $request = [];

    /** @var int[] */
    private $interval = [];

    /** @var int[][] */
    private $steps;

    /** @var Cart[]  */
    private $solutions = [];

    /** @var Cart */
    private $optimum;

    /** @var float */
    private $optimumPrice;

    /**
     * [nbFrn][nbPdt]
     *
     * Liste des produits ordonnés selon le prix par fournisseur
     *
     * @var int[][]
     */
    private $productsOrderedBySupplierPrice = [];

    /**
     * [nbPdt][nbFrn]
     *
     * Liste des fournisseurs ordonnés selon le prix par produit
     *
     * @var int[][]
     */
    private $suppliersOrderedByProductPrice = [];

    /**
     * [nbFrn][nbPdt*(nbFrn-1)][2]
     *
     * Liste des différences ordonnées pour chaque fournisseur,
     * avec comme première coordonnée le fournisseur et ensuite le produit associé à l'écart
     *
     * @var int[][][]
     */
    private $differencesOrderedBySupplier = [];

    /**
     * Optimizer constructor.
     * @param float[][] $prices
     * @param float[][] $thresholds
     * @param int[] $request
     * @param int[] $interval
     * @param int[][] $steps
     * @param int[][] $suppliersOrderedByProductPrice
     * @param int[][] $productsOrderedBySupplierPrice
     * @param int[][][] $differencesOrderedBySupplier
     */
    private function __construct(
        array $prices, array $thresholds, array $request, array $interval, array $steps,
        array $suppliersOrderedByProductPrice, array $productsOrderedBySupplierPrice, array $differencesOrderedBySupplier)
    {
        $this->nbSupplier = count($prices);
        $this->nbProduct = count($prices[0]);
        $this->prices = $prices;
        $this->thresholds = $thresholds;
        $this->request = $request;
        $this->interval = $interval;
        $this->steps = $steps;
        $this->suppliersOrderedByProductPrice = $suppliersOrderedByProductPrice;
        $this->productsOrderedBySupplierPrice = $productsOrderedBySupplierPrice;
        $this->differencesOrderedBySupplier = $differencesOrderedBySupplier;
        $this->optimumPrice = PHP_FLOAT_MAX;
        $this->solutions = array_fill(0, self::recPow(3, $this->nbSupplier), null);
    }

    private static function recPow(int $a, int $b)
    {
        if ($b == 0) {
            return 1;
        }

        $r = self::recPow($a, (int)($b / 2));

        if ($b % 2 == 0) {
            return $r * $r;
        }

        return $r * $r * $a;
    }

    /**
     * @param float[][] $prices
     * @param float[][] $thresholds
     * @param int[] $request
     * @param int[] $interval
     * @param int[][] $steps
     * @return Cart
     * @throws \Exception
     */
    public static function computeOptimum(array $prices, array $thresholds, array $request, array $interval, array $steps)
    {
        $orderTable = new OrderTable($prices);
        $optimizer = new Optimizer(
            $prices, $thresholds, $request, $interval, $steps,
            $orderTable->getSuppliersOrderedByProductPrice(),
            $orderTable->getProductsOrderedBySupplierPrice(),
            $orderTable->getDifferencesOrderedBySupplier()
        );

        $optimizer->initCart();

        $nbSupplier = count($prices);
        $recPow = self::recPow(3, $nbSupplier);

        for ($i = 0; $i < $recPow; ++$i) {
            $optimizer->iterate($i);
        }

        return $optimizer->optimum;
    }

    /**
     * @param int $maxSuppliers
     * @param float[][] $prices
     * @param float[][] $thresholds
     * @param int[] $request
     * @param int[] $interval
     * @param int[][] $steps
     * @return array|null
     * @throws \Exception
     */
    public static function computeOptimumMaxSupplier(
        int $maxSuppliers, array $prices, array $thresholds, array $request, array $interval, array $steps)
    {
        $startTime = microtime(true);

        // On regarde s'il y a des produits non fournis => colonne de prix infinis
        if (!self::checkPrices($prices)) {
            return null;
        }

        $nbSupplier = count($prices);
//        $extractor = [];
//
//        for ($i = 0; $i < $maxSuppliers; ++$i) {
//            $extractor[$i] = $i;
//        }

        /** @var Cart|null $minCart */
        $minCart = null;
        $minPrice = PHP_FLOAT_MAX;
        $minExtractor = null;

        for ($supplier = 1; $supplier <= $maxSuppliers; ++$supplier) {
            $extractor = [];

            for ($i = 0; $i < $supplier; ++$i) {
                $extractor[$i] = $i;
            }

            // On calcule les optimums pour toutes les combinaisons possibles utilisant au plus maxFrn fournisseurs
            do {
                $r = self::extractSupplier($extractor, $prices, $thresholds);
                $newPrices = $r[0];
                $newThresholds = $r[1];

                // Inutile de calculer des paniers irréalisables
                if (self::checkPrices($newPrices)) {
                    $tmpCart = self::computeOptimum($newPrices, $newThresholds, $request, $interval, $steps);
                    $tmpPrice = $tmpCart->computePrice();

                    if ($tmpPrice < $minPrice) {
                        $minPrice = $tmpPrice;
                        $minCart = $tmpCart;
                        $minExtractor = $extractor;
                    }
                }
            } // On calcule l'extracteur suivant
            while (self::nextExtraction($extractor, $nbSupplier));
        }
        // Pas de solution
        if ($minCart === null) {
            return null;
        }

        $res = [$minPrice];
        $cart = $minCart->getCart();
        $i = 0;

        // On rajoute les fournisseurs non concernés pour rester cohérents avec le tableau de départ
        for ($j = 0; $j < $nbSupplier; ++$j) {
            if ($i < $maxSuppliers && $j == $minExtractor[$i]) {
                $res[1][$j] = $cart[$i];
                ++$i;
            }
        }

        $endTime = microtime(true);

        $res[2] = number_format(($endTime - $startTime), 2, '.', ' ');

        return $res;
    }

    private static function extractSupplier(array $extractor, array $prices, array $thresholds)
    {
        $nbSupplier = count($extractor);
        $newPrices = [];
        $newThresholds = [];

        for ($i = 0; $i < $nbSupplier; ++$i) {
            $newPrices[$i] = $prices[$extractor[$i]];
            $newThresholds[$i] = $thresholds[$extractor[$i]];
        }

        return [$newPrices, $newThresholds];
    }

    /**
     * @param int[] $extraction
     * @param int $nbSupplier
     * @return bool
     */
    private static function nextExtraction(array &$extraction, int $nbSupplier)
    {
        $maxSuppliers = count($extraction);
        $i = $maxSuppliers - 1;
        $maxIndex = $nbSupplier - 1;

        while ($i >= 0 && $extraction[$i] >= $maxIndex) {
            --$i;
            --$maxIndex;
        }

        if ($i >= 0) {
            $a = $extraction[$i] + 1;

            for ($j = 0; $j < $maxSuppliers - $i; ++$j) {
                $extraction[$i + $j] = $a + $j;
            }
        }

        return $i >= 0;
    }

    /**
     * @param float[][] $prices
     * @return bool
     */
    private static function checkPrices(array $prices)
    {
        $b = true;
        $nbSupplier = count($prices);
        $nbProduct = count($prices[0]);
        $j = 0;

        while ($b && $j < $nbProduct) {
            $i = 0;

            while ($i < $nbSupplier && $prices[$i][$j] == PHP_FLOAT_MAX) {
                ++$i;
            }

            $b = $i < $nbSupplier;
            ++$j;
        }

        return $b;
    }

    private function initCart()
    {
        $negativeRequest = [];
        $totalSupplier = [];

        for ($i = 0; $i < $this->nbProduct; ++$i) {
            $negativeRequest[$i] = -$this->request[$i];
        }

        for ($i = 0; $i < $this->nbSupplier; ++$i) {
            $totalSupplier[$i] = 0;
        }

        $cartArray = array_fill(0, $this->nbSupplier, array_fill(0, $this->nbProduct, 0));
        $stateArray = array_fill(0, $this->nbSupplier, 0);

        $cart = new Cart(
            $this->prices, count($this->prices), count($this->prices[0]), $this->thresholds, $this->request,
            $this->interval, $this->steps, $cartArray, $totalSupplier, $negativeRequest, $stateArray
        );

        // L'objectif est de remplir tous les fournisseurs tout en remplissant toute la demande (ie que des surplus positifs)
        // On remplit d'abord la demande chez les fournisseurs les moins chers
        for ($product = 0; $product < $this->nbProduct; ++$product) {
            $supplier = $this->productsOrderedBySupplierPrice[$product][0];

            $cart->add($supplier, $product, $this->request[$product]);
        }

        $this->optimum = $cart->copyCart(0);
        $this->optimum->computeIntitialState();
        $this->optimumPrice = $this->optimum->computePrice();

        // On complète les fournisseurs n'ayant pas atteint leur maximum
        for ($supplier = 0; $supplier < $this->nbSupplier; ++$supplier) {
            $product = $this->suppliersOrderedByProductPrice[$supplier][0];
            $quantity = (int) (1 + $this->thresholds[$supplier][0] / $this->prices[$supplier][$product]);
            $quantityMod = $quantity % $this->steps[$supplier][$product];

            if ($quantityMod > 0) {
                $quantity = ($quantity - $quantityMod) + $this->steps[$supplier][$product];
            }

            if ($quantity > 0) {
                // On s'assure d'ajouter vrt qqc
                $quantity = max($quantity, $this->steps[$supplier][$product]);
            }

            $cart->add($supplier, $product, $quantity);
        }

        // On vide les trop pleins
        for ($supplier = 0; $supplier < $this->nbSupplier; ++$supplier) {
            for ($product = 0; $product < $this->nbProduct; ++$product) {
                $cart->empty($supplier, $this->suppliersOrderedByProductPrice[$supplier][$product]);
            }
        }

        $this->checkOptimum($cart);
        $this->solutions[0] = $cart;
    }

    private function checkOptimum(Cart $cart)
    {
        $price = $cart->computePrice();
        $res = $cart->checkValidity();

        if ($res == Limit::NONE && $price < $this->optimumPrice) {
            $this->optimum = $cart;
            $this->optimumPrice = $price;
        }
    }

    private function iterate(int $state)
    {
        ++$state;

        if ($state != count($this->solutions)) {
            // On détermine l'état dont l'itération découle
            $supplier = 0;
            $tmp = $state;
            $tmp2 = 1;

            while ($tmp % 3 == 0) {
                $tmp = (int)($tmp / 3);
                $tmp2 *= 3;
                ++$supplier;
            }

            $newCart = $this->solutions[$state - $tmp2]->copyCart($state);

            // Première étape : on vide dans l'ordre décroissant de prix le panier chez le fournisseur jusqu'à descendre en dessous du seuil
            for ($i = $this->nbProduct - 1; $i >= 0; --$i) {
                $newCart->empty($supplier, $this->suppliersOrderedByProductPrice[$supplier][$i]);
            }

            // Deuxième étape : on transfère les produits jusqu'à descendre en dessous du seuil dans l'ordre croissant des différences
            for ($i = 0; $i < $this->nbProduct * ($this->nbSupplier - 1); ++$i) {
                $supplierProduct = $this->differencesOrderedBySupplier[$supplier][$i];

                if ($this->prices[$supplierProduct[0]][$supplierProduct[1]] == PHP_FLOAT_MAX) {
                    break ;
                }

                $newCart->transfer($supplier, $supplierProduct[0], $supplierProduct[1]);
            }

            $this->checkOptimum($newCart);
            $this->solutions[$state] = $newCart;
        }
    }
}