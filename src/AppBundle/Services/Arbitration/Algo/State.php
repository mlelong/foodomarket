<?php

namespace AppBundle\Services\Arbitration\Algo;

class State
{
    /** @var ShoppingCart[] */
    private $shoppingCarts = [];

    public function __construct(?State $state = null)
    {
        if ($state !== null) {
            foreach ($state->getShoppingCarts() as $shoppingCart) {
                $this->addShoppingCart($shoppingCart->clone());
            }
        }
    }

    /**
     * @return ShoppingCart[]
     */
    public function getShoppingCarts(): array
    {
        return $this->shoppingCarts;
    }

    /**
     * @param ShoppingCart[] $shoppingCarts
     */
    public function setShoppingCarts(array $shoppingCarts): void
    {
        foreach ($shoppingCarts as $shoppingCart) {
            $this->addShoppingCart($shoppingCart);
        }
    }

    public function addShoppingCart(ShoppingCart $shoppingCart): void
    {
        $this->shoppingCarts[$shoppingCart->getSupplier()] = $shoppingCart;
    }

    public function removeShoppingCart(ShoppingCart $shoppingCart): void
    {
        unset($this->shoppingCarts[$shoppingCart->getSupplier()]);
    }

    /**
     * @return State
     */
    public function clone()
    {
        $state = new State();
        $shoppingCarts = [];

        foreach ($this->shoppingCarts as $supplierId => $shoppingCart) {
            $shoppingCarts[$supplierId] = $shoppingCart->clone();
        }

        $state->setShoppingCarts($shoppingCarts);

        return $state;
    }

    /**
     * @return int
     */
    public function countValid()
    {
        $count = 0;

        foreach ($this->shoppingCarts as $shoppingCart) {
            if ($shoppingCart->isValid()) {
                ++$count;
            }
        }

        return $count;
    }

    /**
     * @return int
     */
    public function countCarts()
    {
        return count($this->shoppingCarts);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        foreach ($this->shoppingCarts as $shoppingCart) {
            if (!$shoppingCart->isValid()) {
                return false;
            }
        }

        return true;
    }

    public function isEmpty()
    {
        return empty($this->shoppingCarts);
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        $total = 0.0;

        foreach ($this->shoppingCarts as $shoppingCart) {
            $total += $shoppingCart->getTotal();
        }

        return $total;
    }

    public function getMargin()
    {
        $margin = 0.0;

        foreach ($this->shoppingCarts as $shoppingCart) {
            $margin += $shoppingCart->getDiffToMinOrder();
        }

        return $margin;
    }

    public function dump()
    {
        echo <<<HTML
*** STATE DUMP ***
*** Carts:

HTML;

        $isValid = "YES";

        foreach ($this->shoppingCarts as $shoppingCart) {
            $shoppingCart->dump();

            if (!$shoppingCart->isValid()) {
                $isValid = "NO";
            }
        }

        echo <<<HTML
*** Margin   : {$this->getMargin()}€
*** Total    : {$this->getTotal()}€
*** Is Valid : {$isValid}
*** END STATE DUMP ***


HTML;
    }
}