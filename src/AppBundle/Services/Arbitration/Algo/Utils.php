<?php

namespace AppBundle\Services\Arbitration\Algo;

class Utils
{
    const BEST_PRICE = 1;
    const WORST_PRICE = -1;

    const TRANSFER_CHEAPEAST = 1;
    const TRANSFER_FILL = 2;
    const TRANSFER_FILL_CLOSEST = 4;

    public static function transfer(ShoppingCart $a, ShoppingCart $b, int $productId, float $quantity)
    {
        $aProducts = $a->getProducts();
        $bProducts = $b->getProducts();

        if (isset($aProducts[$productId]) && isset($bProducts[$productId])) {
            $aQty = $aProducts[$productId]->getQuantity();
            $bQty = $bProducts[$productId]->getQuantity();

            $quantityToTransfer = min($aQty, $quantity);

            $aProducts[$productId]->setQuantity($aQty - $quantityToTransfer);
            $bProducts[$productId]->setQuantity($bQty + $quantityToTransfer);
        }
    }

    public static function transferCart(ShoppingCart $src, ShoppingCart $dst)
    {
        $aProducts = $src->getProducts();
        $bProducts = $dst->getProducts();

        foreach ($aProducts as $productId => $aProduct) {
            $bProduct = $bProducts[$productId];

            $bProduct->setQuantity($bProduct->getQuantity() + $aProduct->getQuantity());
            $aProduct->setQuantity(0.0);
        }
    }

    public static function pickState(State $a, State $b)
    {
        $aValid = $a->isValid();
        $bValid = $b->isValid();

        if ($aValid && $bValid) {
            return $a->getTotal() <= $b->getTotal() ? $a : $b;
        } elseif ($aValid) {
            return $a;
        } elseif ($bValid) {
            return $b;
        }

        return null;
    }

    public static function pickBestState(State $a, State $b)
    {
        $aValid = $a->isValid();
        $bValid = $b->isValid();

        if ($aValid && $bValid) {
            return $a->getTotal() <= $b->getTotal() ? $a : $b;
        } elseif ($aValid) {
            return $a;
        } elseif ($bValid) {
            return $b;
        }

        $countValidCartsReducer = function($carry, ShoppingCart $shoppingCart) {
            return $shoppingCart->isValid() ? $carry + 1 : $carry;
        };

        $aValidCarts = array_reduce($a->getShoppingCarts(), $countValidCartsReducer, 0);
        $bValidCarts = array_reduce($b->getShoppingCarts(), $countValidCartsReducer, 0);

        if ($aValidCarts > $bValidCarts) {
            return $a;
        } elseif ($aValidCarts < $bValidCarts) {
            return $b;
        }

//        return $a->getMargin() >= $b->getMargin() ? $a : $b;
        return $a->getTotal() <= $b->getTotal() ? $a : $b;
    }

    public static function getPricesDiff(ShoppingCart $a, ShoppingCart $b)
    {
        $diff = [];

        $aProducts = $a->getProducts();
        $bProducts = $b->getProducts();

        foreach ($aProducts as $productId => $aProduct) {
            $bProduct = $bProducts[$productId];

            $quantity = $aProduct->getQuantity() + $bProduct->getQuantity();
            $diff[$productId] = abs($quantity * ($aProduct->getPrice() - $bProduct->getPrice()));
        }

        uasort($diff, function($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });

        return $diff;
    }

    /**
     * Generates 2 states:
     * - with the valid carts
     * - with the invalid carts
     *
     * @param State $state
     * @return State[]
     */
    public static function splitState(State $state)
    {
        $margins = [];

        $carts = $state->getShoppingCarts();

        foreach ($carts as $supplierId => $shoppingCart) {
            $margins[$supplierId] = $shoppingCart->getDiffToMinOrder();
        }

        uasort($margins, function($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });

        $positiveState = new State();
        $negativeState = new State();

        foreach ($margins as $supplierId => $margin) {
            if ($margin < 0) {
                $negativeState->addShoppingCart($carts[$supplierId]);
            } else {
                $positiveState->addShoppingCart($carts[$supplierId]);
            }
        }

        return [
            'positive' => $positiveState,
            'negative' => $negativeState
        ];
    }

    public static function canTransferCart(ShoppingCart $src, ShoppingCart $dst)
    {
        $srcProducts = $src->getProducts();
        $dstProducts = $dst->getProducts();

        foreach ($srcProducts as $productId => $product) {
            if ($product->getQuantity() > 0) {
                if ($dstProducts[$productId]->getPrice() === PHP_FLOAT_MAX) {
                    return false;
                }
            }
        }

        return true;
    }

    public static function canTransferState(State $src, State $dst)
    {
        $srcShoppingCarts = $src->getShoppingCarts();
        $dstShoppingCarts = $dst->getShoppingCarts();

        foreach ($srcShoppingCarts as $srcShoppingCart) {
            $products = $srcShoppingCart->getProducts();

            foreach ($products as $productId => $product) {
                if ($product->getQuantity() > 0) {
                    $canTransfer = false;

                    foreach ($dstShoppingCarts as $dstShoppingCart) {
                        $dstProduct = $dstShoppingCart->getProducts()[$productId];

                        if ($dstProduct->getPrice() !== PHP_FLOAT_MAX) {
                            $canTransfer = true;
                            break ;
                        }
                    }

                    if (!$canTransfer) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param ShoppingCart $src
     * @param ShoppingCart $dst
     * @param int $productId
     * @return bool
     */
    public static function canTransferProduct(ShoppingCart $src, ShoppingCart $dst, int $productId)
    {
        $srcProduct = $src->getProducts()[$productId];
        $dstProduct = $dst->getProducts()[$productId];

        return $srcProduct->getQuantity() > 0 && $dstProduct->getPrice() !== PHP_FLOAT_MAX;
    }

    /**
     * Transfers every product of src state to dst state
     * If direction is up, then try to transfer product to the cheapest supplier among dst
     * Otherwise, try to transfer product to a more expensive supplier among dst
     *
     * @param State $src
     * @param State $dst
     * @param int $direction
     */
    public static function transferState(State $src, State $dst, int $direction = self::BEST_PRICE)
    {
        $srcCarts = $src->getShoppingCarts();
        $dstCarts = $dst->getShoppingCarts();

        foreach ($srcCarts as $srcSupplierId => $srcCart) {
            $srcProducts = $srcCart->getProducts();

            foreach ($srcProducts as $productId => $srcProduct) {
                if ($srcProduct->getQuantity() > 0) {
                    $dstSupplierId = null;

                    // Find a cheaper one among dst suppliers
                    if ($direction === self::BEST_PRICE) {
                        $price = PHP_FLOAT_MAX;

                        foreach ($dstCarts as $supplierId => $dstCart) {
                            $dstPrice = $dstCart->getProducts()[$productId]->getPrice();

                            if ($dstPrice !== PHP_FLOAT_MAX && $dstPrice <= $price) {
                                $dstSupplierId = $supplierId;
                                $price = $dstCart->getProducts()[$productId]->getPrice();
                            }
                        }
                    }
                    // Find a more expensive one among dst suppliers
                    elseif ($direction === self::WORST_PRICE) {
                        $price = PHP_FLOAT_MIN;

                        foreach ($dstCarts as $supplierId => $dstCart) {
                            $dstPrice = $dstCart->getProducts()[$productId]->getPrice();

                            if ($dstPrice !== PHP_FLOAT_MAX && $dstPrice >= $price) {
                                $dstSupplierId = $supplierId;
                                $price = $dstCart->getProducts()[$productId]->getPrice();
                            }
                        }
                    }

                    // There SHOULD be a valid dstSupplierId if canTransferState = true
                    // It is the caller responsablity to take care that this is a good op
                    self::transfer($srcCart, $dstCarts[$dstSupplierId], $productId, $srcCart->getProducts()[$productId]->getQuantity());
                }
            }
        }
    }

    public static function mergeStates(bool $byReference, State ...$states)
    {
        $merged = new State();

        foreach ($states as $state) {
            foreach ($state->getShoppingCarts() as $shoppingCart) {
                $merged->addShoppingCart($byReference ? $shoppingCart : $shoppingCart->clone());
            }
        }

        return $merged;
    }

    /**
     * Returns the carts that can handle the given product
     *
     * @param State $state
     * @param int $productId
     * @return ShoppingCart[]
     */
    public static function getCandidatesForProduct(State $state, int $productId)
    {
        $candidates = [];

        foreach ($state->getShoppingCarts() as $supplierId => $shoppingCart) {
            $product = $shoppingCart->getProducts()[$productId];

            if ($product->getPrice() !== PHP_FLOAT_MAX) {
                $candidates[$supplierId] = $shoppingCart;
            }
        }

        return $candidates;
    }

    /**
     * Removes products from "base" that have a quantity from "exclude" state
     *
     * @param State $base
     * @param State $exclude
     * @return array
     */
    public static function excludeProductsFromState(State $base, State $exclude)
    {
        $state = $base->clone();
        $excludedProducts = [];

        foreach ($exclude->getShoppingCarts() as $supplierId => $shoppingCart) {
            foreach ($shoppingCart->getProducts() as $productId => $product) {
                if ($product->getQuantity() > 0.0) {
                    foreach ($state->getShoppingCarts() as $id => $cart) {
                        $excludedProducts[$id][] = $cart->getProducts()[$productId];
                        $cart->removeProduct($product);
                    }
                }
            }
        }

        return [
            'state' => $state,
            'excluded' => $excludedProducts
        ];
    }

    /**
     * @param ShoppingCart $srcCart
     * @param Product $product
     * @param float $quantity
     * @param int $transferStrategy
     * @param State ...$states
     * @return State
     */
    public static function transferProduct(ShoppingCart $srcCart, Product $product, float $quantity, $transferStrategy = self::TRANSFER_CHEAPEAST, ...$states)
    {
        $state = Utils::mergeStates(true, ...$states);
        $dstCart = null;
        $cheapestPrice = PHP_FLOAT_MAX;
        $lowestMargin = PHP_FLOAT_MAX;
        $highestMargin = PHP_FLOAT_MIN;

        foreach ($state->getShoppingCarts() as $cart) {
            if ($srcCart->getSupplier() === $cart->getSupplier()) {
                continue ;
            }

            $_product = $cart->getProducts()[$product->getId()];

            if ($_product->getPrice() !== PHP_FLOAT_MAX) {
                switch ($transferStrategy) {
                    case self::TRANSFER_CHEAPEAST:
                        if ($cheapestPrice > $_product->getPrice()) {
                            $cheapestPrice = $_product->getPrice();
                            $dstCart = $cart;
                        }
                        break ;
                    case self::TRANSFER_FILL:
                        $margin = $cart->getDiffToMinOrder();

                        if ($margin < $lowestMargin) {
                            $lowestMargin = $margin;
                            $dstCart = $cart;
                        }
                        break ;
                    case self::TRANSFER_FILL | self::TRANSFER_CHEAPEAST:
                        $margin = $cart->getDiffToMinOrder();

                        if ($margin < 0) {
                            if ($cheapestPrice > $_product->getPrice()) {
                                $cheapestPrice = $_product->getPrice();
                                $dstCart = $cart;
                            }
                        }
                        break ;
                    case self::TRANSFER_FILL_CLOSEST:
                        $margin = $cart->getDiffToMinOrder();

                        if ($margin < 0 && $margin > $highestMargin) {
                            $highestMargin = $margin;
                            $dstCart = $cart;
                        }

                        break ;
                }
            }
        }

        if ($dstCart !== null) {
            Utils::transfer($srcCart, $dstCart, $product->getId(), $quantity);
        }

        return $state;
    }
}