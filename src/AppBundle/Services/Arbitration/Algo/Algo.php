<?php

namespace AppBundle\Services\Arbitration\Algo;

use AppBundle\Services\Arbitration\Arbitrator;

class Algo
{
    /**
     * This is the initial state (filled by user on shop, before launching optim)
     * @var State
     */
    private $initialState;

    /**
     * @var float[]
     */
    private $quantities = [];

    /** @var float[][] */
    private $prices;

    /** @var string */
    private $algo;

    /** @var int */
    private $maxSuppliers;

    /** @var float[][] */
    private $thresholds = [];

    /** @var float[][] */
    private $steps;

    public function __construct(string $algo)
    {
        $this->algo = $algo;
    }

    public function init(int $maxSuppliers, array $_prices, array $thresholds, array $request, array $interval, array $steps)
    {
        $this->maxSuppliers = $maxSuppliers;

        // Init quantities
        foreach ($request as $productId => $quantity) {
            $this->quantities[$productId] = $quantity / Arbitrator::QUANTITY_COEFF;
        }

        $this->prices = $_prices;
        $this->thresholds = $thresholds;
        $this->steps = $steps;

        foreach ($thresholds as $supplierId => $threshold) {
            foreach ($threshold as $key => $value) {
                $this->thresholds[$supplierId][$key] = $value / Arbitrator::QUANTITY_COEFF;
            }
        }

        $shoppingCarts = [];

        foreach ($_prices as $supplier => $prices) {
            $shoppingCart = new ShoppingCart($supplier, $this->thresholds[$supplier][2], $this->thresholds[$supplier][0], $this->thresholds[$supplier][1]);

            foreach ($prices as $productId => $price) {
                $shoppingCart->addProduct(
                    new Product($productId, $request[$productId] / Arbitrator::QUANTITY_COEFF, $price, $steps[$supplier][$productId] / 100)
                );

                $request[$productId] = 0;
            }

            $shoppingCarts[$supplier] = $shoppingCart;
        }

        $this->initialState = new State();

        $this->initialState->setShoppingCarts($shoppingCarts);

        $this->initBest($this->initialState);
    }

    public function dump(?State $state)
    {
        $this->log(__METHOD__, '### DUMP ###');

        if ($state === null) {
            $this->log(__METHOD__, 'NULL');
            return ;
        }

        $state->dump();
    }

    /**
     * This method WILL NOT MUTATE the shopping carts given as parameter
     * Returns an optimal state if possible, or null
     *
     * @param ShoppingCart $a
     * @param ShoppingCart $b
     * @return State|null
     */
    private function optim2(ShoppingCart $a, ShoppingCart $b)
    {
        $tmpRefState1 = new State();
        $tmpRefState2 = new State();

        $tmpRefState1->setShoppingCarts([
            $a->getSupplier() => $a->clone(),
            $b->getSupplier() => $b->clone()
        ]);

        $tmpRefState2->setShoppingCarts([
            $a->getSupplier() => $a->clone(),
            $b->getSupplier() => $b->clone()
        ]);

        // On met tout chez B
        Utils::transferCart($tmpRefState1->getShoppingCarts()[$a->getSupplier()], $tmpRefState1->getShoppingCarts()[$b->getSupplier()]);
        // On met tout chez A
        Utils::transferCart($tmpRefState2->getShoppingCarts()[$b->getSupplier()], $tmpRefState2->getShoppingCarts()[$a->getSupplier()]);

        // On prend le meilleur panier des 2
        $refState = Utils::pickState($tmpRefState1, $tmpRefState2);

        // Il n'y a pas de meilleur panier (A n'a pas des produits de B et/ou vice versa)
        if ($refState === null) {
            $this->log(__METHOD__, 'No solution can be found by putting every product to one of the suppliers');
        } else {
            $this->log(__METHOD__, "Solution found by putting every product to one of the suppliers (total: {$refState->getTotal()}€)");
        }

        // On initialise l'état initial avec les meilleurs prix partout
        $state = new State();
        $state->setShoppingCarts([$a->getSupplier() => $a->clone(), $b->getSupplier() => $b->clone()]);

        $this->log(__METHOD__, 'Initializing state with the best prices available');
        $this->initBest($state);

        $this->log(__METHOD__, 'TMP STATE:');
//        $this->dump($state);

        if ($state->isValid()) {
            $this->log(__METHOD__, 'Best prices give a valid state. Done, returning solution');
            return $state;
        }

        $shoppingCarts = $state->getShoppingCarts();

        if ($shoppingCarts[0]->isValid()) {
            $this->log(__METHOD__, 'A is valid');
            $first = $shoppingCarts[0];
            $second = $shoppingCarts[1];
        } elseif ($shoppingCarts[1]->isValid()) {
            $this->log(__METHOD__, 'B is valid');
            $first = $shoppingCarts[1];
            $second = $shoppingCarts[0];
        } else {
            $this->log(__METHOD__, 'Neither A nor B are valid');

            return null;
        }

        if ($first !== null && $second !== null) {
            $this->log(__METHOD__, 'Computing diff, in ascending order');
            $diff = Utils::getPricesDiff($first, $second);

            $this->log(__METHOD__, 'Transfer products');

            $secondIsValid = false;

            // En partant des différences de prix les plus petites vers les plus grandes
            // et tant que le 2ème fournisseur n'a pas atteint son min de commande...
            foreach ($diff as $productId => $priceDiff) {
                if ($secondIsValid) {
                    break ;
                }

                $firstQuantity = $first->getProducts()[$productId]->getQuantity();

                for ($i = 1; $i <= $firstQuantity && !$secondIsValid; ++$i) {
                    // On transfère une quantité du premier vers le second panier
                    Utils::transfer($first, $second, $productId, 1);

                    // On a rempli le fournisseur qui était en dessous du minimum de commande et maintenant on vérifie il est Ok
                    // et si oui on arrête de déshabiller le premier pour habiller le second
                    if ($second->isValid()) {
                        $this->log(__METHOD__, 'state found after transferring products');
                        $secondIsValid = true;

                        break ;
                    }
                }
            }

            // Est-ce que le premier fournisseur qui était ok l'est toujours ?
            $firstIsStillValid = $first->isValid();

            // Si les 2 paniers sont valides, alors on retourne le meilleur état entre celui qu'on vient de calculer
            // et l'état de référence (le moins cher entre tout chez A et tout chez B) et si cette référence est valide uniquement
            if ($firstIsStillValid && $secondIsValid) {
                $this->log(__METHOD__, 'First state is still valid after transferring products');
                return $refState !== null ? Utils::pickState($state, $refState) : $state;
            }

            // On retourne l'état de référence (peut être null si c'est un état impossible)
            return $refState;
        }

        // Impossible de trouver un panier possible
        return null;
    }

    /**
     * This method puts the cheapest products of each supplier to their respective cart
     * This leads to only having the best prices everywhere
     *
     * #########################
     * /!\ MUTATES THE STATE /!\
     * #########################
     *
     * @param State $state
     */
    private function initBest(State $state)
    {
        foreach ($state->getShoppingCarts() as $a) {
            foreach ($state->getShoppingCarts() as $b) {
                if ($a->getSupplier() === $b->getSupplier()) {
                    continue ;
                }

                $aProducts = $a->getProducts();
                $bProducts = $b->getProducts();

                foreach ($aProducts as $productId => $aProduct) {
                    $bProduct = $bProducts[$productId];

                    if ($aProduct->getPrice() > $bProduct->getPrice()) {
                        Utils::transfer($a, $b, $productId, $aProduct->getQuantity());
                    } else {
                        Utils::transfer($b, $a, $productId, $bProduct->getQuantity());
                    }
                }
            }
        }
    }

    /**
     * @param ShoppingCart[] $positiveCarts
     * @param ShoppingCart[] $negativeCarts
     * @return array
     */
    private function getOrderedDiffs(array $positiveCarts, array $negativeCarts)
    {
        $diffs = [];

        // On récupère les différences de prix
        foreach ($positiveCarts as $positiveSupplierId => $positiveCart) {
            foreach ($negativeCarts as $negativeSupplierId => $negativeCart) {
                $diffs["{$positiveSupplierId}-{$negativeSupplierId}"] = Utils::getPricesDiff($positiveCart, $negativeCart);
            }
        }

        // On trie les différences de prix inter-fournisseur
        // En sortie on aura par exemple: Diff A-C: X€, puis Diff B-C: X€, etc ...
        // $orderedDiffs fera nombre de fournisseur fois la taille du tableau de produits
        $orderedDiffs = [];
        foreach ($diffs as $key => $diff) {
            foreach ($diff as $productId => $price) {
                $orderedDiffs["{$key}-$productId"] = $price;
            }
        }

        uasort($orderedDiffs, function($a, $b) {
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });

        return $orderedDiffs;
    }

    private function optim3(bool $bis, ShoppingCart $a, ShoppingCart $b, ShoppingCart $c)
    {
        // On initialise l'état initial avec les meilleurs prix partout
        $state = new State();
        $state->setShoppingCarts([$a->getSupplier() => $a->clone(), $b->getSupplier() => $b->clone(), $c->getSupplier() => $c->clone()]);

        $this->initBest($state);

        // Au dessus du min de commande pour les 3 fournisseurs
        // et avec les meilleurs prix en plus
        if ($state->isValid()) {
            return $state;
        }

        $splitted = Utils::splitState($state);
        $splittedBackup = Utils::splitState($state);

        // 1) Au dessus du min de commande pour 2 fournisseurs sur 3:
        if ($splitted['positive']->countValid() === 2) {
            $positiveCarts = $splitted['positive']->getShoppingCarts();
            $negativeCarts = $splitted['negative']->getShoppingCarts();
            $orderedDiffs = $this->getOrderedDiffs($positiveCarts, $negativeCarts);

            // On va remplumer le fournisseur en négatif
            foreach ($orderedDiffs as $key => $diff) {
                $key = explode('-', $key);
                list($srcSupplierId, $dstSupplierId, $productId) = $key;
                $srcCart = $positiveCarts[$srcSupplierId];
                $dstCart = $negativeCarts[$dstSupplierId];

                $srcProductQuantity = $srcCart->getProducts()[$productId]->getQuantity();

                for ($i = 0; $i < $srcProductQuantity; ++$i) {
                    Utils::transfer($srcCart, $dstCart, $productId, 1);

                    if ($bis) {
                        if (!$srcCart->isValid()) {
                            Utils::transfer($dstCart, $srcCart, $productId, 1);
                            break;
                        }
                    }

                    if ($dstCart->isValid()) {
                        break ;
                    }
                }

                if ($dstCart->isValid()) {
                    break ;
                }
            }

            // Si après avoir déshabillé A et B on est toujours en dessous du min de commande de C,
            // on lance l'optim 2 fournisseurs avec A et B, après avoir transféré, si possile, les produits de C vers A et B
            if (!$splitted['negative']->isValid()) {
                if (Utils::canTransferState($splittedBackup['negative'], $splittedBackup['positive'])) {
                    Utils::transferState($splittedBackup['negative'], $splittedBackup['positive']);

                    return $this->optim2(...$splittedBackup['positive']->getShoppingCarts());
                }

                return null;
            }

            // A, B et C sont valides
            if ($state->isValid()) {
                return $state;
                // CODE TESTED TILL HERE !
            }

            $checkpoint = true;

            // A ou B est valide, (C est valide)
            if ($splitted['positive']->countValid() === 1) {
                // Exclure les produits du panier C des paniers A et B
                $ret = Utils::excludeProductsFromState($splitted['positive'], $splitted['negative']);
                /** @var State $newPositiveState */
                $newPositiveState = $ret['state'];
                /** @var Product[][] $excludedProducts */
                $excludedProducts = $ret['excluded'];

                $newState = $this->optim2(...$newPositiveState->getShoppingCarts());

                // On concatène les paniers
                if ($newState !== null) {
                    // On remet les produits qu'on avait exclu
                    foreach ($excludedProducts as $supplierId => $products) {
                        foreach ($products as $product) {
                            $product->setQuantity(0.0);
                            $newState->getShoppingCarts()[$supplierId]->addProduct($product);
                        }
                    }

                    // On rajoute le panier C
                    return Utils::mergeStates(false, $newState, $splitted['negative']);
                }

                return null;
            }
            // Ni A ni B ne sont valides
            else {
                // Exclure les produits du panier C des paniers A et B
                $ret = Utils::excludeProductsFromState($splitted['positive'], $splitted['negative']);
                /** @var State $state */
                $state = $ret['state'];
                /** @var Product[][] $excludedProducts */
                $excludedProducts = $ret['excluded'];
                $newState = $this->optim2(...$state->getShoppingCarts());

                // On concatène les paniers
                if ($newState !== null) {
                    // On remet les produits qu'on avait exclu
                    foreach ($excludedProducts as $supplierId => $products) {
                        foreach ($products as $product) {
                            $product->setQuantity(0.0);
                            $newState->getShoppingCarts()[$supplierId]->addProduct($product);
                        }
                    }

                    // On rajoute le panier C
                    return Utils::mergeStates(false, $newState, $splitted['negative']);
                }

                // A et B sont toujours invalides
                $state = Utils::mergeStates(false, ...$splitted);
                $keys = array_keys($state->getShoppingCarts());
                $newState = $state->clone();

                for ($i = 0; $i < count($keys); ++$i) {
                    $tmp = new State();
                    $tmp2 = new State();

                    $tmp->addShoppingCart($state->getShoppingCarts()[$keys[$i]]->clone());

                    for ($j = 0; $j < count($keys); ++$j) {
                        if ($j != $i) {
                            $tmp2->addShoppingCart($state->getShoppingCarts()[$keys[$j]]->clone());
                        }
                    }

                    if (Utils::canTransferState($tmp2, $tmp)) {
                        Utils::transferState($tmp2, $tmp);

                        $newState = Utils::pickBestState($tmp, $newState);
                    }
                }

                if ($newState->isValid()) {
                    return $newState;
                }

                return null;
            }
        }

        // 2) Au dessus du min de commande pour 1 fournisseur sur 3
        elseif ($splitted['positive']->countValid() === 1) {
            $positiveTotal = 0.0;

            // Vérifie si on peut allouer
            foreach ($splitted['positive']->getShoppingCarts() as $shoppingCart) {
                $positiveTotal += ($shoppingCart->getProductsTotal() - $shoppingCart->getMinOrder());
            }

            $negativeTotal = 0.0;

            foreach ($splitted['negative']->getShoppingCarts() as $shoppingCart) {
                $negativeTotal += ($shoppingCart->getMinOrder() - $shoppingCart->getProductsTotal());
            }

            if ($positiveTotal < $negativeTotal) {
                // On travaille sur le fournisseur le plus proche de son min de commande
                $keys = array_keys($splitted['negative']->getShoppingCarts());

                $cart = $splitted['negative']->getShoppingCarts()[$keys[count($keys) - 1]];
                $newState = $splitted['positive']->clone();
                $newState->addShoppingCart($cart);

                $newState = $this->optim2(...$newState->getShoppingCarts());

                return $newState;
            } else {
                $positiveCarts = $splitted['positive']->getShoppingCarts();
                $negativeCarts = $splitted['negative']->getShoppingCarts();
                $orderedDiffs = $this->getOrderedDiffs($positiveCarts, $negativeCarts);

                // On va remplumer le fournisseur en négatif
                foreach ($orderedDiffs as $key => $diff) {
                    $key = explode('-', $key);
                    list($srcSupplierId, $dstSupplierId, $productId) = $key;
                    $srcCart = $positiveCarts[$srcSupplierId];
                    $dstCart = $negativeCarts[$dstSupplierId];

                    $srcProductQuantity = $srcCart->getProducts()[$productId]->getQuantity();

                    for ($i = 0; $i < $srcProductQuantity; ++$i) {
                        Utils::transfer($srcCart, $dstCart, $productId, 1);

                        if ($dstCart->isValid()) {
                            break ;
                        }
                    }

                    if ($dstCart->isValid()) {
                        break ;
                    }
                }

                if ($state->isValid()) {
                    return $state;
                }

                $newSplitted = Utils::splitState($splitted['negative']);

                if ($splitted['positive']->isValid()) {
                    if (!empty($newSplitted['negative']->getShoppingCarts())) {
                        foreach ($newSplitted['negative']->getShoppingCarts() as $shoppingCart) {
                            $splitted['positive']->addShoppingCart($shoppingCart);
                        }

                        $newState = $this->optim2(...$splitted['positive']->getShoppingCarts());

                        if ($newState !== null) {
                            foreach ($newSplitted['positive']->getShoppingCarts() as $shoppingCart) {
                                $newState->addShoppingCart($shoppingCart);
                            }

                            return $newState;
                        }
                    }
                } else {
                    return $this->getBestCouple($splitted['negative']);
                }
            }
        }
        // 3) Au dessous du min de commande pour tous
        else {
            return $this->getBestCouple($splitted['negative']);
        }

        return null;
    }

    private function getBestCouple(State $state)
    {
        $keys = array_keys($state->getShoppingCarts());

        // On fait les 3 couples possibles (AB, BC, AC) et on prend le meilleur
        $ab = new State();
        $bc = new State();
        $ac = new State();

        $ab->addShoppingCart($state->getShoppingCarts()[$keys[0]]);
        $ab->addShoppingCart($state->getShoppingCarts()[$keys[1]]);

        $bc->addShoppingCart($state->getShoppingCarts()[$keys[1]]);
        $bc->addShoppingCart($state->getShoppingCarts()[$keys[2]]);

        $ac->addShoppingCart($state->getShoppingCarts()[$keys[0]]);
        $ac->addShoppingCart($state->getShoppingCarts()[$keys[2]]);

        $best = $state->clone();

        $best = Utils::pickBestState($best, $this->optim2(...$ab->getShoppingCarts()));
        $best = Utils::pickBestState($best, $this->optim2(...$bc->getShoppingCarts()));
        $best = Utils::pickBestState($best, $this->optim2(...$ac->getShoppingCarts()));

        if ($best->isValid()) {
            return $best;
        }

        return null;
    }

    private function _algo(State $initialState)
    {
        // On doit travailler avec au plus $maxSuppliers fournisseurs
        // Il faut trouver toutes les combinaisons de $maxSuppliers fournisseurs qui peuvent satisfaire la demande
        /** @var State[] $combinations */
        $combinations = $this->getCombinations($initialState, $this->maxSuppliers);

        // Si aucune combinaison de fournisseur ne peut satisfaire la demande, alors c'est impossible et on retourne
        if (empty($combinations)) {
            return null;
        }

        $bestState = null;

        foreach ($combinations as $nbSuppliers => $combinationsForNbSuppliers) {
            foreach ($combinationsForNbSuppliers as $combination) {
                // Inits this combination with the best prices available
                $combinationState = $this->initBestForCombination(...$combination);

                // If this combination is valid, then it can not possibly be better,
                // Otherwise we need to make this state valid, in the best way possible
                // If this state contains only 1 shopping cart and it is invalid, then it CAN NOT be fixed
                if (!$combinationState->isValid() && $nbSuppliers > 1) {
                    $combinationState = $this->tryFixState($combinationState);
                }

                if ($bestState === null) {
                    $bestState = $combinationState;
                } else {
                    $bestState = Utils::pickBestState($bestState, $combinationState);
                }
            }
        }

        return $bestState;
    }

    private function tryFixState(State $state)
    {
        // Can this state be fixed ?
        // First of all, let's strip the impossible carts from the state
        $this->stripImpossibleCarts($state);

        // If it is valid after, then we don't have to do anything more, it is already the most optimized cart
        if ($state->isValid()) {
            return $state;
        }

        /**
         * Now we can start the transfer logic...
         *
         * There are two main strategies for the transfer:
         * - emptying the suppliers that have not reach their minimum
         * - fulfilling the suppliers that have not reach their minimum
         *
         * Let's go
        **/
        $emptyState = $this->emptyAlgo($state);
        $fulfillState = $this->fulfillAlgo($state);

        // Returns the best of the best
        return Utils::pickBestState($emptyState, $fulfillState);
    }

    /**
     * Empty Algorithm:
     *
     * This method will try emptying the carts in a negative state following these steps:
     *
     * 1) Empty the carts by sending their items to the cheapeast suppliers,
     *    including other negative states (it will probably put them in a correct state after...)
     *
     * 2) Empty the carts by sending their items to the other suppliers that are in a negative state, the most boycotted one
     *
     * 3) Empty the carts by sending their items to the other suppliers that are in a negative state, but the cheapeast one among them
     *
     * 4) Empty the carts by sending their items to the other suppliers that are in a negative state, but the closest one to their minimum
     *
     * 5) Empty the carts by sending their items to the cheapeast suppliers among the positive one
     *
     * For each of these calls, the best state is returned (which may NOT be a positive state)
     *
     * @param State $state
     * @param bool $tryFillNegative
     * @param int $iteration
     * @return State
     */
    private function emptyAlgo(State $state, bool $tryFillNegative = true, int $iteration = 0)
    {
        $splitted = Utils::splitState($state->clone());

        /** @var ShoppingCart $cart */
        foreach ($splitted['negative']->getShoppingCarts() as $id => $cart) {
            foreach ($cart->getProducts() as $productId => $product) {
                if ($product->getQuantity() > 0.0) {
                    // Transfer this product to the cheapeast supplier among the state
                    if ($tryFillNegative) {
                        // les moins chers
                        $transferStrategy = Utils::TRANSFER_CHEAPEAST;

                        if ($iteration === 1) {
                            // les moins chers parmi ceux à renflouer
                            $transferStrategy = Utils::TRANSFER_CHEAPEAST|Utils::TRANSFER_FILL;
                        } elseif ($iteration === 2) {
                            // les plus en dèch
                            $transferStrategy = Utils::TRANSFER_FILL;
                        } elseif ($iteration === 3) {
                            // les plus proches de leurs min de commande
                            $transferStrategy = Utils::TRANSFER_FILL_CLOSEST;
                        }

                        Utils::transferProduct(
                            $cart,
                            $product,
                            $product->getQuantity(),
                            $transferStrategy,
                            $splitted['negative'],
                            $splitted['positive']
                        );
                    } else {
                        Utils::transferProduct(
                            $cart,
                            $product,
                            $product->getQuantity(),
                            Utils::TRANSFER_CHEAPEAST,
                            $splitted['positive']
                        );
                    }
                }
            }

            if ($cart->isValid()) {
                $splitted['negative']->removeShoppingCart($cart);
            }
        }

        $newState = Utils::mergeStates(true, $splitted['negative'], $splitted['positive']);

        if ($iteration === 5) {
            return $newState;
        }

        if ($iteration < 3) {
            return Utils::pickBestState(
                $this->emptyAlgo($state, true, $iteration + 1),
                $newState
            );
        }

        return Utils::pickBestState(
            $this->emptyAlgo($state, false, $iteration + 1),
            $newState
        );
    }

    /**
     * @param State $state
     * @return State
     */
    private function fulfillAlgo(State $state)
    {
        // We will try to fill the suppliers in a negative state
        $splitted = Utils::splitState($state->clone());
        $transfer = [];

        foreach ($splitted['negative']->getShoppingCarts() as $negativeCart) {
            foreach ($negativeCart->getProducts() as $productId => $product) {
                if ($product->getPrice() !== PHP_FLOAT_MAX) {
                    foreach ($splitted['positive']->getShoppingCarts() as $positiveCart) {
                        $_product = $positiveCart->getProducts()[$productId];

                        if ($_product->getQuantity() > 0.0) {
                            $tmp = $_product->computePrice();
                            $delta = $positiveCart->getDiffToMinOrder() - $tmp;
                            $quantity = $_product->getQuantity();

                            // on essaie d'ajuster la quantité
                            while ($delta < 0 && $quantity > $_product->getStep()) {
                                $quantity -= $_product->getStep();
                                $delta = $positiveCart->getDiffToMinOrder() - ($quantity * $_product->getPrice());
                            }

                            // Si ca ne met pas le fournisseur en dèch de faire le transfert
                            if ($delta >= 0) {
                                $gapFilled = $negativeCart->getDiffToMinOrder() + ($quantity * $product->getPrice());
                                $tmpQuantity = $quantity;

                                if ($gapFilled > 0) {
                                    // On va essayer de diminuer la qté à transférer de sorte à se rapprocher d'un gap de 0
                                    while ($gapFilled > 0 && $tmpQuantity > $product->getStep()) {
                                        $tmpQuantity -= $product->getStep();
                                        $gapFilled = $negativeCart->getDiffToMinOrder() + ($tmpQuantity * $product->getPrice());
                                    }

                                    if ($gapFilled < 0) {
                                        $tmpQuantity += $product->getStep();
                                        $gapFilled = $negativeCart->getDiffToMinOrder() + ($tmpQuantity * $product->getPrice());
                                    }
                                }

                                $transfer[$negativeCart->getSupplier()][$productId][] = [
                                    'pid' => $productId,
                                    'qty' => $tmpQuantity,
                                    'src' => $positiveCart,
                                    'gap' => $gapFilled
                                ];
                            }
                        }
                    }
                }
            }
        }

        $bestState = $state->clone();

        /**
         * On liste les possibilités de remplissage des fournisseur
         * Par exemple on va établir qu'on peut essayer de remplir les couples suivants:
         * - [1]
         * - [0, 1]
         * - [1, 2, 3]
         * - etc....
         *
         * et on prendra la meilleur combinaison de remplissage.
         * Les autres fournisseurs négatifs non présents dans les couples se verront ajoutés leur produit le moins cher de sorte à dépasser le min de commande
         */
        $suppliers = array_keys($transfer);
        $nbSuppliers = count($suppliers);
        $suppliersCombinations = [];

        for ($i = 1; $i <= $nbSuppliers; ++$i) {
            $suppliersCombinations[$i] = $this->getCombination($suppliers, $i);
        }

        foreach ($suppliersCombinations as $nbSupplierInCombination => $supplierCombinations) {
            /**
             * On vient de lister tous les transferts possibles de produit depuis les fournisseurs au delà de leur minimum
             * vers les fournisseurs en deça, tout en respectant la condition de ne pas descendre en dessous du minimum
             * de commande des fournisseurs sources.
             */
            foreach ($supplierCombinations as $suppliers) {
                $tmpBestState = $state->clone();
                $cache = [];

                foreach ($suppliers as $supplierId) {
                    $products = $transfer[$supplierId];
                    // Là il faut prendre la meilleure combinaison de produit qui remet le fournisseur d'équerre
                    // tout en optimisant les coûts
                    $negativeCart = $tmpBestState->getShoppingCarts()[$supplierId];
                    $gaps = [];
                    $gapsMap = [];

                    foreach ($products as $productId => $possibleTransfers) {
                        foreach ($possibleTransfers as $id => $data) {
                            if (!isset($cache[$data['src']->getSupplier()][$data['pid']])) {
                                $cache[$data['src']->getSupplier()][$data['pid']] = true;
                                $gaps[] = $data['gap'];
                                $gapsMap[] = $data;
                            }
                        }
                    }

                    // On va calculer toutes les possibilités de transfert de produit pour le remplissage
                    $gapsCombinations = [];

                    for ($i = 1; $i <= count($gaps); ++$i) {
                        $gapsCombinations[] = $this->getCombination($gaps, $i);
                    }

                    // La on va calculer quelles possibilités de transferts sont les meilleures
                    foreach ($gapsCombinations as $nbGaps => $combinations) {
                        foreach ($combinations as $combinationId => $combination) {
                            $tmpState = $state->clone();

                            foreach ($combination as $id) {
                                $data = $gapsMap[$id];
                                $src = $tmpState->getShoppingCarts()[$data['src']->getSupplier()];
                                $dst = $tmpState->getShoppingCarts()[$negativeCart->getSupplier()];

                                Utils::transfer($src, $dst, $data['pid'], $data['qty']);
                            }

                            $tmpBestState = Utils::pickBestState($tmpState, $tmpBestState);
                        }
                    }
                }

                // Si c'est toujours pas valide avec la "meilleure" combinaison de transfert,
                // alors on rajoute autant de produit que nécessaire chez les fournisseurs qui n'ont pas atteint leur minimum de commande
                if (!$tmpBestState->isValid()) {
                    foreach ($tmpBestState->getShoppingCarts() as $shoppingCart) {
                        if (!$shoppingCart->isValid()) {
                            $cheapest = null;
                            $cheapestPrice = PHP_FLOAT_MAX;

                            foreach ($shoppingCart->getProducts() as $product) {
                                if ($product->getPrice() < $cheapestPrice) {
                                    $cheapestPrice = $product->getPrice();
                                    $cheapest = $product;
                                }
                            }

                            while (!$shoppingCart->isValid()) {
                                $cheapest->setQuantity($cheapest->getQuantity() + $cheapest->getStep());
                            }
                        }
                    }
                }

                $bestState = Utils::pickBestState($bestState, $tmpBestState);
            }
        }

        return $bestState;
    }

    private function stripImpossibleCarts(State $state)
    {
        $carts = $state->getShoppingCarts();

        foreach ($carts as $supplierId => $cart) {
            $tmp = $cart->clone();

            // If we put everything we can on each cart, will the cart be valid ?
            foreach ($this->quantities as $productId => $quantity) {
                $product = $tmp->getProducts()[$productId];

                if ($product->getPrice() !== PHP_FLOAT_MAX) {
                    $product->setQuantity($this->quantities[$productId]);
                }
            }

            // If not, we can strip this cart from the state as it will never be valid...
            if (!$tmp->isValid()) {
                // But first we need to take its product and put it back on the other carts
                // It does not really matter where, but we are gonna put it on the cheapest supplier for this product
                $isEmpty = true;

                foreach ($cart->getProducts() as $productId => $product) {
                    if ($product->getQuantity() > 0.0) {
                        $bestCart = null;
                        $bestPrice = PHP_FLOAT_MAX;

                        foreach ($carts as $_supplierId => $_cart) {
                            if ($_supplierId !== $supplierId) {
                                $_product = $_cart->getProducts()[$productId];

                                if ($bestPrice > $_product->getPrice()) {
                                    $bestPrice = $_product->getPrice();
                                    $bestCart = $_cart;
                                }
                            }
                        }

                        if ($bestCart !== null) {
                            Utils::transfer($cart, $bestCart, $productId, $product->getQuantity());
                        } else {
                            $isEmpty = false;
                        }
                    }
                }

                if ($isEmpty) {
                    $state->removeShoppingCart($cart);
                }
            }
        }
    }

    private function initBestForCombination(int ...$suppliers)
    {
        $state = new State();
        $quantities = $this->quantities;

        foreach ($suppliers as $supplier) {
            $shoppingCart = new ShoppingCart($supplier, $this->thresholds[$supplier][2], $this->thresholds[$supplier][0], $this->thresholds[$supplier][1]);

            foreach ($quantities as $productId => &$quantity) {
                $product = new Product($productId, $quantity, $this->prices[$supplier][$productId], $this->steps[$supplier][$productId] / 100);
                $quantity = 0;

                $shoppingCart->addProduct($product);
            }

            $state->addShoppingCart($shoppingCart);
        }

        $this->initBest($state);

        return $state;
    }

    private function getCombination(array $supplierProducts, int $depth, int $currentDepth = 1, int $startIndex = 0)
    {
        $keys = array_keys($supplierProducts);
        $nbSuppliers = count($keys);
        $combination = [];

        for ($i = $startIndex; $i < $nbSuppliers; ++$i) {
            if ($currentDepth < $depth) {
                $subCombination = $this->getCombination($supplierProducts, $depth, $currentDepth + 1, $i + 1);

                foreach ($subCombination as $sub) {
                    $combination[] = array_merge([$keys[$i]], $sub);
                }
            } else {
                $combination[] = [$keys[$i]];
            }
        }

        return $combination;
    }

    /**
     * Returns the distinct combinations of supplier that can fulfill the products request from 1 to $maxSuppliers
     *
     * @param State $initialState
     * @param int $maxSuppliers
     * @return array
     */
    private function getCombinations(State $initialState, int $maxSuppliers)
    {
        $combinations = [];
        $keys = array_keys($initialState->getShoppingCarts());
        $products = $initialState->getShoppingCarts()[$keys[0]]->getProducts();
        $nbProducts = count($products);
        $productCandidates = [];

        foreach ($products as $productId => $product) {
            $productCandidates[$productId] = Utils::getCandidatesForProduct($initialState, $productId);
        }

        $supplierProducts = [];

        foreach ($productCandidates as $productId => $candidates) {
            // No candidate for a product ? Then it is unfeasable...
            if (empty($candidates)) {
                return [];
            }

            $suppliers = array_keys($candidates);

            foreach ($suppliers as $supplierId) {
                $supplierProducts[$supplierId][] = $productId;
            }
        }

        $keys = array_keys($supplierProducts);
        $nbSuppliers = count($keys);

        // Enumerates all combinations of suppliers in the range [1, $maxSuppliers]
        for ($i = 1; $i <= $nbSuppliers && $i <= $maxSuppliers; ++$i) {
            $combinations[$i] = $this->getCombination($supplierProducts, $i);
        }

        // Removes the combinations that do not work
        foreach ($combinations as $nbSuppliers => &$combinationsForNbSuppliers) {
            foreach ($combinationsForNbSuppliers as $key => $combination) {
                $productAggregate = [];

                foreach ($combination as $supplier) {
                    $productAggregate = array_unique(array_merge($productAggregate, $supplierProducts[$supplier]));
                }

                if (count($productAggregate) !== $nbProducts) {
                    unset($combinationsForNbSuppliers[$key]);
                }
            }

            if (empty($combinationsForNbSuppliers)) {
                unset($combinations[$nbSuppliers]);
            }
        }

        return $combinations;
    }

    /**
     * @param State $initialState
     * @return State
     */
    private function heuristic1(State $initialState)
    {
        $globalState = $initialState->clone();
        $globalCarts = $globalState->getShoppingCarts();
        $nbGlobalCarts = count($globalCarts);
        $keys = array_keys($globalCarts);
        $bestGlobalState = $globalState->clone();

        for ($i = 0; $i < $nbGlobalCarts; ++$i) {
            for ($j = 0; $j < $nbGlobalCarts; ++$j) {
                if ($i !== $j) {
                    $srcCart = $globalCarts[$keys[$i]];
                    $dstCart = $globalCarts[$keys[$j]];
                    $diffs = Utils::getPricesDiff($srcCart, $dstCart);

                    foreach ($diffs as $productId => $diff) {
                        $product = $srcCart->getProducts()[$productId];

                        if (Utils::canTransferProduct($srcCart, $dstCart, $productId)) {
                            $maxQuantity = $product->getQuantity();

                            for ($q = 0; $q < $maxQuantity; ++$q) {
                                Utils::transfer($srcCart, $dstCart, $productId, 1);

                                $bestGlobalState = Utils::pickBestState($bestGlobalState, $globalState)->clone();
                            }
                        }
                    }
                }
            }
        }

        return $bestGlobalState;
    }

    public function compute()
    {
        $state = $this->initialState->clone();

        switch ($this->algo) {
            case '2':
                $state = $this->optim2(...$state->getShoppingCarts());
                break ;
            case '3':
                $state = $this->optim3(false, ...$state->getShoppingCarts());
                break ;
            case '3b':
                $state = $this->optim3(true, ...$state->getShoppingCarts());
                break ;
            case 'h1':
                $state = $this->heuristic1($state);
                break ;
            case 't':
                $state = $this->_algo($state);
                break ;
        }

        if ($state !== null) {
            $res = [
                bcmul($state->getTotal(),  Arbitrator::QUANTITY_COEFF),
                [],
                '0.00'
            ];

            foreach ($state->getShoppingCarts() as $supplierId => $shoppingCart) {
                $quantitiesByProduct = [];

                foreach ($shoppingCart->getProducts() as $productId => $product) {
                    $quantitiesByProduct[$productId] = bcmul($product->getQuantity(),  Arbitrator::QUANTITY_COEFF);
                }

                $res[1][$supplierId] = $quantitiesByProduct;
            }

            return $res;
        }

        return null;
    }

    private function log(string $origin, string $message)
    {
//        echo "$origin: $message\n";
    }
}
