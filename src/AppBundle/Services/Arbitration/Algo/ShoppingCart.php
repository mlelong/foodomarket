<?php

namespace AppBundle\Services\Arbitration\Algo;

use SplObserver;
use SplSubject;

class ShoppingCart implements SplObserver
{
    /** @var int */
    private $supplier;

    /** @var float */
    private $minOrder;

    /** @var float */
    private $francoPort;

    /** @var float */
    private $deliveryCosts;

    /** @var Product[] */
    private $products;

    /** @var float */
    private $subTotal = null;

    /** @var float */
    private $total = null;

    public function __construct(int $supplier, float $minOrder = 0.0, float $francoPort = 0.0, float $deliveryCosts = 0.0)
    {
        $this->supplier = $supplier;
        $this->minOrder = $minOrder;
        $this->francoPort = $francoPort;
        $this->deliveryCosts = $deliveryCosts;
    }

    public function __destruct()
    {
        foreach ($this->products as $product) {
            $product->detach($this);
        }
    }

    public function clone()
    {
        $shoppingCart = new ShoppingCart($this->supplier, $this->minOrder, $this->francoPort, $this->deliveryCosts);

        foreach ($this->products as $product) {
            $shoppingCart->addProduct($product->clone());
        }

        $shoppingCart->subTotal = $this->subTotal;
        $shoppingCart->total = $this->total;

        return $shoppingCart;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function canHandle(Product $product)
    {
        return $this->products[$product->getId()]->getPrice() !== PHP_FLOAT_MAX;
    }

    /**
     * @return float
     */
    public function getProductsTotal()
    {
        if ($this->subTotal === null) {
            return $this->subTotal = array_reduce($this->products, function ($total, Product $product) {
                return $total + $product->computePrice();
            }, 0.0);
        }

        return $this->subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        if ($this->total === null) {
            $productsTotal = $this->getProductsTotal();

            if ($productsTotal >= $this->francoPort) {
                return $productsTotal;
            } elseif ($productsTotal == 0.0) {
                return 0.0;
            }

            return $this->total = $productsTotal + $this->deliveryCosts;
        }

        return $this->total;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $productsTotal = $this->getProductsTotal();

        if ($productsTotal === 0.0) {
            return true;
        }

        return $productsTotal !== INF && $productsTotal !== PHP_FLOAT_MAX && $productsTotal >= $this->minOrder;
    }

    /**
     * @return int
     */
    public function getSupplier(): int
    {
        return $this->supplier;
    }

    /**
     * @param int $supplier
     */
    public function setSupplier(int $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts(array $products): void
    {
        $this->products = [];

        foreach ($products as $product) {
            $product->attach($this);
            $this->products[$product->getId()] = $product;
        }

        $this->reset();
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $product->attach($this);
        $this->products[$product->getId()] = $product;
        $this->reset();
    }

    /**
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $product->detach($this);
        unset($this->products[$product->getId()]);
        $this->reset();
    }

    /**
     * @return float
     */
    public function getMinOrder(): float
    {
        return $this->minOrder;
    }

    /**
     * @param float $minOrder
     */
    public function setMinOrder(float $minOrder): void
    {
        $this->minOrder = $minOrder;
    }

    /**
     * @return float
     */
    public function getFrancoPort(): float
    {
        return $this->francoPort;
    }

    /**
     * @param float $francoPort
     */
    public function setFrancoPort(float $francoPort): void
    {
        $this->francoPort = $francoPort;
    }

    /**
     * @return float
     */
    public function getDeliveryCosts(): float
    {
        return $this->deliveryCosts;
    }

    /**
     * @param float $deliveryCosts
     */
    public function setDeliveryCosts(float $deliveryCosts): void
    {
        $this->deliveryCosts = $deliveryCosts;
    }

    public function getDiffToFill()
    {
        if ($this->isValid()) {
            return 0.0;
        }

        return $this->getDiffToMinOrder();
    }

    public function getDiffToMinOrder()
    {
        $productsTotal = $this->getProductsTotal();

        if ($productsTotal === 0.0) {
            return 0.0;
        }

        if ($productsTotal === INF) {
            return INF;
        }

        return $productsTotal - $this->minOrder;
    }

    public function dump()
    {
        $isValid = $this->isValid() ? "YES" : "NO";

        echo <<<HTML
    ### CART DUMP ###
    ### Supplier      : {$this->supplier}
    ### Min order     : {$this->minOrder}€
    ### Franco        : {$this->francoPort}€
    ### Shipping cost : {$this->deliveryCosts}€
    ### Sub-total     : {$this->getProductsTotal()}€
    ### Total         : {$this->getTotal()}€
    ### Margin        : {$this->getDiffToMinOrder()}€
    ### Is Valid      : {$isValid}
    ### Products      :

HTML;
        foreach ($this->getProducts() as $product) {
            $product->dump();
        }
        echo <<<HTML
    ### END DUMP ###


HTML;
    }

    /**
     * Receive update from subject
     * @link https://php.net/manual/en/splobserver.update.php
     * @param SplSubject $subject <p>
     * The <b>SplSubject</b> notifying the observer of an update.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function update(SplSubject $subject)
    {
        $this->reset();
    }

    private function reset()
    {
        $this->subTotal = null;
        $this->total = null;
    }
}