<?php

namespace AppBundle\Services\Arbitration\Algo;

use SplObserver;
use SplSubject;

class Product implements SplSubject
{
    /** @var int */
    private $id;
    /** @var float */
    private $quantity;
    /** @var float */
    private $price;
    /** @var float */
    private $step;
    /** @var SplObserver[] */
    private $observers = [];

    public function __construct(int $id, float $quantity = 0.0, float $price = 0.0, float $step = 1.0)
    {
        $this->id = $id;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->step = $step;
    }

    public function clone()
    {
        return new Product($this->id, $this->quantity, $this->price, $this->step);
    }

    /**
     * @return float
     */
    public function computePrice()
    {
        return $this->quantity * $this->price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity): void
    {
        $this->quantity = $quantity;
        $this->notify();
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getStep(): float
    {
        return $this->step;
    }

    /**
     * @param float $step
     */
    public function setStep(float $step): void
    {
        $this->step = $step;
    }

    /**
     * Attach an SplObserver
     * @link https://php.net/manual/en/splsubject.attach.php
     * @param SplObserver $observer <p>
     * The <b>SplObserver</b> to attach.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function attach(SplObserver $observer)
    {
        $this->observers[] = $observer;
    }

    /**
     * Detach an observer
     * @link https://php.net/manual/en/splsubject.detach.php
     * @param SplObserver $observer <p>
     * The <b>SplObserver</b> to detach.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function detach(SplObserver $observer)
    {
        $key = array_search($observer,$this->observers, true);

        if($key){
            unset($this->observers[$key]);
        }
    }

    /**
     * Notify an observer
     * @link https://php.net/manual/en/splsubject.notify.php
     * @return void
     * @since 5.1.0
     */
    public function notify()
    {
        foreach ($this->observers as $value) {
            $value->update($this);
        }
    }

    public function dump()
    {
        echo "\t#\t{$this->id}: {$this->quantity} x {$this->price}€ = {$this->computePrice()}€\n";
    }
}