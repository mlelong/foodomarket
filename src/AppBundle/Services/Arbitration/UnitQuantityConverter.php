<?php

namespace AppBundle\Services\Arbitration;

use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;

class UnitQuantityConverter
{
    public static function convert(
        ProductVariant $inputVariant, float $quantity, string $requestedUnit,
        ProductVariant $outputVariant, ?float &$finalQuantity = 0, ?string &$finalUnit = ''
    ) {
        if ($finalUnit === null) {
            $finalQuantity = 0;
        }

        if ($finalUnit === null) {
            $finalUnit = '';
        }

        switch ($requestedUnit) {
            case 'ORDER_PC':
            case 'ORDER_KGPC':
            case 'KGPC':
            case 'PC':
                self::pcToUnitQuantity($inputVariant, $quantity, $outputVariant, $finalUnit, $finalQuantity);
                break ;
            case 'ORDER_CO':
            case 'ORDER_COLIS':
            case 'COLIS':
            case 'CO':
                self::coToUnitQuantity($inputVariant, $quantity, $outputVariant, $finalUnit, $finalQuantity);
                break ;
            case 'ORDER_KG':
            case 'ORDER_L':
            default:
                self::kgToUnitQuantity($inputVariant, $quantity, $outputVariant, $finalUnit, $finalQuantity);
                break ;
        }
    }

    private static function pcToUnitQuantity(ProductVariant $variant, float $quantity, ProductVariant $outputVariant, string &$finalUnit, float &$finalQuantity)
    {
        $inputOrderUnit = self::getOptionCode($variant->getOptionOrderUnit(), 'ORDER_KG');
        $orderUnit = self::getOptionCode($outputVariant->getOptionOrderUnit(), 'ORDER_KG');

        switch ($orderUnit) {
            case 'ORDER_PC':
            case 'ORDER_KGPC':
                $finalQuantity = ceil($quantity * $variant->getWeight() / $outputVariant->getWeight());
                $finalUnit = 'PC';
                break ;
            case 'ORDER_CO':
            case 'ORDER_COLIS':
                if ($inputOrderUnit === 'ORDER_PC') {
                    $unitQuantity = self::getOptionValue($variant->getOptionUnitQuantity(), 1);
                    $quantity /= $unitQuantity;
                }

                $finalQuantity = ceil($quantity * $variant->getWeight() / $outputVariant->getWeight());
                $finalUnit = self::ucCodeToUcValue($orderUnit);
                break ;
            case 'ORDER_KG':
            case 'ORDER_L':
            default:
                $finalQuantity = $quantity * $variant->getWeight() / $outputVariant->getWeight();
                $finalUnit = self::ucCodeToUcValue($orderUnit);
                break ;
        }
    }

    /**
     * @param ProductVariant $variant
     * @param float $quantity
     * @param ProductVariant $outputVariant
     * @param string $finalUnit
     * @param float $finalQuantity
     */
    private static function kgToUnitQuantity(ProductVariant $variant, float $quantity, ProductVariant $outputVariant, &$finalUnit, &$finalQuantity)
    {
        $orderUnit = self::getOptionCode($outputVariant->getOptionOrderUnit(), 'ORDER_KG');

        switch ($orderUnit) {
            case 'ORDER_PC':
            case 'ORDER_CO':
            case 'ORDER_COLIS':
                $finalQuantity = ceil($quantity * $variant->getWeight() / $outputVariant->getWeight());
                $finalUnit = self::ucCodeToUcValue($orderUnit);
                break ;
            case 'ORDER_KGPC':
                $finalQuantity = $quantity;
                $finalUnit = 'KG';
                break ;
            case 'ORDER_KG':
            case 'ORDER_L':
            default:
                $finalQuantity = $quantity;
                $finalUnit = self::ucCodeToUcValue($orderUnit);
                break ;
        }
    }

    private static function coToUnitQuantity(ProductVariant $variant, float $quantity, ProductVariant $outputVariant, string &$finalUnit, float &$finalQuantity)
    {
        $orderUnit = self::getOptionCode($outputVariant->getOptionOrderUnit(), 'ORDER_KG');

        switch ($orderUnit) {
            case 'ORDER_PC':
            case 'ORDER_KGPC':
            case 'ORDER_CO':
            case 'ORDER_COLIS':
                $finalQuantity = ceil($quantity * $variant->getWeight() / $outputVariant->getWeight());
                break ;
            case 'ORDER_KG':
            case 'ORDER_L':
            default:
                $finalQuantity = $quantity * $variant->getWeight();
                break ;
        }

        $finalUnit = self::ucCodeToUcValue($orderUnit);
    }

    /**
     * @param ProductOptionValue|false $optionValue
     * @param null $default
     * @return string|null
     */
    private static function getOptionCode($optionValue, $default = null)
    {
        if (!$optionValue) {
            return $default;
        }

        return $optionValue->getCode();
    }

    /**
     * @param ProductOptionValue|false $optionValue
     * @param null $default
     * @return string|null
     */
    private static function getOptionValue($optionValue, $default = null)
    {
        if (!$optionValue) {
            return $default;
        }

        return $optionValue->getValue();
    }

    private static function ucCodeToUcValue(string $ucCode)
    {
        static $map = [
            'ORDER_KG' => 'KG',
            'ORDER_PC' => 'PC',
            'ORDER_KGPC' => 'PC',
            'ORDER_L' => 'L',
            'ORDER_CO' => 'CO',
            'ORDER_COLIS' => 'CO',
        ];

        return isset($map[$ucCode]) ? $map[$ucCode] : $ucCode;
    }
}