<?php

namespace AppBundle\Services\Arbitration;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Services\Arbitration\Algo\Algo;
use AppBundle\Services\ProductUtils;

class Arbitrator
{
    const QUANTITY_COEFF = 100;
    
    /** @var int */
    private $maxSuppliers = 1;

    /** @var Supplier[] */
    private $suppliers = [];

    /**
     * [variantId => [variant, quantity]]
     * @var array
     */
    private $request = [];

    /** @var ProductUtils */
    private $productUtils;

    /** @var SupplierProductPriceRepository */
    private $supplierProductPriceRepository;

    /**
     * Arbitrator constructor.
     * @param ProductUtils $productUtils
     * @param SupplierProductPriceRepository $supplierProductPriceRepository
     */
    public function __construct(ProductUtils $productUtils, SupplierProductPriceRepository $supplierProductPriceRepository)
    {
        $this->productUtils = $productUtils;
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
    }

    /**
     * @param int $maxSuppliers
     * @return Arbitrator
     */
    public function setMaxSuppliers(int $maxSuppliers): Arbitrator
    {
        $this->maxSuppliers = $maxSuppliers;

        return $this;
    }

    /**
     * @param Supplier $supplier
     * @return Arbitrator
     */
    public function addSupplier(Supplier $supplier): Arbitrator
    {
        $this->suppliers[$supplier->getId()] = $supplier;

        return $this;
    }

    /**
     * @param ProductVariant $variant
     * @param float $quantity
     * @param string|null $unit
     * @return Arbitrator
     */
    public function requestVariant(ProductVariant $variant, float $quantity, ?string $unit = null): Arbitrator
    {
        $this->request[$variant->getId()] = ['variant' => $variant, 'quantity' => $quantity, 'unit' => $unit];

        return $this;
    }

    /**
     * @param Restaurant|int $restaurant
     * @param string $algo
     * @return array|null
     * @throws \Exception
     */
    public function getResult($restaurant = 4, string $algo = 'x')
    {
        $prices = [];
        $thresholds = [];
        $quantities = [];
        $interval = [];
        $suppliers = array_values($this->suppliers);
        $request = array_values($this->request);
        $pricesUnit = [];
        $steps = [];
        $variants = [];

        foreach ($suppliers as $supplierIndex => $supplier) {
            $shouldFillQuantities = empty($quantities);

            foreach ($request as $req) {
                /** @var ProductVariant $requestVariant */
                $requestVariant = $req['variant'];

                // We should not modify the original price/variant requested
                // Try to get a blocked price
                $price = $this->supplierProductPriceRepository->findOneBy([
                    'supplier' => $supplier->getFacadeSupplier(),
                    'restaurant' => $restaurant,
                    'productVariant' => $requestVariant,
                ]);

                // Fetch the same foodomarket price
                if ($price === null) {
                    $price = $this->supplierProductPriceRepository->findOneBy([
                        'supplier' => $supplier,
                        'restaurant' => 4,
                        'productVariant' => $requestVariant,
                    ]);
                }

                // Fetch a similar variant price
                if ($price === null) {
                    $price = $this->productUtils->getCheapestSimilarVariantForSupplierAndRestaurantIso($requestVariant, $supplier, $restaurant);
                }

                $requestOrderUnit = $req['unit'] ?: $requestVariant->getOptionOrderUnit();

                if ($price === null) {
                    $price = PHP_FLOAT_MAX;
                    $unit = null;
                    $finalQuantity = 0;
                    $step = 1;
                    $variant = null;
                } else {
                    $variant = $price->getProductVariant();

                    UnitQuantityConverter::convert($requestVariant, $req['quantity'], $requestOrderUnit, $variant, $finalQuantity, $unit);

                    switch ($unit) {
                        case 'PC':
                            $price = $price->getUnitPrice();
                            $step = 1;
                            break ;
                        case 'COLIS':
                        case 'CO':
                            $price = $price->getUnitPrice();
                            $step = 1;
                            break ;
                        case 'L':
                            $price = $price->getKgPrice();
                            $step = 1;
                            break ;
                        case 'KG':
                        default:
                            $price = $price->getKgPrice();
                            $step = 0.1;
                            break ;
                    }

                    $price /= 100;
                }

                $variants[$supplierIndex][] = $variant;
                $prices[$supplierIndex][] = $price;
                $pricesUnit[$supplierIndex][] = $unit;
                $steps[$supplierIndex][] = $step * self::QUANTITY_COEFF;

                if ($shouldFillQuantities) {
                    $quantities[] = (int) ($req['quantity'] * self::QUANTITY_COEFF);
                    $interval[] = $step * self::QUANTITY_COEFF * 100;
                }
            }

            $thresholds[] = [
                $supplier->getFacadeSupplier()->getFreePort() * self::QUANTITY_COEFF,
                $supplier->getFacadeSupplier()->getShippingCost() * self::QUANTITY_COEFF,
                $supplier->getFacadeSupplier()->getMinimumOrder() * self::QUANTITY_COEFF
            ];
        }

        $report = [];

        foreach ($prices as $supplierIndex => $supplierPrices) {
            $report[0][$supplierIndex] = $suppliers[$supplierIndex]->getDisplayName(true);

            foreach ($supplierPrices as $priceIndex => $supplierPrice) {
                /** @var ProductVariant $variant */
                $variant = $variants[$supplierIndex][$priceIndex];

                if ($variant !== null) {
                    $label = "({$variant->getId()}) {$variant->getFullDisplayName()}: {$supplierPrice}€";
                } else {
                    $label = '';
                }

                $report[$priceIndex + 1][$supplierIndex] = $label;
            }
        }

        switch ($algo) {
            case 'x':
                $res = Optimizer::computeOptimumMaxSupplier($this->maxSuppliers, $prices, $thresholds, $quantities, $interval, $steps);
                break ;
            default:
                $startTime = microtime(true);

                $algo = new Algo($algo);
                $algo->init($this->maxSuppliers, $prices, $thresholds, $quantities, $interval, $steps);
                $res = $algo->compute();

                if ($res === null) {
                    return null;
                }

                $endTime = microtime(true);

                $res[2] = number_format(($endTime - $startTime), 2, '.', ' ');
                break ;
        }

        $ret = [
            'total' => 0,
            'suppliers' => [],
            'time' => 0,
            'report' => $report
        ];

        if ($res !== null) {
            $ret['time'] = $res[2];
            $ret['total'] = $res[0] / self::QUANTITY_COEFF;

            foreach ($res[1] as $supplierIndex => $quantities) {
                $supplier = $suppliers[$supplierIndex];

                $ret['suppliers'][$supplier->getId()] = [
                    'supplier' => $supplier,
                    'items' => [],
                    'total' => 0
                ];

                foreach ($quantities as $productIndex => $qty) {
                    if ($qty != 0) {
                        $price = $prices[$supplierIndex][$productIndex];
                        $qty /= self::QUANTITY_COEFF;
                        $itemTotal = $qty * $price;

                        $ret['suppliers'][$supplier->getId()]['items'][$productIndex] = [
                            'variant' => $variants[$supplierIndex][$productIndex],
                            'quantity' => $qty,
                            'unit' => $pricesUnit[$supplierIndex][$productIndex],
                            'price' => $price,
                            'total' => $itemTotal
                        ];

                        $ret['suppliers'][$supplier->getId()]['total'] += $itemTotal;
                    }
                }

                if (empty($ret['suppliers'][$supplier->getId()]['items'])) {
                    unset($ret['suppliers'][$supplier->getId()]);
                }
            }
        }

        return $ret;
    }
}