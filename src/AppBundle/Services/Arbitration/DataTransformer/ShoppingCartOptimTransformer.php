<?php

namespace AppBundle\Services\Arbitration\DataTransformer;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Entity\ShoppingCartItem;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\Arbitration\Arbitrator;
use AppBundle\Services\Product\ProductFinderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sylius\Component\Core\Model\Product;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ShoppingCartOptimTransformer implements DataTransformerInterface
{
    /** @var Arbitrator */
    private $arbitrator;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var ProductFinderInterface */
    private $productFinder;

    /** @var CacheManager */
    private $liipImagineCacheManager;

    /** @var ProductVariantRepository */
    private $productVariantRepository;

    public function __construct(
        Arbitrator $arbitrator,
        SupplierRepository $supplierRepository,
        ProductFinderInterface $productFinder,
        CacheManager $liipImagineCacheManager,
        ProductVariantRepository $productVariantRepository)
    {
        $this->arbitrator = $arbitrator;
        $this->supplierRepository = $supplierRepository;
        $this->productFinder = $productFinder;
        $this->liipImagineCacheManager = $liipImagineCacheManager;
        $this->productVariantRepository = $productVariantRepository;
    }

    /**
     * Transforms a value from the original representation to a transformed representation.
     *
     * This method is called on two occasions inside a form field:
     *
     * 1. When the form field is initialized with the data attached from the datasource (object or array).
     * 2. When data from a request is submitted using {@link Form::submit()} to transform the new input data
     *    back into the renderable format. For example if you have a date field and submit '2009-10-10'
     *    you might accept this value because its easily parsed, but the transformer still writes back
     *    "2009/10/10" onto the form field (for further displaying or other purposes).
     *
     * This method must be able to deal with empty values. Usually this will
     * be NULL, but depending on your implementation other empty values are
     * possible as well (such as empty strings). The reasoning behind this is
     * that value transformers must be chainable. If the transform() method
     * of the first value transformer outputs NULL, the second value transformer
     * must be able to process that value.
     *
     * By convention, transform() should return an empty string if NULL is
     * passed.
     *
     * @param ShoppingCartOptimData $value The value in the original representation
     *
     * @return mixed The value in the transformed representation
     *
     * @throws TransformationFailedException when the transformation fails
     */
    public function transform($value)
    {
        $partnerSuppliers = $value->getSuppliers();

        // TODO: Should we keep this ?
        if (empty($partnerSuppliers)) {
            $partnerSuppliers = $this->supplierRepository->getByRestaurant($value->getRestaurant());
        } else {
            foreach ($partnerSuppliers as $key => $supplier) {
                if (!$supplier instanceof Supplier) {
                    $partnerSuppliers[$key] = $this->supplierRepository->find($supplier);
                }
            }
        }

        $this->arbitrator->setMaxSuppliers(min($value->getMaxDeliveries(), count($partnerSuppliers)));

        foreach ($partnerSuppliers as $partnerSupplier) {
            $this->arbitrator->addSupplier($partnerSupplier);
        }

        $ret = [];
        $i = 0;

        foreach ($value->getShoppingCarts() as $shoppingCart) {
            $supplier = $shoppingCart->getSupplier();

            if (!isset($ret['original'][$supplier->getId()])) {
                $ret['original'][$supplier->getId()] = [
                    'supplier' => $supplier->getName(),
                    'products' => [],
                    'itemsTotal' => 0,
                    'shippingCost' => $supplier->getShippingCost(),
                    'freePort' => $supplier->getFreePort(),
                    'minOrder' => $supplier->getMinimumOrder(),
                    'total' => 0
                ];
            }

            foreach ($shoppingCart->getItems() as $shoppingCartItem) {
                $product = $this->productFinder->getProduct($shoppingCartItem->getProductVariant(), $shoppingCart->getSupplier(), $value->getRestaurant());

                if (!$product || $product['productPrice'] == 0) {
                    continue ;
                }

                $this->arbitrator->requestVariant($shoppingCartItem->getProductVariant(), $shoppingCartItem->getQuantity(), $shoppingCartItem->getUnit());

                switch ($shoppingCartItem->getUnit()) {
                    case 'PC':
                    case 'CO':
                        $price = bcdiv($product['productPriceUnit'], 100, 2);
                        break ;
                    case 'KG':
                    case 'L':
                    default:
                        $price = bcdiv($product['productPrice'], 100, 2);
                        break ;
                }

                $total = bcmul($price, $shoppingCartItem->getQuantity(), 2);

                /** @var Product $product */
                $product = $shoppingCartItem->getProductVariant()->getProduct();
                /** @var ArrayCollection $pictures */
                $pictures = $product->getImages();
                $picture = $this->liipImagineCacheManager->getBrowserPath($pictures->isEmpty() ? 'shop-default-product.jpg' : $pictures->first()->getPath(), 'front_tiny');

                $ret['original'][$supplier->getId()]['products'][$i++] = [
                    'id' => $shoppingCartItem->getProductVariant()->getId(),
                    'name' => $this->getFullVariantName($shoppingCartItem->getProductVariant()),
                    'picture' => $picture,
                    'quantity' => $shoppingCartItem->getQuantity(),
                    'unit' => $shoppingCartItem->getUnit(),
                    'price' => $price,
                    'total' => $total
                ];

                $ret['original'][$supplier->getId()]['itemsTotal'] += $total;
                $ret['original'][$supplier->getId()]['total'] = $ret['original'][$supplier->getId()]['itemsTotal'];

                if ($ret['original'][$supplier->getId()]['total'] < $ret['original'][$supplier->getId()]['freePort']) {
                    if ($ret['original'][$supplier->getId()]['shippingCost'] != 9999) {
                        $ret['original'][$supplier->getId()]['total'] += $ret['original'][$supplier->getId()]['shippingCost'];
                    }
                }
            }
        }

        try {
            $optimum = $this->arbitrator->getResult($value->getRestaurant(), $value->getAlgo());
        } catch (\Exception $e) {
            throw new TransformationFailedException($e->getMessage(), $e->getCode(), $e);
        }

        foreach ($optimum['suppliers'] as $data) {
            /** @var Supplier $supplier */
            $supplier = $data['supplier'];
            $supplier = $supplier->getFacadeSupplier();

            foreach ($data['items'] as $productIndex => $item) {
                $itemTotal = number_format($item['total'], 2, '.', '');
                /** @var ProductVariant $variant */
                $variant = $item['variant'];

                $ret['optim']['products'][$productIndex][] = [
                    'id' => $variant->getId(),
                    'supplier' => $supplier->getName(),
                    'name' => $this->getFullVariantName($variant),
                    'quantity' => $item['quantity'],
                    'unit' => $item['unit'],
                    'price' => number_format($item['price'], 2, '.', ''),
                    'total' => $itemTotal
                ];

                if (!isset($ret['optim']['suppliers'][$supplier->getId()])) {
                    $ret['optim']['suppliers'][$supplier->getId()] = [
                        'supplier' => $supplier->getName(),
                        'itemsTotal' => 0,
                        'total' => 0,
                        'freePort' => $supplier->getFreePort(),
                        'shippingCost' => $supplier->getShippingCost(),
                        'minOrder' => $supplier->getMinimumOrder(),
                    ];
                }

                $ret['optim']['suppliers'][$supplier->getId()]['itemsTotal'] += $itemTotal;
                $ret['optim']['suppliers'][$supplier->getId()]['total'] = $ret['optim']['suppliers'][$supplier->getId()]['itemsTotal'];

                if ($ret['optim']['suppliers'][$supplier->getId()]['total'] < $ret['optim']['suppliers'][$supplier->getId()]['freePort']) {
                    $ret['optim']['suppliers'][$supplier->getId()]['total'] += $ret['optim']['suppliers'][$supplier->getId()]['shippingCost'];
                }
            }
        }

        $originalTotal = 0;
        $originalShipping = [];

        foreach ($ret['original'] as $data) {
            $originalTotal += $data['itemsTotal'];

            if ($data['itemsTotal'] < $data['freePort']) {
                if ($data['shippingCost'] != 9999) {
                    $originalTotal += $data['shippingCost'];
                }

                $originalShipping[] = [
                    'supplier' => $data['supplier'],
                    'defaultShipping' => $data['shippingCost'],
                    'shippingCost' => $data['shippingCost'],
                    'total' => number_format($data['total'], 2, '.', ' '),
                    'subTotal' => number_format($data['itemsTotal'], 2, '.', ' '),
                    'minOrder' => number_format($data['minOrder'], 2, '.',  ' '),
                    'isValid' => $data['itemsTotal'] >= $data['minOrder']
                ];
            } else {
                $originalShipping[] = [
                    'supplier' => $data['supplier'],
                    'shippingCost' => 0,
                    'defaultShipping' => $data['shippingCost'],
                    'total' => number_format($data['total'], 2, '.', ' '),
                    'subTotal' => number_format($data['itemsTotal'], 2, '.', ' '),
                    'minOrder' => number_format($data['minOrder'], 2, '.',  ' '),
                    'isValid' => $data['itemsTotal'] >= $data['minOrder']
                ];
            }
        }

        $optimShipping = [];

        foreach ($ret['optim']['suppliers'] as $data) {
            $optimShipping[] = [
                'supplier' => $data['supplier'],
                'defaultShipping' => $data['shippingCost'],
                'shippingCost' => $data['itemsTotal'] < $data['freePort'] ? $data['shippingCost'] : 0,
                'total' => number_format($data['total'], 2, '.', ' '),
                'subTotal' => number_format($data['itemsTotal'], 2, '.', ' '),
                'minOrder' => number_format($data['minOrder'], 2, '.',  ' '),
                'isValid' => $data['itemsTotal'] >= $data['minOrder']
            ];
        }

        $ret['time'] = $optimum['time'];
        $ret['report'] = $optimum['report'];

        return [
            'ok' => true,
            'data' => $ret,
            'originalTotal' => number_format($originalTotal, 2, '.', ' '),
            'optimTotal' => number_format($optimum['total'], 2, '.', ' '),
            'shipping' => [
                'original' => $originalShipping,
                'optim' => $optimShipping
            ]
        ];
    }

    /**
     * Transforms a value from the transformed representation to its original
     * representation.
     *
     * This method is called when {@link Form::submit()} is called to transform the requests tainted data
     * into an acceptable format for your data processing/model layer.
     *
     * This method must be able to deal with empty values. Usually this will
     * be an empty string, but depending on your implementation other empty
     * values are possible as well (such as NULL). The reasoning behind
     * this is that value transformers must be chainable. If the
     * reverseTransform() method of the first value transformer outputs an
     * empty string, the second value transformer must be able to process that
     * value.
     *
     * By convention, reverseTransform() should return NULL if an empty string
     * is passed.
     *
     * @param mixed $value The value in the transformed representation
     *
     * @return ShoppingCart[] The value in the original representation
     *
     * @throws TransformationFailedException when the transformation fails
     */
    public function reverseTransform($value)
    {
        /** @var ShoppingCart[] $shoppingCarts */
        $shoppingCarts = [];

        foreach ($value['data']['optim']['suppliers'] as $supplierId => $supplierData) {
            $shoppingCart = new ShoppingCart();

            /** @noinspection PhpParamsInspection */
            $shoppingCart->setSupplier($this->supplierRepository->find($supplierId));

            $shoppingCarts[$supplierData['supplier']] = $shoppingCart;
        }

        foreach ($value['data']['optim']['products'] as $productArray) {
            foreach ($productArray as $product) {
                $shoppingCartItem = new ShoppingCartItem();

                /** @noinspection PhpParamsInspection */
                $shoppingCartItem->setProductVariant($this->productVariantRepository->find($product['id']));
                $shoppingCartItem->setQuantity($product['quantity']);
                $shoppingCartItem->setUnit($product['unit']);
                $shoppingCarts[$product['supplier']]->addItem($shoppingCartItem);
            }
        }

        return $shoppingCarts;
    }

    private function getFullVariantName(ProductVariant $variant)
    {
        $variantName = $variant->getFullDisplayName();
        $explicitContent = $variant->getExplicitContent();
        $origin = $variant->getDisplayOrigin();

        if (!empty($explicitContent)) {
            $variantName .= " - $explicitContent";
        }

        if (!empty($origin)) {
            $variantName .= " - $origin";
        }

        return $variantName;
    }
}