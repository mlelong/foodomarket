<?php

namespace AppBundle\Services\Arbitration\DataTransformer;

use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShoppingCart;

class ShoppingCartOptimData
{
    /** @var Restaurant|null */
    private $restaurant;

    /** @var int */
    private $maxDeliveries;

    /** @var ShoppingCart[] */
    private $shoppingCarts = [];

    /** @var int[] */
    private $suppliers = [];

    private $algo;

    public function __construct($algo, $restaurant = null, $maxDeliveries = 1, array $shoppingCarts = [], array $suppliers = [])
    {
        $this->algo = $algo;
        $this->restaurant = $restaurant;
        $this->maxDeliveries = $maxDeliveries;
        $this->shoppingCarts = $shoppingCarts;
        $this->suppliers = $suppliers;
    }

    /**
     * @param Restaurant|null $restaurant
     * @return ShoppingCartOptimData
     */
    public function setRestaurant(?Restaurant $restaurant): ShoppingCartOptimData
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * @param int $maxDeliveries
     * @return ShoppingCartOptimData
     */
    public function setMaxDeliveries(int $maxDeliveries): ShoppingCartOptimData
    {
        $this->maxDeliveries = $maxDeliveries;

        return $this;
    }

    /**
     * @param ShoppingCart[] $shoppingCarts
     * @return ShoppingCartOptimData
     */
    public function setShoppingCarts(array $shoppingCarts): ShoppingCartOptimData
    {
        $this->shoppingCarts = $shoppingCarts;

        return $this;
    }

    /**
     * @param ShoppingCart $shoppingCart
     * @return ShoppingCartOptimData
     */
    public function addShoppingCart(ShoppingCart $shoppingCart): ShoppingCartOptimData
    {
        $found = false;

        foreach ($this->shoppingCarts as $_shoppingCart) {
            if ($shoppingCart->getId() === $_shoppingCart->getId()) {
                $found = true;
                break ;
            }
        }

        if (!$found) {
            $this->shoppingCarts[] = $shoppingCart;
        }

        return $this;
    }

    /**
     * @return Restaurant|null
     */
    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    /**
     * @return int
     */
    public function getMaxDeliveries(): int
    {
        return $this->maxDeliveries;
    }

    /**
     * @return ShoppingCart[]
     */
    public function getShoppingCarts(): array
    {
        return $this->shoppingCarts;
    }

    /**
     * @return int[]
     */
    public function getSuppliers(): array
    {
        return $this->suppliers;
    }

    /**
     * @param int[] $suppliers
     */
    public function setSuppliers(array $suppliers): void
    {
        $this->suppliers = $suppliers;
    }

    /**
     * @return mixed
     */
    public function getAlgo()
    {
        return $this->algo;
    }

    /**
     * @param mixed $algo
     */
    public function setAlgo($algo): void
    {
        $this->algo = $algo;
    }
}