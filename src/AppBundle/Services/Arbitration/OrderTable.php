<?php

namespace AppBundle\Services\Arbitration;

class OrderTable
{
    /** @var int */
    private $nbSupplier;

    /** @var int */
    private $nbProduct;

    /** @var float[][] */
    private $prices = [];

    /**
     * [nbFrn][nbPdt]
     *
     * Liste des produits ordonnés selon le prix par fournisseur
     *
     * @var int[][]
     */
    private $productsOrderedBySupplierPrice = [];

    /**
     * [nbPdt][nbFrn]
     *
     * Liste des fournisseurs ordonnés selon le prix par produit
     *
     * @var int[][]
     */
    private $suppliersOrderedByProductPrice = [];

    /**
     * [nbFrn][nbPdt*(nbFrn-1)][2]
     *
     * Liste des différences ordonnées pour chaque fournisseur,
     * avec comme première coordonnée le fournisseur et ensuite le produit associé à l'écart
     *
     * @var int[][][]
     */
    private $differencesOrderedBySupplier = [];

    /**
     * OrderTable constructor.
     * @param float[][] $prices
     * @throws \Exception
     */
    public function __construct(array $prices)
    {
        $this->prices = $prices;
        $this->nbSupplier = count($prices);
        $this->nbProduct = count($prices[0]);

        $this->sort();

        if (!$this->checkOrderTable()) {
            throw new \Exception("Prices and/or price differences table is not well sorted !");
        }
    }

    private function sort()
    {
        $supplierIndex = [];
        $productIndex = [];

        for ($i = 0; $i < $this->nbSupplier; ++$i) {
            $supplierIndex[$i] = $i;
        }

        for ($i = 0; $i < $this->nbProduct; ++$i) {
            $productIndex[$i] = $i;
        }

        for ($i = 0; $i < $this->nbSupplier; ++$i) {
            $this->suppliersOrderedByProductPrice[$i] = $productIndex;

            usort($this->suppliersOrderedByProductPrice[$i], function(int $index1, int $index2) use($i) {
                $a = $this->prices[$i][$index1];
                $b = $this->prices[$i][$index2];

                return self::compare($a, $b);
            });
        }

        for ($i = 0; $i < $this->nbProduct; ++$i) {
            $this->productsOrderedBySupplierPrice[$i] = $supplierIndex;

            usort($this->productsOrderedBySupplierPrice[$i], function(int $index1, int $index2) use($i) {
                $a = $this->prices[$index1][$i];
                $b = $this->prices[$index2][$i];

                return self::compare($a, $b);
            });
        }

        for ($i = 0; $i < $this->nbSupplier; ++$i) {
            $index = 0;

            for ($j = 0; $j < $this->nbSupplier; ++$j) {
                if ($j != $i || $this->nbSupplier == 1) {
                    for ($k = 0; $k < $this->nbProduct; ++$k) {
                        $this->differencesOrderedBySupplier[$i][$index][0] = $j;
                        $this->differencesOrderedBySupplier[$i][$index][1] = $k;
                        ++$index;
                    }
                }
            }

            usort(
                $this->differencesOrderedBySupplier[$i],
                /**
                 * @param int[] $ij1
                 * @param int[] $ij2
                 * @return int
                 */
                function(array $ij1, array $ij2) use($i) {
                    if ($this->prices[$i][$ij1[1]] != PHP_FLOAT_MAX &&
                        $this->prices[$ij1[0]][$ij1[1]] != PHP_FLOAT_MAX &&
                        $this->prices[$i][$ij2[1]] != PHP_FLOAT_MAX &&
                        $this->prices[$ij2[0]][$ij2[1]] != PHP_FLOAT_MAX
                    ) {
                        $a = $this->prices[$ij1[0]][$ij1[1]] - $this->prices[$i][$ij1[1]];
                        $b = $this->prices[$ij2[0]][$ij2[1]] - $this->prices[$i][$ij2[1]];

                        return self::compare($a, $b);
                    } else {
                        if ($this->prices[$i][$ij1[1]] != PHP_FLOAT_MAX && $this->prices[$ij1[0]][$ij1[1]] != PHP_FLOAT_MAX) {
                            return -1;
                        } elseif ($this->prices[$i][$ij2[1]] != PHP_FLOAT_MAX && $this->prices[$ij2[0]][$ij2[1]] != PHP_FLOAT_MAX) {
                            return 1;
                        }

                        return 0;
                    }
                }
            );
        }
    }

    /**
     * @param int|float $a
     * @param int|float $b
     * @return int
     */
    private static function compare($a, $b)
    {
        if ($a < $b) {
            return -1;
        } elseif ($a > $b) {
            return 1;
        }

        return 0;
    }

    private function checkOrderTable()
    {
        $b = true;
        $i = 0;

        while ($i < $this->nbSupplier && $b) {
            $j = 0;

            while ($j < $this->nbProduct - 1 && $b) {
                $b = $b && $this->prices[$i][$this->suppliersOrderedByProductPrice[$i][$j]] <= $this->prices[$i][$this->suppliersOrderedByProductPrice[$i][$j + 1]];
                ++$j;
            }

            ++$i;
        }

        $i = 0;

        while ($i < $this->nbProduct && $b) {
            $j = 0;

            while ($j < $this->nbSupplier - 1 && $b) {
                $b = $b && $this->prices[$this->productsOrderedBySupplierPrice[$i][$j]][$i] <= $this->prices[$this->productsOrderedBySupplierPrice[$i][$j + 1]][$i];
                ++$j;
            }

            ++$i;
        }

        $i = 0;

        while ($i < $this->nbSupplier && $b) {
            $j = 1;
            $inf = false;

            while ($j < $this->nbProduct * ($this->nbSupplier - 1) && $b) {
                if (!$inf && (
                    $this->prices[$this->differencesOrderedBySupplier[$i][$j][0]][$this->differencesOrderedBySupplier[$i][$j][1]] == PHP_FLOAT_MAX ||
                    $this->prices[$i][$this->differencesOrderedBySupplier[$i][$j][1]] == PHP_FLOAT_MAX
                )) {
                    $inf = true;
                } elseif ($inf) {
                    $b = $b && (
                        $this->prices[$this->differencesOrderedBySupplier[$i][$j][0]][$this->differencesOrderedBySupplier[$i][$j][1]] == PHP_FLOAT_MAX ||
                        $this->prices[$i][$this->differencesOrderedBySupplier[$i][$j][1]] == PHP_FLOAT_MAX
                    );
                } else {
                    $b = $b && (
                        $this->prices[$this->differencesOrderedBySupplier[$i][$j - 1][0]][$this->differencesOrderedBySupplier[$i][$j - 1][1]] -
                        $this->prices[$i][$this->differencesOrderedBySupplier[$i][$j - 1][1]]
                    ) <= (
                        $this->prices[$this->differencesOrderedBySupplier[$i][$j][0]][$this->differencesOrderedBySupplier[$i][$j][1]] -
                        $this->prices[$i][$this->differencesOrderedBySupplier[$i][$j][1]]
                    );
                }

                ++$j;
            }

            ++$i;
        }

        return $b;
    }

    /**
     * @return int[][]
     */
    public function getProductsOrderedBySupplierPrice(): array
    {
        return $this->productsOrderedBySupplierPrice;
    }

    /**
     * @return int[][]
     */
    public function getSuppliersOrderedByProductPrice(): array
    {
        return $this->suppliersOrderedByProductPrice;
    }

    /**
     * @return int[][][]
     */
    public function getDifferencesOrderedBySupplier(): array
    {
        return $this->differencesOrderedBySupplier;
    }
}