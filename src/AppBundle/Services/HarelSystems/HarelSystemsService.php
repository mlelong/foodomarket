<?php

namespace AppBundle\Services\HarelSystems;

use AppBundle\Entity\Order;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\SupplierAccountLogRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class HarelSystemsService
{
    /** @var Client */
    private $client;

    /** @var Configuration */
    private $configuration;

    /** @var string */
    private $host;

    /** @var string */
    private $token;

    /**
     * Enlève les accents et autres caractères non utf-8 d'une chaîne de caractère
     *
     * @param $string
     * @return bool|false|string
     */
    private static function asciiTranslit($string)
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    }

    public function __construct(
        array $harelSystemsConfiguration
        )
    {
        $this->client = new Client([
            'timeout'  => 30.0,
        ]);

        // Harel systems API config
        $this->configuration = $harelSystemsConfiguration;
    }

    public function setSupplier($supplierId) {
        $this->host = $this->configuration[$supplierId]['host'];
        $this->token = $this->configuration[$supplierId]['token'];
    }

    /**
     * @param array $parameters
     * @return string
     */
    private function getUriStringParameters(array $parameters)
    {
        $params = [];

        foreach ($parameters as $key => $value) {
            $params[] = "$key=" . urlencode(self::asciiTranslit($value));
        }

        return implode('&', $params);
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function identifyCustomer($name = '', $fullname = '', $code = '', $phone = '', $email = '')
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $uriParams = $this->getUriStringParameters([
            'name'          => $name,
            'email'         => $email,
            'fullName'      => $fullname,
            'phoneNumber'   => $phone,
            'code'          => $code,
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/customers/identify",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function searchCustomer($name = '', $code = '')
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $data = '';
        for($i = 0; $i < 2000; $i = $i+100) {
            $results = $this->getCustomers($i, 100);

            foreach($results as $result) {
                $customerInfos = $this->getCustomer($result['id']);

                $customerInfos['delivery_address']['street'] = array_key_exists('street', $customerInfos['delivery_address'])?trim($customerInfos['delivery_address']['street']):'';
                $customerInfos['delivery_address']['zip_code'] = array_key_exists('zip_code', $customerInfos['delivery_address'])?trim($customerInfos['delivery_address']['zip_code']):'';
                $customerInfos['delivery_address']['city'] = array_key_exists('city', $customerInfos['delivery_address'])?trim($customerInfos['delivery_address']['city']):'';
                $data .= $result['id'].";".trim($result['name']).";".$customerInfos['delivery_address']['street'].";".$customerInfos['delivery_address']['zip_code'].";".trim($customerInfos['delivery_address']['city']).";"."\n";
            }
        }

        dump($data);

    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getCustomers($offset = 0, $limit = 100)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $uriParams = $this->getUriStringParameters([
            'limit' => $limit,
            'offset' => $offset
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/customers?$uriParams",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getCustomer($id)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/customers/$id",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getPackagings($offset = 0, $limit = 100)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $uriParams = $this->getUriStringParameters([
            'limit' => $limit,
            'offset' => $offset
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/packagings?$uriParams",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getPackaging($id)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/packagings/$id",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getPrices($customerId, $productId = null)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $url = "$this->host/prices/$customerId";

        if (!empty($productId)) {
            $url .= "/product/$productId";
        }

        try {
            $response = $this->client->request(
                'GET', $url
                ,
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function identifyProduct($name = '', $code = '')
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $uriParams = $this->getUriStringParameters([
//            'label'          => $name,
            'code'          => $code,
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/products/identify$uriParams",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function searchProduct($name = '', $code = '')
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $data = '';
        for($i = 0; $i < 3000; $i = $i+100) {
            $results = $this->getProducts($i, 100);

            if (count($results) == 0) {
                continue;
            }

            foreach($results as $result) {
                $pricesInfos = $this->getPrices(912, $result['id']);

                $pricesDetails = '';
                foreach($pricesInfos as $price) {
                    $pricesDetails .= $price['packaging']['id'].';'.$price['packaging']['name'].';'.$price['price'].';';
                }
                $data .= $result['id'].";".$result['code'].";".trim($result['label']).";".$pricesDetails."\n";
            }
        }

        dump($data);

    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function exportProduct($name = '', $code = '')
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $data = "product-id;product-code;product-label;packaging-id;packaging-label;unit-factor;unit;price;\n";

        for($i = 0; $i < 4000; $i = $i+100) {
            $results = $this->getProducts($i, 100);

            if (count($results) == 0) {
                continue;
            }

            foreach($results as $result) {
                $pricesInfos = $this->getPrices(912, $result['id']);
                $productInfos = $result['id'].";".$result['code'].";".trim($result['label']).";";

                foreach($pricesInfos as $price) {
                    $pricesDetails = $price['packaging']['id'].';'.$price['packaging']['name'].';'.$price['packaging']['unit_factor'].';'.$price['packaging']['unit'].';'.$price['price'].';';
                    $data .= $productInfos.$pricesDetails."\n";
                }
            }
        }

        dump($data);

    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getProducts($offset = 0, $limit = 100)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $uriParams = $this->getUriStringParameters([
            'limit' => $limit,
            'offset' => $offset
        ]);

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/products?$uriParams",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getProduct($id)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/products/$id",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function getOrdersByCustomer($customerId)
    {
        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        try {
            $response = $this->client->request(
                'GET',
                "$this->host/orders/by_customer/$customerId",
                [RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token]]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function createCustomer(Prospect $prospect)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $url = "$this->host/customers";

        $customer = \GuzzleHttp\json_encode(
            [
                'code' => "{$prospect->getId()}",
                'name' => $prospect->getRestaurantName(),
                'address' =>
                    [
                        'recipient' => $prospect->getRestaurantName(),
                        'street' => $prospect->getAddress(),
                        'details' => '',
                        'zipCode' => is_null($prospect->getZipCode())?'':$prospect->getZipCode(),
                        'city' => is_null($prospect->getCity())?'':$prospect->getCity(),
                        'country' => 'FR',
                    ],
            ]
        );

        try {
            $response = $this->client->request(
                'POST', $url
                ,
                [
                    RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token],
                    'body' => $customer
                ]
            );

            $content = $response->getBody()->getContents();

            return \GuzzleHttp\json_decode($content, true);
        } catch (GuzzleException $e) {
            throw $e;
        }
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public function createOrder(Order $order)
    {

        if (empty($this->host)) {
            return new \Exception('Please provide the supplier first');
        }

        $orderData =
            [
                'customer' => 10,
                'shippingMethod' => 1,
                'invoicingAddress' =>
                    [
                        'recipient' => $order->getShoppingCart()->getRestaurant()->getName(),
                        'street' => $order->getShoppingCart()->getRestaurant()->getStreet(),
                        'details' => '',
                        'zipCode' => $order->getShoppingCart()->getRestaurant()->getPostcode(),
                        'city' => $order->getShoppingCart()->getRestaurant()->getCity(),
                        'country' => $order->getShoppingCart()->getRestaurant()->getCountryCode(),
                    ],
                'deliveryAddress' =>
                    [
                        'recipient' => $order->getShoppingCart()->getRestaurant()->getName(),
                        'street' => $order->getShoppingCart()->getRestaurant()->getStreet(),
                        'details' => '',
                        'zipCode' => $order->getShoppingCart()->getRestaurant()->getPostcode(),
                        'city' => $order->getShoppingCart()->getRestaurant()->getCity(),
                        'country' => $order->getShoppingCart()->getRestaurant()->getCountryCode(),
                    ],
                "expectedDeliveryDate" => is_null($order->getShoppingCart()->getScheduledDate())?'':$order->getShoppingCart()->getScheduledDate()->format("Y-m-d"),
                "comment" => is_null($order->getShoppingCart()->getNote())?'':$order->getShoppingCart()->getNote(),
            ]
        ;

        foreach($order->getShoppingCart()->getItems() as $item) {
            $orderData['items'][] = [
                                'packaging' => 6, // $item->getOrderItem()->getVariant()->getId(), // to be replaced by packaging id using transformer
                                'orderedQuantity' => $item->getQuantity(),
                                'comment' => is_null($item->getNote())?'':$item->getNote
                            ];
        }

        $orderData = \GuzzleHttp\json_encode($orderData);

        $url = "$this->host/orders";

        try {
            $response = $this->client->request(
                'POST', $url
                ,
                [
                    RequestOptions::HEADERS => ['X-AUTH-TOKEN' => $this->token],
                    'body' => $orderData
                ]
            );

            $content = $response->getBody()->getContents();
            return \GuzzleHttp\json_decode($content, true);

        } catch (GuzzleException $e) {
            throw $e;
        }
    }

}