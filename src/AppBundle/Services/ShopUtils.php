<?php

namespace AppBundle\Services;

use AppBundle\Entity\ProductCarousel;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\CarouselRepository;
use AppBundle\Repository\OrderRepository;
use AppBundle\Repository\ProductCarouselRepository;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductTaxonRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Repository\TaxonRepository;
use Cocur\Slugify\Slugify;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductTaxon;
use Sylius\Component\Core\Model\Taxon;

class ShopUtils
{
    /** @var ProductCarouselRepository */
    private $productCarouselRepository;

    /** @var ProductFilterService */
    private $productFilterService;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var TaxonRepository */
    private $taxonRepository;

    /** @var ProductRepository */
    private $productRepository;

    /** @var ProductTaxonRepository */
    private $productTaxonRepository;

    /** @var OrderRepository */
    private $orderRepository;

    /** @var RedisCacheManager */
    private $cacheManager;

    /** @var CacheManager */
    private $liipImagineCacheManager;
    /**
     * @var CarouselRepository
     */
    private $carouselRepository;

    public function __construct(
        ProductCarouselRepository $productCarouselRepository,
        ProductFilterService $productFilterService,
        SupplierRepository $supplierRepository,
        TaxonRepository $taxonRepository,
        ProductRepository $productRepository,
        ProductTaxonRepository $productTaxonRepository,
        RedisCacheManager $cacheManager,
        CacheManager $liipImagineCacheManager,
        OrderRepository $orderRepository,
        CarouselRepository $carouselRepository
    ) {
        $this->productCarouselRepository = $productCarouselRepository;
        $this->productFilterService = $productFilterService;
        $this->supplierRepository = $supplierRepository;
        $this->taxonRepository = $taxonRepository;
        $this->productRepository = $productRepository;
        $this->productTaxonRepository = $productTaxonRepository;
        $this->cacheManager = $cacheManager;
        $this->liipImagineCacheManager = $liipImagineCacheManager;
        $this->orderRepository = $orderRepository;
        $this->carouselRepository = $carouselRepository;
    }

    /**
     * @param Restaurant|int $restaurant
     */
    public function setRestaurant($restaurant): void
    {
        $this->productFilterService->setRestaurant($restaurant);
    }

    /**
     * @param int $restaurant
     * @return Supplier[]
     */
    private function getPartnersByRestaurant(int $restaurant = 4)
    {
        if ($restaurant == 4) {
            return $this->supplierRepository->getPartners();
        }

        return $this->supplierRepository->getByRestaurant($restaurant);
    }

    /**
     * @param int $restaurant
     * @return Supplier[]
     */
    private function getFacadesByRestaurant(int $restaurant = 4)
    {
        $partners = $this->getPartnersByRestaurant($restaurant);
        $facades = [];

        foreach ($partners as $partner) {
            $facades[] = $partner->getFacadeSupplier();
        }

        return $facades;
    }

    /**
     * @param int $restaurant
     * @return Supplier[]
     */
    private function getPartnersAndFacadesByRestaurant(int $restaurant = 4)
    {
        $partners = $this->getPartnersByRestaurant();

        if ($restaurant === 4) {
            return $partners;
        }

        $facades = [];

        foreach ($partners as $partner) {
            $facades[] = $partner->getFacadeSupplier();
        }

        return array_merge($partners, $facades);
    }

    /**
     * @param int $restaurant
     * @return array
     */
    public function getShopTaxons(int $restaurant = 4)
    {
        $suppliers = $this->getPartnersByRestaurant($restaurant);

        $key = array_reduce($suppliers, function($carry, Supplier $supplier) {
            return empty($carry) ? "{$supplier->getId()}" : "{$carry}_{$supplier->getId()}";
        }, '');

        $taxons = $this->cacheManager->get(
            str_replace('{supplierId}', $key, RedisCacheManager::CACHE_KEY_SHOP_CATALOG_TAXONS_TREE),
            null,
            function() use($suppliers) {
                return $this->taxonRepository->getShopTaxons($suppliers);
            }
        );

        $promos = $this->getCatalogProductsInPromo($restaurant);

        $taxons['children'][] = [
            'text' => 'Promotions',
            'id' => -1,
            'children' => [],
            'products' => count($promos)
        ];

        return $taxons;
    }

    private function getProductInPromotion()
    {
        $partners = $this->supplierRepository->getPartners();
        $qb = $this->productRepository->createQueryBuilder('p');
        return $qb
            ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = p')
            ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.product = spp.product AND spvi.productVariant = spp.productVariant')
            ->where('p.enabled = 1')
            ->andWhere('spp.supplier IN (:suppliers)')
            ->andWhere('spp.restaurant = 4')
            ->andWhere('spp.promo = 1')
            ->setParameter('suppliers', $partners)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return array
     */
    public function getPromos()
    {
        $products = $this->getProductInPromotion();

        return $this->productFilterService->getProductsAndFilters(
            $products,
            null,
            $this->supplierRepository->getPartners(),
            'front_thumbnail'
        );
    }

    /**
     * Retourne les top produits commandés par les particuliers
     *
     * @param int $maxProducts
     * @param string $orderBy
     * @return array|array[]
     */
    public function getTopSellProducts(int $maxProducts, string $orderBy)
    {
        $products = $this->orderRepository->getIndividualTopSalesProducts($maxProducts, $orderBy);

        return $this->productFilterService->getProductsAndFilters(
            $products,
            null,
            $this->supplierRepository->getIndividualsPartners(),
            'front_thumbnail'
        );
    }

    /**
     * @return array
     */
    public function getTopProducts()
    {
        $products = $this->carouselRepository->getProductsForCarousel('home');

//        /** @var ProductCarousel[] $productsCarousel */
//        $productsCarousel = $this->productCarouselRepository->findAll();
//        $products = [];
//
//        foreach ($productsCarousel as $productCarousel) {
//            $product = $productCarousel->getProduct();
//            $products[$product->getId()] = $product;
//        }

        return $this->productFilterService->getProductsAndFilters(
            $products,
            null,
            $this->supplierRepository->getPartners(),
            'front_thumbnail'
        );
    }

    /**
     * @return array
     */
    public function getTopCategories()
    {
        $categories = [];

        /** @var Taxon[] $taxons */
        $taxons = $this->taxonRepository->findBy(['id' => [
            69, 34, 9, 2, 11
        ]]);

        $partnerSuppliers = $this->supplierRepository->getPartners();

        foreach ($taxons as $taxon) {
            $taxonArray = [$taxon->getId()];
            $taxonName = $taxon->getName();

            if ($taxon->getId() == 34) {
                $taxonArray[] = 507;
                $taxonName = 'Boucherie';
            } else if ($taxon->getId() == 9) {
                $taxonArray[] = 10;
                $taxonName = 'Epicerie';
            }

            $images = $taxon->getImagesByType('image');
            $imagePath = $this->liipImagineCacheManager->getBrowserPath(
                $images->isEmpty() ? 'shop-default-product.jpg' : $images->first()->getPath(), 'front_thumbnail'
            );

            $categories[] = [
                'slug' => $taxon->getSlug(),
                'image' => $imagePath,
                'name' => $taxonName,
//                'description' => $taxon->getDescription(),
//                'products' => $this->productTaxonRepository->countProductsForSuppliersAndCategories($partnerSuppliers, ...$taxonArray),
                'offers' => $this->productTaxonRepository->countOffersForSuppliersAndCategories($partnerSuppliers, ...$taxonArray),
            ];
        }

        usort($categories, function($a, $b) {
            return $a['offers'] < $b['offers'];
        });

        return $categories;
    }

    /**
     * @param string $slug
     * @param int $restaurant
     * @param string $liipFilter
     * @return mixed|null
     */
    public function getCatalogProductsByTaxonSlug(string $slug, int $restaurant = 4, string $liipFilter = 'front_thumbnail')
    {
        $taxonSlug = urldecode($slug);
        $taxon = $this->taxonRepository->findOneBySlug($taxonSlug, 'fr_FR');

        if ($taxon === null) {
            return null;
        }

        $taxonId = $taxon->getId();

        $ret = $this->cacheManager->get(
            str_replace(['{restaurantId}', '{taxonId}'], [$restaurant, $taxonId], RedisCacheManager::CACHE_KEY_SHOP_CATALOG_PRODUCTS),
            86400,
            function() use($taxonId, $restaurant, $liipFilter) {
                $taxonArray = [$taxonId];

                /**
                 * Below condition should be unnecessary if we change the taxon tree a bit to add global categories for
                 * - butchery and delicatessen (respectively id 34 and 507)
                 * - salted and sweet groceries (respectively id 9 and 10)
                 */
                if ($taxonId == 34) {
                    $taxonArray[] = 507;
                } else if($taxonId == 9) {
                    $taxonArray[] = 10;
                } else if ($taxonId == 507) {
                    $taxonArray[] = 34;
                } else if ($taxonId == 10) {
                    $taxonArray[] = 9;
                } else if ($taxonId == 1) {
                    $taxonArray = [69, 34, 507, 9, 10, 503, 2, 11];
                }

                /** @var Taxon $mainTaxon */
                $mainTaxon = $this->taxonRepository->find($taxonId);
                $taxon = $mainTaxon;

                $taxons = [
                    [
                        'id' => $taxon->getId(),
                        'name' => $taxon->getName(),
                        'slug' => $taxon->getSlug()
                    ]
                ];

                while ($taxon = $taxon->getParent()) {
                    $taxons[] = [
                        'id' => $taxon->getId(),
                        'name' => $taxon->getName(),
                        'slug' => $taxon->getSlug()
                    ];
                }

                $products = $this->getProductsByTaxons($taxonArray, $restaurant);
//                $ret = $this->productFilterService->getProductsAndFilters($products, $mainTaxon, $this->getPartnersByRestaurant($restaurant));
                $ret = $this->productFilterService->getProductsAndFilters($products, $mainTaxon, $this->getPartnersAndFacadesByRestaurant($restaurant), $liipFilter);

                $ret['taxons'] = array_reverse($taxons);

                return $ret;
            }
        );

        $taxon = $ret['taxons'][count($ret['taxons']) - 1];
        /** @var Taxon $taxon */
        $taxon = $this->taxonRepository->find($taxon['id']);
        $ret['taxon'] = $this->normalizeName($taxon->getName());
        $ret['childTaxons'] = [];

        foreach ($taxon->getChildren() as $child) {
            $products = $this->productTaxonRepository->countProductsForSuppliersAndCategories(
                $this->getPartnersByRestaurant($restaurant),
                $child
            );

            if ($products > 0) {
                $ret['childTaxons'][] = [
                    'id' => $child->getId(),
                    'name' => $this->normalizeName($child->getName()),
                    'slug' => $child->getSlug()
                ];
            }
        }

        return $ret;
    }

    private function normalizeName(string $name)
    {
        return mb_convert_case($name, MB_CASE_TITLE);
    }

    /**
     * @param string $terms
     * @param int $restaurant
     * @return mixed|null
     */
    public function getCatalogProductsByTerms(string $terms, int $restaurant = 4)
    {
        $ret = $this->cacheManager->get(
            str_replace('{terms}', (new Slugify())->slugify($terms) . "-$restaurant", RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SEARCH_PRODUCTS),
            86400,
            function() use($terms, $restaurant) {
                $products = $this->getProductsByTerms($terms, $restaurant);
//                $ret = $this->productFilterService->getProductsAndFilters($products, null, $this->getPartnersByRestaurant($restaurant));
                $ret = $this->productFilterService->getProductsAndFilters($products, null, $this->getPartnersAndFacadesByRestaurant($restaurant));

                return $ret;
            }
        );

        return $ret;
    }

    /**
     * @param int[] $variantIds
     * @param int $restaurant
     * @return array
     */
    public function getCatalogProductsByVariantIds($variantIds, int $restaurant = 4)
    {
        $products = $this->productRepository
            ->createQueryBuilder('p')
            ->join('p.variants', 'v', 'WITH', 'v.id IN (:variantIds)')
            ->setParameter('variantIds', $variantIds)
            ->getQuery()
            ->getResult()
        ;
//        $ret = $this->productFilterService->getProductsAndFilters($products, null, $this->getPartnersByRestaurant($restaurant));
        $ret = $this->productFilterService->getProductsAndFilters($products, null, $this->getPartnersAndFacadesByRestaurant($restaurant));

        return $ret;
    }

    /**
     * @param Taxon[]|int[] $taxons
     * @param int $restaurant
     * @return Product[]
     */
    public function getProductsByTaxons($taxons, int $restaurant = 4) {
        $qb = $this->productRepository->createQueryBuilder('p');

        return $qb
            ->innerJoin('p.translations', 't', 'WITH', 't.locale = :locale')
            ->join(ProductTaxon::class, 'pt', 'WITH', 'pt.product = p AND pt.taxon IN (:taxons)')
            ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = p')
            ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.supplier = spp.supplier AND spvi.product = spp.product AND spvi.productVariant = spp.productVariant AND spvi.manuallyCreated = 0')
            ->join('spp.productVariant', 'v')
            ->join('spp.supplier', 's')
            ->where('p.enabled = 1')
            ->andWhere('v.enabled = 1')
            ->andWhere('p.mainTaxon IS NOT NULL')
//            ->andWhere('s IN (:suppliers)')
            ->andWhere($qb->expr()->orX('s IN (:partners)', 's IN (:facades)'))
            ->andWhere('spp.restaurant = 4')
            ->orderBy('t.name', 'ASC')
            ->setParameter('taxons', $taxons)
            ->setParameter('locale', 'fr_FR')
//            ->setParameter('suppliers', $this->getPartnersByRestaurant($restaurant))
            ->setParameter('partners', $this->getPartnersByRestaurant($restaurant))
            ->setParameter('facades', $this->getFacadesByRestaurant($restaurant))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string $terms
     * @param int $restaurant
     * @return Product[]
     */
    public function getProductsByTerms(string $terms, int $restaurant = 4) {
        $terms = $this->parseTerms($terms);
        $qb = $this->productRepository->createQueryBuilder('o');

        return $qb
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->join(ProductTaxon::class, 'pt', 'WITH', 'pt.product = o AND pt.taxon NOT IN (:taxons)')
            ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = o')
            ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.supplier = spp.supplier AND spvi.product = spp.product AND spvi.productVariant = spp.productVariant AND spvi.manuallyCreated = 0')
            ->join('spp.productVariant', 'v')
            ->join('spp.supplier', 's')
            ->andWhere('o.mainTaxon IS NOT NULL')
            ->andWhere('o.enabled = 1')
            ->andWhere('v.enabled = 1')
//            ->andWhere('s IN (:suppliers)')
            ->andWhere($qb->expr()->orX('s IN (:partners)', 's IN (:facades)'))
            ->andWhere('spp.restaurant = 4')
            ->andWhere('translation.name LIKE :name')
            ->orderBy('translation.name', 'ASC')
            ->setParameter('name', '%' . trim($terms) . '%')
            ->setParameter('locale', 'fr_FR')
            ->setParameter('taxons', [296])
//            ->setParameter('suppliers', $this->getPartnersByRestaurant($restaurant))
            ->setParameter('partners', $this->getPartnersByRestaurant($restaurant))
            ->setParameter('facades', $this->getFacadesByRestaurant($restaurant))
            ->getQuery()
            ->getResult()
        ;
    }

    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    private function parseTerms($terms)
    {
        if (stripos($terms, ',') !== false) {
            $multipleterms = explode(',', $terms);
            $terms = $multipleterms[0];
        }

        $terms = explode(' ', $terms);

        foreach ($terms as &$term) {
            if (strlen($term) >= 3 && $this->endsWith($term, 's')) {
                $term = substr_replace($term, '', -1);
            }
        }

        $terms = implode('%', $terms);

        return $terms;
    }

    /**
     * @param int $restaurant
     * @return array
     */
    public function getProductsSlugs(int $restaurant = 4)
    {
        $suppliers = $this->getPartnersByRestaurant($restaurant);

        $key = array_reduce($suppliers, function($carry, Supplier $supplier) {
            return empty($carry) ? "{$supplier->getId()}" : "{$carry}_{$supplier->getId()}";
        }, '');

        return $this->cacheManager->get(str_replace('{supplierId}', $key, RedisCacheManager::CACHE_KEY_SHOP_CATALOG_PRODUCTS_SLUGS), null, function() use($suppliers) {
            /** @var Product[] $products */
            $products = $this->productRepository
                ->createQueryBuilder('p')
                ->join('p.translations', 'pt')
                ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = p AND spp.supplier IN (:suppliers)')
                ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.product = spp.product AND spvi.productVariant = spp.productVariant AND spvi.supplier = spp.supplier AND spvi.manuallyCreated = 0')
                ->join('spp.productVariant', 'v')
                ->andWhere('p.enabled = 1')
                ->andWhere('v.enabled = 1')
                ->setParameter('suppliers', $suppliers)
                ->getQuery()
                ->getResult()
            ;

            $slugs = [];

            foreach ($products as $product) {
                $slugs[$product->getId()] = $product->getSlug();
            }

            return $slugs;
        });
    }

    /**
     * @param int $restaurant
     * @return array
     */
    public function getTaxonsSlugs(int $restaurant = 4)
    {
        $suppliers = $this->getPartnersByRestaurant($restaurant);

        $key = array_reduce($suppliers, function($carry, Supplier $supplier) {
            return empty($carry) ? "{$supplier->getId()}" : "{$carry}_{$supplier->getId()}";
        }, '');

        return $this->cacheManager->get(str_replace('{supplierId}', $key, RedisCacheManager::CACHE_KEY_SHOP_CATALOG_TAXONS_SLUGS), null, function() use($suppliers) {
            /** @var Taxon[] $taxons */
            $taxons = $this->taxonRepository->createQueryBuilder('t')
                ->join(ProductTaxon::class, 'pt', 'WITH', 'pt.taxon = t')
                ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = pt.product AND spp.supplier IN (:suppliers)')
                ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.product = spp.product AND spvi.productVariant = spp.productVariant AND spvi.supplier = spp.supplier AND spvi.manuallyCreated = 0')
                ->join('spp.product', 'p')
                ->join('spp.productVariant', 'v')
                ->andWhere('p.enabled = 1')
                ->andWhere('v.enabled = 1')
                ->andWhere('t.level > 1')
                ->setParameter('suppliers', $suppliers)
                ->getQuery()
                ->getResult();

            $slugs = [];

            foreach ($taxons as $taxon) {
                $slugs[$taxon->getId()] = $taxon->getSlug();
            }

            return $slugs;
        });
    }

    public function getCatalogProductsInPromo(int $restaurantId = 4)
    {
        $products = $this->getProductInPromotion();
        $ret = $this->productFilterService->getProductsAndFilters($products, null, $this->getPartnersAndFacadesByRestaurant($restaurantId));

        return $ret;
    }
}