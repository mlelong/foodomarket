<?php


namespace AppBundle\Services\Payment\Lyra;


use AppBundle\Entity\LyraAlias;
use AppBundle\Entity\OrderPayment;
use AppBundle\Repository\LyraAliasRepository;
use AppBundle\Repository\OrderPaymentRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Services\OrderService;
use AppBundle\Services\Payment\Lyra\Exceptions\LyraException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class LyraPaymentService
{
    /**
     * @var Client
     */
    private $clientLyra;
    /**
     * @var OrderPaymentRepository
     */
    private $orderPaymentRepository;
    /**
     * @var OrderService
     */
    private $orderService;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var $lyraAliasRepository
     */
    private $lyraAliasRepository;

    private $prospectRepository;

    public function __construct(
        Client $clientLyra,
        OrderPaymentRepository $orderPaymentRepository,
        OrderService $orderService,
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag,
        LyraAliasRepository $lyraAliasRepository,
        ProspectRepository $prospectRepository
    )
    {
        $this->clientLyra = $clientLyra;
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->orderService = $orderService;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->lyraAliasRepository = $lyraAliasRepository;
        $this->prospectRepository = $prospectRepository;
    }

    public
    function handlePaymentWebhook(string $lyraId)
    {
        try {
            $ret = $this->clientLyra->get("orders/$lyraId");
            $transactions = $this->clientLyra->get("orders/$lyraId/transactions");

            /** @var OrderPayment[] $payments */
            $payments = $this->orderPaymentRepository->findBy(['lyraId' => $lyraId]);
            $order = $payments[0]->getOrder();

            $orderPayment = new OrderPayment();

            $orderPayment->setOrder($order);
            $orderPayment->setLyraId($lyraId);
            $orderPayment->setLyraOrderStatus($ret['status']);
            $orderPayment->setLyraOrderData($ret);

            if (isset($transactions['results']) && !empty($transactions['results'])) {
                // Si le statut n'a pas bougé, on ne fait rien
                if ($transactions['results'][0]['status'] === $payments[count($payments) - 1]->getLyraPaymentStatus()) {
                    return true;
                }

                $orderPayment->setLyraPaymentStatus($transactions['results'][0]['status']);
                $orderPayment->setLyraPaymentData($transactions['results'][0]);

                $order->setPaymentState($orderPayment->getLyraPaymentStatus());
            } else {
                return false;
//                $orderPayment->setLyraPaymentStatus($orderPayment->getLyraOrderStatus());
//                $orderPayment->setLyraPaymentData([]);
//
//                $order->setPaymentState($orderPayment->getLyraOrderStatus());
            }

            $this->orderPaymentRepository->add($orderPayment);

            switch ($orderPayment->getLyraOrderStatus()) {
                case 'PENDING':
                    $order->setState('pending');
                    $this->orderService->postCreateOrder($order);
                    break;
                default:
                    $order->setState(strtolower($orderPayment->getLyraOrderStatus()));
                    break;
            }

            $this->entityManager->flush();
        } catch (LyraException $e) {
            return false;
        }

        return true;
    }

    public
    function handleTokenWebhook($tokeUuid)
    {
        try {
            $tokenData = $this->clientLyra->get("tokens/{$tokeUuid}");

            if (!isset($tokenData['alias'])) {
                return false;
            }

            //Getting the associated prospect
            $prospectId = substr($tokenData['buyer']['reference'], 1);
            $prospect = $this->prospectRepository->find($prospectId);

            if (!$prospect) {
                return false;
            }

            //Getting lyra alias details
            $aliasData = $this->clientLyra->get("marketplaces/{$this->parameterBag->get('lyra.marketplace')}/alias/{$tokenData['alias']}");

            //Checking if we already have an alias with the same uuid
            $lyraAlias = $this->lyraAliasRepository->findOneBy(['lyraAliasUuid' => $tokenData['alias']]);

            if (!$lyraAlias) {
                $lyraAlias = new LyraAlias();
            }

            $lyraAlias->setLyraAliasUuid($tokenData['alias']);
            $lyraAlias->setLyraToken($tokenData['uuid']);
            $lyraAlias->setNumber($aliasData['number']);
            $lyraAlias->setBrand($aliasData['brand']);
            $lyraAlias->setProspect($prospect);

            $expirationDate = new DateTime();
            //@TODO: find the number of the last day of the month
            $expirationDate->setDate($aliasData['expiry_year'], $aliasData['expiry_month'], 28);
            $lyraAlias->setExpirationDate($expirationDate);

            $this->lyraAliasRepository->add($lyraAlias);

        } catch (LyraException $e) {
            return false;
        }

        return true;
    }
}
