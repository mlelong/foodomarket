<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use PhpImap\Mailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;

class MailboxManager
{
    protected $entityManager;
    protected $tmpDirectory;
    protected $mailbox;

    public function __construct(EntityManager $entityManager, $tmpDirectory)
    {
        $this->entityManager = $entityManager;
        $this->tmpDirectory = $tmpDirectory;
    }

    public function getNewEmails($mailbox, $email, $password)
    {
        $this->mailbox = new Mailbox($mailbox, $email, $password, $this->tmpDirectory);

        $mailsIds = $this->mailbox->searchMailbox('UNSEEN');
        if (!$mailsIds) {
            return false;
        }

        return $mailsIds;
    }

    public function getContent($id)
    {
        $mail = $this->mailbox->getMail($id);

        return [
            'content' => $mail->textPlain,
            'attachments' => $mail->getAttachments(),

        ];
    }

    public function delete($id)
    {
        return $this->mailbox->deleteMail($id);
    }

    public function markAsRead($id)
    {
        $this->mailbox->markMailAsRead($id);
    }

    public function markAsUnread($id)
    {
        $this->mailbox->markMailAsUnread($id);
    }

}
