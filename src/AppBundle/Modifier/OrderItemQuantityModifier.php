<?php

namespace AppBundle\Modifier;

use Sylius\Component\Order\Model\OrderItemInterface;
use Sylius\Component\Order\Modifier\OrderItemQuantityModifierInterface;

final class OrderItemQuantityModifier implements OrderItemQuantityModifierInterface
{
    /**
     * {@inheritdoc}
     */
    public function modify(OrderItemInterface $orderItem, int $targetQuantity): void
    {
        $orderItem->setQuantity($targetQuantity);
    }
}
