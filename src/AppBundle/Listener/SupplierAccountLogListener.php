<?php

namespace AppBundle\Listener;

use AppBundle\Entity\Email;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Helper\CurlHelper;
use AppBundle\Repository\EmailRepository;
use AppBundle\Repository\MercurialeRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierCategoryRepository;
use AppBundle\Services\Hubspot\HubspotService;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\MailerInterface;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Symfony\Component\HttpFoundation\Response;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use function Doctrine\ORM\QueryBuilder;

class SupplierAccountLogListener
{
    /** @var SupplierCategoryRepository */
    private $supplierCategoryRepository;

    /** @var SupplierAccountLogRepository */
    private $supplierAccountLogRepository;

    /** @var EmailRepository */
    private $emailRepository;

    /** @var EmailFactory */
    private $emailFactory;

    /** @var MercurialeRepository */
    private $mercurialeRepository;

    /** @var MailerInterface */
    private $mailer;

    /** @var bool|string */
    private $sendEmails;

    /** @var string */
    private $emailDebug;

    /** @var UploaderHelper */
    private $uploaderHelper;

    /** @var string */
    private $webDir;

    /** @var EntityManager */
    private $entityManager;

    /** @var HubspotService */
    private $hubspotService;

    /**
     * SupplierAccountLogType constructor.
     * @param SupplierCategoryRepository $supplierCategoryRepository ,
     * @param SupplierAccountLogRepository $supplierAccountLogRepository
     * @param EmailRepository $emailRepository
     * @param EmailFactory $emailFactory
     * @param MailerInterface $mailer
     * @param MercurialeRepository $mercurialeRepository
     * @param bool|string $sendEmails
     * @param string $emailDebug
     * @param UploaderHelper $uploaderHelper
     * @param string $webDir
     * @param EntityManager $entityManager
     * @param HubspotService $hubspotService
     */
    public function __construct(
        SupplierCategoryRepository $supplierCategoryRepository,
        SupplierAccountLogRepository $supplierAccountLogRepository,
        EmailRepository $emailRepository,
        EmailFactory $emailFactory,
        MailerInterface $mailer,
        MercurialeRepository $mercurialeRepository,
        $sendEmails,
        string $emailDebug,
        UploaderHelper $uploaderHelper,
        string $webDir,
        EntityManager $entityManager,
        HubspotService $hubspotService)
    {
        $this->supplierCategoryRepository = $supplierCategoryRepository;
        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
        $this->emailRepository = $emailRepository;
        $this->emailFactory = $emailFactory;
        $this->mailer = $mailer;
        $this->sendEmails = $sendEmails;
        $this->emailDebug = $emailDebug;
        $this->uploaderHelper = $uploaderHelper;
        $this->webDir = $webDir;
        $this->mercurialeRepository = $mercurialeRepository;
        $this->entityManager = $entityManager;
        $this->hubspotService = $hubspotService;
    }

    public function onCreateOrUpdate(ResourceControllerEvent $event)
    {
        if (!$this->handle($event->getSubject())) {
            $event->setResponse(new Response('Mercuriale ou Sepa introuvable :/'));
        }
    }

    public function handle(SupplierAccountLog $supplierAccountLog)
    {
        $prospect = $supplierAccountLog->getProspect();

        // Keeps all supplier account log in sync for a supplier and prospect
        $this->supplierAccountLogRepository->updateAllAccordingTo($supplierAccountLog);

        if ($supplierAccountLog->getStatus() === SupplierAccountLog::STATUS_ACCOUNT_OPENED
            || $supplierAccountLog->getStatus() === SupplierAccountLog::STATUS_ORDERING) {

            $prospect->setNewsletterPrices(true);

            // add all supplier_category matching the current supplier_category's supplier
            $qb = $this->supplierCategoryRepository->createQueryBuilder('sc');
            $supplierCategoryToAdd = $qb
                    ->where('sc.supplier = :supplier')
                    ->setParameter('supplier', $supplierAccountLog->getSupplierCategory()->getSupplier())
                    ->getQuery()
                    ->getResult()
            ;

            foreach($supplierCategoryToAdd as $supplierCategory) {
                $prospect->addSupplier($supplierCategory);
            }
        } else {
//            $prospect->removeSupplier($supplierAccountLog->getSupplierCategory());
        }

        try {
            $this->entityManager->persist($prospect);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            return false;
        }

        if ($supplierAccountLog->getStatus() === SupplierAccountLog::STATUS_ACCOUNT_OPENED) {
//            $this->hubspotService->createContactAndAccountOpenedHoppy($prospect);

            $qb = $this->emailRepository->createQueryBuilder('o');

            $isEmailAlreadySent = (int)$qb
                ->select("COUNT(o)")
                ->where("o.templateId = 31")
                ->andWhere($qb->expr()->like('o.to', ':email'))
                ->setParameter('email', "%{$prospect->getEmail()}%")
                ->getQuery()
                ->getSingleScalarResult() > 0
            ;

            if ($isEmailAlreadySent) {
                return true;
            }

            CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/oto6093/', [], [
                'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                'FIRST_NAME' => $prospect->getFirstName(),
                'PHONE' => $prospect->getPhone(),
                'MAIL' => $prospect->getEmail(),
                'DATE' => (new DateTime())->format('c'),
                'ADDRESS' => $prospect->getZipCode()
            ]);

            if ($this->sendEmails === false) {
                return true;
            }

            $emailBuilder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

            $emailBuilder
                ->setTemplateId(31)
                ->addTo($prospect->getEmail(), $prospect->getFirstName())
                ->addVariable('INTERLOCUTOR', $prospect->getFirstName())
                ->addVariable('SUPPLIER', $supplierAccountLog->getSupplierCategory()->getSupplier()->getDisplayName())
                ->addVariable('HOW_TO_ORDER', nl2br($supplierAccountLog->getSupplierCategory()->getSupplier()->getFacadeSupplier()->getHowToOrder()))
                ->addVariable('ORDER_MODALITY', nl2br($supplierAccountLog->getSupplierCategory()->getSupplier()->getFacadeSupplier()->getOrderModality()))
                ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
            ;

            foreach($supplierAccountLog->getProspect()->getSupplementaryEmailsArray() as $suppEmail) {
                $emailBuilder->addCc($suppEmail);
            }

            $this->mailer->addEmailToQueue($emailBuilder->build());
        }

        return true;
    }
}