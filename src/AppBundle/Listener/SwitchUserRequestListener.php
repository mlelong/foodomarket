<?php

namespace AppBundle\Listener;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class SwitchUserRequestListener
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onRequest(GetResponseEvent $event)
    {
        if (stripos($event->getRequest()->attributes->get('_controller'), 'MiddleOffice') !== false
            && !in_array($event->getRequest()->attributes->get('_route'), [
                'app_front_logout',
            ])) {
            $foodoSwitch = $this->session->get('_foodo_switch');

            if ($foodoSwitch) {
                $event->getRequest()->query->set('_foodo_switch', $foodoSwitch);
            }
        }
    }
}
