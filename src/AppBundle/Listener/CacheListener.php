<?php

namespace AppBundle\Listener;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Import;
use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Services\RedisCache;
use AppBundle\Services\RedisCacheManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\ORMException;
use JMS\JobQueueBundle\Entity\Job;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductImage;
use Sylius\Component\Core\Model\ProductTranslation;

/**
 * Class CacheListener
 * @package AppBundle\Listener
 */
class CacheListener
{
    /** @var RedisCache */
    private $redisCache;

    /**
     * Entities that are going to be inserted (they don't have an id in onFlush, so we need to keep track of them for use in postFlush)
     * @var array
     */
    private $insertedEntities = [];

    /**
     * Insertion batch (in the form entityClassName:entityId)
     * @var array
     */
    private $insertions = [];

    /**
     * Update batch (in the form entityClassName:entityId)
     * @var array
     */
    private $updates = [];

    /**
     * Deletion batch (in the form entityClassName:entityId)
     * @var array
     */
    private $deletions = [];

    /**
     * CacheListener constructor.
     * @param RedisCache $redisCache
     */
    public function __construct(RedisCache $redisCache)
    {
        $this->redisCache = $redisCache;
    }

    /**
     * This method handles the post actions following the insertion of (some) entities
     *
     * Entities that are taken into account:
     *
     * {
     * - SupplierProductPrice
     * - ProductOptionValue
     * - ProductImage
     * }
     *
     * @param array $entities
     */
    private function handleInsertions(array $entities)
    {
        foreach ($entities as $entity) {
            if ($entity instanceof SupplierProductPrice) {
                $this->insertedEntities[] = ['class' => SupplierProductPrice::class, 'entity' => $entity];
            } elseif ($entity instanceof ProductOptionValue) {
                $this->redisCache->deleteItem(str_replace('{code}', $entity->getOptionCode(), RedisCacheManager::CACHE_KEY_ADMIN_PRODUCT_OPTION_VALUES));
            } elseif ($entity instanceof ProductImage) {
                $this->insertedEntities[] = ['class' => Product::class, 'entity' => $entity->getOwner()];
            }
        }
    }

    /**
     * This method handles the post actions following the update of (some) entities
     *
     * Entities that are taken into account:
     *
     * {
     * - Import
     * - Prospect si suppliers a bougé
     * - SupplierProductPrice
     * - Product
     * - ProductImage
     * - ProductTranslation
     * - ProductVariant
     * - ProductOptionValue
     * }
     *
     * @param array $entities
     */
    private function handleUpdates(array $entities)
    {
        foreach ($entities as $entity) {
            if ($entity instanceof Import) {
                $this->addToBatch($this->updates, Import::class, $entity->getId());
            } else if ($entity instanceof Prospect) {
                $this->addToBatch($this->updates, Prospect::class, $entity->getId());
            } else if ($entity instanceof SupplierProductPrice) {
                $this->addToBatch($this->updates, SupplierProductPrice::class, $entity->getId());
            } else if ($entity instanceof Product) {
                $this->addToBatch($this->updates, Product::class, $entity->getId());
            } else if ($entity instanceof ProductImage) {
                $this->addToBatch($this->updates, Product::class, $entity->getOwner()->getId());
            } else if ($entity instanceof ProductTranslation) {
                $this->addToBatch($this->updates, Product::class, $entity->getTranslatable()->getId());
            } else if ($entity instanceof ProductVariant) {
                $this->addToBatch($this->updates, ProductVariant::class, $entity->getId());
            } elseif ($entity instanceof ProductOptionValue) {
                $this->redisCache->deleteItem(str_replace('{code}', $entity->getOptionCode(), RedisCacheManager::CACHE_KEY_ADMIN_PRODUCT_OPTION_VALUES));
            }
        }
    }

    /**
     * This method handles the post actions following the deletion of (some) entities
     *
     * Entities that are taken into account:
     *
     * {
     * - SupplierProductPrice
     * - Product
     * - ProductVariant
     * - Restaurant
     * - Prospect
     * - Supplier
     * - ProductOptionValue
     * }
     *
     * @param array $entities
     */
    private function handleDeletions(array $entities)
    {
        foreach ($entities as $entity) {
            if ($entity instanceof SupplierProductPrice) {
                $this->handleSupplierProductPriceDeletion($entity);

//                $this->addToBatch($this->deletions, SupplierProductPrice::class, $entity->getId());
            } else if ($entity instanceof Product) {
                $this->handleProductDeletion($entity);

//                $this->addToBatch($this->deletions, Product::class, $entity->getId());
            } else if ($entity instanceof ProductVariant) {
                $this->handleProductVariantDeletion($entity);

//                $this->addToBatch($this->deletions, ProductVariant::class, $entity->getId());
            } else if ($entity instanceof Restaurant) {
                $this->handleRestaurantDeletion($entity);

//                $this->addToBatch($this->deletions, Restaurant::class, $entity->getId());
            } else if ($entity instanceof Prospect) {
                $this->handleProspectDeletion($entity);

//                $this->addToBatch($this->deletions, Prospect::class, $entity->getId());
            } else if ($entity instanceof Supplier) {
                $this->handleSupplierDeletion($entity);

//                $this->addToBatch($this->deletions, Supplier::class, $entity->getId());
            } elseif ($entity instanceof ProductOptionValue) {
                $this->redisCache->deleteItem(str_replace('{code}', $entity->getOptionCode(), RedisCacheManager::CACHE_KEY_ADMIN_PRODUCT_OPTION_VALUES));
            }
        }
    }

    private function handleProspectDeletion(Prospect $entity)
    {
        $entity->getCustomers()->forAll(function($key, Customer $customer) {
            $customer->getRestaurants()->forAll(function($key2, Restaurant $restaurant) {
                $this->handleRestaurantDeletion($restaurant);

                return true;
            });

            return true;
        });
    }

    private function handleSupplierDeletion(Supplier $entity)
    {
        $this->redisCache->removeRestaurantSupplierProductsCache($entity->getId());
        $this->redisCache->removeRestaurantProductsCache();
        $this->redisCache->removeRestaurantShoppingListsCache();
        $this->redisCache->removeRestaurantSupplierVariantCache('*', $entity->getId(), '*');

        $this->redisCache->removeRestaurantCatalogProductsCache();
        $this->redisCache->removeSupplierCatalogProductsCache($entity->getId());
        $this->redisCache->removeCatalogSupplierProductCache("*{$entity->getId()}*");
        $this->redisCache->removeCatalogSearchCache();
    }

    private function handleProductVariantDeletion(ProductVariant $entity)
    {
        $this->redisCache->removeRestaurantSupplierProductsCache();
        $this->redisCache->removeRestaurantProductsCache();
        $this->redisCache->removeRestaurantShoppingListsCache();
        $this->redisCache->removeRestaurantSupplierVariantCache('*', '*', $entity->getId());

        $this->redisCache->removeRestaurantCatalogProductsCache();
        $this->redisCache->removeSupplierCatalogProductsCache();
        $this->redisCache->removeCatalogProductCache($entity->getProduct()->getId());
        $this->redisCache->removeCatalogSupplierProductCache('*', $entity->getProduct()->getId());
        $this->redisCache->removeCatalogSearchCache();
    }

    private function handleProductDeletion(Product $entity)
    {
        $this->redisCache->removeRestaurantSupplierProductsCache();
        $this->redisCache->removeRestaurantProductsCache();
        $this->redisCache->removeRestaurantShoppingListsCache();

        $entity->getVariants()->forAll(function($key, ProductVariant $variant) {
            $this->redisCache->removeRestaurantSupplierVariantCache('*', '*', $variant->getId());

            return true;
        });

        $this->redisCache->removeRestaurantCatalogProductsCache();
        $this->redisCache->removeSupplierCatalogProductsCache();
        $this->redisCache->removeCatalogProductCache($entity->getId());
        $this->redisCache->removeCatalogSupplierProductCache('*', $entity->getId());
        $this->redisCache->removeCatalogSearchCache();
    }

    private function handleSupplierProductPriceDeletion(SupplierProductPrice $entity)
    {
        $this->redisCache->removeRestaurantSupplierProductsCache($entity->getSupplier()->getId(), $entity->getRestaurant()->getId());
        $this->redisCache->removeRestaurantProductsCache($entity->getRestaurant()->getId());
        $this->redisCache->removeRestaurantShoppingListsCache($entity->getRestaurant()->getId());

        $this->redisCache->removeRestaurantSupplierVariantCache($entity->getRestaurant()->getId(), $entity->getSupplier()->getId(), $entity->getProductVariant()->getId());

        $this->redisCache->removeRestaurantCatalogProductsCache($entity->getRestaurant()->getId());
        $this->redisCache->removeSupplierCatalogProductsCache($entity->getSupplier()->getId());
        $this->redisCache->removeCatalogProductCache($entity->getProduct()->getId());
        $this->redisCache->removeCatalogSupplierProductCache('*', $entity->getProduct()->getId());
        $this->redisCache->removeCatalogSearchCache();
    }

    private function handleRestaurantDeletion(Restaurant $entity)
    {
        $this->redisCache->removeRestaurantSuppliers($entity->getId());
        $this->redisCache->removeRestaurantSupplierProductsCache('*', $entity->getId());
        $this->redisCache->removeRestaurantProductsCache($entity->getId());
        $this->redisCache->removeRestaurantShoppingListsCache($entity->getId());
        $this->redisCache->removeRestaurantSupplierVariantCache($entity->getId());
        $this->redisCache->removeRestaurantCatalogProductsCache($entity->getId());
    }

    /**
     * Doctrine event, called automatically by Doctrine during onFlush lifecycle
     *
     * This is where we compute the actions to do depending on our use case
     *
     * (@see handleInsertions, handleUpdates, handleDeletions for business logic details)
     *
     * @param OnFlushEventArgs $eventArgs
     */
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $unitOfWork = $eventArgs->getEntityManager()->getUnitOfWork();

        $this->handleInsertions($unitOfWork->getScheduledEntityInsertions());
        $this->handleUpdates($unitOfWork->getScheduledEntityUpdates());
        $this->handleDeletions($unitOfWork->getScheduledEntityDeletions());
    }

    /**
     * Doctrine event, called automatically by Doctrine during postFlush lifecycle
     *
     * This is where we execute the actions we computed during onFlush
     *
     * @param PostFlushEventArgs $args
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if (!empty($this->insertedEntities)) {
            foreach ($this->insertedEntities as $insertedEntity) {
                /** @noinspection PhpUndefinedMethodInspection */
                $this->addToBatch($this->insertions, $insertedEntity['class'], $insertedEntity['entity']->getId());
            }
        }

        if (!empty($this->insertions) || !empty($this->updates) || !empty($this->deletions)) {
            $this->startCachePopulation($args->getEntityManager());
        }
    }

    /**
     * Adds an entity to a specified batch (either insertions, updates or deletions)
     *
     * @param array $batch
     * @param string $entityClass
     * @param int $entityId
     */
    private function addToBatch(array &$batch, string $entityClass, int $entityId)
    {
        $batch[] = "$entityClass:$entityId";
    }

    /**
     * @param EntityManager $entityManager
     */
    private function startCachePopulation(EntityManager $entityManager)
    {
        $options = [];

        foreach ($this->insertions as $insertion) {
            $options[] = "-i$insertion";
        }

        foreach ($this->updates as $update) {
            $options[] = "-u$update";
        }

        foreach ($this->deletions as $deletion) {
            $options[] = "-d$deletion";
        }

        $this->insertedEntities = [];
        $this->insertions = [];
        $this->updates = [];
        $this->deletions = [];

        $job = new Job('app:cache:remove', $options, true, 'cache_queue');

        try {
            $entityManager->persist($job);
            $entityManager->flush();
        } catch (ORMException $e) {
        }
    }
}
