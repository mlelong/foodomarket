<?php

namespace AppBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use League\Uri\Components\DataPath;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Exception\MappingNotFoundException;
use Vich\UploaderBundle\Exception\NotUploadableException;
use Vich\UploaderBundle\Mapping\PropertyMappingFactory;
use Vich\UploaderBundle\Storage\StorageInterface;

class VichUploadListener
{
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var PropertyMappingFactory
     */
    private $propertyMappingFactory;

    /**
     * VichUploadListener constructor.
     * @param StorageInterface $storage
     * @param PropertyMappingFactory $propertyMappingFactory
     */
    public function __construct(StorageInterface $storage, PropertyMappingFactory $propertyMappingFactory)
    {
        $this->storage = $storage;
        $this->propertyMappingFactory = $propertyMappingFactory;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        try {
            $propertyMappings = $this->propertyMappingFactory->fromObject($entity);

            foreach ($propertyMappings as $propertyMapping) {
                $filenamePropertyName = $propertyMapping->getFileNamePropertyName();
                $data = $entity->{"get{$filenamePropertyName}"}();
                $prefix = 'data:';

                if (substr($data, 0, strlen($prefix)) == $prefix) {
                    $data = substr($data, strlen($prefix));
                    $dataPath = new DataPath($data);

                    if (false === $path = tempnam($directory = sys_get_temp_dir(), 'Base64EncodedFile')) {
                        throw new FileException(sprintf('Unable to create a file into the "%s" directory', $directory));
                    }

                    $fileObject = $dataPath->save($path, 'w');
                    $uploadedFile = new UploadedFile($fileObject->getPathname(), $fileObject->getFilename(), $dataPath->getMimeType(), null, null, true);

                    $entity->{"set{$propertyMapping->getFilePropertyName()}"}($uploadedFile);

                    $this->storage->upload($entity, $propertyMapping);
                }
            }
        } catch (NotUploadableException $e) {
        } catch (MappingNotFoundException $e) {
        }
    }
}