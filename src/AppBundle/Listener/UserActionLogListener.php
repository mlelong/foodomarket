<?php

namespace AppBundle\Listener;

use AppBundle\Factory\UserActivityLogFactory;
use AppBundle\Repository\UserActivityLogRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class UserActionLogListener
{
    /** @var RequestStack */
    private $requestStack;
    /** @var UserActivityLogFactory */
    private $userActivityLogFactory;
    /** @var UserActivityLogRepository */
    private $userActivityLogRepository;

    /**
     * UserActionLogListener constructor.
     * @param RequestStack $requestStack
     * @param UserActivityLogFactory $userActivityLogFactory
     * @param UserActivityLogRepository $userActivityLogRepository
     */
    public function __construct(
        RequestStack $requestStack,
        UserActivityLogFactory $userActivityLogFactory,
        UserActivityLogRepository $userActivityLogRepository
    ) {
        $this->requestStack = $requestStack;
        $this->userActivityLogFactory = $userActivityLogFactory;
        $this->userActivityLogRepository = $userActivityLogRepository;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_object($controller) && !empty($controller) && isset($controller[0]) && is_object($controller[0])
            && $this->isInNamespace('AppBundle\MiddleOffice', $controller[0])) {
            $log = $this->userActivityLogFactory->createFromRequest($this->requestStack->getMasterRequest());

            if ($log->getIp() !== '127.0.0.1') {
                $this->userActivityLogRepository->add($log);
            }
        }
    }

    private function isInNamespace($namespace, $object) {
        return strpos(get_class($object), $namespace . '\\') === 0;
    }
}