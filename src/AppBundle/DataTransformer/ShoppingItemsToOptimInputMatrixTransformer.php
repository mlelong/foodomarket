<?php

namespace AppBundle\DataTransformer;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Entity\ShoppingItemInterface;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class ProductArrayToOptimInputMatrixTransformer
 * @package AppBundle\DataTransformer
 */
class ShoppingItemsToOptimInputMatrixTransformer implements DataTransformerInterface
{
    const JAVA_POSITIVE_INFINITY = 1000000.42;
    const COEFF_MUL = 1000;

    /** @var ContainerInterface */
    private $container;

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    /**
     * @var SupplierProductPriceRepository
     */
    private $supplierProductPriceRepository;

    /**
     * @var Supplier[]
     */
    private $suppliers = [];

    /**
     * @var \AppBundle\Services\ProductUtils|object
     */
    private $productUtils;

    /**
     * @var Restaurant
     */
    private $restaurant;

    /**
     * @var Restaurant
     */
    private $partnerRestaurant;

    /** @var int */
    private $maxSuppliers = 2;

    /**
     * ProductArrayToOptimInputMatrixTransformer constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->init();
    }

    private function init() {
        $this->manager = $this->container->get('doctrine')->getManager();

        $this->supplierRepository = $this->manager->getRepository(Supplier::class);
        $this->supplierProductPriceRepository = $this->manager->getRepository(SupplierProductPrice::class);
        $this->partnerRestaurant = $this->manager->find(Restaurant::class, 4);
        $suppliers = $this->supplierRepository->findBy(['enabled' => false]);

        foreach ($suppliers as $supplier) {
            $this->suppliers[] = ['supplier' => $supplier, 'restaurant' => $this->partnerRestaurant];
        }

        $this->productUtils = $this->container->get('app.utils.product');
    }

    public function setSuppliers($suppliers)
    {
        $this->suppliers = [];

        foreach ($suppliers as $supplier) {
            $this->suppliers[] = ['supplier' => $supplier, 'restaurant' => $this->partnerRestaurant];
        }
    }

    /**
     * @param ShoppingItemInterface[] $value The value in the original representation
     *
     * @return mixed The value in the transformed representation
     *
     * @throws TransformationFailedException when the transformation fails
     */
    public function transform($value)
    {
        $data = [];
        $items = [];

        foreach ($value as $shoppingItem) {
            $found = false;

            foreach ($this->suppliers as $supplierArray) {
                if ($supplierArray['supplier']->getId() == $shoppingItem->getSupplier()->getId() && $supplierArray['restaurant']->getId() == $this->restaurant->getId()) {
                    $found = true;
                    break ;
                }
            }

            if (!$found) {
                $this->suppliers[] = ['supplier' => $shoppingItem->getSupplier(), 'restaurant' => $this->restaurant];
            }
        }

        foreach ($this->suppliers as $supplierArray) {
            /** @var Supplier $supplier */
            $supplier = $supplierArray['supplier'];
            /** @var Restaurant $restaurant */
            $restaurant = $supplierArray['restaurant'];

            foreach ($value as $shoppingItem) {
                $variantId = $shoppingItem->getProductVariant()->getId();

                /** @noinspection PhpParamsInspection */
                $price = $this->productUtils->getCheapestSimilarVariantForSupplierAndRestaurant($shoppingItem->getProductVariant(), $supplier, $restaurant, true);
                $primitivePrice = self::JAVA_POSITIVE_INFINITY;
                $quantity = $shoppingItem->getQuantity();

                if ($price !== null) {
                    $weightRatio = 1;
                    $originalWeight = $shoppingItem->getProductVariant()->getWeight();
                    /** @noinspection PhpUndefinedMethodInspection */
                    $replacementWeight = $price->getProductVariant()->getWeight();

                    if ($originalWeight !== null && $originalWeight > 0 && $replacementWeight !== null && $replacementWeight > 0) {
                        $weightRatio = $shoppingItem->getProductVariant()->getWeight() / $price->getProductVariant()->getWeight();
                    }

                    $quantity = $quantity * $weightRatio;
                    $primitivePrice = $shoppingItem->getUnit() == 'PC' ? $price->getUnitPrice() : $price->getKgPrice();
                    $primitivePrice = $primitivePrice / (100 * self::COEFF_MUL);
                }

                $key = "{$supplier->getId()}-{$restaurant->getId()}";

                $items[$key]['prices'][$variantId] = floatval($primitivePrice);
                $items[$key]['quantity'][$variantId] = floatval($quantity * self::COEFF_MUL);
                $items[$key]['variantIds'][$variantId] = $price !== null ? $price->getProductVariant()->getId() : null;
                $items[$key]['variants'][$variantId] = $price !== null ? $price->getProductVariant() : null;
                $items[$key]['interleave'][$variantId] = 0.0;
                $items[$key]['units'][$variantId] = $shoppingItem->getUnit();

                if (!isset($items[$key]['threshold'])) {
                    $items[$key]['threshold'] = [
                        $supplier->getFreePort(),
                        $supplier->getShippingCost()
                    ];
                }

                if (!isset($items[$key]['supplier'])) {
                    $items[$key]['supplier'] = $supplier;
                }
            }
        }

        $algoParams = [
            'maxFrn' => $this->maxSuppliers,
            'prix' => [],
            'seuils' => [],
            'demande' => [],
            'intervalle' => []
        ];

        foreach ($items as $supplier => $itemsForSupplier) {
            $algoParams['prix'][] = array_values($itemsForSupplier['prices']);
            $algoParams['demande'] = array_values($itemsForSupplier['quantity']);
            $algoParams['intervalle'] = array_values($itemsForSupplier['interleave']);
            $algoParams['seuils'][] = $itemsForSupplier['threshold'];
        }

        $data['items'] = $items;
        $data['input_matrix'] = $algoParams;
        $data['encoded_input_matix'] = json_encode($algoParams, JSON_PRESERVE_ZERO_FRACTION|JSON_NUMERIC_CHECK);

        return $data;
    }

    public function reverseTransform($value) {}

    /**
     * @param $data
     * @param $matrix
     * @param $total
     * @param bool $test
     * @return RestaurantStock
     */
    public function toShoppingList($data, $matrix, &$total, bool $test = false)
    {
        $optimCart = new RestaurantStock();

        $optimCart->setRestaurant($this->restaurant);
        $optimCart->setName(uniqid());

        $total = $data[0];
        $supplierCarts = $data[1];
        $nbCarts = count($supplierCarts);

        for ($i = 0; $i < $nbCarts; ++$i) {
            $j = 0;
            /** @var Supplier $supplier */
            $supplier = null;

            $preservedCouple = null;

            foreach ($matrix['items'] as $couple => $supplierItems) {
                if ($i === $j) {
                    $preservedCouple = $couple;
                    $supplier = $supplierItems['supplier'];
                    break ;
                }

                ++$j;
            }

            foreach ($supplierCarts[$i] as $index => $quantity) {
                if ($quantity == 0)
                    continue ;

                $quantity = $quantity /self::COEFF_MUL;

                $key = array_keys($matrix['items'][$preservedCouple]['variants'])[$index];

                /** @var ProductVariant $variant */
                $variant = $matrix['items'][$preservedCouple]['variants'][$key];

                $shoppingListItem = new RestaurantStockItem();

                $shoppingListItem->setProductVariant($variant);
                $shoppingListItem->setSupplier($supplier);
                $shoppingListItem->setStock($quantity);
                $shoppingListItem->setStockUnit($matrix['items'][$preservedCouple]['units'][$key]);

                // Only for BO Admin Test purpose
                if ($test) {
                    $shoppingListItem->setNote("{$index}-{$preservedCouple}");
                }

                $optimCart->addItem($shoppingListItem);
            }
        }

        return $optimCart;
    }

    public function setRestaurant(Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @param int $maxSuppliers
     */
    public function setMaxSuppliers(int $maxSuppliers): void
    {
        $this->maxSuppliers = $maxSuppliers;
    }
}