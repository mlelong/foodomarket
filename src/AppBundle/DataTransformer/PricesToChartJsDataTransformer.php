<?php

namespace AppBundle\DataTransformer;

use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductPriceHistory;

/**
 * Class PricesToChartJsDataTransformer
 * @package AppBundle\DataTransformer
 *
 * This class prepares chartjs presentational data based on prices we feed to it
 *
 * Feed with
 *      - add() method with a SupplierProductPrice or SupplierProductPriceHistory
 *      - addAll() method with a collection of (mixed or not) SupplierProductPrice and SupplierProductPriceHistory
 *
 * (don't bother feeding it in a right order, this class reorders the data and add missing points)
 *
 * Then
 *      - getLabels() to get all the labels (dates, in ascendind order)
 *      - getDatasets() to get all the data formatted for chartjs
 *
 * Or
 *      - getChartData() to get all the config object along with the data needed for chartjs
 */
class PricesToChartJsDataTransformer
{
    private $chartData;
    private $labels = [];
    private $datasets = [];

    public function __construct()
    {
        $this->chartData = [
            'type' => 'line',
            'responsive' => true,
            'stacked' => false,
            'options' => [
                'scales' => [
                    'yAxes' => [
                        [
                            'id' => 'kgPrice',
                            'type' => 'linear',
                            'position' => 'left',
                            'ticks' => [
                                'beginAtZero' => true
                            ],
                            'scaleLabel' => [
                                'display' => true,
                                'labelString' => 'Prix au kilo'
                            ]
                        ],
                        [
                            'id' => 'unitPrice',
                            'type' => 'linear',
                            'position' => 'right',
                            'ticks' => [
                                'beginAtZero' => true
                            ],
                            'scaleLabel' => [
                                'display' => true,
                                'labelString' => 'Prix unitaire'
                            ],
                            'gridLines' => [
                                'drawOnChartArea' => false
                            ]
                        ]
                    ],
                    'xAxes' => [
                        [
                            'scaleLabel' => [
                                'display' => true,
                                'labelString' => 'Dates de prix'
                            ]
                        ]
                    ]
                ],
                'tooltips' => [
                    'mode' => 'nearest',
                    'axis' => 'y'
                ]
            ],
            'data' => [
                'labels' => [],
                'datasets' => []
            ]
        ];
    }

    public function getChartData() {
        $this->chartData['data']['labels'] = $this->getLabels();
        $this->chartData['data']['datasets'] = $this->getDatasets();

        return $this->chartData;
    }

    public function getLabels() {
        $this->labels = array_unique($this->labels);
        sort($this->labels);

        return $this->labels;
    }

    public function getDatasets() {
        $labels = $this->getLabels();
        $nbLabels = count($labels);

        foreach ($this->datasets as $key => &$dataset) {
            ksort($dataset['data']);

            $isKg = strripos($key, '-kg') !== false;

            if (count($dataset['data']) == $nbLabels) {
                foreach ($dataset['data'] as $date => $value) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $dataset['infos'][$date] = [
                        'date' => $date,
                        'restaurant' => $dataset['infos']['restaurant']->getId(),
                        'restaurant_name' => $dataset['infos']['restaurant']->getName(),
                        'supplier' => $dataset['infos']['supplier']->getId(),
                        'supplier_name' => $dataset['infos']['supplier']->getName(),
                        'unitPrice' => is_object($value) ? $value->getUnitPrice() : $value,
                        'kgPrice' => is_object($value) ? $value->getKgPrice() : $value,
                    ];

                    if ($value instanceof SupplierProductPrice || $value instanceof SupplierProductPriceHistory) {
                        $dataset['infos'][$date] = array_merge($dataset['infos'][$date], [
                            'type' => get_class($value),
                            'id' => $value->getId(),
                            'unitPrice' => $value->getUnitPrice(),
                            'kgPrice' => $value->getKgPrice(),
                        ]);
                    }

                    $dataset['data'][$date] = is_object($value) ? ($isKg ? $value->getKgPrice() / 100 : $value->getUnitPrice() / 100) : $value;
                }

                ksort($dataset['infos']);
                $dataset['infos'] = array_values($dataset['infos']);
                $dataset['data'] = array_values($dataset['data']);

                continue ;
            }

            // Il faut rajouter des points manquants
            foreach ($labels as $label) {
                if (isset($dataset['data'][$label])) {
                    continue ;
                }

                $point = 0;
                foreach ($dataset['data'] as $date => $data) {
                    if ($date < $label) {
                        $point = $data;

                        continue ;
                    }

                    // Insert before
                    $dataset['data'][$label] = $point;
                    ksort($dataset['data']);

                    continue 2;
                }

                $dataset['data'][$label] = $point;
                ksort($dataset['data']);
            }

            foreach ($dataset['data'] as $date => $value) {
                /** @noinspection PhpUndefinedMethodInspection */
                $dataset['infos'][$date] = [
                    'date' => $date,
                    'restaurant' => $dataset['infos']['restaurant']->getId(),
                    'restaurant_name' => $dataset['infos']['restaurant']->getName(),
                    'supplier' => $dataset['infos']['supplier']->getId(),
                    'supplier_name' => $dataset['infos']['supplier']->getName(),
                    'unitPrice' => is_object($value) ? $value->getUnitPrice() : $value,
                    'kgPrice' => is_object($value) ? $value->getKgPrice() : $value,
                ];

                if ($value instanceof SupplierProductPrice || $value instanceof SupplierProductPriceHistory) {
                    // Check if it is an added point from a previous one, in that case we should not touch this point for updates or removes !
                    if ($date == $value->getCreatedAt()->format('Y-m-d')) {
                        $dataset['infos'][$date] = array_merge($dataset['infos'][$date], [
                            'type' => get_class($value),
                            'id' => $value->getId()
                        ]);
                    }
                }

                $dataset['data'][$date] = is_object($value) ? ($isKg ? $value->getKgPrice() / 100 : $value->getUnitPrice() / 100) : $value;
            }

            ksort($dataset['infos']);
            $dataset['infos'] = array_values($dataset['infos']);
            $dataset['data'] = array_values($dataset['data']);
        }

        return array_values($this->datasets);
    }

    /**
     * @param SupplierProductPrice|SupplierProductPriceHistory $price
     */
    public function add($price) {
        $restaurant = $price->getRestaurant();
        $supplier = $price->getSupplier();
        $date = $price->getCreatedAt()->format('Y-m-d');
        $keyKg = "{$restaurant->getId()}-{$supplier->getId()}-kg";
        $keyUnit = "{$restaurant->getId()}-{$supplier->getId()}-unit";

        if (!isset($this->datasets[$keyKg])) {
            $this->datasets[$keyKg] = [
                'label' => "{$restaurant->getName()} chez {$supplier->getName()} (kg)",
                'infos' => ['restaurant' => $restaurant, 'supplier' => $supplier],
                'borderWidth' => 1,
                'yAxisID' => 'kgPrice',
                'fill' => false,
                'data' => []
            ];
        }

        if (!isset($this->datasets[$keyUnit])) {
            $this->datasets[$keyUnit] = [
                'label' => "{$restaurant->getName()} chez {$supplier->getName()} (unit)",
                'infos' => ['restaurant' => $restaurant, 'supplier' => $supplier],
                'borderWidth' => 1,
                'yAxisID' => 'unitPrice',
                'fill' => false,
                'borderDash' => [5, 5],
                'data' => []
            ];
        }

        $this->labels[] = $date;
        $this->datasets[$keyKg]['data'][$date] = $price;
        $this->datasets[$keyUnit]['data'][$date] = $price;
    }

    /**
     * @param SupplierProductPrice[]|SupplierProductPriceHistory[] $prices
     */
    public function addAll($prices)
    {
        foreach ($prices as $price) {
            if ($price instanceof SupplierProductPrice || $price instanceof SupplierProductPriceHistory) {
                $this->add($price);
            }
        }
    }
}