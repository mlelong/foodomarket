<?php

declare(strict_types=1);

namespace AppBundle\Event;

use AppBundle\Entity\Prospect;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ProspectShowMenuBuilderEvent extends MenuBuilderEvent
{
    /**
     * @var Prospect
     */
    private $prospect;

    /**
     * @param FactoryInterface $factory
     * @param ItemInterface $menu
     * @param Prospect $prospect
     */
    public function __construct(FactoryInterface $factory, ItemInterface $menu, Prospect $prospect)
    {
        parent::__construct($factory, $menu);

        $this->prospect = $prospect;
    }

    /**
     * @return Prospect
     */
    public function getProspect(): Prospect
    {
        return $this->prospect;
    }
}
