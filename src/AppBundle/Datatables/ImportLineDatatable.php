<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\ImportLine;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sylius\Component\Core\Model\ProductTranslation;
use Sylius\Component\Core\Model\ProductVariant;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Taxonomy\Model\TaxonTranslation;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

/**
 * Class ImportLineDatatable
 *
 * @package AppBundle\Datatables
 */
class ImportLineDatatable extends AbstractDatatable
{
    /**
     * @var TaxonRepositoryInterface
     */
    private $taxonRepository;

    private $taxons = array();

    public function setTaxonRepository(TaxonRepositoryInterface $taxonRepository)
    {
//        $this->taxonRepository = $taxonRepository;
//
//        /** @var TaxonInterface[] $allTaxons */
//        $allTaxons = $this->taxonRepository->findAll();
//
//        foreach ($allTaxons as &$taxon) {
//            $this->taxons[] = array(
//                'value' => $taxon->getId(),
//                'text' => $taxon->getName()
//            );
//        }
    }

    public function getLineFormatter()
    {
        return function($row) {
            switch ($row['status']) {
                case ImportLine::STATUS_AWAITING_CHECK:
                    $row['status'] = 'En attente';
                    break ;
                case ImportLine::STATUS_MATCHED_WITH_CATALOG:
                    $row['status'] = 'Connu au catalogue';
                    break ;
                case ImportLine::STATUS_IMPORTED:
                    $row['status'] = 'Importé';
                    break ;
            }

            return $row;
        };
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'classes' => Style::SEMANTIC_UI_STYLE,
            'order_multi' => true,
            'order_fixed' => array('id' => 'ASC')
        ));

        $this->callbacks->set(array(
            'row_callback' => array(
                'template' => '@App/Admin/Import/row_callback.js.twig',
            )
        ));

        $this->features->set(array(
//            'auto_width' => true,
            'scroll_x' => true,

        ));

        $this->columnBuilder
            ->add('status', Column::class, array(
                'title' => 'Statut',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('import.id', Column::class, array('visible' => false))
            ->add('content', Column::class, array('visible' => false,
            ))
            ->add('supplierName', Column::class, array(
                'title' => 'Libellé fichier',
                'orderable' => true,
                'searchable' => true,
            ))
            ->add('bio', Column::class, array(
                'title' => 'BIO',
                'orderable' => true,
                'searchable' => false
            ))
            ->add('salesUnit', Column::class, array(
                'title' => 'Unité de mesure mercuriale',
                'orderable' => true,
                'searchable' => false,
            ))
/*
            ->add('orderUnit', Column::class, array(
                'title' => 'Unité de commande',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('conditioning', Column::class, array(
                'title' => 'Conditionnement',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('originCountry', Column::class, array(
                'title' => 'Pays d\'origine',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('naming', Column::class, array(
                'title' => 'Appellation',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('category', Column::class, array(
                'title' => 'Catégorie',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('caliber', Column::class, array(
                'title' => 'Calibre',
                'orderable' => true,
                'searchable' => false,
            ))
*/
            ->add('price', Column::class, array(
                'title' => 'Prix fichier',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('unitQuantity', Column::class, array(
                'title' => 'Nombre d\'unité',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('unitContentQuantity', Column::class, array(
                'title' => 'Contenu de l\'unité',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('unitContentMeasurement', Column::class, array(
                'title' => 'Mesure',
                'orderable' => false,
                'searchable' => false,
            ))
            ->add('weight', Column::class, array(
                'title' => 'Poids Total',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('priceVariant', Column::class, array(
                'title' => 'Prix de vente',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('kgPrice', Column::class, array(
                'title' => 'Prix au kilo',
                'orderable' => true,
                'searchable' => false,
            ))
            ->add('taxon', Column::class, array(
                'title' => 'Taxon',
                'orderable' => true,
                'searchable' => true,
                'dql' => '(SELECT {t}.name FROM ' . TaxonTranslation::class .' {t} WHERE {t}.translatable = importline.taxon AND {t}.locale = \'fr_FR\')',
            ))
            ->add('product', Column::class, array(
                'title' => 'Produit Associé',
                'orderable' => true,
                'searchable' => true,
                'dql' => '(SELECT {p}.name FROM ' . ProductTranslation::class .' {p} WHERE {p}.translatable = importline.product AND {p}.locale = \'fr_FR\')'
            ))
            ->add('productVariant', Column::class, array(
                'title' => 'Id variante associée',
                'orderable' => false,
                'searchable' => false,
                'dql' => '(SELECT {pv}.id FROM ' . ProductVariant::class .' {pv} WHERE {pv}.id = importline.productVariant)'
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'width' => '300',
                'actions' => array(
                    array(
                        'route' => 'app_admin_importline_line',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'ui mini button primary',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'app_admin_importline_without_control',
                        'route_parameters' => array(
                            'import' => 'import.id',
                            'importline' => 'id'
                        ),
                        'label' => 'Valider',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'Valider',
                            'class' => 'ui mini button positive',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\ImportLine';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'importline_datatable';
    }
}
