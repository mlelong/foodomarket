function initImportProgress(current, total) {
    $('#import-progress')
        .progress({
            duration : 200,
            total    : total,
            value    : current,
            text     : {
                active: '{value} produits sur {total}'
            }
        })
    ;
}

function setProgress(value) {
    $('#import-progress').progress('set progress', value);
}

function listenToDisplayOptionsChange($productFormContainer) {
    const $displayContentOptions = $productFormContainer.find('#display-content-options');
    const handleContentOptionsChange = function() {
        $displayContentOptions.find('.ui.message').removeClass('positive').addClass('info').find('p').text('Calcul en cours...');

        VariantUtils.getExplicitContent(
            $('#app_admin_import_line_orderUnit').find('option:selected').text(),
            $('#app_admin_import_line_conditioning').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_unitQuantity').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_unitContentQuantity').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_unitContentMeasurement').closest('.sylius-autocomplete').dropdown('get text'),
            function(result) {
                $displayContentOptions.find('.ui.message').removeClass('info').addClass('positive').find('p').text(result.explicit_content);
            }
        );
    };

    $displayContentOptions.find('select').change(function() {
        handleContentOptionsChange();

        const val = $(this).val();

        if (val === 'ORDER_KG' || val === 'ORDER_L') {
            $displayContentOptions.find('.sylius-autocomplete').closest('.field').addClass('disabled');
        } else {
            $displayContentOptions.find('.sylius-autocomplete').closest('.field').removeClass('disabled');
        }
    });

    $displayContentOptions.find('.ui.message').find('.refresh.icon').click(handleContentOptionsChange);
    $displayContentOptions.find('.sylius-autocomplete').each(function() {
        $(this).autoCompleteResource(handleContentOptionsChange, undefined, 0, true);
    });

    const $displayNameOptions = $productFormContainer.find('#display-name-options');
    const handleNamingOptionsChange = function() {
        $displayNameOptions.find('.ui.message').removeClass('positive').addClass('info').find('p').text('Calcul en cours...');

        const productName = $('#app_admin_import_line_product').closest('.sylius-autocomplete').dropdown('get text');
        const defaultProductName = $productFormContainer.find('.card > .content > .header > #default-product-name').text();

        VariantUtils.getVariantName(
            productName.trim().length > 0 ? encodeURIComponent(productName) : encodeURIComponent(defaultProductName),
            $('#app_admin_import_line_caliber').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_category').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_variety').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_brand').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_bio').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_naming').closest('.sylius-autocomplete').dropdown('get text'),
            function(result) {
                $displayNameOptions.find('.ui.message').removeClass('info').addClass('positive').find('p').text(result.variant_name);
            }
        );

        $productFormContainer.find('#image-search').find('.input input').val(productName);
    };

    $displayNameOptions.find('.ui.message').find('.refresh.icon').click(handleNamingOptionsChange);
    $displayNameOptions.find('.sylius-autocomplete').each(function() {
        $(this).autoCompleteResource(handleNamingOptionsChange, undefined, 0, true);
    });

    $('#app_admin_import_line_product').change(handleNamingOptionsChange);

    const $displayPriceOptions = $productFormContainer.find('#display-price-options');
    const handleDisplayPriceOptionsChange = function() {
        $displayPriceOptions.find('.ui.message').removeClass('positive').addClass('info').find('p').text('Calcul en cours...');

        VariantUtils.getVariantPrice(
            $('#app_admin_import_line_orderUnit').find('option:selected').text(),
            $('#app_admin_import_line_salesUnit').find('option:selected').text(),
            $('#app_admin_import_line_unitQuantity').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_conditioning').closest('.sylius-autocomplete').dropdown('get text'),
            $('#app_admin_import_line_weight').val(),
            $('#app_admin_import_line_kgPrice').val(),
            $('#app_admin_import_line_priceVariant').val(),
            function(result) {
                $displayPriceOptions.find('.ui.message').removeClass('info').addClass('positive').find('p').text(result.price_detail.full);
            }
        );
    };

    $displayPriceOptions.find('select').change(handleDisplayPriceOptionsChange);
    $displayPriceOptions.find('input').change(handleDisplayPriceOptionsChange);
    $displayPriceOptions.find('.ui.message').find('.refresh.icon').click(handleDisplayPriceOptionsChange);

    $('#app_admin_import_line_unitQuantity, #app_admin_import_line_conditioning')
        .closest('.sylius-autocomplete')
        .dropdown('setting', 'onChange', function() {
            handleContentOptionsChange();
            handleDisplayPriceOptionsChange();
        });
    $('#app_admin_import_line_orderUnit').change(function() {
        handleContentOptionsChange();
        handleDisplayPriceOptionsChange();
    });

    $('#app_admin_import_line_weight').change(function() {
        const salesUnit = $('#app_admin_import_line_salesUnit').val();
        const weight = $(this).val().replace(',', '.');

        if (salesUnit === 'PC' || salesUnit === 'COLIS' || salesUnit === 'CO') {
            const unitPrice = $('#app_admin_import_line_priceVariant').val().replace(',', '.');

            $('#app_admin_import_line_kgPrice').val(weight > 0 ? Math.round10(unitPrice / weight, -2) : 0);
        } else {
            const kgPrice = $('#app_admin_import_line_kgPrice').val().replace(',', '.');

            $('#app_admin_import_line_priceVariant').val(weight > 0 ? Math.round10(kgPrice * weight, -2) : 0);
        }

        handleDisplayPriceOptionsChange();
    });

    $('#app_admin_import_line_priceVariant').change(function() {
        const salesUnit = $('#app_admin_import_line_salesUnit').val();

        if (salesUnit === 'PC' || salesUnit === 'COLIS' || salesUnit === 'CO') {
            const weight = $('#app_admin_import_line_weight').val().replace(',', '.');
            const unitPrice = $(this).val().replace(',', '.');

            $('#app_admin_import_line_kgPrice').val(weight > 0 ? Math.round10(unitPrice / weight, -2) : 0);
        }

        handleDisplayPriceOptionsChange();
    });

    $('#app_admin_import_line_kgPrice').change(function() {
        const salesUnit = $('#app_admin_import_line_salesUnit').val();

        if (!(salesUnit === 'PC' || salesUnit === 'COLIS' || salesUnit === 'CO')) {
            const weight = $('#app_admin_import_line_weight').val().replace(',', '.');
            const kgPrice = $(this).val().replace(',', '.');

            $('#app_admin_import_line_priceVariant').val(weight > 0 ? Math.round10(kgPrice * weight, -2) : 0);
        }

        handleDisplayPriceOptionsChange();
    });

    const $displayOriginOptions = $productFormContainer.find('#display-origin-options');

    $displayOriginOptions.find('.sylius-autocomplete').each(function() {
        $(this).autoCompleteResource(undefined, undefined, 0, true);
    });
}

function onPencilIconClicked($input, $autocomplete)
{
    const value = $input.val();
    const currentDropdownText = $autocomplete.dropdown('get text');
    const currentDropdownValue = $autocomplete.dropdown('get value');

    if (value != '' && value != currentDropdownValue && value != currentDropdownText) {
        $autocomplete.find('.menu').find('.item').removeClass('selected');
        $autocomplete.find('.menu').find('.addition.item').addClass('selected');

        const e = jQuery.Event("keydown");
        e.which = 13; // Enter Key
        e.keyCode = 13;
        $input.trigger(e);
    }
}

function bindForm() {
    var $productFormContainer = $('#product-form-container');
    var $taxonSearch = $productFormContainer.find('#taxon-search');
    var $productSearch = $productFormContainer.find('#product-search');
    var $productVariantSearch = $productFormContainer.find('#product-variant-search');

    $productFormContainer.find('.ui.accordion').accordion({exclusive: false});

    // Adds the taxons suggestions to the taxon dropdown results so that we can select a suggestion as value
    $productFormContainer.find('.taxon.suggestion ol li').each(function() {
        var $li = $(this);

        $taxonSearch.find('.menu').append(`<div class="item" data-value="${$li.data('taxonid')}">${$li.data('taxonname')}</div>`);
    });

    const $taxonAutocomplete = $taxonSearch.find('.sylius-autocomplete');

    // Inits and binds taxon autocomplete so that on update it refreshes the product autocomplete widget
    $taxonAutocomplete.autoCompleteResource(function(value, text, $choice) {
        // handle the product refresh upon product changed
        const $productAutocomplete = $productSearch.find('.sylius-autocomplete');
        const productSearchUrl = Routing.generate('app_admin_product_import_search_product', {taxonId: value});

        $productAutocomplete.dropdown('clear');
        $productAutocomplete.dropdown('setup menu', {values: []});

        $productAutocomplete.attr('data-url', productSearchUrl);
        $productAutocomplete.data('url', productSearchUrl);

        $productAutocomplete.dropdown('refresh');
    });

    const currentTaxonId = $taxonSearch.find('.sylius-autocomplete').dropdown('get value');
    const productSearchUrl = Routing.generate('app_admin_product_import_search_product', {taxonId: currentTaxonId !== '' && currentTaxonId !== undefined ? currentTaxonId : 0});
    const $productAutocomplete = $productSearch.find('.sylius-autocomplete');

    // Inits and binds product autocomplete so that on update it displays if it is going to create a new product or not
    $productAutocomplete
        .attr('data-url', productSearchUrl)
        .data('url', productSearchUrl)
        .autoCompleteResource(function(value, text, $choice) {
            const $productMessage = $productSearch.find('.ui.message');

            if (value != '' && text != '' && ((value == 0 || !$.isNumeric(value)) && $.isNumeric($taxonAutocomplete.dropdown('get value')))) {
                $productMessage.find('#product-name').text(text);
                $productMessage.find('#taxon-path').text($taxonAutocomplete.dropdown('get text'));
                $productMessage.removeClass('hidden');
            } else {
                $productMessage.removeClass('hidden').addClass('hidden');
            }

            $('#app_admin_import_line_productName').val(text);

            // handle the product variant refresh upon product changed
            const $productVariantAutocomplete = $productVariantSearch.find('.sylius-autocomplete');
            const productVariantSearchUrl = Routing.generate('app_admin_product_import_search_product_variant', {productId: value});

            $productVariantAutocomplete.dropdown('clear');
            $productVariantAutocomplete.dropdown('setup menu', {values: []});

            $productVariantAutocomplete.attr('data-url', productVariantSearchUrl);
            $productVariantAutocomplete.data('url', productVariantSearchUrl);

            $productVariantAutocomplete.dropdown('refresh');
        }, undefined, 3, true);

    $productSearch.find('.menu').append(`<div class="item" data-value="0">${$('#app_admin_import_line_productName').val()}</div>`);
    $productAutocomplete.dropdown('refresh');

    const currentProductId = $productAutocomplete.dropdown('get value');

    if (currentProductId === undefined || currentProductId == '') {
        $productAutocomplete.dropdown('set selected', '0');
    }

    // Binds the variant autocomplete
    const $productVariantAutocomplete = $productVariantSearch.find('.sylius-autocomplete');
    const productVariantSearchUrl = Routing.generate('app_admin_product_import_search_product_variant', {productId: currentProductId !== '' && currentProductId !== undefined ? currentProductId : 0});
    $productVariantAutocomplete
        .attr('data-url', productVariantSearchUrl)
        .data('url', productVariantSearchUrl)
        .autoCompleteResource(function(value, text, $choice) {
            // TODO: post process the fetching of variant
        });

    // Binds taxon suggestion click event to update product search URL with the selected taxon id
    $productFormContainer.find('.taxon.suggestion ol li').off('click').click(function() {
        $taxonSearch.find('.sylius-autocomplete').dropdown('set selected', $(this).data('taxonid'));
    });

    // Binds form fields change events that impact previews
    listenToDisplayOptionsChange($productFormContainer);

    // Adds a "clear icon" above each autocomplete field
    $productFormContainer.find('.sylius-autocomplete').each(function() {
        const $autocomplete = $(this);
        const $label = $autocomplete.prev('label');
        const $clearField = $('<span title="Vider le champ" class="clear-field"><i class="close icon"></i></span>');

        $clearField.click(function() {
            $autocomplete.dropdown('clear');

            if ($autocomplete.closest('#product-search').length > 0) {
                $autocomplete.dropdown('setup menu', {values: []});
                $autocomplete.dropdown('refresh');
            }
        });

        if ($autocomplete.closest('#taxon-search').length === 0) {
            const $input = $autocomplete.find('input.search');
            const $pencilIcon = $('<span class="pencil-icon"><i class="pencil icon" title="Valider"></i></span>');

            $pencilIcon.click(function () {
                onPencilIconClicked($input, $autocomplete);
            });

            $autocomplete.append($pencilIcon);
        }

        $label.html($label.text()).append($clearField);
    });

    $('#reset-default-product').off('click').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        const defaultProductName = $('#default-product-name').text();

        $productAutocomplete.dropdown('clear');
        $productAutocomplete.dropdown('setup menu', {values: []});
        $productSearch.find('.menu').append(`<div class="item" data-value="${defaultProductName}">${defaultProductName}</div>`);
        $productAutocomplete.dropdown('refresh');
        $productAutocomplete.dropdown('set selected', defaultProductName);
    });

    // Prevent form submit when pressing enter
    $productFormContainer.find('form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    // Handle form submit
    $productFormContainer.find('#save-button').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        const $form = $productFormContainer.find('.form');
        const formAction = $form.attr('action');
        const formData = $form.serialize();

        $(this).addClass('loading');

        $.post(formAction, formData).then(function(response) {
            if (typeof response === 'object' && response.hasOwnProperty('ok') && response.ok) {
                document.location = response.redirect_uri;
            } else {
                $productFormContainer.html(response);

                bindForm();
            }
        });
    });

    $productFormContainer.find('#import-button').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        const $form = $productFormContainer.find('.form');
        const formData = $form.serialize();
        const formAction = Routing.generate('app_admin_product_import_import_product', {id: $('#importId').val(), lineId: $('#lineId').val()});

        $(this).addClass('loading');

        $.post(formAction, formData).then(function(response) {
            if (typeof response === 'object' && response.hasOwnProperty('ok') && response.ok) {
                document.location = Routing.generate('app_admin_product_import_edit', {id: $('#importId').val()});
            } else {
                $productFormContainer.html(response);

                bindForm();
            }
        });
    });

    const $imageSearch = $productFormContainer.find('#image-search');

    $imageSearch.find('input').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            e.stopPropagation();

            doImageSearch($(this).val(), 1);
        }
    });

    $imageSearch.find('.input .icon').off('click').click(function() {
        doImageSearch($imageSearch.find('.input input').val(), 1);
    });

    $imageSearch.find('#remove-image').off('click').click(function() {
        const $selectedImageColumn = $imageSearch.find('#selected-image-column');

        $selectedImageColumn.find('a').attr('href', '').text('');
        $selectedImageColumn.find('img').attr('src', '');
    });
}

function doImageSearch(query, startIndex = 1)
{
    const $imageSearch = $('#image-search');
    const $input = $imageSearch.find('.input');
    const $prevImages = $imageSearch.find('#prev-images');
    const $nextImages = $imageSearch.find('#next-images');

    $input.removeClass('loading').addClass('loading');

    $.get('https://www.googleapis.com/customsearch/v1?cx=003208644587978917648:hn3retkstcc&key=AIzaSyCZ95vfxqPOevvUcmOO-LQb_JAtJrF3S7Y&q=' + encodeURIComponent(query) + '&start=' + startIndex)
        .then(function(results) {
                if (results.queries.hasOwnProperty('previousPage')) {
                    $prevImages.removeClass('disabled');

                    $prevImages.off('click').click(function() {
                        doImageSearch(query, results.queries.previousPage[0].startIndex);
                    });
                } else {
                    $prevImages.removeClass('disabled').addClass('disabled').off('click');
                }

                if (results.queries.hasOwnProperty('nextPage') && results.queries.nextPage[0].startIndex < 100) {
                    $nextImages.removeClass('disabled');

                    $nextImages.off('click').click(function() {
                        doImageSearch(query, results.queries.nextPage[0].startIndex);
                    });
                } else {
                    $nextImages.removeClass('disabled').addClass('disabled').off('click');
                }

                const $cards = $('<div class="ui five cards doubling"></div>');

                if (results.hasOwnProperty('items')) {
                    const items = $(results.items);

                    items.each(function(index, item) {
                        if (item.hasOwnProperty('pagemap')
                            && item.pagemap.hasOwnProperty('cse_image')
                            && item.pagemap.cse_image.length > 0)
                        {
                            const $card = $('<div class="ui card"></div>');
                            const $image = $(`<div class="image"><img src="${item.pagemap.cse_image[0].src}"/></div>`);
                            const $content = $('<div class="content"></div>');
                            const $meta = $(`<div class="meta"><a target="_blank" title="Voir la source" href="${item.link}">${item.title}</a></div>`);
                            const $extra = $('<div class="extra content"></div>');
                            const $useImageButton = $(`<button data-src="${item.pagemap.cse_image[0].src}" type="button" role="button" class="ui button fluid">Utiliser cette image</button>`);

                            $useImageButton.click(function() {
                                const imageSrc = $(this).data('src');

                                $('#app_admin_import_line_productPicture').val(imageSrc);

                                $imageSearch.find('#selected-image-column').find('a').attr('href', imageSrc).text(imageSrc);
                                $imageSearch.find('#selected-image-column').find('img').attr('src', imageSrc);
                            });

                            $content.append($meta);
                            $card.append($image).append($content);
                            $extra.append($useImageButton);
                            $card.append($extra);
                            $cards.append($card);
                        }
                    });
                }

                $('#image-search-results').html('').append($cards);

                $input.removeClass('loading');
            }
        );
}

function loadForm(importId, importLineId) {
    var $productFormContainer = $('#product-form-container');

    $('#prev-product, #next-product').off('click').click(function() {
        var currentLineId = $productFormContainer.find('#lineId').val();

        $productFormContainer.parent().find('.dimmer').removeClass('active').addClass('active');

        $.get(Routing.generate('app_admin_product_import_edit_next_product', {id: importId, lineId: currentLineId})).then(function(response) {
            $productFormContainer.html(response);

            bindForm();

            $productFormContainer.parent().find('.dimmer').removeClass('active');
        });
    });

    $productFormContainer.parent().find('.dimmer').removeClass('active').addClass('active');

    $.get(Routing.generate('app_admin_product_import_edit_product', {id: importId, lineId: importLineId})).then(function(response) {
        $productFormContainer.html(response);

        bindForm();

        $productFormContainer.parent().find('.dimmer').removeClass('active');
    });
}