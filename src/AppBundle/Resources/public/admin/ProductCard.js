const ProductCard = props => {
    const {product, isAppDev} = props;

    let startingPrice = product.startingPriceOnline;
    //Extracting the price per kilo from prodcut starting price
    const regexParenthesis = /\(([^)]+)\)/;
    const matches = regexParenthesis.exec(startingPrice);

    if (matches) {
        startingPrice = matches[1].replace('soit', '');
    }

    return (
        <div className="ui fluid card product-card" style={{minHeight: product.promo ? '445px' : '400px'}}>
            {product.promo &&
            <div className={'ui red ribbon label'}>
                Promotion
            </div>
            }
            <img className={'ui fluid image'} src={product.picture}/>
            <div className={'content'}>
                <div className={'header'}>{product.name}{isAppDev && ` (${product.id})`}</div>
                <div className={'meta'}>
                    {product.variants.length} offre{product.variants.length > 1 && 's'}<br/>
                    {startingPrice.length > 0 && <span className={"starting-price-text"}>à partir de <span className={"starting-price-price"}>{startingPrice}</span></span>}
                </div>
            </div>
            <div className={'content extra'}>
                <i className={'icon truck'}/>
                {product.suppliers.length} fournisseur{product.suppliers.length > 1 && 's'}
            </div>
        </div>
    );
};
