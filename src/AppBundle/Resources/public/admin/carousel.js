$(document).ready(function() {
    setTimeout(function() {
        $('.sylius-autocomplete').each(function() {
            const $this = $(this);
            // Initialize autocomplete with onChange event
            $this.autoCompleteResource(handleProductChange, undefined, 3, true);

            // Initial call to onChange to fetch the graphical data
            handleProductChange($this.find('input.autocomplete').val(), undefined, $this);
        });
    }, 100);

    $('.button[data-form-collection="add"]').click(function() {
        setTimeout(function() {
            $('.sylius-autocomplete').each(function() {
                const $this = $(this);
                // Initialize autocomplete with onChange event
                $this.autoCompleteResource(handleProductChange, undefined, 3, true);
            });
        }, 100);
    });
});

function handleProductChange(value, text, $choice)
{
    $.get(Routing.generate('app_admin_carousel_ajax_product_data', {code: value})).then(function(response) {
        const $additionalDataContainer = $choice.closest('.field').parent().parent().find('.additional-data');

        if (response.ok) {
            ReactDOM.render(<ProductCard product={response.data} isAppDev={true}/>, $additionalDataContainer[0]);
        } else {
            $additionalDataContainer.html('<b>Offre non disponible</b>')
        }
    });
}
