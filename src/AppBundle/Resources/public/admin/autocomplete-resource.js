(function ( $ ) {
    'use strict';

    $.fn.extend({
        autoCompleteResource: function (onChange, additionnalCriterias, minCharacters = 3, allowAdditions = false) {
            $(this).each(function () {
                var element = $(this);
                var criteriaName = element.data('criteria-name');
                var choiceName = element.data('choice-name');
                var choiceValue = element.data('choice-value');
                var autocompleteValue = element.find('input.autocomplete').val();
                var loadForEditUrl = element.data('load-edit-url');
                var url = element.data('url');
                var prevQuery = undefined;

                element.dropdown({
                    onChange: function(value, text, $choice) {
                        if (typeof value !== "undefined" && onChange !== undefined) {
                            onChange(value, text, $choice);
                        }
                    },
                    delay: {
                        search: 250
                    },
                    minCharacters: minCharacters,
                    selectOnKeydown: true,
                    forceSelection: false,
                    match: 'text',
                    clearable: true,
                    allowAdditions: allowAdditions,
                    placeholder: false,
                    apiSettings: {
                        dataType: 'JSON',
                        cache: false,
                        beforeSend: function(settings) {
                            settings.data[criteriaName] = settings.urlData.query;

                            if (additionnalCriterias !== undefined) {
                                for (var i in additionnalCriterias) {
                                    if (additionnalCriterias.hasOwnProperty(i)) {
                                        if (additionnalCriterias[i] !== undefined && additionnalCriterias[i] != '') {
                                            settings.data[i] = additionnalCriterias[i];
                                        }
                                    }
                                }
                            }

                            if (url !== element.data('url')) {
                                url = element.data('url');
                            } else {
                                if (prevQuery !== undefined && settings.urlData.query === prevQuery) {
                                    return false;
                                }
                            }

                            prevQuery = settings.urlData.query;

                            return settings;
                        },
                        onResponse: function (response) {
                            var choiceName = element.data('choice-name');
                            var choiceValue = element.data('choice-value');
                            var myResults = [];
                            $.each(response, function (index, item) {
                                myResults.push({
                                    name: item[choiceName],
                                    value: item[choiceValue]
                                });
                            });

                            return {
                                success: true,
                                results: myResults
                            };
                        }
                    }
                });

                if (0 < autocompleteValue.split(',').length) {
                    var menuElement = element.find('div.menu');

                    menuElement.api({
                        on: 'now',
                        method: 'GET',
                        url: loadForEditUrl,
                        beforeSend: function (settings) {
                            settings.data[choiceValue] = autocompleteValue.split(',');

                            return settings;
                        },
                        onSuccess: function (response) {
                            $.each(response, function (index, item) {
                                menuElement.append(
                                    $('<div class="item" data-value="'+item[choiceValue]+'">'+item[choiceName]+'</div>')
                                );
                            });

                            window.setTimeout(function () {
                                element.dropdown('set selected', element.find('input.autocomplete').val().split(','));
                            }, 100);
                        }
                    });
                }
            });
        }
    });
})( jQuery );