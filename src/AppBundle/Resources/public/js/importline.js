(function ( $ ) {
    'use strict';

    $.fn.extend({
        autoCompleteTaxon: function () {
            $(this).each(function () {
                var element = $(this);
                var criteriaName = $(this).data('criteria-name');
                var choiceName = $(this).data('choice-name');
                var choiceValue = $(this).data('choice-value');
                var autocompleteValue = $(this).find('input.autocomplete').val();
                var loadForEditUrl = $(this).data('load-edit-url');

                element.dropdown({
                    delay: {
                        search: 250
                    },
                    forceSelection: false,
                    apiSettings: {
                        dataType: 'JSON',
                        cache: true,
                        beforeSend: function(settings) {
                            settings.data[criteriaName] = settings.urlData.query;

                            return settings;
                        },
                        onResponse: function (response) {
                            var choiceName = element.data('choice-name');
                            var choiceValue = element.data('choice-value');
                            var myResults = [];
                            $.each(response, function (index, item) {
                                myResults.push({
                                    name: item[choiceName],
                                    value: item[choiceValue]
                                });
                            });

                            return {
                                success: true,
                                results: myResults
                            };
                        }
                    }
                });

                if (0 < autocompleteValue.split(',').length) {
                    var menuElement = element.find('div.menu');

                    menuElement.api({
                        on: 'now',
                        method: 'GET',
                        url: loadForEditUrl,
                        beforeSend: function (settings) {
                            settings.data[choiceValue] = autocompleteValue.split(',');

                            return settings;
                        },
                        onSuccess: function (response) {
                            $.each(response, function (index, item) {
                                menuElement.append(
                                    $('<div class="item" data-value="'+item[choiceValue]+'">'+item[choiceName]+'</div>')
                                );
                            });
                        }
                    });
                }

                window.setTimeout(function () {
                    element.dropdown('set selected', element.find('input.autocomplete').val().split(','));
                }, 5000);
            });
        }
    });
})( jQuery );

function _bindVariantTunneling($form) {
    var $taxon = $form.find('#vitalis_import_line_type_taxon');
    var $product = $form.find('#vitalis_import_line_type_product');
    var $variant = $form.find('#vitalis_import_line_type_productVariant');

    $taxon.off('change').change(function() {
        $.get(Routing.generate('app_admin_importline_products_by_taxon', {'taxon-code': $(this).val()})).then(function(response) {
            if (typeof response === 'object') {
                if (!response.error) {
                    _updateSelect($product, response.data);
                }
            }
        });
    });

    $product.off('change').change(function() {
        $.get(Routing.generate('app_admin_importline_variants_by_product', {'product-id': $(this).val()})).then(function(response) {
            if (typeof response === 'object') {
                if (!response.error) {
                    _updateSelect($variant, response.data);
                }
            }
        });
    });
}

function _updateSelect($select, data) {
    $select.empty();
    $select.append('<option value>---</option>');

    $(data).each(function(index, element) {
        $select.append('<option value="' + element.id + '">' + element.name + '</option>');
    });
}

function _computePrice(saleUnit, content, weight, price) {
    var kgPrice, salePrice = 0;

    if (saleUnit === 'PC' || saleUnit === 'L') {
        salePrice = content * price;
    } else if (saleUnit === 'KG') {
        salePrice = weight * price;
    } else if (saleUnit === 'CO') {
        salePrice = price;
    }

    kgPrice = salePrice / weight;

    console.log(saleUnit);
    console.log(content);
    console.log(weight);

    return {'kgPrice': Math.round(kgPrice), 'salePrice': Math.round(salePrice)};
}

function _bindComputePrice($form) {
    var $saleUnit = $form.find('#vitalis_import_line_type_content_salesUnit');
    var $content = $form.find('#vitalis_import_line_type_content_unitQuantity');
    var $weight = $form.find('#vitalis_import_line_type_content_weight');
    var $kgPrice = $form.find('#vitalis_import_line_type_content_priceKg');
    var $salePrice = $form.find('#vitalis_import_line_type_content_priceVariant');
    var $price = $form.find('#vitalis_import_line_type_content_price');

    var onChange = function() {
        var prices = _computePrice($saleUnit.val(), $content.val(), $weight.val(), $price.val());

        $kgPrice.val(prices.kgPrice);
        $salePrice.val(prices.salePrice);
    };

    $saleUnit.off('keyup').keyup(onChange);
    $content.off('keyup').keyup(onChange);
    $weight.off('keyup').keyup(onChange);
}

function _initForm($form, aData, $modal) {
    $form.find('.sylius-autocomplete').autoCompleteTaxon();
    _bindVariantTunneling($form);
    _bindComputePrice($form);

    var $updateButton = $form.find('#vitalis_import_line_type_validateLine');
    var $submitButton = $form.find('#vitalis_import_line_type_submit');

    $updateButton.off('click').click(function(updateButtonEvent) {
        updateButtonEvent.stopPropagation();
        updateButtonEvent.preventDefault();

        var $ref = $(this);

        $form.parent().removeClass('error');
        $form.parent().removeClass('success');

        $ref.addClass('loading');
        $.post(Routing.generate(
            'app_admin_importline_update',
            {'import': aData.import.id, 'importline': aData.id}),
            $form.serialize()
        ).then(function(updateResponse) {
            $ref.removeClass('loading');

            if (typeof updateResponse === "object") {
                $form.parent().addClass(updateResponse.error ? 'error' : 'success');

                if (!updateResponse.error) {
                    $('#sg-datatables-importline_datatable').DataTable({retrieve:true}).ajax.reload(null, false);
                } else {
                    $form.replaceWith(updateResponse.form);
                    $form = $modal.find('form');

                    _initForm($form, aData, $modal);

                    $form.find('#vitalis_import_line_type_taxon').trigger('change');
                }
            }
        });
    });

    $submitButton.off('click').click(function(submitButtonEvent) {
        submitButtonEvent.stopPropagation();
        submitButtonEvent.preventDefault();

        var $ref = $(this);

        $form.parent().removeClass('error');
        $form.parent().removeClass('success');

        $ref.addClass('loading');

        $.post(Routing.generate(
            'app_admin_importline_validate',
            {'import': aData.import.id, 'importline': aData.id}),
            $form.serialize()
        ).then(function(submitResponse) {
            $ref.removeClass('loading');

            if (typeof submitResponse === "object") {
                $form.parent().addClass(submitResponse.error ? 'error' : 'success');

                if (!submitResponse.error) {
                    $('#sg-datatables-importline_datatable').DataTable({retrieve:true}).ajax.reload(null, false);
                    $modal.modal('hide');
                } else {
                    $form.replaceWith(submitResponse.form);
                    $form = $modal.find('form');

                    _initForm($form, aData, $modal);

                    $form.find('#vitalis_import_line_type_taxon').trigger('change');
                }
            }
        });
    });
}