// noinspection ES6ModulesDependencies
jQuery.each(["put", "delete"], function(i, method) {
    jQuery[method] = function(url, data, callback, type) {
        // noinspection ES6ModulesDependencies
        if (jQuery.isFunction(data) ) {
            type = type || callback;
            callback = data;
            data = undefined;
        }

        return jQuery.ajax({
            url: url,
            type: method,
            dataType: type,
            data: data,
            success: callback
        });
    };
});

var PageCheck = {
    debug: true,

    _call: function(requestType, uri, data, callback) {
        if (typeof showLoadingIndicator === 'function') {
            showLoadingIndicator();
        }

        jQuery[requestType](uri, data, function(response) {
            if (PageCheck.debug) {
                console.log(response);
            }

            if (typeof hideLoadingIndicator === 'function') {
                hideLoadingIndicator();
            }

            if (callback instanceof Function) {
                callback(response);
            }
        });
    },

    get: function(uri, callback) {
        PageCheck._call('get', uri, null, callback);
    },

    post: function(uri, data, callback) {
        PageCheck._call('post', uri, data, callback);
    },

    put: function(uri, data, callback) {
        PageCheck._call('put', uri, data, callback);
    },

    delete: function(uri, data, callback) {
        PageCheck._call('delete', uri, data, callback);
    },

    Taxon: {
        rename: function(id, name, callback) {
            PageCheck.put(Routing.generate('app_admin_page_check_rename_taxon', {taxonId: id}), {taxonName: name}, callback);
        },

        move: function(id, newTaxonId, oldTaxonId, callback) {
            PageCheck.put(Routing.generate('app_admin_page_check_move_taxon', {taxonId: id, newTaxonId: newTaxonId, oldTaxonId: oldTaxonId}), null, callback);
        },

        remove: function(id, callback) {
            PageCheck.delete(Routing.generate('app_admin_page_check_remove_taxon', {taxonId: id}), null, callback);
        },

        create: function(parentId, name, callback) {
            PageCheck.post(Routing.generate('app_admin_page_check_create_taxon', {taxonId: parentId}), {name: name}, callback);
        }
    },

    Product: {
        rename: function(id, name, callback) {
            PageCheck.put(Routing.generate('app_admin_page_check_rename_product', {productId: id}), {productName: name}, callback);
        },

        remove: function(id, callback) {
            PageCheck.delete(Routing.generate('app_admin_page_check_remove_product', {productId: id}), null, callback);
        },

        move: function(id, newTaxonId, oldTaxonId, callback) {
            PageCheck.put(
                Routing.generate('app_admin_page_check_move_product', {
                    productId: id,
                    newTaxonId: newTaxonId,
                    oldTaxonId: oldTaxonId
                }),
                null,
                callback
            );
        },

        disable: function(id, callback) {
            PageCheck.put(Routing.generate('app_admin_page_check_disable_product', {productId: id}), null, callback);
        },

        enable: function(id, callback) {
            PageCheck.put(Routing.generate('app_admin_page_check_enable_product', {productId: id}), null, callback);
        },

        create: function(parentId, name, callback) {
            PageCheck.post(
                Routing.generate('app_admin_page_check_create_product', {taxonId: parentId}),
                {name: name},
                callback
            );
        },

        paste: function(productId, variantIds, callback) {
            PageCheck.put(
                Routing.generate('app_admin_page_check_paste_variants_on_product', {productId: productId}),
                {variantIds: variantIds},
                callback
            );
        }
    },

    Variant: {
        paste: function(pasteOnVariantId, variantIds, callback) {
            PageCheck.put(
                Routing.generate('app_admin_page_check_paste_variants_on_variant', {variantId: pasteOnVariantId}),
                {variantIds: variantIds},
                callback
            );
        },

        rename: function(id, name, callback) {
            PageCheck.put(
                Routing.generate('app_admin_page_check_rename_variant', {variantId: id}),
                {variantName: name},
                callback
            );
        },

        move: function(id, newProductId, oldProductId, callback) {
            PageCheck.put(
                Routing.generate('app_admin_page_check_move_variant', {
                    variantId: id,
                    newProductId: newProductId,
                    oldProductId: oldProductId
                }),
                null,
                callback
            );
        },

        remove: function(id, callback) {
            PageCheck.delete(Routing.generate('app_admin_page_check_remove_variant', {variantId: id}), null, callback);
        },

        create: function(productId, callback) {
            PageCheck.post(Routing.generate('app_admin_page_check_create_variant', {productId: productId}), null, callback);
        },

        enable: function(variantId, callback) {
            PageCheck.put(Routing.generate('app_admin_page_check_enable_variant', {variantId: variantId}), null, callback);
        },

        disable: function(variantId, callback) {
            PageCheck.put(Routing.generate('app_admin_page_check_disable_variant', {variantId: variantId}), null, callback);
        },

        associateSupplier: function(variantId, supplierId, supplierLabel, callback) {
            PageCheck.post(
                Routing.generate('app_admin_page_check_associate_supplier', {variantId: variantId}),
                {supplierId: supplierId, label: supplierLabel},
                callback
            );
        },

        disassociateSupplier: function(variantId, informationsId, callback) {
            PageCheck.delete(Routing.generate('app_admin_page_check_disassociate_supplier', {variantId: variantId, informationsId: informationsId}), null, callback);
        },

        associate: function(variantId, supplierId, restaurantId, callback) {
            PageCheck.post(
                Routing.generate('app_admin_page_check_associate_variant', {variantId: variantId}),
                {supplierId: supplierId, restaurantId: restaurantId},
                callback
            );
        },

        disassociate(variantId, supplierId, restaurantId, callback) {
            PageCheck.delete(Routing.generate('app_admin_page_check_disassociate_variant', {variantId: variantId, supplierId: supplierId, restaurantId: restaurantId}), null, callback);
        }
    },

    Price: {
        create: function(productId, variantId, restaurant, supplier, kgPrice, unitPrice, date, callback) {
            PageCheck.post(
                Routing.generate('app_admin_page_check_create_price'),
                {
                    product_id: productId,
                    variant_id: variantId,
                    price_restaurant: restaurant,
                    price_supplier: supplier,
                    price_date: date,
                    kg_price: kgPrice,
                    unit_price: unitPrice
                },
                callback
            );
        },

        update: function(id, type, restaurant, supplier, kgPrice, unitPrice, date, callback) {
            PageCheck.put(
                Routing.generate('app_admin_page_check_update_price', {id: id, type: type}),
                {
                    price_restaurant: restaurant,
                    price_supplier: supplier,
                    price_date: date,
                    kg_price: kgPrice,
                    unit_price: unitPrice
                },
                callback
            );
        },

        remove: function(id, type, callback) {
            PageCheck.delete(Routing.generate('app_admin_page_check_remove_price', {id: id, type: type}), null, callback);
        },

        togglePromo: function(id, callback) {
            PageCheck.get(Routing.generate('app_admin_page_check_toggle_promo', {id: id}), callback);
        }
    },

    Supplier: {
        updateVariantLabel: function(informationsId, variantId, supplierReference, orderRef, supplierLabel, displayLabel, weight, unitQuantity, salesUnit, callback) {
            PageCheck.put(
                Routing.generate('app_admin_page_check_update_supplier_variant_label', {
                    informationsId: informationsId,
                    'variantId': variantId
                }),
                {supplierReference: supplierReference, orderRef: orderRef, supplierLabel: supplierLabel, displayLabel: displayLabel, weight: weight, unitQuantity: unitQuantity, salesUnit:salesUnit},
                callback
            );
        }
    },

    OptionValue: {
        create: function(optionCode, optionValue, callback) {
            PageCheck.post(
                Routing.generate('app_admin_page_check_create_option_value', {code: optionCode}),
                {value: optionValue},
                callback
            );
        }
    }
};