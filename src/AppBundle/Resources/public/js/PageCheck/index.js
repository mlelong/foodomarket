$(document).ready(function() {
    var $tree = $('#catalog-tree');
    var $search = $('#catalog-search');
    var $searchContainer = $('#catalog-search-container');
    var $catalogEditContainer = $('#catalog-edit-container');
    var $catalogEditFormContainer;
    var currentNodeId, activeTab;
    var currentY = 0;
    var priceHistoryData;
    var priceChart;

    $(window).scroll(function() {
        currentY = $(this).scrollTop();

        if ($catalogEditFormContainer !== undefined) {
            $catalogEditFormContainer.css('margin-top', currentY + "px");
        }
    });

    var colors = [["#69d2e7","#a7dbd8","#e0e4cc","#f38630","#fa6900"],["#fe4365","#fc9d9a","#f9cdad","#c8c8a9","#83af9b"],["#ecd078","#d95b43","#c02942","#542437","#53777a"],["#556270","#4ecdc4","#c7f464","#ff6b6b","#c44d58"],["#774f38","#e08e79","#f1d4af","#ece5ce","#c5e0dc"],["#e8ddcb","#cdb380","#036564","#033649","#031634"],["#490a3d","#bd1550","#e97f02","#f8ca00","#8a9b0f"],["#594f4f","#547980","#45ada8","#9de0ad","#e5fcc2"],["#00a0b0","#6a4a3c","#cc333f","#eb6841","#edc951"],["#e94e77","#d68189","#c6a49a","#c6e5d9","#f4ead5"],["#3fb8af","#7fc7af","#dad8a7","#ff9e9d","#ff3d7f"],["#d9ceb2","#948c75","#d5ded9","#7a6a53","#99b2b7"],["#ffffff","#cbe86b","#f2e9e1","#1c140d","#cbe86b"],["#efffcd","#dce9be","#555152","#2e2633","#99173c"],["#343838","#005f6b","#008c9e","#00b4cc","#00dffc"],["#413e4a","#73626e","#b38184","#f0b49e","#f7e4be"],["#ff4e50","#fc913a","#f9d423","#ede574","#e1f5c4"],["#99b898","#fecea8","#ff847c","#e84a5f","#2a363b"],["#655643","#80bca3","#f6f7bd","#e6ac27","#bf4d28"],["#00a8c6","#40c0cb","#f9f2e7","#aee239","#8fbe00"],["#351330","#424254","#64908a","#e8caa4","#cc2a41"],["#554236","#f77825","#d3ce3d","#f1efa5","#60b99a"],["#ff9900","#424242","#e9e9e9","#bcbcbc","#3299bb"],["#5d4157","#838689","#a8caba","#cad7b2","#ebe3aa"],["#8c2318","#5e8c6a","#88a65e","#bfb35a","#f2c45a"],["#fad089","#ff9c5b","#f5634a","#ed303c","#3b8183"],["#ff4242","#f4fad2","#d4ee5e","#e1edb9","#f0f2eb"],["#d1e751","#ffffff","#000000","#4dbce9","#26ade4"],["#f8b195","#f67280","#c06c84","#6c5b7b","#355c7d"],["#1b676b","#519548","#88c425","#bef202","#eafde6"],["#bcbdac","#cfbe27","#f27435","#f02475","#3b2d38"],["#5e412f","#fcebb6","#78c0a8","#f07818","#f0a830"],["#452632","#91204d","#e4844a","#e8bf56","#e2f7ce"],["#eee6ab","#c5bc8e","#696758","#45484b","#36393b"],["#f0d8a8","#3d1c00","#86b8b1","#f2d694","#fa2a00"],["#f04155","#ff823a","#f2f26f","#fff7bd","#95cfb7"],["#2a044a","#0b2e59","#0d6759","#7ab317","#a0c55f"],["#bbbb88","#ccc68d","#eedd99","#eec290","#eeaa88"],["#b9d7d9","#668284","#2a2829","#493736","#7b3b3b"],["#b3cc57","#ecf081","#ffbe40","#ef746f","#ab3e5b"],["#a3a948","#edb92e","#f85931","#ce1836","#009989"],["#67917a","#170409","#b8af03","#ccbf82","#e33258"],["#e8d5b7","#0e2430","#fc3a51","#f5b349","#e8d5b9"],["#aab3ab","#c4cbb7","#ebefc9","#eee0b7","#e8caaf"],["#300030","#480048","#601848","#c04848","#f07241"],["#ab526b","#bca297","#c5ceae","#f0e2a4","#f4ebc3"],["#607848","#789048","#c0d860","#f0f0d8","#604848"],["#a8e6ce","#dcedc2","#ffd3b5","#ffaaa6","#ff8c94"],["#3e4147","#fffedf","#dfba69","#5a2e2e","#2a2c31"],["#b6d8c0","#c8d9bf","#dadabd","#ecdbbc","#fedcba"],["#fc354c","#29221f","#13747d","#0abfbc","#fcf7c5"],["#1c2130","#028f76","#b3e099","#ffeaad","#d14334"],["#edebe6","#d6e1c7","#94c7b6","#403b33","#d3643b"],["#cc0c39","#e6781e","#c8cf02","#f8fcc1","#1693a7"],["#dad6ca","#1bb0ce","#4f8699","#6a5e72","#563444"],["#a7c5bd","#e5ddcb","#eb7b59","#cf4647","#524656"],["#fdf1cc","#c6d6b8","#987f69","#e3ad40","#fcd036"],["#5c323e","#a82743","#e15e32","#c0d23e","#e5f04c"],["#230f2b","#f21d41","#ebebbc","#bce3c5","#82b3ae"],["#b9d3b0","#81bda4","#b28774","#f88f79","#f6aa93"],["#3a111c","#574951","#83988e","#bcdea5","#e6f9bc"],["#5e3929","#cd8c52","#b7d1a3","#dee8be","#fcf7d3"],["#1c0113","#6b0103","#a30006","#c21a01","#f03c02"],["#382f32","#ffeaf2","#fcd9e5","#fbc5d8","#f1396d"],["#e3dfba","#c8d6bf","#93ccc6","#6cbdb5","#1a1f1e"],["#000000","#9f111b","#b11623","#292c37","#cccccc"],["#c1b398","#605951","#fbeec2","#61a6ab","#accec0"],["#8dccad","#988864","#fea6a2","#f9d6ac","#ffe9af"],["#f6f6f6","#e8e8e8","#333333","#990100","#b90504"],["#1b325f","#9cc4e4","#e9f2f9","#3a89c9","#f26c4f"],["#5e9fa3","#dcd1b4","#fab87f","#f87e7b","#b05574"],["#951f2b","#f5f4d7","#e0dfb1","#a5a36c","#535233"],["#413d3d","#040004","#c8ff00","#fa023c","#4b000f"],["#eff3cd","#b2d5ba","#61ada0","#248f8d","#605063"],["#2d2d29","#215a6d","#3ca2a2","#92c7a3","#dfece6"],["#cfffdd","#b4dec1","#5c5863","#a85163","#ff1f4c"],["#4e395d","#827085","#8ebe94","#ccfc8e","#dc5b3e"],["#9dc9ac","#fffec7","#f56218","#ff9d2e","#919167"],["#a1dbb2","#fee5ad","#faca66","#f7a541","#f45d4c"],["#ffefd3","#fffee4","#d0ecea","#9fd6d2","#8b7a5e"],["#a8a7a7","#cc527a","#e8175d","#474747","#363636"],["#ffedbf","#f7803c","#f54828","#2e0d23","#f8e4c1"],["#f8edd1","#d88a8a","#474843","#9d9d93","#c5cfc6"],["#f38a8a","#55443d","#a0cab5","#cde9ca","#f1edd0"],["#4e4d4a","#353432","#94ba65","#2790b0","#2b4e72"],["#0ca5b0","#4e3f30","#fefeeb","#f8f4e4","#a5b3aa"],["#a70267","#f10c49","#fb6b41","#f6d86b","#339194"],["#9d7e79","#ccac95","#9a947c","#748b83","#5b756c"],["#edf6ee","#d1c089","#b3204d","#412e28","#151101"],["#046d8b","#309292","#2fb8ac","#93a42a","#ecbe13"],["#4d3b3b","#de6262","#ffb88c","#ffd0b3","#f5e0d3"],["#fffbb7","#a6f6af","#66b6ab","#5b7c8d","#4f2958"],["#ff003c","#ff8a00","#fabe28","#88c100","#00c176"],["#fcfef5","#e9ffe1","#cdcfb7","#d6e6c3","#fafbe3"],["#9cddc8","#bfd8ad","#ddd9ab","#f7af63","#633d2e"],["#30261c","#403831","#36544f","#1f5f61","#0b8185"],["#d1313d","#e5625c","#f9bf76","#8eb2c5","#615375"],["#ffe181","#eee9e5","#fad3b2","#ffba7f","#ff9c97"],["#aaff00","#ffaa00","#ff00aa","#aa00ff","#00aaff"],["#c2412d","#d1aa34","#a7a844","#a46583","#5a1e4a"]];

    function setVariantDisplayName(id) {
        var tree = $tree.jstree(true);
        var variantNode = tree.get_node(id, false);
        var variantOptionValues = variantNode.original.optionValues;
        var variantName = [];
        var interestingOptions = ['UM', 'UC', 'OP', 'BI', 'CO', 'MQ'];

        for (var i in interestingOptions) {
            var interestingOption = interestingOptions[i];

            if (variantOptionValues !== undefined && variantOptionValues.hasOwnProperty(interestingOption)) {
                var optionValue = variantOptionValues[interestingOption];

                variantName.push('<span class="variant-option">' + interestingOption + '</span>: <span class="variant-option-value">' + optionValue.value + '</span>');
            }
        }

        if (variantNode.text.length < 2) {
            variantNode.text = '';
        } else {
            variantNode.text = variantNode.text  + ' | ' ;
        }
        variantName = variantNode.text + variantNode.original.weight + 'kg <span class="variant-infos">(' + variantName.join(', ') + ')</span>' + ' [' + id + ']';

        tree.set_text(variantNode, variantName);
    }

    function setNodeDisplayName(id)
    {
        var tree = $tree.jstree(true);
        var node = tree.get_node(id, false);
        var nodeName = node.text + ' [' + id + ']';

        tree.set_text(node, nodeName);
    }

    function initPriceChart(priceHistoryData) {
        var $priceSegment = $('#price-segment');
        var $priceSegmentHeader = $priceSegment.find('.header');
        var $priceForm = $priceSegment.find('#price-form');
        var $priceRestaurant = $priceForm.find('#price-restaurant');
        var $priceSupplier = $priceForm.find('#price-supplier');
        var $priceDate = $priceForm.find('#price-date');
        var $priceKg = $priceForm.find('#kg-price');
        var $priceUnit = $priceForm.find('#unit-price');
        var $priceAddModifyButton = $priceForm.find('#price-add-modify-button');
        var $priceRemoveButton = $priceForm.find('#price-remove-button');
        var createPrice = function() {
            var productId = $tree.jstree(true).get_node('v' + currentNodeId).parent.replace(/[tpv]+/, '');

            PageCheck.Price.create(
                productId,
                currentNodeId,
                $priceRestaurant.val(),
                $priceSupplier.val(),
                $priceKg.val(),
                $priceUnit.val(),
                $priceDate.val(),
                function (response) {
                    if (response.ok) {
                        loadVariantEditForm(currentNodeId);
                    } else {
                        alert(response.message);
                    }
                }
            );
        };

        priceHistoryData.options.onClick = function(event, elements) {
            if (elements.length === 0)
                return ;

            var element = priceChart.getElementAtEvent(event)[0];
            var pricePoint = priceHistoryData.data.datasets[element._datasetIndex].infos[element._index];

            $priceSegmentHeader.text("Modifier un prix pour " + pricePoint.restaurant_name + " chez " + pricePoint.supplier_name);
            $priceAddModifyButton.text(pricePoint.id > 0 ? "Modifier" : "Ajouter");

            $priceRestaurant.val(pricePoint.restaurant);
            $priceSupplier.val(pricePoint.supplier);
            $priceDate.val(pricePoint.date);
            $priceKg.val((pricePoint.kgPrice / 100).toFixed(2));
            $priceUnit.val((pricePoint.unitPrice / 100).toFixed(2));

            $priceRemoveButton.off('click');
            $priceAddModifyButton.off('click');

            if (pricePoint.id > 0) {
                $priceRemoveButton.show();

                $priceRemoveButton.click(function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    showConfirmModal(
                        'Suppression de point de tarif',
                        'Continuer avec la suppression du tarif pour '
                        + pricePoint.restaurant_name + ' chez ' + pricePoint.supplier_name
                        + ' du ' + pricePoint.date + ' ?',
                        function() {
                            PageCheck.Price.remove(pricePoint.id, pricePoint.type, function(response) {
                                if (response.ok) {
                                    loadVariantEditForm(currentNodeId);
                                } else {
                                    alert(response.message);
                                }
                            });
                        }
                    )
                });
            } else {
                $priceRemoveButton.hide();
            }

            // $priceSegment.css('border-top', '2px solid ' + element._view.borderColor);
            $priceSegment.find('.inverted.segment').css('background-color', element._view.borderColor);

            $priceAddModifyButton.click(function(e) {
                e.preventDefault();
                e.stopPropagation();

                if (pricePoint.id > 0) {
                    PageCheck.Price.update(
                        pricePoint.id,
                        pricePoint.type,
                        $priceRestaurant.val(),
                        $priceSupplier.val(),
                        $priceKg.val(),
                        $priceUnit.val(),
                        $priceDate.val(),
                        function (response) {
                            if (response.ok) {
                                loadVariantEditForm(currentNodeId);
                            } else {
                                alert(response.message);
                            }
                        }
                    );
                } else {
                    createPrice();
                }
            });
        };

        $priceAddModifyButton.off('click').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            createPrice();
        });

        for (var i = 0, j = 0; i < priceHistoryData.data.datasets.length; ++i) {
            var color = colors[j % colors.length].concat(colors[(j + 1) % colors.length]);

            priceHistoryData.data.datasets[i].backgroundColor = color[0];
            priceHistoryData.data.datasets[i].borderColor = color[0];
            priceHistoryData.data.datasets[i].pointBackgroundColor = color[0];
            priceHistoryData.data.datasets[i].pointBorderColor = color[0];
            priceHistoryData.data.datasets[i].pointHoverBackgroundColor = color[0];
            priceHistoryData.data.datasets[i].pointHoverBorderColor = color[0];
            priceHistoryData.data.datasets[i].cubicInterpolationMode = 'monotone';

            // Since unit price immediately follows kg price, let's use the same color for readability
            if (i % 2) {
                ++j;
            }
        }
    }

    function initSupplierAssociationForm(variantId) {
        var $associationForm = $('#pair-supplier-form');
        var $assocSupplier = $associationForm.find('#pair-supplier');
        var $assocLabel = $associationForm.find('#pair-supplier-label');
        var $assocSubmit = $associationForm.find('#pair-supplier-button');

        $assocSubmit.off('click').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var supplierId = $assocSupplier.val();
            var supplierLabel = $assocLabel.val();

            if (supplierId === undefined || supplierLabel.trim().length === 0) {
                alert("Remplissez tous les champs !");
            } else {
                PageCheck.Variant.associateSupplier(variantId, supplierId, supplierLabel, function(response) {
                    if (response.ok) {
                        loadVariantEditForm(variantId);
                    } else {
                        alert(response.message);
                    }
                });
            }
        });

    }

    function initAssociationForm(variantId) {
        var $associationForm = $('#association-form');
        var $assocSupplier = $associationForm.find('#assoc-supplier');
        var $assocRestaurant = $associationForm.find('#assoc-restaurant');
        var $assocSubmit = $associationForm.find('#associate-button');

        $assocSubmit.off('click').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var supplierId = $assocSupplier.val();
            var restaurantId = $assocRestaurant.val();

            if (supplierId === undefined || restaurantId === undefined) {
                alert("Sélectionnez un fournisseur et un restaurant !")
            } else {
                PageCheck.Variant.associate(variantId, supplierId, restaurantId, function (response) {
                    if (response.ok) {
                        loadVariantEditForm(variantId);
                    } else {
                        alert(response.message);
                    }
                });
            }
        });
    }

    function initSupplierTab(variantId) {
        $catalogEditContainer.find('.edit-supplier-label').off('click').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $editLabelButton = $(this);

            showConfirmModal('Mise à jour des informations de matching fournisseur', "Continuer avec la mise à jour des informations fournisseur ?", function() {
                PageCheck.Supplier.updateVariantLabel(
                    $editLabelButton.data('informationsid'),
                    variantId,
                    $editLabelButton.closest('tr').find('.supplier-file-reference-input').val(),
                    $editLabelButton.closest('tr').find('.supplier-file-order-ref-input').val(),
                    $editLabelButton.closest('tr').find('.supplier-file-label-input').val(),
                    $editLabelButton.closest('tr').find('.supplier-display-label-input').val(),
                    $editLabelButton.closest('tr').find('.supplier-weight-input').val(),
                    $editLabelButton.closest('tr').find('.supplier-unit-quantity-input').val(),
                    $editLabelButton.closest('tr').find('.supplier-sales-unit-input').val(),
                    function(response) {
                        if (response.ok) {
                            loadVariantEditForm(variantId);
                        } else {
                            alert(response.message);
                        }
                    }
                );
            });
        });

        $catalogEditContainer.find('.remove-supplier-assoc').off('click').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $removeAssocButton = $(this);

            showConfirmModal("Suppression d'association fournisseur", "Enlever l'association entre cette variante et ce fournisseur ?", function() {
                PageCheck.Variant.disassociateSupplier(variantId, $removeAssocButton.data('informationsid'), function(response) {
                    if (response.ok) {
                        loadVariantEditForm(variantId);
                    } else {
                        alert(response.message);
                    }
                });
            });

        });
    }

    function initRestaurantTab(variantId) {
        $catalogEditContainer.find('.remove-supplier-restaurant-assoc').off('click').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $removeAssocButton = $(this);

            showConfirmModal("Suppression d'association fournisseur-restaurant", "Enlever l'association variante - fournisseur - restaurant ?", function() {
                PageCheck.Variant.disassociate(variantId, $removeAssocButton.data('supplier'), $removeAssocButton.data('restaurant'), function(response) {
                    if (response.ok) {
                        loadVariantEditForm(variantId);
                    } else {
                        alert(response.message);
                    }
                });
            });
        });

        $catalogEditContainer.find('.toggle-promo').off('click').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var priceId = $(this).data('price-id');

            PageCheck.Price.togglePromo(priceId, function(response) {
                if (response.ok) {
                    loadVariantEditForm(variantId);
                } else {
                    alert(response.message);
                }
            });
        });
    }

    function displayVariantPrice() {
        var options = {};

        $('#check_product_variant_optionValues').find('select').each(function() {
            var $elem = $(this);

            if ($elem.val() !== undefined && $elem.val() !== '') {
                options[$elem.attr('id').substr($elem.attr('id').lastIndexOf('_') + 1)] = {value: $elem.val(), text: $elem.find('option:selected').text()};
            }
        });

        var $variantPriceDisplay = $('#variant-price-display');
        $variantPriceDisplay.html('');

        if (options.hasOwnProperty('UC')) {
            var activePrices = JSON.parse($('#variant-active-prices').html());
            var $list = $('<div class="ui list"></div>');

            $(activePrices).each(function(index, activePrice) {
                var product = {
                    productPrice: activePrice.kgPrice,
                    productPriceUnit: activePrice.unitPrice,
                    unitQuantity: (options.UQ)?options.UQ.text:'',
                    unitContentQuantity: (options.UCQ)?options.UCQ.text:'',
                    unitContentMeasurement: (options.UCM)?options.UCM.text:''
                };

                let price, unit, pricePerUnit, unitOfPricePerUnit;

                switch (options.UC.value) {
                    case 'KG':
                    case 'ORDER_KG':
                        price = (product.productPrice / 100).toFixed(2);
                        unit = 'kg';
                        break;
                    case 'L':
                    case 'ORDER_L':
                        price = (product.productPrice / 100).toFixed(2);
                        unit = 'L';
                        break;
                    case 'PC':
                    case 'ORDER_PC':
                        pricePerUnit = (product.productPrice / 100).toFixed(2);
                        unitOfPricePerUnit = 'kg';

                        price = (product.productPriceUnit / 100 / product.unitQuantity).toFixed(2);
                        unit = 'pc';
                        break;
                    case 'CO':
                    case 'ORDER_CO':
                    case 'COLIS':
                    case 'ORDER_COLIS':
                        pricePerUnit = (product.productPriceUnit / 100 / product.unitQuantity).toFixed(2);
                        unitOfPricePerUnit = product.unitContentQuantity > 1 ? product.unitContentQuantity + ' ' + product.unitContentMeasurement : product.unitContentMeasurement;

                        price = (product.productPriceUnit / 100).toFixed(2);
                        unit = 'COLIS';
                        break;
                    default:
                        price = (product.productPrice / 100).toFixed(2);
                        unit = 'kg';
                        break;
                }

                var extra = '';

                if (unit !== 'kg' && unit !== 'L') {
                    extra = ` (soit ${pricePerUnit}€/${unitOfPricePerUnit})`;
                }

                $list.append(`<div class="item"><strong>${activePrice.supplier}: </strong><span>${price}€<span>/${unit}${extra}</span></span></div>`);
            });

            $variantPriceDisplay.append($list);
        } else {
            $variantPriceDisplay.html('Il faut préciser une unité de commande !');
        }
    }

    function initOptionTab(variantId) {
        $('#variant-options-container').find('label').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $label = $(this);

            var labelFor = $label.prop('for');
            var optionCode = labelFor.substr(labelFor.lastIndexOf('_') + 1);
            var optionName = $label.text();

            showCreateOptionValueModal(optionName, optionCode, function(optionCode, optionValue) {
                PageCheck.OptionValue.create(optionCode, optionValue, function(response) {
                    if (response.ok) {
                        loadVariantEditForm(variantId);
                    } else {
                        alert(response.message);
                    }
                });
            });
        });

        $('#check_product_variant_optionValues').find('select').change(function() {
            displayVariantPrice();
        });

        displayVariantPrice();
    }

    function loadVariantEditForm(variantId) {
        showLoadingIndicator();

        var productId = $tree.jstree(true).get_node('v' + variantId).parent.replace(/[tpv]+/, '');
        var newActiveTab = $catalogEditContainer.find('#variant-edit-tab .item.active').data('tab');

        if (newActiveTab !== undefined)
            activeTab = newActiveTab;

        $.get(Routing.generate('app_admin_page_check_variant_edit', {id: variantId})).then(function(response) {
            hideLoadingIndicator();

            $catalogEditContainer.html(response);

            initOptionTab(variantId);
            initAssociationForm(variantId);
            initSupplierAssociationForm(variantId);
            initSupplierTab(variantId);
            initRestaurantTab(variantId);

            $catalogEditFormContainer = $catalogEditContainer.find('#variant-edit-form-container');
            $catalogEditFormContainer.css('margin-top', currentY + 'px');

            priceHistoryData = JSON.parse($catalogEditContainer.find('#variant-price-history').html());
            initPriceChart(priceHistoryData);
            priceChart = new Chart('chart', priceHistoryData);

            var $tabItems = $catalogEditContainer.find('#variant-edit-tab .item');

            $tabItems.tab();

            if (activeTab === undefined)
                activeTab = 'options';

            $tabItems.tab('change tab', activeTab);

            var $variantEditForm = $catalogEditContainer.find('form');

            var onSubmit = function(event) {
                event.preventDefault();
                event.stopPropagation();

                activeTab = $catalogEditContainer.find('#variant-edit-tab .item.active').data('tab');

                showLoadingIndicator();

                $.post(Routing.generate('app_admin_page_check_variant_edit', {id: variantId}), $variantEditForm.serialize(), function(response) {
                    $catalogEditContainer.html(response);

                    initOptionTab(variantId);
                    initAssociationForm(variantId);
                    initSupplierAssociationForm(variantId);
                    initSupplierTab(variantId);
                    initRestaurantTab(variantId);

                    $catalogEditFormContainer = $catalogEditContainer.find('#variant-edit-form-container');
                    $catalogEditFormContainer.css('margin-top', currentY + 'px');

                    priceHistoryData = JSON.parse($catalogEditContainer.find('#variant-price-history').html());
                    initPriceChart(priceHistoryData);
                    priceChart = new Chart('chart', priceHistoryData);

                    $variantEditForm = $catalogEditContainer.find('form');
                    $variantEditForm.on('submit', onSubmit);

                    $tabItems = $catalogEditContainer.find('#variant-edit-tab .item');

                    $tree.on('refresh_node.jstree', function() {
                        $tabItems.tab('change tab', activeTab);
                        $tree.off('refresh_node.jstree');

                        hideLoadingIndicator();
                    });

                    $tree.jstree(true).refresh_node('p' + productId);
                });
            };

            $variantEditForm.on('submit', onSubmit);
        });
    }

    function loadProductEditForm(productId)
    {
        showLoadingIndicator();

        $.get(Routing.generate('app_admin_page_check_product_edit', {id: productId})).then(function(response) {
            $catalogEditContainer.html(response);

            var $tabItems = $catalogEditContainer.find('#product-edit-tab .item');

            $tabItems.tab();

            var $taxonTree = $catalogEditContainer.find("#taxon-tree");
            var taxonTreeJson = JSON.parse($catalogEditContainer.find('#taxon-tree-json').text());

            $taxonTree
                .on('check_node.jstree', function() {
                    $taxonTree.jstree(true).check_node($taxonTree.jstree(true).get_undetermined());
                })
                .on('uncheck_node.jstree', function(event, data) {
                    $taxonTree.jstree(true).uncheck_node($taxonTree.jstree(true).get_checked_descendants(data.node.id));
                })
                .jstree({
                    core: {
                        data: [taxonTreeJson]
                    },
                    plugins: ['checkbox'],
                    checkbox: {
                        three_state: false,
                        cascade: 'undetermined',
                        tie_selection: false
                    }
                });

            var onSubmit = function(e) {
                e.preventDefault();
                e.stopPropagation();

                var $form = $catalogEditContainer.find('form');
                var data = new FormData($form[0]);

                $.ajax({
                    url: $form.prop('action'),
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    type: 'POST',
                    success: function(response) {
                        $catalogEditContainer.html(response);
                        $catalogEditContainer.find('#submit-product-picture').off('click').click(onSubmit);
                    }
                });
            };

            $catalogEditContainer.find('#submit-product-picture').off('click').click(onSubmit);

            $catalogEditContainer.find('#update-categories-button').off('click').click(function(e) {
                e.preventDefault();

                var taxons = $taxonTree.jstree(true).get_checked();

                $.post(Routing.generate('app_admin_page_check_product_taxons_update', {id: productId}), {taxons: taxons}).then(function(response) {
                    if (response.ok) {
                        loadProductEditForm(productId);
                    } else {
                        alert(response);
                    }
                });
            });

            hideLoadingIndicator();
        });
    }

    $tree
        .on('select_node.jstree', function(node, selected) {
            var nodeType = selected.node.type;
            var nodeId = selected.node.original.entityId;

            if (currentNodeId === nodeId) {
                return ;
            }

            currentNodeId = nodeId;

            switch(nodeType) {
                case 'v':
                    loadVariantEditForm(nodeId);
                    break ;
                case 'p':
                    loadProductEditForm(nodeId);
                    break ;
                default:
                    $catalogEditContainer.html('');
                    break ;
            }
        })
        .on('move_node.jstree', function(event, data) {
            var parentNodeId = data.parent.replace(/[tpv]+/, '');
            var oldParentNodeId = data.old_parent.replace(/[tpv]+/, '');
            var tree = $tree.jstree(true);

            switch (data.node.type) {
                case 'v':
                    PageCheck.Variant.move(data.node.original.entityId, parentNodeId, oldParentNodeId, function(response) {
                        if (response.ok) {
                            tree.refresh_node(data.old_parent);
                            tree.refresh_node(data.parent);
                        } else {
                            alert(response.message);
                        }
                    });
                    break ;
                case 'p':
                    PageCheck.Product.move(data.node.original.entityId, parentNodeId, oldParentNodeId, function(response) {
                        if (!response.ok) {
                            alert(response.message);
                        }
                    });
                    break ;
                case 't':
                    PageCheck.Taxon.move(data.node.original.entityId, parentNodeId, oldParentNodeId, function(response) {
                        if (!response.ok) {
                            alert(response.message);
                        }
                    });
                    break ;
            }
        })
        .on('create_node.jstree', function(event, node) {
            var inst = $tree.jstree(true);

            inst.deselect_node(node.parent, true);
            inst.select_node(node.node.id);
            inst.refresh_node(node.parent);
        })
        .on('load_node.jstree', function(event, node) {
            if (node.status) {
                if (node.node.type === 't') {
                    $(node.node.children).each(function(index, element) {
                        setNodeDisplayName(element);
                    });
                }

                if (node.node.type === 'p') {
                    $(node.node.children).each(function(index, element) {
                        setVariantDisplayName(element);
                    });
                }
            }
        })
        .on('after_open.jstree', function(event, node) {
            if (node.node.type === 'p') {
                $(node.node.children).each(function(index, element) {
                    var $variantNode = $('#' + element);

                    $variantNode.popup({
                        position: 'right center'
                    });
                });
            }
        })
        .on('rename_node.jstree', function(event, node) {
            switch (node.node.type) {
                case 't':
                    PageCheck.Taxon.rename(node.node.original.entityId, node.text, function(response) {
                        if (response.ok) {
                            setNodeDisplayName(node.node.id);
                        } else {
                            alert(response.message);
                        }
                    });
                    break ;
                case 'p':
                    PageCheck.Product.rename(node.node.original.entityId, node.text, function(response) {
                        if (response.ok) {
                            setNodeDisplayName(node.node.id);
                        } else {
                            alert(response.message);
                        }
                    });
                    break ;
                case 'v':
                    PageCheck.Variant.rename(node.node.original.entityId, node.text, function(response) {
                        if (response.ok) {
                            setVariantDisplayName(node.node.id);
                        } else {
                            alert(response.message);
                        }
                    });
                    break ;
            }
        })
        .on('paste.jstree', function(event, node) {
            var inst = $tree.jstree(true);
            inst.refresh_node(inst.get_node(node.parent));
        })
        .on('search.jstree', function() {
            $searchContainer.removeClass('loading');
            hideLoadingIndicator();
        })
        .jstree({
                core: {
                    data: {
                        url: function(node) {
                            return Routing.generate('app_admin_page_check_tree_leaf', {
                                type: node.type !== undefined ? node.type : 't',
                                id: node.original !== undefined ? node.original.entityId : node.id
                            });
                        }
                    },
                    check_callback: true
                },
                types: {
                    t: {
                        icon: 'fa fa-folder',
                        valid_children: ['t', 'p']
                    },
                    p: {
                        icon: 'fa fa-object-group',
                        valid_children: ['v']
                    },
                    v: {
                        icon: 'fa fa-object-ungroup',
                        valid_children: []
                    }
                },
                search: {
                    show_only_matches: true,
                    show_only_matches_children: true,
                    ajax: {
                        url: Routing.generate('app_admin_page_check_tree_search'),
                        dataType: 'json',
                        method: 'GET'
                    },
                },
                massload: {
                    url: function() {
                        return Routing.generate('app_admin_page_check_tree_leaves');
                    },
                    data: function(nodes) {
                        return {ids: nodes.join(',')};
                    }
                },
                plugins: ['search', 'unique', 'dnd', 'contextmenu', 'types', 'massload'],
                // plugins: ['search', 'unique', 'dnd', 'contextmenu', 'types', 'state', 'massload'],
                contextmenu: {
                    items: function(node) {
                        var tree = $tree.jstree(true);

                        var isNodeLoaded = node.type === 'v' || tree.is_loaded(node);
                        var hasChildren = node.children.length > 0;
                        var removeTitle = '';

                        switch (node.type) {
                            case 't':
                                var isMainTaxon = node.parent === '#';
                                var isOpen = tree.is_open(node);

                                if (isMainTaxon) {
                                    removeTitle = 'Impossible de supprimer le taxon principal !';
                                } else if (!isNodeLoaded) {
                                    removeTitle = 'Déplier le taxon avant de pouvoir le supprimer';
                                } else if (hasChildren) {
                                    removeTitle = 'Impossible de supprimer le taxon car des produits/taxons y sont attachés';
                                }

                                return {
                                    refresh: {
                                        label: 'Rafraîchir',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            inst.refresh_node(inst.get_node(data.reference));
                                        }
                                    },
                                    toggle: {
                                        label: isOpen ? 'Replier tout' : 'Déplier tout',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);

                                            if (isOpen) {
                                                inst.close_all(inst.get_node(data.reference));
                                            } else {
                                                inst.open_all(inst.get_node(data.reference));
                                            }
                                        }
                                    },
                                    rename: {
                                        label: 'Renommer ce taxon',
                                        _disabled: isMainTaxon,
                                        title: isMainTaxon ? 'Impossible de renommer le taxon principal' : '',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            inst.edit(inst.get_node(data.reference));
                                        }
                                    },
                                    remove: {
                                        label: 'Supprimer ce taxon',
                                        _disabled: isMainTaxon || !isNodeLoaded || hasChildren,
                                        title: removeTitle,
                                        action: function(data) {
                                            showConfirmModal('Suppression de taxon', 'Continuer avec la suppression du taxon "' + node.text + '" ?', function() {
                                                PageCheck.Taxon.remove(node.original.entityId, function(response) {
                                                    if (response.ok) {
                                                        var inst = $.jstree.reference(data.reference);
                                                        inst.delete_node(inst.get_node(data.reference));
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                });
                                            });
                                        }
                                    },
                                    createTaxon: {
                                        label: 'Nouveau taxon',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            var parentNode = inst.get_node(data.reference);

                                            showCreateTaxonModal(parentNode.text, function(newTaxonName) {
                                                PageCheck.Taxon.create(parentNode.original.entityId, newTaxonName, function(response) {
                                                    if (response.ok) {
                                                        inst.create_node(
                                                            parentNode.id,
                                                            {id: 't' + response.id, entityId: response.id, text: newTaxonName, type: 't'},
                                                            'first'
                                                        );
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                });
                                            });
                                        }
                                    },
                                    createProduct: {
                                        label: 'Nouveau produit',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            var parentNode = inst.get_node(data.reference);

                                            showCreateProductModal(parentNode.text, function(newProductName) {
                                                PageCheck.Product.create(parentNode.original.entityId, newProductName, function(response) {
                                                    if (response.ok) {
                                                        inst.create_node(
                                                            parentNode.id,
                                                            {id: 'p' + response.id, entityId: response.id, text: newProductName, type: 'p'},
                                                            'first'
                                                        );
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                });
                                            });
                                        }
                                    }
                                };
                            case 'p':
                                var isProductEnabled = node.original.enabled;

                                if (!isNodeLoaded) {
                                    removeTitle = 'Déplier le produit avant de pouvoir le désactiver';
                                }

                                return {
                                    refresh: {
                                        label: 'Rafraîchir',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            inst.refresh_node(inst.get_node(data.reference));
                                        }
                                    },
                                    rename: {
                                        label: 'Renommer ce produit',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            inst.edit(inst.get_node(data.reference));
                                        }
                                    },
                                    remove: {
                                        label: 'Supprimer ce produit',
                                        action: function(data) {
                                            showConfirmModal('Suppression de produit', 'Continuer avec la suppression du produit "' + node.text + '" ?', function() {
                                                PageCheck.Product.remove(node.original.entityId, function(response) {
                                                    if (response.ok) {
                                                        var inst = $.jstree.reference(data.reference);
                                                        inst.refresh_node(node.parent);
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                });
                                            });
                                        }
                                    },
                                    toggle: {
                                        label: isProductEnabled ? 'Désactiver ce produit' : 'Activer ce produit',
                                        action: function(data) {
                                            if (isProductEnabled) {
                                                showConfirmModal('Désactivation de produit', "Continuer avec la désactivation du produit `" + node.text + "` ?", function() {
                                                    PageCheck.Product.disable(node.original.entityId, function(response) {
                                                        if (response.ok) {
                                                            var inst = $.jstree.reference(data.reference);
                                                            inst.refresh_node(node.parent);
                                                        } else {
                                                            alert(response.message);
                                                        }
                                                    })
                                                });
                                            } else {
                                                showConfirmModal('Activation de produit', "Continuer avec l'activation du produit `" + node.text + "` ?", function() {
                                                    PageCheck.Product.enable(node.original.entityId, function(response) {
                                                        if (response.ok) {
                                                            var inst = $.jstree.reference(data.reference);
                                                            inst.refresh_node(node.parent);
                                                        } else {
                                                            alert(response.message);
                                                        }
                                                    })
                                                });
                                            }
                                        }
                                    },
                                    create: {
                                        label: 'Créer une variante',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            var parentNode = inst.get_node(data.reference);

                                            showConfirmModal('Nouvelle variante', 'Créer une nouvelle variante du produit ' + parentNode.text + ' ?', function() {
                                                PageCheck.Variant.create(parentNode.original.entityId, function(response) {
                                                    if (response.ok) {
                                                        inst.create_node(
                                                            parentNode.id,
                                                            {id: 'v' + response.id, entityId: response.id, text: parentNode.text, type: 'v'},
                                                            'first'
                                                        );
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                });
                                            });
                                        }
                                    },
                                    paste: {
                                        label: 'Coller',
                                        _disabled: !tree.can_paste(),
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            var node = inst.get_node(data.reference);
                                            var buffer = inst.get_buffer();

                                            var productId = node.original.entityId;
                                            var variantIds = buffer.node.map(function(variant) {
                                                return variant.original.entityId;
                                            });

                                            PageCheck.Product.paste(productId, variantIds, function(response) {
                                                if (response.ok) {
                                                    tree.paste(node);
                                                } else {
                                                    alert(response.message);
                                                }
                                            });
                                        }
                                    }
                                };
                            case 'v':
                                var isVariantEnabled = node.original.enabled;

                                return {
                                    rename: {
                                        label: 'Renommer cette variante',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            var node = inst.get_node(data.reference);

                                            node.text = node.original.text;
                                            inst.edit(node);
                                        }
                                    },
                                    remove: {
                                        label: 'Supprimer cette variante',
                                        action: function(data) {
                                            showConfirmModal('Suppression de variante', 'Continuer avec la suppression de la variante "' + node.text + '" ?', function() {
                                                PageCheck.Variant.remove(node.original.entityId, function(response) {
                                                    if (response.ok) {
                                                        var inst = $.jstree.reference(data.reference);
                                                        inst.delete_node(inst.get_node(data.reference));
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                });
                                            });
                                        }
                                    },
                                    copy: {
                                        label: 'Copier',
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            inst.copy(inst.get_node(data.reference));
                                        }
                                    },
                                    paste: {
                                        label: 'Coller',
                                        _disabled: !tree.can_paste(),
                                        action: function(data) {
                                            var inst = $.jstree.reference(data.reference);
                                            var node = inst.get_node(data.reference);
                                            var buffer = inst.get_buffer();

                                            var pasteOnVariantId = node.original.entityId;
                                            var variantIds = buffer.node.map(function(variant) {
                                                return variant.original.entityId;
                                            });

                                            PageCheck.Variant.paste(pasteOnVariantId, variantIds, function(response) {
                                                if (response.ok) {
                                                    tree.paste(node);
                                                } else {
                                                    alert(response.message);
                                                }
                                            });
                                        }
                                    },
                                    toggle: {
                                        label: isVariantEnabled ? 'Désactiver cette variante' : 'Activer cette variante',
                                        action: function(data) {
                                            if (isVariantEnabled) {
                                                showConfirmModal('Désactivation de variante', 'Continuer avec la désactivation de la variante ?', function() {
                                                    PageCheck.Variant.disable(node.original.entityId, function(response) {
                                                        if (response.ok) {
                                                            var inst = $.jstree.reference(data.reference);
                                                            inst.refresh_node(node.parent);
                                                        } else {
                                                            alert(response.message);
                                                        }
                                                    })
                                                });
                                            } else {
                                                showConfirmModal('Activation de variante', "Continuer avec l'activation de la variante `" + node.text + "` ?", function() {
                                                    PageCheck.Variant.enable(node.original.entityId, function(response) {
                                                        if (response.ok) {
                                                            var inst = $.jstree.reference(data.reference);
                                                            inst.refresh_node(node.parent);
                                                        } else {
                                                            alert(response.message)
                                                        }
                                                    });
                                                });
                                            }
                                        }
                                    }
                                };
                        }
                    }
                }
            }
        );

    $search.keypress(function(e) {
        if (e.which == 13) {
            $searchContainer.addClass('loading');
            showLoadingIndicator();
            $tree.jstree(true).search($search.val());
        }
    });
});