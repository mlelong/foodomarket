function showLoadingIndicator() {
    $('#page-check-dimmer').dimmer('show');
}

function hideLoadingIndicator() {
    $('#page-check-dimmer').dimmer('hide');
}

function showConfirmModal(title, message, onApprove, onDeny) {
    var $confirmModal = $('#page-check-confirm-modal');

    $confirmModal.find('.header').text(title);
    $confirmModal.find('.content > p').text(message);

    $confirmModal.modal({
        closable: true,
        onDeny: function() {
            if (onDeny instanceof Function) {
                onDeny();
            }

            return true;
        },
        onApprove: function() {
            if (onApprove instanceof Function) {
                onApprove();
            }

            return true;
        }
    }).modal('show');
}

function showCreateTaxonModal(parentTaxonName, onApprove, onDeny) {
    var $createTaxonModal = $('#page-check-create-taxon-modal');

    $createTaxonModal.find('#taxon-parent-name').text(parentTaxonName);

    $createTaxonModal.modal({
        closable: true,
        onDeny: function() {
            if (onDeny instanceof Function) {
                onDeny();
            }

            return true;
        },
        onApprove: function() {
            if (onApprove instanceof Function) {
                var newTaxonName = $createTaxonModal.find('#taxon-name').val();

                if (newTaxonName === undefined || newTaxonName === null || newTaxonName.trim().length === 0) {
                    newTaxonName = 'Nouveau taxon';
                }

                onApprove(newTaxonName);
            }

            return true;
        }
    }).modal('show');
}

function showCreateProductModal(parentTaxonName, onApprove, onDeny) {
    var $createProductModal = $('#page-check-create-product-modal');

    $createProductModal.find('#taxon-parent-name').text(parentTaxonName);

    $createProductModal.modal({
        closable: true,
        onDeny: function() {
            if (onDeny instanceof Function) {
                onDeny();
            }

            return true;
        },
        onApprove: function() {
            if (onApprove instanceof Function) {
                var newProductName = $createProductModal.find('#product-name').val();

                if (newProductName === undefined || newProductName === null || newProductName.trim().length === 0) {
                    newProductName = 'Nouveau produit';
                }

                onApprove(newProductName);
            }

            return true;
        }
    }).modal('show');
}

function showCreateOptionValueModal(optionName, optionCode, onApprove, onDeny) {
    var $createOptionValueModal = $('#page-check-create-option-value');

    $createOptionValueModal.find('#option-name').text(optionName);
    $createOptionValueModal.find('#option-code').val(optionCode);
    var $optionValue = $createOptionValueModal.find('#option-value');

    $optionValue.parent().removeClass('error');
    $optionValue.val('');

    $createOptionValueModal.modal({
        closable: true,
        onDeny: function() {
            if (onDeny instanceof Function) {
                onDeny();
            }

            return true;
        },
        onApprove: function() {
            var optionValue = $optionValue.val();

            if (optionValue === undefined || optionValue === null || optionValue.length === 0) {
                $optionValue.parent().addClass('error');

                return false;
            } else {
                $optionValue.parent().removeClass('error');

                if (onApprove instanceof Function) {
                    onApprove(optionCode, optionValue);
                }
            }

            return true;
        }
    }).modal('show');
}