$.arrayIntersect = function(a, b) {
    return $.grep(a, function(i) {
        return $.inArray(i, b) > -1;
    });
};

$(document).ready(function() {
    $('.filters').click(function(e) {
        if (e.target.value === '0') {
            if (e.target.checked) {
                $(e.target).closest('.filter-row').find('.filter[value!="0"]').prop('checked', false);
            }
        } else {
            $(e.target).closest('.filter-row').find('.filter[value="0"]').prop('checked', false);
        }

        var selectedFilters = $.map($(this).find('.filter-row'), function(filterRow) {
            var $filterRow = $(filterRow);

            var filterValues = $.map($filterRow.find('.filter:selected,.filter:checked,.filter[name="options"]'), function(filterElem) {
                return $(filterElem).val().split(',');
            });

            return {'tag':$filterRow.data('tag'),'values':filterValues};
        });

        $('.filterable-row').each(function() {
            var $this = $(this);
            var show = true;

            $(selectedFilters).each(function(index, selectedFilter) {
                var all = $.inArray('0', selectedFilter.values) > -1;
                var emptySelection = selectedFilter.values.length === 0 || selectedFilter.values[0] === '';
                var data = String($this.data(selectedFilter.tag)).split(/,/);

                if (show) {
                    if ($.arrayIntersect(data, selectedFilter.values).length > 0)
                        show = true;
                    else if (emptySelection)
                        show = true;
                    else show = all;
                }
            });

            show ? $this.show() : $this.hide();
        });
    });
});