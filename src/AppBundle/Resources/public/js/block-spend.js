var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host;

$.get(baseUrl + '/bundles/app/js/100.json', function (colors) {
    data.datasets[0].backgroundColor = colors[0].concat(colors[1]);
    var ctx = "spend-chart";
    var spendChart = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: {
            cutoutPercentage: 10,
            pieceLabel: {
                mode: 'percentage',
                precision: 2,
                fontColor: '#fff',
                fontStyle: 'bold',
                fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"
            }
        }
    });
});
