var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host;

$.get(baseUrl + '/bundles/app/js/100.json', function (colors) {
    for (var i = 0, j = 0; i < trendData.datasets.length; ++i) {
        var color = colors[j % colors.length].concat(colors[(j + 1) % colors.length]);

        trendData.datasets[i].backgroundColor = color[0];
        trendData.datasets[i].borderColor = color[0];
        trendData.datasets[i].pointBackgroundColor = color[0];
        trendData.datasets[i].pointBorderColor = color[0];
        trendData.datasets[i].pointHoverBackgroundColor = color[0];
        trendData.datasets[i].pointHoverBorderColor = color[0];
        trendData.datasets[i].cubicInterpolationMode = 'monotone';
        ++j;
    }

    var trendChart = new Chart('product-trend', {
        type: 'line',
        data: trendData,
        options: {
            responsive: true,
            legend: {
                position: 'bottom'
            },
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Dates'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Prix'
                    }
                }]
            }
        }
    });
});

function reloadTrends() {
    var period = $('#trend-period-select').val();
    var type = $('.trend-type-button:focus').val();

    if (type === undefined)
        type = $('.trend-type-button.active').val();

    window.location.href = window.location.href.replace(/[\?#].*|$/, "?trend_period=" + period + '&trend_type=' + type);
}