<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Services\RedisCacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CacheController extends Controller
{
    public function indexAction($prefix = null)
    {
        $envPrefix = $this->getParameter('redis_namespace');

        if (empty($prefix)) {
            return $this->render('@App/Admin/Cache/index.html.twig', ['keys_family' => []]);
        } else {
            $prefix = constant("AppBundle\Services\RedisCacheManager::$prefix");
        }

        $keys = $this->get('app.redis.cache.manager')->getKeys($prefix . '*');
        $keysByFamily = [];

        try {
            $reflection = new \ReflectionClass('AppBundle\Services\RedisCacheManager');
            $constants  = $reflection->getConstants();
        } catch (\ReflectionException $e) {
            $constants = [];
        }

        foreach ($constants as $constantName => &$constantValue) {
            $constantValue = "{$envPrefix}$constantValue";
            $constantValue = preg_replace('/\{[A-Za-z_]+\}/', '[\S]+', $constantValue);
        }

        foreach ($keys as $key) {
            foreach ($constants as $constantName => $constantPattern) {
                if (preg_match("/^{$constantPattern}$/", $key)) {
                    $keysByFamily[$constantName][] = $key;
                }
            }
        }

        return $this->render('@App/Admin/Cache/index.html.twig', ['keys_family' => $keysByFamily]);
    }

    public function showAction(Request $request)
    {
        $key = $request->get('key');
        $cacheManager = $this->get('app.redis.cache.manager');

        $data = $cacheManager->get(str_replace($cacheManager->getRedisNamespace(), '', $key));

        return $this->render('@App/Admin/Cache/show.html.twig', ['key' => $key, 'data' => $data]);
    }

    public function removeForKeyAction(Request $request)
    {
        $key = $request->get('key');
        $this->get('app.redis.cache.manager')->deleteItem($key, false);

        return $this->redirectToRoute('app_admin_cache_index');
    }

    public function removeAction()
    {
        $cacheManager = $this->get('app.redis.cache.manager');

        $cacheManager->deleteItemsByPattern(RedisCacheManager::CACHE_KEY_PREFIX . '*');

        return $this->redirectToRoute('app_admin_cache_index');
    }

    public function flushAction()
    {
        $cacheManager = $this->get('app.redis.cache.manager');

        $cacheManager->getClient()->flushall();

        return $this->redirectToRoute('app_admin_cache_index');
    }
}