<?php


namespace AppBundle\Admin\Controller;

use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\ImportLineRepository;
use AppBundle\Repository\ImportRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\RedisCacheManager;
use DateTime;
use JMS\JobQueueBundle\Entity\Job;
use JMS\JobQueueBundle\Entity\Repository\JobManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProductMatchingController extends AbstractController
{
    /** @var SupplierProductVariantInformationsRepository */
    private $spviRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var SupplierProductPriceRepository */
    private $priceRepository;

    /** @var RedisCacheManager */
    private $redisCacheManager;

    /** @var ImportLineRepository */
    private $importLineRepository;

    /** @var ImportRepository */
    private $importRepository;

    /** @var JobManager */
    private $jobManager;

    public function __construct(
        SupplierProductVariantInformationsRepository $spviRepository,
        SupplierRepository $supplierRepository,
        SupplierProductPriceRepository $priceRepository,
        RedisCacheManager $redisCacheManager,
        ImportLineRepository $importLineRepository,
        ImportRepository $importRepository,
        JobManager $jobManager
    ) {
        $this->spviRepository = $spviRepository;
        $this->supplierRepository = $supplierRepository;
        $this->priceRepository = $priceRepository;
        $this->redisCacheManager = $redisCacheManager;
        $this->importLineRepository = $importLineRepository;
        $this->importRepository = $importRepository;
        $this->jobManager = $jobManager;
    }

    public function indexAction()
    {
        /** @var Supplier[] $suppliers */
        $suppliers = $this->supplierRepository->findBy(['enabled' => false]);
        $data = [];

        foreach ($suppliers as $supplier) {
            $data[] = [
                'id' => $supplier->getId(),
                'name' => $supplier->getDisplayName(true),
                'nbActiveProducts' => $this->supplierRepository->countActiveVariants($supplier),
                'nbProducts' => $this->spviRepository->count(['supplier' => $supplier])
            ];
        }

        usort($data, function($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });

        return $this->render('@App/Admin/ProductMatching/index.html.twig', ['suppliers' => $data]);
    }

    public function showAction(int $id)
    {
        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($id);
        $matching = $this->redisCacheManager->get(str_replace('{supplierId}', $id, RedisCacheManager::CACHE_KEY_ADMIN_PRODUCT_MATCHING_LIST));

        if ($matching === null) {
            // is there a job running somewhere, computing the data already or not ?
            $job = $this->jobManager->findJobForRelatedEntity('app:generate:matching-report', $supplier);

            if ($job === null) {
                $job = new Job('app:generate:matching-report', ['supplierId' => $supplier->getId()]);

                $job->addRelatedEntity($supplier);

                $em = $this->getDoctrine()->getManager();
                $em->persist($job);
                $em->flush();
            }

            return $this->render('@App/Admin/ProductMatching/show.html.twig', [
                'supplier' => $supplier->getDisplayName(true),
                'supplierId' => $supplier->getId(),
                'matching' => null,
                'job' => $job
            ]);
        }

        return $this->render('@App/Admin/ProductMatching/show.html.twig', [
            'supplier' => $supplier->getDisplayName(true),
            'supplierId' => $supplier->getId(),
            'matching' => $matching
        ]);
    }

    public function refreshAction(Request $request)
    {
        $supplierId = $request->attributes->get('id');
        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($supplierId);

        $this->redisCacheManager->deleteItem(
            str_replace('{supplierId}', $supplierId, RedisCacheManager::CACHE_KEY_ADMIN_PRODUCT_MATCHING_LIST)
        );

        $em = $this->getDoctrine()->getManager();
        $job = $this->jobManager->findJobForRelatedEntity('app:generate:matching-report', $supplier);

        if ($job !== null) {
            $em->remove($job);
            $em->flush();
        }

        $job = new Job('app:generate:matching-report', ['supplierId' => $supplier->getId()]);

        $job->addRelatedEntity($supplier);

        $em->persist($job);
        $em->flush();

        return $this->redirectToRoute('app_admin_product_matching_show', ['id' => $supplierId]);
    }

    public function exportAction(Request $request)
    {
        $id = $request->attributes->get('id');
        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($id);
        /** @var SupplierProductVariantInformations[] $spvis */
        $spvis = $this->spviRepository->findBy(['supplier' => $supplier]);
        $matching = [];
        $date = new DateTime();
        $filename = "Matching {$supplier->getDisplayName(true)} ({$date->format('Y-m-d')}).csv";

        foreach ($spvis as $spvi) {
            /** @var SupplierProductPrice|null $price */
            $price = $this->priceRepository->findOneBy(['supplier' => $supplier, 'restaurant' => 4, 'product' => $spvi->getProduct(), 'productVariant' => $spvi->getProductVariant()]);

            $matching[] = [
                'id' => $spvi->getId(),
                'ref' => $spvi->getReference(),
                'product' => $spvi->getProductLabel(),
                'pid' => $spvi->getProduct() ? $spvi->getProduct()->getId() : '',
                'vid' => $spvi->getProductVariant() ? $spvi->getProductVariant()->getId() : '',
                'um' => $spvi->getSalesUnit(),
                'weight' => $spvi->getWeight(),
                'variantWeight' => $spvi->getProductVariant() ? $spvi->getProductVariant()->getWeight() : '',
                'units' => $spvi->getUnitQuantity(),
                'kgPrice' => $price ? bcdiv($price->getKgPrice(), 100, 2) : '',
                'unitPrice' => $price ? bcdiv($price->getUnitPrice(), 100, 2) : '',
            ];
        }

        usort($matching, function($a, $b) {
            return strcasecmp($a['product'], $b['product']);
        });

        $formatter = function($value) {
            ///remove any ESCAPED double quotes within string.
            $value = str_replace('\\"','"',$value);
            //then force escape these same double quotes And Any UNESCAPED Ones.
            $value = str_replace('"','\"',$value);
            //force wrap value in quotes and return
            return '"'.$value.'"';
        };

        return new StreamedResponse(function() use($matching, $formatter) {
            $stdout = fopen('php://memory', 'r+');

            fputs($stdout, implode(',', array_map($formatter, ['ID', 'Référence', 'Produit', 'PID', 'VID', 'Unité de mesure', 'Poids', 'Poids variante', 'Nombre d\'unités', 'Prix kilo', 'Prix pièce'])) . "\r\n");

            foreach ($matching as $match) {
                fputs($stdout, implode(',', array_map($formatter, [
                    $match['id'],
                    $match['ref'],
                    $match['product'],
                    $match['pid'],
                    $match['vid'],
                    $match['um'],
                    $match['weight'],
                    $match['variantWeight'],
                    $match['units'],
                    $match['kgPrice'],
                    $match['unitPrice'],
                ])) . "\r\n");
            }

            rewind($stdout);
            echo stream_get_contents($stdout);
        }, 200, ['Content-Type' => 'text/csv', 'Content-Disposition' => "attachment; filename=$filename"]);
    }
}
