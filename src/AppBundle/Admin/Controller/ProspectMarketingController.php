<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\Prospect;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Request;

class ProspectMarketingController extends Controller
{
    public function indexAction(Request $request)
    {
        $form = $this->getForm();

        $form->handleRequest($request);

        $prospects = [];

        if ($form->isSubmitted() && $form->isValid()) {
            $startDate = $form->get('startDate')->getData();
            $endDate = $form->get('endDate')->getData();
            $source = $form->get('source')->getData();

            $qb = $this->get('app.repository.prospect')->createQueryBuilder('p');

            if ($source !== 'all') {
                if ($source !== 'other') {
                    $qb
                        ->andWhere('p.source = :source')
                        ->setParameter('source', $source);
                } else {
                    $qb->andWhere($qb->expr()->notIn('p.source', ['facebook', 'adwords', 'landing', 'linkedin', 'email']));
                }
            }

            $prospects = $qb
                ->andWhere('p.createdAt BETWEEN :start AND :end')
                ->setParameter('start', $startDate)
                ->setParameter('end', $endDate)
                ->getQuery()
                ->getResult()
            ;
        }

        $stats = [];

        /** @var Prospect $prospect */
        foreach ($prospects as $prospect) {
            $source = !empty($prospect->getSource()) ? $prospect->getSource() : 'Autre';

            if (!isset($stats[$source])) {
                $stats[$source] = [
                    'source' => $source,
                    'total' => 0,
                    'utm_campaign' => [],
                ];
            }

            $campaign = !empty($prospect->getUtmCampaign()) ? $prospect->getUtmCampaign() : 'Autre';

            if (!isset($stats[$source]['utm_campaign'][$campaign])) {
                $stats[$source]['utm_campaign'][$campaign] = [
                    'utm_campaign' => $campaign,
                    'total' => 0
                ];
            }

            ++$stats[$source]['total'];
            ++$stats[$source]['utm_campaign'][$campaign]['total'];
        }

        foreach ($stats as &$stat) {
            $stat['utm_campaign'] = array_values($stat['utm_campaign']);
        }

        $stats = array_values($stats);

        return $this->render('@App/Admin/ProspectMarketing/index.html.twig', [
            'form' => $form->createView(),
            'stats' => $stats
        ]);
    }

    private function getForm()
    {
        return $this->createFormBuilder()
            ->add('startDate', DateType::class, ['label' => 'Date de début', 'widget' => 'single_text', 'attr' => ['class' => 'ui datepicker']])
            ->add('endDate', DateType::class, ['label' => 'Date de fin', 'widget' => 'single_text', 'attr' => ['class' => 'ui datepicker']])
            ->add('source', ChoiceType::class, ['label' => 'Source', 'choices' => [
                'Tous' => 'all',
                'Facebook' => 'facebook',
                'Adwords' => 'adwords',
                'Linkedin' => 'linkedin',
                'Email' => 'email',
                'Landing' => 'landing',
                'Autre' => 'other'
            ]])
            ->getForm()
        ;
    }
}