<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Constant\PaymentMode;
use AppBundle\Entity\Mercuriale;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Form\Type\MercurialeType;
use AppBundle\Repository\MercurialeRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\SupplierCategoryRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\RedisCacheManager;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Sylius\Component\Core\Model\Channel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use Throwable;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SupplierController extends AbstractController
{
    public function indexAction()
    {
        $supplierRepository = $this->getSupplierRepository();

        $suppliers = $supplierRepository->findBy(
            ['enabled' => 1, 'facadeSupplier' => null, 'createdByUser' => false],
            ['name' => 'ASC']
        );

        return $this->render('@App/Admin/Suppliers/index.html.twig', ['suppliers' => $suppliers]);
    }

    public function newAction(Request $request)
    {
        $form = $this->getSupplierForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var Supplier $supplier */
                $supplier = $form->getData();

                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();

                try {
                    $em->transactional(function (EntityManager $em) use ($supplier) {
                        /** @var Channel $channel */
                        $channel = $this->get('sylius.factory.channel')->createNamed($supplier->getName());

                        $channel->setCode((new Slugify())->slugify($supplier->getName()));
                        $channel->setEnabled(true);
                        $channel->setTaxCalculationStrategy('order_items_based');
                        /** @noinspection PhpParamsInspection */
                        $channel->setDefaultLocale($this->get('sylius.repository.locale')->findOneBy(['code' => 'fr_FR']));
                        /** @noinspection PhpParamsInspection */
                        $channel->setBaseCurrency($this->get('sylius.repository.currency')->findOneBy(['code' => 'EUR']));

                        $em->persist($channel);

                        $supplier->setEnabled(true);
                        /** @noinspection PhpParamsInspection */
                        $supplier->setChannel($channel);

                        $em->persist($supplier);
                    });
                } catch (Throwable $e) {
                    return $this->render('@App/Admin/Suppliers/new.html.twig', ['form' => $form->createView(), 'error' => $e->getMessage()]);
                }

                return $this->redirectToRoute('app_admin_suppliers_edit', ['id' => $supplier->getId()]);
            }
        }

        return $this->render('@App/Admin/Suppliers/new.html.twig', ['form' => $form->createView()]);
    }

    public function newMercurialeAction(Request $request)
    {
        /** @var Supplier $partner */
        $partner = $this->getSupplierRepository()->find($request->attributes->get('id'));
        /** @var Restaurant $restaurant */
        $restaurant = $this->getRestaurantRepository()->find(4);

        $mercuriale = new Mercuriale();

        $mercuriale->setRestaurant($restaurant);
        $mercuriale->setSupplier($partner);

        $form = $this->createForm(MercurialeType::class, $mercuriale, ['action' => $this->generateUrl('app_admin_suppliers_mercuriale_new', ['id' => $partner->getId()])]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $mercuriale = $form->getData();

                $this->getMercurialeRepository()->add($mercuriale);
                return $this->json(['ok' => true]);
            } else {
                return $this->json(['ok' => false, 'form' => $this->renderView('@App/Admin/Suppliers/_mercuriale_form.html.twig', ['form' => $form->createView()])]);
            }
        }

        return $this->json(['form' => $this->renderView('@App/Admin/Suppliers/_mercuriale_form.html.twig', ['form' => $form->createView()])]);
    }

    public function editAction(Request $request)
    {
        /** @var Supplier $supplier */
        $supplier = $this->getSupplierRepository()->find($request->attributes->get('id'));
        $form = $this->getSupplierForm($supplier);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->render('@App/Admin/Suppliers/edit.html.twig', [
            'supplier' => $supplier,
            'form' => $form->createView(),
            'pricesGrids' => $this->getPricesGrids($supplier),
            'priceGridForm' => $this->getPriceGridForm($supplier)->createView()
        ]);
    }

    private function getPricesGrids(Supplier $supplier)
    {
        $supplierCategoryRepository = $this->getSupplierCategoryRepository();
        $mercurialeRepository = $this->getMercurialeRepository();
        $partners = $supplier->getPartnerSuppliers();
        $pricesGrids = [];

        foreach ($partners as $partner) {
            $supplierCategories = $supplierCategoryRepository->findBySupplier($partner);
            $grids = [];

            foreach ($supplierCategories as $supplierCategory) {
                $mercuriales = $mercurialeRepository->findBySupplierCategory($supplierCategory);

                $grids[] = [
                    'category' => Supplier::getDisplayCategory($supplierCategory->getCategory()),
                    'mercuriales' => $mercuriales
                ];
            }

            $pricesGrids[] = [
                'id' => $partner->getId(),
                'name' => $partner->getName(),
                'grids' => $grids
            ];
        }

        return $pricesGrids;
    }

    public function newPriceGridAction(Request $request)
    {
        $supplierRepository = $this->getSupplierRepository();
        /** @var Supplier $supplier */
        $supplier = $supplierRepository->find($request->attributes->get('id'));
        $form = $this->getPriceGridForm($supplier);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gridName = $form->get('name')->getData();

            try {
                /** @var EntityManager $em */
                $em = $this->getDoctrine()->getManager();

                $em->transactional(function(EntityManager $em) use($supplier, $gridName) {
                    /** @var Channel $channel */
                    $channel = $this->get('sylius.factory.channel')->createNamed($gridName);

                    $channel->setCode((new Slugify())->slugify($gridName));
                    $channel->setEnabled(true);
                    $channel->setTaxCalculationStrategy('order_items_based');
                    /** @noinspection PhpParamsInspection */
                    $channel->setDefaultLocale($this->get('sylius.repository.locale')->findOneBy(['code' => 'fr_FR']));
                    /** @noinspection PhpParamsInspection */
                    $channel->setBaseCurrency($this->get('sylius.repository.currency')->findOneBy(['code' => 'EUR']));

                    $em->persist($channel);

                    // Create a new partner supplier
                    $partner = new Supplier();

                    $partner->setName($gridName);
                    $partner->setFacadeSupplier($supplier);
                    $partner->setEnabled(false);
                    /** @noinspection PhpParamsInspection */
                    $partner->setChannel($channel);
                    $partner->copySettingsFrom($supplier);

                    $em->persist($partner);

                    // Adds this newly created supplier as partner for this facade
                    $supplier->addPartnerSupplier($partner);
                });

                return $this->json(['ok' => true]);
            } catch (Throwable $e) {
                return $this->json(['ok' => false, 'message' => $e->getMessage()]);
            }
        }

        return $this->json(['ok' => false, 'message' => 'Impossible de créer les dépendances pour la nouvelle grille tarifaire.']);
    }

    public function barometerAction(int $supplier, bool $useCache)
    {
        /** @var Supplier $supplier */
        $supplier = $this->getSupplierRepository()->find($supplier);
        $cacheManager = $this->get('app.redis.cache.manager');
        $barometer = [];

        foreach ($supplier->getPartnerSuppliers() as $partner) {
            $grid = $partner->getId();
            $key = str_replace('{supplierId}', $grid, RedisCacheManager::CACHE_KEY_FRONT_MERCURIAL_PATTERN);

            if (!$useCache) {
                $cacheManager->deleteItem($key);
            }

            $prices = $cacheManager->get(
                $key,
                86400,
                function () use ($grid) {
                    $productListService = $this->get('app.service.product_list');

                    return array_merge(
                        $productListService->getPriceList(false, 'all', $grid),
                        $productListService->getPriceList(true, 'all', $grid)
                    );
                }
            );

            $barometer[] = [
                'grid' => $partner->getDisplayName(true),
                'prices' => $prices
            ];
        }

        return $this->render('@App/Admin/Suppliers/_price_list.html.twig', ['barometer' => $barometer]);
    }

    public function togglePartnerAction(Request $request)
    {
        $supplierId = $request->attributes->get('id');
        /** @var Supplier $supplier */
        $supplier = $this->getSupplierRepository()->find($supplierId);

        $supplier->setPartner(!$supplier->isPartner());

        foreach ($supplier->getPartnerSuppliers() as $partner) {
            $partner->setPartner($supplier->isPartner());
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->json(['ok' => true, 'partner' => $supplier->isPartner()]);
    }

    /**
     * @return SupplierRepository
     */
    private function getSupplierRepository()
    {
        return $this->get('app.repository.supplier');
    }

    /**
     * @return SupplierCategoryRepository
     */
    private function getSupplierCategoryRepository()
    {
        return $this->get('app.repository.supplier_category');
    }

    /**
     * @return MercurialeRepository
     */
    private function getMercurialeRepository()
    {
        return $this->get('app.repository.mercuriale');
    }

    /**
     * @return RestaurantRepository
     */
    private function getRestaurantRepository()
    {
        return $this->get('app.repository.restaurant');
    }

    private function getPriceGridForm(Supplier $supplier)
    {
        return $this->createFormBuilder(null, ['action' => $this->generateUrl('app_admin_suppliers_new_price_grid', ['id' => $supplier->getId()])])
            ->add('name', TextType::class, ['label' => 'Nom interne de la grille'])
            ->getForm()
        ;
    }

    /**
     * @noinspection PhpDocMissingThrowsInspection
     * @param Supplier|null $supplier
     * @return FormInterface
     */
    private function getSupplierForm(?Supplier $supplier = null)
    {
        $action = $supplier === null
            ? $this->generateUrl('app_admin_suppliers_new')
            : $this->generateUrl('app_admin_suppliers_edit', ['id' => $supplier->getId()])
        ;

        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->createFormBuilder($supplier, ['action' => $action, 'data_class' => Supplier::class])
            ->add('name', TextType::class, ['label' => 'Nom du fournisseur'])
            ->add('logo', VichImageType::class, ['required' => false, 'label' => 'Logo du fournisseur'])
            ->add('categories', ChoiceType::class, [
                'label' => false,
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    Supplier::getDisplayCategory(Supplier::CAT_FRUITS_LEGUMES) => Supplier::CAT_FRUITS_LEGUMES,
                    Supplier::getDisplayCategory(Supplier::CAT_FRUITS_LEGUMES_BIO) => Supplier::CAT_FRUITS_LEGUMES,
                    Supplier::getDisplayCategory(Supplier::CAT_CREMERIE) => Supplier::CAT_CREMERIE,
                    Supplier::getDisplayCategory(Supplier::CAT_BOUCHERIE) => Supplier::CAT_BOUCHERIE,
                    Supplier::getDisplayCategory(Supplier::CAT_MAREE) => Supplier::CAT_MAREE,
                    Supplier::getDisplayCategory(Supplier::CAT_CAVE) => Supplier::CAT_CAVE,
                    Supplier::getDisplayCategory(Supplier::CAT_EPICERIE) => Supplier::CAT_EPICERIE,
                    Supplier::getDisplayCategory(Supplier::CAT_TRAITEUR) => Supplier::CAT_TRAITEUR,
                    Supplier::getDisplayCategory(Supplier::CAT_CONSOMMABLES) => Supplier::CAT_CONSOMMABLES
                ]
            ])
            ->add('phone', TextType::class, ['label' => 'Numéro de téléphone'])
            ->add('street', TextType::class, ['label' => 'Rue'])
            ->add('postcode', TextType::class, ['label' => 'Code postal', 'constraints' => [new Length(['min' => 5, 'max' => 5]), new Regex(['pattern' => '/^\d{5,5}$/'])]])
            ->add('city', TextType::class, ['label' => 'Ville'])
            ->add('countryCode', TextType::class, ['label' => 'Pays', 'constraints' => [new Length(['min' => 2, 'max' => 2])]])
            ->add('description', TextareaType::class, ['required' => false, 'label' => 'Description'])
            ->add('deliveryTerms', TextareaType::class, ['required' => false, 'label' => 'Modalités de livraison'])

            ->add('orderLimitTime', TextType::class, ['required' => false, 'label' => 'Heure limite de commande pour être livré à J+1', 'constraints' => [new Regex(['pattern' => '/^[\d]{2,2}:[\d]{2,2}$/'])]])
            ->add('minimumOrder', MoneyType::class, ['required' => false, 'label' => 'Minimum de commande', 'currency' => 'EUR', 'divisor' => 1])
            ->add('freePort', MoneyType::class, ['required' => false, 'label' => 'Francos de port', 'currency' => 'EUR', 'divisor' => 1])
            ->add('shippingCost', MoneyType::class, ['required' => false, 'label' => 'Frais de livraison', 'currency' => 'EUR', 'divisor' => 1])
            ->add('deliveryOnMonday', CheckboxType::class, ['required' => false, 'label' => 'Lundi'])
            ->add('deliveryOnTuesday', CheckboxType::class, ['required' => false, 'label' => 'Mardi'])
            ->add('deliveryOnWednesday', CheckboxType::class, ['required' => false, 'label' => 'Mercredi'])
            ->add('deliveryOnThursday', CheckboxType::class, ['required' => false, 'label' => 'Jeudi'])
            ->add('deliveryOnFriday', CheckboxType::class, ['required' => false, 'label' => 'Vendredi'])
            ->add('deliveryOnSaturday', CheckboxType::class, ['required' => false, 'label' => 'Samedi'])
            ->add('deliveryOnSunday', CheckboxType::class, ['required' => false, 'label' => 'Dimanche'])
            ->add('deliveryZones', TextareaType::class, ['label' => false])

            ->add('chronofreshDelivery', CheckboxType::class, ['required' => false, 'label' => 'Livraison via Chronofresh'])

            ->add('paymentOnDelivery', CheckboxType::class, ['required' => false, 'label' => 'Paiement à la livraison'])
            ->add('paymentOnline', CheckboxType::class, ['required' => false, 'label' => 'Accepte les paiements en ligne'])
            ->add('paymentByCheque', CheckboxType::class, ['required' => false, 'label' => PaymentMode::toString(PaymentMode::CHEQUE)])
            ->add('paymentByCash', CheckboxType::class, ['required' => false, 'label' => PaymentMode::toString(PaymentMode::CASH)])
            ->add('paymentByCreditCard', CheckboxType::class, ['required' => false, 'label' => PaymentMode::toString(PaymentMode::CREDIT_CARD)])
            ->add('paymentByBankTransfer', CheckboxType::class, ['required' => false, 'label' => PaymentMode::toString(PaymentMode::BANK_TRANSFER)])
            ->add('paymentByDirectDebit', CheckboxType::class, ['required' => false, 'label' => PaymentMode::toString(PaymentMode::DIRECT_DEBIT)])

            ->add('autoBind', CheckboxType::class, ['required' => false, 'label' => 'Affectation automatique, par code postal'])
            ->add('accountOpening', CheckboxType::class, ['required' => false, 'label' => 'Nécessite une ouverture de compte (SEPA obligatoire)'])
            ->add('applicationNeeded', CheckboxType::class, ['required' => false, 'label' => 'Ouverture de compte soumise à validation (Le founisseur valide le SEPA)'])
            ->add('sepa', VichFileType::class, ['required' => false, 'label' => 'SEPA', 'allow_delete' => true])
            ->add('rib', VichFileType::class, ['required' => false, 'label' => 'RIB', 'allow_delete' => true])

            ->add('howToOrder', TextareaType::class, ['label' => 'Comment contacter le grossiste (envoi des mercuriales)?'])
            ->add('orderModality', TextareaType::class, ['required' => false, 'label' => 'Modalités de commande (ouverture de compte grossiste)'])
            ->add('orderAdditionalElements', TextareaType::class, ['required' => false, 'label' => 'Eléments complémentaires (ouverture de compte grossiste)'])
            ->add('creditCardPriceMultiplier', NumberType::class, ['required' => true, 'label' => 'Multiplicateur pour paiement CB'])

            ->add('lyraUuid', TextType::class, ['required' => false, 'label' => 'Lyra UUID (prod)'])
            ->add('lyraDevUuid', TextType::class, ['required' => false, 'label' => 'Lyra UUID (dev)'])

            ->add('email', EmailType::class, ['label' => 'Email du grossiste'])
            ->add('commercialEmails', CollectionType::class, ['required' => true, 'label' => 'Emails pour ouverture de compte', 'entry_type' => EmailType::class, 'allow_delete' => true, 'allow_add' => true])
            ->add('orderEmails', CollectionType::class, ['required' => true, 'label' => 'Emails pour passage de commande', 'entry_type' => EmailType::class, 'allow_delete' => true, 'allow_add' => true])

            ->add('partner', CheckboxType::class, ['label' => 'Partenaire ?', 'required' => false])
            ->add('createdByUser', CheckboxType::class, ['label' => 'Créé par un utilisateur de l\'app ?', 'required' => false])

            ->getForm()
        ;
    }
}