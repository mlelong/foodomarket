<?php


namespace AppBundle\Admin\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class InvoiceImportController extends Controller
{
    public function indexAction()
    {
        $invoicePath = $this->getParameter('kernel.project_dir') . '/var/invoice';
        $reportFilePath = "$invoicePath/report.csv";
        $report = [];

        if (file_exists($reportFilePath)) {
            $handle = fopen($reportFilePath, 'r');
            // Skip the header
            fgetcsv($handle);

            while ($line = fgetcsv($handle)) {
                $filename = str_replace("$invoicePath/", '', $line[0]);

                $report[] = [
                    'url' => $this->generateUrl('app_admin_invoice_import_show_file', ['file' => urlencode($line[0])]),
                    'file' => $filename,
                    'error' => $line[1]
                ];
            }
        }

        return $this->render('@App/Admin/InvoiceImport/index.html.twig', [
            'report' => $report
        ]);
    }

    public function showFileAction($file)
    {
        // Return a response with the contract as content
        $file = urldecode($file);
        $response = new Response(file_get_contents($file));

        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            basename($file)
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'application/pdf');

        // Dispatch request
        return $response;
    }
}
