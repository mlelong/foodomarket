<?php


namespace AppBundle\Admin\Controller;


use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ProductFilterService;
use Sylius\Component\Core\Model\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CarouselController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductFilterService
     */
    private $productFilterService;
    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    public function __construct(ProductRepository $productRepository, ProductFilterService $productFilterService, SupplierRepository $supplierRepository)
    {
        $this->productRepository = $productRepository;
        $this->productFilterService = $productFilterService;
        $this->supplierRepository = $supplierRepository;
    }

    public function productData(string $code)
    {
        $product = $this->productRepository->findOneByCode($code);

        if ($product === null) {
            return $this->json(['ok' => false]);
        }

        $products = [$product];
        $data = $this->productFilterService->getProductsAndFilters($products, null, $this->supplierRepository->getPartners(false), 'front_big');

        if (isset($data['products'][0])) {
            return $this->json(['ok' => true, 'data' => $data['products'][0]]);
        }

        return $this->json(['ok' => false]);
    }

    public function productsByName(Request $request)
    {
        $phrase = $request->get('phrase');
        $ret = [];

        if (!empty($phrase)) {
            $qb = $this->productRepository->createQueryBuilder('p');
            $products = $qb
                ->join('p.translations', 't', 'WITH', 't.locale = :locale')
                ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = p')
                ->where('t.name LIKE :name')
                ->andWhere('p.enabled = 1')
                ->andWhere('spp.supplier IN (:suppliers)')
                ->setParameter('suppliers', $this->supplierRepository->getPartners(false))
                ->setParameter('locale', 'fr_FR')
                ->setParameter('name', '%' . implode('%', explode(' ', $phrase)) . '%')
                ->getQuery()
                ->getResult()
            ;

            /** @var Product $product */
            foreach ($products as $product) {
                $ret[] = [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                    'code' => $product->getCode()
                ];
            }
        }

        return $this->json($ret);
    }
}
