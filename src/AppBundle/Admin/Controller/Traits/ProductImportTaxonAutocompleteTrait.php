<?php

namespace AppBundle\Admin\Controller\Traits;

use Sylius\Component\Core\Model\Taxon;
use Symfony\Component\HttpFoundation\Request;

trait ProductImportTaxonAutocompleteTrait
{
    private function getFullTaxonPath(?Taxon $taxon)
    {
        if (is_null($taxon)) {
            return 'Ce taxon n\'existe plus';
        }

        $ancestors = array_reverse($taxon->getAncestors()->toArray());
        $ancestors[] = $taxon;

        return array_reduce($ancestors, function($carry, Taxon $current) {
            $currentName = $current->getId() === 1 ? 'Catalogue' : $current->getName();

            return !empty($carry) ? "$carry > $currentName" : $currentName;
        }, '');
    }

    public function searchTaxonAction(Request $request)
    {
        /** @var Taxon[] $taxons */
        $taxons = $this->get('sylius.repository.taxon')->findByNamePart($request->query->get('phrase'));
        $results = [];

        foreach ($taxons as $taxon) {
            $fullName = $this->getFullTaxonPath($taxon);

            $results[] = [
                'id' => $taxon->getId(),
                'fullname' => empty($fullName) ? 'Catalogue' : $fullName
            ];
        }

        return $this->json($results);
    }

    public function getTaxonAction(Request $request)
    {
        /** @var Taxon[] $taxons */
        $taxons = $this->get('sylius.repository.taxon')->findBy(['id' => $request->query->get('id')]);
        $results = [];

        foreach ($taxons as $taxon) {
            $fullName = $this->getFullTaxonPath($taxon);

            $results[] = [
                'id' => $taxon->getId(),
                'fullname' => empty($fullName) ? 'Catalogue' : $fullName
            ];
        }

        return $this->json($results);
    }
}