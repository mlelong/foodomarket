<?php

namespace AppBundle\Admin\Controller\Traits;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductTaxon;
use Symfony\Component\HttpFoundation\Request;

trait ProductImportProductAutocompleteTrait
{
    public function searchProductAction(Request $request)
    {
        $taxonId = $request->get('taxonId');

        if ($taxonId == 0) {
            /** @var Product[] $products */
            $products = $this->get('sylius.repository.product')->findByNamePart($request->query->get('phrase'), 'fr_FR');
        } else {
            /** @var Product[] $products */
            $products = $this->get('sylius.repository.product')->createQueryBuilder('p')
                ->innerJoin('p.translations', 't', 'WITH')
                ->join(ProductTaxon::class, 'pt', 'WITH', 'pt.product = p AND pt.taxon = :taxon')
                ->setParameter('taxon', $taxonId)
                ->where('t.name LIKE :phrase')
                ->andWhere('t.locale = :locale')
                ->setParameter('phrase', "%{$request->query->get('phrase')}%")
                ->setParameter('locale', 'fr_FR')
                ->getQuery()
                ->getResult()
            ;
        }

        $results = [];

        foreach ($products as $product) {
            $results[$product->getName()] = [
                'id' => $product->getId(),
                'name' => $product->getName()
            ];
        }

        ksort($results);

        return $this->json($results);
    }

    public function getProductAction(Request $request)
    {
        /** @var Product[] $products */
        $products = $this->get('sylius.repository.product')->findBy(['id' => $request->query->get('id')]);
        $results = [];

        foreach ($products as $product) {

            $results[$product->getName()] = [
                'id' => $product->getId(),
                'name' => $product->getName()
            ];
        }

        ksort($results);

        return $this->json($results);
    }
}