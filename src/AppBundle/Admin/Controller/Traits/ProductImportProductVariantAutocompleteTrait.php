<?php

namespace AppBundle\Admin\Controller\Traits;

use AppBundle\Entity\ProductVariant;
use Symfony\Component\HttpFoundation\Request;

trait ProductImportProductVariantAutocompleteTrait
{
    public function searchProductVariantAction(Request $request)
    {
        $variantInformationsRepository = $this->get('app.repository.supplier_product_variant_informations');
        $productId = $request->get('productId');

        if ($productId == 0) {
            return $this->json([]);
        } else {
            /** @var ProductVariant[] $variants */
            $variants = $this->get('sylius.repository.product_variant')->findBy(['product' => $productId]);
        }

        $results = [];

        foreach ($variants as $variant) {

            $supplierInformations = $variantInformationsRepository->findBy(['productVariant' => $variant->getId()]);
            foreach ($supplierInformations as $supplierInformation) {
                if (is_null($supplierInformation->getSupplier()->getFacadeSupplier())) {
                    $variantInformation = $supplierInformation->getSupplier()->getName().' - '.$supplierInformation->getProductLabel();
                } else {
                    $variantInformation = $supplierInformation->getSupplier()->getFacadeSupplier()->getName().' - '.$supplierInformation->getProductLabel();
                }

                $results[$variantInformation] = [
                    'id' => $variant->getId(),
                    'name' => $variantInformation.' - '.$this->getDisplayNameForVariant($variant)
                ];
            }
        }
        ksort($results);

        return $this->json($results);
    }

    public function getProductVariantAction(Request $request)
    {
        $variantInformationsRepository = $this->get('app.repository.supplier_product_variant_informations');
        /** @var ProductVariant[] $variants */
        $variants = $this->get('sylius.repository.product_variant')->findBy(['id' => $request->query->get('id')]);
        $results = [];

        foreach ($variants as $variant) {

            $supplierInformations = $variantInformationsRepository->findBy(['productVariant' => $variant->getId()]);
            foreach ($supplierInformations as $supplierInformation) {
                if (is_null($supplierInformation->getSupplier()->getFacadeSupplier())) {
                    $variantInformation = $supplierInformation->getSupplier()->getName().' - '.$supplierInformation->getProductLabel();
                } else {
                    $variantInformation = $supplierInformation->getSupplier()->getFacadeSupplier()->getName().' - '.$supplierInformation->getProductLabel();
                }

                $results[$variantInformation] = [
                    'id' => $variant->getId(),
                    'name' => $variantInformation.' - '.$this->getDisplayNameForVariant($variant)
                ];
            }
        }
        ksort($results);

        return $this->json($results);
    }

    private function getDisplayNameForVariant(ProductVariant $variant)
    {
        return "{$variant->getInternalName()} - UC: {$variant->getOptionOrderUnit()->getValue()} - UF: {$variant->getOptionSupplierSalesUnit()->getValue()} - {$variant->getWeight()}kg";
    }
}