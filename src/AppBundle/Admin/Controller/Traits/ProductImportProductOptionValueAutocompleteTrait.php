<?php

namespace AppBundle\Admin\Controller\Traits;

use AppBundle\Entity\ProductOptionValue;
use Symfony\Component\HttpFoundation\Request;

trait ProductImportProductOptionValueAutocompleteTrait
{
    public function searchProductOptionValueAction(Request $request)
    {
        /** @var ProductOptionValue[] $productOptionValues */
        $productOptionValues = $this->get('sylius.repository.product_option_value')->createQueryBuilder('pov')
            ->innerJoin('pov.translations', 't', 'WITH', 't.locale = :locale')
            ->innerJoin('pov.option', 'o', 'WITH', 'o.code = :code')
            ->andWhere('t.value LIKE :phrase')
            ->andWhere('t.locale = :locale')
            ->setParameter('code', $request->attributes->get('optionCode'))
            ->setParameter('phrase', "%{$request->query->get('phrase')}%")
            ->setParameter('locale', 'fr_FR')
            ->getQuery()
            ->getResult()
        ;

        $results = [];

        foreach ($productOptionValues as $productOptionValue) {
            $results[] = [
                'code' => $productOptionValue->getCode(),
                'name' => $productOptionValue->getValue()
            ];
        }

        $this->sortProductOptionValues($results);

        return $this->json($results);
    }

    public function getProductOptionValueAction(Request $request)
    {
        /** @var ProductOptionValue[] $productOptionValues */
        $productOptionValues = $this->get('sylius.repository.product_option_value')
            ->createQueryBuilder('pov')
            ->join('pov.option', 'po', 'WITH', 'po.code = :optionCode')
            ->where('pov.code = :code')
            ->setParameters([
                'optionCode' => $request->attributes->get('optionCode'),
                'code' => $request->query->get('code'),
            ])
            ->getQuery()
            ->getResult()
        ;
        $results = [];

        foreach ($productOptionValues as $productOptionValue) {
            $results[] = [
                'code' => $productOptionValue->getCode(),
                'name' => $productOptionValue->getValue()
            ];
        }

        $this->sortProductOptionValues($results);

        return $this->json($results);
    }

    private function sortProductOptionValues(array &$results)
    {
        usort($results, function($a, $b) {
            if (is_numeric($a['name']) && is_numeric($b['name'])) {
                return $a['name'] > $b['name'];
            }

            return strcasecmp($a['name'], $b['name']);
        });
    }
}