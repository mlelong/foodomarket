<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\Prospect;
use AppBundle\Form\Type\ProspectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdvancedUserController extends Controller
{
    public function indexAction(Request $request)
    {
        $lifecycleStages = $this->get('app.services.hubspot')->getLifeCycleStages();

        return $this->render('@App/Admin/AdvancedUser/index.html.twig', ['lifecyclestages' => $lifecycleStages]);
    }

    public function searchAction(Request $request)
    {
        $terms = $request->get('terms');
        $users = $this->get('app.services.hubspot')->searchContacts($terms);

        if ($users === null) {
            return new JsonResponse(['ok' => false, 'terms' => $terms, 'results' => []]);
        }

        $users = array_map(function($contact) {
            return [
                'vid' => $contact['vid'],
                'name' => isset($contact['properties']['firstname']['value']) ? $contact['properties']['firstname']['value'] : '',
                'email' => isset($contact['properties']['email']['value']) ? $contact['properties']['email']['value'] : '',
                'restaurant' => isset($contact['properties']['company']['value']) ? $contact['properties']['company']['value'] : 'N/A',
                'url' => isset($contact['profile-url']) ? $contact['profile-url'] : '',
                'transaction' => isset($contact['properties']['lifecyclestage']['value']) ? $contact['properties']['lifecyclestage']['value'] : '',
                'phone' => isset($contact['properties']['phone']['value']) ? $contact['properties']['phone']['value'] : '',
                'lastmodifieddate' => isset($contact['properties']['lastmodifieddate']['value']) ? $contact['properties']['lastmodifieddate']['value'] : ''
            ];
        }, $users['contacts']);

        return new JsonResponse(['ok' => true, 'terms' => $terms, 'results' => $users]);
    }

    public function vidFromEmailAction(Request $request)
    {
        $email = $request->get('email');
        $contacts = $this->get('app.services.hubspot')->searchContacts($email);

        if (!is_array($contacts) || empty($contacts['contacts'])) {
            $vid = null;
        } else {
            $vid = $contacts['contacts'][0]['vid'];
        }

        return new JsonResponse(['vid' => $vid]);
    }

    public function transactionsFromEmailAction(Request $request)
    {
        $email = $request->get('email');

        $prospect = $this->get('app.repository.prospect')->findOneBy(['email' => $email]);

        if ($prospect === null) {
            return new Response('');
        } else {
            $request->query->set('id', $prospect->getId());

            return $this->transactionsFromProspectAction($request);
        }
    }

    public function transactionsFromProspectAction(Request $request)
    {
        $hubspotService = $this->get('app.services.hubspot');
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($request->get('id'));
        $email = $prospect->getEmail();
        $contacts = $hubspotService->searchContacts($email);
        $hubspotUrl = '';
        $vid = null;

        if (!is_array($contacts) || ! array_key_exists('contacts', $contacts)) {
            $user = $hubspotService->createUser($prospect);
            $vid = isset($user['vid']) ? $user['vid'] : null;
            $hubspotUrl = isset($user['profile-url']) ? $user['profile-url'] : '';

            if ($vid !== null) {
                $prospect->setHubspotId($vid);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        if (is_array($contacts) && array_key_exists('contacts', $contacts) && array_key_exists(0, $contacts['contacts'])) {
            $vid = $contacts['contacts'][0]['vid'];
            $hubspotUrl = isset($contacts['contacts'][0]['profile-url']) ? $contacts['contacts'][0]['profile-url'] : '';

            if ($prospect->getHubspotId() === null || empty($prospect->getHubspotId())) {
                $prospect->setHubspotId($vid);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        $data = $this->getUserDeals($vid);
        $data['vid'] = $vid;
        $data['hubspotUrl'] = $hubspotUrl;

        return $this->render('@App/Admin/AdvancedUser/_transactions.html.twig', $data);
    }


    public function syncAction(Request $request, $email)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $hubspotService = $this->get('app.services.hubspot');
        $contacts = $hubspotService->searchContacts($email);

        if (is_null($contacts) || empty($contacts['contacts'])) {
            $vid = $contact = null;
        } else {
            $contact = $contacts['contacts'][0];
            $vid = $contact['vid'];
        }

        $prospect = $this->get('app.repository.prospect')->findOneByEmail($email);

        if (!$prospect) {
            $prospect = new Prospect();
            $prospect->setHubspotId($vid);

            $prospect->setCreatedBy($this->getUser());
            $prospect->setUpdatedBy($this->getUser());

            $entityManager->persist($prospect);
        }

        // we have prospect, but nothing in hubspot
        if ($prospect->getId() && is_null($vid)) {
            $hubspotService->createUser($prospect);
        }

        // we have prospect and hubspot
        if ($prospect->getId() && $vid) {
            $hubspotService->updateUser($vid, $prospect);

            if (array_key_exists('lastname', $contact['properties']) && $contact['properties']['lastname']['value'] && $prospect->getContactName() == '') {
                $prospect->setContactName($contact['properties']['lastname']['value']);
            }
            if (array_key_exists('firstname', $contact['properties']) && $contact['properties']['firstname']['value'] && $prospect->getFirstName() == '') {
                $prospect->setFirstName($contact['properties']['firstname']['value']);
            }
            if (array_key_exists('company', $contact['properties']) && $contact['properties']['company']['value'] && $prospect->getCompanyName() == '') {
                $prospect->setCompanyName($contact['properties']['company']['value']);
            }
            if (array_key_exists('company', $contact['properties']) && $contact['properties']['company']['value'] && $prospect->getRestaurantName() == '') {
                $prospect->setRestaurantName($contact['properties']['company']['value']);
            }
            if (array_key_exists('phone', $contact['properties']) && $contact['properties']['phone']['value'] && $prospect->getPhone() == '') {
                $prospect->setPhone($contact['properties']['phone']['value']);
            }
            if (array_key_exists('address', $contact['properties']) && $contact['properties']['address']['value'] && $prospect->getAddress() == '') {
                $prospect->setAddress($contact['properties']['address']['value']);
            }
            if (array_key_exists('fournisseur_actuel', $contact['properties'])) {
                $prospect->setCurrentSupplier($contact['properties']['fournisseur_actuel']['value']);
            }
            if (array_key_exists('total_revenue', $contact['properties'])) {
                $prospect->setTurnover(intval($contact['properties']['total_revenue']['value']));
            }
        }

        // we have no prospect, but hubspot
        if (is_null($prospect->getId()) && $vid) {

            $prospect->setEmail($contact['properties']['email']['value']);
            $prospect->setContactName($contact['properties']['lastname']['value']);
            if (array_key_exists('firstname', $contact['properties'])) {
                $prospect->setFirstName($contact['properties']['firstname']['value']);
            }
            if (array_key_exists('company', $contact['properties'])) {
                $prospect->setCompanyName($contact['properties']['company']['value']);
            }
            if (array_key_exists('company', $contact['properties'])) {
                $prospect->setRestaurantName($contact['properties']['company']['value']);
            }
            if (array_key_exists('phone', $contact['properties'])) {
                $prospect->setPhone($contact['properties']['phone']['value']);
            }
            if (array_key_exists('address', $contact['properties'])) {
                $prospect->setAddress($contact['properties']['address']['value']);
            }
            if (array_key_exists('fournisseur_actuel', $contact['properties'])) {
                $prospect->setCurrentSupplier($contact['properties']['fournisseur_actuel']['value']);
            }
            if (array_key_exists('total_revenue', $contact['properties'])) {
                $prospect->setTurnover(intval($contact['properties']['total_revenue']['value']));
            }
            $prospect->setSiren('');
            $prospect->setDeliveryHours('');

            $prospect->setEmailSubscribed(true);
            $prospect->setNewsletterPrices(true);

            $prospect->setHubspotId($vid);

        }

        $entityManager->flush();

        // Needed to keep search results table in sync with updated data after a sync occured
        // (otherwise we would have to wait 2 minutes before it is invalidated)
        $hubspotService->removeSearchContactCache();

        $request->attributes->set('vid', $vid);
        return $this->userEditAction($request);
    }

    public function userDetailsAction(Request $request)
    {
        $vid = $request->get('vid');
        $hubspotService = $this->get('app.services.hubspot');
        $details = $hubspotService->getUser($vid);

        $companyTransactions = [];
        $companyDeals = [];
        $userDeals = [];

        if (is_array($details) && isset($details['associated-company']['company-id'])) {
            $companyTransactions = $hubspotService->getCompanyAssociatedTransactions($details['associated-company']['company-id']);
        }

        $userTransactions = $hubspotService->getUserAssociatedTransactions($details['vid']);

        if (is_array($companyTransactions) && array_key_exists('deals', $companyTransactions)) {
            foreach ($companyTransactions['deals'] as $deal) {
                $companyDeals[] = $hubspotService->getDealDetails($deal['dealId']);
            }
        }

        if (is_array($userTransactions) && array_key_exists('deals', $userTransactions)) {
            foreach ($userTransactions['deals'] as $deal) {
                $userDeals[] = $hubspotService->getDealDetails($deal['dealId']);
            }
        }

        return new JsonResponse(['ok' => true, 'vid' => $vid, 'details' => $details]);
    }

    public function userEditAction(Request $request)
    {
        $vid = $request->get('vid');
        $hubspotService = $this->get('app.services.hubspot');
        $details = $hubspotService->getUser($vid);

        $details = null;

        $companyTransactions = [];
        $companyDeals = [];
        $userDeals = [];

        if (is_array($details) && isset($details['associated-company']['company-id'])) {
            $companyTransactions = $hubspotService->getCompanyAssociatedTransactions($details['associated-company']['company-id']);
        }

        $userTransactions = $hubspotService->getUserAssociatedTransactions($details['vid']);

        if (is_array($companyTransactions) && !empty($companyTransactions['deals'])) {
            foreach ($companyTransactions['deals'] as $deal) {
                $companyDeals[] = $hubspotService->getDealDetails($deal['dealId']);
            }
        }

        if (is_array($userTransactions) && !empty($userTransactions['deals'])) {
            foreach ($userTransactions['deals'] as $deal) {
                $userDeals[] = $hubspotService->getDealDetails($deal['dealId']);
            }
        }

        $lifecycleStages = $this->get('app.services.hubspot')->getLifeCycleStages();

        $companyDeals2 = [];
        foreach ($companyDeals as $companyDeal) {
            $dealForm = $this->getOpeningAccountDealForm($companyDeal, $vid);
            $companyDeals2[] = ['deal' => $companyDeal, 'form' => $dealForm->createView()];
        }

        $userDeals2 = [];
        foreach ($userDeals as $userDeal) {
            $dealForm = $this->getOpeningAccountDealForm($userDeal, $vid);
            $userDeals2[] = ['deal' => $userDeal, 'form' => $dealForm->createView()];
        }

        $qb = $this->get('app.repository.prospect')->createQueryBuilder('p');

        $qb
            ->where('p.hubspotId = :hubspotId')
            ->setParameter('hubspotId', $vid)
        ;

        $email = '';
        if (isset($details['properties']['email']['value'])) {
            $qb
                ->orWhere('p.email = :email')
                ->setParameter('email', $details['properties']['email']['value'])
            ;
            $email = $details['properties']['email']['value'];
        }

        $prospects = $qb->getQuery()->getResult();

        if (empty($prospects)) {
            $prospect = new Prospect();
            $prospect->setHubspotId($vid);
        } else {
            $prospect = $prospects[0];
        }

        $prospectForm = $this->getProspectForm($prospect);

        return $this->render('@App/Admin/AdvancedUser/_details_form.html.twig', [
            'email'        => $email,
            'userDetails' => $details,
            'companyDetails' => $details,
            'companyDeals' => $companyDeals2,
            'userDeals' => $userDeals2,
            'prospectForm' => $prospectForm->createView(),
            'lifecycleStages' => $lifecycleStages,
        ]);
    }

    private function getUserDeals($vid)
    {
        $companyDeals2 = [];
        $userDeals2 = [];

        if ($vid !== null) {
            $hubspotService = $this->get('app.services.hubspot');
            $details = $hubspotService->getUser($vid);

            $companyTransactions = [];
            $companyDeals = [];
            $userDeals = [];

            if (is_array($details) && isset($details['associated-company']['company-id'])) {
                $companyTransactions = $hubspotService->getCompanyAssociatedTransactions($details['associated-company']['company-id']);
            }

            $userTransactions = $hubspotService->getUserAssociatedTransactions($details['vid']);

            if (is_array($companyTransactions) && !empty($companyTransactions['deals'])) {
                foreach ($companyTransactions['deals'] as $deal) {
                    $companyDeals[] = $hubspotService->getDealDetails($deal['dealId']);
                }
            }

            if (is_array($userTransactions) && !empty($userTransactions['deals'])) {
                foreach ($userTransactions['deals'] as $deal) {
                    $userDeals[] = $hubspotService->getDealDetails($deal['dealId']);
                }
            }

            foreach ($companyDeals as $companyDeal) {
                $dealForm = $this->getOpeningAccountDealForm($companyDeal, $vid);
                $companyDeals2[] = ['deal' => $companyDeal, 'form' => $dealForm->createView()];
            }

            foreach ($userDeals as $userDeal) {
                $dealForm = $this->getOpeningAccountDealForm($userDeal, $vid);
                $userDeals2[] = ['deal' => $userDeal, 'form' => $dealForm->createView()];
            }
        }

        $lifecycleStages = $this->get('app.services.hubspot')->getLifeCycleStages();

        return [
            'companyDeals' => $companyDeals2,
            'userDeals' => $userDeals2,
            'lifecycleStages' => $lifecycleStages
        ];
    }

    public function userTransactionsAction(Request $request)
    {
        $data = $this->getUserDeals($request->get('vid'));
        return $this->render('@App/Admin/AdvancedUser/_transactions.html.twig', $data);
    }

    public function ajaxOpeningAccountDealFormAction(Request $request)
    {
        $dealId = $request->get('dealId');
        $userId = $request->get('userId');
        $hubspotService = $this->get('app.services.hubspot');

        if ($dealId != 0) {
            $dealDetail = $hubspotService->getDealDetails($dealId);
        } else {
            $openingAccountDealPropertyGroup = $hubspotService->getDealPropertyGroup('ouverture_de_compte');

            $dealDetail = [
                'dealId' => null,
                'properties' => [
                    'dealname' => ['value' => 'Nouveau Deal'],
                    'dealstage' => ['value' => '']
                ]
            ];

            if (is_array($openingAccountDealPropertyGroup) && !empty($openingAccountDealPropertyGroup['properties'])) {
                foreach ($openingAccountDealPropertyGroup['properties'] as $property) {
                    $dealDetail['properties'][$property['name']] = ['value' => null];
                }
            }
        }
        
        $dealForm = $this->getOpeningAccountDealForm($dealDetail, $userId);

        $dealForm->handleRequest($request);

        if ($dealForm->isSubmitted() && $dealForm->isValid()) {
            $dealDetailSubmitted = $dealForm->getData();

            if ($dealId != 0) {
                $response = $hubspotService->updateDeal($dealId, $dealDetailSubmitted);
            } else {
                $response = $hubspotService->createDeal(null, [$userId], $dealDetailSubmitted);
            }
        }

        return $this->render('@App/Admin/AdvancedUser/_deal.html.twig', [
            'deal' => [
                'deal' => $dealDetail,
                'form' => $dealForm->createView(),
            ],
            'lifecycleStages' => $hubspotService->getLifeCycleStages()
        ]);
    }

    private function getOpeningAccountDealForm($dealDetail, $userId)
    {
        $hubspotService = $this->get('app.services.hubspot');

        $openingAccountDealPropertyGroup = $hubspotService->getDealPropertyGroup('ouverture_de_compte');

        $formBuilder = $this->createFormBuilder($dealDetail)
            ->addModelTransformer(new class($openingAccountDealPropertyGroup['properties']) implements DataTransformerInterface {
                private $properties = [];

                public function __construct($properties)
                {
                    $this->properties = $properties;
                }

                public function transform($value)
                {
                    if ($value === null) {
                        return '';
                    }

                    $transformation = [];

                    $transformation['dealname'] = isset($value['properties']['dealname']['value']) ? $value['properties']['dealname']['value'] : '';
                    $transformation['dealstage'] = isset($value['properties']['dealstage']['value']) ? $value['properties']['dealstage']['value'] : 'presentationscheduled';

                    foreach ($this->properties as $property) {
                        if (isset($value['properties'][$property['name']])) {
                            if ($property['type'] == 'enumeration' && $property['fieldType'] == 'checkbox') {
                                $transformationValue = explode(';', $value['properties'][$property['name']]['value']);
                            } else {
                                $transformationValue = $value['properties'][$property['name']]['value'];
                            }

                            $transformation[$property['name']] = $transformationValue;
                        } else {
                            $transformation[$property['name']] = null;
                        }
                    }

                    $transformation['dealId'] = isset($value['dealId']) ? $value['dealId'] : null;

                    return $transformation;
                }

                public function reverseTransform($value)
                {
                    unset($value['dealId']);
                    $ret = ['properties' => []];

                    foreach ($value as $paramName => $paramValue) {
                        if (is_array($paramValue)) {
                            $paramValue = implode(';', $paramValue);
                        }
                        $ret['properties'][] = ['name' => $paramName, 'value' => $paramValue];
                    }

                    return $ret;
                }
            })
        ;

        $lifecycleStages = $hubspotService->getLifeCycleStages();
        $stageChoices = [];

        if (is_array($lifecycleStages)) {
            foreach ($lifecycleStages as $stage) {
                $stageChoices[$stage['label']] = $stage['stageId'];
            }
        }

        $formBuilder
            ->add('dealname', TextType::class, ['required' => true, 'label' => false])
            ->add('dealstage', ChoiceType::class, [
                'required' => true,
                'label' => 'Phase de la transaction',
                'choices' => $stageChoices,
                'multiple' => false,
                'expanded' => false
            ])
        ;

        foreach ($openingAccountDealPropertyGroup['properties'] as $property) {
            switch ($property['type']) {
                case 'enumeration':
                    $choices = [];

                    foreach ($property['options'] as $option) {
                        $choices[$option['label']] = $option['value'];
                    }

                    $formBuilder->add($property['name'], ChoiceType::class, [
                        'label' => $property['label'],
                        'multiple' => $property['fieldType'] != 'select',
                        'expanded' => $property['fieldType'] == 'checkbox',
                        'choices' => $choices,
                    ]);
                    break ;
                case 'string':
                default:
                    $formBuilder->add($property['name'], $property['fieldType'] == 'text' ? TextType::class : TextareaType::class, [
                        'label' => $property['label']
                    ]);
                    break ;
            }
        }

        $formBuilder->add('dealId', HiddenType::class);

        if (isset($dealDetail['dealId']) && !empty($dealDetail['dealId'])) {
            $formBuilder->setAction($this->generateUrl('app_admin_advanceduser_account_deal_form', ['dealId' => $dealDetail['dealId'], 'userId' => $userId]));
        } else {
            $formBuilder->setAction($this->generateUrl('app_admin_advanceduser_account_deal_form', ['userId' => $userId]));
        }

        return $formBuilder->getForm();
    }

    /**
     * @param Prospect|null $prospect
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getProspectForm(Prospect $prospect = null)
    {
        if ($prospect === null) {
            $prospect = new Prospect();
            $routeParams = [];
        } else {
            $routeParams = ['id' => $prospect->getId()];
        }

        return $this->createForm(ProspectType::class, $prospect, ['action' => $this->generateUrl('app_admin_advanceduser_prospect_form', $routeParams)]);
    }

    public function ajaxProspectFormAction(Request $request)
    {
        if ($request->get('id') !== null) {
            $prospect = $this->get('app.repository.prospect')->find($request->get('id'));
        } else {
            $prospect = null;
        }

        $form = $this->getProspectForm($prospect);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Prospect $prospect */
            $prospect = $form->getData();

            $manager = $this->getDoctrine()->getManager();

            if ($prospect->getId() === null) {
                $prospect->setCreatedBy($this->getUser());
            }

            $prospect->setUpdatedBy($this->getUser());

            $manager->persist($prospect);
            $manager->flush();
        }

        return $this->render('@App/Admin/AdvancedUser/_prospect.html.twig', ['form' => $form->createView()]);
    }
}
