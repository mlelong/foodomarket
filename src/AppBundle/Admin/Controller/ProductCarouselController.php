<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\ProductCarousel;
use AppBundle\Repository\ProductCarouselRepository;
use Sylius\Component\Core\Model\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductCarouselController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var ProductCarouselRepository $productCarouselRepository */
        $productCarouselRepository = $this->get('app.repository.product_carousel');
        /** @var ProductCarousel[] $products */
        $products = $productCarouselRepository->findAll();
        $productCarousel = [];

        foreach ($products as $product) {
            $productCarousel[] = [
                'name' => $product->getProduct()->getName(),
                'unit' => $product->getUnit(),
                'supplierPrices' => $product->getSupplierPrices()
            ];
        }

        return $this->render('@App/Admin/ProductCarousel/index.html.twig', [
            'product_carousel' => $productCarousel
        ]);
    }

    public function submitAction(Request $request)
    {
        $data = $request->request->all();
        $productRepository = $this->get('sylius.repository.product');

        $manager = $this->getDoctrine()->getManager();
        /** @var ProductCarouselRepository $productCarouselRepository */
        $productCarouselRepository = $this->get('app.repository.product_carousel');
        /** @var ProductCarousel[] $allProducts */
        $allProducts = $productCarouselRepository->findAll();

        foreach ($data as $productData) {
            /** @var Product $product */
            $product = $productRepository->findByName($productData['product'], 'fr_FR')[0];
            /** @var ProductCarousel|null $productCarousel */
            $productCarousel = null;

            foreach ($allProducts as $index => $_productCarousel) {
                if ($_productCarousel->getProduct()->getName() === $product->getName()) {
                    $productCarousel = $_productCarousel;

                    unset($allProducts[$index]);
                    break ;
                }
            }

            if ($productCarousel === null) {
                $productCarousel = new ProductCarousel();

                $productCarousel->setProduct($product);
            }

            $productCarousel->setUnit($productData['unit']);
            $productCarousel->setSupplierPrices($productData['supplierPrices']);

            $manager->persist($productCarousel);
            $manager->flush();
        }

        foreach ($allProducts as $removedProduct) {
            $productCarouselRepository->remove($removedProduct);
        }

        return new JsonResponse(['ok' => true]);
    }

    public function searchProductAction(Request $request)
    {
        $query = $request->get('query');
        $productRepository = $this->get('sylius.repository.product');
        /** @var Product[] $products */
        $products = $productRepository->findByNamePart($query, 'fr_FR');
        $result = [];

        foreach ($products as $product) {
            $result[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
            ];
        }

        return new JsonResponse(['items' => $result]);
    }
}