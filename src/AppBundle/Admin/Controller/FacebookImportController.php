<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\ContactRequest;
use AppBundle\Entity\Prospect;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FacebookImportController extends Controller
{
    public function indexAction(Request $request)
    {
        $form = $this
            ->createFormBuilder()
            ->add('file', FileType::class, ['required' => true, 'label' => 'Fichier CSV'])
            ->add('submit', SubmitType::class, ['label' => 'Ok', 'attr' => ['class' => 'ui button primary']])
            ->getForm()
        ;

        $result = null;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if ($data) {
                $result = [
                    'existing' => 0,
                    'new' => 0,
                ];

                /** @var UploadedFile $file */
                $file = $data['file'];
                $tsvFile = fopen($file->getPathname(), 'rb');

                // Ignore header line
                $header = fgetcsv($tsvFile, 0, "\t");

                $this->sanitize($header);

                $emailIndex = array_search('email', $header);
                $restaurantIndex = array_search('nom_du_restaurant', $header);

                if ($emailIndex !== false && $restaurantIndex !== false) {
                    $manager = $this->getDoctrine()->getManager();
                    $prospectRepository = $this->get('app.repository.prospect');
                    $hubspotService = $this->get('app.services.hubspot');

                    while (($lead = fgetcsv($tsvFile, 0, "\t"))) {
                        $this->sanitize($lead);

                        if (!isset($lead[$emailIndex]) || !isset($lead[$restaurantIndex])) {
                            continue;
                        }

                        $email = strtolower($lead[$emailIndex]);
                        $restaurantName = trim($lead[$restaurantIndex], '"');

                        $prospectFoodo = $prospectRepository->findOneBy(['email' => $email]);

                        if ($prospectFoodo === null) {
                            $prospectFoodo = new Prospect();

                            $prospectFoodo->setEmail($email);
                            $prospectFoodo->setRestaurantName($restaurantName);
                            $prospectFoodo->setSource('facebook');
                            $prospectFoodo->setUtmCampaign('facebook_lead_form');
                            $prospectFoodo->setNewsletterPrices(true);

                            $manager->persist($prospectFoodo);

                            $manager->flush();

                            ++$result['new'];
                        } else {
                            ++$result['existing'];

                            $prospectFoodo->setRestaurantName($restaurantName);
                            $manager->flush();
                        }

//                        $found = false;
//
//                        foreach ($prospectFoodo->getContactRequests() as $contactRequest) {
//                            if (strtolower($contactRequest->getEmail()) === $email
//                                && $contactRequest->getUtmCampaign() === 'facebook_lead_form') {
//                                $found = true;
//                                break ;
//                            }
//                        }
//
//                        if (!$found) {
//                            $contactRequest = new ContactRequest();
//                            $contactRequest->setEmail($email);
//                            $contactRequest->setRestaurantName($restaurantName);
//                            $contactRequest->setUtmSource('facebook');
//                            $contactRequest->setUtmCampaign('facebook_lead_form');
//
//                            $manager->persist($contactRequest);
//                            $prospectFoodo->addContactRequest($contactRequest);
//
//                            $manager->flush();
//                        }

                        $hubspotService->createContactAndSubscriptionHoppy($prospectFoodo);
                    }
                }
            }
        }

        return $this->render('AppBundle:Admin/FacebookImport:index.html.twig', ['form' => $form->createView(), 'result' => $result]);
    }

    private function sanitize(&$strings) {
        foreach ($strings as &$string) {
            $string = mb_convert_encoding($string, 'utf8', 'utf16');
        }
    }
}