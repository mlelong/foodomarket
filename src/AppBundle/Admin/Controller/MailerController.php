<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\Email;
use AppBundle\Entity\Mercuriale;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierCategory;
use AppBundle\Services\Mailer\AbstractMailManager;
use AppBundle\Services\Mailer\SendInBlueManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailerController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var Prospect[] $prospects */
        $prospects = $this->get('app.repository.prospect')->findAll();
        $prospectsArray = [];
        $supplierCategoryArray = [];

        $prospectId = $request->get('prospect', null);

        foreach ($prospects as $prospect) {
            $prospectsArray[$prospect->getId()] = [
                'id' => $prospect->getId(),
                'email' => $prospect->getEmail(),
                'contactName' => $prospect->getContactName()
            ];

            $suppliers = $prospect->getSuppliers();

            foreach ($suppliers as $supplier) {
                if (!isset($supplierCategoryArray[$supplier->getId()])) {
                    $supplierCategoryArray[$supplier->getId()] = [
                        'id' => $supplier->getId(),
                        'name' => $supplier->__tostring()
                    ];
                }
            }
        }

        $prospectsArray = array_values($prospectsArray);
        $supplierCategoryArray = array_values($supplierCategoryArray);
        $restaurants = $this->get('app.repository.restaurant')->findAll();

        return $this->render('@App/Admin/Mailer/index.html.twig', [
            'prospects' => $prospectsArray,
            'suppliers' => $supplierCategoryArray,
            'selectedProspect' => $prospectId,
            'restaurants' => $restaurants
        ]);
    }

    public function batchAction()
    {
        /** @var Prospect[] $prospects */
        $prospects = $this->get('app.repository.prospect')->findAll();
        $data = [];

        foreach ($prospects as $prospect) {
            $suppliers = $prospect->getSuppliers();

            foreach ($suppliers as $supplier) {
                if (!isset($data[$supplier->getId()])) {
                    $data[$supplier->getId()] = [
                        'id' => $supplier->getId(),
                        'name' => $supplier->__tostring()
                    ];
                }
            }
        }

        return new JsonResponse(['data' => array_values($data)]);
    }

    public function transactionnalAction()
    {
        /** @var Prospect[] $prospects */
        $prospects = $this->get('app.repository.prospect')->findAll();
        $data = [];

        foreach ($prospects as $prospect) {
            $data[$prospect->getId()] = [
                'id' => $prospect->getId(),
                'email' => $prospect->getEmail()
            ];
        }

        return new JsonResponse(['data' => array_values($data)]);
    }

    public function sendPricesAction(Request $request)
    {
        $suppliers = $request->get('suppliers');
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($request->get('dst'));
        /** @var QueryBuilder $qb */
        $qb = $this->get('app.repository.supplier_category')->createQueryBuilder('sc');
        /** @var SupplierCategory[] $supplierCategories */
        $supplierCategories = $qb->where($qb->expr()->in('sc.id', $suppliers))->getQuery()->getResult();
        $mercurialeRepository = $this->get('app.repository.mercuriale');
        /** @var Mercuriale[] $mercuriales */
        $mercuriales = [];

        foreach ($supplierCategories as $category) {
            $mercuriales[] = $mercurialeRepository->findOneBy(['category' => $category->getCategory(), 'supplier' => $category->getSupplier()]);
        }

        /** @var SendInBlueManager $sendInBlueManager */
        $sendInBlueManager = $this->get('app.sendinblue.manager');

        $genericMercurialeTemplateId = 8;
        $uploadHelper = $this->get('vich_uploader.templating.helper.uploader_helper');
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        foreach ($mercuriales as $mercuriale) {
            $mercurialeLink = $baseUrl . $uploadHelper->asset($mercuriale, 'file');

            $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);
            $emailBuilder
                ->setTemplateId($genericMercurialeTemplateId)
                ->addVariable('EMAIL', $prospect->getEmail())
                ->addVariable('FIRSTNAME', $prospect->getFirstName())
                ->addVariable('MERCURIAL_URL', $mercurialeLink)
                ->addVariable('SUPPLIER', $mercuriale->getSupplier()->getFacadeSupplier()->getName())
                ->addVariable('CATEGORY', Supplier::getDisplayCategory($mercuriale->getCategory()))
                ->addVariable('HOWTO_ORDER', nl2br($mercuriale->getSupplier()->getFacadeSupplier()->getHowToOrder()))
                ->addVariable('INFORMATIONS_GREEN', '')
                ->addVariable('INFORMATIONS_ORANGE', '')
                ->addVariable('INFORMATIONS_RED', '')
            ;

            $emailBuilder
                ->addTo($prospect->getEmail(), $prospect->getFirstName())
                ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
            ;

            foreach($prospect->getSupplementaryEmailsArray() as $suppEmail) {
                $emailBuilder->addTo($suppEmail);
            }
            $sendInBlueManager->addEmailToQueue($emailBuilder->build());
        }

        return new JsonResponse(['ok' => true]);
    }

    public function sendMailAction(Request $request)
    {
        $prospectRepository = $this->get('app.repository.prospect');
        $type = $request->get('type');
        $suppliers = $request->get('suppliers');
        $prospectsIds = $request->get('prospects');
        $restaurantIds = $request->get('restaurants');
        $tpl = $request->get('tpl');
        $provider = $request->get('provider');

        /** @var Prospect[] $prospects */
        $prospects = [];

        /** @var Mercuriale $mercuriale */
        $mercuriale = null;

        // testing mail checkbox in the form !
        $testingEmails = $request->get('_debug');

        if ($request->get('sendToAll') == '0' && $request->get('sendToCustomers') == '0' && $request->get('sendToProspects') == '0') {
            if (!is_null($suppliers) && intval($suppliers)) {
                $qb = $prospectRepository->createQueryBuilder('p');

                $additionalProspects = $qb
                    ->join('p.suppliers', 'suppliers', 'WITH', 'suppliers = :supplier')
                    ->setParameter('supplier', $suppliers)
                    ->getQuery()
                    ->getResult();

                /** @var Prospect $prospect */
                foreach ($additionalProspects as $prospect) {
                    $prospects[$prospect->getId()] = $prospect;
                }

                $supplierCategory = $this->getDoctrine()->getRepository(SupplierCategory::class)->find($suppliers);
                if ($supplierCategory) {
                    $mercuriale = $this->get('app.repository.mercuriale')->findOneBy(['category' => $supplierCategory->getCategory(), 'supplier' => $supplierCategory->getSupplier()]);
                }
            }

            if ($restaurantIds !== 'null' && !empty($restaurantIds)) {
                $qb = $this->get('app.repository.restaurant')->createQueryBuilder('r');
                /** @var Restaurant[] $restaurants */
                $restaurants = $qb->where($qb->expr()->in('r.id', explode(',', $restaurantIds)))->getQuery()->getResult();

                foreach ($restaurants as $restaurant) {
                    foreach ($restaurant->getCustomers() as $customer) {
                        $prospects[$customer->getProspect()->getId()] = $customer->getProspect();
                    }
                }
            }

            if ($prospectsIds !== 'null' && !empty($prospectsIds)) {
                $qb = $prospectRepository->createQueryBuilder('p');
                $additionalProspects = $qb->where($qb->expr()->in('p.id', explode(',', $prospectsIds)))->getQuery()->getResult();

                /** @var Prospect $prospect */
                foreach ($additionalProspects as $prospect) {
                    $prospects[$prospect->getId()] = $prospect;
                }
            }
        }

        if ($request->get('sendToAll') == '1') {
            $prospects = $prospectRepository->findAll();
        } else if ($request->get('sendToCustomers') == '1') {
            $prospects = $prospectRepository->createQueryBuilder('p')
                ->leftJoin('p.suppliers', 'suppliers')
                ->andWhere("suppliers IS NOT NULL")
                ->getQuery()
                ->getResult()
            ;
        } else if ($request->get('sendToProspects') == '1') {
            $prospects = $prospectRepository->createQueryBuilder('p')
                ->leftJoin('p.suppliers', 'suppliers')
                ->andWhere("suppliers IS NULL")
                ->getQuery()
                ->getResult()
            ;
        }

        /** @var AbstractMailManager $mailManager */
        $mailManager = null;

        switch ($provider) {
            case Email::MANAGER_SENDINBLUE:
                $mailManager = $this->get('app.sendinblue.manager');
                break ;
            case Email::MANAGER_MAILJET:
                $mailManager = $this->get('app.mailjet.manager');
                break ;
        }

        $templates = $mailManager->getTemplates();
        $template = null;

        foreach ($templates as $template_) {
            if ($template_->getTemplateId() == $tpl) {
                $template = $template_;
                break ;
            }
        }

        $dsts = [];

        foreach ($prospects as $prospect) {
            $dsts[] = $prospect->getEmail();
            $vars = $template->getContentVars();
            $templateVars = [];

            foreach ($vars as $var) {
                $value = '';
                $replacementVar = $request->get($var);

                if ($replacementVar !== null && !empty($replacementVar)) {
                    $value = nl2br($replacementVar);
                } else {
                    switch ($var) {
                        case 'prénom':
                        case 'FIRSTNAME':
                        case 'firstname':
                            $value = $prospect->getFirstName();
                            break;
                        case 'CATEGORY':
                            if ($mercuriale !== null) {
                                $value = Supplier::getDisplayCategory($mercuriale->getCategory());
                            } else {
                                $value = '';
                            }
                            break;
                        case 'HOWTO_ORDER':
                            if ($mercuriale !== null) {
                                $value = nl2br($mercuriale->getSupplier()->getHowToOrder());
                            } else {
                                $value = '';
                            }
                            break;
                        case 'SUPPLIER':
                            if ($mercuriale !== null) {
                                $value = nl2br($mercuriale->getSupplier()->getFacadeSupplier()->getName());
                            } else {
                                $value = '';
                            }
                            break;
                        case 'MERCURIAL_URL':
                            if ($mercuriale !== null) {
                                $uploadHelper = $this->get('vich_uploader.templating.helper.uploader_helper');

                                $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
                                $value = $baseUrl . $uploadHelper->asset($mercuriale, 'file');
                            } else {
                                $value = '';
                            }
                            break;
                    }
                }

                $templateVars[$var] = $value;
            }

            $emailBuilder = $this->get('app.factory.email')->createBuilder($provider);

            $emailBuilder->setTemplateId($tpl);

            foreach ($templateVars as $varName => $varValue) {
                $emailBuilder->addVariable($varName, $varValue);
            }

            if ($testingEmails) {
                $emailBuilder
                    ->addTo($this->getParameter('email_debug'), $prospect->getFirstName());
            } else {
                if ($prospect->isEmailSubscribed() || $prospect->isNewsletterPrices()) {
                    $emailBuilder
                        ->addTo($prospect->getEmail(), $prospect->getFirstName())
                        ->addVariable('EMAIL', $prospect->getEmail())
                        ;

                    if ($tpl != 8) {
                        $emailBuilder->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com');
                    }

                    foreach ($prospect->getSupplementaryEmailsArray() as $suppEmail) {
                        $emailBuilder->addTo($suppEmail);
                    }
                }
            }

            $mail = $emailBuilder->build();

            if (count($mail->getTo())) {
                $mailManager->addEmailToQueue($mail);
            }

        }

        return new JsonResponse(['ok' => true, 'type' => $type, 'suppliers' => $suppliers, 'prospects' => $prospects, 'tpl' => $tpl, 'provider' => $provider, 'dsts' => implode(', ', $dsts)]);
    }

    public function templatesAction()
    {
        $templates = [];
        $providers = [
            Email::MANAGER_MAILJET => $this->get('app.mailjet.manager'),
            Email::MANAGER_SENDINBLUE => $this->get('app.sendinblue.manager'),
        ];

        /**
         * @var string $providerKey
         * @var AbstractMailManager $provider
         */
        foreach ($providers as $providerKey => $provider) {
            $templates[$providerKey] = [
                'providerKey' => $providerKey,
                'provider' => $providerKey,
                'templates' => $provider->getTemplates()
            ];
        }

        return $this->render('@App/Admin/Mailer/templates.html.twig', ['templates' => $templates]);
    }

    public function subjectAction(Request $request)
    {
        $provider = $request->get('provider');
        $templateId = $request->get('templateId');

        $mailManager = null;

        switch ($provider) {
            case Email::MANAGER_MAILJET:
                $mailManager = $this->get('app.mailjet.manager');
                break ;
            case Email::MANAGER_SENDINBLUE:
                $mailManager = $this->get('app.sendinblue.manager');
                break ;
        }

        if ($mailManager === null) {
            return new Response('---');
        }

        $templates = $mailManager->getTemplates();

        foreach ($templates as $template) {
            if ($template->getTemplateId() == $templateId) {
                return new Response($template->getSubject());
            }
        }

        return new Response('---');
    }

    public function bodyAction(Request $request)
    {
        $provider = $request->get('provider');
        $templateId = $request->get('templateId');

        $mailManager = null;

        switch ($provider) {
            case Email::MANAGER_MAILJET:
                $mailManager = $this->get('app.mailjet.manager');
                break ;
            case Email::MANAGER_SENDINBLUE:
                $mailManager = $this->get('app.sendinblue.manager');
                break ;
        }

        if ($mailManager === null) {
            return new Response('---');
        }

        $templates = $mailManager->getTemplates();

        foreach ($templates as $template) {
            if ($template->getTemplateId() == $templateId) {
                return new Response($template->getContent());
            }
        }

        return new Response('---');
    }

    public function resendAction(Request $request)
    {
        $emailId = $request->get('emailId');
        /** @var Email $email */
        $email = $this->get('app.repository.email')->find($emailId);

        $mailManager = null;

        switch ($email->getMailManagerName()) {
            case Email::MANAGER_MAILJET:
                $mailManager = $this->get('app.mailjet.manager');
                break ;
            case Email::MANAGER_SENDINBLUE:
                $mailManager = $this->get('app.sendinblue.manager');
                break ;
        }

        $mailManager->sendEmail($email);

        $message = array_key_exists('message', $email->getApiResponse())?$email->getApiResponse()['message']: 'cause inconnue';
        if ($email->getStatus() == Email::STATUS_SENT) {
            $this->addFlash('success', $message);
        } else {
            $this->addFlash('error', $message);
        }

        return $this->redirectToRoute('app_admin_email_show', ['id' => $emailId]);
    }
}