<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\Email;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Customer;
use AppBundle\Entity\ShopUser;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Entity\SupplierCategory;
use AppBundle\Form\RestaurantType;
use Sylius\Bundle\CustomerBundle\Form\Type\CustomerType;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Grid\Parameters;
use Sylius\Component\Grid\View\GridView;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\Iban;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ProspectController extends ResourceController
{
    public function editAction(Request $request)
    {
        $prospectId = $request->attributes->get('id');
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($prospectId);

        $form = $this->getProspectForm($prospect);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($prospect);
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add('success', 'La modification du prospect a réussi.');
            } else {
                $this->get('session')->getFlashBag()->add('error', 'La modification du prospect a échoué.');
            }
        }

        return $this->render('@App/Admin/Prospect/edit.html.twig', [
            'resource' => $prospect,
            'form' => $form->createView(),
            'formCustomers' => $this->getProspectCustomersForm($prospect)->createView(),
            'formRestaurants' => $this->getRestaurantsForm($prospect)->createView()
        ]);
    }

    public function editCustomersAction(Request $request)
    {
        $prospectId = $request->attributes->get('id');
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($prospectId);

        $form = $this->getProspectCustomersForm($prospect);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager = $this->getDoctrine()->getManager();

                $manager->persist($prospect);
                $manager->flush();

                $this->get('session')->getFlashBag()->add('success', 'La modification/ajout de l\'utilisateur du shop a réussi.');
            } else {
                $this->get('session')->getFlashBag()->add('error', 'La modification/ajout de l\'utilisateur du shop a échoué.');
            }
        }

        return $this->redirectToRoute('app_admin_admin_prospect_edit', ['id' => $prospectId]);
    }

    public function editRestaurantsAction(Request $request)
    {
        $prospectId = $request->attributes->get('id');
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($prospectId);

        $form = $this->getRestaurantsForm($prospect);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $restaurants = $form->getData()['restaurants'];
                $manager = $this->getDoctrine()->getManager();

                foreach ($restaurants as $restaurant) {
                    $manager->persist($restaurant);
                }

                $manager->flush();
                $this->get('session')->getFlashBag()->add('success', 'Restaurant(s) ajouté(s)/mis à jour.');
            } else {
                $errors = "";

                foreach ($form->getErrors(true) as $error) {
                    $errors .= "{$error->getMessage()}, ";
                }

                $errors = substr($errors, 0, -2);

                $this->get('session')->getFlashBag()->add('error', "Restaurant(s) non ajouté(s)/mis à jour: $errors");
            }
        }

        return $this->redirectToRoute('app_admin_admin_prospect_edit', ['id' => $prospectId]);
    }

    private function getRestaurantsForm(Prospect $prospect)
    {
        $restaurants = [];

        foreach ($prospect->getCustomers() as $customer) {
            foreach ($customer->getRestaurants() as $restaurant) {
                $restaurants[$restaurant->getId()] = $restaurant;
            }
        }

        $formBuilder = $this->createFormBuilder(null, [
            'action' => $this->generateUrl('app_admin_admin_prospect_editRestaurants', ['id' => $prospect->getId()]),
            'validation_groups' => 'back-office'
        ]);

        return $formBuilder
            ->add('restaurants', CollectionType::class, [
                'label' => 'Restaurants',
                'entry_type' => RestaurantType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'data' => array_values($restaurants),
                'validation_groups' => 'back-office',
                'entry_options' => [
                    'prospect' => $prospect
                ],
            ])
            ->getForm()
        ;
    }

    /**
     * @param Prospect $prospect
     * @return FormInterface
     */
    private function getProspectForm(Prospect $prospect)
    {
        $builder = $this->createFormBuilder($prospect, [
            'action' => $this->generateUrl('app_admin_admin_prospect_edit', ['id' => $prospect->getId()]),
            'data_class' => Prospect::class
        ]);

        return $builder
            ->add('type', ChoiceType::class, [
                'required' => true,
                'label' => 'Type de prospect',
                'multiple' => false,
                'expanded' => true,
                'choices' => [
                    'Professionnel' => Prospect::TYPE_PROFESSIONNAL,
                    'Particulier' => Prospect::TYPE_INDIVIDUAL
                ]
            ])
            ->add('civility', ChoiceType::class, [
                'label' => 'Civilité',
                'choices' => [
                    'M.' => 'MONSIEUR',
                    'Mme.' => 'MADAME'
                ],
                'multiple' => false,
                'expanded' => true
            ])
            ->add('email', EmailType::class, ['label' => 'Email'])
            ->add('supplementaryEmails', TextType::class, ['required' => false, 'label' => 'Emails supplémentaires)'])
            ->add('firstName', TextType::class, ['label' => 'Prénom'])
            ->add('contactName', TextType::class, ['label' => 'Nom'])
            ->add('restaurantName', TextType::class, ['label' => 'Restaurant'])
            ->add('address', TextType::class, ['label' => 'Adresse du restaurant'])
            ->add('zipCode', TextType::class, ['label' => 'Code postal du restaurant'])
            ->add('city', TextType::class, ['label' => 'Ville du restaurant'])

            ->add('phone', TextType::class, ['required' => false, 'label' => 'Numéro de téléphone fixe'])
            ->add('mobile', TextType::class, ['required' => false, 'label' => 'Numéro de téléphone portable'])

            ->add('companyName', TextType::class, ['required' => false, 'label' => 'Société'])
            ->add('siren', TextType::class, ['required' => false, 'label' => 'SIREN'])

            ->add('iban', TextType::class, ['required' => false, 'constraints' => [new Iban()]])
            ->add('kbisFile', VichFileType::class, ['required' => false, 'label' => 'KBis'])
            ->add('ribFile', VichFileType::class, ['required' => false, 'label' => 'RIB'])

            ->add('canUseArbitration', CheckboxType::class, ['required' => false, 'label' => "Peut utiliser l'arbitrage"])

            ->add('categories', ChoiceType::class, [
                'label' => "Catégories de produit qui l'intéressent",
                'required' => false,
                'choices' => [
                    'Fruits & Légumes' => Supplier::CAT_FRUITS_LEGUMES,
                    'Crèmerie' => Supplier::CAT_CREMERIE,
                    'Boucherie/Charcuterie' => Supplier::CAT_BOUCHERIE,
                    'Marée' => Supplier::CAT_MAREE,
                    'Epicerie' => Supplier::CAT_EPICERIE
                ],
                'multiple' => true,
                'expanded' => true
            ])
            ->add('currentSupplier', TextareaType::class, ['label' => 'Fournisseurs actuels', 'required' => false])
            ->add('deliveryHours', TextType::class, ['label' => 'Horaires de livraison souhaités', 'required' => true])

            ->add('emailSubscribed', CheckboxType::class, ['required' => false, 'label' => 'Inscrit aux newsletters'])
            ->add('newsletterPrices', CheckboxType::class, ['required' => false, 'label' => 'Inscrit aux newsletters prix'])

            ->add('suppliers', EntityType::class, [
                'required' => false,
                'label' => 'Comptes grossiste ouverts',
                'class' => SupplierCategory::class,
                'multiple' => true,
                'expanded' => false,
                'attr' => ['class' => 'ui search']
            ])
            ->getForm()
        ;
    }

    private function getProspectCustomersForm(Prospect $prospect)
    {
        $builder = $this->createFormBuilder($prospect, [
            'action' => $this->generateUrl('app_admin_admin_prospect_editCustomers', ['id' => $prospect->getId()]),
            'data_class' => Prospect::class
        ]);

        return $builder
            ->add('customers', CollectionType::class, [
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => CustomerType::class,
                'label' => 'Utilisateurs du shop',
                'by_reference' => false
            ])
            ->getForm()
        ;
    }

    public function sendFinalizeRegistrationEmailAction(Request $request)
    {
        $prospectRepository = $this->get('app.repository.prospect');
        /** @var Prospect $prospect */
        $prospect = $prospectRepository->find($request->get('id'));

        if (empty($prospect->getHash())) {
            $prospect->setHash($this->get('sylius.shop_user.token_generator.email_verification')->generate());
        }

        $this->getDoctrine()->getManager()->flush();

        $this->get('app.sendinblue.manager')->addEmailToQueue(
            $this->get('app.factory.email')
                ->createBuilder(Email::MANAGER_SENDINBLUE)
                ->setTemplateId(17)
                ->addTo($prospect->getEmail(), $prospect->getFirstName())
                ->addVariable('FIRSTNAME', $prospect->getFirstName())
                ->addVariable('URL', $this->generateUrl('app_front_finalize_registration_page', ['token' => $prospect->getHash()], UrlGeneratorInterface::ABSOLUTE_URL))
                ->build()
        );

        $this->get('session')->getFlashBag()->add("success", "Mail de finalisation d'inscription bien envoyé");

        return $this->redirectToRoute('app_admin_admin_prospect_show', ['id' => $prospect->getId()]);
    }

    public function sendRegistrationEmailAction(Request $request)
    {
        $prospectRepository = $this->get('app.repository.prospect');
        /** @var Prospect $prospect */
        $prospect = $prospectRepository->find($request->get('id'));

        if (empty($prospect->getHash())) {
            $prospect->setHash($this->get('sylius.shop_user.token_generator.email_verification')->generate());
        }

        $this->getDoctrine()->getManager()->flush();

        $this->get('app.sendinblue.manager')->addEmailToQueue(
            $this->get('app.factory.email')
                ->createBuilder(Email::MANAGER_SENDINBLUE)
                ->setTemplateId(46)
                ->addTo($prospect->getEmail(), $prospect->getFirstName())
                ->addVariable('FIRSTNAME', $prospect->getFirstName())
                ->addVariable('URL', $this->generateUrl('app_front_subscription_page', ['token' => $prospect->getHash()], UrlGeneratorInterface::ABSOLUTE_URL))
                ->build()
        );

        $this->get('session')->getFlashBag()->add("success", "Mail de pré-inscription bien envoyé");

        return $this->redirectToRoute('app_admin_admin_prospect_show', ['id' => $prospect->getId()]);
    }

    public function sendAccountValidationEmailAction(Request $request)
    {
        $prospectRepository = $this->get('app.repository.prospect');
        /** @var Prospect $prospect */
        $prospect = $prospectRepository->find($request->get('id'));

        if (!$prospect->isEnabled()) {
            foreach ($prospect->getCustomers() as $customer) {
                $shopUser = $customer->getShopUser();

                if (!$shopUser->isVerified()) {
                    $verifyUrl = $this->generateUrl('app_front_register_verify_page', ['token' => $shopUser->getEmailVerificationToken()], Router::ABSOLUTE_URL);

                    // if we are in app_dev, the url will be generated in app_dev...
                    // everyone uses the BO in app_dev
                    // so sending the verification email will show in app_dev
                    // let's replace this manually
                    $verifyUrl = str_replace('app_dev.php/', '', $verifyUrl);

                    $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

                    $emailBuilder->setTemplateId(17);
                    $emailBuilder->addTo($prospect->getEmail());
                    $emailBuilder->addVariable('FIRSTNAME', $shopUser->getCustomer()->getFirstName());
                    $emailBuilder->addVariable('URL', $verifyUrl);

                    $this->get('app.sendinblue.manager')->addEmailToQueue($emailBuilder->build());
                }
            }

            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('app_admin_admin_prospect_show', ['id' => $prospect->getId()]);
    }

    public function createShopAccountAction(Request $request)
    {
        $entityManager = $this->get('doctrine.orm.default_entity_manager');
        $prospectRepository = $this->get('app.repository.prospect');
        $restaurantRepository = $this->get('app.repository.restaurant');
        $customerRepository = $this->get('sylius.repository.customer');
        $shopUserRepository = $this->get('sylius.repository.shop_user');

        // get prospect
        /** @var Prospect $prospect */
        $prospect = $prospectRepository->find($request->get('id'));
        $prospect->setNewsletterPrices(true);

        // check & create restaurant
        $restaurants = $restaurantRepository->findBy(['name' => $prospect->getRestaurantName()]);

        $restaurant = null;
        if (empty($restaurants)) {
            $restaurant = new Restaurant();

            $restaurant->setName($prospect->getRestaurantName());
            $restaurant->setEnabled(true);

            if ($prospect->getDeliveryHours() !== null) {
                $restaurant->setDeliveryHours($prospect->getDeliveryHours());
            }

            if ($prospect->getSiren() !== null) {
                $restaurant->setSiren(substr(preg_replace('/[^ -~]/', '', $prospect->getSiren()), 0, 9));
            }

            $restaurant->setCountryCode('FR');
            $restaurant->setInterlocutor(trim("{$prospect->getFirstName()} {$prospect->getContactName()}"));
            $restaurant->setStreet($prospect->getAddress());
            $restaurant->setCity($prospect->getCity());
            $restaurant->setPostcode($prospect->getZipCode());

            $restaurantRepository->add($restaurant);
        } else {
            $restaurant = $restaurants[0];
        }

        if (is_null($restaurant)) {
            $this->addFlash('error', "Le restaurant n'a pas été créé");
            return new JsonResponse(['ok' => false, 'message' => "Le restaurant n'a pas été créé"]);
        }

        // check & create customer
        $customer = $customerRepository->findOneByEmail($prospect->getEmail());
        if (is_null($customer)) {
            $customer = new Customer();

            $customer->setProspect($prospect);
            $customer->setEmail($prospect->getEmail());
            $customer->setFirstName($prospect->getFirstName());
            $customer->setLastName($prospect->getContactName() !== null ? $prospect->getContactName() : '');
            /** @noinspection PhpParamsInspection */
            $customer->setGroup($this->get('sylius.repository.customer_group')->findOneBy(['code' => 'restaurant']));
            $customer->setPhoneNumber($prospect->getPhone());

            $customer->addRestaurant($restaurant);

            $entityManager->persist($customer);
        }

        $shopUser = $shopUserRepository->findOneByEmail($prospect->getEmail());
        if (is_null($shopUser)) {
            $shopUser = new ShopUser();
            $shopUser->setCustomer($customer);

            $shopUser->setEmail($prospect->getEmail());
            $shopUser->setEnabled(true);
            $password = ucfirst(explode(' ', $prospect->getRestaurantName())[0]).'119';
            $shopUser->setPlainPassword($password);
            $prospect->setPlainPassword($password);
            $shopUser->setUsername($prospect->getEmail());
            $shopUser->setPhone($prospect->getPhone());

            $customer->setShopUser($shopUser);

            $entityManager->persist($shopUser);
        } else {
            if ($shopUser->isEnabled() === false) {
                // activate the shop user
                $shopUser->setEnabled(true);
                $password = ucfirst(explode(' ', $prospect->getRestaurantName())[0]).'119';
                $shopUser->setPlainPassword($password);
                $prospect->setPlainPassword($password);
            } else {
                $this->addFlash('warning', "Le shop user existe déjà et est actif");
                //return new JsonResponse(['ok' => false, 'message' => "Le shop user existe déjà et est actif"]);
            }
        }

        if (is_null($shopUser)) {
            $this->addFlash('error', "Le shop user n'a pas été créé");
            return new JsonResponse(['ok' => false, 'message' => "Le shop user n'a pas été créé"]);
        }

        if (is_null($customer)) {
            $this->addFlash('error', "Le client n'a pas été créé");
            return new JsonResponse(['ok' => false, 'message' => "Le client n'a pas été créé"]);
        }

        $entityManager->flush();

        // send email with identifiants
        $emailManager = $this->get('app.sendinblue.manager');
        $emailBuilder = $this->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);
        $emailBuilder
            ->setTemplateId(27)
            ->addVariable('EMAIL', $prospect->getEmail())
            ->addVariable('FIRSTNAME', $prospect->getFirstName())
            ->addVariable('PASSWORD', $prospect->getPlainPassword())
            ->addVariable('COMMENT', '')
            ->addTo($prospect->getEmail(), $prospect->getFirstName())
            ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
        ;

        if ($prospect->getPlainPassword() != '') {
            $emailManager->addEmailToQueue($emailBuilder->build());
            $this->addFlash('success', "Le mail d'ouverture de compte a été envoyé");
        }
        
        return new JsonResponse(['ok' => true]);

    }

    public function openAccountSuppliersAction(Request $request)
    {
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($request->get('id'));
        $partners = $this->get('app.repository.supplier')->getPartners(true);
        $supplierAccountLogRepository = $this->get('app.repository.supplier_account_log');
        $data = [];

        foreach ($partners as $partner) {
            /** @var SupplierCategory[] $supplierCategories */
            $supplierCategories = $this->get('app.repository.supplier_category')->findBySupplier($partner);
            /** @var SupplierAccountLog[] $supplierAccountLogs */
            $supplierAccountLogs = $supplierAccountLogRepository->findBy(['prospect' => $prospect, 'supplierCategory' => $supplierCategories]);

            $status = '';

            foreach ($supplierAccountLogs as $supplierAccountLog) {
                if (!in_array($status, [SupplierAccountLog::STATUS_ACCOUNT_OPENED, SupplierAccountLog::STATUS_ORDERING])) {
                    $status = $supplierAccountLog->getStatus();
                }
            }

            $data[$partner->getId()] = [
                'id' => $partner->getId(),
                'name' => $partner->getDisplayName(true),
                'status' => $status
            ];
        }

        usort($data, function($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });

        return $this->render('@App/Admin/Prospect/_open_account_modal.html.twig', ['data' => $data]);
    }

    public function openAccountAction(Request $request)
    {
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($request->get('id'));

        if (!$this->isProspectOkForAccountOpening($prospect)) {
            return new JsonResponse(['ok' => false, 'message' => "Il manque des informations obligatoires dans le prospect pour l'ouverture de compte ! L'opération a été annulée."]);
        }

        $suppliersIds = explode(',', $request->get('suppliers'));
        $supplierCategories = $this->get('app.repository.supplier_category')->findBySupplier($suppliersIds);
        $manager = $this->getDoctrine()->getManager();
        $emailFactory = $this->get('app.factory.email');
        $emailManager = $this->get('app.sendinblue.manager');
        $errors = [];

        $supplierCategoriesBySupplier = [];

        foreach ($supplierCategories as $supplierCategory) {
            $supplierCategoriesBySupplier[$supplierCategory->getSupplier()->getId()][] = $supplierCategory;
        }

        foreach ($supplierCategoriesBySupplier as $supplierId => $supplierCategories) {
            $categories = [];

            // On enregistre les demandes d'ouverture de compte
            foreach ($supplierCategories as $supplierCategory) {
                $supplierAccountLog = new SupplierAccountLog();
                $supplierAccountLog->setProspect($prospect);
                $supplierAccountLog->setSupplierCategory($supplierCategory);
                $supplierAccountLog->setStatus(SupplierAccountLog::STATUS_WAITING);

                try {
                    $supplierAccountLog->setHash(substr(strtr(base64_encode(bin2hex(random_bytes(32))), '+', '.'), 0, 44));
                } catch (\Exception $e) {
                    $supplierAccountLog->setHash(uniqid());
                }

                $manager->persist($supplierAccountLog);

                // Adds the supplier category to the prospect
                $prospect->addSupplier($supplierCategory);

                $categories[] = Supplier::getDisplayCategory($supplierCategory->getCategory());
            }

            $manager->flush();

            $supplierCategory = $supplierCategories[0];

            $emailBuilder = $emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

            // Send email to :
            if (count($supplierCategory->getSupplier()->getFacadeSupplier()->getCommercialEmails()) == 0) {
                $errors[] = "Nous n'avons pas l'email de ce fournisseur ({$supplierCategory->getSupplier()->getName()})";
                continue ;
            }

            foreach ($supplierCategory->getSupplier()->getFacadeSupplier()->getCommercialEmails() as $email) {
                $emailBuilder->addTo($email);
            }

            $emailBuilder->addVariable('RECIPIENTS', $supplierCategory->getSupplier()->getFacadeSupplier()->getName());

            $categories = implode(', ', $categories);

            if ($supplierId == 17) {
                $gridName = "Grille 3 {$categories}";
            } elseif ($supplierId == 18) {
                $gridName = "Grille 4 {$categories}";
            } elseif ($supplierId == 23) {
                $gridName = "Grille SP {$categories}";
            } else {
                $gridName = $categories;
            }

            // Send email to supplier
            $emailBuilder
                ->setTemplateId(14)
                ->addVariable('PRICE_GRID', $gridName)
            ;

            $emailBuilder
                ->addVariable('RESTAURANT_NAME', $prospect->getRestaurantName())
                ->addVariable('COMPANY', $prospect->getCompanyName())
                ->addVariable('SIREN', $prospect->getSiren())
                ->addVariable('ADDRESS_FULL', trim("{$prospect->getAddress()} {$prospect->getZipCode()} {$prospect->getCity()}"))
                ->addVariable('MANAGER_NAME', "{$prospect->getFirstName()} {$prospect->getContactName()}")
                ->addVariable('INTERLOCUTOR', "{$prospect->getFirstName()} {$prospect->getContactName()}")
                ->addVariable('TELEPHONE', $prospect->getPhone())
                ->addVariable('EMAIL', $prospect->getEmail())
                ->addVariable('DELIVERY_HOURS', $prospect->getDeliveryHours())
            ;

            $kbis = $prospect->getKbis();

            if (!empty($kbis)) {
                $kbisFilePath = $this->getParameter('kbis_path') . $kbis;

                if (file_exists($kbisFilePath)) {
                    $emailBuilder->addAttachment($kbis, file_get_contents($kbisFilePath));
                }
            }

            // TODO: HACK : We should not send an email to Domafrais at this stage (needs to be redone, in a better way)
            if ($supplierId != 39) {
                $emailManager->addEmailToQueue($emailBuilder->build());
            }
        }

        // Send email to client
        $webDir = $this->getParameter('kernel.web_dir');
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $uploaderHelper = $this->get('vich_uploader.templating.helper.uploader_helper');
        $mercurialeRepository = $this->get('app.repository.mercuriale');

        /** @var SupplierCategory[] $supplierCategories */
        foreach ($supplierCategoriesBySupplier as $supplierId => $supplierCategories) {
            $partnerSupplier = $supplierCategories[0]->getSupplier();
            $emailBuilder = $emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

            $emailBuilder
                ->setTemplateId(29)
                ->addTo($prospect->getEmail(), $prospect->getFirstName())
                ->addVariable('FIRSTNAME', $prospect->getFirstName())
                ->addVariable('SUPPLIER', $partnerSupplier->getDisplayName())
                ->addVariable('ORDER_MODALITY', nl2br($partnerSupplier->getFacadeSupplier()->getOrderModality()))
                ->addVariable('ORDER_ADDITIONAL_ELEMENTS', nl2br($partnerSupplier->getFacadeSupplier()->getOrderAdditionalElements()))
//                ->addVariable('INFORMATIONS', $partnerSupplier->getFacadeSupplier()->getHowToOrder())
            ;

            $categories = [];
            $mercuriales = [];

            foreach ($supplierCategories as $supplierCategory) {
                $categories[] = Supplier::getDisplayCategory($supplierCategory->getCategory());
                $mercuriales[Supplier::getDisplayCategory($supplierCategory->getCategory())] = $mercurialeRepository->findOneBy(['category' => $supplierCategory->getCategory(), 'supplier' => $supplierCategory->getSupplier()]);
            }

            $mercurialeUrls = '';
            foreach ($mercuriales as $categorie => $mercuriale) {
                $mercurialeUrls .= sprintf("<a href='%s'>Mercuriale %s</a><br>", $baseUrl . $uploaderHelper->asset($mercuriale, 'file'), $categorie);
            }

            // TODO: Handle the facade and partner correctly next time...
//            if (!empty($partnerSupplier->getSepaFileName())) {
//                $sepaFile = null;
//                $sepaPath = $webDir . $uploaderHelper->asset($partnerSupplier,'sepa');
//                $sepaExtension = substr($sepaPath, strrpos($sepaPath, '.') + 1);
//
//                if (file_exists($sepaPath)) {
//                    $sepaFile = file_get_contents($sepaPath);
//
//                    $emailBuilder->addAttachment("SEPA.$sepaExtension", $sepaFile);
//                }
//            } elseif (!empty($partnerSupplier->getFacadeSupplier()->getSepaFileName())) {
//                $sepaFile = null;
//                $sepaPath = $webDir . $uploaderHelper->asset($partnerSupplier->getFacadeSupplier(),'sepa');
//                $sepaExtension = substr($sepaPath, strrpos($sepaPath, '.') + 1);
//
//                if (file_exists($sepaPath)) {
//                    $sepaFile = file_get_contents($sepaPath);
//
//                    $emailBuilder->addAttachment("SEPA.$sepaExtension", $sepaFile);
//                }
//            }

            $emailBuilder->addVariable('CATEGORIES', implode(', ', $categories));
            $emailBuilder->addVariable('MERCURIALE_URLS', $mercurialeUrls);

            // TODO: HACK : We should not send an email to prospect for Jacob at this stage (needs to be redone, in a better way)
//            if ($supplierId != 37) {
                $emailManager->addEmailToQueue($emailBuilder->build());
//            }
        }

//        $this->get('app.services.hubspot')->createContactAndGridSentAccountNotOpenedHoppy($prospect);

        return new JsonResponse(['ok' => true, 'errors' => $errors]);
    }

    private function isProspectOkForAccountOpening(Prospect $prospect)
    {
        return !empty($prospect->getRestaurantName())
            && !empty($prospect->getCompanyName())
            && !empty($prospect->getSiren())
            && !empty($prospect->getAddress())
            && !empty($prospect->getFirstName())
            && !empty($prospect->getContactName())
            && !empty($prospect->getPhone())
            && !empty($prospect->getEmail())
            && !empty($prospect->getDeliveryHours())
        ;
    }

    public function supplierAccountLogAction(Request $request): Response
    {
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($request->get('id'));
        /** @var SupplierAccountLog[] $supplierAccountLogs */
        $supplierAccountLogs = $this->get('app.repository.supplier_account_log')->findBy(['prospect' => $prospect]);

        $gridDefinition = $this->get('sylius.grid.provider')->get('app_admin_supplier_account_log');
        $gridView = new GridView($supplierAccountLogs, $gridDefinition, new Parameters());

        return $this->render('@App/Admin/Prospect/_supplier_account_logs.html.twig', ['resources' => $gridView]);
    }

    public function contactRequestHistoryAction(Request $request): Response
    {
        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($request->get('id'));
        $contactRequests = $prospect->getContactRequests();

        $gridDefinition = $this->get('sylius.grid.provider')->get('app_admin_contact_request');
        $gridView = new GridView($contactRequests, $gridDefinition, new Parameters());

        return $this->render('@App/Admin/Prospect/_contact_requests.html.twig', ['resources' => $gridView]);
    }

    public function emailHistoryAction(Request $request): Response
    {
        $emailRepository = $this->get('app.repository.email');

        /** @var Prospect $prospect */
        $prospect = $this->get('app.repository.prospect')->find($request->get('id'));
        $qb = $emailRepository->createQueryBuilder('e');

        $emails = $qb
            ->where($qb->expr()->like('e.to', ':to'))
            ->setParameter('to', "%{$prospect->getEmail()}%")
            ->orderBy('e.createdAt', 'DESC')
            ->setMaxResults(15)
            ->getQuery()
            ->getResult()
        ;

        $gridDefinition = $this->get('sylius.grid.provider')->get('app_admin_email');
        $gridView = new GridView($emails, $gridDefinition, new Parameters());

        return $this->render('@App/Admin/Prospect/_emails.html.twig', ['resources' => $gridView]);
    }

    public function bulkExportAction(Request $request): Response
    {
        $supplierCategoryId = intval($request->get('supplier-category'));
        $subscribedToPrices = $request->get('subscribed-prices');
        $subscribedToEmail = $request->get('subscribed-email');
        $isCustomer = intval($request->get('is-customer'));
        $ids = $request->get('ids');

        if (strlen($ids)) {
            $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
            $prospects = $this->resourcesCollectionProvider->get($configuration, $this->repository);
        } else {
            $qb = $this->repository->createQueryBuilder('p');

            if ($subscribedToPrices == 'true') {
                $qb
                    ->andWhere('p.newsletterPrices = :subscribedprices')
                    ->setParameter('subscribedprices', 1);
            }
            if ($subscribedToPrices == 'false') {
                $qb
                    ->andWhere('p.newsletterPrices = :subscribedprices')
                    ->setParameter('subscribedprices', 0);
            }
            if ($subscribedToEmail == 'true') {
                $qb
                    ->andWhere('p.emailSubscribed = :subscribedemail')
                    ->setParameter('subscribedemail', 1);
            }
            if ($subscribedToEmail == 'false') {
                $qb
                    ->andWhere('p.emailSubscribed = :subscribedemail')
                    ->setParameter('subscribedemail', 0);
            }
            if ($supplierCategoryId > 0) {
                $qb
                    ->join('p.suppliers', 'suppliers', 'WITH', 'suppliers = :supplier')
                    ->setParameter('supplier', $supplierCategoryId);
            } else {
                if ($isCustomer == 1) {
                    $qb
                        ->join('p.suppliers', 'suppliers')
                        ->andWhere('suppliers is not null');
                }
                if ($isCustomer == 2) {
                    $qb
                        ->leftJoin('p.suppliers', 'suppliers')
                        ->andWhere('suppliers is null');
                }
            }

            $query = $qb->getQuery();
            $prospects = $query->getResult();
        }

        if (empty($supplierCategoryId)) {
            $filename = 'Export Clients du ' . date('Y-m-d') . '.csv';
        } else {
            $supplierCategory = $this->get('app.repository.supplier_category')->find($supplierCategoryId);
            $filename = "Export Clients {$supplierCategory} du " . date('Y-m-d') . '.csv';
        }

        return new StreamedResponse(function() use($prospects) {
            $stdout = fopen('php://memory', 'r+');

            fputcsv($stdout, ['ID', 'Prénom', 'Nom', 'E-Mail', 'Restaurant', 'Code postal', 'Newsletter', 'Tarifs', 'Fournisseurs', 'Téléphone fixe', 'Téléphone portable', 'Date de création']);

            /** @var Prospect $prospect */
            foreach ($prospects as $prospect) {
                fputcsv($stdout, [
                    $prospect->getId(),
                    $prospect->getFirstName(),
                    $prospect->getContactName(),
                    $prospect->getEmail(),
                    $prospect->getRestaurantName(),
                    $prospect->getZipCode(),
                    $prospect->isEmailSubscribed() ? '1' : '0',
                    $prospect->isNewsletterPrices() ? '1' : '0',
                    implode(', ', $prospect->getSuppliers()->map(function(SupplierCategory $supplierCategory) {
                        return $supplierCategory->__toString();
                    })->toArray()),
                    $prospect->getPhone(),
                    $prospect->getMobile(),
                    $prospect->getCreatedAt() ? $prospect->getCreatedAt()->format('d/m/Y H:i:s') : ''
                ]);
            }

            rewind($stdout);
            echo stream_get_contents($stdout);
        }, 200, ['Content-Type' => 'text/csv', 'Content-Disposition' => "attachment; filename=$filename"]);
    }
}
