<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\ProspectSource;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\InvoiceRepository;
use AppBundle\Repository\OrderRepository;
use AppBundle\Repository\ProspectSourceRepository;
use AppBundle\Services\RedisCacheManager;
use DateTime;
use AppBundle\Entity\Invoice;
use AppBundle\Entity\Order;
use Sonata\Form\Type\DateRangeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use AppBundle\Entity\Prospect;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;


class CustomerSuccesController extends AbstractController
{
    public static function getSubscribedServices()
    {
        return array_merge(
            parent::getSubscribedServices(),
            [
                'app.redis.cache.manager' => RedisCacheManager::class
            ]
        );
    }

    public function refreshCacheAction()
    {
        $redisCacheManager = $this->get('app.redis.cache.manager');

        $redisCacheManager->deleteItem(RedisCacheManager::CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_LIST);
        $redisCacheManager->deleteItemsByPattern(str_replace('{prospectId}', '*', RedisCacheManager::CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_ITEM));

        return $this->redirectToRoute('app_admin_customer_succes_index');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->getForm();
        $form->handleRequest($request);

        $startDateRange = $endDateRange = null;
        $minSuppliers = 0;
        $maxSuppliers = PHP_INT_MAX;
        $callbacks = [];
        $export = 0;

        // Récupération du formulaire des dates
        if ($form->isSubmitted() && $form->isValid()) {
            $startDateRange = $form->get('startDate')->getData();
            $endDateRange = $form->get('endDate')->getData();
            $minSuppliers = $form->get('minSuppliers')->getData();
            $maxSuppliers = $form->get('maxSuppliers')->getData();
            $callbacks = $form->get('callback')->getData();
            $export = (int)$form->get('export')->getData();
        }

        $redisCacheManager = $this->get('app.redis.cache.manager');

        $keys = $redisCacheManager->get(RedisCacheManager::CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_LIST, null, function() use($redisCacheManager) {
            $em = $this->getDoctrine()->getManager();
            // Récupération du repository Order & de Invoice
            /** @var OrderRepository $repoOrder */
            $repoOrder = $em->getRepository(Order::class);
            /** @var InvoiceRepository $repoInvoice */
            $repoInvoice = $em->getRepository(Invoice::class);
            /** @var ProspectSourceRepository $prospectSourceRepository */
            $prospectSourceRepository = $em->getRepository(ProspectSource::class);

            $prospectsThatHaveAlreadyOrdered = $repoOrder->getProspectsThatHaveAlreadyOrdered();
            $prospectsThatHaveAlreadyBeenInvoiced = $repoInvoice->getProspectsThatHaveAlreadyBeenInvoiced();
            /** @var Prospect[] $prospects */
            $prospects = array_unique(array_merge($prospectsThatHaveAlreadyOrdered, $prospectsThatHaveAlreadyBeenInvoiced));

            $keys = [];

            foreach ($prospects as $prospect) {
                $key = str_replace('{prospectId}', $prospect->getId(), RedisCacheManager::CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_ITEM);

                $data = $redisCacheManager->get(
                    $key,
                    null,
                    function () use($prospect, $repoOrder, $repoInvoice, $prospectSourceRepository) {
                        /** @var Supplier[] $realSuppliers */
                        $realSuppliers = [];

                        foreach ($prospect->getSuppliers() as $supplierCategory) {
                            $realSuppliers[$supplierCategory->getSupplier()->getId()] = $supplierCategory->getSupplier();
                        }

                        $rangesBySupplier = [];

                        // Maintenant, possible de boucler sur les suppliers
                        foreach($realSuppliers as $supplier) {
                            $datesOrder = $repoOrder->getOrderDateRange($supplier, $prospect);
                            $dateInvoice = $repoInvoice->getInvoiceDateRange($supplier, $prospect);
                            $range = $this->getRealRange($datesOrder, $dateInvoice);

                            $minType = $maxType = null;
                            $minId = $maxId = null;

                            if ($datesOrder['min'] !== null) {
                                if ($datesOrder['min'] === $range['min']) {
                                    $minType = 'order';
                                    $minId = $datesOrder['minId'];
                                }
                            }

                            if ($dateInvoice['min'] !== null) {
                                if ($dateInvoice['min'] === $range['min']) {
                                    $minType = 'invoice';
                                    $minId = $dateInvoice['minId'];
                                }
                            }

                            if ($datesOrder['max'] !== null) {
                                if ($datesOrder['max'] === $range['max']) {
                                    $maxType = 'order';
                                    $maxId = $datesOrder['maxId'];
                                }
                            }

                            if ($dateInvoice['max'] !== null) {
                                if ($dateInvoice['max'] === $range['max']) {
                                    $maxType = 'invoice';
                                    $maxId = $dateInvoice['maxId'];
                                }
                            }

                            $rangesBySupplier[] = [
                                'min' => $range['min'],
                                'max' => $range['max'],
                                'supplier' => $supplier->getDisplayName(true),
                                'minType' => $minType,
                                'maxType' => $maxType,
                                'minId' => $minId,
                                'maxId' => $maxId
                            ];
                        }

                        // On calcule les bornes générales min max (cross supplier)
                        $veryFirstOrderDate = $veryLastOrderDate = ['date' => null, 'supplier' => null];

                        foreach ($rangesBySupplier as $rangeBySupplier) {
                            if ($veryFirstOrderDate['date'] === null) {
                                $veryFirstOrderDate['date'] = $rangeBySupplier['min'];
                                $veryFirstOrderDate['supplier'] = $rangeBySupplier['supplier'];
                                $veryFirstOrderDate['type'] = $rangeBySupplier['minType'];
                                $veryFirstOrderDate['id'] = $rangeBySupplier['minId'];
                            } elseif ($rangeBySupplier['min'] !== null) {
                                if ($veryFirstOrderDate['date'] > $rangeBySupplier['min']) {
                                    $veryFirstOrderDate['date'] = $rangeBySupplier['min'];
                                    $veryFirstOrderDate['type'] = $rangeBySupplier['minType'];
                                    $veryFirstOrderDate['id'] = $rangeBySupplier['minId'];
                                    $veryFirstOrderDate['supplier'] = $rangeBySupplier['supplier'];
                                }
                            }

                            if ($veryLastOrderDate['date'] === null) {
                                $veryLastOrderDate['date'] = $rangeBySupplier['max'];
                                $veryLastOrderDate['supplier'] = $rangeBySupplier['supplier'];
                                $veryLastOrderDate['type'] = $rangeBySupplier['maxType'];
                                $veryLastOrderDate['id'] = $rangeBySupplier['maxId'];
                            } elseif ($rangeBySupplier['max'] !== null) {
                                if ($veryLastOrderDate['date'] < $rangeBySupplier['max']) {
                                    $veryLastOrderDate['date'] = $rangeBySupplier['max'];
                                    $veryLastOrderDate['type'] = $rangeBySupplier['maxType'];
                                    $veryLastOrderDate['id'] = $rangeBySupplier['maxId'];
                                    $veryLastOrderDate['supplier'] = $rangeBySupplier['supplier'];
                                }
                            }
                        }

                        if ($veryFirstOrderDate['date'] === null && $veryLastOrderDate['date'] === null) {
                            return null;
                        }

                        $prospectSources = $prospectSourceRepository->getSources($prospect);
                        $sources = [];

                        foreach ($prospectSources as $prospectSource) {
                            $sources[] = "{$prospectSource->getSource()} (le {$prospectSource->getCreatedAt()->format('d/m/Y H:i:s')})";
                        }

                        return [
                            'idProspect' => $prospect->getId(),
                            'idHubspot' => $prospect->getHubspotId(),
                            'source' => $prospect->getSource(),
                            'sources' => $sources,
                            'restaurant' => $prospect->getRestaurantName(),
                            'email' => $prospect->getUsername(),
                            'phone' => $prospect->getPhone(),
                            'mobile' => $prospect->getMobile(),
                            'zipCode' => $prospect->getZipCode(),
                            'nbTotalCommande' => $repoOrder->countByTotalOrderForCustomer($prospect),
                            'dataSuppliers' => $rangesBySupplier,
                            'nbSuppliers' => count($realSuppliers),
                            'globalRange' => [
                                'start' => $veryFirstOrderDate,
                                'end' => $veryLastOrderDate,
                            ],
                            'successFlagJ' => $prospect->isSuccessFlagJ(),
                            'successFlagJPlus7' => $prospect->isSuccessFlagJPlus7(),
                            'successFlagJPlus30' => $prospect->isSuccessFlagJPlus30(),
                        ];
                    }
                );

                if ($data === null) {
                    continue ;
                }

                $keys[] = $key;
            }

            return $keys;
        });

        $allData = [];

        foreach ($keys as $key) {
            $allData[] = $redisCacheManager->get($key);
        }

        // On va filter allData par rapport aux dates sélectionnées
        $filteredData = $this->filterByDateRange($allData, $startDateRange, $endDateRange);

        // Ensuite on va filter par rapport au nombre de fournisseurs
        $filteredData = $this->filterBySuppliersRange($filteredData, $minSuppliers, $maxSuppliers);

        // Et enfin on va filtrer par rapport aux statuts de callback
        $filteredData = $this->filterByCallbackStatus($filteredData, $callbacks);

        if ($export > 0) {
            return $this->export($filteredData);
        }

        return $this->render('@App/Admin/CustomerSucces/index.html.twig', [
            'allData' => $filteredData,
            'form' => $form->createView(),
        ]);
    }

    private function export($data)
    {
        $filename = 'Churn ' . (new DateTime())->format('d/m/Y H:i:s') . '.csv';

        return new StreamedResponse(function() use($data) {
            $stdout = fopen('php://memory', 'r+');

            fputcsv($stdout, ['Nom du restaurant', 'Email', 'Téléphone', 'Date 1ère commande', 'Date dernière commande', 'Nombre de fournisseurs', 'Nombre de cdes shop', 'Code postal', 'Source'], ';');

            foreach ($data as $datum) {
                fputcsv($stdout, [
                    $datum['restaurant'],
                    $datum['email'],
                    implode(' - ', array_filter([$datum['mobile'], $datum['phone']])),
                    "{$datum['globalRange']['start']['date']->format('d/m/Y H:i:s')} ({$datum['globalRange']['start']['supplier']})",
                    "{$datum['globalRange']['end']['date']->format('d/m/Y H:i:s')} ({$datum['globalRange']['end']['supplier']})",
                    $datum['nbSuppliers'],
                    $datum['nbTotalCommande'],
                    $datum['zipCode'],
                    implode("\n", array_filter([implode("\n", $datum['sources']), "{$datum['source']}"]))
                ], ';');
            }

            rewind($stdout);
            echo stream_get_contents($stdout);
        }, 200, ['Content-Type' => 'text/csv', 'Content-Disposition' => "attachment; filename=$filename"]);
    }

    public function toggleProspectAction(int $id, string $flag)
    {
        $em = $this->getDoctrine()->getManager();
        $prospect = $em->find(Prospect::class, $id);

        switch ($flag) {
            case 'j':
                $prospect->setSuccessFlagJ(!$prospect->isSuccessFlagJ());
                $flag = $prospect->isSuccessFlagJ();
                break ;
            case 'j7':
                $prospect->setSuccessFlagJPlus7(!$prospect->isSuccessFlagJPlus7());
                $flag = $prospect->isSuccessFlagJPlus7();
                break ;
            case 'j30':
                $prospect->setSuccessFlagJPlus30(!$prospect->isSuccessFlagJPlus30());
                $flag = $prospect->isSuccessFlagJPlus30();
                break ;
        }

        $em->flush();

        // Supprime le cache de la liste et de l'item concerné
        $this->get('app.redis.cache.manager')->deleteItems([
            RedisCacheManager::CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_LIST,
            str_replace('{prospectId}', $prospect->getId(), RedisCacheManager::CACHE_KEY_ADMIN_CUSTOMER_SUCCESS_ITEM)
        ]);

        return $this->json(['ok' => true, 'successFlag' => $flag]);
    }

    /**
     * @param array $data
     * @param array $callbacks
     * @return array
     */
    private function filterByCallbackStatus(array $data, array $callbacks)
    {
        return array_filter($data, function($datum) use($callbacks) {
            $shouldKeep = (int)false;

            foreach ($callbacks as $callback) {
                switch ($callback) {
                    case 'j':
                        $shouldKeep |= (int)$datum['successFlagJ'];
                        break ;
                    case 'j7':
                        $shouldKeep |= (int)$datum['successFlagJPlus7'];
                        break ;
                    case 'j30':
                        $shouldKeep |= (int)$datum['successFlagJPlus30'];
                        break ;
                }
            }

            return empty($callbacks) ? true : $shouldKeep;
        });
    }

    /**
     * @param array $data
     * @param int $minSuppliers
     * @param int $maxSuppliers
     * @return array
     */
    private function filterBySuppliersRange(array $data, int $minSuppliers, int $maxSuppliers)
    {
        return array_filter($data, function($datum) use($minSuppliers, $maxSuppliers) {
            return $datum['nbSuppliers'] >= $minSuppliers && $datum['nbSuppliers'] <= $maxSuppliers;
        });
    }

    /**
     * @param array $data
     * @param $startRange
     * @param $endRange
     * @return array
     */
    private function filterByDateRange(array $data, $startRange, $endRange)
    {
        /** @var DateTime|null $startDateMin */
        /** @var DateTime|null $startDateMax */
        /** @var DateTime|null $endDateMin */
        /** @var DateTime|null $endDateMax */
        $startDateMin = $startDateMax = $endDateMin = $endDateMax = null;

        if ($startRange !== null) {
            $startDateMin = $startRange['start'];
            $startDateMax = $startRange['end'];
        }

        if ($endRange !== null) {
            $endDateMin = $endRange['start'];
            $endDateMax = $endRange['end'];
        }

        $data = array_filter($data, function($datum) use($startDateMin, $startDateMax, $endDateMin, $endDateMax) {
            $range = $datum['globalRange'];

            if ($startDateMin !== null) {
                if ($range['start']['date'] < $startDateMin) {
                    return false;
                }
            }

            if ($startDateMax !== null) {
                if ($range['start']['date'] > $startDateMax) {
                    return false;
                }
            }

            if ($endDateMin !== null) {
                if ($range['end']['date'] < $endDateMin) {
                    return false;
                }
            }

            if ($endDateMax !== null) {
                if ($range['end']['date'] > $endDateMax) {
                    return false;
                }
            }

            return true;
        });

        return $data;
    }

    /**
     * @param array $orderRange
     * @param array $invoiceRange
     * @return array
     */
    private function getRealRange(array $orderRange, array $invoiceRange)
    {
        if ($orderRange['min'] !== null && $invoiceRange['min'] !== null) {
            $minValue = min($orderRange['min'], $invoiceRange['min']);
        } elseif ($orderRange['min'] !== null) {
            $minValue = $orderRange['min'];
        } else {
            $minValue = $invoiceRange['min'];
        }

        if ($orderRange['max'] !== null && $invoiceRange['max'] !== null) {
            $maxValue = max($orderRange['max'], $invoiceRange['max']);
        } elseif ($orderRange['max'] !== null) {
            $maxValue = $orderRange['max'];
        } else {
            $maxValue = $invoiceRange['max'];
        }

        return [
            'min' => $minValue,
            'max' => $maxValue
        ];
    }

    /**
     * @return FormInterface
     */
    private function getForm()
    {
        return $this->createFormBuilder()
            ->add('startDate', DateRangeType::class, [
                'required' => false,
                'label' => 'Date de la première commande',
                'field_options' => [
                    'widget' => 'single_text'
                ],
                'attr' => [
                    'class' => 'two fields'
                ]
            ])
            ->add('endDate', DateRangeType::class, [
                'required' => false,
                'label' => 'Date de la dernière commande',
                'field_options' => [
                    'widget' => 'single_text'
                ],
                'attr' => [
                    'class' => 'two fields'
                ]
            ])
            ->add('minSuppliers', IntegerType::class, [
                'required' => false,
                'label' => 'Nombre de fournisseurs min',
                'empty_data' => '1',
                'attr' => [
                    'placeholder' => '1'
                ]
            ])
            ->add('maxSuppliers', IntegerType::class, [
                'required' => false,
                'label' => 'Nombre de fournisseurs max',
                'empty_data' => '100',
                'attr' => [
                    'placeholder' => '100'
                ]
            ])
            ->add('callback', ChoiceType::class, [
                'required' => false,
                'label' => 'Voir les',
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'J' => 'j',
                    'J+7' => 'j7',
                    'J+30' => 'j30'
                ]
            ])
            ->add('export', HiddenType::class, ['empty_data' => 0, 'attr' => ['class' => 'export-csv']])

            ->getForm();
    }

}