<?php

namespace AppBundle\Admin\Controller;

use AppBundle\DataTransformer\ProductArrayToShoppingItemsTransformer;
use AppBundle\Entity\Onboarding;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Services\Suggestions\Suggestion;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OnboardingController extends ResourceController
{
    public function showAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var Onboarding $resource */
        $resource = $this->findOr404($configuration);

        $this->eventDispatcher->dispatch(ResourceActions::SHOW, $configuration, $resource);

        /** @var EntityRepository $supplierProductVariantRepository */
        $supplierProductVariantRepository = $this->getDoctrine()->getRepository(SupplierProductVariantInformations::class);

        $products = $resource->getProducts();
        $productsDisplay = [];
        $userTotal = 0.0;

        foreach ($products as &$product) {
            /** @var SupplierProductVariantInformations $info */
            try {
                $info = $supplierProductVariantRepository
                    ->createQueryBuilder('s')
                    ->where('s.productVariant = :v')
                    ->setParameter('v', $product['variantId'])
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getSingleResult();

                $product['name'] = $info->getDisplayName();

                $productsDisplay[] = [
                    'name' => $product['name'],
                    'quantity' => $product['quantity'],
                    'quantityUnit' => $product['quantityUnit'],
                    'price' => $product['price'],
                    'priceUnit' => $product['priceUnit'],
                    'total' => bcmul($product['price'], $product['quantity'], 2)
                ];

                $userTotal += bcmul($product['quantity'], $product['price'], 2);
            } catch (NoResultException $e) {
            } catch (NonUniqueResultException $e) {
            }
        }

        $supplier = new Supplier();

        $supplier->setName('tmp');
        $supplier->setCategories([$resource->getCategory()]);

        $shoppingCart = new ShoppingCart();
        $shoppingCart->setSupplier($supplier);

        /** @var ProductArrayToShoppingItemsTransformer $transformer */
        $transformer = $this->get('app.transformer.product_array_to_shopping_items');
        $items = $transformer->transform($products);

        $shoppingCart->setItems($items);

        $benchmarkService = $this->container->get('app.services.benchmark');
        $benchmark = $benchmarkService->getBenchmark($shoppingCart->getItems(), floatval($userTotal));

        $benchmarkDisplay = [];

        foreach ($products as &$product) {
            $variantId = $product['variantId'];
            $benchmarkProduct = [
                'product' => [
                    'id' => $variantId,
                    'name' => $product['name']
                ],
                'restaurants' => []
            ];

            foreach ($benchmark['trace'] as $traceName => $trace) {
                if ($trace[$variantId] === null)
                    continue ;

                preg_match('/Restaurant (?P<restaurantName>[\S\s]+) Fournisseur (?P<supplierName>[\S\s]+)/i', $traceName, $match);

                $restaurantName = $match['restaurantName'];
                $supplierName = $match['supplierName'];

                $replacementVariantId = $trace[$variantId]['replacement']['id'];
                $replacementVariantName = $trace[$variantId]['replacement']['name'];

                try {
                    $info = $supplierProductVariantRepository
                        ->createQueryBuilder('s')
                        ->where('s.productVariant = :v')
                        ->setParameter('v', $replacementVariantId)
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getSingleResult();

                    $replacementVariantName = $info->getDisplayName();
                } catch (NoResultException $e) {
                } catch (NonUniqueResultException $e) {
                }

                /** @var Supplier $replacementSupplier */
                $replacementSupplier = $trace[$variantId]['replacement']['supplier'];

                $price = floatval($trace[$variantId]['replacement']['price']);
                $unitPrice = bcdiv($trace[$variantId]['replacement']['price'], $product['quantity'], 2);

                $isCheaper = $price <= $trace[$variantId]['original']['price'];

                $benchmarkProduct['restaurants'][$restaurantName]['suppliers'][] = [
                    'name' => $supplierName,
                    'price' => $price,
                    'unitPrice' => $unitPrice . '€/' . $trace[$variantId]['replacement']['unit'],
                    'productName' => $replacementVariantName,
                    'productId' => $replacementVariantId,
                    'isPartnerSupplier' => $replacementSupplier->getEnabled() === false,
                    'isCheaper' => $isCheaper
                ];
            }

            $benchmarkDisplay[] = $benchmarkProduct;
        }

        $restaurant = $this->getDoctrine()->getManager()->find(Restaurant::class, 4);

        $suggestionsService = $this->get('app.services.suggestions');
        /** @var Suggestion[]|ArrayCollection $suggestions */
        $suggestions = $suggestionsService->getSuggestions($shoppingCart->getItems(), $restaurant);

        $suggestionsDisplay = [];

        if ($userTotal == 0)
            $userTotal = 1;

        foreach ($suggestions as $suggestion) {
            $supplierTotal = bcdiv($suggestion->getAmount(), 100, 2);
            $delta = (($supplierTotal - $userTotal) * 100) / $userTotal;

            $suggestionsDisplay[] = [
                'supplierName' => $suggestion->getSupplier()->getName(),
                'total' => $supplierTotal,
                'delta' => number_format($delta, 2, ',', ' ')
            ];
        }

        return $this->render('@App/Admin/Onboarding/show.html.twig', [
            'category' => $resource->getCategory(),
            'turnover' => $resource->getTurnover(),
            'createdAt' => $resource->getCreatedAt(),
            'products' => $productsDisplay,
            'benchmark' => $benchmarkDisplay,
            'suggestions' => $suggestionsDisplay,
            'onboardingId' => $resource->getId(),
            'userTotal' => $userTotal,
            'onboarding' => $resource
        ]);
    }
}