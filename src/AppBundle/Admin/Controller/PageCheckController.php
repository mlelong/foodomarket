<?php

namespace AppBundle\Admin\Controller;

use AppBundle\DataTransformer\PricesToChartJsDataTransformer;
use AppBundle\Entity\CatalogChangeLog;
use AppBundle\Entity\ChannelPricing;
use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductPriceHistory;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Form\Type\CheckProductVariantType;
use AppBundle\Form\Type\ProductImageType;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use Cocur\Slugify\Slugify;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductImage;
use Sylius\Component\Core\Model\ProductImageInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductTaxon;
use Sylius\Component\Product\Model\ProductOption;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Product\Model\ProductVariantTranslationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class PageCheckController extends Controller
{
    public function indexAction(): Response
    {
        $prospectRepository = $this->get('app.repository.supplier');
        $partners = $prospectRepository->getPartners(true);
        $suppliers = [];

        foreach ($partners as $partner) {
            $suppliers[] = [
                'id' => $partner->getId(),
                'name' => $partner->getDisplayName(true)
            ];
        }

        usort($suppliers, function($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });

        return $this->render('AppBundle:Admin/PageCheck:index.html.twig', [
            'filters' => [
                'suppliers' => $suppliers,
            ]
        ]);
    }

    public function toggleFilterAction(Request $request)
    {
        $filterName = $request->attributes->get('filterName');
        $filterValue = $request->get('filterValue');
        $session = $this->get('session');
        $filters = $session->get('filters', [
            'suppliers' => [],
            'options' => []
        ]);

        $filters[$filterName] = $filterValue;

        $session->set('filters', $filters);

        return $this->json(['ok' => true]);
    }

    public function logchange($status, $taxon, $product, $productVariant, $description, $message)
    {
        $log = new CatalogChangeLog();
        $log->setStatus($status)
            ->setTaxon($taxon)
            ->setProduct($product)
            ->setProductVariant($productVariant)
            ->setDescription($description)
            ->setMessage($message)
        ;

        try {
            $this->get('doctrine.orm.entity_manager')->persist($log);
        } catch (ORMException $e) {
            return false;
        }

        return true;
    }

    public function treeLeafAction(Request $request): Response
    {
        $type = $request->get('type');
        $id = $request->get('id');

        $data = $this->getNodeData($type, $id);

        return new JsonResponse($data);
    }

    public function treeLeavesAction(Request $request): Response
    {
        $ids = explode(',', $request->get('ids'));
        $data = [];

        foreach ($ids as $id) {
            $data[$id] = $this->getNodeData($id[0], substr($id, 1));
        }

        return new JsonResponse($data);
    }

    private function getNodeData($type, $id)
    {
        if ($id == '#') {
            return [['id' => 't1', 'entityId' => 1, 'text' => 'Catalogue', 'type' => 't', 'children' => true]];
        }

        /** @var TaxonRepository $taxonRepository */
        $taxonRepository = $this->get('sylius.repository.taxon');
        /** @var ProductRepository $productRepository */
        $productRepository = $this->get('sylius.repository.product');
        /** @var ProductVariantRepository $variantRepository */
        $variantRepository = $this->get('sylius.repository.product_variant');
//        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
//        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        /** @var SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository */
        $supplierProductVariantInformationsRepository = $this->get('app.repository.supplier_product_variant_informations');

        $data = [];

//        $filters = $this->get('session')->get('filters', ['suppliers' => [], 'options' => []]);

        switch ($type) {
            case 'p':
                /** @var ProductVariant[] $variants */
                $variants = [];
                $variants = $variantRepository->findBy(['product' => $id]);
                // TODO: put this again
//                $spvis = $supplierProductVariantInformationsRepository->findBy(['product' => $id, 'supplier' => $filters['suppliers']]);

//                foreach ($spvis as $spvi) {
//                    $variants[$spvi->getProductVariant()->getId()] = $spvi->getProductVariant();
//                }

                foreach ($variants as $variant) {
                    $options = [];
                    /** @var ProductOptionValueInterface[] $optionValues */
                    $optionValues = $variant->getOptionValues();

                    foreach ($optionValues as $optionValue) {
                        $options[$optionValue->getOption()->getCode()] = [
                            'option' => $optionValue->getOption()->getName(),
                            'value' => $optionValue->getValue()
                        ];
                    }

                    $hasDuplicate = false;

                    foreach ($variants as $variant2) {
                        if ($variant2->getId() == $variant->getId()) {
                            continue ;
                        }

                        if ($variant->equals($variant2)) {
                            $hasDuplicate = true;
                            break ;
                        }
                    }

                    $class = $variant->isEnabled() ? 'variant-enabled' : 'variant-disabled';

                    if ($hasDuplicate)
                        $class .= ' variant-duplicated';

                    //$prices = $supplierProductPriceRepository->getPrices(null, [$variant->getProduct()], [$variant], null, false);
                    $variantTooltip = [];

                    /** @var SupplierProductVariantInformations $spvi */
                    $spvi = $supplierProductVariantInformationsRepository->findBy([
                        'product' => $variant->getProduct(),
                        'productVariant' => $variant
                    ]);

                    /** @var SupplierProductVariantInformations $informations */
                    foreach ($spvi as $informations) {

                        $variantTooltip[] = [
                            'supplierName' => $informations->getSupplier()->getName(),
                            'supplierProductName' => $informations->getProductLabel(),
                            'reference' => $informations->getReference()
                        ];
                    }

                    $variantTooltip = array_reduce($variantTooltip, function($carry, $current) {
                        $tooltip = "<li class='item'>{$current['supplierName']} : {$current['supplierProductName']} ({$current['reference']})</li>";

                        return "{$carry}{$tooltip}";
                    }, '');

                    $data[] = [
                        'id' => 'v' . $variant->getId(),
                        'entityId' => $variant->getId(),
                        'text' => $variant->getName(),
                        'type' => 'v',
                        'children' => false,
                        'optionValues' => $options,
                        'weight' => $variant->getWeight(),
                        'enabled' => $variant->isEnabled(),
                        'li_attr' => ['class' => $class, 'data-html' => empty($variantTooltip) ? 'Pas de fournisseur / Pas de prix' : "<ul class='ui list'>$variantTooltip</ul>"]
                    ];
                }
                break ;
            case 'v':
                break ;
            case 't':
            default:
                $taxon = $taxonRepository->find($id);
                $taxons = $taxonRepository->findBy(['parent' => $taxon], ['code' => 'ASC']);
                $products = $productRepository->findMatchingNotCustom($taxon->getCode());

                /** @var Taxon $taxon */
                foreach ($taxons as $taxon) {
                    $hasChildren = $taxon->hasChildren();

                    if (!$hasChildren) {
                        $hasChildren = !empty($productRepository->findMatching($taxon->getCode()));
                    }

                    $data[] = [
                        'id' => 't' . $taxon->getId(),
                        'entityId' => $taxon->getId(),
                        'text' => $taxon->getName(),
                        'type' => 't',
                        'children' => $hasChildren
                    ];
                }

                if (!empty($products)) {
                    /** @var CacheManager */
                    $imagineCacheManager = $this->get('liip_imagine.cache.manager');

                    /** @var Product $product */
                    foreach ($products as $product) {
                        // TODO: put this filter again
//                        if ($supplierProductVariantInformationsRepository->count(['product' => $product, 'supplier' => $filters['suppliers']]) === 0) {
//                            continue ;
//                        }

                        $nodeData = [
                            'id' => 'p' . $product->getId(),
                            'entityId' => $product->getId(),
                            'text' => $product->getName(),
                            'type' => 'p',
                            'children' => !empty($variantRepository->findBy(['product' => $product])),
                            'enabled' => $product->isEnabled(),
                            'li_attr' => ['class' => $product->isEnabled() ? 'product-enabled' : 'product-disabled']
                        ];

                        if ($product->hasImages()) {
                            /** @var ProductImageInterface $image */
                            $image = $product->getImages()[0];
                            $picturePath = $imagineCacheManager->getBrowserPath($image->getPath(), 'front_tiny');
                            $nodeData['picture'] = $picturePath;
                        }

                        $data[] = $nodeData;
                    }
                }

                break ;
        }

        return $data;
    }

    public function treeSearchAction(Request $request)
    {
        $terms = $request->get('str');

        $taxonRepository = $this->get('sylius.repository.taxon');
        $productRepository = $this->get('sylius.repository.product');
        $variantRepository = $this->get('sylius.repository.product_variant');

        /** @var Taxon[] $taxons */
        $taxons = $taxonRepository->findByNamePart($terms, 'fr_FR');
        /** @var Product[] $products */
        $products = $productRepository->findByNamePart($terms, 'fr_FR');
        /** @var ProductVariant[] $variants */
        $variants = $variantRepository->findByPhraseOrReference($terms, 'fr_FR');

        $data = [];

        foreach ($taxons as $taxon) {
            $data[] = 't' . $taxon->getId();
            $data = array_merge($data, $taxon->getAncestors()->map(function(Taxon $taxon) {
                return 't' . $taxon->getId();
            })->toArray());
        }

        foreach ($products as $product) {
            $data[] = 'p' . $product->getId();
            $taxon = $product->getMainTaxon();

            if ($taxon === null)
                continue ;

            $data[] = 't' . $taxon->getId();
            $data = array_merge($data, $taxon->getAncestors()->map(function(Taxon $taxon) {
                return 't' . $taxon->getId();
            })->toArray());
        }

        foreach ($variants as $variant) {
            /** @var Product $product */
            $product = $variant->getProduct();
            $data[] = 'p' . $product->getId();
            $taxon = $product->getMainTaxon();

            if ($taxon === null)
                continue ;

            $data[] = 't' . $taxon->getId();
            $data = array_merge($data, $taxon->getAncestors()->map(function(Taxon $taxon) {
                return 't' . $taxon->getId();
            })->toArray());
        }

        $data = array_values(array_unique($data));

        return new JsonResponse($data);
    }

    private function updatePricesByWeight(ProductVariant $variant, $oldWeight)
    {
        $ratio = bcdiv($oldWeight, $variant->getWeight(), 2);

        if ($ratio == 1)
            return true;

        $saleUnit = $variant->getOptionSupplierSalesUnit()->getCode();

        if ($saleUnit == 'KG') {
            return true;
        }

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($variant) {
                $spp = $manager->getRepository(SupplierProductPrice::class)->findBy(['productVariant' => $variant]);
                $spph = $manager->getRepository(SupplierProductPriceHistory::class)->findBy(['productVariant' => $variant]);
                /** @var SupplierProductPrice[]|SupplierProductPriceHistory[] $prices */
                $prices = array_merge($spp, $spph);

                foreach ($prices as $price) {
                    $price->setKgPrice(bcdiv($price->getUnitPrice(), $variant->getWeight()));

                    if ($price instanceof SupplierProductPrice) {
                        /** @noinspection PhpParamsInspection */
                        $this->insertOrUpdateChannelPricing($variant, $price->getRestaurant(), $price->getSupplier(), $price->getKgPrice(), $price->getUnitPrice());
                    }
                }

                return true;
            });
        } catch (Throwable $e) {
            return false;
        }
    }

    public function productEditAction(Request $request)
    {
        $productId = $request->get('id');
        /** @var Product $product */
        $product = $this->get('sylius.repository.product')->find($productId);
        /** @var ProductImage[]|ArrayCollection $images */
        $images = $product->getImages();
        /** @var ProductImage $image */
        $image = null;

        if (!$images->isEmpty()) {
            $image = $images->first();
        }

        $form = $this->createFormBuilder(null, ['action' => $this->get('router')->generate('app_admin_page_check_product_edit', ['id' => $productId])])
            ->add('productImage', ProductImageType::class, [
                'product' => $product,
            ])
            ->add('remove', CheckboxType::class, ['label' => 'Supprimer', 'required' => false])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $remove = $form->get('remove')->getData();
            $manager = $this->getDoctrine()->getManager();

            if ($remove) {
                foreach ($product->getImages() as $productImage) {
                    $product->removeImage($productImage);
                }

                $manager->flush();
            } else {
                $image = $form->get('productImage')->getData();
                $image->setOwner($product);

                $this->get('sylius.image_uploader')->upload($image);

                $manager->persist($image);
                $manager->flush();
            }
        }

        /** @var CacheManager */
        $imagineCacheManager = $this->get('liip_imagine.cache.manager');

        $picture = $imagineCacheManager->getBrowserPath('shop-default-product.jpg', 'front_big');

        if ($image !== null) {
            $picture = $imagineCacheManager->getBrowserPath($image->getPath(), 'front_big');
        }

        /** @noinspection PhpParamsInspection */
        $taxonTree = $this->getTaxonTree(
            $this->container->get('sylius.repository.taxon')->find(1),
            $product
        );

        return $this->render('AppBundle:Admin/PageCheck:_product_edit.html.twig', [
            'form' => $form->createView(),
            'picture' => $picture,
            'taxonTree' => json_encode($taxonTree)
        ]);
    }

    public function updateProductTaxonsAction(Request $request)
    {
        /** @var Product $product */
        $product = $this->get('sylius.repository.product')->find($request->get('id'));
        /** @var Taxon[] $taxons */
        $taxons = $this->get('sylius.repository.taxon')->findBy(['id' => $request->get('taxons')]);
        $productTaxonFactory = $this->get('sylius.factory.product_taxon');
        $productTaxonRepository = $this->get('sylius.repository.product_taxon');
        $productTaxons = $productTaxonRepository->findBy(['product' => $product]);

        foreach ($productTaxons as $productTaxon) {
            $productTaxonRepository->remove($productTaxon);
        }

        foreach ($taxons as $taxon) {
            /** @var ProductTaxon $productTaxon */
            $productTaxon = $productTaxonFactory->createNew();

            $productTaxon->setProduct($product);
            $productTaxon->setTaxon($taxon);
            $productTaxon->setPosition(0);

            $productTaxonRepository->add($productTaxon);
        }

        return new JsonResponse(['ok' => true]);
    }

    /**
     * @param Taxon $taxon
     * @param Product $product
     * @return array
     */
    private function getTaxonTree(Taxon $taxon, Product $product)
    {
        $mainTaxon = $product->getMainTaxon();

        $isChecked = array_reduce($product->getTaxons()->toArray(), function($carry, Taxon $productTaxon) use($taxon) {
            return $carry ?: $productTaxon->getId() === $taxon->getId();
        }, false);

        $isDisabled = $taxon->getId() === $mainTaxon->getId() ?: array_reduce(
            $mainTaxon->getAncestors()->toArray(),
            function($carry, Taxon $productTaxon) use($taxon) {
                return $carry ?: $productTaxon->getId() === $taxon->getId();
            },
            false
        );

        return [
            'id' => $taxon->getId(),
            'text' => $taxon->getName(),
            'state' => [
                'opened' => $isChecked,
                'disabled' => $isDisabled,
                'selected' => false,
                'checked' => $isChecked
            ],
            'children' => array_map(function(Taxon $child) use($product) {
                return $this->getTaxonTree($child, $product);
            }, $taxon->getChildren()->toArray())
        ];
    }

    public function variantEditAction(Request $request)
    {
        $variantId = $request->get('id');
        /** @var ProductVariant $variant */
        $variant = $this->get('sylius.repository.product_variant')->find($variantId);
        $formErrors = [];

        $weight = $variant->getWeight();

        $form = $this->createForm(CheckProductVariantType::class, $variant);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($weight != $variant->getWeight()) {
                    $this->logchange(true, null, $variant->getProduct(), $variant, "change le poids de {$weight}kg à {$variant->getWeight()}kg", "");
                }

                $this->getDoctrine()->getManager()->flush();
                $this->updatePricesByWeight($variant, $weight);
            } else {
                $errors = $form->getErrors(true);

                foreach ($errors as $error) {
                    $formErrors[] = $error->getMessage();
                }
            }
        }

        $additionalInfos = $this->getAdditionalEditData($variant);

        $restaurants = $this->getDoctrine()->getRepository(Restaurant::class)->findBy([], ['name' => 'ASC']);
        $suppliers = $this->getDoctrine()->getRepository(Supplier::class)->findBy([], ['name' => 'ASC']);

        $pairedSuppliers = [];
        $unpairedSuppliers = [];

        foreach ($suppliers as $supplier) {
            foreach ($additionalInfos['suppliers'] as $pairedSupplier) {
                /** @noinspection PhpUndefinedMethodInspection */
                if ($pairedSupplier['supplier']->getId() == $supplier->getId()) {
                    $pairedSuppliers[] = $supplier;

                    continue 2;
                }
            }

            $unpairedSuppliers[] = $supplier;
        }

        $pairedRestaurants = [];

        foreach ($additionalInfos['restaurants'] as $pairedRestaurant) {
            $pairedRestaurants[] = $pairedRestaurant['restaurant'];
        }

        $activePrices = [];

        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');

        /** @var Supplier $pairedSupplier */
        foreach ($pairedSuppliers as $pairedSupplier) {
            if (null === ($price = $supplierProductPriceRepository->findOneBy(['supplier' => $pairedSupplier, 'productVariant' => $variant]))) {
                continue ;
            }

            $activePrices[] = [
                'supplier' => $pairedSupplier->getName(),
                'kgPrice' => $price->getKgPrice(),
                'unitPrice' => $price->getUnitPrice()
            ];
        }

        return $this->render('AppBundle:Admin/PageCheck:_variant_edit.html.twig', [
            'variant_id' => $variantId,
            'product_id' => $variant->getProduct()->getId(),
            'form' => $form->createView(),
            'errors' => $formErrors,
            'suppliers' => $additionalInfos['suppliers'],
            'restaurants' => $additionalInfos['restaurants'],
            'history' => json_encode($additionalInfos['history']),
            'restaurants_select' => $restaurants,
            'suppliers_select' => $suppliers,
            'unpaired_suppliers' => $suppliers,
            'paired_suppliers' => $pairedSuppliers,
            'paired_restaurants' => $pairedRestaurants,
            'active_prices' => json_encode($activePrices)
        ]);
    }

    private function getAdditionalEditData(ProductVariant $variant)
    {
        $ret = [
            'suppliers' => [],
            'restaurants' => [],
            'history' => []
        ];

        /** @var SupplierProductVariantInformations[] $infos */
        $infos = $this
            ->getDoctrine()
            ->getRepository(SupplierProductVariantInformations::class)
            ->findBy(['productVariant' => $variant, 'manuallyCreated' => false])
        ;

        $priceRepository = $this->getDoctrine()->getRepository(SupplierProductPrice::class);
        $priceHistoryRepository = $this->getDoctrine()->getRepository(SupplierProductPriceHistory::class);

        foreach ($infos as $info) {
            $supplier = $info->getSupplier();
            $supplierLabel = $info->getProductLabel();

            $ret['suppliers'][$info->getId()] = [
                'id'            => $info->getId(),
                'supplier'      => $supplier,
                'reference'     => $info->getReference(),
                'orderRef'      => $info->getOrderRef(),
                'label'         => $supplierLabel,
                'displayLabel'  => $info->getDisplayName(),
                'weight'        => $info->getWeight(),
                'unitQuantity'  => $info->getUnitQuantity(),
                'salesUnit'     => $info->getSalesUnit(),
            ];

            $pricesForSupplierAndVariant = $priceRepository->findBy(['supplier' => $supplier, 'productVariant' => $variant]);

            foreach ($pricesForSupplierAndVariant as $price) {
                /** @var Restaurant $restaurant */
                $restaurant = $price->getRestaurant();

                $ret['restaurants'][$restaurant->getId()]['restaurant'] = $restaurant;
                $ret['restaurants'][$restaurant->getId()]['suppliers'][$supplier->getId()] = [
                    'supplier' => $supplier,
                    'price' => $price
                ];
            }

//            $pricesHistoryForSupplierAndVariant = $priceHistoryRepository->findBy(['supplier' => $supplier, 'productVariant' => $variant]);
//
//            foreach ($pricesHistoryForSupplierAndVariant as $price) {
//                /** @var Restaurant $restaurant */
//                $restaurant = $price->getRestaurant();
//
//                $ret['restaurants'][$restaurant->getId()]['restaurant'] = $restaurant;
//
//                if (!isset($ret['restaurants'][$restaurant->getId()]['suppliers'][$supplier->getId()])) {
//                    $ret['restaurants'][$restaurant->getId()]['suppliers'][$supplier->getId()] = [
//                        'supplier' => $supplier,
//                        'price' => $price
//                    ];
//                }
//            }
        }

        $dataHelper = new PricesToChartJsDataTransformer();

        foreach ($ret['restaurants'] as $restaurantSuppliers) {
            foreach ($restaurantSuppliers['suppliers'] as $supplierPrice) {
                $prices = $priceHistoryRepository->findBy(
                    [
                        'restaurant' => $restaurantSuppliers['restaurant'],
                        'supplier' => $supplierPrice['supplier'],
                        'productVariant' => $variant
                    ],
                    [
                        'createdAt' => 'ASC'
                    ]
                );

                $dataHelper->addAll($prices);
                $dataHelper->add($supplierPrice['price']);
            }
        }

        $ret['history'] = $dataHelper->getChartData();

        return $ret;
    }

    public function moveProductAction(Request $request)
    {
        $productId = $request->get('productId');
        $oldTaxonId = $request->get('oldTaxonId');
        $newTaxonId = $request->get('newTaxonId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($productId, $oldTaxonId, $newTaxonId) {
                $product = $manager->find(Product::class, $productId);
                $currentTaxon = $product->getMainTaxon();

                if ($currentTaxon === null || $currentTaxon->getId() != $oldTaxonId) {
                    return new JsonResponse(['ok' => false, 'message' => "L'ancien taxon de ce produit n'est plus le même ! Tapez F5 avant de refaire le changement ;)"]);
                }

                $newTaxon = $manager->find(Taxon::class, $newTaxonId);

                $product->setMainTaxon($newTaxon);

                $this->rebuildProductTaxonsForProduct($product, $manager);

                $this->logchange(
                    true,
                    $newTaxon,
                    $product,
                    null,
                    "déplace le produit du taxon {$currentTaxon->getName()} au {$newTaxon->getName()}",
                    "MP: P({$productId}) from T({$oldTaxonId} to T({$newTaxonId}))"
                );

                return new JsonResponse(['ok' => true, 'id' => $productId, 'oldTaxonId' => $oldTaxonId, 'newTaxonId' => $newTaxonId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function moveTaxonAction(Request $request)
    {
        $taxonId = $request->get('taxonId');
        $oldTaxonId = $request->get('oldTaxonId');
        $newTaxonId = $request->get('newTaxonId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($taxonId, $oldTaxonId, $newTaxonId) {
                $taxon = $manager->find(Taxon::class, $taxonId);
                $currentTaxon = $taxon->getParent();

                if ($oldTaxonId != $currentTaxon->getId()) {
                    return new JsonResponse(['ok' => false, 'message' => "L'ancien taxon de ce taxon n'est pas le même ! Tapez F5 d'abord ;)"]);
                }

                $newTaxon = $manager->find(Taxon::class, $newTaxonId);

                $taxon->setParent($newTaxon);

                $manager->flush();

                $products = $this->get('sylius.repository.product')->createQueryBuilder('p')
                    ->join(ProductTaxon::class, 'pt', 'WITH', 'pt.product = p AND pt.taxon = :taxon')
                    ->setParameter('taxon', $taxon)
                    ->getQuery()
                    ->getResult()
                ;

                foreach ($products as $product) {
                    $this->rebuildProductTaxonsForProduct($product, $manager);
                }

                $this->logchange(
                    true,
                    $taxon,
                    null,
                    null,
                    "déplace le taxon `{$taxon->getName()}` de `{$currentTaxon->getName()}` vers `{$newTaxon->getName()}`",
                    "MT: T({$taxonId} from T({$oldTaxonId}) to T({$newTaxonId}))"
                );

                return new JsonResponse(['ok' => true, 'id' => $taxonId, 'oldTaxonId' => $oldTaxonId, 'newTaxonId' => $newTaxonId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e]);
        }
    }

    /**
     * @param Product $product
     * @param EntityManager $manager
     * @throws ORMException
     */
    private function rebuildProductTaxonsForProduct(Product $product, EntityManager $manager)
    {
        $productTaxons = $this->get('sylius.repository.product_taxon')->findBy(['product' => $product]);
        $productTaxonFactory = $this->get('sylius.factory.product_taxon');

        foreach ($productTaxons as $productTaxon) {
            $manager->remove($productTaxon);
        }

        $manager->flush();

        $taxon = $product->getMainTaxon();

        while ($taxon !== null) {
            /** @var ProductTaxon $productTaxon */
            $productTaxon = $productTaxonFactory->createNew();

            $productTaxon->setProduct($product);
            $productTaxon->setTaxon($taxon);
            $productTaxon->setPosition(0);

            $manager->persist($productTaxon);

            $taxon = $taxon->getParent();
        }

        $manager->flush();
    }

    public function renameTaxonAction(Request $request)
    {
        $taxonId = $request->get('taxonId');
        $taxonName = $request->get('taxonName');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($taxonId, $taxonName) {
                $taxon = $manager->find(Taxon::class, $taxonId);
                $slugifier = new Slugify();

                $taxon->setCurrentLocale('fr_FR');
                $oldName = $taxon->getName();
                $taxon->setName($taxonName);
                $taxon->setSlug($slugifier->slugify($taxonName));

                $this->logchange(true, $taxon, null, null, "renomme le taxon `$oldName` en `$taxonName`", "R: T({$taxonId})");

                return new JsonResponse(['ok' => true, 'id' => $taxonId, 'name' => $taxonName]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function renameProductAction(Request $request)
    {
        $productId = $request->get('productId');
        $productName = $request->get('productName');

        $productRepository = $this->get('sylius.repository.product');
        $productManager = $this->get('sylius.manager.product');

        $product = $productRepository->find($productId);

        $product->setCurrentLocale('fr_FR');
        $oldName = $product->getName();
        $product->setName($productName);
        $slugifier = new Slugify();
        $product->setSlug($slugifier->slugify($productName));

        $this->logchange(true, $product->getMainTaxon(), $product, null, "renomme le produit de `$oldName` en `$productName`", '');

        try {
            $productManager->flush();
        } catch (OptimisticLockException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        } catch (ORMException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['ok' => true, 'id' => $productId, 'name' => $productName]);
    }

    public function renameVariantAction(Request $request)
    {
        $variantId = $request->get('variantId');
        $variantName = $request->get('variantName');
        if (strpos($variantName, '|')) {
            $variantName = trim(substr($variantName, 0, strpos($variantName, '|')-1));
        }
        if (strpos($variantName, '<')) {
            $variantName = trim(substr($variantName, 0, strpos($variantName, '<')-1));
        }
        if (strrpos($variantName, '(')) {
            $variantName = trim(substr($variantName, 0, strrpos($variantName, '(')-1));
        }

        $variantRepository = $this->get('app.repository.variant');
        $variantManager = $this->get('app.manager.variant');

        $variant = $variantRepository->find($variantId);

        $variant->setCurrentLocale('fr_FR');
        $oldName = $variant->getName();
        $variant->setName($variantName);

        $this->logchange(true, $variant->getProduct()->getMainTaxon(), $variant->getProduct(), $variant, "renomme la variante de `$oldName` en `$variantName`", '');

        try {
            $variantManager->flush();
        } catch (OptimisticLockException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        } catch (ORMException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['ok' => true, 'id' => $variantId, 'name' => $variantName]);
    }

    public function removeProductAction(Request $request)
    {
        $productId = $request->get('productId');
        $manager = $this->getDoctrine()->getManager();
        $product = $manager->find(Product::class, $productId);

        if ($product->hasVariants()) {
            return new JsonResponse(['ok' => false, 'id' => $productId]);
        }

        $manager->remove($product);
        $this->logchange(true, null, null, null, "supprime le produit `$productId`", "D: P($productId)");

        $manager->flush();

        return new JsonResponse(['ok' => true, 'id' => $productId]);
    }

    public function disableProductAction(Request $request)
    {
        $productId = $request->get('productId');

        $productRepository = $this->get('sylius.repository.product');
        $productManager = $this->get('sylius.manager.product');

        $product = $productRepository->find($productId);

        $product->setCurrentLocale('fr_FR');
        $oldName = $product->getName();

        $product->setEnabled(false);

        $this->logchange(true, $product->getMainTaxon(), $product, null, "désactive le produit `$oldName`", '');

        try {
            $productManager->flush();
        } catch (OptimisticLockException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        } catch (ORMException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['ok' => true, 'id' => $productId]);
    }

    public function enableProductAction(Request $request)
    {
        $productId = $request->get('productId');

        $productRepository = $this->get('sylius.repository.product');
        $productManager = $this->get('sylius.manager.product');

        $product = $productRepository->find($productId);

        $product->setCurrentLocale('fr_FR');
        $oldName = $product->getName();

        $product->setEnabled(true);

        $this->logchange(true, $product->getMainTaxon(), $product, null, "active le produit `$oldName`", '');

        try {
            $productManager->flush();
        } catch (OptimisticLockException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        } catch (ORMException $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['ok' => true, 'id' => $productId]);
    }

    public function enableVariantAction(Request $request)
    {
        $variantId = $request->get('variantId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($variantId) {
                $variant = $manager->find(ProductVariant::class, $variantId);

                if ($variant === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Could not find variant with id `$variantId`"]);
                }

                $variant->setEnabled(true);

                $this->logchange(true, null, null, $variant, "désactive la variante `$variantId`", "E: V($variantId)");

                return new JsonResponse(['ok' => true, 'variantId' => $variantId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function disableVariantAction(Request $request)
    {
        $variantId = $request->get('variantId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($variantId) {
                $variant = $manager->find(ProductVariant::class, $variantId);

                if ($variant === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Could not find variant with id `$variantId`"]);
                }

                $variant->setEnabled(false);

                $this->logchange(true, null, null, $variant, "désactive la variante `$variantId`", "D: V($variantId)");

                return new JsonResponse(['ok' => true, 'variantId' => $variantId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function removeVariantAction(Request $request)
    {
        $variantId = $request->get('variantId');
        $manager = $this->getDoctrine()->getManager();
        $variant = $manager->find(ProductVariant::class, $variantId);
        $manager->remove($variant);
        $this->logchange(true, null, null, null, "supprime la variante `$variantId` dans le produit ".$variant->getProduct()->getId(), "D: P(".$variant->getProduct()->getId().") V($variantId)");

        $manager->flush();

        // TODO
        return new JsonResponse(['ok' => true, 'id' => $variantId]);
    }

    public function moveVariantAction(Request $request)
    {
        $variantId = $request->get('variantId');
        $oldProductId = $request->get('oldProductId');
        $newProductId = $request->get('newProductId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function(EntityManager $manager) use ($variantId, $oldProductId, $newProductId) {
                $variant = $manager->find(ProductVariant::class, $variantId);

                if ($variant->getProduct()->getId() != $oldProductId) {
                    return new JsonResponse(['ok' => false, 'message' => "L'ancien produit de cette variante n'est pas celui indiqué ! Tapez F5 d'abord ;)"]);
                }

                if ($variant->getProduct()->getId() != $newProductId) {
                    $product = $manager->find(Product::class, $newProductId);

                    if ($product === null) {
                        return new JsonResponse(['ok' => false, 'message' => "Impossible de trouver le produit avec l'id `$newProductId`"]);
                    }

                    $product->addVariant($variant);

                    $spviRepository = $manager->getRepository(SupplierProductVariantInformations::class);
                    $sppRepository = $manager->getRepository(SupplierProductPrice::class);
                    $spphRepository = $manager->getRepository(SupplierProductPriceHistory::class);

                    /** @var SupplierProductVariantInformations[] $spvi */
                    $spvi = $spviRepository->findBy(['product' => $oldProductId, 'productVariant' => $variantId]);
                    /** @var SupplierProductPrice[] $spp */
                    $spp = $sppRepository->findBy(['product' => $oldProductId, 'productVariant' => $variantId]);
                    /** @var SupplierProductPriceHistory[] $spph */
                    $spph = $spphRepository->findBy(['product' => $oldProductId, 'productVariant' => $variantId]);

                    $entities = array_merge($spvi, $spp, $spph);

                    foreach ($entities as $entity) {
                        /** @noinspection PhpUndefinedMethodInspection */
                        $entity->setProduct($product);
                    }

                    $this->logchange(true, null, null, $variant, "déplace la variante de `$oldProductId` vers `$newProductId`", "M: V($variantId) from P($oldProductId) to P($newProductId)");
                }

                return new JsonResponse(['ok' => true, 'id' => $variantId, 'oldProductId' => $oldProductId, 'newProductId' => $newProductId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function pasteVariantsOnProductAction(Request $request)
    {
        $productId = $request->get('productId');
        $variantIds = $request->get('variantIds');
        $manager = $this->getDoctrine()->getManager();

        $product = $manager->find(Product::class, $productId);

        if ($product === null) {
            return new JsonResponse(['ok' => false, "Impossible de trouver le produit avec l'id `$productId`"]);
        }

        $status = [];

        foreach ($variantIds as $variantId) {
            $status[$variantId] = [
                'variantId' => $variantId,
                'status' => $this->pasteVariantOnProduct($product, $variantId)
            ];
        }

        return new JsonResponse(['ok' => true, 'productId' => $productId, 'variantIds' => $variantIds]);
    }

    private function pasteVariantOnProduct(ProductInterface $product, int $variantId)
    {
        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function(EntityManager $manager) use (&$product, $variantId) {

                $variant = $manager->find(ProductVariant::class, $variantId);

                if ($variant === null) {
                    return false;
                }

                /**
                 * Copy the variant first
                 */
                $variantTranslationFactory = $this->get('sylius.factory.product_variant_translation');
                /** @var ProductVariant $newVariant */
                $newVariant = $this->get('sylius.factory.product_variant')->createForProduct($product);

                $newVariant->setCode(uniqid());
                $newVariant->setWeight($variant->getWeight());

                foreach ($variant->getOptionValues() as $optionValue) {
                    $newVariant->addOptionValue($optionValue);
                }

                /** @var ProductVariantTranslationInterface $translation */
                foreach ($variant->getTranslations() as $translation) {
                    /** @var ProductVariantTranslationInterface $newTranslation */
                    $newTranslation = $variantTranslationFactory->createNew();

                    $newTranslation->setTranslatable($newVariant);
                    $newTranslation->setLocale($translation->getLocale());
                    $newTranslation->setName($translation->getName());
                }

                $manager->persist($newVariant);

                $this->logchange(true, null, $product, $newVariant, "copie la variante `$variantId` sur le produit `{$product->getId()}`", "");

                /**
                 * Copy supplier informations
                 */
                $supplierProductVariantInformations = $manager->getRepository(SupplierProductVariantInformations::class)->findBy([
                    'product' => $variant->getProduct(),
                    'productVariant' => $variant
                ]);

                foreach ($supplierProductVariantInformations as $spvi) {
                    $newSpvi = new SupplierProductVariantInformations();

                    $newSpvi->setProduct($product);
                    $newSpvi->setProductVariant($newVariant);
                    $newSpvi->setDisplayName($spvi->getDisplayName());
                    $newSpvi->setSupplier($spvi->getSupplier());
                    $newSpvi->setProductLabel($spvi->getProductLabel());
                    $newSpvi->setVariantLabel($spvi->getVariantLabel());
                    $newSpvi->setSalesUnit($spvi->getSalesUnit());
                    $newSpvi->setUnitQuantity($spvi->getUnitQuantity());
                    $newSpvi->setContent($spvi->getContent());
                    $newSpvi->setWeight($spvi->getWeight());
                    $newSpvi->setManuallyCreated($spvi->isManuallyCreated());
                    $newSpvi->setReference($spvi->getReference());

                    $manager->persist($newSpvi);
                }

                /**
                 * Copy prices
                 */
                $supplierProductPrices = $manager->getRepository(SupplierProductPrice::class)->findBy([
                    'product' => $variant->getProduct(),
                    'productVariant' => $variant
                ]);

                $supplierProductPricesHistory = $manager->getRepository(SupplierProductPriceHistory::class)->findBy([
                    'product' => $variant->getProduct(),
                    'productVariant' => $variant
                ]);

                $prices = array_merge($supplierProductPrices, $supplierProductPricesHistory);

                /** @var SupplierProductPrice|SupplierProductPriceHistory $price */
                foreach ($prices as $price) {
                    if ($price instanceof SupplierProductPrice) {
                        $newPrice = new SupplierProductPrice();
                    } else {
                        $newPrice = new SupplierProductPriceHistory();
                    }

                    $newPrice->setProduct($product);
                    $newPrice->setProductVariant($newVariant);
                    $newPrice->setSupplier($price->getSupplier());
                    $newPrice->setRestaurant($price->getRestaurant());
                    $newPrice->setChannelCode($price->getChannelCode());
                    $newPrice->setKgPrice($price->getKgPrice());
                    $newPrice->setUnitPrice($price->getUnitPrice());
                    $newPrice->setCreatedAt($price->getCreatedAt());

                    $manager->persist($newPrice);
                }

                /**
                 * Copy channel pricing
                 */
                $channelPricings = $manager->getRepository(ChannelPricing::class)->findBy(['productVariant' => $variant]);

                foreach ($channelPricings as $channelPricing) {
                    $newChannelPricing = new ChannelPricing();

                    $newChannelPricing->setProductVariant($newVariant);
                    $newChannelPricing->setChannelCode($channelPricing->getChannelCode());
                    $newChannelPricing->setRestaurant($channelPricing->getRestaurant());
                    $newChannelPricing->setPrice($channelPricing->getPrice());
                    $newChannelPricing->setOriginalPrice($channelPricing->getOriginalPrice());

                    $manager->persist($newChannelPricing);
                }

                return true;
            });
        } catch (Throwable $e) {
            return false;
        }
    }

    public function pasteVariantsOnVariantAction(Request $request)
    {
        $pasteOnVariantId = $request->get('variantId');
        $variantIds = $request->get('variantIds');
        $manager = $this->getDoctrine()->getManager();

        $variant = $manager->find(ProductVariant::class, $pasteOnVariantId);

        if ($variant === null) {
            return new JsonResponse(['ok' => false, "Impossible de trouver la variant avec l'id `$pasteOnVariantId`"]);
        }

        $status = [];

        foreach ($variantIds as $variantId) {

            $status[$variantId] = [
                'variantId' => $pasteOnVariantId,
                'status' => $this->pasteVariantOnVariant($variant, $variantId)
            ];
        }

        return new JsonResponse(['ok' => true, 'variantId' => $pasteOnVariantId, 'variantIds' => $variantIds]);
    }

    private function pasteVariantOnVariant(ProductVariant $pasteOnVariant, int $variantId)
    {
        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function(EntityManager $manager) use (&$pasteOnVariant, $variantId) {

                $variant = $manager->find(ProductVariant::class, $variantId);

                $this->logchange(true, null, $variant->getProduct(), $variant, "merge la variante `$variantId` sur la variante `{$pasteOnVariant->getId()}`", "");

                if ($variant === null) {
                    return false;
                }

                /**
                 * Copy supplier informations
                 */
                $supplierProductVariantInformations = $manager->getRepository(SupplierProductVariantInformations::class)->findBy([
                    'product' => $variant->getProduct(),
                    'productVariant' => $variant
                ]);

                foreach ($supplierProductVariantInformations as $spvi) {
                    $newSpvi = new SupplierProductVariantInformations();

                    $newSpvi->setProduct($pasteOnVariant->getProduct());
                    $newSpvi->setProductVariant($pasteOnVariant);
                    $newSpvi->setDisplayName($spvi->getDisplayName());
                    $newSpvi->setSupplier($spvi->getSupplier());
                    $newSpvi->setProductLabel($spvi->getProductLabel());
                    $newSpvi->setVariantLabel($spvi->getVariantLabel());
                    $newSpvi->setSalesUnit($spvi->getSalesUnit());
                    $newSpvi->setUnitQuantity($spvi->getUnitQuantity());
                    $newSpvi->setContent($spvi->getContent());
                    $newSpvi->setWeight($spvi->getWeight());
                    $newSpvi->setManuallyCreated($spvi->isManuallyCreated());
                    $newSpvi->setReference($spvi->getReference());

                    $manager->persist($newSpvi);
                }

                /**
                 * Copy prices
                 */
                $supplierProductPrices = $manager->getRepository(SupplierProductPrice::class)->findBy([
                    'product' => $variant->getProduct(),
                    'productVariant' => $variant
                ]);

                $supplierProductPricesHistory = $manager->getRepository(SupplierProductPriceHistory::class)->findBy([
                    'product' => $variant->getProduct(),
                    'productVariant' => $variant
                ]);

                $prices = array_merge($supplierProductPrices, $supplierProductPricesHistory);

                /** @var SupplierProductPrice|SupplierProductPriceHistory $price */
                foreach ($prices as $price) {
                    if ($price instanceof SupplierProductPrice) {
                        $newPrice = new SupplierProductPrice();
                    } else {
                        $newPrice = new SupplierProductPriceHistory();
                    }

                    $newPrice->setProduct($pasteOnVariant->getProduct());
                    $newPrice->setProductVariant($pasteOnVariant);
                    $newPrice->setSupplier($price->getSupplier());
                    $newPrice->setRestaurant($price->getRestaurant());
                    $newPrice->setChannelCode($price->getChannelCode());
                    $newPrice->setKgPrice($price->getKgPrice());
                    $newPrice->setUnitPrice($price->getUnitPrice());
                    $newPrice->setCreatedAt($price->getCreatedAt());

                    $manager->persist($newPrice);
                }

                /**
                 * Copy channel pricing
                 */
                $channelPricings = $manager->getRepository(ChannelPricing::class)->findBy(['productVariant' => $variant]);

                foreach ($channelPricings as $channelPricing) {
                    $newChannelPricing = new ChannelPricing();

                    $newChannelPricing->setProductVariant($pasteOnVariant);
                    $newChannelPricing->setChannelCode($channelPricing->getChannelCode());
                    $newChannelPricing->setRestaurant($channelPricing->getRestaurant());
                    $newChannelPricing->setPrice($channelPricing->getPrice());
                    $newChannelPricing->setOriginalPrice($channelPricing->getOriginalPrice());

                    $manager->persist($newChannelPricing);
                }

                $variant->setEnabled(false);

                return true;
            });
        } catch (Throwable $e) {
            return false;
        }
    }

    public function removeTaxonAction(Request $request)
    {
        $taxonId = $request->get('taxonId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($taxonId) {
                $taxon = $manager->find(Taxon::class, $taxonId);

                if ($taxon !== null) {
                    if (!$taxon->getChildren()->isEmpty()) {
                        return new JsonResponse(['ok' => false, 'message' => "Ce taxon a des enfants !"]);
                    }

                    $products = $manager->getRepository(Product::class)->findBy(['mainTaxon' => $taxon]);

                    if (!empty($products)) {
                        return new JsonResponse(['ok' => false, 'message' => "Des produits sont rattachés à ce taxon !"]);
                    }

                    $manager->remove($taxon);

                    $this->logchange(true, null, null, null, "suppression du taxon `{$taxon->getName()}`", "D: T({$taxonId})");
                }

                return new JsonResponse(['ok' => true, 'id' => $taxonId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function createTaxonAction(Request $request)
    {
        $parentTaxonId = $request->get('taxonId');
        $newTaxonName = $request->get('name');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($parentTaxonId, $newTaxonName) {
                $parentTaxon = $manager->find(Taxon::class, $parentTaxonId);

                $slugifier = new Slugify();

                $taxon = new Taxon();
                $taxon->setParent($parentTaxon);

                $taxon->setCurrentLocale('fr_FR');
                $taxon->setName($newTaxonName);
                $taxon->setCode(uniqid());
                $taxon->setSlug($slugifier->slugify($newTaxonName));

                $manager->persist($taxon);

                $this->logchange(true, $parentTaxon, null, null, "création d'un nouveau taxon `$newTaxonName` dans `{$parentTaxon->getName()}`", "CT: T({$taxon->getId()}) in T({$parentTaxonId})");

                return new JsonResponse(['ok' => true, 'id' => $taxon->getId()]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function createProductAction(Request $request)
    {
        $parentTaxonId = $request->get('taxonId');
        $newProductName = $request->get('name');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (Entitymanager $manager) use ($parentTaxonId, $newProductName) {
                /** @var Product $product */
                $product = $this->get('sylius.factory.product')->createNew();
                $parentTaxon = $manager->find(Taxon::class, $parentTaxonId);

                $product->setCurrentLocale('fr_FR');
                $product->setName($newProductName);

                $slugifier = new Slugify();

                $product->setMainTaxon($parentTaxon);
                $product->setCode(uniqid());
                $product->setSlug($slugifier->slugify($newProductName));

                $manager->persist($product);

                $this->rebuildProductTaxonsForProduct($product, $manager);

                $this->logchange(true, null, $product, null, "création du produit `$newProductName` dans le taxon `{$parentTaxon->getName()}`", "C: P({$product->getId()}) in T($parentTaxonId)");

                return new JsonResponse(['ok' => true, 'id' => $product->getId()]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function createVariantAction(Request $request)
    {
        $productId = $request->get('productId');
        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($productId) {
                $product = $manager->find(Product::class, $productId);

                if ($product === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Impossible de trouver le produit avec l'id `$productId`"]);
                }

                $variant = $this->get('sylius.factory.product_variant')->createForProduct($product);

                $variant->setCurrentLocale('fr_FR');
                $variant->setWeight(1);
                $variant->setName('-');
                $variant->setCode(uniqid());

                $manager->persist($variant);
                $manager->flush();

                $this->logchange(true, null, $product, $variant, "création d'une nouvelle variante pour le produit `{$product->getName()}`", "C: V({$variant->getId()}) in P($productId)");

                return new JsonResponse(['ok' => true, 'id' => $variant->getId()]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function removePriceAction(Request $request)
    {
        $resourceId = $request->get('id');
        $resourceType = $request->get('type');

        if ($resourceId === null || $resourceId == 0 || $resourceType === null) {
            return new JsonResponse(['ok' => false, 'message' => 'Resource id and/or type is not defined']);
        }

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($resourceId, $resourceType) {
                /** @var SupplierProductPrice|SupplierProductPriceHistory|null $resource */
                $resource = $manager->find($resourceType, $resourceId);

                if ($resource === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find resource with id `$resourceId` and type `$resourceType`"]);
                }

                if ($resource instanceof SupplierProductPrice) {
                    $lastHistoryPrice = $manager->getRepository(SupplierProductPriceHistory::class)->findBy([
                        'product' => $resource->getProduct(),
                        'productVariant' => $resource->getProductVariant(),
                        'supplier' => $resource->getSupplier(),
                        'restaurant' => $resource->getRestaurant()
                    ], ['createdAt' => 'DESC'], 1);

                    $isEmpty = empty($lastHistoryPrice);

                    if ($isEmpty) {
                        /** @noinspection PhpParamsInspection */
                        $this->removeChannelPricing($resource->getProductVariant(), $resource->getRestaurant(), $resource->getSupplier());
                    }

                    $manager->remove($resource);
                    $manager->flush();

                    if (!$isEmpty) {
                        /** @var SupplierProductPriceHistory $lastHistoryPrice */
                        $lastHistoryPrice = $lastHistoryPrice[0];

                        $price = new SupplierProductPrice();

                        $price->setProduct($lastHistoryPrice->getProduct());
                        $price->setProductVariant($lastHistoryPrice->getProductVariant());
                        $price->setSupplier($lastHistoryPrice->getSupplier());
                        $price->setRestaurant($lastHistoryPrice->getRestaurant());
                        $price->setChannelCode($lastHistoryPrice->getChannelCode());
                        $price->setKgPrice($lastHistoryPrice->getKgPrice());
                        $price->setUnitPrice($lastHistoryPrice->getUnitPrice());
                        $price->setCreatedAt($lastHistoryPrice->getCreatedAt());

                        $manager->persist($price);

                        $manager->remove($lastHistoryPrice);

                        /** @noinspection PhpParamsInspection */
                        $this->insertOrUpdateChannelPricing($price->getProductVariant(), $price->getRestaurant(), $price->getSupplier(), $price->getKgPrice(), $price->getUnitPrice());
                    }
                } else {
                    $manager->remove($resource);
                }

                return new JsonResponse(['ok' => true, 'message' => "Successfully removed resource with id `$resourceId` and type `$resourceType`"]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function updatePriceAction(Request $request)
    {
        $resourceId = $request->get('id');
        $resourceType = $request->get('type');

        if ($resourceId === null || $resourceId == 0 || $resourceType === null) {
            return new JsonResponse(['ok' => false, 'message' => 'Resource id and/or type is not defined']);
        }

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($request, $resourceId, $resourceType) {
                /** @var SupplierProductPrice|SupplierProductPriceHistory|null $resource */
                $resource = $manager->find($resourceType, $resourceId);

                if ($resource === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find resource with id `$resourceId` and type `$resourceType`"]);
                }

                $kgPrice = $request->get('kg_price');
                $unitPrice = $request->get('unit_price');
                $priceDate = $request->get('price_date') . ' 00:00:00';
                $priceRestaurant = $request->get('price_restaurant');
                $priceSupplier = $request->get('price_supplier');

                $restaurant = $manager->find(Restaurant::class, $priceRestaurant);
                $supplier = $manager->find(Supplier::class, $priceSupplier);

                if ($restaurant === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find restaurant with id `$priceRestaurant`"]);
                }

                if ($supplier === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find supplier with id `$priceSupplier`"]);
                }

                $kgPrice = bcmul($kgPrice, 100);
                $unitPrice = bcmul($unitPrice, 100);

                $resource->setRestaurant($restaurant);
                $resource->setSupplier($supplier);
                $resource->setKgPrice($kgPrice);
                $resource->setUnitPrice($unitPrice);
                $resource->setCreatedAt(new DateTime($priceDate));

                /** @var ProductVariant $variant */
                $variant = $resource->getProductVariant();

                // Modifie le prix courant dans ChannelPricing
                if ($resource instanceof SupplierProductPrice) {
                    $this->insertOrUpdateChannelPricing($variant, $restaurant, $supplier, $kgPrice, $unitPrice);
                }

                return new JsonResponse(['ok' => true, 'message' => "Successfully updated `$resourceType` ($resourceId)"]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function createPriceAction(Request $request)
    {
        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($request) {
                $productId = $request->get('product_id');
                $variantId = $request->get('variant_id');
                $kgPrice = $request->get('kg_price');
                $unitPrice = $request->get('unit_price');
                $priceDate = $request->get('price_date') . ' 00:00:00';
                $priceRestaurant = $request->get('price_restaurant');
                $priceSupplier = $request->get('price_supplier');

                $product = $manager->find(Product::class, $productId);
                $variant = $manager->find(ProductVariant::class, $variantId);
                $restaurant = $manager->find(Restaurant::class, $priceRestaurant);
                $supplier = $manager->find(Supplier::class, $priceSupplier);

                if ($product === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find product with id `$productId`"]);
                }

                if ($variant === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find variant with id `$variantId`"]);
                }

                if ($restaurant === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find restaurant with id `$priceRestaurant`"]);
                }

                if ($supplier === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Unable to find supplier with id `$priceSupplier`"]);
                }

                $priceDate = new DateTime($priceDate);
                $kgPrice = bcmul($kgPrice, 100);
                $unitPrice = bcmul($unitPrice, 100);

                /** @var SupplierProductPrice[] $currentPrice */
                $currentPrice = $manager->getRepository(SupplierProductPrice::class)->findBy(
                    ['product' => $product, 'productVariant' => $variant, 'restaurant' => $restaurant, 'supplier' => $supplier],
                    ['createdAt' => 'DESC'],
                    1
                );

                // On le crée si inexistant
                if (empty($currentPrice)) {
                    $supplierProductPrice = new SupplierProductPrice();

                    $supplierProductPrice->setProduct($product);
                    $supplierProductPrice->setProductVariant($variant);
                    $supplierProductPrice->setRestaurant($restaurant);
                    $supplierProductPrice->setSupplier($supplier);
                    $supplierProductPrice->setKgPrice($kgPrice);
                    $supplierProductPrice->setUnitPrice($unitPrice);
                    $supplierProductPrice->setChannelCode($supplier->getChannel()->getCode());
                    $supplierProductPrice->setCreatedAt($priceDate);

                    $manager->persist($supplierProductPrice);

                    $this->insertOrUpdateChannelPricing($variant, $restaurant, $supplier, $kgPrice, $unitPrice);
                } else {
                    $currentPrice = $currentPrice[0];

                    // Move to history then update
                    if ($currentPrice->getCreatedAt() <= $priceDate) {
                        // Create history with current data
                        $supplierProductPriceHistory = new SupplierProductPriceHistory();

                        $supplierProductPriceHistory->setProduct($currentPrice->getProduct());
                        $supplierProductPriceHistory->setProductVariant($currentPrice->getProductVariant());
                        $supplierProductPriceHistory->setRestaurant($currentPrice->getRestaurant());
                        $supplierProductPriceHistory->setSupplier($currentPrice->getSupplier());
                        $supplierProductPriceHistory->setKgPrice($currentPrice->getKgPrice());
                        $supplierProductPriceHistory->setUnitPrice($currentPrice->getUnitPrice());
                        $supplierProductPriceHistory->setChannelCode($currentPrice->getChannelCode());
                        $supplierProductPriceHistory->setCreatedAt($currentPrice->getCreatedAt());

                        $manager->persist($supplierProductPriceHistory);

                        // Update
                        $currentPrice->setKgPrice($kgPrice);
                        $currentPrice->setUnitPrice($unitPrice);
                        $currentPrice->setCreatedAt($priceDate);

                        $this->insertOrUpdateChannelPricing($variant, $restaurant, $supplier, $kgPrice, $unitPrice);
                    } // Create history price
                    else if ($currentPrice->getCreatedAt() > $priceDate) {
                        $supplierProductPriceHistory = new SupplierProductPriceHistory();

                        $supplierProductPriceHistory->setProduct($product);
                        $supplierProductPriceHistory->setProductVariant($variant);
                        $supplierProductPriceHistory->setRestaurant($restaurant);
                        $supplierProductPriceHistory->setSupplier($supplier);
                        $supplierProductPriceHistory->setKgPrice($kgPrice);
                        $supplierProductPriceHistory->setUnitPrice($unitPrice);
                        $supplierProductPriceHistory->setChannelCode($supplier->getChannel()->getCode());
                        $supplierProductPriceHistory->setCreatedAt($priceDate);

                        $manager->persist($supplierProductPriceHistory);
                    }
                }

                return new JsonResponse(['ok' => true, 'message' => "Successfully created price for variant `$variantId`"]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function togglePromoAction(int $id)
    {
        /** @var SupplierProductPrice $supplierProductPrice */
        $supplierProductPrice = $this->get('app.repository.supplier_product_price')->find($id);

        $supplierProductPrice->setPromo(!$supplierProductPrice->isPromo());

        $this->getDoctrine()->getManager()->flush();

        return $this->json(['ok' => true, 'promo' => $supplierProductPrice->isPromo()]);
    }

    private function removeChannelPricing(ProductVariant $variant, Restaurant $restaurant, Supplier $supplier)
    {
        $manager = $this->getDoctrine()->getManager();
        $channelPricingRepository = $manager->getRepository(ChannelPricing::class);
        $channelPricing = $channelPricingRepository->findOneBy([
            'productVariant' => $variant,
            'restaurant' => $restaurant,
            'channelCode' => $supplier->getChannel()->getCode()
        ]);

        if ($channelPricing !== null) {
            $manager->remove($channelPricing);
        }
    }

    private function insertOrUpdateChannelPricing(ProductVariant $variant, Restaurant $restaurant, Supplier $supplier, int $kgPrice, int $unitPrice)
    {
        $manager = $this->getDoctrine()->getManager();

        switch ($variant->getOptionOrderUnit()->getCode()) {
            case 'ORDER_PC':
                $channelPricingPrice = $unitPrice;
                break ;
            case 'ORDER_KG':
            default:
                $channelPricingPrice = $kgPrice;
                break ;
        }

        $channelPricingRepository = $manager->getRepository(ChannelPricing::class);

        $channelPricing = $channelPricingRepository->findOneBy([
            'productVariant' => $variant,
            'restaurant' => $restaurant,
            'channelCode' => $supplier->getChannel()->getCode()
        ]);

        $oldPrice = 0;
        if ($channelPricing !== null) {
            $oldPrice = $channelPricing->getPrice();
            $channelPricing->setPrice($channelPricingPrice);
            $channelPricing->setOriginalPrice($channelPricingPrice);
        } else {
            $channelPricing = new ChannelPricing();

            $channelPricing->setProductVariant($variant);
            $channelPricing->setRestaurant($restaurant);
            $channelPricing->setChannelCode($supplier->getChannel()->getCode());
            $channelPricing->setPrice($channelPricingPrice);
            $channelPricing->setOriginalPrice($channelPricingPrice);

            $manager->persist($channelPricing);
        }

        $this->logchange(true, null, $variant->getProduct(), $variant, "change le prix de `$oldPrice` à `$channelPricingPrice` pour le couple F/R `{$supplier->getId()}`/`{$restaurant->getId()}`", "");
    }

    public function associateSupplierAction(Request $request)
    {
        $variantId = $request->get('variantId');
        $supplierId = $request->get('supplierId');
        $label = $request->get('label');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($variantId, $supplierId, $label) {
                $variant = $manager->find(ProductVariant::class, $variantId);
                $supplier = $manager->find(Supplier::class, $supplierId);

                if ($variant === null || $supplier === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Could not find required informations"]);
                }

                $spviRepository = $manager->getRepository(SupplierProductVariantInformations::class);

                $info = $spviRepository->findOneBy([
                    'productVariant' => $variant,
                    'product' => $variant->getProduct(),
                    'supplier' => $supplierId
                ]);

                if ($info === null) {
                    $info = new SupplierProductVariantInformations();

                    $info->setSupplier($supplier);
                    $info->setProduct($variant->getProduct());
                    $info->setProductVariant($variant);
                    $info->setWeight($variant->getWeight());
                    /** @noinspection PhpParamsInspection */
                    $info->setContent($variant->getOptionUnitContentQuantity()->getValue());
                    $info->setProductLabel($label);
                    $info->setDisplayName($label);
                    $info->setVariantLabel($variant->getName());
                    /** @noinspection PhpParamsInspection */
                    $info->setUnitQuantity($variant->getOptionUnitQuantity()->getValue());
                    $info->setSalesUnit(substr($variant->getOptionSupplierSalesUnit()->getValue(), 0, 2));

                    $manager->persist($info);
                }

                return new JsonResponse(['ok' => true, 'variantId' => $variantId, 'supplierId' => $supplierId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function associateVariantAction(Request $request)
    {
        $variantId = $request->get('variantId');
        $supplierId = $request->get('supplierId');
        $restaurantId = $request->get('restaurantId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($variantId, $supplierId, $restaurantId) {
                $variant = $manager->find(ProductVariant::class, $variantId);
                $supplier = $manager->find(Supplier::class, $supplierId);
                $restaurant = $manager->find(Restaurant::class, $restaurantId);

                if ($variant === null || $supplier === null || $restaurant === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Could not find required informations"]);
                }

                $supplierProductPriceRepository = $manager->getRepository(SupplierProductPrice::class);

                $price = $supplierProductPriceRepository->findOneBy(['productVariant' => $variant, 'supplier' => $supplier, 'restaurant' => $restaurant]);

                if ($price === null) {
                    $price = new SupplierProductPrice();

                    $date = new DateTime();
                    $date->setTime(0, 0, 0, 0);

                    $price->setProductVariant($variant);
                    $price->setProduct($variant->getProduct());
                    $price->setSupplier($supplier);
                    $price->setRestaurant($restaurant);
                    $price->setKgPrice(0);
                    $price->setUnitPrice(0);
                    $price->setChannelCode($supplier->getChannel()->getCode());
                    $price->setCreatedAt($date);

                    $manager->persist($price);
                }

                return new JsonResponse(['ok' => true, 'variantId' => $variantId, 'supplierId' => $supplierId, 'restaurantId' => $restaurantId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function updateSupplierVariantLabelAction(Request $request)
    {
        $informationsId = $request->get('informationsId');
        $variantId      = $request->get('variantId');
        $supplierReference  = $request->get('supplierReference');
        $orderRef  = $request->get('orderRef');
        $supplierLabel  = $request->get('supplierLabel');
        $displayLabel   = $request->get('displayLabel');
        $weight         = $request->get('weight');
        $unitQuantity   = $request->get('unitQuantity');
        $salesUnit      = $request->get('salesUnit');

        if (is_null($displayLabel)) {
            $displayLabel = $supplierLabel;
        }

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($informationsId, $variantId, $supplierLabel, $supplierReference, $orderRef, $displayLabel, $weight, $unitQuantity, $salesUnit) {
                $supplierProductVariantInformations = $manager->getRepository(SupplierProductVariantInformations::class)->findOneBy(['id' => $informationsId, 'productVariant' => $variantId]);

                if ($supplierProductVariantInformations === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Could not find supplier informations entity with id `$informationsId` and variant id `$variantId`"]);
                }

                $supplierProductVariantInformations->setProductLabel($supplierLabel);
                $supplierProductVariantInformations->setReference($supplierReference);
                $supplierProductVariantInformations->setOrderRef($orderRef);
                $supplierProductVariantInformations->setDisplayName($displayLabel);
                $supplierProductVariantInformations->setWeight($weight);
                $supplierProductVariantInformations->setUnitQuantity($unitQuantity);
                $supplierProductVariantInformations->setSalesUnit($salesUnit);

                $this->logchange(true, null, $supplierProductVariantInformations->getProduct(), $supplierProductVariantInformations->getProductVariant(), "change les informations de matching fournisseur", "informations-id:`$informationsId`|reference:`$supplierReference`|weight:`$weight`|unit-quantity:`$unitQuantity`|sales-unit:`$salesUnit`|label:`$supplierLabel`");

                return new JsonResponse(['ok' => true, 'id' => $informationsId, 'variantId' => $variantId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function disassociateSupplierAction(Request $request)
    {
        $informationsId = $request->get('informationsId');
        $variantId = $request->get('variantId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($informationsId, $variantId) {

                $supplierProductVariantInformations = $manager->getRepository(SupplierProductVariantInformations::class)->findOneBy(['id' => $informationsId, 'productVariant' => $variantId]);
                $supplier = $supplierProductVariantInformations->getSupplier();

                if ($supplierProductVariantInformations === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Could not find entity with id `$informationsId` and variant id `$variantId`"]);
                }

                $manager->remove($supplierProductVariantInformations);
/*
                $spp = $manager->getRepository(SupplierProductPrice::class)->findBy(['supplier' => $supplier->getId(), 'productVariant' => $variantId]);
                $spph = $manager->getRepository(SupplierProductPriceHistory::class)->findBy(['supplier' => $supplierId, 'productVariant' => $variantId]);
                $prices = array_merge($spp, $spph);

                foreach ($prices as $price) {
                    $manager->remove($price);
                }

                $channelPricings = $manager->getRepository(ChannelPricing::class)->findBy([
                    'channelCode' => $supplier->getChannel()->getCode(),
                    'productVariant' => $variantId
                ]);

                foreach ($channelPricings as $channelPricing) {
                    $manager->remove($channelPricing);
                }
*/
                $this->logchange(true, null, null, null, "supprime les informations de matching fournisseur `{$supplier->getName()}`", "id:`$informationsId`|variante:`$variantId`");

                return new JsonResponse(['ok' => true, 'informationsId' => $informationsId, 'variantId' => $variantId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function disassociateVariantAction(Request $request)
    {
        $supplierId = $request->get('supplierId');
        $restaurantId = $request->get('restaurantId');
        $variantId = $request->get('variantId');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($supplierId, $restaurantId, $variantId) {
                $spp = $manager->getRepository(SupplierProductPrice::class)->findBy([
                    'supplier' => $supplierId,
                    'productVariant' => $variantId,
                    'restaurant' => $restaurantId
                ]);

                $spph = $manager->getRepository(SupplierProductPriceHistory::class)->findBy([
                    'supplier' => $supplierId,
                    'productVariant' => $variantId,
                    'restaurant' => $restaurantId
                ]);

                $prices = array_merge($spp, $spph);

                /** @var SupplierProductPrice|SupplierProductPriceHistory $price */
                foreach ($prices as $price) {
                    $manager->remove($price);
                }

                $supplier = $manager->find(Supplier::class, $supplierId);

                $channelPricings = $manager->getRepository(ChannelPricing::class)->findBy([
                    'channelCode' => $supplier->getChannel()->getCode(),
                    'productVariant' => $variantId,
                    'restaurant' => $restaurantId
                ]);

                $this->logchange(true, null, null, null, "supprime les prix du fournisseur `{$supplier->getName()}`", "supplier:`$supplierId`|variante:`$variantId`");

                foreach ($channelPricings as $channelPricing) {
                    $manager->remove($channelPricing);
                }

                return new JsonResponse(['ok' => true, 'supplierId' => $supplierId, 'restaurantId' => $restaurantId, 'variantId' => $variantId]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => false, 'message' => $e->getMessage()]);
        }
    }

    public function createOptionValueAction(Request $request)
    {
        $code = $request->get('code');
        $value = $request->get('value');

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        try {
            return $manager->transactional(function (EntityManager $manager) use ($code, $value) {
                /** @var ProductOption|null $option */
                $option = $this->get('sylius.repository.product_option')->findOneBy(['code' => $code]);

                if ($option === null) {
                    return new JsonResponse(['ok' => false, 'message' => "Could not find option with code `$code`"]);
                }

                /** @var ProductOptionValue $optionValue */
                $optionValue = $this->get('sylius.factory.product_option_value')->createNew();

                $codifiedValue = preg_replace('/[\s]+/', '_', $value);

                $optionValue->setOption($option);
                $optionValue->setCode("{$code}_{$codifiedValue}");
                $optionValue->setCurrentLocale('fr_FR');
                $optionValue->setFallbackLocale('fr_FR');
                $optionValue->setValue($value);

                $manager->persist($optionValue);

                $this->logchange(true, null, null, null, "ajoute une valeur d'option `$value` pour l'option `{$option->getCode()}`", "");

                return new JsonResponse(['ok' => true]);
            });
        } catch (Throwable $e) {
            return new JsonResponse(['ok' => true, 'message' => $e->getMessage()]);
        }
    }
}