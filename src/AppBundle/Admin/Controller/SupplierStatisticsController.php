<?php

namespace AppBundle\Admin\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class SupplierStatisticsController extends Controller
{

    public function indexAction(Request $request)
    {

        $form = $this->getForm();
        $form->handleRequest($request);

        $endDate = new DateTime("last day of this month 23:59:59");
        $startDate = new DateTime("first day of 2 month ago 00:00:00");
        dump($startDate);

        if ($form->isSubmitted() && $form->isValid()) {
            $startDate = $form->get('startDate')->getData();
            $endDate = $form->get('endDate')->getData();
        }

        $startDateTxt = $startDate->format('Y-m-d');
        $endDateTxt = $endDate->format('Y-m-d');

        $supplierStats = $this->get('sylius.repository.order')->getTopSales($startDate, $endDate, null, true, true);

        // get suppliers list
        $suppliersList = [];
        foreach($supplierStats as $month => $statsByMonth) {
            foreach($statsByMonth as $key => $stats) {
                $suppliersList[$key] = $stats['grossiste'];
            }
        }

        dump($supplierStats, $suppliersList);

        return $this->render('@App/Admin/SupplierStatistics/index.html.twig', [
            'form' => $form->createView(),
            'start' => $startDateTxt,
            'end'   => $endDateTxt,
            'supplier_list'     => $suppliersList,
            'supplier_stats'    => $supplierStats,
        ]);
    }

    public function detailAction($id, $start, $end)
    {
        if ($start == 0 && $end == 0) {
            $startDate = (new DateTime())->modify('-180 day');
            $endDate = new DateTime();
        } else {
            $startDate = new DateTime($start);
            $endDate = new DateTime($end);
        }

        $prospect = $this->get('app.repository.prospect')->findOneBy(['id' => $id]);

        $supplierStats = $this->get('sylius.repository.order')->getTopSales($startDate, $endDate, $id, true);
        $productStats = $this->get('sylius.repository.order')->getProductTopSales($startDate, $endDate, $id);
        $orderStats = $this->get('sylius.repository.order')->getOrderSales($startDate, $endDate, $id);
        $lastOrderDate = $this->get('sylius.repository.order')->getLastOrderDate($id);

        // get suppliers list
        $suppliersList = [];
        foreach($supplierStats as $month => $statsByMonth) {
            foreach($statsByMonth as $key => $stats) {
                $suppliersList[$key] = $stats['grossiste'];
            }
        }

        // get product list
        $productList = [];
        foreach($productStats as $month => $statsByMonth) {
            foreach($statsByMonth as $key => $stats) {
                $productList[$key] = $stats['produit'];
            }
        }

        return $this->render('@App/Admin/SupplierStatistics/detail.html.twig', [
            'prospect'          => $prospect,
            'supplier_list'     => $suppliersList,
            'supplier_stats'    => $supplierStats,
            'product_list'      => $productList,
            'product_stats'     => $productStats,
            'order_stats'       => $orderStats,
            'last_order'        => $lastOrderDate,
        ]);
    }

    private function getForm()
    {
        return $this->createFormBuilder()
            ->add('startDate', DateType::class, ['label' => 'Date de début', 'widget' => 'single_text', 'attr' => ['class' => 'ui datepicker']])
            ->add('endDate', DateType::class, ['label' => 'Date de fin', 'widget' => 'single_text', 'attr' => ['class' => 'ui datepicker']])
            ->getForm()
            ;
    }


}
