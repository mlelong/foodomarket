<?php


namespace AppBundle\Admin\Controller;


use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ShopUserService;
use AppBundle\Util\SupplierUtil;
use Doctrine\ORM\EntityManagerInterface;
use JMS\JobQueueBundle\Entity\Job;
use JMS\JobQueueBundle\Entity\Repository\JobManager;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class SupplierAccountController extends AbstractController
{
    /**
     * @var ProspectRepository
     */
    private $prospectRepository;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    /**
     * @var SupplierAccountLogRepository
     */
    private $supplierAccountLogRepository;

    /**
     * @var ShopUserService
     */
    private $shopUserService;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        ProspectRepository $prospectRepository,
        SupplierRepository $supplierRepository,
        SupplierAccountLogRepository $supplierAccountLogRepository,
        ShopUserService $shopUserService,
        JobManager $jobManager,
        EntityManagerInterface $entityManager
    ) {
        $this->prospectRepository = $prospectRepository;
        $this->supplierRepository = $supplierRepository;
        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
        $this->shopUserService = $shopUserService;
        $this->jobManager = $jobManager;
        $this->entityManager = $entityManager;
    }

    public function index(Request $request)
    {
        $searchForm = $this->getSearchForm();

        $searchForm->handleRequest($request);

        $zip = $firstname = $type = $suggestionsOnly = $email = null;
        $applySuggestions = $request->get('applySuggestions') === 'true';
        /** @var Job $job */
        $job = $this->entityManager->getRepository(Job::class)->findOneBy(['command' => 'app:batch-open:supplier-account']);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $zip = $searchForm->get('zip')->getData();
            $firstname = $searchForm->get('firstname')->getData();
            $type = $searchForm->get('type')->getData();
            $suggestionsOnly = $searchForm->get('suggestionsOnly')->getData();
            $email = $searchForm->get('email')->getData();

            if ($applySuggestions) {
                if ($job !== null && !($job->isNew() || $job->isPending() || $job->isRunning())) {
                    $this->entityManager->remove($job);
                    $this->entityManager->flush();

                    $job = null;
                }

                if ($job === null) {
                    $job = new Job('app:batch-open:supplier-account', [$zip, $firstname, $type, $email]);

                    $this->entityManager->persist($job);
                    $this->entityManager->flush();

                    $request->query->remove('applySuggestions');

                    return $this->index($request);
                }
            }

            $prospects = $this->fetchProspects($zip, $firstname, $type, $email);
        } else {
            $prospects = $this->fetchProspects($zip, $firstname, $type, $email, 0, 10);
        }

        $partners = $this->supplierRepository->getPartners(false);
        $individualPartners = $this->supplierRepository->getIndividualsPartners();
        $display = [];
        $nbSuggestions = 0;

        foreach ($prospects as $prospect) {
            /** @var SupplierAccountLog[] $supplierAccountLogs */
            $supplierAccountLogs = $this->supplierAccountLogRepository->findBy(['prospect' => $prospect]);
            $suppliers = [];

            foreach ($supplierAccountLogs as $supplierAccountLog) {
                $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();

                $suppliers[$supplier->getId()] = [
                    'name' => $supplier->getDisplayName(true),
                    'status' => $supplierAccountLog->getStatus()
                ];
            }

            $retainedPartners = array_filter(
                $prospect->getType() === Prospect::TYPE_INDIVIDUAL ? $individualPartners : $partners,
                function($partner) use($suppliers, $prospect) {
                    $facade = $partner->getFacadeSupplier();

                    // If it is HP, then auto bound only the SP grid
                    if ($facade->getId() == 11 && $partner->getId() != 23) {
                        return false;
                    }

                    if ($prospect->getType() === Prospect::TYPE_PROFESSIONNAL && !$facade->isAutoBind()) {
                        return false;
                    }

                    return !isset($suppliers[$partner->getId()]) && !empty($prospect->getZipCode())
                        && SupplierUtil::getDeliveryCost($partner, $prospect->getZipCode()) !== null
                    ;
                }
            );
            $suggestions = [];

            foreach ($retainedPartners as $partner) {
                $suggestions[] = [
                    'id' => $partner->getId(),
                    'name' => $partner->getName(),
                ];
            }

            if (!empty($suggestions)) {
                ++$nbSuggestions;
            } elseif ($suggestionsOnly) {
                continue ;
            }

            $display[] = [
                'id' => $prospect->getId(),
                'firstname' => $prospect->getFirstName(),
                'zip' => $prospect->getZipCode(),
                'type' => $prospect->getType(),
                'suppliers' => $suppliers,
                'suggestions' => $suggestions
            ];
        }

        return $this->render('@App/Admin/SupplierAccount/index.html.twig', [
            'job' => $job,
            'searchForm' => $searchForm->createView(),
            'prospects' => $display,
            'nbSuggestions' => $nbSuggestions
        ]);
    }

    public function openAccount($prospectId, $supplierId)
    {
        /** @var Prospect $prospect */
        $prospect = $this->prospectRepository->find($prospectId);
        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($supplierId);
        $facade = $supplier->getFacadeSupplier();

        if ($prospect->getType() === Prospect::TYPE_PROFESSIONNAL) {
            if (!$facade->isAutoBind()) {
                return $this->json(['ok' => false]);
            }

            $this->shopUserService->openSupplierAccount($prospect, $supplierId, $facade->isPaymentOnline() ? SupplierAccountLog::STATUS_ORDERING : SupplierAccountLog::STATUS_WAITING);
        } else {
            $this->shopUserService->openSupplierAccount($prospect, $supplierId, SupplierAccountLog::STATUS_ACCOUNT_OPENED);
        }

        $this->shopUserService->sendNewlyBoundNotification($prospect, [$supplier]);

        return $this->json(['ok' => true]);
    }

    /**
     * @param string|null $zip
     * @param string|null $firstname
     * @param string|null $type
     * @param string|null $email
     * @param int $offset
     * @param int $limit
     * @return Prospect[]
     */
    private function fetchProspects(?string $zip = null, ?string $firstname = null, ?string $type = null, ?string $email = null, ?int $offset = null, ?int $limit = null)
    {
        $qb = $this->prospectRepository->createQueryBuilder('p');

        if ($zip) {
            $qb
                ->andWhere($qb->expr()->like('p.zipCode', ':zip'))
                ->setParameter('zip', "$zip%")
            ;
        }

        if ($firstname) {
            $qb
                ->andWhere($qb->expr()->like('p.firstName', ':firstname'))
                ->setParameter('firstname', $firstname)
            ;
        }

        if ($type) {
            $qb
                ->andWhere('p.type = :type')
                ->setParameter('type', $type)
            ;
        }

        if ($email) {
            $qb
                ->andWhere('p.email = :email')
                ->setParameter('email', $email)
            ;
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $qb->orderBy('p.id', 'desc');

        return $qb->setMaxResults($limit)->getQuery()->getResult();
    }

    private function getSearchForm()
    {
        return $this->createFormBuilder(null, ['method' => 'GET', 'csrf_protection' => false])
            ->add('zip', TextType::class, ['label' => 'Code postal', 'required' => false])
            ->add('firstname', TextType::class, ['label' => 'Prénom', 'required' => false])
            ->add('email', EmailType::class, ['label' => 'Email', 'required' => false])
            ->add('type', ChoiceType::class, ['label' => 'Type', 'required' => false, 'choices' => [
                'Particulier' => Prospect::TYPE_INDIVIDUAL,
                'Professionnel' => Prospect::TYPE_PROFESSIONNAL
            ]])
            ->add('suggestionsOnly', CheckboxType::class, ['label' => 'Seulement les suggestions', 'required' => false])
            ->getForm()
        ;
    }
}
