<?php


namespace AppBundle\Admin\Controller;


use JMS\JobQueueBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AsyncJobController extends AbstractController
{
    public function show(int $id)
    {
        $job = $this->getDoctrine()->getManager()->find(Job::class, $id);

        return $this->render('@App/Admin/AsyncJob/show.html.twig', ['job' => $job]);
    }
}