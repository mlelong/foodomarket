<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\BlockedPrice;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Form\ProductVariantAutocompleteChoiceType;
use AppBundle\Form\Type\CustomProductOptionValueCollectionType;
use AppBundle\Repository\BlockedPriceRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ProductUtils;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Sylius\Component\Core\Model\ProductImage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;

class SupplierPricesController extends Controller
{
    public function indexAction()
    {
        /** @var SupplierRepository $supplierRepository */
        $supplierRepository = $this->get('app.repository.supplier');
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        /** @var ProspectRepository $prospectRepository */
        $prospectRepository = $this->get('app.repository.prospect');
        $partners = $supplierRepository->getPartners();
        $suppliersDisplay = [];

        foreach ($partners as $partner) {
            $prospects = $prospectRepository->getProspectsBoundToPartnerSupplier($partner);
            $restaurants = array_reduce($prospects, function(int $carry, Prospect $prospect) use($prospectRepository) {
                return $carry + count($prospectRepository->getProspectRestaurants($prospect));
            }, 0);

            $suppliersDisplay[] = [
                'id' => $partner->getId(),
                'name' => $partner->getDisplayName(true),
                'products' => $supplierProductPriceRepository->countPrices([$partner], null, null, [4], false),
                'clients' => count($prospects),
                'restaurants' => $restaurants
            ];
        }

        usort($suppliersDisplay, function($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });

        return $this->render('@App/Admin/SupplierPrices/index.html.twig', ['suppliers' => $suppliersDisplay]);
    }

    public function foodomarketEditAction(Request $request)
    {
        $supplierId = $request->get('id');
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($supplierId);
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        $prices = $supplierProductPriceRepository->getPrices([$supplierId], null, null, [4], false);

        return $this->render('@App/Admin/SupplierPrices/foodomarket_edit.html.twig', [
            'supplier' => $supplier->getDisplayName(true),
            'prices' => $this->pricesToPricesDisplay($prices)
        ]);
    }

    /**
     * @param SupplierProductPrice[]|BlockedPrice[] $prices
     * @return array
     */
    private function pricesToPricesDisplay(array $prices)
    {
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        /** @var SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository */
        $supplierProductVariantInformationsRepository = $this->get('app.repository.supplier_product_variant_informations');
        $pricesDisplay = [];

        foreach ($prices as $price) {
            if ($price instanceof BlockedPrice) {
                $realPrice = $supplierProductPriceRepository->getByBlockedPrice($price);
            } else {
                $realPrice = $price;
            }

            $spvi = $supplierProductVariantInformationsRepository->getByPrice($realPrice);
            $variant = $price->getProductVariant();
            /** @var ProductImage $image */
            $image = $price->getProduct()->getImages()->first();

            $pricesDisplay[] = [
                'id' => $price->getId(),
                'reference' => $spvi->getReference() ?: '',
                'weight' => $spvi->getWeight(),
                'quantity' => $spvi->getUnitQuantity(),
                'um' => $spvi->getSalesUnit(),
                'product' => $variant->getInternalName(),
                'kgPrice' => bcdiv($price->getKgPrice(), 100, 2),
                'unitPrice' => bcdiv($price->getUnitPrice(), 100, 2),
                'validFrom' => $price->getValidFrom() ? $price->getValidFrom()->format('Y-m-d') : '',
                'validTo' => $price->getValidTo() ? $price->getValidTo()->format('Y-m-d') : '',
                'promo' => $price->isPromo(),
                'productId' => $price->getProduct()->getId(),
                'productImage' => $image ? $image->getPath() : 'shop-default-product.jpg'
            ];
        }

        usort($pricesDisplay, function($a, $b) {
            return strcasecmp($a['product'], $b['product']);
        });

        return $pricesDisplay;
    }

    public function restaurantEditAction(Request $request)
    {
        $supplierId = $request->get('id');
        /** @var SupplierRepository $supplierRepository */
        $supplierRepository = $this->get('app.repository.supplier');
        /** @var ProspectRepository $prospectRepository */
        $prospectRepository = $this->get('app.repository.prospect');
        /** @var Supplier $supplier */
        $supplier = $supplierRepository->find($supplierId);
        $prospects = $prospectRepository->getProspectsBoundToPartnerSupplier($supplier);

        $restaurantsDisplay = [];

        foreach ($prospects as $prospect) {
            $restaurants = $prospectRepository->getProspectRestaurants($prospect);

            foreach ($restaurants as $restaurant) {
                $blockedPrices = $this->get('app.repository.supplier_product_price')->getPrices([$supplier->getFacadeSupplier()], null, null, [$restaurant], false);

                $restaurantsDisplay[$restaurant->getId()] = [
                    'id' => $restaurant->getId(),
                    'name' => $restaurant->getName(),
                    'address' => $restaurant->getFormattedAddress(),
                    'createdAt' => $restaurant->getCreatedAt()->format('d/m/Y H:i:s'),
                    'nbBlockedPrice' => count($blockedPrices)
                ];
            }
        }

        return $this->render('@App/Admin/SupplierPrices/restaurant_edit.html.twig', [
            'supplierId' => $supplier->getId(),
            'supplier' => $supplier->getDisplayName(true),
            'restaurants' => $restaurantsDisplay
        ]);
    }

    public function historyAction(Request $request)
    {
        $priceId = $request->get('id');
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        $variantKnife = $this->get('app.service.variant_knife');

        /** @var SupplierProductPrice $currentPrice */
        $currentPrice = $supplierProductPriceRepository->find($priceId);
        $currentVariant = $currentPrice->getProductVariant();

        $synthesis = $variantKnife->getVariantHistorySynthesis($currentVariant, $currentPrice->getSupplier());

        /** @noinspection PhpUndefinedMethodInspection */
        return $this->render('@App/Admin/SupplierPrices/history.html.twig', [
            'synthesis' => array_reverse($synthesis),
            'product' => "({$synthesis[0]['id']}) {$synthesis[0]['name']}",
            'supplier' => $synthesis[0]['history'][0]->getSupplier()->getDisplayName(true)
        ]);
    }

    public function restaurantPricesEditAction(Request $request)
    {
        $restaurantId = $request->get('id');
        $supplierId = $request->get('supplierId');
        /** @var Restaurant $restaurant */
        $restaurant = $this->get('app.repository.restaurant')->find($restaurantId);
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($supplierId);
//        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
//        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
//        $restaurantPrices = $supplierProductPriceRepository->getPrices([$supplier->getFacadeSupplier()], null, null, [$restaurantId], false);
        /** @var BlockedPriceRepository $blockedPriceRepository */
        $blockedPriceRepository = $this->get('app.repository.blocked_price');

        $blockedPrices = $blockedPriceRepository->getBlockedPricesByRestaurant($restaurant);

        $blockedPrice = new BlockedPrice();

        $blockedPrice->setRestaurant($restaurant);
        $blockedPrice->setSupplier($supplier->getFacadeSupplier());

        return $this->render('@App/Admin/SupplierPrices/restaurant_prices_edit.html.twig', [
            'prices' => $this->pricesToPricesDisplay($blockedPrices),
            'restaurant' => $restaurant->getName(),
            'supplier' => $supplier->getDisplayName(),
            'supplierId' => $supplier->getId(),
            'blockedPriceForm' => $this->getBlockedPriceForm($blockedPrice)->createView()
        ]);
    }

    public function getBlockedPriceFormAction(Request $request)
    {
        $blockedPriceId = $request->get('id');
        $blockedPriceRepository = $this->get('app.repository.blocked_price');
        /** @var BlockedPrice $blockedPrice */
        $blockedPrice = $blockedPriceRepository->find($blockedPriceId);
        $form = $this->getBlockedPriceForm($blockedPrice);

        return $this->render('@App/Admin/SupplierPrices/_blocked_price_form.html.twig', ['blockedPriceForm' => $form->createView()]);
    }

    public function updateBlockedPriceAction(Request $request)
    {
        $blockedPriceId = $request->get('id');
        $blockedPriceRepository = $this->get('app.repository.blocked_price');
        /** @var SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository */
        $supplierProductVariantInformationsRepository = $this->get('app.repository.supplier_product_variant_informations');
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        /** @var BlockedPrice $blockedPrice */
        $blockedPrice = $blockedPriceRepository->find($blockedPriceId);
        $price = $supplierProductPriceRepository->getByBlockedPrice($blockedPrice);
        $spvi = $supplierProductVariantInformationsRepository->getByPrice($price);
        $form = $this->getBlockedPriceForm($blockedPrice);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager = $this->getDoctrine()->getManager();

                $manager->persist($blockedPrice);

                $price->setProduct($blockedPrice->getProduct());
                $price->setProductVariant($blockedPrice->getProductVariant());
                $price->setKgPrice($blockedPrice->getKgPrice());
                $price->setUnitPrice($blockedPrice->getUnitPrice());
                $price->setValidFrom($blockedPrice->getValidFrom());
                $price->setValidTo($blockedPrice->getValidTo());

                $manager->persist($price);

                $spvi->setProduct($blockedPrice->getProduct());
                $spvi->setProductVariant($blockedPrice->getProductVariant());

                $manager->persist($spvi);

                $manager->flush();
            } else {
                $errors = $form->getErrors(true);
                $messages = [];

                foreach ($errors as $error) {
                    $messages[] = $error->getMessage();
                }

                return $this->json(['ok' => false, 'message' => implode(', ', $messages)]);
            }
        }

        return $this->json(['ok' => true]);
    }

    public function searchVariantAction(Request $request)
    {
        $supplierId = $request->get('supplierId');
        $phrase = $request->query->get('phrase');
        $variantRepository = $this->get('app.repository.variant');

        $variants = $variantRepository->findByPhraseForSupplier($phrase, $supplierId);
        $ret = [];

        foreach ($variants as $variant) {
            $ret[] = [
                'name' => "{$variant->getFullDisplayName()} - {$variant->getExplicitContent()} - {$variant->getDisplayOrigin()}",
                'code' => $variant->getCode()
            ];
        }

        return $this->json($ret);
    }

    public function getWeightAndPriceByVariantCodeAndSupplierAction(Request $request)
    {
        $code = $request->get('code');
        $supplierId = $request->get('supplierId');
        /** @var ProductVariant $variant */
        $variant = $this->get('app.repository.variant')->findOneBy(['code' => $code]);
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');

        /** @var SupplierProductPrice $price */
        $price = $supplierProductPriceRepository->getPrices(
            [$supplierId],
            [$variant->getProduct()],
            [$variant], [4],
            false
        )[0];

        return $this->json([
            'kgPrice' => bcdiv($price->getKgPrice(), 100, 2),
            'unitPrice' => bcdiv($price->getUnitPrice(), 100, 2),
            'weight' => $variant->getWeight()
        ]);
    }

    public function deletePriceAction(Request $request)
    {
        /** @var BlockedPriceRepository $blockedPriceRepository */
        $blockedPriceRepository = $this->get('app.repository.blocked_price');
        /** @var BlockedPrice $blockedPrice */
        $blockedPrice = $blockedPriceRepository->find($request->get('id'));

        if ($blockedPrice === null) {
            return $this->json(['ok' => false, 'message' => "Impossible de trouver le prix"]);
        }

        /** @var SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository */
        $supplierProductVariantInformationsRepository = $this->get('app.repository.supplier_product_variant_informations');
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');

        $price = $supplierProductPriceRepository->getByBlockedPrice($blockedPrice);
        $spvi = $supplierProductVariantInformationsRepository->getByPrice($price);

        $supplierProductVariantInformationsRepository->remove($spvi);
        $supplierProductPriceRepository->remove($price);
        $blockedPriceRepository->remove($blockedPrice);

        return $this->json(['ok' => true]);
    }

    public function updatePriceAction(Request $request)
    {
        $priceId = $request->get('id');
        $kgPrice = $request->get('kgPrice');
        $unitPrice = $request->get('unitPrice');
        $validFrom = $request->get('validFrom', null);
        $validTo = $request->get('validTo', null);

        /** @var SupplierProductPriceRepository $supplierProductPriceRepositoy */
        $supplierProductPriceRepositoy = $this->get('app.repository.supplier_product_price');
        $supplierProductPriceManager = $this->get('app.manager.supplier_product_price');
        /** @var SupplierProductPrice $price */
        $price = $supplierProductPriceRepositoy->find($priceId);

        $price->setKgPrice(bcmul(str_replace(',', '.', $kgPrice), 100));
        $price->setUnitPrice(bcmul(str_replace(',', '.', $unitPrice), 100));

        $spviManager = $this->get('app.manager.supplier_product_variant_informations');
        $spvi = $this->get('app.repository.supplier_product_variant_informations')->getByPrice($price);

        if ($spvi !== null) {
            // SPVI
            $weight = $request->get('weight', $spvi->getWeight());
            $unitQuantity = $request->get('quantity', $spvi->getUnitQuantity());
            $salesUnit = $request->get('um', $spvi->getSalesUnit());

            $spvi->setWeight($weight);
            $spvi->setUnitQuantity($unitQuantity);
            $spvi->setSalesUnit($salesUnit);
        }

        if ($validFrom !== null && $validTo !== null) {
            $price->setValidFrom(new DateTime($validFrom));
            $price->setValidTo(new DateTime($validTo));
        }

        try {
            $supplierProductPriceManager->flush($price);

            if ($spvi !== null) {
                $spviManager->flush($spvi);
            }
        } catch (OptimisticLockException $e) {
            return $this->json(['ok' => false, 'message' => $e->getMessage()]);
        }

        return $this->json(['ok' => true]);
    }

    public function createBlockedPriceAction(Request $request)
    {
        $restaurantId = $request->get('id');
        $supplierId = $request->get('supplierId');
        /** @var Restaurant $restaurant */
        $restaurant = $this->get('app.repository.restaurant')->find($restaurantId);
        /** @var Supplier $supplier */
        $supplier = $this->get('app.repository.supplier')->find($supplierId);

        $blockedPrice = new BlockedPrice();

        $blockedPrice->setRestaurant($restaurant);
        $blockedPrice->setSupplier($supplier);

        $form = $this->getBlockedPriceForm($blockedPrice);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $manager = $this->getDoctrine()->getManager();
                /** @var BlockedPrice $blockedPrice */
                $blockedPrice = $form->getData();
                $variant = $blockedPrice->getProductVariant();

                $spp = new SupplierProductPrice();
                $spvi = new SupplierProductVariantInformations();

                $spp->setProduct($blockedPrice->getProduct());
                $spp->setProductVariant($blockedPrice->getProductVariant());
                $spp->setRestaurant($blockedPrice->getRestaurant());
                $spp->setSupplier($blockedPrice->getSupplier());
                $spp->setChannelCode($blockedPrice->getSupplier()->getChannel()->getCode());
                $spp->setKgPrice($blockedPrice->getKgPrice());
                $spp->setUnitPrice($blockedPrice->getUnitPrice());
                $spp->setValidFrom($blockedPrice->getValidFrom());
                $spp->setValidTo($blockedPrice->getValidTo());
                $spp->setMercurialeId(0);

                $spvi->setSupplier($supplier);
                $spvi->setProduct($blockedPrice->getProduct());
                $spvi->setProductVariant($variant);
                $spvi->setManuallyCreated(false);
                $spvi->setSalesUnit($variant->getOptionSupplierSalesUnit()->getValue());
                $spvi->setUnitQuantity($variant->getOptionUnitQuantity()->getValue());
                $spvi->setWeight($blockedPrice->getProductVariant()->getWeight());
                $spvi->setContent(0);
                $spvi->setReference('');
                $spvi->setProductLabel($variant->getProduct()->getName());
                $spvi->setVariantLabel('-');
                $spvi->setDisplayName($variant->getProduct()->getName());

                $manager->persist($blockedPrice);
                $manager->persist($spp);
                $manager->persist($spvi);

                $manager->flush();

                return $this->json(['ok' => true]);
            }
        }

        return $this->json(['ok' => false]);
    }

    /**
     * @param BlockedPrice $blockedPrice
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getBlockedPriceForm(BlockedPrice $blockedPrice)
    {
        $options = $this->get('sylius.repository.product_option')->findAll();

        if ($blockedPrice->getId() === null) {
            $action = $this->generateUrl('app_admin_prices_blocked_price_create', ['id' => $blockedPrice->getRestaurant()->getId(), 'supplierId' => $blockedPrice->getSupplier()->getId()]);
        } else {
            $action = $this->generateUrl('app_admin_supplier_prices_blocked_price_update', ['id' => $blockedPrice->getId()]);
        }

        $formBuilder = $this->createFormBuilder($blockedPrice, [
            'action' => $action,
            'method' => 'POST'
        ])
            ->add('productVariant', ProductVariantAutocompleteChoiceType::class, ['label' => 'Variante', 'required' => true])
            ->add('kgPrice', MoneyType::class, ['required' => true, 'label' => 'Prix kilo', 'attr' => ['class' => 'kg-price']])
            ->add('unitPrice', MoneyType::class, ['required' => true, 'label' => 'Prix unitaire', 'attr' => ['class' => 'unit-price']])
            ->add('validFrom', DateType::class, ['required' => true, 'label' => 'Début de validité', 'widget' => 'single_text',])
            ->add('validTo', DateType::class, ['required' => true, 'label' => 'Fin de validité', 'widget' => 'single_text',])
            ->add('weight', HiddenType::class, ['mapped' => false, 'data' => $blockedPrice->getProductVariant() ? $blockedPrice->getProductVariant()->getWeight() : null, 'attr' => ['class' => 'weight']])
            ->add('optionValues', CustomProductOptionValueCollectionType::class, ['label' => false, 'options' => $options, 'required' => false, 'requiredOptions' => []])
            ->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
                /** @var SupplierProductPrice $price */
                $price = $event->getData();

                $price->setProduct($price->getProductVariant()->getProduct());

                $event->setData($price);
            })
        ;

        return $formBuilder->getForm();
    }

    public function benchAction(Request $request)
    {
        $supplierId = $request->get('id');
        /** @var SupplierRepository $supplierRepository */
        $supplierRepository = $this->get('app.repository.supplier');
        /** @var Supplier $supplier */
        $supplier = $supplierRepository->find($supplierId);
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        $prices = $supplierProductPriceRepository->getPrices([$supplierId], null, null, [4], false);
        $bench = [];

        $partners = $supplierRepository->getPartners();

        /** @var ProductUtils $productUtils */
        $productUtils = $this->get('app.utils.product');

        foreach ($prices as $i => $price) {
            $bench[0][0] = $supplier->getDisplayName(true);
            $bench[$i + 1][0] = $price->getProductVariant()->getInternalName();

            $j = 1;

            foreach ($partners as $partner) {
                if ($supplier->getId() != $partner->getId()) {
                    $bench[0][$j] = $partner->getDisplayName(true);

                    $partnerPrice = $supplierProductPriceRepository->findOneBy([
                        'supplier' => $partner,
                        'restaurant' => 4,
                        'productVariant' => $price->getProductVariant(),
                    ]);

                    // Fetch a similar variant price
                    if ($partnerPrice === null) {
                        $partnerPrice = $productUtils->getCheapestSimilarVariantForSupplierAndRestaurantIso($price->getProductVariant(), $partner, 4);
                    }

                    if ($partnerPrice !== null) {
                        $bench[$i + 1][$j] = $partnerPrice->getProductVariant()->getInternalName();
                    } else {
                        $bench[$i + 1][$j] = '';
                    }

                    ++$j;
                }
            }
        }

        return $this->render('@App/Admin/SupplierPrices/bench.html.twig', [
            'supplier' => $supplier->getDisplayName(true),
            'bench' => $bench
        ]);
    }
}