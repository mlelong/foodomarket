<?php


namespace AppBundle\Admin\Controller;


use AppBundle\Entity\ProspectSource;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProspectSourceController extends ResourceController
{
    public function bulkExportAction(Request $request): Response
    {
        $ids = $request->get('ids');

        if (strlen($ids)) {
            $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
            /** @var ProspectSource[] $prospectsSource */
            $prospectsSource = $this->resourcesCollectionProvider->get($configuration, $this->repository);
        } else {
            /** @var ProspectSource[] $prospectsSource */
            $prospectsSource = $this->repository->findAll();
        }

        $filename = 'Export Prospect source du ' . date('Y-m-d') . '.csv';

        return new StreamedResponse(function() use($prospectsSource) {
            $stdout = fopen('php://memory', 'r+');

            fputcsv($stdout, ['ID du prospect', 'Date', 'Source', 'Code postal', 'action']);

            /** @var ProspectSource $prospect */
            foreach ($prospectsSource as $prospect) {
                fputcsv($stdout, [
                    $prospect->getProspect() ? $prospect->getProspect()->getEmail() : "Prospect supprimé",
                    $prospect->getCreatedAt()->format('Y-m-d H:i:s'),
                    $prospect->getSource(),
                    $prospect->getProspect() ? $prospect->getProspect()->getZipCode() : '',
                    $prospect->getAction()
                ]);
            }

            rewind($stdout);
            echo stream_get_contents($stdout);
        }, 200, ['Content-Type' => 'text/csv', 'Content-Disposition' => "attachment; filename=$filename"]);
    }
}