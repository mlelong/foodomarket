<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Datatables\ImportLineDatatable;
use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use AppBundle\Form\ImportLineType;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\View\View;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Resource\Exception\UpdateHandlingException;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ImportController extends ResourceController
{
    public function ajaxLoadProductsByTaxonCode(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return  new JsonResponse(['error' => true, 'message' => 'Only XMLHttpRequest allowed']);
        }

        $taxonCode = $request->get('taxon-code');
        /** @var ProductInterface[] $products */
        $products = $this->get('sylius.repository.product')->findMatching($taxonCode);
        $data = [];

        foreach ($products as &$product) {
            $data[] = ['id' => $product->getId(), 'name' => $product->getName()];
        }

        return new JsonResponse(['error' => false, 'data' => $data]);
    }

    public function ajaxLoadVariantsByProductId(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return  new JsonResponse(['error' => true, 'message' => 'Only XMLHttpRequest allowed']);
        }

        $productId = $request->get('product-id');
        /** @var ProductInterface[] $products */
        $products = $this->get('sylius.repository.product')->getAllProductVariants($productId);
        $data = [];

        foreach ($products as &$product) {
            $data[] = ['id' => $product->getId(), 'name' => $product->getName()];
        }

        return new JsonResponse(['error' => false, 'data' => $data]);
    }

    public function updateLineAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['error' => true, 'status' => 'waiting', 'message' => "Only XMLHttpRequest allowed"]);
        }

        /** @var ImportLine $importLine */
        $importLine = $this->get('app.repository.import_line')->find($request->get('importline'));

        $import = $importLine->getImport();

        $formLineBuilder = $this->container->get('form.factory')->createBuilder(ImportLineType::class, $importLine, array());
        $formLineBuilder->setAction($this->generateUrl('app_admin_importline_update', ['import' => $import->getId(), 'importline' => $importLine->getId()]));
        $formLineBuilder->setMethod('POST');
        $form = $formLineBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($importLine->getProduct() && $importLine->getTaxon() && $importLine->getStatus() != ImportLine::STATUS_IMPORTED) {
                    $importLine->setStatus(ImportLine::STATUS_MATCHED_WITH_CATALOG);
                }

                $this->manager->persist($importLine);
                $this->manager->flush();

                return new JsonResponse(['error' => false, 'status' => $importLine->getStatus(), 'form' => $this->renderView('@App/Admin/Form/ImportLine.html.twig', ['form' => $form->createView()])]);
            } else {
                return new JsonResponse(['error' => true, 'status' => $importLine->getStatus(), 'form' => $this->renderView('@App/Admin/Form/ImportLine.html.twig', ['form' => $form->createView()])]);
            }
        }

        return new JsonResponse(['error' => false, 'status' => 'waiting']);
    }

    public function validateLineWithoutControlAction(Request $request) {
        $lineId = $request->get('importline');
        /** @var ImportLine $importLine */
        $importLine = $this->get('app.repository.import_line')->find($lineId);
        $productManager = $this->get('app.product.manager');

        try {
            $product = $productManager->importProduct($importLine->getImport(), $importLine);

            if ($product !== null) {
                $importLine->setProduct($product);
            } else {
                $importLine->setStatus(ImportLine::STATUS_AWAITING_CHECK);
            }

            $this->get('doctrine')->getManager()->flush();

            return new JsonResponse(['error' => false]);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => true, 'message' => $e->getMessage()]);
        }
    }

    public function validateLineAction(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(['error' => true, 'status' => 'waiting', 'message' => "Only XMLHttpRequest allowed"]);
        }

        /** @var ImportLine $importLine */
        $importLine = $this->get('app.repository.import_line')->find($request->get('importline'));

        $import = $importLine->getImport();

        $formLineBuilder = $this->container->get('form.factory')->createBuilder(ImportLineType::class, $importLine, array());
        $formLineBuilder->setAction($this->generateUrl('app_admin_importline_validate', ['import' => $import->getId(), 'importline' => $importLine->getId()]));
        $formLineBuilder->setMethod('POST');
        $form = $formLineBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (true || $importLine->getStatus() != ImportLine::STATUS_IMPORTED) {
                    $product = $this->container->get('app.product.manager')->importProduct($import, $importLine);

                    if ($product !== null) {
                        $importLine->setProduct($product);
                    } else {
                        $importLine->setStatus(ImportLine::STATUS_AWAITING_CHECK);
                    }

                    $this->manager->persist($importLine);
                    $this->manager->flush();
                }

                return new JsonResponse(['error' => false, 'status' => $importLine->getStatus(), 'id' => $importLine->getId()]);
            } else {
                return new JsonResponse(['error' => true, 'status' => $importLine->getStatus(), 'form' => $this->renderView('@App/Admin/Form/ImportLine.html.twig', ['form' => $form->createView()])]);
            }
        }

        return new JsonResponse(['error' => false, 'status' => 'waiting']);
    }

    /**
     * @param ImportLine $importLine
     * @return FormInterface
     */
    private function getImportLineForm(ImportLine $importLine) {
        $formLineBuilder = $this->container->get('form.factory')->createBuilder(ImportLineType::class, $importLine, array());
        $formLineBuilder->setAction($this->generateUrl('app_admin_importline_validate', ['import' => $importLine->getImport()->getId(), 'importline' => $importLine->getId()]));
        $formLineBuilder->setMethod('POST');

        return $formLineBuilder->getForm();
    }

    public function refreshTaxon(Request $request)
    {
        $importId = $request->get('import');
        $productManager = $this->get('app.product.manager');

        $productManager->refreshTaxonAndProduct($importId);
        $this->get('doctrine')->getManager()->flush();

        return $this->redirectToRoute('app_admin_import_update', ['id' => $importId]);
    }

    /**
     * Returns the import line form for a specified import line
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxLoadImportLine(Request $request)
    {
        $importLineRepository = $this->manager->getRepository('AppBundle:ImportLine');
        $importLine = $importLineRepository->find($request->get('id'));

        $form = $this->renderView('@App/Admin/Form/ImportLine.html.twig', ['form' => $this->getImportLineForm($importLine)->createView()]);

        return new JsonResponse(['error' => false, 'form' => $form]);
    }

    public function updateAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Import $resource */
        $resource = $this->findOr404($configuration);

        /** @var ImportLineDatatable $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(ImportLineDatatable::class);
        $datatable->setTaxonRepository($this->get('sylius.repository.taxon'));
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);

            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            /** @var QueryBuilder $qb */
            $qb = $datatableQueryBuilder->getQb();
            $qb->where('importline.import = :import');
            $qb->setParameter('import', $resource);

            return $responseService->getResponse();
        }

//        $form = $this->get('app.resource_controller.import_form_factory')->create($configuration, $resource);
        $form = $this->resourceFormFactory->create($configuration, $resource);

        if (in_array($request->getMethod(), ['POST', 'PUT', 'PATCH'], true) && $form->handleRequest($request)->isValid()) {
            $resource = $form->getData();

            /** @var ResourceControllerEvent $event */
            $event = $this->eventDispatcher->dispatchPreEvent(ResourceActions::UPDATE, $configuration, $resource);

            if ($event->isStopped() && !$configuration->isHtmlRequest()) {
                throw new HttpException($event->getErrorCode(), $event->getMessage());
            }
            if ($event->isStopped()) {
                $this->flashHelper->addFlashFromEvent($configuration, $event);

                if ($event->hasResponse()) {
                    return $event->getResponse();
                }

                return $this->redirectHandler->redirectToResource($configuration, $resource);
            }

            try {
                $this->resourceUpdateHandler->handle($resource, $configuration, $this->manager);
            } catch (UpdateHandlingException $exception) {
                if (!$configuration->isHtmlRequest()) {
                    return $this->viewHandler->handle(
                        $configuration,
                        View::create($form, $exception->getApiResponseCode())
                    );
                }

                $this->flashHelper->addErrorFlash($configuration, $exception->getFlash());

                return $this->redirectHandler->redirectToReferer($configuration);
            }

            $postEvent = $this->eventDispatcher->dispatchPostEvent(ResourceActions::UPDATE, $configuration, $resource);

            if (!$configuration->isHtmlRequest()) {
                $view = $configuration->getParameters()->get('return_content', false) ? View::create($resource, Response::HTTP_OK) : View::create(null, Response::HTTP_NO_CONTENT);

                return $this->viewHandler->handle($configuration, $view);
            }

            $this->flashHelper->addSuccessFlash($configuration, ResourceActions::UPDATE, $resource);

            if ($postEvent->hasResponse()) {
                return $postEvent->getResponse();
            }

            return $this->redirectHandler->redirectToResource($configuration, $resource);
        }

        if (!$configuration->isHtmlRequest()) {
            return $this->viewHandler->handle($configuration, View::create($form, Response::HTTP_BAD_REQUEST));
        }

        $this->eventDispatcher->dispatchInitializeEvent(ResourceActions::UPDATE, $configuration, $resource);

        $view = View::create()
            ->setData([
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'resource' => $resource,
                $this->metadata->getName() => $resource,
                'form' => $form->createView(),
                'datatable' => $datatable
            ])
            ->setTemplate($configuration->getTemplate(ResourceActions::UPDATE . '.html'))
        ;

        return $this->viewHandler->handle($configuration, $view);
    }


    public function validateAction(Request $request)
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

//        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var Import $import */
        $import = $this->findOr404($configuration);

        $productManager = $this->get('app.product.manager');

        /*
         * DO NOT CHECK  IMPORT AS WE WILL IMPORT ONLY MATCHED LINES
         *
        $return = $productManager->isImportReady($import);
        if ($return !== true) {
            $this->addFlash('error', 'Document data is not ready, check following products :');

            foreach($return as $productName) {
                $this->addFlash('error', $productName);
            }

            return $this->redirectToRoute('app_admin_import_index');
        }
        */

        $this->getDoctrine()->getConnection()->beginTransaction();
        //$this->getDoctrine()->getConnection()->setAutoCommit(false);

        try {
            foreach ($import->getLines() as $line) {
                if ($line->getStatus() == ImportLine::STATUS_MATCHED_WITH_CATALOG) {
                    $this->container->get('app.product.manager')->importProduct($import, $line);
                }
            }
            $import->setStatus(Import::STATUS_IMPORTED);
            $this->getDoctrine()->getManager()->flush();

            $this->getDoctrine()->getConnection()->commit();

            $this->addFlash('warning', 'The validation has been processed correctly');

        } catch (\Exception $e) {

            $this->getDoctrine()->getConnection()->rollBack();

            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('app_admin_import_index');
    }
}
