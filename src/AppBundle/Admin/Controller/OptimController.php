<?php

namespace AppBundle\Admin\Controller;

use AppBundle\DataTransformer\ShoppingItemsToOptimInputMatrixTransformer;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Services\RedisCacheManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OptimController extends Controller
{
    public function indexAction()
    {
        $manager = $this->getDoctrine()->getManager();

        $restaurants = $manager->getRepository(Restaurant::class)->findAll();

        return $this->render('@App/Admin/Optim/index.html.twig', [
            'restaurants' => $restaurants
        ]);
    }

    public function searchSuppliersAction(Request $request)
    {
        $terms = $request->get('terms');
        $qb = $this->get('app.repository.supplier')->createQueryBuilder('s');

        /** @var Supplier[] $suppliers */
        $suppliers = $qb
            ->where($qb->expr()->like('s.name', ':terms'))
            ->setParameter('terms', "%$terms%")
            ->getQuery()
            ->getResult()
        ;

        $result = ['results' => []];

        foreach ($suppliers as $supplier) {
            if ($supplier->getEnabled()) {
                $key = 'supplier';
                $name = 'Fournisseur standard';
            } else {
                $key = 'partner-supplier';
                $name = 'Fournisseur partenaire';
            }

            if (!isset($result['results'][$key])) {
                $result['results'][$key] = ['name' => $name, 'results' => []];
            }

            $result['results'][$key]['results'][] = [
                'title' => $supplier->getName(),
                'franco' => $supplier->getFreePort(),
                'frais' => $supplier->getShippingCost(),
                'supplierId' => $supplier->getId()
            ];
        }

        return new JsonResponse($result);
    }

    private function getRestaurantProducts($restaurant)
    {
        $qb = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(SupplierProductVariantInformations::class)
            ->createQueryBuilder('spvi')
        ;

        return $qb
            ->select('spvi.displayName as displayName')
            ->addSelect('spvi.productLabel as productLabel')
            ->addSelect('product.id AS productId')
            ->addSelect('variant.id AS variantId')
            ->addSelect('supplier.id AS supplierId')
            ->addSelect('supplier.name AS supplierName')
            ->addSelect('pt.name AS productName')
            ->addSelect('ov.code AS umOptionValue')
            ->addSelect('ov3.code AS ucOptionValue')
            ->addSelect('ov2.code AS unitQuantity')
            ->addSelect('price.kgPrice AS kgPrice')
            ->addSelect('price.unitPrice AS unitPrice')
            ->join('spvi.product', 'product')
            ->join('spvi.productVariant', 'variant')
            ->join('spvi.supplier', 'supplier')
            ->join(SupplierProductPrice::class, 'price', 'WITH', 'price.supplier = spvi.supplier AND price.restaurant = :restaurant AND price.productVariant = variant')
            ->join('product.translations', 'pt')
            ->leftJoin('variant.optionValues', 'ov')
            ->leftJoin('variant.optionValues', 'ov2')
            ->leftJoin('variant.optionValues', 'ov3')
            ->leftJoin('ov.option', 'ovv')
            ->leftJoin('ov2.option', 'ovv2')
            ->leftJoin('ov3.option', 'ovv3')
            ->where('pt.locale = :locale')
            ->andWhere('ovv.code = \'UM\'')
            ->andWhere('ovv2.code = \'UQ\'')
            ->andWhere('ovv3.code = \'UC\'')
            ->andWhere('variant.enabled = 1')
            ->andWhere('product.enabled = 1')
            ->setParameter('restaurant', $restaurant)
            ->setParameter('locale', 'fr_FR')
            ->groupBy('spvi')
            ->addGroupBy('product')
            ->addGroupBy('variant')
            ->addGroupBy('supplier')
            ->addGroupBy('pt')
            ->addGroupBy('ov')
            ->addGroupBy('ov2')
            ->addGroupBy('ov3')
            ->addGroupBy('price')
            ->getQuery()
            ->getScalarResult()
        ;
    }

    public function searchRestaurantProductsAction(Request $request)
    {
        $terms = $request->get('terms');
        $restaurant = $request->get('restaurant');

        $spvis = $this->get('app.redis.cache.manager')->get(
            str_replace('{restaurantId}', $restaurant, RedisCacheManager::CACHE_KEY_ADMIN_OPTIM_PRODUCTS_PATTERN),
            86400,
            function() use($restaurant) {
                return $this->getRestaurantProducts($restaurant);
            }
        );

        $result = [
            'results' => []
        ];

        foreach ($spvis as $spvi) {
            if (preg_match("/$terms/i", $spvi['productName'])
                || preg_match("/$terms/i", $spvi['displayName'])
                || preg_match("/$terms/i", $spvi['productLabel'])) {

                $ucOptionValue = $spvi['ucOptionValue'];

                switch ($ucOptionValue) {
                    case 'ORDER_KG':
                        $orderUnit = 'KG';
                        break ;
                    case 'ORDER_PC':
                        $orderUnit = 'PC';
                        break ;
                    case 'ORDER_KGPC':
                    default:
                        $umOptionValue = $spvi['umOptionValue'];

                        switch ($umOptionValue) {
                            case 'KG':
                            case 'L':
                                $orderUnit = 'KG';
                                break ;
                            case 'PC':
                            default:
                                $orderUnit = 'PC';
                                break ;
                        }

                        break ;
                }

                $uqOptionValue = $spvi['unitQuantity'];

                if (!is_numeric($uqOptionValue)) {
                    $uqOptionValue = 1;
                }

                $key = "supplier-{$spvi['supplierId']}";

                if (!isset($result['results'][$key])) {
                    $result['results'][$key] = [
                        'name' => $spvi['supplierName'],
                        'results' => []
                    ];
                }

                $result['results'][$key]['results'][] = [
                    'title' => $spvi['displayName'],
//                    'image' => '',
                    'price' => $orderUnit == 'PC'
                        ? bcdiv($spvi['unitPrice'] / $uqOptionValue, 100, 2) . "€/$orderUnit"
                        : bcdiv($spvi['kgPrice'], 100, 2) . "€/$orderUnit",
                    'variantId' => $spvi['variantId'],
                    'supplierId' => $spvi['supplierId'],
                    'supplierName' => $spvi['supplierName']
                ];
            }
        }

        return new JsonResponse($result);
    }

    public function optimizeAction(Request $request)
    {
        $restaurantId = $request->get('restaurant');
        $nbSuppliers = $request->get('nb_suppliers');
        $suppliers = $request->get('suppliers');
        $variants = $request->get('variants');

//        file_put_contents($this->getParameter('kernel.root_dir') . '/../test.json', json_encode(['restaurant' => $restaurantId, 'nb_suppliers' => $nbSuppliers, 'suppliers' => $suppliers, 'variants' => $variants]));

//        $json = json_decode('{"restaurant":"1","nb_suppliers":"1","suppliers":[{"id":"17","franco":"50","frais":"60"},{"id":"18","franco":"50","frais":"60"},{"id":"22","franco":"50","frais":"60"},{"id":"6","franco":"50","frais":"60"}],"variants":[{"variantId":"1381","supplierId":"18","quantity":"2","unit":"KG"},{"variantId":"1077","supplierId":"18","quantity":"1","unit":"KG"},{"variantId":"1087","supplierId":"18","quantity":"10","unit":"KG"},{"variantId":"1395","supplierId":"18","quantity":"2","unit":"KG"},{"variantId":"1064","supplierId":"18","quantity":"5","unit":"KG"},{"variantId":"1082","supplierId":"18","quantity":"1","unit":"KG"},{"variantId":"692","supplierId":"18","quantity":"4","unit":"KG"},{"variantId":"694","supplierId":"18","quantity":"1","unit":"KG"},{"variantId":"1379","supplierId":"18","quantity":"2","unit":"KG"},{"variantId":"1822","supplierId":"18","quantity":"1","unit":"KG"},{"variantId":"688","supplierId":"18","quantity":"1","unit":"KG"},{"variantId":"1364","supplierId":"18","quantity":"3","unit":"KG"},{"variantId":"1031","supplierId":"18","quantity":"4","unit":"PC"}]}', true);
//
//        $restaurantId = $json['restaurant'];
//        $nbSuppliers = $json['nb_suppliers'];
//        $suppliers = $json['suppliers'];
//        $variants = $json['variants'];

        $manager = $this->getDoctrine()->getManager();
        $sppRepository = $manager->getRepository(SupplierProductPrice::class);

        $restaurant = $manager->find(Restaurant::class, $restaurantId);

        $shoppingList = new RestaurantStock();
        $shoppingList->setRestaurant($restaurant);

        $customSuppliers = [];

        foreach ($suppliers as $supplierArray) {
            $supplier = $manager->find(Supplier::class, $supplierArray['id']);

            $supplier->setFreePort($supplierArray['franco']);
            $supplier->setShippingCost($supplierArray['frais']);

            $customSuppliers[] = $supplier;
        }

        $originalTotal = 0;
        $replacementTotal = 0;

        foreach ($variants as $variantArray) {
            $shoppingListItem = new RestaurantStockItem();

            $shoppingListItem->setProductVariant($manager->find(ProductVariant::class, $variantArray['variantId']));
            $shoppingListItem->setSupplier($manager->find(Supplier::class, $variantArray['supplierId']));
            $shoppingListItem->setStock($variantArray['quantity']);
            $shoppingListItem->setStockUnit($variantArray['unit']);

            /** @var SupplierProductPrice $price */
            $price = $sppRepository->findOneBy([
                'productVariant' => $shoppingListItem->getProductVariant(),
                'supplier' => $shoppingListItem->getSupplier(),
                'restaurant' => $shoppingList->getRestaurant()
            ]);

            switch ($shoppingListItem->getUnit()) {
                case 'KG':
                    $originalTotal += $price->getKgPrice() * $shoppingListItem->getQuantity();
                    break ;
                case 'PC':
                    $originalTotal += $price->getUnitPrice() * $shoppingListItem->getQuantity();
                    break ;
            }

            $shoppingList->addItem($shoppingListItem);
        }

        /** @var ShoppingItemsToOptimInputMatrixTransformer $transformer */
        $transformer = $this->get('app.transformer.shopping_items_to_optim_input_matrix');
        $transformer->setRestaurant($shoppingList->getRestaurant());

        $transformer->setMaxSuppliers($nbSuppliers);
        // Needed to override fees
        $transformer->setSuppliers($customSuppliers);

        $matrix = $transformer->transform($shoppingList->getItems());

        $url = $this->getParameter('optim_algo_url');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['json' => $matrix['encoded_input_matix']]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $curlInfo = curl_getinfo($ch);

        $replacement = [];

        if ($curlInfo['http_code'] == 200) {
            $data = json_decode($result, true);

            $newShoppingList = $transformer->toShoppingList($data, $matrix, $replacementTotal, true);
            $items = $newShoppingList->getItems();

            foreach ($items as $item) {
                $note = explode('-', $item->getNote());
                $index = $note[0];
                $wrightRestaurant = $note[2];

                $variant = $item->getProductVariant();

                $userPrice = $manager->getRepository(SupplierProductPrice::class)->findOneBy([
                    'productVariant' => $variant,
                    'supplier' => $item->getSupplier(),
                    'restaurant' => $wrightRestaurant
                ]);

                $replacement[] = [
                    'supplier' => $item->getSupplier()->getName(),
                    'variant' => "{$variant->getDisplayName()} ({$variant->getId()})",
                    'price' => $item->getUnit() == 'KG' ? bcdiv($userPrice->getKgPrice(), 100, 2) . '€/KG' : bcdiv($userPrice->getUnitPrice(), 100, 2) . '€/PC',
                    'quantity' => $item->getQuantity(),
                    'originalId' => "s-{$variants[$index]['supplierId']}-v-{$variants[$index]['variantId']}"
                ];
            }
        }

//        // Ajoute un au hasard pour voir, à supprimer après
//        $replacement[] = $replacement[random_int(0, count($replacement) - 1)];

        return new JsonResponse([
            'ok' => true,
            'nb_suppliers' => $nbSuppliers,
            'suppliers' => $suppliers,
            'variants' => $variants,
            'replacement' => $replacement,
            'originalTotal' => bcdiv($originalTotal, 100, 2) . '€',
            'replacementTotal' => number_format($replacementTotal, 2, '.', ' ') . '€'
        ]);
    }
}
