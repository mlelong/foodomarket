<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Admin\Form\Type\SupplierAccountLogSupplierType;
use AppBundle\Entity\SupplierAccountLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SupplierAccountLogSupplierController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var SupplierAccountLog $supplierAccountLog */
        $supplierAccountLog = $this->getDoctrine()->getRepository(SupplierAccountLog::class)->findOneBy([
            'hash' => $request->get('hash')
        ]);

        if ($supplierAccountLog === null) {
            return $this->render('@App/Admin/SupplierAccountLog/404.html.twig');
        }

        $form = $this->getForm($supplierAccountLog);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplierAccountLog->setHash(null);

            $this->getDoctrine()->getManager()->flush();

            $this->get('app.supplier_account_log.listener')->handle($supplierAccountLog);

            return $this->render('@App/Admin/SupplierAccountLog/confirm.html.twig');
        }

        return $this->render('@App/Admin/SupplierAccountLog/index.html.twig', ['form' => $form->createView()]);
    }

    private function getForm(SupplierAccountLog $supplierAccountLog)
    {
        $form = $this->createForm(SupplierAccountLogSupplierType::class, $supplierAccountLog, [
            'action' => $this->generateUrl('app_admin_supplier_account_log_public_index', [
                'hash' => $supplierAccountLog->getHash()
            ])
        ]);

        return $form;
    }
}