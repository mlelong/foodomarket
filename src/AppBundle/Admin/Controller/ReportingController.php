<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Entity\InvoiceDeliveryNote;
use DateTime;
use Exception;
use AppBundle\Entity\Invoice;
use phpDocumentor\Reflection\Types\AbstractList;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Prospect;
use Symfony\Component\HttpFoundation\Response;
use IntlDateFormatter;


class ReportingController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function indexAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        // Récupération des factures
        /** @var Invoice[] $invoices */
        $invoices = $em->getRepository(Invoice::class)
            ->findAll();

        $form = $this->getForm();
        $form->handleRequest($request);

        /* Partie formulaire des dates */
        // Configurations des dates
        $todayDate = new DateTime();

        $endDate = new DateTime("last day of this month 23:59:59");
        $startDate = new DateTime("first day of 0 month ago 00:00:00");

        // Récupération du formulaire des dates
        if ($form->isSubmitted() && $form->isValid()) {
            $startDate = $form->get('startDate')->getData();
            $endDate = $form->get('endDate')->getData();
        }

        $startDateTxt = $startDate->format('Y-m-d');
        $endDateTxt = $endDate->format('Y-m-d');
        $startDateFormatted = $this->dateFormat($startDate);
        $endDateFormatted = $this->dateFormat($endDate);
        /*
         * Fin partie formulaire des dates
         */

        // Récupération des repo pour y appeler des fonctions
        $repoDelivery = $em->getRepository(InvoiceDeliveryNote::class);

        // Ce tableau va reccueillir toutes les données à passer au twig
        $allData = [];
        foreach($invoices as $invoice){
            /** @var Prospect $prospects */
            $prospect = $invoice->getProspect();
            // Si l'id n'est pas déjà dans la table $allData, remplir chaque ligne de $data
            if (!isset($allData[$prospect->getId()])) {
                $data = [
                    'idProspect' => $prospect->getId(),
                    'nom' => $prospect->getUsername(),
                    'restaurant' => $prospect->getRestaurantName(),
                    'phone' => $prospect->getPhone(),
                    'mobile' => $prospect->getMobile(),
                    'totalAchat' => 0,
                    'nbDeliveryTotal' => $repoDelivery->countDelivery($prospect), // Nombre total livraison
                    'nbDeliveryBetweenDat' => $repoDelivery->countDeliveryBetweenDate($prospect, $startDate, $endDate), // Nombre de livraisons période donnée
                    'lastDateDelivery' => $repoDelivery->lastDateDelivery($prospect), // Date de la dernière livraison
                    'averageDeliveryBetDate' => $repoDelivery->averageDeliveryBetweenDate($prospect, $startDate, $endDate), // Moyenne des livraisons entre deux dates
                    'deliveryDates' => [], // Array de toutes les dates de livraisons de chaque prospect
                    'avgFrequency' => 0, // Fréquence de livraison
                    'riskLevel' => 0, // Niveau de risque
                    'dayWithoutDelivery' => 0 // x jour(s) sans livraison
                ];
                $allData[$prospect->getId()] = $data; // Remplissage de l'array final
            }
            $allData[$prospect->getId()]['totalAchat'] += $invoice->getTotal();
            //$allData[$prospect->getId()]['nombreTotalFacture'] += 1;

            // Récupération de toutes les dates de livraisons de chaque prospect
            foreach ($invoice->getInvoiceDeliveryNotes() as $deliveryNote) {
                $allData[$prospect->getId()]['deliveryDates'][] = $deliveryNote->getDate();
            }
        }

        foreach ($allData as &$data) {

            // Tri des dates dans l'ordre croissant
            usort($data['deliveryDates'], function(DateTime $a, DateTime $b) {
                return $a > $b;
            });

            $frequencyBetweenOrders = [];

            // Prend la taille totale de l'array des dates
            $nbDeliveryDates = count($data['deliveryDates']);

            // Compare la différence de jours entre chaque commande/livraison
            for ($i = 1; $i < $nbDeliveryDates; ++$i) {
                /** @var DateTime $previousDate */
                $previousDate = $data['deliveryDates'][$i - 1];
                /** @var DateTime $currentDate */
                $currentDate = $data['deliveryDates'][$i];
                $frequencyBetweenOrders[$i - 1] = $currentDate->diff($previousDate)->d;
            }

            if (($count = count($frequencyBetweenOrders)+1) > 0) {
                $data['avgFrequency'] = array_sum($frequencyBetweenOrders) / $count;
            } else { // Si division à 0
                $data['avgFrequency'] = -1;
            }

            $todayDate = new DateTime();
            $lastDate = end($data['deliveryDates']);
            // Récupération de la différence de jour entre la dernière date de livraison et la date d'aujourd'hui
            $diffDeliveryDate = $todayDate->diff($lastDate)->format('%a%');

            // Passage des nombres de jours auxquelles le prospect n'a pas commandé
                $data['dayWithoutDelivery'] = $diffDeliveryDate;

            // Comparaison avec la fréquence de livraison des prospects
            if($diffDeliveryDate >= $data['avgFrequency'] +2){
                //riskLevel à rouge
                $data['riskLevel'] = 2;
            }elseif($diffDeliveryDate >= $data['avgFrequency'] +1){
                //riskLevel à jaune
                $data['riskLevel'] = 1;
            }else{
                //riskLevel à vert
                $data['riskLevel'] = 0;
            }

            // Libération de la mémoire (destruction variable)
            unset($data['deliveryDates']);
        }

        // Le tri par défaut qui permet de voir en premier les prospects les plus à risque
        // et qui fait un tri en second temps sur ceux qui ont plus de jours sans commander
        usort($allData, function($a, $b) {
            $compare = $a['riskLevel'] < $b['riskLevel'];
            if($a['riskLevel'] == $b['riskLevel']){
                $compare = $a['dayWithoutDelivery'] < $b['dayWithoutDelivery'];
            }
            return $compare;
        });

        return $this->render('@App/Admin/Reporting/index.html.twig',
        [   'allData' => $allData,
            'form' => $form->createView(),
            'start' => $startDateTxt,
            'end'   => $endDateTxt,
            'startFormatted' => $startDateFormatted,
            'endFormatted' => $endDateFormatted
        ]);
    }


    /**
     * @param $id
     * @return Response
     */
    public function detailAction($id){

        $em = $this->getDoctrine()->getManager();
        /** @var Prospect $prospect */
        $prospect = $em->getRepository(Prospect::class)->find($id);

        // Tableau qui recueille toutes les données
        $allData = [];

        // Changer de méthodes de récupérations des fournisseurs.
        // Passer par les factures pour avoir le fournisseur. Ca renvoi un id,
        // Besoin d'une requête dql pour obtenir le nom du fournisseur selon l'id du fournisseur ?

        // Récupération des fournisseurs du prospect
        /*$suppliers = $prospect->getSuppliers();
        foreach($suppliers as $supplier){
            $supplierDouble = $supplier->getSupplier()->getName();

                $data = [
                    'nameSupplier' => $supplier->getSupplier()->getName(),
                    'category' => $supplier->getCategory()
                ];

            $allData[$supplier->getId()] = $data;
        }*/
        /*
        // Récupération des factures du prospect renseigné

        /** @var Invoice[] $invoices */
        /*
        $invoices = $em->getRepository(Invoice::class)
            ->findBy(
                array('prospect' => $id)
            );

        foreach($invoices as $invoice){
            $data = [
                'nameSupplier' => $invoice->getSupplier()->getName()
            ];
            $allData[$invoice->getId()] = $data;
        }

        $suppliers = $prospect->getSuppliers();

        foreach($suppliers as $supplier){

        }*/

        return $this->render('@App/Admin/Reporting/show.html.twig',
            [
                'allData' => $allData,
                'prospect' => $prospect,
                //'suppliers' => $suppliers
            ]);
    }

    /**
     * @return FormInterface
     */
    private function getForm()
    {
        return $this->createFormBuilder()
            ->add('startDate', DateType::class, ['label' => 'Date de début', 'widget' => 'single_text', 'attr' => ['class' => 'ui datepicker']])
            ->add('endDate', DateType::class, ['label' => 'Date de fin', 'widget' => 'single_text', 'attr' => ['class' => 'ui datepicker']])
            ->getForm()
            ;
    }

    /**
     * @param $date
     * @return mixed
     */
    // Fonction pour formater une date (e.g : mercredi 11 novembre)
    public function dateFormat($date){
        $formatter = new IntlDateFormatter('fr_FR', IntlDateFormatter::NONE, IntlDateFormatter::NONE);
        $formatter->setPattern('EEEE d MMMM');

        return $formatter->format($date);
    }

    /**
     * @param $array
     * @param $uniqueKey
     * @return array
     */
    public function uniqueArray($array, $uniqueKey) {

        if (!is_array($array))
            return array();

        $uniqueKeys = array();
        foreach ($array as $key => $item)
            if (!in_array($item[$uniqueKey], $uniqueKeys))
                $uniqueKeys[$item[$uniqueKey]] = $item;

        return $uniqueKeys;
    }
}