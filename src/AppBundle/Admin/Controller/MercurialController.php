<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Services\ApacheTikaClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Form\Type\VichFileType;

class MercurialController extends AbstractController
{
    /** @var ApacheTikaClient */
    private $apacheTikaClient;

    /**
     * @param ApacheTikaClient $apacheTikaClient
     */
    public function __construct(ApacheTikaClient $apacheTikaClient)
    {
        $this->apacheTikaClient = $apacheTikaClient;
    }

    public function indexAction(Request $request)
    {
        $form = $this->getMercurialUploadForm();

        $form->handleRequest($request);
        $output = $rawOutput = '';

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $mercurialFile */
            $mercurialFile = $form->get('mercurial')->getData();

//            $client = $this->get('apache_tika.client');

            try {
                $output = nl2br(($rawOutput = $this->apacheTikaClient->getText($mercurialFile->getPathname())));
            } catch (\Exception $e) {
                $output = $e->getMessage();
            }

            $form = $this->getMercurialUploadForm();
        }

        return $this->render('@App/Admin/Mercurial/index.html.twig', [
            'form' => $form->createView(),
            'output' => $output,
            'raw_output' => $rawOutput
        ]);
    }

    private function getMercurialUploadForm()
    {
        return $this->createFormBuilder()
            ->add('mercurial', VichFileType::class, [
                'required' => true,
                'label' => 'Mercuriale',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Lancer le debug !',
                'attr' => [
                    'class' => 'ui button primary'
                ]
            ])
            ->getForm()
        ;
    }
}