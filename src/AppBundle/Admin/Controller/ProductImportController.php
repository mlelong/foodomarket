<?php

namespace AppBundle\Admin\Controller;

use AppBundle\Admin\Controller\Traits\ProductImportProductAutocompleteTrait;
use AppBundle\Admin\Controller\Traits\ProductImportProductOptionValueAutocompleteTrait;
use AppBundle\Admin\Controller\Traits\ProductImportProductVariantAutocompleteTrait;
use AppBundle\Admin\Controller\Traits\ProductImportTaxonAutocompleteTrait;
use AppBundle\Admin\Form\Type\ImportLineType;
use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\ImportRepository;
use AppBundle\Services\ProductVariantUtils;
use AppBundle\Services\RedisCacheManager;
use AppBundle\Util\BestSimilars;
use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\Taxon;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductImportController extends Controller
{
    use ProductImportTaxonAutocompleteTrait;
    use ProductImportProductAutocompleteTrait;
    use ProductImportProductVariantAutocompleteTrait;
    use ProductImportProductOptionValueAutocompleteTrait;

    /**
     * @var string
     */
    private $databaseHost;
    /**
     * @var string
     */
    private $databaseName;
    /**
     * @var string
     */
    private $databaseUser;
    /**
     * @var string
     */
    private $databasePassword;
    /**
     * @var string
     */
    private $databasePort;
    /**
     * @var string
     */
    private $tableDumpDir;

    public function __construct(string $databaseHost, string $databaseName, string $databaseUser, string $databasePassword, ?string $databasePort, string $tableDumpDir)
    {
        $this->databaseHost = $databaseHost;
        $this->databaseName = $databaseName;
        $this->databaseUser = $databaseUser;
        $this->databasePassword = $databasePassword;
        $this->databasePort = $databasePort;
        $this->tableDumpDir = $tableDumpDir;
    }

    public function indexAction(Request $request)
    {
        $importRepository = $this->get('app.repository.import');
        /** @var Import[] $imports */
        $imports = [];

        $distinctSuppliers = $importRepository->createQueryBuilder('i')
            ->select('s')
            ->join(Supplier::class, 's', 'WITH', 's = i.supplier')
            ->getQuery()
            ->getResult()
        ;

        foreach ($distinctSuppliers as $supplier) {
            $imports = array_merge($imports, $importRepository->createQueryBuilder('i')
                ->join('i.supplier', 's')
                ->andWhere('i.restaurant = 4')
                ->andWhere('s.enabled = 0')
                ->andWhere("s.id = {$supplier->getId()}")
                ->orderBy('i.createdAt', 'DESC')
                ->setMaxResults(4)
                ->getQuery()
                ->getResult()
            );
        }

        $importsBySupplier = [];

        foreach ($imports as $import) {
            if (!isset($importsBySupplier[$import->getSupplier()->getId()])) {
                $importsBySupplier[$import->getSupplier()->getId()] = [
                    'supplierId' => $import->getSupplier()->getId(),
                    'supplierName' => $import->getSupplier()->getName(),
                    'imports' => []
                ];
            }

            $importsBySupplier[$import->getSupplier()->getId()]['imports'][] = array_merge(
                [
                    'id' => $import->getId(),
                    'date' => $import->getCreatedAt()->format('d/m/Y H:i:s'),
                    'prices_date' => $import->getPricesDate()->format('d/m/Y'),
                    'filename' => $import->getFilename(),
                ],
                $this->getImportStatus($import)
            );
        }

        return $this->render('@App/Admin/ProductImport/index.html.twig', ['imports_by_supplier' => $importsBySupplier]);
    }

    private function getImportStatus(Import $import)
    {
        /** @var Connection $connection */
        $connection = $this->getDoctrine()->getConnection();

        try {
            $ret = $connection->fetchAssoc("
                SELECT
                    COUNT(*) AS nbLines,
                    SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS waitingLines,
                    SUM(CASE WHEN status = 3 THEN 1 ELSE 0 END) AS importedLines,
                    SUM(CASE WHEN status = 2 THEN 1 ELSE 0 END) AS knownLines
                
                FROM
                    sylius_import_line
                
                WHERE
                    import_id = {$import->getId()}
                
                GROUP BY
                    import_id
                "
            );

            if ($ret === false) {
                $nbLines = $waitingLines = $importedLines = $knownLines = 0;
            } else {
                $nbLines = $ret['nbLines'];
                $waitingLines = $ret['waitingLines'];
                $importedLines = $ret['importedLines'];
                $knownLines = $ret['knownLines'];
            }
        } catch (DBALException $e) {
            /** @var ImportLine[] $lines */
            $lines = $import->getLines();
            $nbLines = 0;
            $waitingLines = 0;
            $knownLines = 0;
            $importedLines = 0;

            foreach ($lines as $line) {
                switch ($line->getStatus()) {
                    case ImportLine::STATUS_AWAITING_CHECK:
                        ++$waitingLines;
                        break ;
                    case ImportLine::STATUS_IMPORTED:
                        ++$importedLines;
                        break ;
                    case ImportLine::STATUS_MATCHED_WITH_CATALOG:
                        ++$knownLines;
                        break ;
                }

                ++$nbLines;
            }
        }

        return [
            'nb_lines' => $nbLines,
            'waiting_lines' => $waitingLines,
            'known_lines' => $knownLines,
            'imported_lines' => $importedLines
        ];
    }

    public function getLinesByStatusAction(Request $request)
    {
        /** @var Import $import */
        $import = $this->get('app.repository.import')->find($request->get('id'));
        $query = $request->query->get('q');
        $status = $request->get('status');
        $lines = [];

        /** @var ImportLine $line */
        foreach ($import->getLines() as $line) {
            if ($line->getStatus() == $status) {
                $productName = $line->getSupplierName();
                if (strlen($line->getReference())) {
                    $productName .= ' ('.$line->getReference().')';
                }

                $query = implode('[\S\s]+', explode(' ', $query));

                if (preg_match("/$query/i", $productName)) {
                    $lines[] = [
                        'id' => $line->getId(),
                        'name' => $productName
                    ];
                }
            }
        }

        return $this->json(['results' => $lines]);
    }

    public function deleteAction(Request $request)
    {
        /** @var ImportRepository $importRepository */
        $importRepository = $this->get('app.repository.import');
        /** @var Import $import */
        $import = $importRepository->find($request->get('id'));

        $importRepository->remove($import);

        return $this->redirectToRoute('app_admin_product_import_index');
    }

    public function duplicateAction(Request $request)
    {
        $supplierId = $request->attributes->get('supplierId');
        $supplierRepository = $this->get('app.repository.supplier');
        $supplierName = $supplierRepository->find($supplierId)->getDisplayName(true);
        $formBuilder = $this->createFormBuilder(null, ['action' => $this->generateUrl('app_admin_product_import_duplicate', ['supplierId' => $supplierId])]);
        $suppliers = $supplierRepository->getPartners(true);
        $choices = [];

        foreach ($suppliers as $supplier) {
            if ($supplier->getId() != $supplierId) {
                $choices[] = $supplier;
            }
        }

        $form = $formBuilder
            ->add('suppliers', EntityType::class, [
                'label' => false,
                'required' => true,
                'choices' => $choices,
                'class' => Supplier::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => function(Supplier $supplier) {
                    return $supplier->getId() . ' - ' . $supplier->getDisplayName(true);
                }
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Supplier[] $suppliers */
            $suppliers = $form->get('suppliers')->getData();

            // Backup product_matching before that
            if (!$this->backupProductMatching()) {
                $this->get('session')->getFlashBag()->add('error', "Catalogue {$supplierName} non dupliqué: Impossible de faire une sauvegarde des infos avant la duplication");

                return $this->redirectToRoute('app_admin_product_import_index');
            }

            foreach ($suppliers as $supplier) {
                $status = $this->duplicateCatalog($supplierId, $supplier->getId());

                if (is_int($status)) {
                    $this->get('session')->getFlashBag()->add('success', "Catalogue {$supplierName} dupliqué pour {$supplier->getDisplayName(true)}: $status prix copiés");
                } else {
                    $this->get('session')->getFlashBag()->add('error', "Catalogue {$supplierName} non dupliqué pour {$supplier->getDisplayName(true)}: $status");
                }
            }

            return $this->redirectToRoute('app_admin_product_import_index');
        }

        return $this->render('@App/Admin/ProductImport/_duplicate_modal.html.twig', [
            'form' => $form->createView(),
            'supplier' => $supplierName
        ]);
    }

    private function duplicateCatalog(int $idSupplierSrc, int $idSupplierDst)
    {
        /** @var Connection $connection */
        $connection = $this->getDoctrine()->getConnection();
        $supplierRepository = $this->get('app.repository.supplier');
        /** @var Supplier $supplierDst */
        $supplierDst = $supplierRepository->find($idSupplierDst);
        $dstChannelCode = $supplierDst->getChannel()->getCode();

        try {
            $ret = $connection->transactional(function (Connection $connection) use ($idSupplierSrc, $idSupplierDst, $dstChannelCode) {
                // On nettoie d'abord
                $nbSpiDeleted = $connection->delete('sylius_supplier_product_informations', ['supplier_id' => $idSupplierDst]);
                $nbSpviDeleted = $connection->delete('sylius_supplier_product_variant_informations', ['supplier_id' => $idSupplierDst]);
                $nbSppDeleted = $connection->delete('sylius_supplier_product_price', ['supplier_id' => $idSupplierDst]);

                // On insère en copie ensuite
                $nbSpiInserted = $connection->exec("INSERT INTO sylius_supplier_product_informations(supplier_id, product_id, label, reference, created_at, updated_at)
                    SELECT $idSupplierDst, spi.product_id, spi.label, spi.reference, spi.created_at, spi.updated_at FROM sylius_supplier_product_informations spi WHERE spi.supplier_id = $idSupplierSrc
                ");

                $nbSpviInserted = $connection->exec("INSERT INTO sylius_supplier_product_variant_informations(supplier_id, product_id, product_variant_id, product_label, variant_label, reference, weight, content, sales_unit, manually_created, unit_quantity, display_name, created_at, updated_at, order_ref)
                    SELECT $idSupplierDst, spvi.product_id, spvi.product_variant_id, spvi.product_label, spvi.variant_label, spvi.reference, spvi.weight, spvi.content, spvi.sales_unit, spvi.manually_created, spvi.unit_quantity, spvi.display_name, spvi.created_at, spvi.updated_at, spvi.order_ref FROM sylius_supplier_product_variant_informations spvi WHERE spvi.supplier_id = $idSupplierSrc
                ");

                $nbSppInserted = $connection->exec("INSERT INTO sylius_supplier_product_price(supplier_id, restaurant_id, product_id, product_variant_id, kg_price, unit_price, channel_code, created_at, updated_at, mercuriale_id, valid_from, valid_to, promo)
                    SELECT $idSupplierDst, spp.restaurant_id, spp.product_id, spp.product_variant_id, spp.kg_price, spp.unit_price, '$dstChannelCode', spp.created_at, spp.updated_at, $idSupplierDst, spp.valid_from, spp.valid_to, spp.promo FROM sylius_supplier_product_price spp WHERE spp.supplier_id = $idSupplierSrc
                ");

                return $nbSppInserted;
            });

            $this->get('app.redis.cache.manager')->deleteItemsByPattern("supplier-*{$idSupplierDst}*");

            if ($supplierDst->getFacadeSupplier()) {
                $this->get('app.redis.cache.manager')->deleteItemsByPattern("supplier-*{$supplierDst->getFacadeSupplier()->getId()}*");
            }

            return $ret;
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    public function editAction(Request $request)
    {
        /** @var Import $import */
        $import = $this->get('app.repository.import')->find($request->get('id'));
        $importStatus = $this->getImportStatus($import);
        $candidateLineId = $request->get('desired_line_id', 0);

        if ($candidateLineId == 0) {
            /** @var ImportLine $line */
            foreach ($import->getLines() as $line) {
                if ($line->getStatus() === ImportLine::STATUS_AWAITING_CHECK) {
                    $candidateLineId = $line->getId();
                    break;
                }
            }
        }

        return $this->render('@App/Admin/ProductImport/edit.html.twig', array_merge(
            [
                'supplier_name' => $import->getSupplier()->getName(),
                'import_id' => $import->getId(),
                'candidate_line_id' => $candidateLineId
            ],
            $importStatus
        ));
    }

    public function nextProductAction(Request $request)
    {
        /** @var ImportLine $importLine */
        try {
            $importLine = $this->get('app.repository.import_line')->createQueryBuilder('l')
                ->where('l.id > :currentLineId')
                ->andWhere('l.import = :import')
                ->andWhere('l.status = :status')
                ->setParameter('currentLineId', $request->get('lineId'))
                ->setParameter('import', $request->get('id'))
                ->setParameter('status', ImportLine::STATUS_AWAITING_CHECK)
                ->getQuery()
                ->setMaxResults(1)
                ->getOneOrNullResult();

            if ($importLine === null) {
                $importLine = $this->get('app.repository.import_line')->createQueryBuilder('l')
                    ->where('l.id != :currentLineId')
                    ->andWhere('l.import = :import')
                    ->andWhere('l.status = :status')
                    ->setParameter('currentLineId', $request->get('lineId'))
                    ->setParameter('import', $request->get('id'))
                    ->setParameter('status', ImportLine::STATUS_AWAITING_CHECK)
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult();
            }
        } catch (NonUniqueResultException $ignored) {
            $importLine = null;
        }

        if ($importLine === null) {
            return $this->render('@App/Admin/ProductImport/_done.html.twig');
        }

        return $this->redirectToRoute('app_admin_product_import_edit_product', [
            'id' => $importLine->getImport()->getId(),
            'lineId' => $importLine->getId()
        ]);
    }

    private function getTaxonBasedTaxonsSuggestions(string $productName)
    {
        $taxons = $this->get('app.redis.cache.manager')->get(RedisCacheManager::CACHE_KEY_ADMIN_TAXONS, null, function() {
            $taxonInterator = $this->get('sylius.repository.taxon')->createQueryBuilder('t')->getQuery()->iterate();
            $taxonManager = $this->get('sylius.manager.taxon');
            $taxons = [];

            foreach ($taxonInterator as $row) {
                /** @var Taxon $taxon */
                $taxon = &$row[0];

                $taxons[$taxon->getId()] = [
                    'id' => $taxon->getId(),
                    'name' => $taxon->getName()
                ];

                $taxonManager->detach($taxon);
            }

            return $taxons;
        });


        $bestSimilarTaxons = new BestSimilars($productName, 5, function($value) {
            /** @noinspection PhpUndefinedFieldInspection */
            similar_text(strtolower($this->value), strtolower($value['name']), $percent);

            return $percent;
        });

        foreach ($taxons as $taxon) {
            $bestSimilarTaxons->add($taxon);
        }

        $bestTaxons = $bestSimilarTaxons->getBests();
        $bestTaxonsDisplay = [];

        $taxonRepository = $this->get('sylius.repository.taxon');

        foreach ($bestTaxons as $bestTaxon) {
            /** @noinspection PhpParamsInspection */
            $bestTaxonsDisplay[] = [
                'id' => $bestTaxon['value']['id'],
                'name' => $this->getFullTaxonPath($taxonRepository->find($bestTaxon['value']['id'])),
                'score' => number_format($bestTaxon['score'], 2, ',', ' ')
            ];
        }

        return $bestTaxonsDisplay;
    }

    private function getProductBasedTaxonsSuggestions(string $productName)
    {
        $products = $this->get('app.redis.cache.manager')->get(RedisCacheManager::CACHE_KEY_ADMIN_PRODUCTS, null, function() {
            $productIterator = $this->get('sylius.repository.product')->createQueryBuilder('p')->getQuery()->iterate();
            $productManager = $this->get('sylius.manager.product');
            $products = [];

            foreach ($productIterator as $row) {
                /** @var Product $product */
                $product = &$row[0];

                $products[$product->getId()] = [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                    'main_taxon_id' => $product->getMainTaxon() ? $product->getMainTaxon()->getId() : '',
                    'main_taxon_name' => $product->getMainTaxon() ? $product->getMainTaxon()->getName() : ''
                ];

                $productManager->detach($product);
            }

            return $products;
        });

        $bestSimilarProducts = new BestSimilars($productName, 5, function($value) {
            /** @noinspection PhpUndefinedFieldInspection */
            similar_text(strtolower($this->value), strtolower($value['name']), $percent);

            return $percent;
        });

        foreach ($products as $product) {
            $bestSimilarProducts->add($product);
        }

        $bestProducts = $bestSimilarProducts->getBests();
        $bestTaxonsDisplay = [];

        $taxonRepository = $this->get('sylius.repository.taxon');

        foreach ($bestProducts as $bestProduct) {
            if (!empty($bestProduct['value']['main_taxon_name'])) {
                /** @noinspection PhpParamsInspection */
                $bestTaxonsDisplay[$bestProduct['value']['main_taxon_id']] = [
                    'id' => $bestProduct['value']['main_taxon_id'],
                    'name' => $this->getFullTaxonPath($taxonRepository->find($bestProduct['value']['main_taxon_id'])),
                    'score' => number_format($bestProduct['score'], 2, ',', ' ')
                ];
            }
        }

        return array_values($bestTaxonsDisplay);
    }

    public function editProductAction(Request $request)
    {
        $importId = $request->get('id');
        $importLineId = $request->get('lineId');

        if ($importLineId == 0) {
            return $this->render('@App/Admin/ProductImport/_done.html.twig');
        }

        /** @var ImportLine $importLine */
        $importLine = $this->get('app.repository.import_line')->findOneBy(['id' => $importLineId, 'import' => $importId]);

        if ($importLine->getProductVariant() !== null) {
            $importLine->loadOptionsFromProductVariant();
        }

        $form = $this->getImportLineForm($importLine);
        $errors = [];

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $importLineManager = $this->get('app.manager.import_line');

                try {
                    $importLine = $form->getData();

                    if ($importLine->getProduct() !== null && $importLine->getStatus() === ImportLine::STATUS_AWAITING_CHECK) {
                        $importLine->setStatus(ImportLine::STATUS_MATCHED_WITH_CATALOG);
                    }

                    $importLineManager->flush($importLine);

                    return new JsonResponse([
                        'ok' => true,
                        'redirect_uri' => $this->generateUrl('app_admin_product_import_edit', [
                            'id' => $importId,
                            'desired_line_id' => $importLineId
                        ], UrlGeneratorInterface::ABSOLUTE_URL)
                    ]);
                } catch (OptimisticLockException $e) {
                    $errors[] = $e->getMessage();
                }
            } else {
                $formErrors = $form->getErrors(true);

                foreach ($formErrors as $formError) {
                    $errors[] = $formError->getMessage();
                }
            }
        }

        return $this->render('@App/Admin/ProductImport/_product_form.html.twig', [
                'line' => $importLine,
                'form' => $form->createView(),
                'errors' => $errors,
                'suggestions' => [
                    'taxons' => $this->getTaxonsSuggestion($importLine->getSupplierName(), $importLine->getTaxonDeterminant())
                ],
                'file_price' => bcdiv($importLine->getPrice(), 100, 2)
            ] + $this->getDisplayOptions($importLine)
        );
    }

    public function importProductAction(Request $request)
    {
        $importId = $request->get('id');
        $importLineId = $request->get('lineId');
        /** @var ImportLine $importLine */
        $importLine = $this->get('app.repository.import_line')->findOneBy(['id' => $importLineId, 'import' => $importId]);
        $form = $this->getImportLineForm($importLine);
        $errors = [];

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $importLineManager = $this->get('app.manager.import_line');

                try {
                    $importLine = $form->getData();
                    $importLineManager->flush($importLine);

                    // Now import the product
                    $product = $this->get('app.product.manager')->importProduct($importLine->getImport(), $importLine);

                    if ($product !== null) {
                        $importLine->setProduct($product);
                    } else {
                        $importLine->setStatus(ImportLine::STATUS_AWAITING_CHECK);
                    }

                    $this->getDoctrine()->getManager()->flush();

                    if ($importLine->getStatus() === ImportLine::STATUS_IMPORTED) {
                        return new JsonResponse(['ok' => true]);
                    }
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }
            } else {
                $formErrors = $form->getErrors(true);

                foreach ($formErrors as $formError) {
                    $errors[] = $formError->getMessage();
                }
            }
        }

        return $this->render('@App/Admin/ProductImport/_product_form.html.twig', [
                'line' => $importLine,
                'form' => $form->createView(),
                'errors' => $errors,
                'suggestions' => [
                    'taxons' => $this->getTaxonsSuggestion($importLine->getSupplierName(), $importLine->getTaxonDeterminant())
                ],
                'file_price' => bcdiv($importLine->getPrice(), 100, 2)
            ] + $this->getDisplayOptions($importLine)
        );
    }

    private function getTaxonsSuggestion(string $productName, string $taxonName = '')
    {
        $taxon1Suggestions = $this->getTaxonBasedTaxonsSuggestions($taxonName);
        $taxon2Suggestions = $this->getTaxonBasedTaxonsSuggestions($productName);
        $taxon3Suggestions = $this->getProductBasedTaxonsSuggestions($productName);

        $mergedSuggestions = array_merge($taxon1Suggestions, $taxon2Suggestions, $taxon3Suggestions);

        $keys = [];
        foreach($mergedSuggestions as $key => $suggestion) {
            if (array_key_exists($suggestion['id'], $keys)) {
                unset($mergedSuggestions[$key]);
            }
            $keys[$suggestion['id']] = $suggestion['id'];
        }

        usort($mergedSuggestions, function($a, $b) {
            return floatval(str_replace(',', '.', $a['score'])) < floatval(str_replace(',', '.', $b['score']));
        });

        array_splice($mergedSuggestions, 10);

        return $mergedSuggestions;
    }

    private function getDisplayOptions(ImportLine $importLine)
    {
        $productOptionValueRepository = $this->get('sylius.repository.product_option_value');

        /** @var ProductOptionValue $salesUnit */
        $salesUnit = $productOptionValueRepository->findOneBy(['code' => $importLine->getSalesUnit()]);
        /** @var ProductOptionValue $orderUnit */
        $orderUnit = $productOptionValueRepository->findOneBy(['code' => $importLine->getOrderUnit()]);
        /** @var ProductOptionValue $conditionning */
        $conditionning = $productOptionValueRepository->findOneBy(['code' => $importLine->getConditioning()]);
        /** @var ProductOptionValue $unitQuantity */
        $unitQuantity = $productOptionValueRepository->findOneBy(['code' => $importLine->getUnitQuantity()]);
        /** @var ProductOptionValue $unitContentQuantity */
        $unitContentQuantity = $productOptionValueRepository->findOneBy(['code' => $importLine->getUnitContentQuantity()]);
        /** @var ProductOptionValue $unitContentMeasurement */
        $unitContentMeasurement = $productOptionValueRepository->findOneBy(['code' => $importLine->getUnitContentMeasurement()]);
        /** @var ProductOptionValue $caliber */
        $caliber = $productOptionValueRepository->findOneBy(['code' => $importLine->getCaliber()]);
        /** @var ProductOptionValue $category */
        $category = $productOptionValueRepository->findOneBy(['code' => $importLine->getCategory()]);
        /** @var ProductOptionValue $variety */
        $variety = $productOptionValueRepository->findOneBy(['code' => $importLine->getVariety()]);
        /** @var ProductOptionValue $brand */
        $brand = $productOptionValueRepository->findOneBy(['code' => $importLine->getBrand()]);
        /** @var ProductOptionValue $bio */
        $bio = $productOptionValueRepository->findOneBy(['code' => $importLine->getBio()]);
        /** @var ProductOptionValue $appellation */
        $appellation = $productOptionValueRepository->findOneBy(['code' => $importLine->getNaming()]);

        $productName = $importLine->getProduct() !== null ? $importLine->getProduct()->getName() : $importLine->getProductName();

        if ($productName === null) {
            $productName = '';
        }

        return [
            'explicit_content' => ProductVariantUtils::getExplicitContent(
                $orderUnit ? $orderUnit->getValue() : '',
                $conditionning ? $conditionning->getValue() : '',
                $unitQuantity ? $unitQuantity->getValue() : '',
                $unitContentQuantity ? $unitContentQuantity->getValue() : '',
                $unitContentMeasurement ? $unitContentMeasurement->getValue() : ''
            ),
            'variant_name' => ProductVariantUtils::getName(
                $productName,
                $caliber ? $caliber->getValue() : '',
                $category ? $category->getValue() : '',
                $variety ? $variety->getValue() : '',
                $brand ? $brand->getValue() : '',
                $bio ? $bio->getValue() : '',
                $appellation ? $appellation->getValue() : ''
            ),
            'price' => ProductVariantUtils::getPriceDetail(
                $orderUnit ? $orderUnit->getValue() : '',
                $salesUnit ? $salesUnit->getValue() : '',
                $unitQuantity ? $unitQuantity->getValue() : 0,
                $conditionning ? $conditionning->getValue() : '',
                $importLine->getWeight() ? $importLine->getWeight() : 0,
                $importLine->getKgPrice() ? $importLine->getKgPrice() : 0,
                $importLine->getPriceVariant() ? $importLine->getPriceVariant() : 0
            )['full']
        ];
    }

    private function getImportLineForm(ImportLine $importLine)
    {
        $form = $this->createForm(ImportLineType::class, $importLine, [
            'action' => $this->generateUrl('app_admin_product_import_edit_product', [
                'id' => $importLine->getImport()->getId(),
                'lineId' => $importLine->getId()
            ])
        ]);

        return $form;
    }

    public function getExplicitContentAction(Request $request)
    {
        $explicitContent = ProductVariantUtils::getExplicitContent(
            $request->query->get('uc', 'KG'),
            $request->query->get('co', ''),
            intval($request->query->get('uq', 1)),
            $request->query->get('ucq', 1),
            $request->query->get('ucm', 'KG')
        );

        return new JsonResponse(['explicit_content' => $explicitContent]);
    }

    public function getVariantNameAction(Request $request)
    {
        $variantName = ProductVariantUtils::getName(
            urldecode($request->attributes->get('product')),
            $request->query->get('ca', ''),
            $request->query->get('cc', ''),
            $request->query->get('va', ''),
            $request->query->get('mq', ''),
            $request->query->get('bi', ''),
            $request->query->get('ap', '')
        );

        return new JsonResponse(['variant_name' => $variantName]);
    }

    public function getPriceDetailAction(Request $request)
    {
        $priceDetail = ProductVariantUtils::getPriceDetail(
            $request->query->get('uc', 'KG'),
            $request->query->get('um', 'KG'),
            intval($request->query->get('uq', 1)),
            $request->query->get('co', ''),
            floatval(str_replace(',', '.', $request->query->get('weight', 0))),
            bcmul(str_replace(',', '.', $request->query->get('kgPrice', 0)), 100),
            bcmul(str_replace(',', '.', $request->query->get('unitPrice', 0)), 100)
        );

        return new JsonResponse(['price_detail' => $priceDetail]);
    }

    private function backupProductMatching()
    {
        $tables = [
            'sylius_supplier_product_informations',
            'sylius_supplier_product_variant_informations',
            'sylius_supplier_product_price'
        ];

        $now = (new DateTime())->format('YmdHis');

        if (!file_exists("$this->tableDumpDir/$now")) {
            if (!mkdir("$this->tableDumpDir/$now", 0777, true)) {
                return false;
            }
        }

        foreach ($tables as $table) {
            $ret = exec("mysqldump -u{$this->databaseUser} -p{$this->databasePassword} {$this->databaseName} {$table} > {$this->tableDumpDir}/{$now}/{$table}.sql");

            if (!empty($ret)) {
                return false;
            }
        }

        return true;
    }
}