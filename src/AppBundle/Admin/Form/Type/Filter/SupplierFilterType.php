<?php

namespace AppBundle\Admin\Form\Type\Filter;

use AppBundle\Entity\SupplierCategory;
use AppBundle\Repository\SupplierCategoryRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class SupplierFilterType extends AbstractType
{
    /** @var SupplierCategoryRepository */
    private $supplierCategoryRepository;

    public function __construct(SupplierCategoryRepository $supplierCategoryRepository)
    {
        $this->supplierCategoryRepository = $supplierCategoryRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [['---' => 0]];
        /** @var SupplierCategory[] $supplierCategories */
        $supplierCategories = $this->supplierCategoryRepository->findAll();

        foreach ($supplierCategories as $supplierCategory) {
            $choices[] = [
                "$supplierCategory" => $supplierCategory->getId()
            ];
        }

        $builder->add('supplier', ChoiceType::class, [
            'required' => false,
            'label' => false,
            'choices' => $choices,
            'multiple' => false,
            'expanded' => false,
        ]);
    }
}