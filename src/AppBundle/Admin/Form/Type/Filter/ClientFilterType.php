<?php

namespace AppBundle\Admin\Form\Type\Filter;

use Sonata\CoreBundle\Form\Type\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hasSuppliers', BooleanType::class, ['label' => false, 'required' => false, 'placeholder' => 'Tous']);
    }
}