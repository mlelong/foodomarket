<?php

namespace AppBundle\Admin\Form\Type;

use AppBundle\Entity\ProductOptionValue;
use Doctrine\ORM\NonUniqueResultException;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceAutocompleteChoiceType;
use Sylius\Component\Product\Model\ProductOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductOptionValueAutocompleteChoiceType extends AbstractType
{
    /** @var EntityRepository */
    private $productOptionValueRepository;

    /**
     * ProductOptionValueAutocompleteChoiceType constructor.
     * @param EntityRepository $productOptionValueRepository
     */
    public function __construct(EntityRepository $productOptionValueRepository)
    {
        $this->productOptionValueRepository = $productOptionValueRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new class($this->productOptionValueRepository, $options['option']) implements DataTransformerInterface {
            /** @var EntityRepository */
            private $productOptionValueRepository;

            /** @var ProductOption */
            private $productOption;

            public function __construct(EntityRepository $productOptionValueRepository, ProductOption $productOption) {
                $this->productOptionValueRepository = $productOptionValueRepository;
                $this->productOption = $productOption;
            }

            /**
             * @param string|null $value (Option value or option code, first try with option value, then with code)
             * @return ProductOptionValue|null
             */
            public function transform($value) {
                try {
                    return $this->productOptionValueRepository->createQueryBuilder('pov')
                        ->innerJoin('pov.option', 'o', 'WITH', 'o.code = :option_code')
                        ->innerJoin('pov.translations', 't', 'WITH', 't.locale = :locale')
                        ->where('t.value = :option_value')
                        ->orWhere('pov.code = :option_value')
                        ->setParameter('option_code', $this->productOption->getCode())
                        ->setParameter('locale', 'fr_FR')
                        ->setParameter('option_value', $value)
                        ->getQuery()
                        ->setMaxResults(1)
                        ->getOneOrNullResult();
                } catch (NonUniqueResultException $e) {
                    return null;
                }
            }

            /**
             * @param ProductOptionValue|null $value
             * @return string|null
             */
            public function reverseTransform($value) {
                if ($value instanceof ProductOptionValue) {
                    return $value->getCode();
                }

                return $value;
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('option');
        $resolver->setAllowedTypes('option', [ProductOption::class]);

        $resolver->setDefaults([
            'resource' => 'sylius.product_option_value',
            'choice_name' => 'name',
            'choice_value' => 'code'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        /** @var ProductOption $option */
        $option = $options['option'];

        $view->vars['remote_criteria_type'] = 'contains';
        $view->vars['remote_criteria_name'] = 'phrase';
        $view->vars['multiple'] = false;
        $view->vars['option_code'] = $option->getCode();
    }

    public function getBlockPrefix()
    {
        return 'app_product_option_value_autocomplete_choice';
    }

    public function getParent()
    {
        return ResourceAutocompleteChoiceType::class;
    }
}