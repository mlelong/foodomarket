<?php

namespace AppBundle\Admin\Form\Type;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Entity\SupplierCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SupplierAccountLogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clientCodes', CollectionType::class, [
                'required' => false,
                'label' => 'Codes client',
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => TextType::class
            ])
            ->add('status', ChoiceType::class, [
                'required' => true,
                'label' => 'Statut',
                'choices' => [
                    'En attente' => SupplierAccountLog::STATUS_WAITING,
                    'Compte ouvert' => SupplierAccountLog::STATUS_ACCOUNT_OPENED,
                    'Compte non ouvert (déja client ou autre)' => SupplierAccountLog::STATUS_ACCOUNT_BLOCKED,
                    'Passe des commandes' => SupplierAccountLog::STATUS_ORDERING
                ],
                'multiple' => false,
                'expanded' => true,
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('hash', TextType::class, [
                'required' => false,
                'label' => 'Hash code',
                'attr' => [
                    'placeholder' => 'Laisser vide pour le comportement par défaut'
                ]
            ])
            ->add('contractId', TextType::class, ['required' => false, 'label' => 'ID du contrat Sell & Sign'])
            ->add('signed', CheckboxType::class, ['required' => false, 'label' => 'Contrat Sell & Sign signé numériquement'])
            ->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $formEvent) {
                $form = $formEvent->getForm();
                /** @var SupplierAccountLog $supplierAccountLog */
                $supplierAccountLog = $formEvent->getData();

                if (!$supplierAccountLog || $supplierAccountLog->getId() === null) {
                    $form
                        ->add('prospect', EntityType::class, [
                            'required' => true,
                            'class' => Prospect::class,
                            'choice_label' => function(Prospect $prospect) {
                                return "{$prospect->getFirstName()} {$prospect->getContactName()} ({$prospect->getRestaurantName()})";
                            },
                            'label' => 'Prospect',
                            'constraints' => [
                                new NotBlank()
                            ]
                        ])
                        ->add('supplierCategory', EntityType::class, [
                            'required' => true,
                            'class' => SupplierCategory::class,
                            'label' => 'Grossiste/Catégorie',
                            'choice_label' => function(SupplierCategory $supplierCategory) {
                                return $supplierCategory->__toString();
                            },
                        ])
                    ;
                } else {
                    $form
                        ->add('prospect', EntityType::class, [
                            'required' => true,
                            'class' => Prospect::class,
                            'choice_label' => function(Prospect $prospect) {
                                return "{$prospect->getFirstName()} {$prospect->getContactName()} ({$prospect->getRestaurantName()})";
                            },
                            'label' => 'Prospect',
                            'disabled' => true,
                            'constraints' => [
                                new NotBlank()
                            ]
                        ])
                        ->add('supplierCategory', EntityType::class, [
                            'required' => true,
                            'class' => SupplierCategory::class,
                            'label' => 'Grossiste/Catégorie',
                            'choice_label' => function(SupplierCategory $supplierCategory) {
                                return $supplierCategory->__toString();
                            },
//                            'disabled' => true,
                        ])
                    ;
                }
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $formEvent) {
                /** @var SupplierAccountLog $supplierAccountLog */
                $supplierAccountLog = $formEvent->getData();

                if (empty($supplierAccountLog->getHash())
                    && $supplierAccountLog->getStatus() === SupplierAccountLog::STATUS_WAITING) {
                    try {
                        $supplierAccountLog->setHash(substr(strtr(base64_encode(bin2hex(random_bytes(32))), '+', '.'), 0, 44));
                    } catch (\Exception $e) {
                        $supplierAccountLog->setHash(uniqid());
                    }
                } else {
                    $supplierAccountLog->setHash(null);
                }
            });
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => SupplierAccountLog::class]);
    }

    public function getBlockPrefix()
    {
        return 'app_admin_supplier_account_log';
    }

}