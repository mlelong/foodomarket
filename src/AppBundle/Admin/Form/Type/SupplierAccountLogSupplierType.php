<?php

namespace AppBundle\Admin\Form\Type;

use AppBundle\Entity\SupplierAccountLog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SupplierAccountLogSupplierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clientCode', TextType::class, [
                'required' => true,
                'label' => 'Code client',
                'constraints' => [
                    new NotBlank(['message' => 'Merci de renseigner le code client SVP'])
                ],
            ])
            ->add('status', ChoiceType::class, [
                'required' => true,
                'label' => 'Statut',
                'choices' => [
                    'Compte ouvert' => SupplierAccountLog::STATUS_ACCOUNT_OPENED,
                    'Compte non ouvert (déja client ou autre)' => SupplierAccountLog::STATUS_ACCOUNT_BLOCKED,
                ],
                'multiple' => false,
                'expanded' => true,
                'constraints' => [
                    new NotBlank(['message' => 'Merci de renseigner si le compte a été ouvert ou pas SVP'])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => SupplierAccountLog::class]);
    }

    public function getBlockPrefix()
    {
        return 'app_admin_supplier_account_log_supplier';
    }

}