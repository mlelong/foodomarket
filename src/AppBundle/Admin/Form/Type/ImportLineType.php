<?php

namespace AppBundle\Admin\Form\Type;

use AppBundle\Admin\Form\DataTransformer\ImportLineDataTransformer;
use AppBundle\Admin\Form\DataTransformer\ProductOptionValueDataTransformer;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\ProductOptionValue;
use AppBundle\Form\ProductVariantAutocompleteChoiceType;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductTaxonRepository;
use AppBundle\Repository\TaxonRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Knp\Bundle\GaufretteBundle\FilesystemMap;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductOptionRepository;
use Sylius\Bundle\MoneyBundle\Form\Type\MoneyType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductAutocompleteChoiceType;
use Sylius\Bundle\ProductBundle\Form\Type\ProductOptionValueChoiceType;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Bundle\TaxonomyBundle\Form\Type\TaxonAutocompleteChoiceType;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductImage;
use Sylius\Component\Core\Model\ProductTaxon;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Product\Factory\ProductFactory;
use Sylius\Component\Product\Model\ProductOption;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportLineType extends AbstractType
{
    /** @var ProductOptionRepository */
    private $productOptionRepository;
    /** @var ImportLineDataTransformer */
    private $importLineDataTransformer;
    /** @var ProductOptionValueDataTransformer */
    private $productOptionValueDataTransformer;
    /** @var ProductRepository */
    private $productRepository;
    /** @var ProductFactory */
    private $productFactory;
    /** @var ProductTaxonRepository */
    private $productTaxonRepository;
    /** @var TaxonRepository */
    private $taxonRepository;
    /** @var EntityRepository */
    private $productOptionValueRepository;
    /** @var FactoryInterface */
    private $productOptionValueFactory;
    /** @var FilesystemMap */
    private $filesystemMap;
    /** @var EntityManager */
    private $entityManager;

    public function __construct(
        ProductOptionRepository $productOptionRepository,
        ImportLineDataTransformer $importLineDataTransformer,
        ProductOptionValueDataTransformer $productOptionValueDataTransformer,
        ProductRepository $productRepository,
        ProductFactory $productFactory,
        ProductTaxonRepository $productTaxonRepository,
        TaxonRepository $taxonRepository,
        EntityRepository $productOptionValueRepository,
        FactoryInterface $productOptionValueFactory,
        FilesystemMap $filesystemMap,
        EntityManager $entityManager
    ) {
        $this->productOptionRepository = $productOptionRepository;
        $this->importLineDataTransformer = $importLineDataTransformer;
        $this->productOptionValueDataTransformer = $productOptionValueDataTransformer;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->productTaxonRepository = $productTaxonRepository;
        $this->taxonRepository = $taxonRepository;
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->productOptionValueFactory = $productOptionValueFactory;
        $this->filesystemMap = $filesystemMap;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('taxon', TaxonAutocompleteChoiceType::class, [
                'label' => 'Taxon',
                'choice_value' => 'id',
                'required' => false
            ])
            ->add('product', ProductAutocompleteChoiceType::class, [
                'label' => 'Produit',
                'choice_value' => 'id',
                'required' => false
            ])
            ->add('productVariant', ProductVariantAutocompleteChoiceType::class, [
                'label' => 'Variante',
                'choice_value' => 'id',
                'required' => false
            ])
            ->add('productName', HiddenType::class)
            ->add('orderUnit', ProductOptionValueChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'UC']),
                'label' => 'Unité de commande',
                'required' => false,
            ])
            ->add('conditioning', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'CO']),
                'label' => 'Conditionnement',
                'required' => false,
            ])
            ->add('unitQuantity', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'UQ']),
                'label' => 'Nombre d\'unités',
                'required' => false,
            ])
            ->add('unitContentQuantity', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'UCQ']),
                'label' => 'Contenu de l\'unité',
                'required' => false,
            ])
            ->add('unitContentMeasurement', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'UCM']),
                'label' => 'Unité de mesure du contenu',
                'required' => false,
            ])
            ->add('category', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'CC']),
                'label' => 'Catégorie',
                'required' => false,
            ])
            ->add('caliber', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'CA']),
                'label' => 'Calibre',
                'required' => false,
            ])
            ->add('variety', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'VA']),
                'label' => 'Variété',
                'required' => false,
            ])
            ->add('brand', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'MQ']),
                'label' => 'Marque',
                'required' => false,
            ])
            ->add('bio', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'BI']),
                'label' => 'Bio',
                'required' => false,
            ])
            ->add('naming', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'AP']),
                'label' => 'Appellation',
                'required' => false,
            ])
            ->add('salesUnit', ProductOptionValueChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'UM']),
                'label' => 'Unité de facturation',
                'required' => false
            ])
            ->add('weight', NumberType::class, [
                'label' => 'Poids (en kg)',
                'required' => false,
                'scale' => 3
            ])
            ->add('kgPrice', MoneyType::class, [
                'label' => 'Prix au kilo',
                'required' => false,
            ])
            ->add('priceVariant', MoneyType::class, [
                'label' => 'Prix de vente',
                'required' => false
            ])
            ->add('productPicture', HiddenType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('originCountry', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'OP']),
                'label' => "Pays d'origine",
                'required' => false
            ])
            ->add('origin', ProductOptionValueAutocompleteChoiceType::class, [
                'option' => $this->productOptionRepository->findOneBy(['code' => 'OV']),
                'label' => "Origine",
                'required' => false
            ])
        ;

        $builder->get('orderUnit')->addModelTransformer($this->productOptionValueDataTransformer);
        $builder->get('salesUnit')->addModelTransformer($this->productOptionValueDataTransformer);

        $builder->addModelTransformer($this->importLineDataTransformer);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $formEvent) {
            /** @var ImportLine $data */
            $data = $formEvent->getData();
            $form = $formEvent->getForm();

            if ($data !== null) {
                if ($data->getProduct() !== null) {
                    /** @var Product $product */
                    $product = $data->getProduct();
                    $images = $product->getImages();

                    if (!$images->isEmpty()) {
                        $form->add('productPicture', HiddenType::class, [
                            'mapped' => false,
                            'required' => false,
                            'data' => $images->first()->getPath()
                        ]);
                    }
                }
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $formEvent) {
            $data = $formEvent->getData();

            // Try to create the product if it is not set already, and if a name and a taxon is set for it
            if (!empty($data['product']) && !is_numeric($data['product'])) {
                if (!empty($data['taxon']) && is_numeric($data['taxon'])) {
                    /** @var Product $product */
                    $product = $this->productFactory->createNew();
                    /** @var Taxon $taxon */
                    $taxon = $this->taxonRepository->find($data['taxon']);

                    $slug = (new Slugify())->slugify($data['product']);

                    $product->setName($data['product']);
                    $product->setMainTaxon($taxon);
                    $product->setSlug($slug);
                    $product->setCode($slug);

                    try {
                        $this->productRepository->add($product);
                        $data['product'] = $product->getId();

                        $taxons = array_merge([$taxon], $taxon->getAncestors()->toArray());

                        foreach ($taxons as $taxon) {
                            $productTaxon = new ProductTaxon();

                            $productTaxon->setProduct($product);
                            $productTaxon->setTaxon($taxon);

                            try {
                                $this->productTaxonRepository->add($productTaxon);
                            } catch (\Exception $ignored) {
                                // If an exception is caught, then probably the couple Product-Taxon is already in the database
                            }
                        }
                    } catch (\Exception $e) {
                    }
                }
            }

            if (is_numeric($data['product']) && !empty($data['productPicture'])) {
                /** @var Product $product */
                $product = $this->productRepository->find($data['product']);
                $found = false;

                /** @var ProductImage $image */
                foreach ($product->getImages() as $image) {
                    if ($image->getPath() === $data['productPicture']) {
                        $found = true;
                        break ;
                    }
                }

                if (!$found) {
                    $mediaFilesytem = $this->filesystemMap->get('sylius_image');
                    $id = $product->getId();
                    $filename = "$id-" . (new Slugify())->slugify($product->getName());
                    $reversedId = strrev(str_pad($id%1000, 3, '0',STR_PAD_LEFT));
                    $storagePath = '';

                    for($i=0; $i<3; $i++) {
                        $storagePath .= $reversedId[$i].'/';
                    }

                    $extension = '';

                    if (preg_match_all('/(\.[a-zA-Z]{3,4})/', $data['productPicture'], $matches)) {
                        $extension = $matches[0][count($matches[0]) - 1];
                    }

                    if (!in_array(strtolower($extension), ['.jpg', '.jpeg', '.png', '.gif', '.apng', '.webp'])) {
                        $extension = '.jpg';
                    }

                    $newFilename = $storagePath.$filename.$extension;

                    $ch = curl_init($data['productPicture']);

                    $header = [
                        'accept-language: fr,en-US;q=0.9,en;q=0.8',
                        'upgrade-insecure-requests: 1',
                        'cache-control: max-age=0',
                        'accept: image/webp,image/apng,image/*,*/*;q=1'
                    ];

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.98 Chrome/71.0.3578.98 Safari/537.36');
                    curl_setopt($ch, CURLOPT_ACCEPT_ENCODING, 'gzip, deflate, br');
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_TIMEOUT,30);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

                    $imageContent = curl_exec($ch);
                    $status = @curl_getinfo($ch);

                    if ($status['http_code'] == 200) {
                        if ($product->hasImages()) {
                            foreach ($product->getImages() as $image) {
                                $product->removeImage($image);
                            }
                            // the picture remove listener will remove the file
                            $this->entityManager->flush();
                        }

                        $ret = $mediaFilesytem->write($newFilename, $imageContent);

                        if ($ret !== false && $ret > 0) {
                            // update or insert the path in dbb
                            $image = new ProductImage();
                            $image->setPath($newFilename);
                            $image->setType('image');
                            $product->addImage($image);

                            $this->entityManager->flush();

                            $data['productPicture'] = $newFilename;

                            $formEvent->setData($data);
                        }
                    }
                }
            }

            $optionKeys = [
                'orderUnit' => 'UC',
                'conditioning' => 'CO',
                'unitQuantity' => 'UQ',
                'unitContentQuantity' => 'UCQ',
                'unitContentMeasurement' => 'UCM',
                'caliber' => 'CA',
                'category' => 'CC',
                'variety' => 'VA',
                'naming' => 'AP',
                'bio' => 'BI',
                'brand' => 'MQ',
                'salesUnit' => 'UM',
                'originCountry' => 'OP',
                'origin' => 'OV'
            ];

            foreach ($optionKeys as $optionKey => $optionCode) {
                if (!empty($data[$optionKey])) {
                    // Try to fetch option value by option code and option value code
                    /** @var ProductOptionValue|null $productOptionValue */
                    $productOptionValue = $this->productOptionValueRepository
                        ->createQueryBuilder('pov')
                        ->join('pov.option', 'po', 'WITH', 'po.code = :optionCode')
                        ->where('pov.code = :optionValueCode')
                        ->setParameter('optionCode', $optionCode)
                        ->setParameter('optionValueCode', $data[$optionKey])
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult()
                    ;

                    if ($productOptionValue === null) {
                        /** @var ProductOptionValue $productOptionValue */
                        $productOptionValue = $this->productOptionValueFactory->createNew();
                        /** @var ProductOption $productOption */
                        $productOption = $this->productOptionRepository->findOneBy(['code' => $optionCode]);

                        $optionValue = $data[$optionKey];
                        $codifiedOptionValue = preg_replace('/[\s]+/', '_', strtoupper($optionValue));

                        $productOptionValue->setOption($productOption);
                        $productOptionValue->setValue($optionValue);
                        $productOptionValue->setCode("{$optionCode}_{$codifiedOptionValue}");

                        $this->productOptionValueRepository->add($productOptionValue);

                        $data[$optionKey] = $productOptionValue->getCode();
                    }
                }
            }

            $formEvent->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ImportLine::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_admin_import_line';
    }
}