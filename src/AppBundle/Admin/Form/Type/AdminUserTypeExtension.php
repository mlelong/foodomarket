<?php

namespace AppBundle\Admin\Form\Type;

use Sylius\Bundle\CoreBundle\Form\Type\User\AdminUserType;
use Sylius\Component\Core\Model\AdminUserInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

final class AdminUserTypeExtension extends AbstractTypeExtension
{
    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return AdminUserType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    "Admin" => AdminUserInterface::DEFAULT_ADMIN_ROLE,
                    "Marketing" => "ROLE_MARKETING"
                ],
                'multiple' => true
            ])
        ;
    }

    public static function getExtendedTypes()
    {
        return [AdminUserType::class];
    }
}