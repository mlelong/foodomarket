<?php

namespace AppBundle\Admin\Form\Type\Shipping\Calculator;

use AppBundle\Entity\Supplier;
use AppBundle\Repository\SupplierRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

final class SupplierConfigurationType extends AbstractType
{
    /** @var SupplierRepository */
    private $supplierRepository;

    /**
     * SupplierConfigurationType constructor.
     * @param SupplierRepository $supplierRepository
     */
    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Supplier[] $partnerSuppliers */
        $partnerSuppliers = $this->supplierRepository->findAll();

        foreach ($partnerSuppliers as $partnerSupplier) {
            $builder->add("limit_{$partnerSupplier->getId()}", NumberType::class, [
                'label' => "{$partnerSupplier->getName()} livre gratuitement à partir de",
                'constraints' => [
                    new NotBlank(),
                    new Type(['type' => 'numeric']),
                ]
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $defaults = ['data_class' => null];

        /** @var Supplier[] $partnerSuppliers */
        $partnerSuppliers = $this->supplierRepository->findAll();

        foreach ($partnerSuppliers as $partnerSupplier) {
            $defaults["limit_{$partnerSupplier->getId()}"] = $partnerSupplier->getFreePort();
        }

        $resolver->setDefaults($defaults);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'app_shipping_calculator_supplier';
    }
}