<?php

namespace AppBundle\Admin\Form\ChoiceLoaderCallback;

use AppBundle\Admin\Form\ChoiceFormatterInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;

class GenericResourceChoiceLoaderCallback extends CallbackChoiceLoader
{
    /** @var EntityRepository */
    private $repository;

    /** @var ChoiceFormatterInterface */
    private $choiceFormatter;

    public function setRepository(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function setChoiceFormatter(ChoiceFormatterInterface $choiceFormatter)
    {
        $this->choiceFormatter = $choiceFormatter;
    }

    public function loadChoicesForValues(array $values, $value = null)
    {
        $choices = [];

        foreach ($values as $value) {
            $entity = $this->repository->find($value);

            if ($entity !== null) {
                $choices[] = [
                    'name' => $this->choiceFormatter->getLabel($entity),
                    'id' => "{$this->choiceFormatter->getValue($entity)}"
                ];
            }
        }

        return $choices;
    }

    public function loadValuesForChoices(array $choices, $value = null)
    {
        $values = [];

        foreach ($choices as $choice) {
            if ($choice === null) {
                continue ;
            }

            $values[] = "{$choice['id']}";
        }

        return $values;
    }
}