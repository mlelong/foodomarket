<?php

namespace AppBundle\Admin\Form\DataTransformer;

use AppBundle\Entity\ProductOptionValue;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\DataTransformerInterface;

class ProductOptionValueDataTransformer implements DataTransformerInterface
{
    /** @var EntityRepository */
    private $productOptionValueRepository;

    public function __construct(EntityRepository $productOptionValueRepository)
    {
        $this->productOptionValueRepository = $productOptionValueRepository;
    }

    /**
     * @param string|null $value
     * @return ProductOptionValue|object|null
     */
    public function transform($value)
    {
        if (empty($value)) {
            return null;
        }

        return $this->productOptionValueRepository->findOneBy(['code' => $value]);
    }

    /**
     * @param ProductOptionValue|string|null $value
     * @return mixed
     */
    public function reverseTransform($value)
    {
        if ($value instanceof ProductOptionValue) {
            return $value->getCode();
        }

        return $value;
    }
}