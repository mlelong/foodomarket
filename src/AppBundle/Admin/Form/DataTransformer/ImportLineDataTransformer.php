<?php

namespace AppBundle\Admin\Form\DataTransformer;

use AppBundle\Entity\ImportLine;
use Symfony\Component\Form\DataTransformerInterface;

class ImportLineDataTransformer implements DataTransformerInterface
{
    /**
     * @param ImportLine|null $value
     * @return ImportLine
     */
    public function transform($value)
    {
        if ($value instanceof ImportLine) {
            $value->contentToProperties();
        }

        return $value;
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($value)
    {
        if ($value instanceof ImportLine) {
            $value->propertiesToContent();
        }

        return $value;
    }
}