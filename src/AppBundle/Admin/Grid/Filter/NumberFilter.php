<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace AppBundle\Admin\Grid\Filter;

use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

final class NumberFilter implements FilterInterface
{

    /**
     * {@inheritdoc}
     */
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if (empty($data)) {
            return;
        }

        $field = isset($options['field']) ? $options['field'] : $name;

        $greaterThan = $this->getDataValue($data, 'greaterThan');
        $lessThan = $this->getDataValue($data, 'lessThan');

        $expressionBuilder = $dataSource->getExpressionBuilder();

        if ('' !== $greaterThan) {
            $expressionBuilder->greaterThan($field, $greaterThan);
        }
        if ('' !== $lessThan) {
            $expressionBuilder->lessThan($field, $lessThan);
        }
    }

    /**
     * @param string[] $data
     * @param string $key
     *
     * @return string
     */
    private function getDataValue(array $data, string $key): string
    {
        return isset($data[$key]) ? $data[$key] : '';
    }
}
