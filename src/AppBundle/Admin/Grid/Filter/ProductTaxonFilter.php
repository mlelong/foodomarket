<?php

namespace AppBundle\Admin\Grid\Filter;

use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

class ProductTaxonFilter implements FilterInterface
{
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        // Your filtering logic. DataSource is kind of query builder.
        // $data['stats'] contains the submitted value!
        // here is an example

        // $data['taxon'] => Taxon Id

        $dataSource->restrict($dataSource->getExpressionBuilder()->equals('mainTaxon', $data['taxon']));
    }
}