<?php

namespace AppBundle\Admin\Grid\Filter;

use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

class SupplierFilter implements FilterInterface
{
    /**
     * @param DataSourceInterface $dataSource
     * @param string $name
     * @param mixed $data
     * @param array $options
     */
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if (isset($data['supplier']) && !empty($data['supplier']) && $data['supplier'] != 0) {
            $supplierCategoryId = $data['supplier'];
            $dataSource->restrict("s IN ($supplierCategoryId)");
        }
    }
}