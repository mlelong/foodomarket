<?php

namespace AppBundle\Admin\Grid\Filter;

use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

class ClientFilter implements FilterInterface
{
    /**
     * @param DataSourceInterface $dataSource
     * @param string $name
     * @param mixed $data
     * @param array $options
     */
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if ($data['hasSuppliers'] == 1) {
            $dataSource->restrict('s IS NOT NULL');
        } elseif ($data['hasSuppliers'] == 2) {
            $dataSource->restrict('s IS NULL');
        }
    }
}