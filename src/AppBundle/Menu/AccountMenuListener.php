<?php
/**
 * Created by PhpStorm.
 * User: chekib
 * Date: 02/07/17
 * Time: 20:56
 */

namespace AppBundle\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AccountMenuListener
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function addAccountMenuItems(MenuBuilderEvent $event)
    {
        $menu = $event->getMenu();

        $menu
            ->addChild('restaurant_stock', ['route' => 'app_shop_account_restaurant_stock_index'])
            ->setLabel('Restaurant Stock')
            ->setLabelAttribute('icon', 'star')
        ;
    }
}