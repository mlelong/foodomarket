<?php

declare(strict_types=1);

namespace AppBundle\Menu;

use AppBundle\Entity\Prospect;
use AppBundle\Event\ProspectShowMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ProspectShowMenuBuilder
{
    public const EVENT_NAME = 'app.menu.admin.prospect.show';

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ProspectShowMenuBuilder constructor.
     * @param FactoryInterface $factory
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(FactoryInterface $factory, EventDispatcherInterface $eventDispatcher)
    {
        $this->factory = $factory;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!isset($options['prospect'])) {
            return $menu;
        }

        $prospect = $options['prospect'];
        $this->addChildren($menu, $prospect);

        $this->eventDispatcher->dispatch(
            self::EVENT_NAME,
            new ProspectShowMenuBuilderEvent($this->factory, $menu, $prospect)
        );

        return $menu;
    }

    /**
     * @param ItemInterface $menu
     * @param Prospect $prospect
     */
    private function addChildren(ItemInterface $menu, Prospect $prospect): void
    {
        $menu->setExtra('column_id', 'actions');

        $menu
            ->addChild('update', [
                'route' => 'app_admin_admin_prospect_edit',
                'routeParameters' => ['id' => $prospect->getId()],
            ])
            ->setAttribute('type', 'edit')
            ->setLabel('sylius.ui.edit')
        ;

//        $menu
//            ->addChild('order_index', [
//                'route' => 'sylius_admin_customer_order_index',
//                'routeParameters' => ['id' => $prospect->getId()],
//            ])
//            ->setAttribute('type', 'show')
//            ->setLabel('sylius.ui.show_orders')
//        ;

        $menu
            ->addChild('prospect_delete', [
                'route' => 'app_admin_admin_prospect_delete',
                'routeParameters' => ['id' => $prospect->getId()],
            ])
            ->setAttribute('type', 'delete')
            ->setAttribute('resource_id', $prospect->getId())
            ->setLabel('sylius.ui.delete')
        ;
    }
}