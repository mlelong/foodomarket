<?php

namespace AppBundle\Menu;

use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    const USELESS_ENTRIES = [
        'products',
        'inventory',
        'association_types',
        'attributes',
        'variants',
        'payments',
        'shipments',
        'promotions',
        'product_reviews',
//        'countries',
//        'zones',
        'currencies',
        'exchange_rates',
//        'tax_rates',
//        'tax_categories',
        'payment_methods',
        'shipping_methods',
        'shipping_categories',
    ];

    private function removeUselessEntries(ItemInterface $menu)
    {
        foreach ($menu->getChildren() as $child) {
            if ($child->hasChildren()) {
                foreach ($child->getChildren() as $child2) {
                    if (in_array($child2->getName(), self::USELESS_ENTRIES)) {
                        $child->removeChild($child2);
                    }
                }
            }
        }
    }

    /**
     * @param MenuBuilderEvent $event
     */
    public function addAdminMenuItems(MenuBuilderEvent $event)
    {
        $menu = $event->getMenu();

        $this->removeUselessEntries($menu);

//        $algoCategory = $menu
//            ->addChild('algorithm')
//            ->setLabel('Algorithme')
//        ;
//
//        $algoCategory
//            ->addChild('optim', ['route' => 'app_admin_optim_index'])
//            ->setLabel('Optimisation Panier')
//            ->setLabelAttribute('icon', 'gem')
//        ;

        foreach ($menu->getChildren() as $child) {
            switch ($child->getName()) {
                case 'configuration':
                    $child
                        ->addChild('cache', ['route' => 'app_admin_cache_index'])
                        ->setLabel('Cache')
                        ->setLabelAttribute('icon', 'archive')
                    ;

                    $child
                        ->addChild('product_carousel', ['route' => 'app_admin_product_carousel_index'])
                        ->setLabel('Caroussel produits')
//                        ->setLabelAttribute('icon', 'sliders horizontal')
                    ;

                    $child
                        ->addChild('carousel', ['route' => 'app_admin_carousel_index'])
                        ->setLabel('Caroussels')
                        ->setLabelAttribute('icon', 'sliders horizontal')
                    ;

                    break ;
                case 'customers':
                    $child
                        ->addChild('supplier_account', ['route' => 'app_admin_supplier_account_index'])
                        ->setLabel('Comptes grossistes')
                        ->setLabelAttribute('icon', 'industry')
                    ;
                    $child
                        ->addChild('restaurants', ['route' => 'app_admin_restaurant_index'])
                        ->setLabel('Restaurants')
                        ->setLabelAttribute('icon', 'food')
                    ;
                    $child
                        ->addChild('prospect', ['route' => 'app_admin_admin_prospect_index'])
                        ->setLabel('Prospects')
                        ->setLabelAttribute('icon', 'user')
                    ;
//                    $child
//                        ->addChild('advanced', ['route' => 'app_admin_advanceduser_index'])
//                        ->setLabel('Prospects x Hubspot')
//                        ->setLabelAttribute('icon', 'search')
//                    ;
                    $child
                        ->addChild('supplier_account_log', ['route' => 'app_admin_supplier_account_log_index'])
                        ->setLabel('Ouvertures de compte')
                        ->setLabelAttribute('icon', 'linkify')
                    ;
                    $child
                        ->addChild('litigation', ['route' => 'app_admin_litigation_index'])
                        ->setLabel('Litiges')
                        ->setLabelAttribute('icon', 'bomb')
                    ;
                    $child
                        ->addChild('mailer', ['route' => 'app_admin_mailer_index'])
                        ->setLabel('E-Mailing')
                        ->setLabelAttribute('icon', 'send')
                    ;
                    $child
                        ->addChild('email', ['route' => 'app_admin_email_index'])
                        ->setLabel('Logs des emails')
                        ->setLabelAttribute('icon', 'list')
                    ;
                    $child
                        ->addChild('user_activity_log', ['route' => 'app_admin_user_activity_log_index'])
                        ->setLabel('Parcours utilisateur')
                        ->setLabelAttribute('icon', 'blind')
                    ;
//                    $child
//                        ->addChild('prospect_sales_statistics', ['route' => 'app_admin_prospect_statistics_index'])
//                        ->setLabel('Statistiques achats')
//                        ->setLabelAttribute('icon', 'money bill')
//                    ;
                    $child
                        ->addChild('prospect_source', ['route' => 'app_admin_prospect_source_index'])
                        ->setLabel('Prospect source')
                        ->setLabelAttribute('icon', 'money bill')
                    ;
                    $child
                        ->addChild('reporting', ['route' => 'app_admin_reporting_index'])
                        ->setLabel('Reporting')
                        ->setLabelAttribute('icon', 'chart area')
                    ;
                    $child
                        ->addChild('customer_succes', ['route' => 'app_admin_customer_succes_index'])
                        ->setLabel('Customer Success')
                        ->setLabelAttribute('icon', 'smile')
                    ;
                    $child
                        ->addChild('invoice-import', ['route' => 'app_admin_invoice_import_index'])
                        ->setLabel("Rapport d'intégration factures")
                        ->setLabelAttribute('icon', 'money bill outline')
                    ;
                    break ;
                case 'marketing':
                    $menu->removeChild($child);

//                    $child
//                        ->addChild('emailing_statistics', ['route' => 'app_admin_emailing_statistics_index'])
//                        ->setLabel('Statistiques emailing')
//                        ->setLabelAttribute('icon', 'mail')
//                    ;
//                    $child
//                        ->addChild('prospect_marketing', ['route' => 'app_admin_prospect_marketing_index'])
//                        ->setLabel('Statistiques acquisition')
//                        ->setLabelAttribute('icon', 'industry')
//                    ;
//                    $child
//                        ->addChild('facebook_import', ['route' => 'app_admin_facebook_import_index'])
//                        ->setLabel('Importer leads facebook')
//                        ->setLabelAttribute('icon', 'download')
//                    ;
//                    $child
//                        ->addChild('onboarding', ['route' => 'app_admin_onboarding_index'])
//                        ->setLabel('Onboarding - Benchmark')
//                        ->setLabelAttribute('icon', 'calculator')
//                    ;
//
//                    $child
//                        ->addChild('contact_request', ['route' => 'app_admin_contact_request_index'])
//                        ->setLabel('Demande de contact')
//                        ->setLabelAttribute('icon', 'phone volume')
//                    ;
                    break ;
                case 'catalog':
                    $child
                        ->addChild('product_matching', ['route' => 'app_admin_product_matching_index'])
                        ->setLabel('Matching des produits')
                        ->setLabelAttribute('icon', 'microchip')
                    ;

//                    $child
//                        ->addChild('suppliers', ['route' => 'app_admin_supplier_index'])
//                        ->setLabel('Grossistes')
//                        ->setLabelAttribute('icon', 'users')
//                    ;

                    $child
                        ->addChild('suppliers2', ['route' => 'app_admin_suppliers_index'])
                        ->setLabel('Grossistes V2')
                        ->setLabelAttribute('icon', 'truck')
                    ;

//                    $child
//                        ->addChild('variant', ['route' => 'app_admin_variant_index'])
//                        ->setLabel('Variantes')
//                        ->setLabelAttribute('icon', 'cube')
//                    ;

//                    $child
//                        ->addChild('variant_informations', ['route' => 'app_admin_supplier_product_variant_informations_index'])
//                        ->setLabel('Correspondances Catalogue')
//                        ->setLabelAttribute('icon', 'copy')
//                    ;

                    $child
                        ->addChild('page_check', ['route' => 'app_admin_page_check_index'])
                        ->setLabel('Modifier le catalogue')
                        ->setLabelAttribute('icon', 'sitemap')
                    ;

                    $child
                        ->addChild('change_log', ['route' => 'app_admin_catalog_change_log_index'])
                        ->setLabel('Traces des changements')
                        ->setLabelAttribute('icon', 'fork')
                    ;

                    $child
                        ->addChild('mercuriale', ['route' => 'app_admin_mercuriale_index'])
                        ->setLabel('Mercuriales à télécharger')
                        ->setLabelAttribute('icon', 'cloud download')
                    ;

//                    $child
//                        ->addChild('import', ['route' => 'app_admin_import_index'])
//                        ->setLabel('Importer un mercuriale')
//                        ->setLabelAttribute('icon', 'download')
//                    ;

                    $child
                        ->addChild('product-import', ['route' => 'app_admin_product_import_index'])
                        ->setLabel('Importer un mercuriale V2')
                        ->setLabelAttribute('icon', 'download')
                    ;

                    $child
                        ->addChild('mercurial', ['route' => 'app_admin_mercurial_index'])
                        ->setLabel('Tester des regexp fichier')
                        ->setLabelAttribute('icon', 'bug')
                    ;

                    $child
                        ->addChild('supplier-prices', ['route' => 'app_admin_supplier_prices_index'])
                        ->setLabel('Gestion des prix')
                        ->setLabelAttribute('icon', 'eur')
                    ;

                    $child
                        ->addChild('supplier_sales_statistics', ['route' => 'app_admin_supplier_statistics_index'])
                        ->setLabel('Statistiques grossistes')
                        ->setLabelAttribute('icon', 'money bill')
                    ;
                    break ;
            }
        }
    }
}
