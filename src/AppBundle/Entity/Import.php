<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Core\Model\Channel;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class Import implements ResourceInterface
{
    // status
    const STATUS_AWAITING = 1;
    const STATUS_VALIDATED = 2;
    const STATUS_PARTIALLY_IMPORTED = 3;
    const STATUS_IMPORTED = 4;

    const STATUS_LABEL = [
        1 => 'AWAITING',
        2 => 'VALIDATED',
        3 => 'PARTIALLY_IMPORTED',
        4 => 'IMPORTED',
    ];

    const MERCURIAL	          = 1;
    const BON_DE_COMMANDE	  = 2;
    const BON_DE_LIVRAISON	  = 3;
    const FACTURE             = 4;


    private static $file_type_names = array(
        self::MERCURIAL                         => 'Mercurial',
        self::BON_DE_COMMANDE                   => 'Bon de commande',
        self::BON_DE_LIVRAISON                  => 'Bon de livraison',
        self::FACTURE                           => 'Facture',
    );

    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var \DateTime
     */
    protected $pricesDate;

    /**
     * @var string
     */
    protected $fileType;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $fetcher;

    /**
     * @var Channel
     */
    protected $channel;

    /**
     * @var Supplier
     */
    protected $supplier;

    /**
     * @var Restaurant
     */
    protected $restaurant;

    /**
     * @var int
     */
    protected $mercurialeId;

    /**
     * @var ImportLine[]|ArrayCollection
     */
    protected $lines;

    /** @var boolean */
    protected $promo = false;

    /**
     * constructor.
     *
     * initialize ArrayCollection
     */
    public function __construct()
    {
        $this->status = self::STATUS_AWAITING;
        $this->lines = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status int
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * status label
     *
     * @return string
     */
    public function getStatusLabel()
    {
        return self::STATUS_LABEL[$this->status];
    }

    /**
     * Get the literal type of the file
     *
     * @return string The named type in english
     */
    public function getFileTypeLabel()
    {
        return self::$file_type_names[$this->fileType];
    }

    /**
     * Set fileType
     *
     * @param string $fileType
     *
     * @return Company
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;

        return $this;
    }

    /**
     * Get fileType
     *
     * @return string
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    public function getFetcher()
    {
        return $this->fetcher;
    }

    public function setFetcher($fetcher)
    {
        $this->fetcher = $fetcher;

        return $this;
    }

    /**
     * @return Channel
     */
    public function getChannel()
    {
        return $this->channel;

    }

    /**
     * @param Channel $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier = null)
    {
        $this->supplier = $supplier;
        //$this->setChannel($supplier->getChannel());

        return $this;
    }

    /**
     * @return Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * @param Restaurant $restaurant
     */
    public function setRestaurant(Restaurant $restaurant = null)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * @return int
     */
    public function getMercurialeId(): int
    {
        return $this->mercurialeId;
    }

    /**
     * @param int $mercurialeId
     */
    public function setMercurialeId(int $mercurialeId)
    {
        $this->mercurialeId = $mercurialeId;
        return $this;
    }

    public function getLines()
    {
        return $this->lines;
    }

    public function addLine(ImportLine $line)
    {
        $this->lines->add($line);
        $line->setImport($this);

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPricesDate()
    {
        return $this->pricesDate;
    }

    /**
     * @param Channel $channel
     */
    public function setPricesDate($pricesDate)
    {
        $this->pricesDate = new \DateTime($pricesDate);
        return $this;
    }

    /**
     * @return bool
     */
    public function isPromo(): bool
    {
        return $this->promo;
    }

    /**
     * @param bool $promo
     */
    public function setPromo(bool $promo): void
    {
        $this->promo = $promo;
    }
}
