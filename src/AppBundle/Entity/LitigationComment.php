<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Blameable\Traits\BlameableEntity;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * LitigationComment
 */
class LitigationComment implements ResourceInterface
{
    use TimestampableTrait,
        BlameableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var Litigation
     */
    private $litigation;

    /**
     * @var string
     */
    private $content;

    /**
     * @var LitigationCommentPicture[]|ArrayCollection
     */
    private $pictures;

    /**
     * constructor.
     *
     * initialize ArrayCollection
     */
    public function __construct()
    {
        $this->pictures = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set litigation.
     *
     * @param Litigation $litigation
     *
     * @return LitigationComment
     */
    public function setLitigation(Litigation $litigation)
    {
        $this->litigation = $litigation;

        return $this;
    }

    /**
     * Get litigation.
     *
     * @return Litigation
     */
    public function getLitigation()
    {
        return $this->litigation;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return LitigationComment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return LitigationCommentPicture[]|ArrayCollection
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    public function addPicture(LitigationCommentPicture $picture)
    {
        if (! $this->pictures->contains($picture)) {
            $picture->setComment($this);
            $this->pictures->add($picture);
        }

        return $this;
    }

    /**
     * @param LitigationCommentPicture[]|ArrayCollection $pictures
     * @return LitigationComment
     */
    public function setPictures($pictures)
    {
        foreach($pictures as $picture) {
            $this->addPicture($picture);
        }
        return $this;
    }


}
