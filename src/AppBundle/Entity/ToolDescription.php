<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;

/**
 * ToolDescription
 */
class ToolDescription implements ResourceInterface, TranslatableInterface
{
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
        getTranslation as private doGetTranslation;
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $icon = '';

    /**
     * @var Tool
     */
    private $tool;

    /**
     * @var int
     */
    private $position = 0;

    /**
     * Tool constructor.
     */
    public function __construct()
    {
        $this->initializeTranslationsCollection();
    }

    /**
     * {@inheritdoc}
     */
    protected function createTranslation()
    {
        return new ToolDescriptionTranslation();
    }

    /**
     * @param null|string $locale
     *
     * @return ToolDescriptionTranslation
     */
    public function getTranslation(?string $locale = null): TranslationInterface
    {
        return $this->doGetTranslation($locale);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return ToolDescription
     */
    public function setIcon($icon) : ToolDescription
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon() : string
    {
        return $this->icon;
    }

    /**
     * @return Tool
     */
    public function getTool() : Tool
    {
        return $this->tool;
    }

    /**
     * @param Tool $tool
     *
     * @return ToolDescription
     */
    public function setTool(Tool $tool) : ToolDescription
    {
        $this->tool = $tool;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition() : int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return ToolDescription
     */
    public function setPosition(int $position) : ToolDescription
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->getTranslation()->getTitle();
    }

    /**
     * @param string $title
     *
     * @return ToolDescription
     */
    public function setTitle(string $title) : ToolDescription
    {
        $this->getTranslation()->setTitle($title);

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->getTranslation()->getDescription();
    }

    public function setDescription(string $description) : ToolDescription
    {
        $this->getTranslation()->setDescription($description);

        return $this;
    }
}
