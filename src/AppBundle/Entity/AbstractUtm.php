<?php

namespace AppBundle\Entity;

abstract class AbstractUtm
{
    /**
     * @var string|null
     */
    protected $ip;

    /**
     * @var string|null
     */
    protected $utmEmail;

    /**
     * @var string|null
     */
    protected $utmSource;

    /**
     * @var string|null
     */
    protected $utmMedium;

    /**
     * @var string|null
     */
    protected $utmCampaign;

    /**
     * @var string|null
     */
    protected $utmTerm;

    /**
     * @var string|null
     */
    protected $utmContent;

    /**
     * @return null|string
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param null|string $ip
     */
    public function setIp(?string $ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @return null|string
     */
    public function getUtmEmail(): ?string
    {
        return $this->utmEmail;
    }

    /**
     * @param null|string $utmEmail
     */
    public function setUtmEmail(?string $utmEmail): void
    {
        $this->utmEmail = $utmEmail;
    }

    /**
     * @return null|string
     */
    public function getUtmSource(): ?string
    {
        return $this->utmSource;
    }

    /**
     * @param null|string $utmSource
     */
    public function setUtmSource(?string $utmSource): void
    {
        $this->utmSource = $utmSource;
    }

    /**
     * @return null|string
     */
    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }

    /**
     * @param null|string $utmMedium
     */
    public function setUtmMedium(?string $utmMedium): void
    {
        $this->utmMedium = $utmMedium;
    }

    /**
     * @return null|string
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    /**
     * @param null|string $utmCampaign
     */
    public function setUtmCampaign(?string $utmCampaign): void
    {
        $this->utmCampaign = $utmCampaign;
    }

    /**
     * @return null|string
     */
    public function getUtmTerm(): ?string
    {
        return $this->utmTerm;
    }

    /**
     * @param null|string $utmTerm
     */
    public function setUtmTerm(?string $utmTerm): void
    {
        $this->utmTerm = $utmTerm;
    }

    /**
     * @return null|string
     */
    public function getUtmContent(): ?string
    {
        return $this->utmContent;
    }

    /**
     * @param null|string $utmContent
     */
    public function setUtmContent(?string $utmContent): void
    {
        $this->utmContent = $utmContent;
    }
}