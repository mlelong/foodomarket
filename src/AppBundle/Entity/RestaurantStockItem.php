<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * RestaurantStock
 *
 */
class RestaurantStockItem implements ResourceInterface, ShoppingItemInterface
{
    use TimestampableTrait;

    /**
     * @var int
     *
     */
    private $id;

    /**
     * @var bool
     *
     */
    private $deleted = false;

    /**
     * @var float
     */
    private $stock;

    /**
     * The stock unit (G, KG, PC)
     * @var string
     */
    private $stockUnit;

    /**
     * @var RestaurantStock
     */
    private $restaurantStock;

    /**
     * @var ProductVariant
     */
    protected $productVariant;

    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * @var string
     */
    private $note;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stock
     *
     * @param float $stock
     *
     * @return RestaurantStockItem
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return float
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return RestaurantStockItem
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set restaurantStock
     *
     * @param RestaurantStock $restaurantStock
     *
     * @return RestaurantStockItem
     */
    public function setRestaurantStock(RestaurantStock $restaurantStock = null)
    {
        $this->restaurantStock = $restaurantStock;

        return $this;
    }

    /**
     * Get restaurantStock
     *
     * @return RestaurantStock
     */
    public function getRestaurantStock()
    {
        return $this->restaurantStock;
    }

    /**
     * Set productVariant
     *
     * @param ProductVariant $productVariant
     *
     * @return RestaurantStockItem
     */
    public function setProductVariant(ProductVariant $productVariant = null)
    {
        $this->productVariant = $productVariant;

        return $this;
    }

    /**
     * Get productVariant
     *
     * @return ProductVariant
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    /**
     * @return string
     */
    public function getStockUnit(): string
    {
        return $this->stockUnit;
    }

    /**
     * @param string $stockUnit
     * @return $this
     */
    public function setStockUnit(string $stockUnit)
    {
        $this->stockUnit = $stockUnit;

        return $this;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     * @return $this
     */
    public function setSupplier(?Supplier $supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote(?string $note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string {"KG", "PC"}
     */
    public function getUnit()
    {
        return $this->stockUnit;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->getStock();
    }
}
