<?php

namespace AppBundle\Entity;

use AppBundle\Constant\PaymentMode;
use Doctrine\Common\Collections\ArrayCollection;

use Exception;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Channel\Model\Channel;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * Class Supplier
 * @package AppBundle\Entity
 * @Vich\Uploadable
 */
class Supplier extends SellerUser implements ResourceInterface
{
    use TimestampableTrait;

    const CAT_DEFAULT                   = "DEFAULT";
    const CAT_FRUITS_LEGUMES            = 'FRUITS_LEGUMES';
    const CAT_MAREE                     = 'MAREE';
    const CAT_EPICERIE                  = 'EPICERIE';
    const CAT_BOUCHERIE                 = 'BOUCHERIE';
    const CAT_TRAITEUR                  = 'TRAITEUR';
    const CAT_CREMERIE                  = 'CREMERIE';
    const CAT_CAVE                      = 'CAVE';
    const CAT_FRUITS_LEGUMES_BIO        = 'FRUITS_LEGUMES_BIO';
    const CAT_CONSOMMABLES              = 'CONSOMMABLES';

    /**
     * @var string
     *
     */
    private $fileType;

    /**
     * @var string
     *
     */
    private $name;

    /**
     * @var bool
     *
     */
    private $enabled;

    /**
     * @var string
     *
     */
    private $street;

    /**
     * @var string
     *
     */
    private $city;

    /**
     * @var string
     *
     */
    private $postcode;

    /**
     * @var string
     *
     */
    private $countryCode;

    /**
     * @var string
     *
     */
    private $email;

    /**
     * @var string
     *
     */
    private $mercurialPattern;

    /**
     * @var string
     *
     */
    private $invoicePattern;

    /**
     * @var Channel
     *
     */
    private $channel;

    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var float
     */
    private $shippingCost = 0.0;

    /**
     * @var float : amountForFreeShippingCost
     */
    private $freePort = 0.0;

    /**
     * @var float
     */
    private $minimumOrder = 0.0;

    /** @var string|null */
    private $phone;

    /**
     * @var string
     */
    private $howToOrder;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $deliveryTerms;

    /**
     * @var boolean
     */
    private $paymentOnDelivery = false;

    /**
     * @var boolean
     */
    private $paymentOnline = false;

    /**
     * @var boolean
     */
    private $paymentOffline = false;

    /** @var bool */
    private $autoBind = false;

    /**
     * @var boolean
     */
    private $accountOpening = false;

    /**
     * @var boolean
     */
    private $applicationNeeded = false;

    /**
     * @var boolean
     */
    private $deliveryOnSunday = false;

    /**
     * @var boolean
     */
    private $deliveryOnMonday = false;

    /**
     * @var boolean
     */
    private $deliveryOnTuesday = false;

    /**
     * @var boolean
     */
    private $deliveryOnWednesday = false;

    /**
     * @var boolean
     */
    private $deliveryOnThursday = false;

    /**
     * @var boolean
     */
    private $deliveryOnFriday = false;

    /**
     * @var boolean
     */
    private $deliveryOnSaturday = false;

    /** @var string|null */
    private $deliveryZones;

    /**
     * @var Supplier|null
     */
    private $facadeSupplier;

    /**
     * @var Supplier[]|ArrayCollection
     */
    private $partnerSuppliers;

    /**
     * @var string|null
     */
    private $orderLimitTime;

    /**
     * @Vich\UploadableField(mapping="supplier_logo", fileNameProperty="logoFileName")
     * @var File|null
     */
    private $logo;

    /** @var string|null */
    private $logoFileName;

    /**
     * @Vich\UploadableField(mapping="supplier_sepa", fileNameProperty="sepaFileName")
     * @var File|null
     */
    private $sepa;

    /**
     * @Vich\UploadableField(mapping="supplier_rib", fileNameProperty="ribFileName")
     * @var File|null
     */
    private $rib;

    /** @var string|null */
    private $sepaFileName;

    /** @var string|null */
    private $ribFileName;

    /** @var string[] */
    private $commercialEmails = [];

    /** @var string[] */
    private $orderEmails = [];

    /** @var string|null */
    private $orderModality;

    /** @var string|null */
    private $orderAdditionalElements;

    // Les modes de paiement
    /** @var boolean */
    private $paymentByCheque = false;

    /** @var boolean */
    private $paymentByCash = false;

    /** @var boolean */
    private $paymentByCreditCard = false;

    /** @var boolean */
    private $paymentByBankTransfer = false;

    /** @var boolean */
    private $paymentByDirectDebit = false;

    /** @var bool */
    private $partner = false;

    /** @var bool */
    private $createdByUser = false;

    /** @var float */
    private $creditCardPriceMultiplier = 1.0;

    /** @var string|null */
    private $lyraUuid;

    /** @var string|null */
    private $lyraDevUuid;

    /** @var string */
    private $chronofreshDelivery;

    /**
     * constructor.
     *
     * initialize Boolean
     * initialize ArrayCollection
     */
    public function __construct()
    {
        $this->enabled = false;
        $this->partnerSuppliers = new ArrayCollection();
    }

    public function __tostring()
    {
        return $this->getName();
    }

    /**
     * Set fileType
     *
     * @param string $fileType
     *
     * @return Supplier
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;

        return $this;
    }

    /**
     * Get fileType
     *
     * @return string
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Supplier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Supplier
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Supplier
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Supplier
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Supplier
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return Supplier
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Supplier
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mercurialPattern
     *
     * @param string $mercurialPattern
     *
     * @return Supplier
     */
    public function setMercurialPattern($mercurialPattern)
    {
        $this->mercurialPattern = $mercurialPattern;

        return $this;
    }

    /**
     * Get mercurialPattern
     *
     * @return string
     */
    public function getMercurialPattern()
    {
        return $this->mercurialPattern;
    }

    /**
     * Set invoicePattern
     *
     * @param string $invoicePattern
     *
     * @return Supplier
     */
    public function setInvoicePattern($invoicePattern)
    {
        $this->invoicePattern = $invoicePattern;

        return $this;
    }

    /**
     * Get invoicePattern
     *
     * @return string
     */
    public function getInvoicePattern()
    {
        return $this->invoicePattern;
    }

    /**
     * Set channel.
     *
     * @param Channel $channel
     *
     * @return Supplier
     */
    public function setChannel(Channel $channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel.
     *
     * @return Channel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    public function addCategory(string $category)
    {
        $categoryCollection = new ArrayCollection($this->categories);

        if (!$categoryCollection->contains($category)) {
            $categoryCollection->add($category);
        }

        $this->categories = $categoryCollection->toArray();
    }

    /**
     * @return float
     */
    public function getShippingCost(): float
    {
        return $this->shippingCost;
    }

    /**
     * @param float $shippingCost
     */
    public function setShippingCost(float $shippingCost): void
    {
        $this->shippingCost = $shippingCost;
    }

    /**
     * @return float
     */
    public function getFreePort(): float
    {
        return $this->freePort;
    }

    /**
     * @param float $freePort
     */
    public function setFreePort(float $freePort): void
    {
        $this->freePort = $freePort;
    }

    public function getCoordinates()
    {
        return "{$this->getName()}, {$this->getStreet()} {$this->getPostcode()} {$this->getCity()}<br/>{$this->getEmail()} - {$this->getPhone()}";
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * Easing function to get display name of category
     *
     * @param string $category
     * @return string
     */
    public static function getDisplayCategory(string $category)
    {
        switch ($category) {
            case self::CAT_FRUITS_LEGUMES:
                return 'Fruits & Légumes';
            case self::CAT_CREMERIE:
                return 'Crèmerie';
            case self::CAT_BOUCHERIE:
                return 'Boucherie';
            case self::CAT_MAREE:
                return 'Marée';
            case self::CAT_EPICERIE:
                return 'Epicerie';
            case self::CAT_CAVE:
                return 'Cave';
            case self::CAT_FRUITS_LEGUMES_BIO:
                return 'Fruits & Légumes Bio';
            case self::CAT_TRAITEUR:
                return 'Traiteur';
            case self::CAT_CONSOMMABLES:
                return 'Consommables';
            default:
                return $category;
        }
    }

    /**
     * @return string|null
     */
    public function getHowToOrder()
    {
        return $this->howToOrder;
    }

    /**
     * @param string|null $howToOrder
     */
    public function setHowToOrder($howToOrder)
    {
        $this->howToOrder = $howToOrder;
    }

    /**
     * @return float
     */
    public function getMinimumOrder(): float
    {
        return $this->minimumOrder;
    }

    /**
     * @param float $minimumOrder
     * @return Supplier
     */
    public function setMinimumOrder(float $minimumOrder): Supplier
    {
        $this->minimumOrder = $minimumOrder;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Supplier
     */
    public function setDescription(?string $description): Supplier
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryTerms(): ?string
    {
        return $this->deliveryTerms;
    }

    /**
     * @param string $deliveryTerms
     * @return Supplier
     */
    public function setDeliveryTerms(?string $deliveryTerms): Supplier
    {
        $this->deliveryTerms = $deliveryTerms;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaymentOnDelivery(): ?bool
    {
        return $this->paymentOnDelivery;
    }

    /**
     * @param bool $paymentOnDelivery
     * @return Supplier
     */
    public function setPaymentOnDelivery(bool $paymentOnDelivery): Supplier
    {
        $this->paymentOnDelivery = $paymentOnDelivery;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaymentOnline(): ?bool
    {
        return $this->paymentOnline;
    }

    /**
     * @param bool $paymentOnline
     * @return Supplier
     */
    public function setPaymentOnline(bool $paymentOnline): Supplier
    {
        $this->paymentOnline = $paymentOnline;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaymentOffline(): ?bool
    {
        return $this->paymentOffline;
    }

    /**
     * @param bool $paymentOffline
     * @return Supplier
     */
    public function setPaymentOffline(bool $paymentOffline): Supplier
    {
        $this->paymentOffline = $paymentOffline;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAccountOpening(): ?bool
    {
        return $this->accountOpening;
    }

    /**
     * @param bool $accountOpening
     * @return Supplier
     */
    public function setAccountOpening(bool $accountOpening): Supplier
    {
        $this->accountOpening = $accountOpening;
        return $this;
    }

    /**
     * @return bool
     */
    public function isApplicationNeeded(): ?bool
    {
        return $this->applicationNeeded;
    }

    /**
     * @param bool $applicationNeeded
     * @return Supplier
     */
    public function setApplicationNeeded(bool $applicationNeeded): Supplier
    {
        $this->applicationNeeded = $applicationNeeded;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryOnSunday(): ?bool
    {
        return $this->deliveryOnSunday;
    }

    /**
     * @param bool $deliveryOnSunday
     * @return Supplier
     */
    public function setDeliveryOnSunday(bool $deliveryOnSunday): Supplier
    {
        $this->deliveryOnSunday = $deliveryOnSunday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryOnMonday(): ?bool
    {
        return $this->deliveryOnMonday;
    }

    /**
     * @param bool $deliveryOnMonday
     * @return Supplier
     */
    public function setDeliveryOnMonday(bool $deliveryOnMonday): Supplier
    {
        $this->deliveryOnMonday = $deliveryOnMonday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryOnTuesday(): ?bool
    {
        return $this->deliveryOnTuesday;
    }

    /**
     * @param bool $deliveryOnTuesday
     * @return Supplier
     */
    public function setDeliveryOnTuesday(bool $deliveryOnTuesday): Supplier
    {
        $this->deliveryOnTuesday = $deliveryOnTuesday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryOnWednesday(): ?bool
    {
        return $this->deliveryOnWednesday;
    }

    /**
     * @param bool $deliveryOnWednesday
     * @return Supplier
     */
    public function setDeliveryOnWednesday(bool $deliveryOnWednesday): Supplier
    {
        $this->deliveryOnWednesday = $deliveryOnWednesday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryOnThursday(): ?bool
    {
        return $this->deliveryOnThursday;
    }

    /**
     * @param bool $deliveryOnThursday
     * @return Supplier
     */
    public function setDeliveryOnThursday(bool $deliveryOnThursday): Supplier
    {
        $this->deliveryOnThursday = $deliveryOnThursday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryOnFriday(): ?bool
    {
        return $this->deliveryOnFriday;
    }

    /**
     * @param bool $deliveryOnFriday
     * @return Supplier
     */
    public function setDeliveryOnFriday(bool $deliveryOnFriday): Supplier
    {
        $this->deliveryOnFriday = $deliveryOnFriday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeliveryOnSaturday(): ?bool
    {
        return $this->deliveryOnSaturday;
    }

    /**
     * @param bool $deliveryOnSaturday
     * @return Supplier
     */
    public function setDeliveryOnSaturday(bool $deliveryOnSaturday): Supplier
    {
        $this->deliveryOnSaturday = $deliveryOnSaturday;
        return $this;
    }

    /**
     * @return Supplier|null
     */
    public function getFacadeSupplier(): ?Supplier
    {
        if ($this->enabled) {
            return $this;
        }

        return $this->facadeSupplier;
    }

    /**
     * @param Supplier|null $facadeSupplier
     */
    public function setFacadeSupplier(?Supplier $facadeSupplier): void
    {
        $this->facadeSupplier = $facadeSupplier;
    }

    /**
     * @return Supplier[]|ArrayCollection
     */
    public function getPartnerSuppliers()
    {
        return $this->partnerSuppliers;
    }

    /**
     * @param Supplier[]|ArrayCollection $partnerSuppliers
     */
    public function setPartnerSuppliers($partnerSuppliers): void
    {
        $this->partnerSuppliers = $partnerSuppliers;
    }

    public function addPartnerSupplier(Supplier $supplier): void
    {
        if (!$this->partnerSuppliers->contains($supplier)) {
            $this->partnerSuppliers->add($supplier);
        }
    }

    public function removePartnerSupplier(Supplier $supplier): void
    {
        $this->partnerSuppliers->removeElement($supplier);
    }

    /**
     * @return null|string
     */
    public function getOrderLimitTime(): ?string
    {
        return $this->orderLimitTime;
    }

    /**
     * @param null|string $orderLimitTime
     */
    public function setOrderLimitTime(?string $orderLimitTime): void
    {
        $this->orderLimitTime = $orderLimitTime;
    }

    /**
     * @return null|File
     */
    public function getLogo(): ?File
    {
        return $this->logo;
    }

    /**
     * @param null|File $logo
     */
    public function setLogo(?File $logo): void
    {
        $this->logo = $logo;

        if ($logo !== null) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return null|string
     */
    public function getLogoFileName(): ?string
    {
        return $this->logoFileName;
    }

    /**
     * @param null|string $logoFileName
     */
    public function setLogoFileName(?string $logoFileName): void
    {
        $this->logoFileName = $logoFileName;
    }

    /**
     * @return null|File
     */
    public function getSepa(): ?File
    {
        return $this->sepa;
    }

    /**
     * @param null|File $sepa
     */
    public function setSepa(?File $sepa): void
    {
        $this->sepa = $sepa;

        if ($sepa !== null) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return null|string
     */
    public function getSepaFileName(): ?string
    {
        return $this->sepaFileName;
    }

    /**
     * @param null|string $sepaFileName
     */
    public function setSepaFileName(?string $sepaFileName): void
    {
        $this->sepaFileName = $sepaFileName;
    }

    /**
     * @param null|File $rib
     */
    public function setRib(?File $rib): void
    {
        $this->rib = $rib;

        if ($rib !== null) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return null|File
     */
    public function getRib(): ?File
    {
        return $this->rib;
    }

    /**
     * @return string|null
     */
    public function getRibFileName(): ?string
    {
        return $this->ribFileName;
    }

    /**
     * @param string|null $ribFileName
     */
    public function setRibFileName(?string $ribFileName): void
    {
        $this->ribFileName = $ribFileName;
    }

    /**
     * @return string[]
     */
    public function getCommercialEmails(): array
    {
        return $this->commercialEmails;
    }

    /**
     * @param string[] $commercialEmails
     */
    public function setCommercialEmails(array $commercialEmails): void
    {
        $this->commercialEmails = $commercialEmails;
    }

    public function addCommercialEmail(?string $commercialEmail)
    {
        $this->commercialEmails = array_unique(array_merge($this->commercialEmails, [$commercialEmail]));
    }

    public function removeCommercialEmail(string $commercialEmail)
    {
        $this->commercialEmails = array_filter($this->commercialEmails, function(string $email) use($commercialEmail) {
            return $commercialEmail !== $email;
        });
    }

    /**
     * @return string[]
     */
    public function getOrderEmails(): array
    {
        return $this->orderEmails;
    }

    /**
     * @param string[] $orderEmails
     */
    public function setOrderEmails(array $orderEmails): void
    {
        $this->orderEmails = $orderEmails;
    }

    public function addOrderEmail(?string $orderEmail)
    {
        $this->orderEmails = array_unique(array_merge($this->orderEmails, [$orderEmail]));
    }

    public function removeOrderEmail(string $orderEmail)
    {
        $this->orderEmails = array_filter($this->orderEmails, function(string $email) use($orderEmail) {
            return $orderEmail !== $email;
        });
    }

    public function getDisplayName(bool $internalName = false)
    {
        if ($this->enabled || $this->facadeSupplier === null) {
            return $this->name;
        }

        if ($internalName) {
            switch ($this->id) {
                case 17:
                    return "{$this->facadeSupplier->getName()} - Grille 3";
                case 18:
                    return "{$this->facadeSupplier->getName()} - Grille 4";
                case 23:
                    return "{$this->facadeSupplier->getName()} - Grille SP";
            }
        }

        return $this->facadeSupplier->getName();
    }

    public function getOpeningDays() {
        $days = [];

        if ($this->isDeliveryOnSunday()) {
            $days[] = 0;
        }
        if ($this->isDeliveryOnMonday()) {
            $days[] = 1;
        }
        if ($this->isDeliveryOnTuesday()) {
            $days[] = 2;
        }
        if ($this->isDeliveryOnWednesday()) {
            $days[] = 3;
        }
        if ($this->isDeliveryOnThursday()) {
            $days[] = 4;
        }
        if ($this->isDeliveryOnFriday()) {
            $days[] = 5;
        }
        if ($this->isDeliveryOnSaturday()) {
            $days[] = 6;
        }

        return $days;
    }

    public function getClosingDays() {
        $days = [];

        if (!$this->isDeliveryOnSunday()) {
            $days[] = 0;
        }
        if (!$this->isDeliveryOnMonday()) {
            $days[] = 1;
        }
        if (!$this->isDeliveryOnTuesday()) {
            $days[] = 2;
        }
        if (!$this->isDeliveryOnWednesday()) {
            $days[] = 3;
        }
        if (!$this->isDeliveryOnThursday()) {
            $days[] = 4;
        }
        if (!$this->isDeliveryOnFriday()) {
            $days[] = 5;
        }
        if (!$this->isDeliveryOnSaturday()) {
            $days[] = 6;
        }

        return $days;
    }

    /**
     * @return string|null
     */
    public function getOrderModality(): ?string
    {
        return $this->orderModality;
    }

    /**
     * @param string|null $orderModality
     */
    public function setOrderModality(?string $orderModality): void
    {
        $this->orderModality = $orderModality;
    }

    /**
     * @return string|null
     */
    public function getOrderAdditionalElements(): ?string
    {
        return $this->orderAdditionalElements;
    }

    /**
     * @param string|null $orderAdditionalElements
     */
    public function setOrderAdditionalElements(?string $orderAdditionalElements): void
    {
        $this->orderAdditionalElements = $orderAdditionalElements;
    }

    /**
     * @return string|null
     */
    public function getDeliveryZones(): ?string
    {
        return $this->deliveryZones;
    }

    /**
     * @param string|null $deliveryZones
     */
    public function setDeliveryZones(?string $deliveryZones): void
    {
        $this->deliveryZones = $deliveryZones;
    }

    /**
     * @return bool
     */
    public function isPaymentByCheque(): bool
    {
        return $this->paymentByCheque;
    }

    /**
     * @param bool $paymentByCheque
     */
    public function setPaymentByCheque(bool $paymentByCheque): void
    {
        $this->paymentByCheque = $paymentByCheque;
    }

    /**
     * @return bool
     */
    public function isPaymentByCash(): bool
    {
        return $this->paymentByCash;
    }

    /**
     * @param bool $paymentByCash
     */
    public function setPaymentByCash(bool $paymentByCash): void
    {
        $this->paymentByCash = $paymentByCash;
    }

    /**
     * @return bool
     */
    public function isPaymentByCreditCard(): bool
    {
        return $this->paymentByCreditCard;
    }

    /**
     * @param bool $paymentByCreditCard
     */
    public function setPaymentByCreditCard(bool $paymentByCreditCard): void
    {
        $this->paymentByCreditCard = $paymentByCreditCard;
    }

    /**
     * @return bool
     */
    public function isPaymentByBankTransfer(): bool
    {
        return $this->paymentByBankTransfer;
    }

    /**
     * @param bool $paymentByBankTransfer
     */
    public function setPaymentByBankTransfer(bool $paymentByBankTransfer): void
    {
        $this->paymentByBankTransfer = $paymentByBankTransfer;
    }

    /**
     * @return bool
     */
    public function isPaymentByDirectDebit(): bool
    {
        return $this->paymentByDirectDebit;
    }

    /**
     * @param bool $paymentByDirectDebit
     */
    public function setPaymentByDirectDebit(bool $paymentByDirectDebit): void
    {
        $this->paymentByDirectDebit = $paymentByDirectDebit;
    }

    public function copySettingsFrom(Supplier $supplier)
    {
        $this->setStreet($supplier->getStreet());
        $this->setPostcode($supplier->getPostcode());
        $this->setCity($supplier->getCity());
        $this->setCountryCode($supplier->getCountryCode());
        $this->setDescription($supplier->getDescription());
        $this->setSepaFileName($supplier->getSepaFileName());
        $this->setRibFileName($supplier->getRibFileName());
    }

    /**
     * Returns all the payment modes
     *
     * @return array
     */
    public function getPaymentModes()
    {
        $paymentModes = [];

        try {
            if ($this->isPaymentByCash()) {
                $paymentModes[PaymentMode::CASH] = PaymentMode::toString(PaymentMode::CASH);
            }

            if ($this->isPaymentByCheque()) {
                $paymentModes[PaymentMode::CHEQUE] = PaymentMode::toString(PaymentMode::CHEQUE);
            }

            if ($this->isPaymentByCreditCard()) {
                $paymentModes[PaymentMode::CREDIT_CARD] = PaymentMode::toString(PaymentMode::CREDIT_CARD);
            }

            if ($this->isPaymentByBankTransfer()) {
                $paymentModes[PaymentMode::BANK_TRANSFER] = PaymentMode::toString(PaymentMode::BANK_TRANSFER);
            }

            if ($this->isPaymentByDirectDebit()) {
                $paymentModes[PaymentMode::DIRECT_DEBIT] = PaymentMode::toString(PaymentMode::DIRECT_DEBIT);
            }
        } catch (Exception $ignored) {
        }

        return $paymentModes;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isPartner(): bool
    {
        return $this->partner;
    }

    /**
     * @param bool $partner
     */
    public function setPartner(bool $partner): void
    {
        $this->partner = $partner;
    }

    /**
     * @return bool
     */
    public function isCreatedByUser(): bool
    {
        return $this->createdByUser;
    }

    /**
     * @param bool $createdByUser
     */
    public function setCreatedByUser(bool $createdByUser): void
    {
        $this->createdByUser = $createdByUser;
    }

    /**
     * @return float
     */
    public function getCreditCardPriceMultiplier(): float
    {
        return $this->creditCardPriceMultiplier;
    }

    /**
     * @param float $creditCardPriceMultiplier
     */
    public function setCreditCardPriceMultiplier(float $creditCardPriceMultiplier): void
    {
        $this->creditCardPriceMultiplier = $creditCardPriceMultiplier;
    }

    /**
     * @return string|null
     */
    public function getLyraUuid(): ?string
    {
        return $this->lyraUuid;
    }

    /**
     * @param string|null $lyraUuid
     */
    public function setLyraUuid(?string $lyraUuid): void
    {
        $this->lyraUuid = $lyraUuid;
    }

    /**
     * @return string|null
     */
    public function getLyraDevUuid(): ?string
    {
        return $this->lyraDevUuid;
    }

    /**
     * @param string|null $lyraDevUuid
     */
    public function setLyraDevUuid(?string $lyraDevUuid): void
    {
        $this->lyraDevUuid = $lyraDevUuid;
    }

    /**
     * @return bool
     */
    public function isAutoBind(): bool
    {
        return $this->autoBind;
    }

    /**
     * @param bool $autoBind
     */
    public function setAutoBind(bool $autoBind): void
    {
        $this->autoBind = $autoBind;
    }

    /**
     * @return bool
     */
    public function isChronofreshDelivery(): ?bool
    {
        return $this->chronofreshDelivery;
    }

    /**
     * @param bool $chronofreshDelivery
     */
    public function setChronofreshDelivery(bool $chronofreshDelivery): void
    {
        $this->chronofreshDelivery = $chronofreshDelivery;
    }
}
