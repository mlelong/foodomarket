<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\UtmTrait;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Serializable;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Resource\Model\ToggleableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Blameable\Traits\Blameable;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Prospect
 * @package AppBundle\Entity
 * @Vich\Uploadable
 */
class Prospect extends User implements Serializable
{
    use UtmTrait, TimestampableTrait, Blameable, ToggleableTrait;

    const TYPE_PROFESSIONNAL = "professionnal";
    const TYPE_INDIVIDUAL = "individual";

    /**
     * @var string|null
     *
     * @Assert\Email(message="showcase.contact_request.form.full.email.email", checkMX=true, groups={"app_register","app_login"})
     * @Assert\NotBlank(payload={"show"=false}, groups={"app_register"})
     */
    private $email;

    /**
     * @var string|null
     */
    private $supplementaryEmails = '';

    /**
     * @var bool
     */
    private $newsletterPrices = true;

    /**
     * @var string|null
     */
    private $source;

    /**
     * @var string
     */
    private $contactName;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"app_register"})
     */
    private $restaurantName;

    /**
     * @var string
     */
    private $siren;

    /**
     * @var string
     */
    private $deliveryHours;

    /**
     * @var string
     */
    private $currentSupplier;

    /**
     * @var string
     */
    private $address;

    /** @var string|null */
    private $zipCode;

    /** @var string|null */
    private $city;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $companyName;

    /**
     * @var int
     */
    private $turnover;

    /**
     * @var string[]
     */
    private $categories = [];

    /**
     * @var string
     */
    private $hubspotId;

    /**
     * @var bool
     */
    private $emailSubscribed = false;

    /**
     * @var ArrayCollection|SupplierCategory[]
     */
    private $suppliers;

    /**
     * @var Customer[]|ArrayCollection
     */
    private $customers;

    /**
     * @var ContactRequest[]|ArrayCollection
     */
    private $contactRequests;

    /**
     * @var bool
     */
    private $canUseArbitration = false;

    /**
     * @var int
     */
    private $registrationStep = 1;

    /**
     * @Vich\UploadableField(mapping="prospect_kbis", fileNameProperty="kbis")
     *
     * @var File|null
     */
    private $kbisFile;

    /**
     * @var string|null
     */
    private $kbis;

    /**
     * @Vich\UploadableField(mapping="prospect_rib", fileNameProperty="rib")
     *
     * @var File|null
     */
    private $ribFile;

    /** @var string|null */
    private $rib;

    /** @var string|null */
    private $iban;

    /**
     * @var string|null
     */
    private $hash;

    /**
     * Should be any of [MONSIEUR, MADAME, MADEMOISELLE] or null
     * @var string|null
     */
    private $civility = 'MONSIEUR';

    /**
     * @var string|null
     */
    private $plainPassword;

    /**
     * 0 = never sent
     * 1 = 30 minutes after first fill of popup
     * 2 = 1 day after first fill of popup
     * 3 = 7 days after first fill of popup
     *
     * @var int|null
     */
    private $popupRevival = 0;

    /**
     * 0 = never sent
     * 1 = 1 day after
     *
     * @var int
     */
    private $subscriptionRevival = 0;

    /**
     * 0 = never sent
     * 1 = 3 days after
     * 2 = 7 days after
     *
     * @var int
     */
    private $callbackRevival = 0;

    /**
     * @var string|null
     */
    private $job;

    /**
     * @var ArrayCollection|SupplierAccountLog[]
     */
    private $supplierAccountLogs;

    /**
     * @var string|null
     */
    private $hubspotDealStage;

    /**
     * @var bool
     */
    private $successFlagJ = false;

    /**
     * @var bool
     */
    private $successFlagJPlus7 = false;

    /**
     * @var bool
     */
    private $successFlagJPlus30 = false;

    /** @var string */
    private $type = self::TYPE_PROFESSIONNAL;

    /**
     * Prospect constructor.
     */
    public function __construct()
    {
        $this->categories = [];
        $this->suppliers = new ArrayCollection();
        $this->customers = new ArrayCollection();
        $this->contactRequests = new ArrayCollection();
        $this->supplierAccountLogs = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isNewsletterPrices(): bool
    {
        return $this->newsletterPrices;
    }

    /**
     * @param bool $newsletterPrices
     */
    public function setNewsletterPrices(bool $newsletterPrices): void
    {
        $this->newsletterPrices = $newsletterPrices;
    }

    /**
     * @return null|string
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param null|string $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    public function addSource(?string $source): void
    {
        if (empty($source)) {
            return;
        }

        if ($this->getSource() === null) {
            $sources = [];
        } else {
            $sources = explode(',', $this->getSource());
        }

        $newSources = explode(',', $source);

        foreach ($newSources as $source) {
            $sources[] = $source;
        }

        $this->setSource(implode(',', $sources));
    }

    /**
     * @return string
     */
    public function getSupplementaryEmails(): ?string
    {
        return $this->supplementaryEmails;
    }

    /**
     * @return string
     */
    public function getSupplementaryEmailsArray(): ?array
    {
        return explode(',', $this->supplementaryEmails);
    }

    /**
     * @param string $supplementaryEmails
     */
    public function setSupplementaryEmails(?string $supplementaryEmails): void
    {
        $this->supplementaryEmails = $supplementaryEmails;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return ['ROLE_PROSPECT'];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return '';
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->newsletterPrices,
            $this->source,
//            $this->canUseArbitration
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->newsletterPrices,
            $this->source,
//            $this->canUseArbitration
            ) = unserialize($serialized);
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     */
    public function setContactName(?string $contactName): void
    {
        $this->contactName = $contactName;
    }

    /**
     * @return string
     */
    public function getRestaurantName()
    {
        return $this->restaurantName;
    }

    /**
     * @param string $restaurantName
     */
    public function setRestaurantName(?string $restaurantName): void
    {
        $this->restaurantName = $restaurantName;
    }

    /**
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @param string $siren
     */
    public function setSiren(?string $siren): void
    {
        $this->siren = $siren;
    }

    /**
     * @return string
     */
    public function getDeliveryHours()
    {
        return $this->deliveryHours;
    }

    /**
     * @param string $deliveryHours
     */
    public function setDeliveryHours(?string $deliveryHours): void
    {
        $this->deliveryHours = $deliveryHours;
    }

    /**
     * @return string
     */
    public function getCurrentSupplier()
    {
        return $this->currentSupplier;
    }

    /**
     * @param string $currentSupplier
     */
    public function setCurrentSupplier(?string $currentSupplier): void
    {
        $this->currentSupplier = $currentSupplier;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(?string $mobile): void
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName(?string $companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return int
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * @param int $turnover
     */
    public function setTurnover(int $turnover): void
    {
        $this->turnover = $turnover;
    }

    /**
     * @return ArrayCollection|string[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param ArrayCollection|string[] $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @param string $category
     */
    public function addCategory(string $category): void
    {
        $this->categories = array_unique(array_merge($this->categories, [$category]));
    }

    /**
     * @param string $category
     */
    public function removeCategory(string $category): void
    {
        $this->categories = array_filter($this->categories, function ($value) use ($category) {
            return $value != $category;
        });
    }

    /**
     * @return string
     */
    public function getHubspotId()
    {
        return $this->hubspotId;
    }

    /**
     * @param string $hubspotId
     */
    public function setHubspotId(?string $hubspotId): void
    {
        $this->hubspotId = $hubspotId;
    }

    /**
     * @return bool
     */
    public function isEmailSubscribed()
    {
        return $this->emailSubscribed;
    }

    /**
     * @param bool $emailSubscribed
     */
    public function setEmailSubscribed(bool $emailSubscribed): void
    {
        $this->emailSubscribed = $emailSubscribed;
    }

    /**
     * @return SupplierCategory[]|ArrayCollection
     */
    public function getSuppliers()
    {
        return $this->suppliers;
    }

    /**
     * @param SupplierCategory[]|ArrayCollection $suppliers
     */
    public function setSuppliers($suppliers): void
    {
        $this->suppliers = $suppliers;
    }

    /**
     * @param SupplierCategory $supplier
     */
    public function addSupplier(SupplierCategory $supplier): void
    {
        if (!$this->suppliers->contains($supplier)) {
            $this->suppliers->add($supplier);
        }
    }

    /**
     * @param SupplierCategory $supplier
     */
    public function removeSupplier(SupplierCategory $supplier): void
    {
        $this->suppliers->removeElement($supplier);
    }

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param null|string $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return Customer[]|ArrayCollection
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * @param Customer[]|ArrayCollection $customers
     */
    public function setCustomers($customers): void
    {
        $this->customers = $customers;
    }

    public function addCustomer(Customer $customer)
    {
        if (!$this->customers->contains($customer)) {
            $customer->setProspect($this);
            $this->customers->add($customer);
        }
    }

    public function removeCustomer(Customer $customer)
    {
        $customer->setProspect(null);
        $this->customers->removeElement($customer);
    }

    /**
     * @return ContactRequest[]|ArrayCollection
     */
    public function getContactRequests()
    {
        return $this->contactRequests;
    }

    /**
     * @param ContactRequest[]|ArrayCollection $contactRequests
     */
    public function setContactRequests($contactRequests): void
    {
        $this->contactRequests = $contactRequests;
    }

    /**
     * @param ContactRequest $contactRequest
     */
    public function addContactRequest(ContactRequest $contactRequest)
    {
        if ($this->contactRequests && !$this->contactRequests->contains($contactRequest)) {
            $contactRequest->setProspect($this);
            $this->contactRequests->add($contactRequest);
        }
    }

    /**
     * @param ContactRequest $contactRequest
     */
    public function removeContactRequest(ContactRequest $contactRequest)
    {
        $contactRequest->setProspect(null);
        $this->contactRequests->removeElement($contactRequest);
    }

    public function getCurrentSuppliers(): string
    {
        $currentSuppliers = [];

        if (is_array($this->getContactRequests())) {
            foreach ($this->getContactRequests() as $contactRequest) {
                foreach ($contactRequest->getCategoriesFull() as $category) {
                    if (isset($category['suppliers']) && !empty($category['suppliers'])) {
                        foreach ($category['suppliers'] as $supplier) {
                            $currentSuppliers[] = $supplier['name'];
                        }
                    }

                    if (isset($category['otherSuppliers']) && !empty($category['otherSuppliers'])) {
                        $currentSuppliers[] = $category['otherSuppliers'];
                    }
                }
            }
        }

        return implode(', ', array_unique($currentSuppliers));
    }

    /**
     * @return bool
     */
    public function isCanUseArbitration(): bool
    {
        return $this->canUseArbitration;
    }

    /**
     * @param bool $canUseArbitration
     */
    public function setCanUseArbitration(bool $canUseArbitration): void
    {
        $this->canUseArbitration = $canUseArbitration;
    }

    /**
     * @return int
     */
    public function getRegistrationStep(): int
    {
        return $this->registrationStep;
    }

    /**
     * @param int $registrationStep
     */
    public function setRegistrationStep(int $registrationStep): void
    {
        $this->registrationStep = $registrationStep;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile|null $kbisFile
     * @throws \Exception
     */
    public function setKbisFile(?File $kbisFile = null): void
    {
        $this->kbisFile = $kbisFile;

        if (null !== $kbisFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getKbisFile()
    {
        return $this->kbisFile;
    }

    /**
     * @return string|null
     */
    public function getKbis()
    {
        return $this->kbis;
    }

    /**
     * @param string|null $kbis
     */
    public function setKbis($kbis)
    {
        $this->kbis = $kbis;
    }

    /**
     * @return File|null
     */
    public function getRibFile(): ?File
    {
        return $this->ribFile;
    }

    /**
     * @param File|null $ribFile
     * @throws \Exception
     */
    public function setRibFile(?File $ribFile): void
    {
        $this->ribFile = $ribFile;

        if (null !== $ribFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    /**
     * @return string|null
     */
    public function getRib(): ?string
    {
        return $this->rib;
    }

    /**
     * @param string|null $rib
     */
    public function setRib(?string $rib): void
    {
        $this->rib = $rib;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return string|null
     */
    public function getCivility(): ?string
    {
        return $this->civility;
    }

    /**
     * @param string|null $civility
     */
    public function setCivility(?string $civility): void
    {
        $this->civility = $civility;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     */
    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return int|null
     */
    public function getPopupRevival(): ?int
    {
        return $this->popupRevival;
    }

    /**
     * @param int|null $popupRevival
     */
    public function setPopupRevival(?int $popupRevival): void
    {
        $this->popupRevival = $popupRevival;
    }

    /**
     * @return int
     */
    public function getSubscriptionRevival(): int
    {
        return $this->subscriptionRevival;
    }

    /**
     * @param int $subscriptionRevival
     */
    public function setSubscriptionRevival(int $subscriptionRevival): void
    {
        $this->subscriptionRevival = $subscriptionRevival;
    }

    /**
     * @return int
     */
    public function getCallbackRevival(): int
    {
        return $this->callbackRevival;
    }

    /**
     * @param int $callbackRevival
     */
    public function setCallbackRevival(int $callbackRevival): void
    {
        $this->callbackRevival = $callbackRevival;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * @param string|null $zipCode
     */
    public function setZipCode(?string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getIban(): ?string
    {
        return $this->iban;
    }

    /**
     * @param string|null $iban
     */
    public function setIban(?string $iban): void
    {
        $this->iban = $iban;
    }

    /**
     * @return string|null
     */
    public function getJob(): ?string
    {
        return $this->job;
    }

    /**
     * @param string|null $job
     */
    public function setJob(?string $job): void
    {
        $this->job = $job;
    }

    public function getRegistrationStatus()
    {
        if (!empty($this->email) && !empty($this->restaurantName) && !empty($this->address) && !empty($this->getPhone())) {
            if (!empty($this->kbis) || !$this->supplierAccountLogs->isEmpty()) {
                if (!$this->customers->isEmpty()) {
                    /** @var Customer $customer */
                    $customer = $this->customers->first();

                    if ($customer->getShopUser()->isEnabled()) {
                        return ['status' => 5, 'statusText' => 'Client avec compte Shop activé'];
                    }

                    return ['status' => 4, 'statusText' => 'Client avec compte Shop non activé'];
                }

                return ['status' => 3, 'statusText' => 'Inscription Finale'];
            }

            return ['status' => 2, 'statusText' => 'Pré-Inscription'];
        }

        if (!empty($this->email) && !empty($this->restaurantName)) {
            return ['status' => 1, 'statusText' => 'Popup email restaurant'];
        }

        return ['status' => 0, 'statusText' => 'Too fast ou autre'];
    }

    public function getSupplierAccountLogs()
    {
        return $this->supplierAccountLogs;
    }

    /**
     * @return string|null
     */
    public function getHubspotDealStage(): ?string
    {
        return $this->hubspotDealStage;
    }

    /**
     * @param string|null $hubspotDealStage
     */
    public function setHubspotDealStage(?string $hubspotDealStage): void
    {
        $this->hubspotDealStage = $hubspotDealStage;
    }

    /**
     * @return Restaurant[]
     */
    public function getBoundRestaurants()
    {
        $restaurants = [];

        foreach ($this->customers as $customer) {
            foreach ($customer->getRestaurants() as $restaurant) {
                $restaurants[$restaurant->getId()] = $restaurant;
            }
        }

        return array_values($restaurants);
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isSuccessFlagJ(): bool
    {
        return $this->successFlagJ;
    }

    /**
     * @param bool $successFlagJ
     */
    public function setSuccessFlagJ(bool $successFlagJ): void
    {
        $this->successFlagJ = $successFlagJ;
    }

    /**
     * @return bool
     */
    public function isSuccessFlagJPlus7(): bool
    {
        return $this->successFlagJPlus7;
    }

    /**
     * @param bool $successFlagJPlus7
     */
    public function setSuccessFlagJPlus7(bool $successFlagJPlus7): void
    {
        $this->successFlagJPlus7 = $successFlagJPlus7;
    }

    /**
     * @return bool
     */
    public function isSuccessFlagJPlus30(): bool
    {
        return $this->successFlagJPlus30;
    }

    /**
     * @param bool $successFlagJPlus30
     */
    public function setSuccessFlagJPlus30(bool $successFlagJPlus30): void
    {
        $this->successFlagJPlus30 = $successFlagJPlus30;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @param Supplier $supplier
     * @return string|null
     */
    public function getAccountStatus(Supplier $supplier)
    {
        foreach ($this->supplierAccountLogs as $supplierAccountLog) {
            if ($supplierAccountLog->getSupplierCategory()->getSupplier()->getFacadeSupplier()->getId() === $supplier->getFacadeSupplier()->getId()) {
                return $supplierAccountLog->getStatus();
            }
        }

        return null;
    }
}
