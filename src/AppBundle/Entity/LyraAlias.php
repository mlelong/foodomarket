<?php


namespace AppBundle\Entity;


use DateTime;
use Sylius\Component\Resource\Model\ResourceInterface;

class LyraAlias implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Prospect
     */
    private $prospect;

    /**
     * @var string
     */
    private $lyraAliasUuid;

    /**
     * @var string
     */
    private $lyraToken;

    /**
     * @var string|null;
     */
    private $label;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $brand;

    /**
     * @var DateTime
     */
    private $expirationDate;

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Prospect
     */
    public function getProspect(): Prospect
    {
        return $this->prospect;
    }

    /**
     * @param Prospect $prospect
     */
    public function setProspect(Prospect $prospect): void
    {
        $this->prospect = $prospect;
    }

    /**
     * @return string
     */
    public function getLyraAliasUuid(): string
    {
        return $this->lyraAliasUuid;
    }

    /**
     * @param string $lyraAliasUuid
     */
    public function setLyraAliasUuid(string $lyraAliasUuid): void
    {
        $this->lyraAliasUuid = $lyraAliasUuid;
    }

    /**
     * @return string
     */
    public function getLyraToken(): string
    {
        return $this->lyraToken;
    }

    /**
     * @param string $lyraToken
     */
    public function setLyraToken(string $lyraToken): void
    {
        $this->lyraToken = $lyraToken;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return DateTime
     */
    public function getExpirationDate(): DateTime
    {
        return $this->expirationDate;
    }

    /**
     * @param DateTime $expirationDate
     */
    public function setExpirationDate(DateTime $expirationDate): void
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }
}