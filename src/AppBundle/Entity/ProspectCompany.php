<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;

class ProspectCompany implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var ProspectRestaurant[]|ArrayCollection
     */
    private $restaurants;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


}