<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Core\Model\ProductInterface;

/**
 * Restaurant
 *
 */
class SupplierProductInformations implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     *
     */
    private $id;

    /**
     * @var string
     *
     */
    private $label;

    /**
     * @var string
     *
     */
    private $reference;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Supplier
     */
    protected $supplier;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier = null)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param ProductInterface|Product $product
     */
    public function setProduct(ProductInterface $product = null)
    {
        $this->product = $product;
    }
}
