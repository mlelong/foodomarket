<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * Litigation
 */
class Litigation implements ResourceInterface
{
    use TimestampableTrait;

    // status
    const STATUS_OPEN = 1;
    const STATUS_SENT_TO_SUPPLIER = 2;
    const STATUS_CLOSED = 3;

    // problem
    const PROBLEM_MISSING_PRODUCT = 1;
    const PROBLEM_PRODUCT_QUALIITY = 2;
    const PROBLEM_PRODUCT_ERROR = 3;
    const PROBLEM_LATE_DELIVERY = 4;
    const PROBLEM_MISSING_DELIVERY = 5;
    const PROBLEM_INVOICE_ERROR = 6;

    //
    const DEMAND_DELIVERY = 1;
    const DEMAND_CREDIT = 2;

    const STATUS_LABEL = [
        1 => 'Ouvert',
        2 => 'Transmis',
        3 => 'Ferme',
    ];

    const PROBLEM_LABEL = [
        1 => 'Produit Manquant',
        2 => 'Qualité des produits',
        3 => 'Erreur de produit',
        4 => 'Livraison en retard',
        5 => 'Pas de livraison',
        6 => 'Erreur de facturation',
    ];

    const DEMAND_LABEL = [
        1 => 'Livraison à la prochaine commande',
        2 => 'Un Avoir',
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $status;

    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * @var Prospect
     */
    private $prospect;

    /**
     * @var Restaurant
     */
    private $restaurant;

    /**
     * @var string
     */
    private $problem;

    /**
     * @var string
     */
    private $demand;

    /**
     * @var bool
     */
    private $askToBeCalled;

    /**
     * @var LitigationComment[]|ArrayCollection
     */
    private $comments;

    /** @var string */
    private $hash;

    /**
     * constructor.
     *
     * initialize ArrayCollection
     */
    public function __construct()
    {
        $this->status = self::STATUS_OPEN;
        $this->askToBeCalled = false;
        $this->comments = new ArrayCollection();
    }

    /**
     * Get the literal status
     *
     * @return string
     */
    public function getStatusLabel(): string
    {
        return self::STATUS_LABEL[$this->getStatus()];
    }

    /**
     * Get the literal status
     *
     * @return string
     */
    public function getProblemLabel(): string
    {
        return self::PROBLEM_LABEL[$this->getProblem()];
    }

    /**
     * Get the literal status
     *
     * @return string
     */
    public function getDemandLabel(): string
    {
        return self::DEMAND_LABEL[$this->getDemand()];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier|null $supplier
     * @return Litigation
     */
    public function setSupplier(?Supplier $supplier): Litigation
    {
        $this->supplier = $supplier;
        return $this;
    }

    /**
     * @return Prospect|null
     */
    public function getProspect(): ?Prospect
    {
        return $this->prospect;
    }

    /**
     * @param Prospect|null $prospect
     * @return Litigation
     */
    public function setProspect(?Prospect $prospect): Litigation
    {
        $this->prospect = $prospect;
        return $this;
    }

    /**
     * Set restaurant.
     *
     * @param Restaurant|null $restaurant
     *
     * @return Litigation
     */
    public function setRestaurant(?Restaurant $restaurant): Litigation
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant.
     *
     * @return Restaurant|null
     */
    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    /**
     * Set problem.
     *
     * @param string|null $problem
     *
     * @return Litigation
     */
    public function setProblem(?string $problem): Litigation
    {
        $this->problem = $problem;

        return $this;
    }

    /**
     * Get problem.
     *
     * @return string|null
     */
    public function getProblem(): ?string
    {
        return $this->problem;
    }

    /**
     * Set demand.
     *
     * @param string|null $demand
     *
     * @return Litigation
     */
    public function setDemand(?string $demand)
    {
        $this->demand = $demand;

        return $this;
    }

    /**
     * Get demand.
     *
     * @return string|null
     */
    public function getDemand(): ?string
    {
        return $this->demand;
    }

    /**
     * Set askToBeCalled.
     *
     * @param bool $askToBeCalled
     *
     * @return Litigation
     */
    public function setAskToBeCalled(bool $askToBeCalled): Litigation
    {
        $this->askToBeCalled = $askToBeCalled;

        return $this;
    }

    /**
     * Get askToBeCalled.
     *
     * @return bool
     */
    public function isAskToBeCalled(): bool
    {
        return $this->askToBeCalled;
    }

    /**
     * Set status.
     *
     * @param string|null $status
     *
     * @return Litigation
     */
    public function setStatus(?string $status): Litigation
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return LitigationComment[]|ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param LitigationComment $comment
     * @return Litigation
     */
    public function addComment(LitigationComment $comment): Litigation
    {
        if (!$this->comments->contains($comment)) {
            $comment->setLitigation($this);
            $this->comments->add($comment);
        }

        return $this;
    }

    /**
     * @param LitigationComment $comment
     * @return Litigation
     */
    public function removeComment(LitigationComment $comment): Litigation
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    /**
     * @param LitigationComment[]|ArrayCollection $comments
     * @return Litigation
     */
    public function setComments($comments): Litigation
    {
        foreach($comments as $comment) {
            $this->addComment($comment);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }
}
