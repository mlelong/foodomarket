<?php
/**
 * Created by PhpStorm.
 * User: chekib
 * Date: 05/06/17
 * Time: 01:43
 */

namespace AppBundle\Entity;

use AppBundle\Services\ProductVariantUtils;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Model\ProductVariant as BaseProductVariant;

class ProductVariant extends BaseProductVariant implements \JsonSerializable
{
    /** @var bool */
    protected $enabled = true;

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function getChannelPricingForChannel(ChannelInterface $channel): ?ChannelPricingInterface
    {
        /** @var ChannelPricingInterface $channelPricing */
        foreach ($this->channelPricings as $channelPricing) {
            if ($channelPricing->getChannelCode() === $channel->getCode()) {
                return $channelPricing;
            }
        }

        return null;
    }

    public function getChannelPricingForChannelAndRestaurant(ChannelInterface $channel, Restaurant $restaurant = null)
    {
        foreach ($this->channelPricings as $channelPricing) {
            if ($channelPricing->getchannelCode() == $channel->getCode() && $channelPricing->getRestaurant() && $restaurant && $channelPricing->getRestaurant()->getId() == $restaurant->getId()) {
                return $channelPricing;
            }
        }
        foreach ($this->channelPricings as $channelPricing) {
            if ($channelPricing->getchannelCode() == $channel->getCode() && is_null($channelPricing->getRestaurant())) {
                return $channelPricing;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function removeChannelPricing(ChannelPricingInterface $channelPricing): void
    {
        if ($this->hasChannelPricing($channelPricing)) {
            $channelPricing->setProductVariant(null);
            $this->channelPricings->removeElement($channelPricing);
        }
    }

    public function getOptionValuesByOptionName($name)
    {
        return $this->getOptionValues()->filter(
            function($entry) use ($name) {
                if ($entry->getOption()->getName() == $name) {
                    return $entry;
                }
            }

        )->first();
    }

    /**
     * @param $code
     * @return ProductOptionValue|false
     */
    public function getOptionValuesByOptionCode($code)
    {
        return $this->getOptionValues()->filter(
            function(ProductOptionValue $entry) use ($code) {
                if ($entry->getOption()->getCode() == $code) {
                    return $entry;
                }

                return false;
            }
        )->first();
    }

    public function getOptionOriginCountry()
    {
        return $this->getOptionValuesByOptionCode('OP');
    }

    public function getOptionSupplierSalesUnit()
    {
        return $this->getOptionValuesByOptionCode('UM');
    }

    public function getOptionOrderUnit()
    {
        return $this->getOptionValuesByOptionCode('UC');
    }

    public function getOptionBio()
    {
        return $this->getOptionValuesByOptionCode('BI');
    }

    public function getOptionConditionning()
    {
        return $this->getOptionValuesByOptionCode('CO');
    }

    public function getOptionCaliber()
    {
        return $this->getOptionValuesByOptionCode('CA');
    }

    public function getOptionCategory()
    {
        return $this->getOptionValuesByOptionCode('CC');
    }

    public function getOptionVariety()
    {
        return $this->getOptionValuesByOptionCode('VA');
    }

    public function getOptionUnitQuantity()
    {
        return $this->getOptionValuesByOptionCode('UQ');
    }

    public function getOptionUnitContentQuantity()
    {
        return $this->getOptionValuesByOptionCode('UCQ');
    }

    public function getOptionUnitContentMeasurement()
    {
        return $this->getOptionValuesByOptionCode('UCM');
    }

    public function getDisplayName()
    {
        $name = $this->getProduct()->getName();
        $weight = $this->getWeight();

        if ($weight !== null && $weight > 0) {
            return "{$name} - {$weight}kg";
        }

        return $name;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return ['name' => $this->getDisplayName(), 'code' => $this->getCode()];
    }

    /**
     * Checks if this variant equals another variant based on weight and option values
     *
     * @param ProductVariant $other
     * @return bool
     */
    public function equals(ProductVariant $other)
    {
        if ($this->getWeight() != $other->getWeight())
            return false;

        $equals = true;

        /** @var ProductOptionValue $optionValue */
        foreach ($this->getOptionValues() as $optionValue) {
            if (!$this->equalsOption($optionValue, $other->getOptionValuesByOptionCode($optionValue->getOptionCode()))) {
                $equals = false;
                break ;
            }
        }

        if (!$equals) {
            return false;
        }

        foreach ($other->getOptionValues() as $optionValue) {
            if (!$this->equalsOption($this->getOptionValuesByOptionCode($optionValue->getOptionCode()), $optionValue)) {
                $equals = false;
                break ;
            }
        }

        return $equals;
    }

    /**
     * @param ProductOptionValue|false $option
     * @param ProductOptionValue|false $otherOption
     * @return bool
     */
    private function equalsOption($option, $otherOption) {
        if ($option === false && $otherOption === false)
            return true;

        $aIsOV = $option instanceof ProductOptionValue;
        $bIsOV = $otherOption instanceof ProductOptionValue;

        if ($aIsOV && $bIsOV && $option->getOptionCode() != $otherOption->getOptionCode())
            return false;

        $optionCode = '';

        if ($aIsOV) {
            $optionCode = $option->getOptionCode();
        } elseif ($bIsOV) {
            $optionCode = $otherOption->getOptionCode();
        }

        // Two exceptions: BI false = BI NON ---- CO false = CO VRAC
        switch ($optionCode) {
            case 'BI':
                if (($aIsOV && $bIsOV && $option->getCode() == $otherOption->getCode())
                    || ($aIsOV && !$bIsOV && $option->getCode() == 'NOBIO')
                    || (!$aIsOV && $bIsOV && $otherOption->getCode() == 'NOBIO'))
                    return true;
                break ;
            case 'CO':
                if (($aIsOV && $bIsOV && $option->getCode() == $otherOption->getCode())
                    || ($aIsOV && !$bIsOV && $option->getCode() == 'CO_VRAC')
                    || (!$aIsOV && $bIsOV && $otherOption->getCode() == 'CO_VRAC'))
                    return true;
                break ;
            default:
                return $aIsOV && $bIsOV && $option->getCode() == $otherOption->getCode();
        }

        return false;
    }

    public function getFullDisplayName()
    {
        return ProductVariantUtils::getNameForVariant($this);
    }

    public function getExplicitContent()
    {
        return ProductVariantUtils::getExplicitContentForVariant($this);
    }

    public function getInternalName()
    {
        $name = "({$this->id}) {$this->getFullDisplayName()}";
        $explicitContent = $this->getExplicitContent();
        $displayOrigin = $this->getDisplayOrigin();

        if (!empty($explicitContent)) {
            $name .= " - $explicitContent";
        }

        if (!empty($displayOrigin)) {
            $name .= " - $displayOrigin";
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getDisplayOrigin()
    {
        $productOrigin = [];

        if ($this->getOptionValuesByOptionCode('OV')) {
            $productOrigin[] = ucwords(mb_strtolower($this->getOptionValuesByOptionCode('OV')->getValue()), " \t\r\n\f\v-");
        }

        if ($this->getOptionOriginCountry()) {
            $productOrigin[] = ucwords(mb_strtolower($this->getOptionOriginCountry()->getValue()), " \t\r\n\f\v-");
        }

        return implode(', ', $productOrigin);
    }

    public function __toString(): string
    {
        return $this->getFullDisplayName();
    }
}
