<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class SupplierAccountLog implements ResourceInterface
{
    const STATUS_WAITING = 'WAITING';
    const STATUS_ACCOUNT_OPENED = 'ACCOUNT_OPENED';
    const STATUS_ACCOUNT_BLOCKED = 'ACCOUNT_BLOCKED';
    const STATUS_ORDERING = 'ORDERING';

    use TimestampableTrait;

    /** @var int */
    private $id;

    /** @var SupplierCategory|null */
    private $supplierCategory;

    /** @var Prospect|null */
    private $prospect;

    /** @var string[] */
    private $clientCodes = [];

    /** @var string|null */
    private $hash;

    /** @var string|null */
    private $status;

    /** @var int|null */
    private $contractId;

    /** @var string|null */
    private $token;

    /** @var int|null */
    private $tokenExpiration;

    /** @var boolean */
    private $signed = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return SupplierCategory|null
     */
    public function getSupplierCategory(): ?SupplierCategory
    {
        return $this->supplierCategory;
    }

    /**
     * @param SupplierCategory|null $supplierCategory
     */
    public function setSupplierCategory(?SupplierCategory $supplierCategory): void
    {
        $this->supplierCategory = $supplierCategory;
    }

    /**
     * @return Prospect|null
     */
    public function getProspect(): ?Prospect
    {
        return $this->prospect;
    }

    /**
     * @param Prospect|null $prospect
     */
    public function setProspect(?Prospect $prospect): void
    {
        $this->prospect = $prospect;
    }

    /**
     * @return null|string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param null|string $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param null|string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getContractId(): ?int
    {
        return $this->contractId;
    }

    /**
     * @param int|null $contractId
     */
    public function setContractId(?int $contractId): void
    {
        $this->contractId = $contractId;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     */
    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return int|null
     */
    public function getTokenExpiration(): ?int
    {
        return $this->tokenExpiration;
    }

    /**
     * @param int|null $tokenExpiration
     */
    public function setTokenExpiration(?int $tokenExpiration): void
    {
        $this->tokenExpiration = $tokenExpiration;
    }

    /**
     * @return bool
     */
    public function isSigned(): bool
    {
        return $this->signed;
    }

    /**
     * @param bool $signed
     */
    public function setSigned(bool $signed): void
    {
        $this->signed = $signed;
    }

    /**
     * @return string[]
     */
    public function getClientCodes(): array
    {
        return $this->clientCodes;
    }

    /**
     * @param string[] $clientCodes
     */
    public function setClientCodes(array $clientCodes): void
    {
        $this->clientCodes = $clientCodes;
    }

    /**
     * @param string $clientCode
     */
    public function addClientCode(string $clientCode): void
    {
        $this->clientCodes = array_values(array_unique(array_merge($this->clientCodes, [$clientCode])));
    }

    /**
     * @param string $clientCode
     */
    public function removeClientCode(string $clientCode): void
    {
        foreach ($this->clientCodes as $i => $code) {
            if ($clientCode === $code) {
                unset($this->clientCodes[$i]);
            }
        }
    }

    /**
     * @param string $statusCode
     * @return string
     */
    public static function getDisplayStatus(string $statusCode): string
    {
        switch ($statusCode) {
            case self::STATUS_WAITING:
                return 'En attente';
            case self::STATUS_ACCOUNT_BLOCKED:
                return 'Rejeté';
            case self::STATUS_ACCOUNT_OPENED:
            case self::STATUS_ORDERING:
                return 'Compte ouvert';
            default:
                return '';
        }
    }
}