<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use AppBundle\Entity\Supplier;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;

/**
 * Restaurant
 *
 */
class SupplierProductVariantInformations implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     *
     */
    private $id;

    /**
     * @var string
     *
     */
    private $productLabel;

    /**
     * @var string
     *
     */
    private $displayName;

    /**
     * @var string
     *
     */
    private $variantLabel;

    /**
     * @var string
     *
     */
    private $reference;

    /**
     * @var string|null
     */
    private $orderRef;

    /**
     * @var float
     *
     */
    private $weight;

    /**
     * @var integer
     *
     */
    private $content;

    /**
     * @var integer
     *
     */
    private $unitQuantity;

    /**
     * @var string
     *
     */
    private $salesUnit;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductVariant
     */
    protected $productVariant;

    /**
     * @var Supplier
     */
    protected $supplier;

    /**
     * @var boolean
     */
    protected $manuallyCreated = false;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Informations
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Informations
     */
    public function setProductLabel($label)
    {
        $this->productLabel = $label;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getProductLabel()
    {
        return $this->productLabel;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Informations
     */
    public function setVariantLabel($label)
    {
        $this->variantLabel = $label;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getVariantLabel()
    {
        return $this->variantLabel;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Informations
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get name
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set content
     *
     * @param integer $content
     *
     * @return Informations
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get name
     *
     * @return integer
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getUnitQuantity()
    {
        return $this->unitQuantity;
    }

    /**
     * @param int $unitQuantity
     */
    public function setUnitQuantity($unitQuantity)
    {
        $this->unitQuantity = $unitQuantity;
    }

    /**
     * Set salesUnit
     *
     * @param string $salesUnit
     *
     * @return Informations
     */
    public function setSalesUnit($salesUnit)
    {
        $this->salesUnit = $salesUnit;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getSalesUnit()
    {
        return $this->salesUnit;
    }

    /**
     * @return supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier = null)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(ProductInterface $product = null)
    {
        $this->product = $product;
    }

    /**
     * @return ProductVariant
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    public function getOptionOriginCountry()
    {
        return $this->productVariant->getOptionValuesByOptionCode('OP');
    }

    public function getOptionSupplierSalesUnit()
    {
        return $this->productVariant->getOptionValuesByOptionCode('UM');
    }

    public function getOptionOrderUnit()
    {
        return $this->productVariant->getOptionValuesByOptionCode('UC');
    }

    public function getOptionBio()
    {
        return $this->productVariant->getOptionValuesByOptionCode('BI');
    }

    public function getOptionConditionning()
    {
        return $this->productVariant->getOptionValuesByOptionCode('CO');
    }

    public function getOptionCaliber()
    {
        return $this->productVariant->getOptionValuesByOptionCode('CA');
    }

    public function getOptionUnitQuantity()
    {
        return $this->productVariant->getOptionValuesByOptionCode('UQ');
    }

    public function getOptionUnitContentQuantity()
    {
        return $this->getOptionValuesByOptionCode('UCQ');
    }

    public function getOptionUnitContentMeasurement()
    {
        return $this->getOptionValuesByOptionCode('UCM');
    }

    /**
     * @param ProductVariant $productVariant
     */
    public function setProductVariant(ProductVariantInterface $productVariant = null)
    {
        $this->productVariant = $productVariant;
    }

    /**
     * @return bool
     */
    public function isManuallyCreated(): bool
    {
        return $this->manuallyCreated;
    }

    /**
     * @param bool $manuallyCreated
     */
    public function setManuallyCreated(bool $manuallyCreated)
    {
        $this->manuallyCreated = $manuallyCreated;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        if (is_null($this->displayName)) {
            return '';
        }
        return $this->displayName;
    }

    /**
     * @param string $displayName
     */
    public function setDisplayName(string $displayName): void
    {
        $this->displayName = $displayName;
    }

    /**
     * @return string|null
     */
    public function getOrderRef(): ?string
    {
        return $this->orderRef;
    }

    /**
     * @param string|null $orderRef
     */
    public function setOrderRef(?string $orderRef): void
    {
        $this->orderRef = $orderRef;
    }
}
