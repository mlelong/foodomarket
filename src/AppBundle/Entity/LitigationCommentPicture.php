<?php

namespace AppBundle\Entity;

use DateTimeImmutable;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * LitigationCommentPicture
 * @Vich\Uploadable
 */
class LitigationCommentPicture implements ResourceInterface
{

    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var LitigationComment
     */
    private $comment;

    /**
     * @Vich\UploadableField(mapping="litigation_comment_picture", fileNameProperty="picture")
     *
     * @var File|null
     */

    private $pictureFile;

    /**
     * @var string|null
     */
    private $picture;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set litigationComment.
     *
     * @param int $litigationComment
     *
     * @return LitigationCommentPicture
     */
    public function setComment(LitigationComment $litigationComment)
    {
        $this->comment = $litigationComment;

        return $this;
    }

    /**
     * Get litigationComment.
     *
     * @return int
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile|null $kbisFile
     * @throws \Exception
     */
    public function setPictureFile(?File $pictureFile = null): void
    {
        $this->pictureFile = $pictureFile;

        if (null !== $pictureFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    /**
     * Get file.
     *
     * @return string|null
     */
    public function getPictureFile()
    {
        return $this->pictureFile;
    }

    /**
     * @return string|null
     */
    public function getPicture(): ?string
    {
        return $this->picture;
    }

    /**
     * @param string|null $picture
     * @return LitigationCommentPicture
     */
    public function setPicture(?string $picture): LitigationCommentPicture
    {
        $this->picture = $picture;
        return $this;
    }


}
