<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class SupplierCategory implements ResourceInterface
{
    use TimestampableTrait;

    /** @var int */
    private $id;

    /** @var Supplier|null */
    private $supplier;

    /** @var string|null */
    private $category;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier|null $supplier
     */
    public function setSupplier(?Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return null|string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param null|string $category
     */
    public function setCategory(?string $category): void
    {
        $this->category = $category;
    }

    public function __toString()
    {
        $category = Supplier::getDisplayCategory($this->getCategory());

        return $this->supplier !== null ? "{$this->supplier->getName()} ($category)" : '---';
    }

    public function getGridName(): ?string
    {
        if ($this->getSupplier() === null)
            return null;

        $category = Supplier::getDisplayCategory($this->getCategory());

        switch ($this->getSupplier()->getId()) {
            case 17:
                return "Grille 3 {$category}";
            case 18:
                return "Grille 4 {$category}";
            case 23:
                return "Grille SP {$category}";
            default:
                return $category;
        }
    }
}