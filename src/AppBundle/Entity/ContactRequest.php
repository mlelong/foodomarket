<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * ContactRequest
 */
class ContactRequest extends AbstractUtm implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_full"})
     */
    private $restaurantName;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_full"})
     */
    private $address;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_full"})
     */
    private $zipCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_full"})
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_full"})
     */
    private $deliveryHours;

    /**
     * @var string
     */
    private $deliveryHoursComment;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_light","request_form_full"})
     */
    private $managerName;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_light","request_form_full"})
     */
    private $managerFirstName;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_full"})
     */
    private $companyName;

    /**
     * @var string
     */
    private $siren;

    /**
     * @var string
     *
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_light","request_form_full"})
     * @Assert\Regex(
     *     message="showcase.contact_request.form.all.telephone.regex",
     *     groups={"request_form_light","request_form_full"},
     *     pattern="/^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$/"
     * )
     */
    private $telephone;

    /**
     * @var string
     *
     * @Assert\Email(message="showcase.contact_request.form.full.email.email", checkMX=true, groups={"request_form_full"})
     * @Assert\NotBlank(payload={"show"=false}, groups={"request_form_full"})
     */
    private $email;

    /**
     * @var int
     */
    private $turnover;

    /**
     * @var array
     */
    private $categories;

    /**
     * @var array
     */
    private $categoriesFull;

    /**
     * @var FoodType|null
     */
    private $profile;

    /**
     * @var Prospect|null
     */
    private $prospect;

    /**
     * @var bool
     */
    private $reachedByPhone = false;

    /**
     * @var string|null
     */
    private $staffComment;

    public function __construct()
    {
        $this->categories = array();
        $this->categoriesFull = array();
    }

    /**
     */
    public function prePersist() {
        if ($this->getEmail() == '' && $this->getUtmEmail() != '') {
            $this->setEmail($this->getUtmEmail());
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set restaurantName
     *
     * @param string $restaurantName
     *
     * @return ContactRequest
     */
    public function setRestaurantName($restaurantName)
    {
        $this->restaurantName = $restaurantName;

        return $this;
    }

    /**
     * Get restaurantName
     *
     * @return string
     */
    public function getRestaurantName()
    {
        return $this->restaurantName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return ContactRequest
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return ContactRequest
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return ContactRequest
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set deliveryHours
     *
     * @param string $deliveryHours
     *
     * @return ContactRequest
     */
    public function setDeliveryHours($deliveryHours)
    {
        $this->deliveryHours = $deliveryHours;

        return $this;
    }

    /**
     * Get deliveryHours
     *
     * @return string
     */
    public function getDeliveryHours()
    {
        return $this->deliveryHours;
    }

    /**
     * @return string
     */
    public function getDeliveryHoursComment()
    {
        return $this->deliveryHoursComment;
    }

    /**
     * @param string $deliveryHoursComment
     */
    public function setDeliveryHoursComment($deliveryHoursComment)
    {
        $this->deliveryHoursComment = $deliveryHoursComment;
    }

    /**
     * Set managerName
     *
     * @param string $managerName
     *
     * @return ContactRequest
     */
    public function setManagerName($managerName)
    {
        $this->managerName = $managerName;

        return $this;
    }

    /**
     * Get managerName
     *
     * @return string
     */
    public function getManagerName()
    {
        return $this->managerName;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return ContactRequest
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return ContactRequest
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ContactRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set turnover
     *
     * @param integer $turnover
     *
     * @return ContactRequest
     */
    public function setTurnover($turnover)
    {
        $this->turnover = $turnover;

        return $this;
    }

    /**
     * Get turnover
     *
     * @return int
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * Set categories
     *
     * @param array $categories
     *
     * @return ContactRequest
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories
     *
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param string $category
     *
     * @return ContactRequest
     */
    public function addCategory($category)
    {
        $this->categories = array_unique(array_merge($this->categories, [$category]));

        return $this;
    }

    /**
     * @param string $category
     *
     * @return ContactRequest
     */
    public function removeCategory($category)
    {
        $this->categories = array_filter($this->categories, function($value) use($category) {
            return $value != $category;
        });

        return $this;
    }

    /**
     * Set categories
     *
     * @param array $categories
     *
     * @return ContactRequest
     */
    public function setCategoriesFull($categories)
    {
        $this->categoriesFull = $categories;

        return $this;
    }

    /**
     * Get categories
     *
     * @return array
     */
    public function getCategoriesFull()
    {
        return $this->categoriesFull;
    }

    /**
     * @param string $category
     *
     * @return ContactRequest
     */
    public function addFullCategory($category)
    {
        $this->categoriesFull = array_unique(array_merge($this->categories, [$category]));

        return $this;
    }

    /**
     * @param string $category
     *
     * @return ContactRequest
     */
    public function removeFullCategory($category)
    {
        $this->categoriesFull = array_filter($this->categories, function($value) use($category) {
            return $value != $category;
        });

        return $this;
    }

    /**
     * @return FoodType|null
     */
    public function getProfile(): ?FoodType
    {
        return $this->profile;
    }

    /**
     * @param FoodType|null $profile
     */
    public function setProfile(?FoodType $profile): void
    {
        $this->profile = $profile;
    }

    /**
     * @return string|null
     */
    public function getManagerFirstName(): ?string
    {
        return $this->managerFirstName;
    }

    /**
     * @param string|null $managerFirstName
     */
    public function setManagerFirstName(?string $managerFirstName): void
    {
        $this->managerFirstName = $managerFirstName;
    }

    /**
     * @return string|null
     */
    public function getSiren(): ?string
    {
        return $this->siren;
    }

    /**
     * @param string|null $siren
     */
    public function setSiren(?string $siren): void
    {
        $this->siren = $siren;
    }

    /**
     * @return Prospect|null
     */
    public function getProspect(): ?Prospect
    {
        return $this->prospect;
    }

    /**
     * @param Prospect|null $prospect
     */
    public function setProspect(?Prospect $prospect): void
    {
        $this->prospect = $prospect;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param $payload
     *
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (!empty($this->getSiren())) {
            if (!self::isSiren($this->getSiren())) {
                $context->buildViolation('showcase.contact_request.form.full.siren')
                    ->atPath('siren')
                    ->addViolation()
                ;
            }
        }
    }

    public static function isSiren($siren)
    {
        $siren = preg_replace('/[\s]*/', '', $siren);

        if (strlen($siren) != 9) return false; // le SIREN doit contenir 9 caractères
        if (!is_numeric($siren)) return false; // le SIREN ne doit contenir que des chiffres

        // on prend chaque chiffre un par un
        // si son index (position dans la chaîne en commence à 0 au premier caractère) est impair
        // on double sa valeur et si cette dernière est supérieure à 9, on lui retranche 9
        // on ajoute cette valeur à la somme totale

        $sum = 0;

        for ($index = 0; $index < 9; $index ++)
        {
            $number = (int) $siren[$index];
            if (($index % 2) != 0) { if (($number *= 2) > 9) $number -= 9; }
            $sum += $number;
        }

        // le numéro est valide si la somme des chiffres est multiple de 10
        return ($sum % 10) == 0;
    }

    /**
     * @return bool
     */
    public function isReachedByPhone(): bool
    {
        return $this->reachedByPhone;
    }

    /**
     * @param bool $reachedByPhone
     */
    public function setReachedByPhone(bool $reachedByPhone): void
    {
        $this->reachedByPhone = $reachedByPhone;
    }

    /**
     * @return null|string
     */
    public function getStaffComment(): ?string
    {
        return $this->staffComment;
    }

    /**
     * @param null|string $staffComment
     */
    public function setStaffComment(?string $staffComment): void
    {
        $this->staffComment = $staffComment;
    }

    public function getAddressFull(): string
    {
        return "{$this->getAddress()} {$this->getZipCode()} {$this->getCity()}";
    }

    public function getDeliveryHoursText(): string
    {
        switch ($this->getDeliveryHours()) {
            case '_':
                $deliveryHours = 'Autre';
                break ;
            case '_6':
                $deliveryHours = 'Avant 6h';
                break ;
            case '6_8':
                $deliveryHours = 'Entre 6h et 8h';
                break ;
            case '8_10':
                $deliveryHours = 'Entre 8h et 10h';
                break ;
            case '10_12':
                $deliveryHours = 'Entre 10h et 12h';
                break ;
            case '12_14':
                $deliveryHours = 'Entre 12h et 14h';
                break ;
            case '14_16':
                $deliveryHours = 'Entre 14h et 16h';
                break ;
            case '16_18':
                $deliveryHours = 'Entre 16h et 18h';
                break ;
            case '18_':
                $deliveryHours = 'Après 18h';
                break ;
            default:
                $deliveryHours = $this->getDeliveryHours();
                break ;
        }

        if (!empty($this->getDeliveryHoursComment())) {
            $deliveryHours .= " ({$this->getDeliveryHoursComment()})";
        }

        return $deliveryHours;
    }

    public function getCurrentSuppliers()
    {
        $suppliers = [];

        foreach ($this->getCategoriesFull() as $category) {
            if (isset($category['suppliers'])) {
                foreach ($category['suppliers'] as $supplier) {
                    $suppliers[] = $supplier['name'];
                }
            }

            if (isset($category['otherSuppliers'])) {
                $suppliers[] = $category['otherSuppliers'];
            }
        }

        return implode(', ', array_values(array_unique($suppliers)));
    }
}

