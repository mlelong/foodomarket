<?php

namespace AppBundle\Entity;

use Gedmo\Blameable\Traits\BlameableEntity;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class ShoppingCartItem implements ResourceInterface, ShoppingItemInterface
{
    use TimestampableTrait, BlameableEntity;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var ProductVariant
     */
    protected $productVariant;

    /**
     * @var float
     */
    protected $quantity;

    /**
     * @var string
     */
    protected $unit;

    /**
     * @var ShoppingCart
     */
    protected $shoppingCart;

    /**
     * @var string
     */
    protected $note;

    /**
     * @var OrderItem|null
     */
    protected $orderItem;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ProductVariant
     */
    public function getProductVariant(): ?ProductVariant
    {
        return $this->productVariant;
    }

    /**
     * @param ProductVariant $productVariant
     */
    public function setProductVariant(?ProductVariant $productVariant)
    {
        $this->productVariant = $productVariant;
    }

    /**
     * @return float
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(?float $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit(?string $unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return ShoppingCart
     */
    public function getShoppingCart(): ShoppingCart
    {
        return $this->shoppingCart;
    }

    /**
     * @param ShoppingCart $shoppingCart
     */
    public function setShoppingCart(ShoppingCart $shoppingCart)
    {
        $this->shoppingCart = $shoppingCart;
    }

    /**
     * @return string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote(?string $note)
    {
        $this->note = $note;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier()
    {
        return $this->shoppingCart !== null ? $this->shoppingCart->getSupplier() : null;
    }

    /**
     * @return OrderItem|null
     */
    public function getOrderItem(): ?OrderItem
    {
        return $this->orderItem;
    }
}