<?php

namespace AppBundle\Entity;

use DateTime;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * Class ProspectSource
 * @package AppBundle\Entity
 */
class ProspectSource implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var Prospect|null
     */
    private $prospect;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * @var string|null
     */
    private $source;

    /**
     * @var string
     */
    private $action;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Prospect|null
     */
    public function getProspect(): ?Prospect
    {
        return $this->prospect;
    }

    /**
     * @param Prospect|null $prospect
     */
    public function setProspect(?Prospect $prospect): void
    {
        $this->prospect = $prospect;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }



}