<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Blameable\Traits\BlameableEntity;
use Sylius\Component\Core\Model\Order;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class ShoppingCart implements ResourceInterface
{
    use TimestampableTrait, BlameableEntity;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var Supplier
     */
    protected $supplier;

    /**
     * @var Restaurant
     */
    protected $restaurant;

    /**
     * @var ShoppingCartItem[]|ArrayCollection
     */
    protected $items;

    /**
     * @var string
     */
    protected $note;

    /**
     * @var boolean
     */
    protected $ordered = false;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var \DateTime|null
     */
    protected $scheduledDate;

    /**
     * @var bool
     */
    protected $manuallyCreated = false;

    /**
     * ShoppingCart constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(?Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Restaurant|null
     */
    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    /**
     * @param Restaurant|null $restaurant
     */
    public function setRestaurant(?Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @return ShoppingCartItem[]|ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    public function addItem(ShoppingCartItem $item)
    {
        $item->setShoppingCart($this);
        $this->items->add($item);
    }

    public function removeItem(ShoppingCartItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * @param ShoppingCartItem[]|ArrayCollection $items
     */
    public function setItems($items)
    {
        foreach ($items as &$item) {
            $item->setShoppingCart($this);
        }

        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote(?string $note)
    {
        $this->note = $note;
    }

    /**
     * @return bool
     */
    public function isOrdered(): bool
    {
        return $this->ordered;
    }

    /**
     * @param bool $ordered
     */
    public function setOrdered(bool $ordered)
    {
        $this->ordered = $ordered;
    }

    /**
     * @return Order
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(?Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return \DateTime|null
     */
    public function getScheduledDate(): ?\DateTime
    {
        return $this->scheduledDate;
    }

    /**
     * @param \DateTime|null $scheduledDate
     */
    public function setScheduledDate(?\DateTime $scheduledDate): void
    {
        $this->scheduledDate = $scheduledDate;
    }

    /**
     * @return bool
     */
    public function isManuallyCreated(): bool
    {
        return $this->manuallyCreated;
    }

    /**
     * @param bool $manuallyCreated
     */
    public function setManuallyCreated(bool $manuallyCreated): void
    {
        $this->manuallyCreated = $manuallyCreated;
    }
}