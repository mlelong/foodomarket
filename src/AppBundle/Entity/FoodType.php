<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Core\Model\ImagesAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;

/**
 * FoodType
 */
class FoodType implements ResourceInterface, TranslatableInterface, ImagesAwareInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Collection|ImageInterface[]
     */
    protected $images;

    /**
     * @var int
     */
    private $position = 0;

    /**
     * @var ProductVariant[]|ArrayCollection
     */
    private $variants;

    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
        getTranslation as private doGetTranslation;
    }

    public function __construct()
    {
        $this->initializeTranslationsCollection();
        $this->variants = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->addImage(new FoodTypeImage());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Create resource translation model.
     *
     * @return FoodTypeTranslation
     */
    protected function createTranslation(): TranslationInterface
    {
        return new FoodTypeTranslation();
    }

    /**
     * @param string $type
     *
     * @return FoodType
     */
    public function setType($type)
    {
        $this->getTranslation()->setType($type);

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->getTranslation()->getType();
    }

    /**
     * @param array $specialities
     *
     * @return FoodType
     */
    public function setSpecialities($specialities)
    {
        $this->getTranslation()->setSpecialities($specialities);

        return $this;
    }

    /**
     * @param string $speciality
     *
     * @return FoodType
     */
    public function addSpeciality($speciality)
    {
        $this->getTranslation()->addSpeciality($speciality);

        return $this;
    }

    /**
     * @param string $speciality
     *
     * @return FoodType
     */
    public function removeSpeciality($speciality)
    {
        $this->getTranslation()->removeSpeciality($speciality);

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getSpecialities()
    {
        return $this->getTranslation()->getSpecialities();
    }

    /**
     * @param null|string $locale
     *
     * @return FoodTypeTranslation
     */
    public function getTranslation(?string $locale = null): TranslationInterface
    {
        return $this->doGetTranslation($locale);
    }

    /**
     * @return ProductVariant[]|ArrayCollection
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * @param ProductVariant[]|SupplierProductPrice[]|ArrayCollection $variants
     */
    public function setVariants($variants): void
    {
        if (!empty($variants) && $variants[0] instanceof SupplierProductPrice) {
            $this->variants = array_map(function(SupplierProductPrice $price) {
                return $price->getProductVariant();
            }, $variants);
        } else {
            $this->variants = $variants;
        }
    }

    /**
     * @param ProductVariant|SupplierProductPrice $variant
     *
     * @return FoodType
     */
    public function addVariant($variant)
    {
        if ($variant instanceof SupplierProductPrice) {
            $variant = $variant->getProductVariant();
        }

        if (!$this->variants->contains($variant)) {
            $this->variants->add($variant);
        }

        return $this;
    }

    /**
     * @param ProductVariant|SupplierProductPrice $variant
     *
     * @return FoodType
     */
    public function removeVariant($variant)
    {
        if ($variant instanceof SupplierProductPrice) {
            $variant = $variant->getProductVariant();
        }

        $this->variants->removeElement($variant);

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition() : int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return FoodType
     */
    public function setPosition(int $position) : FoodType
    {
        $this->position = $position;

        return $this;
    }

    public function getPicture()
    {
        if ($this->images === null) {
            $this->images = new ArrayCollection();
        }

        if ($this->images->isEmpty()) {
            return '';
        }

        /** @var FoodTypeImage $image */
        $image = $this->images->get(0);

        if ($image->getPath() !== null && !empty($image->getPath())) {
            return $image->getPath();
        }

        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getImages(): Collection
    {
        if ($this->images === null) {
            $this->images = new ArrayCollection();
        }

        if ($this->images->isEmpty()) {
            $this->addImage(new FoodTypeImage());
        }

        return $this->images;
    }

    /**
     * {@inheritdoc}
     */
    public function getImagesByType(string $type): Collection
    {
        return $this->images->filter(function (ImageInterface $image) use ($type) {
            return $type === $image->getType();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function hasImages(): bool
    {
        if ($this->images === null) {
            $this->images = new ArrayCollection();
        }

        return !$this->images->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function hasImage(ImageInterface $image): bool
    {
        if ($this->images === null) {
            $this->images = new ArrayCollection();
        }

        return $this->images->contains($image);
    }

    /**
     * {@inheritdoc}
     */
    public function addImage(ImageInterface $image): void
    {
        if ($this->images === null) {
            $this->images = new ArrayCollection();
        }

        $image->setOwner($this);
        $this->images->add($image);
    }

    /**
     * {@inheritdoc}
     */
    public function removeImage(ImageInterface $image): void
    {
        if ($this->hasImage($image)) {
            $image->setOwner(null);
            $this->images->removeElement($image);
        }
    }
}
