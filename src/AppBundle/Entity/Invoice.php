<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * Class Invoice
 * @package AppBundle\Entity
 */
class Invoice implements ResourceInterface
{
    use TimestampableTrait;

    /** @var int */
    private $id;

    /** @var string|null */
    private $number;

    /** @var \DateTime|null */
    private $date;

    /** @var Supplier|null */
    private $supplier;

    /** @var Prospect|null */
    private $prospect;

    /** @var string|null */
    private $clientCode;

    /** @var string|null */
    private $invoicePath;

    /** @var ArrayCollection|InvoiceDeliveryNote[] */
    private $invoiceDeliveryNotes;

    /** @var float|null */
    private $total = 0.0;

    /**
     * Invoice constructor.
     */
    public function __construct()
    {
        $this->number = '';
        $this->invoiceDeliveryNotes = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     */
    public function setDate(?\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return null|string
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param null|string $number
     */
    public function setNumber(?string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier|null $supplier
     */
    public function setSupplier(?Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Prospect|null
     */
    public function getProspect(): ?Prospect
    {
        return $this->prospect;
    }

    /**
     * @param Prospect|null $prospect
     */
    public function setProspect(?Prospect $prospect): void
    {
        $this->prospect = $prospect;
    }

    /**
     * @return null|string
     */
    public function getClientCode(): ?string
    {
        return $this->clientCode;
    }

    /**
     * @param null|string $clientCode
     */
    public function setClientCode(?string $clientCode): void
    {
        $this->clientCode = $clientCode;
    }

    /**
     * @return null|string
     */
    public function getInvoicePath(): ?string
    {
        return $this->invoicePath;
    }

    /**
     * @param null|string $invoicePath
     */
    public function setInvoicePath(?string $invoicePath): void
    {
        $this->invoicePath = $invoicePath;
    }

    /**
     * @return InvoiceDeliveryNote[]|ArrayCollection
     */
    public function getInvoiceDeliveryNotes()
    {
        return $this->invoiceDeliveryNotes;
    }

    /**
     * @param InvoiceDeliveryNote[]|ArrayCollection $invoiceDeliveryNotes
     */
    public function setInvoiceDeliveryNotes($invoiceDeliveryNotes): void
    {
        $this->invoiceDeliveryNotes = $invoiceDeliveryNotes;

        $total = 0.0;

        foreach ($this->invoiceDeliveryNotes as $invoiceDeliveryNote) {
            $total += $invoiceDeliveryNote->getTotal();
        }

        $this->setTotal($total);
    }

    /**
     * @param InvoiceDeliveryNote $invoiceDeliveryNote
     */
    public function addInvoiceDeliveryNote(InvoiceDeliveryNote $invoiceDeliveryNote)
    {
        if (!$this->invoiceDeliveryNotes->contains($invoiceDeliveryNote)) {
            $invoiceDeliveryNote->setInvoice($this);
            $this->invoiceDeliveryNotes->add($invoiceDeliveryNote);

            $this->setTotal($this->total + $invoiceDeliveryNote->getTotal());
        }
    }

    /**
     * @param InvoiceDeliveryNote $invoiceDeliveryNote
     */
    public function removeInvoiceDeliveryNote(InvoiceDeliveryNote $invoiceDeliveryNote)
    {
        if ($this->invoiceDeliveryNotes->contains($invoiceDeliveryNote)) {
            $this->invoiceDeliveryNotes->removeElement($invoiceDeliveryNote);
            $invoiceDeliveryNote->setInvoice(null);

            $this->setTotal($this->total - $invoiceDeliveryNote->getTotal());
        }
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param float|null $total
     */
    public function setTotal(?float $total): void
    {
        $this->total = $total;
    }
}