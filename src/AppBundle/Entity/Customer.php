<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Serializable;
use Sylius\Component\Channel\Model\ChannelInterface;
use Sylius\Component\Channel\Model\ChannelsAwareInterface;
use Sylius\Component\Core\Model\Customer as BaseCustomer;
use Sylius\Component\Core\Model\ShopUserInterface;

class Customer extends BaseCustomer implements ChannelsAwareInterface, Serializable
{
    /**
     * @var Prospect|null
     */
    private $prospect;

    /**
     * @var Restaurant[]|ArrayCollection
     */
    private $restaurants;

    /**
     * @var ArrayCollection|ChannelInterface[]
     */
    protected $channels;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->channels = new ArrayCollection();
        $this->restaurants = new ArrayCollection();
    }

    /**
     * @return Restaurant[]|ArrayCollection
     */
    public function getRestaurants()
    {
        return $this->restaurants;
    }

    /**
     * @param Restaurant[]|ArrayCollection $restaurants
     */
    public function setRestaurants($restaurants)
    {
        $this->restaurants = $restaurants;
    }

    public function addRestaurant(Restaurant $restaurant)
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants->add($restaurant);
        }
    }

    public function removeRestaurant(Restaurant $restaurant)
    {
        $this->restaurants->removeElement($restaurant);
    }

    /**
     * @return Collection|ChannelInterface[]
     */
    public function getChannels(): Collection
    {
        return $this->channels;
    }

    /**
     * @param ChannelInterface $channel
     *
     * @return bool
     */
    public function hasChannel(ChannelInterface $channel): bool
    {
        return $this->channels->containsKey($channel->getId());
    }

    /**
     * @param ChannelInterface $channel
     */
    public function addChannel(ChannelInterface $channel): void
    {
        $this->channels->set($channel->getId(), $channel);
    }

    /**
     * @param ChannelInterface $channel
     */
    public function removeChannel(ChannelInterface $channel): void
    {
        $this->channels->remove($channel->getId());
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->emailCanonical,
            $this->firstName,
            $this->lastName,
            $this->gender,
            $this->restaurants->toArray(),
            $this->prospect
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        [
            $this->id,
            $this->email,
            $this->emailCanonical,
            $this->firstName,
            $this->lastName,
            $this->gender,
            $this->restaurants,
            $this->prospect
        ] = $data;

        // Re make it an ArrayCollection
        if (is_array($this->restaurants)) {
            $this->restaurants = new ArrayCollection($this->restaurants);
        }
    }

    /**
     * @return Prospect|null
     */
    public function getProspect(): ?Prospect
    {
        return $this->prospect;
    }

    /**
     * @param Prospect|null $prospect
     */
    public function setProspect(?Prospect $prospect): void
    {
        $this->prospect = $prospect;
    }

    /**
     * @return ShopUserInterface|ShopUser|null
     */
    public function getShopUser(): ?ShopUser
    {
        return $this->user;
    }

    /**
     * @param ShopUser|null $shopUser
     */
    public function setShopUser(?ShopUser $shopUser): void
    {
        $this->setUser($shopUser);
    }
}
