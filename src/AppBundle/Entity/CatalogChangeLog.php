<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * CatalogChangeLog
 */
class CatalogChangeLog implements ResourceInterface
{

    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;


    /**
     * @var taxon
     */
    protected $taxon;

    /**
     * @var product
     */
    private $product;

    /**
     * @var ProductVariant
     */
    private $productVariant;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $message;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTaxon()
    {
        return $this->taxon;
    }

    public function setTaxon(?Taxon $taxon)
    {
        $this->taxon = $taxon;

        return $this;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return CatalogChangeLog
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set variant
     *
     * @param ProductVariant $variant
     *
     * @return CatalogChangeLog
     */
    public function setProductVariant($variant)
    {
        $this->productVariant = $variant;

        return $this;
    }

    /**
     * Get variant
     *
     * @return ProductVariant
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return CatalogChangeLog
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CatalogChangeLog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return CatalogChangeLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function getTaxonId()
    {
        if ($this->taxon) {
            return $this->taxon->getCode();
        }

        return null;
    }

    public function getProductId()
    {
        if ($this->product) {
            return $this->product->getId();
        }

        return null;
    }

    public function getProductVariantId()
    {
        if ($this->productVariant) {
            return $this->productVariant->getId();
        }

        return null;
    }

}

