<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serializer;

class ApiResult{
    /**
     * @Serializer\XmlKeyValuePairs
     */
    private $items;

    public function __construct($items)
    {
        $this->items = $items;
    }
}