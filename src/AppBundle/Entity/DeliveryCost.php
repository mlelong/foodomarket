<?php

namespace AppBundle\Entity;
use Sylius\Component\Resource\Model\ResourceInterface;


/**
 * DeliveryCost
 */
class DeliveryCost implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $deliveryFees;

    /**
     * @var int
     */
    private $freeDeliveryThreshold;

    /**
     * @var Restaurant
     */
    private $restaurant;

    /**
     * @var Supplier
     */
    private $supplier;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deliveryFees
     *
     * @param integer $deliveryFees
     *
     * @return DeliveryCost
     */
    public function setDeliveryFees($deliveryFees)
    {
        $this->deliveryFees = $deliveryFees;

        return $this;
    }

    /**
     * Get deliveryFees
     *
     * @return int
     */
    public function getDeliveryFees()
    {
        return $this->deliveryFees;
    }

    /**
     * Set freeDeliveryThreshold
     *
     * @param integer $freeDeliveryThreshold
     *
     * @return DeliveryCost
     */
    public function setFreeDeliveryThreshold($freeDeliveryThreshold)
    {
        $this->freeDeliveryThreshold = $freeDeliveryThreshold;

        return $this;
    }

    /**
     * Get freeDeliveryThreshold
     *
     * @return int
     */
    public function getFreeDeliveryThreshold()
    {
        return $this->freeDeliveryThreshold;
    }

    /**
     * Set restaurant
     *
     * @param integer $restaurant
     *
     * @return DeliveryCost
     */
    public function setRestaurant(Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant
     *
     * @return Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Set supplier
     *
     * @param Supplier $supplier
     *
     * @return DeliveryCost
     */
    public function setSupplier(Supplier $supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }
}

