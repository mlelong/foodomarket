<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Product\Model\Product;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

use Symfony\Component\Validator\Constraints as Assert;

class ImportLine implements ResourceInterface
{
    // status
    const STATUS_AWAITING_CHECK = 1;
    const STATUS_MATCHED_WITH_CATALOG = 2;
    const STATUS_IMPORTED = 3;

    const STATUS_LABEL = [
        1 => 'AWAITING_CHECK',
        2 => 'MATCHED_WITH_CATALOG',
        3 => 'IMPORTED',
    ];

    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var Import
     */
    protected $import;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductVariant
     */
    protected $productVariant;

    /**
     * @var Taxon
     * @Assert\NotBlank(message="Vous devez spécifier un taxon")
     */
    protected $taxon;

    /**
     * @var string
     */
    protected $reference;

    /**
     * @var ProductOptionValueInterface[]|ArrayCollection
     */
    protected $optionValues;

    /**
     * @var string
     */
    protected $supplierName;

    /**
     * @var int
     */
    protected $price;

    /**
     * @var string
     */
    protected $productName;

    /**
     * @var string
     */
    protected $variantName;

    //@Assert\NotEqualTo(value=0, message="Il faut un poids ")
    /**
     * @var float
     *
     */
    protected $weight;

    /**
     * @var string
     */
    protected $salesUnit;

    /**
     * @var string
     */
    protected $orderUnit;

    /**
     * @var int
     */
    protected $units;

    /**
     * @var int
     */
    protected $kgPrice;

    /**
     * @var int
     */
    protected $priceVariant;

    /**
     * @var string
     */
    protected $originCountry;

    /**
     * @var string
     */
    protected $conditioning;

    /**
     * @var string
     */
    protected $origin;

    /**
     * @var string
     */
    protected $caliber;

    /**
     * @var string
     */
    protected $naming;

    /**
     * @var string
     */
    protected $variety;

    /**
     * @var string
     */
    protected $category;

    /**
     * @var string
     */
    protected $bio;

    /**
     * @var string
     */
    protected $brand;

    /**
     * @var string
     */
    protected $taxonDeterminant;

    /**
     * @var int
     */
    protected $unitQuantity;

    /**
     * @var int
     */
    protected $unitContentQuantity;

    /**
     * @var string
     */
    protected $unitContentMeasurement;

    /**
     * @var string|null
     */
    protected $line;

    public function setId(int $id) {

    }

    public function __construct()
    {
        $this->status = self::STATUS_AWAITING_CHECK;

        $this->optionValues = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImport()
    {
        return $this->import;
    }

    public function setImport(Import $import)
    {
        $this->import = $import;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status int
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * status label
     *
     * @return string
     */
    public function getStatusLabel()
    {
        return self::STATUS_LABEL[$this->status];
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct(?Product $product)
    {
        $this->product = $product;

        return $this;
    }

    public function getProductVariant()
    {
        return $this->productVariant;
    }

    public function setProductVariant(?ProductVariant $variant)
    {
        $this->productVariant = $variant;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductVariantId(): ?string
    {
        if ($this->getProductVariant()) {
            return $this->getProductVariant()->getId();
        }
    }

    public function getTaxon()
    {
        return $this->taxon;
    }

    public function setTaxon(?Taxon $taxon)
    {
        $this->taxon = $taxon;

        return $this;
    }

    public function __toString()
    {
        return $this->content;
    }

    public function getOptionValues(): Collection
    {
        return $this->optionValues;
    }

    /**
     * @param ArrayCollection|ProductOptionValueInterface[] $optionValues
     */
    public function setOptionValues($optionValues)
    {
        $this->optionValues = $optionValues;
    }

    /**
     * @return string
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference(?string $reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return string
     */
    public function getSupplierName(): ?string
    {
        return $this->supplierName;
    }

    /**
     * @param string $supplierName
     */
    public function setSupplierName(?string $supplierName)
    {
        $this->supplierName = $supplierName;
    }

    /**
     * @return int
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(?int $price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getProductName(): ?string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     */
    public function setProductName(?string $productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return string
     */
    public function getVariantName(): ?string
    {
        return $this->variantName;
    }

    /**
     * @param string $variantName
     */
    public function setVariantName(?string $variantName)
    {
        $this->variantName = $variantName;
    }

    /**
     * @return float
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(?float $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getSalesUnit(): ?string
    {
        return $this->salesUnit;
    }

    /**
     * @param string $salesUnit
     */
    public function setSalesUnit(?string $salesUnit)
    {
        $this->salesUnit = $salesUnit;
    }

    /**
     * @return string
     */
    public function getOrderUnit(): ?string
    {
        return $this->orderUnit;
    }

    /**
     * @param string $orderUnit
     */
    public function setOrderUnit(?string $orderUnit)
    {
        $this->orderUnit = $orderUnit;
    }

    /**
     * @return int
     */
    public function getUnits(): ?int
    {
        return $this->units;
    }

    /**
     * @param int $units
     */
    public function setUnits(?int $units)
    {
        $this->units = $units;
    }

    /**
     * @return int
     */
    public function getKgPrice(): ?int
    {
        return $this->kgPrice;
    }

    /**
     * @param int $kgPrice
     */
    public function setKgPrice(?int $kgPrice)
    {
        $this->kgPrice = $kgPrice;
    }

    /**
     * @return int
     */
    public function getPriceVariant(): ?int
    {
        return $this->priceVariant;
    }

    /**
     * @param int $priceVariant
     */
    public function setPriceVariant(?int $priceVariant)
    {
        $this->priceVariant = $priceVariant;
    }

    /**
     * @return string
     */
    public function getOriginCountry(): ?string
    {
        return $this->originCountry;
    }

    /**
     * @param string $originCountry
     */
    public function setOriginCountry(?string $originCountry)
    {
        $this->originCountry = $originCountry;
    }

    /**
     * @return string
     */
    public function getConditioning(): ?string
    {
        return $this->conditioning;
    }

    /**
     * @param string $conditioning
     */
    public function setConditioning(?string $conditioning)
    {
        $this->conditioning = $conditioning;
    }

    /**
     * @return string
     */
    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin(?string $origin)
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getCaliber(): ?string
    {
        return $this->caliber;
    }

    /**
     * @param string $caliber
     */
    public function setCaliber(?string $caliber)
    {
        $this->caliber = $caliber;
    }

    /**
     * @return string
     */
    public function getNaming(): ?string
    {
        return $this->naming;
    }

    /**
     * @param string $naming
     */
    public function setNaming(?string $naming)
    {
        $this->naming = $naming;
    }

    /**
     * @return string
     */
    public function getVariety(): ?string
    {
        return $this->variety;
    }

    /**
     * @param string $variety
     */
    public function setVariety(?string $variety)
    {
        $this->variety = $variety;
    }

    /**
     * @return string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(?string $category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getTaxonDeterminant(): ?string
    {
        return $this->taxonDeterminant;
    }

    /**
     * @param string $taxonDeterminant
     */
    public function setTaxonDeterminant(?string $taxonDeterminant)
    {
        $this->taxonDeterminant = $taxonDeterminant;
    }

    /**
     * @return string
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string $bio
     */
    public function setBio(?string $bio)
    {
        $this->bio = $bio;
    }

    /**
     * @return string
     */
    public function getBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(?string $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return int
     */
    public function getUnitQuantity()
    {
        return $this->unitQuantity;
    }

    /**
     * @param int $unitQuantity
     */
    public function setUnitQuantity($unitQuantity)
    {
        $this->unitQuantity = $unitQuantity;
    }

    /**
     * @return int
     */
    public function getUnitContentQuantity()
    {
        return $this->unitContentQuantity;
    }

    /**
     * @param int $unitContentQuantity
     */
    public function setUnitContentQuantity($unitContentQuantity)
    {
        $this->unitContentQuantity = $unitContentQuantity;

//        $this->unitContentQuantity = floatval(str_replace(',', '.', $unitContentQuantity));
    }

    /**
     * @return int
     */
    public function getUnitContentMeasurement()
    {
        return $this->unitContentMeasurement;
    }

    /**
     * @param string $unitContentMeasurement
     */
    public function setUnitContentMeasurement($unitContentMeasurement)
    {
        $this->unitContentMeasurement = $unitContentMeasurement;
    }

    public function contentToProperties()
    {
        try {
            $content = unserialize($this->content);

            if (isset($content['weight']))
                $this->weight = number_format($content['weight'], 3, '.', '');

            if (isset($content['content']))
                $this->units = $content['content'];

            if (isset($content['reference']))
                $this->reference = $content['reference'];

            if (isset($content['variety']))
                $this->variety = $content['variety'];

            if (isset($content['priceKg']))
                $this->kgPrice = $content['priceKg'];

            if (isset($content['priceVariant']))
                $this->priceVariant = $content['priceVariant'];

            if (isset($content['salesUnit']))
                $this->salesUnit = $content['salesUnit'];

            if (isset($content['orderUnit']))
                $this->orderUnit = $content['orderUnit'];

            if (isset($content['productName']))
                $this->productName = $content['productName'];

            if (isset($content['variantName']))
                $this->variantName = $content['variantName'];

            if (isset($content['price']))
                $this->price = $content['price'];

            if (isset($content['supplierName']))
                $this->supplierName = $content['supplierName'];

            if (isset($content['originCountry']))
                $this->originCountry = $content['originCountry'];

            if (isset($content['origin']))
                $this->origin = $content['origin'];

            if (isset($content['category']))
                $this->category = $content['category'];

            if (isset($content['naming']))
                $this->naming = $content['naming'];

            if (isset($content['caliber']))
                $this->caliber = $content['caliber'];

            if (isset($content['conditioning']))
                $this->conditioning = $content['conditioning'];

            if (isset($content['taxon']))
                $this->taxonDeterminant = $content['taxon'];

            if (isset($content['bio'])) {
                $this->bio = $content['bio'];
            }

            if (isset($content['brand'])) {
                $this->brand = $content['brand'];
            }

            if (isset($content['unitQuantity'])) {
                $this->unitQuantity = $content['unitQuantity'];
            }

            if (isset($content['unitContentQuantity'])) {
                $this->unitContentQuantity = floatval(str_replace(',', '.', $content['unitContentQuantity']));
            }

            if (isset($content['unitContentMeasurement'])) {
                $this->unitContentMeasurement = $content['unitContentMeasurement'];
            }

        } catch (\Exception $ignored) {

        }
    }

    public function propertiesToContent()
    {
        $content = unserialize($this->content);

        $content['weight'] = number_format($this->weight, 3, '.', '');
        $content['content'] = $this->units;
        $content['reference'] = $this->reference;
        $content['variety'] = $this->variety;
        $content['priceKg'] = $this->kgPrice;
        $content['priceVariant'] = $this->priceVariant;
        $content['salesUnit'] = $this->salesUnit;
        $content['orderUnit'] = $this->orderUnit;
        $content['productName'] = $this->productName;
        $content['variantName'] = $this->variantName;
        $content['price'] = $this->price;
        $content['supplierName'] = $this->supplierName;
        $content['originCountry'] = $this->originCountry;
        $content['origin'] = $this->origin;
        $content['category'] = $this->category;
        $content['naming'] = $this->naming;
        $content['caliber'] = $this->caliber;
        $content['conditioning'] = $this->conditioning;
        $content['taxon'] = $this->taxonDeterminant;
        $content['bio'] = $this->bio;
        $content['brand'] = $this->brand;
        $content['unitQuantity'] = $this->unitQuantity;
        $content['unitContentQuantity'] = $this->unitContentQuantity;
        $content['unitContentMeasurement'] = $this->unitContentMeasurement;

        $this->setContent(serialize($content));
    }

    public function preSave()
    {
        $this->contentToProperties();
    }

    /**
     * @return string|null
     */
    public function getLine(): ?string
    {
        return $this->line;
    }

    /**
     * @param string|null $line
     */
    public function setLine(?string $line): void
    {
        $this->line = $line;
    }

    public function loadOptionsFromProductVariant()
    {
        if (!$this->productVariant) {
            return ;
        }

        $this->weight = $this->productVariant->getWeight();

        /** @var \AppBundle\Entity\ProductOptionValue[] $optionValues */
        $optionValues = $this->productVariant->getOptionValues();

        foreach ($optionValues as $optionValue) {
            switch ($optionValue->getOptionCode()) {
                case 'UM';
                    $this->salesUnit = $optionValue->getCode();
                    break ;
                case 'UC':
                    $this->orderUnit = $optionValue->getCode();
                    break ;
                case 'UQ':
                    $this->unitQuantity = $optionValue->getValue();
                    break ;
                case 'UCQ':
                    $this->unitContentQuantity = floatval(str_replace(',', '.', $optionValue->getValue()));
                    break ;
                case 'UCM':
                    $this->unitContentMeasurement = $optionValue->getCode();
                    break ;
                case 'CC':
                    $this->category = $optionValue->getCode();
                    break ;
                case 'CO':
                    $this->conditioning = $optionValue->getCode();
                    break ;
                case 'CA':
                    $this->caliber = $optionValue->getCode();
                    break ;
                case 'VA':
                    $this->variety = $optionValue->getCode();
                    break ;
                case 'AP':
                    $this->naming = $optionValue->getCode();
                    break ;
                case 'BI':
                    $this->bio = $optionValue->getCode();
                    break ;
                case 'OP':
                    $this->originCountry = $optionValue->getCode();
                    break ;
                case 'OV':
                    $this->origin = $optionValue->getCode();
                    break ;
                case 'MQ':
                    $this->brand = $optionValue->getCode();
                    break ;
            }
        }

        $this->propertiesToContent();
    }
}
