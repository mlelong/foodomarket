<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Order extends \Sylius\Component\Core\Model\Order
{
    /** @var ShoppingCart|null */
    protected $shoppingCart;

    /** @var OrderStatus[]|ArrayCollection */
    protected $statusHistory;

    /** @var OrderPayment[]|ArrayCollection */
    protected $lyraPayments;

    /** @var int|null */
    protected $adjustedTTCAmount;

    public function __construct()
    {
        $this->statusHistory = new ArrayCollection();
        $this->lyraPayments = new ArrayCollection();

        parent::__construct();
    }

    /**
     * @return ShoppingCart|null
     */
    public function getShoppingCart(): ?ShoppingCart
    {
        return $this->shoppingCart;
    }

    /**
     * @param ShoppingCart|null $shoppingCart
     */
    public function setShoppingCart(?ShoppingCart $shoppingCart): void
    {
        $this->shoppingCart = $shoppingCart;
    }

    /**
     * @return OrderStatus[]|ArrayCollection
     */
    public function getStatusHistory()
    {
        return $this->statusHistory;
    }

    /**
     * @param OrderStatus $orderStatus
     */
    public function addStatusHistory(OrderStatus $orderStatus): void
    {
        foreach ($this->statusHistory as $status) {
            if ($status->getStatus() === $orderStatus->getStatus()) {
                return ;
            }
        }

        $orderStatus->setOrder($this);
        $this->statusHistory->add($orderStatus);
    }

    /**
     * @return OrderPayment[]|ArrayCollection
     */
    public function getLyraPayments()
    {
        return $this->lyraPayments;
    }

    /**
     * @param OrderPayment[] $lyraPayments
     */
    public function setLyraPayments(array $lyraPayments): void
    {
        $lyraPayments = array_map(function(OrderPayment $lyraPayment) {
            $lyraPayment->setOrder($this);
        }, $lyraPayments);

        $this->lyraPayments = $lyraPayments;
    }

    public function addLyraPayment(OrderPayment $orderPayment)
    {
        $orderPayment->setOrder($this);

        $this->lyraPayments->add($orderPayment);
    }

    /**
     * @return int|null
     */
    public function getAdjustedTTCAmount(): ?int
    {
        return $this->adjustedTTCAmount;
    }

    /**
     * @param int|null $adjustedTTCAmount
     */
    public function setAdjustedTTCAmount(?int $adjustedTTCAmount): void
    {
        $this->adjustedTTCAmount = $adjustedTTCAmount;
    }

    /**
     * @return float
     */
    public function getTotalWeight(): float
    {
        $weight = 0.0;

        /** @var OrderItem $item */
        foreach ($this->getItems() as $item) {
            $found = false;

            // Get the unit from shopping cart
            if ($this->shoppingCart !== null) {
                // Loop on shopping cart items
                foreach ($this->shoppingCart->getItems() as $shoppingCartItem) {
                    // If same variant as current order item
                    // and the selected unit is either KG or L
                    // and it is the same quantity as the one in the cart (precaution in case of duplicate shitty shopping cart state regarding order one)
                    if ($shoppingCartItem->getProductVariant()->getId() === $item->getVariant()->getId()
                        && in_array($shoppingCartItem->getUnit(), ['KG', 'L'])
                        && $shoppingCartItem->getQuantity() === $item->getQuantityFloat()
                    ) {
                        $weight += $item->getQuantityFloat();
                        $found = true;
                        break ;
                    }
                }
            }

            if (!$found) {
                $weight += $item->getQuantityFloat() * $item->getVariant()->getWeight();
            }
        }

        return $weight;
    }
}
