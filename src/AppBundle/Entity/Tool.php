<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;

/**
 * Tool
 */
class Tool implements ResourceInterface, TranslatableInterface
{
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
        getTranslation as private doGetTranslation;
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var ToolDescription[]|ArrayCollection
     */
    protected $descriptions;

    /**
     * @var int
     */
    protected $position = 0;

    /**
     * Tool constructor.
     */
    public function __construct()
    {
        $this->initializeTranslationsCollection();
        $this->descriptions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPosition() : int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return Tool
     */
    public function setPosition(int $position) : Tool
    {
        $this->position = $position;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function createTranslation()
    {
        return new ToolTranslation();
    }

    /**
     * @param null|string $locale
     *
     * @return ToolTranslation
     */
    public function getTranslation(?string $locale = null): TranslationInterface
    {
        return $this->doGetTranslation($locale);
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->getTranslation()->getTitle();
    }

    /**
     * @param string $title
     *
     * @return Tool
     */
    public function setTitle($title) : Tool
    {
        $this->getTranslation()->setTitle($title);

        return $this;
    }

    /**
     * @return ToolDescription[]|ArrayCollection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param ToolDescription[]|ArrayCollection $descriptions
     */
    public function setDescriptions($descriptions): void
    {
        $this->descriptions = $descriptions;
    }

    /**
     * @param ToolDescription $description
     *
     * @return Tool
     */
    public function addDescription(ToolDescription $description) : Tool
    {
        if (!$this->descriptions->contains($description)) {
            $description->setTool($this);
            $this->descriptions->add($description);
        }

        return $this;
    }

    /**
     * @param ToolDescription $description
     *
     * @return Tool
     */
    public function removeDescription(ToolDescription $description) : Tool
    {
        $this->descriptions->removeElement($description);

        return $this;
    }
}
