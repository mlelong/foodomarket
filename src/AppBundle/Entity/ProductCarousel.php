<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\ResourceInterface;

class ProductCarousel implements ResourceInterface
{
    /** @var int */
    private $id;

    /** @var Product|null */
    private $product;

    /** @var string|null */
    private $unit;

    /** @var array */
    private $supplierPrices = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|Product
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param null|Product $product
     */
    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return null|string
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param null|string $unit
     */
    public function setUnit(?string $unit): void
    {
        $this->unit = $unit;
    }

    /**
     * @return array
     */
    public function getSupplierPrices(): array
    {
        return $this->supplierPrices;
    }

    /**
     * @param array $supplierPrices
     */
    public function setSupplierPrices(array $supplierPrices): void
    {
        $this->supplierPrices = $supplierPrices;
    }
}