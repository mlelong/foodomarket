<?php


namespace AppBundle\Entity;


use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class CarouselItem implements ResourceInterface
{
    use TimestampableTrait;

    /** @var int */
    private $id;

    /** @var Carousel */
    private $carousel;

    /** @var Product */
    private $product;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Carousel
     */
    public function getCarousel(): Carousel
    {
        return $this->carousel;
    }

    /**
     * @param Carousel $carousel
     */
    public function setCarousel(Carousel $carousel): void
    {
        $this->carousel = $carousel;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }
}
