<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * Class OnboardingCallback
 * @package AppBundle\Entity
 */
class OnboardingCallback extends AbstractUtm implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $prospectName;

    /**
     * @var string
     */
    protected $prospectPhone;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $deliveryForm;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProspectName(): ?string
    {
        return $this->prospectName;
    }

    /**
     * @param string $prospectName
     */
    public function setProspectName(?string $prospectName): void
    {
        $this->prospectName = $prospectName;
    }

    /**
     * @return string
     */
    public function getProspectPhone(): ?string
    {
        return $this->prospectPhone;
    }

    /**
     * @param string $prospectPhone
     */
    public function setProspectPhone(?string $prospectPhone): void
    {
        $this->prospectPhone = $prospectPhone;
    }

    /**
     * @return string
     */
    public function getDeliveryForm()
    {
        return $this->deliveryForm;
    }

    /**
     * @param string $deliveryForm
     */
    public function setDeliveryForm($deliveryForm)
    {
        $this->deliveryForm = $deliveryForm;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}