<?php


namespace AppBundle\Entity;


use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class OrderPayment implements ResourceInterface
{
    use TimestampableTrait;

    /** @var int */
    private $id;

    /** @var Order */
    private $order;

    /** @var string */
    private $lyraId;

    /** @var string */
    private $lyraOrderStatus;

    /** @var string */
    private $lyraPaymentStatus;

    /** @var array */
    private $lyraOrderData;

    /** @var array */
    private $lyraPaymentData;

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getLyraId(): string
    {
        return $this->lyraId;
    }

    /**
     * @param string $lyraId
     */
    public function setLyraId(string $lyraId): void
    {
        $this->lyraId = $lyraId;
    }

    /**
     * @return string
     */
    public function getLyraOrderStatus(): string
    {
        return $this->lyraOrderStatus;
    }

    /**
     * @param string $lyraOrderStatus
     */
    public function setLyraOrderStatus(string $lyraOrderStatus): void
    {
        $this->lyraOrderStatus = $lyraOrderStatus;
    }

    /**
     * @return array
     */
    public function getLyraOrderData(): array
    {
        return $this->lyraOrderData;
    }

    /**
     * @param array $lyraOrderData
     */
    public function setLyraOrderData(array $lyraOrderData): void
    {
        $this->lyraOrderData = $lyraOrderData;
    }

    /**
     * @return string
     */
    public function getLyraPaymentStatus(): string
    {
        return $this->lyraPaymentStatus;
    }

    /**
     * @param string $lyraPaymentStatus
     */
    public function setLyraPaymentStatus(string $lyraPaymentStatus): void
    {
        $this->lyraPaymentStatus = $lyraPaymentStatus;
    }

    /**
     * @return array
     */
    public function getLyraPaymentData(): array
    {
        return $this->lyraPaymentData;
    }

    /**
     * @param array $lyraPaymentData
     */
    public function setLyraPaymentData(array $lyraPaymentData): void
    {
        $this->lyraPaymentData = $lyraPaymentData;
    }
}
