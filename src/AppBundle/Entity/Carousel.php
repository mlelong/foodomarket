<?php


namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class Carousel implements ResourceInterface
{
    use TimestampableTrait;

    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var CarouselItem[]|ArrayCollection */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return CarouselItem[]|ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param CarouselItem[]|ArrayCollection $items
     */
    public function setItems($items): void
    {
        $this->items = new ArrayCollection();

        foreach ($items as $item) {
            $this->addItem($item);
        }
    }

    public function addItem(CarouselItem $item)
    {
        if (!$this->items->contains($item)) {
            $item->setCarousel($this);
            $this->items->add($item);
        }
    }

    public function removeItem(CarouselItem $item)
    {
        $this->items->removeElement($item);
    }
}
