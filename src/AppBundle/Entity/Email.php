<?php

namespace AppBundle\Entity;

use JMS\JobQueueBundle\Entity\Job;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class Email implements ResourceInterface
{
    const STATUS_PENDING = 'PENDING';
    const STATUS_SENT = 'SENT';
    const STATUS_ERROR = 'ERROR';

    const MANAGER_SENDINBLUE = 'sendinblue';
    const MANAGER_MAILJET = 'mailjet';
    
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $subject;

    /**
     * @var array
     */
    protected $from = [];

    /**
     * @var array
     */
    protected $to = [];

    /**
     * @var array
     */
    protected $replyTo = [];

    /**
     * @var array
     */
    protected $cc = [];

    /**
     * @var array
     */
    protected $bcc = [];

    /**
     * @var string|null
     */
    protected $htmlBody;

    /**
     * @var string|null
     */
    protected $textBody;

    /**
     * @var array
     */
    protected $attachments = [];

    /**
     * @var string
     */
    protected $mailManagerName;

    /**
     * @var string
     */
    protected $templateId;

    /**
     * @var array
     */
    protected $variables = [];

    /**
     * @var string
     */
    protected $status = self::STATUS_PENDING;

    /**
     * @var Job|null
     */
    protected $job;

    /**
     * @var array
     */
    protected $apiResponse = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string|null $subject
     */
    public function setSubject(?string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getFrom(): array
    {
        return $this->from;
    }

    /**
     * @param array $from
     */
    public function setFrom(array $from): void
    {
        $this->from = $from;
    }

    /**
     * @return array
     */
    public function getTo(): array
    {
        return $this->to;
    }

    /**
     * @param array $to
     */
    public function setTo(array $to): void
    {
        $this->to = $to;
    }

    /**
     * @return array
     */
    public function getReplyTo(): array
    {
        return $this->replyTo;
    }

    /**
     * @param array $replyTo
     */
    public function setReplyTo(array $replyTo): void
    {
        $this->replyTo = $replyTo;
    }

    /**
     * @return array
     */
    public function getCc(): array
    {
        return $this->cc;
    }

    /**
     * @param array $cc
     */
    public function setCc(array $cc): void
    {
        $this->cc = $cc;
    }

    /**
     * @return array
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    /**
     * @param array $bcc
     */
    public function setBcc(array $bcc): void
    {
        $this->bcc = $bcc;
    }

    /**
     * @return string|null
     */
    public function getHtmlBody(): ?string
    {
        return $this->htmlBody;
    }

    /**
     * @param string|null $htmlBody
     */
    public function setHtmlBody(?string $htmlBody): void
    {
        $this->htmlBody = $htmlBody;
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param array $attachments
     */
    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    /**
     * @return string
     */
    public function getMailManagerName(): string
    {
        return $this->mailManagerName;
    }

    /**
     * @param string $mailManagerName
     */
    public function setMailManagerName(string $mailManagerName): void
    {
        $this->mailManagerName = $mailManagerName;
    }

    /**
     * @return string|null
     */
    public function getTemplateId(): ?string
    {
        return $this->templateId;
    }

    /**
     * @param string|null $templateId
     */
    public function setTemplateId(?string $templateId): void
    {
        $this->templateId = $templateId;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Job|null
     */
    public function getJob(): ?Job
    {
        return $this->job;
    }

    /**
     * @param Job|null $job
     */
    public function setJob(?Job $job): void
    {
        $this->job = $job;
    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     */
    public function setVariables(array $variables): void
    {
        $this->variables = $variables;
    }

    /**
     * @return null|string
     */
    public function getTextBody(): ?string
    {
        return $this->textBody;
    }

    /**
     * @param null|string $textBody
     */
    public function setTextBody(?string $textBody): void
    {
        $this->textBody = $textBody;
    }

    /**
     * @return array
     */
    public function getApiResponse(): array
    {
        return $this->apiResponse;
    }

    /**
     * @param array $apiResponse
     */
    public function setApiResponse(array $apiResponse): void
    {
        $this->apiResponse = $apiResponse;
    }
}