<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Mercuriale
 * @package AppBundle\Entity
 * @Vich\Uploadable
 */
class Mercuriale implements ResourceInterface
{
    use TimestampableTrait;

    /** @var int */
    private $id;

    /**
     * @Vich\UploadableField(mapping="mercuriale", fileNameProperty="fileName", size="fileSize")
     * @var File|null
     */
    private $file;

    /** @var string|null */
    private $fileName;

    /** @var int|null */
    private $fileSize;

    /** @var Supplier */
    private $supplier;

    /** @var Restaurant|null */
    private $restaurant;

    /** @var \DateTime */
    private $dlcFrom;

    /** @var \DateTime|null */
    private $dlcTo;

    /** @var string|null */
    private $category;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(?Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return \DateTime
     */
    public function getDlcFrom(): ?\DateTime
    {
        return $this->dlcFrom;
    }

    /**
     * @param \DateTime $dlcFrom
     */
    public function setDlcFrom(?\DateTime $dlcFrom): void
    {
        $this->dlcFrom = $dlcFrom;
    }

    /**
     * @return \DateTime|null
     */
    public function getDlcTo(): ?\DateTime
    {
        return $this->dlcTo;
    }

    /**
     * @param \DateTime|null $dlcTo
     */
    public function setDlcTo(?\DateTime $dlcTo): void
    {
        $this->dlcTo = $dlcTo;
    }

    /**
     * @return File
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setFile(?File $file = null): void
    {
        $this->file = $file;

        if ($file !== null) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return null|string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param null|string $fileName
     */
    public function setFileName(?string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     */
    public function setFileSize(?int $fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    /**
     * @return Restaurant|null
     */
    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    /**
     * @param Restaurant|null $restaurant
     */
    public function setRestaurant(?Restaurant $restaurant): void
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @return string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(?string $category): void
    {
        $this->category = $category;
    }
}