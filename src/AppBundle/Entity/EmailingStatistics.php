<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * EmailingStatistics
 */
class EmailingStatistics implements ResourceInterface
{

    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $openingCount;

    /**
     * @var int
     */
    private $clickCount;

    /**
     * @var int
     */
    private $noOpeningCount;

    /**
     * @var int
     */
    private $note;

    /**
     * @var bool
     */
    private $unsubscribed;


    /**
     * @var float
     */
    private $openingRate;

    /**
     * @var float
     */
    private $clickRate;

    public function __construct()
    {
        $this->unsubscribed = false;
        $this->note = 0;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return EmailingStatistics
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set openingCount.
     *
     * @param int $openingCount
     *
     * @return EmailingStatistics
     */
    public function setOpeningCount($openingCount)
    {
        $this->openingCount = $openingCount;

        return $this;
    }

    /**
     * Get openingCount.
     *
     * @return int
     */
    public function getOpeningCount()
    {
        return $this->openingCount;
    }

    /**
     * Set clickCount.
     *
     * @param int $clickCount
     *
     * @return EmailingStatistics
     */
    public function setClickCount($clickCount)
    {
        $this->clickCount = $clickCount;

        return $this;
    }

    /**
     * Get clickCount.
     *
     * @return int
     */
    public function getClickCount()
    {
        return $this->clickCount;
    }

    /**
     * Set noOpeningCount.
     *
     * @param int $noOpeningCount
     *
     * @return EmailingStatistics
     */
    public function setNoOpeningCount($noOpeningCount)
    {
        $this->noOpeningCount = $noOpeningCount;

        return $this;
    }

    /**
     * Get noOpeningCount.
     *
     * @return int
     */
    public function getNoOpeningCount()
    {
        return $this->noOpeningCount;
    }

    /**
     * Set note.
     *
     * @param int $note
     *
     * @return EmailingStatistics
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return int
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set unsubscribed.
     *
     * @param bool $unsubscribed
     *
     * @return EmailingStatistics
     */
    public function setUnsubscribed($unsubscribed)
    {
        $this->unsubscribed = $unsubscribed;

        return $this;
    }

    /**
     * Get unsubscribed.
     *
     * @return bool
     */
    public function getUnsubscribed()
    {
        return $this->unsubscribed;
    }

    /**
     * @return float
     */
    public function getOpeningRate(): float
    {
        return $this->openingRate;
    }

    /**
     * @param float $openingRate
     */
    public function setOpeningRate(float $openingRate)
    {
        $this->openingRate = $openingRate;
        return $this;
    }

    /**
     * @return float
     */
    public function getClickRate(): float
    {
        return $this->clickRate;
    }

    /**
     * @param float $clickRate
     */
    public function setClickRate(float $clickRate)
    {
        $this->clickRate = $clickRate;
        return $this;
    }

}
