<?php

namespace AppBundle\Entity;


use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Product\Model\ProductInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * ProductPricesStatistics
 */

class ProductPricesStatistics
{

    use TimestampableTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \stdClass
     */
    private $product;

    /**
     * @var bool
     */
    private $isBio;

    /**
     * @var float
     */
    private $averageKgPrice;

    /**
     * @var float
     */
    private $minimumKgPrice;

    /**
     * @var float
     */
    private $maximumKgPrice;

    /**
     * @var float
     */
    private $averageUnitPrice;

    /**
     * @var float
     */
    private $minimumUnitPrice;

    /**
     * @var float
     */
    private $maximumUnitPrice;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * {@inheritdoc}
     */
    public function setProduct(ProductInterface $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Set isBio.
     *
     * @param bool $isBio
     *
     * @return ProductPricesStatistics
     */
    public function setIsBio($isBio)
    {
        $this->isBio = $isBio;

        return $this;
    }

    /**
     * Get isBio.
     *
     * @return bool
     */
    public function isBio()
    {
        return $this->getIsBio();
    }

    /**
     * Set averageKgPrice.
     *
     * @param float $averageKgPrice
     *
     * @return ProductPricesStatistics
     */
    public function setAverageKgPrice($averageKgPrice)
    {
        $this->averageKgPrice = $averageKgPrice;

        return $this;
    }

    /**
     * Get averageKgPrice.
     *
     * @return float
     */
    public function getAverageKgPrice()
    {
        return $this->averageKgPrice;
    }

    /**
     * Set minimumKgPrice.
     *
     * @param float $minimumKgPrice
     *
     * @return ProductPricesStatistics
     */
    public function setMinimumKgPrice($minimumKgPrice)
    {
        $this->minimumKgPrice = $minimumKgPrice;

        return $this;
    }

    /**
     * Get minimumKgPrice.
     *
     * @return float
     */
    public function getMinimumKgPrice()
    {
        return $this->minimumKgPrice;
    }

    /**
     * Set maximumKgPrice.
     *
     * @param float $maximumKgPrice
     *
     * @return ProductPricesStatistics
     */
    public function setMaximumKgPrice($maximumKgPrice)
    {
        $this->maximumKgPrice = $maximumKgPrice;

        return $this;
    }

    /**
     * Get maximumKgPrice.
     *
     * @return float
     */
    public function getMaximumKgPrice()
    {
        return $this->maximumKgPrice;
    }

    /**
     * Set averageUnitPrice.
     *
     * @param float $averageUnitPrice
     *
     * @return ProductPricesStatistics
     */
    public function setAverageUnitPrice($averageUnitPrice)
    {
        $this->averageUnitPrice = $averageUnitPrice;

        return $this;
    }

    /**
     * Get averageUnitPrice.
     *
     * @return float
     */
    public function getAverageUnitPrice()
    {
        return $this->averageUnitPrice;
    }

    /**
     * Set minimumUnitPrice.
     *
     * @param float $minimumUnitPrice
     *
     * @return ProductPricesStatistics
     */
    public function setMinimumUnitPrice($minimumUnitPrice)
    {
        $this->minimumUnitPrice = $minimumUnitPrice;

        return $this;
    }

    /**
     * Get minimumUnitPrice.
     *
     * @return float
     */
    public function getMinimumUnitPrice()
    {
        return $this->minimumUnitPrice;
    }

    /**
     * Set maximumUnitPrice.
     *
     * @param float $maximumUnitPrice
     *
     * @return ProductPricesStatistics
     */
    public function setMaximumUnitPrice($maximumUnitPrice)
    {
        $this->maximumUnitPrice = $maximumUnitPrice;

        return $this;
    }

    /**
     * Get maximumUnitPrice.
     *
     * @return float
     */
    public function getMaximumUnitPrice()
    {
        return $this->maximumUnitPrice;
    }
}
