<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class BlockedPrice implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var Product|null
     */
    protected $product;

    /**
     * @var ProductVariant|null
     */
    protected $productVariant;

    /**
     * @var Supplier|null
     */
    protected $supplier;

    /**
     * @var Restaurant|null
     */
    protected $restaurant;

    /**
     * @var int|null
     */
    protected $kgPrice;

    /**
     * @var int|null
     */
    protected $unitPrice;

    /**
     * @var \DateTime|null
     */
    protected $validFrom;

    /**
     * @var \DateTime|null
     */
    protected $validTo;

    /** @var ProductOptionValue[]|ArrayCollection */
    protected $optionValues;

    public function __construct()
    {
        $this->optionValues = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ProductOptionValue[]|ArrayCollection
     */
    public function getOptionValues()
    {
        return $this->optionValues;
    }

    /**
     * @param ProductOptionValue[]|ArrayCollection $optionValues
     */
    public function setOptionValues($optionValues): void
    {
        $this->optionValues = $optionValues;
    }

    /**
     * @param ProductOptionValue $optionValue
     */
    public function addOptionValue(ProductOptionValue $optionValue): void
    {
        if (!$this->optionValues->contains($optionValue)) {
            $this->optionValues->add($optionValue);
        }
    }

    /**
     * @param ProductOptionValue $optionValue
     */
    public function removeOptionValue(ProductOptionValue $optionValue): void
    {
        $this->optionValues->removeElement($optionValue);
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     */
    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return ProductVariant|null
     */
    public function getProductVariant(): ?ProductVariant
    {
        return $this->productVariant;
    }

    /**
     * @param ProductVariant|null $productVariant
     */
    public function setProductVariant(?ProductVariant $productVariant): void
    {
        $this->productVariant = $productVariant;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier|null $supplier
     */
    public function setSupplier(?Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Restaurant|null
     */
    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    /**
     * @param Restaurant|null $restaurant
     */
    public function setRestaurant(?Restaurant $restaurant): void
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @return int|null
     */
    public function getKgPrice(): ?int
    {
        return $this->kgPrice;
    }

    /**
     * @param int|null $kgPrice
     */
    public function setKgPrice(?int $kgPrice): void
    {
        $this->kgPrice = $kgPrice;
    }

    /**
     * @return int|null
     */
    public function getUnitPrice(): ?int
    {
        return $this->unitPrice;
    }

    /**
     * @param int|null $unitPrice
     */
    public function setUnitPrice(?int $unitPrice): void
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return \DateTime|null
     */
    public function getValidFrom(): ?\DateTime
    {
        return $this->validFrom;
    }

    /**
     * @param \DateTime|null $validFrom
     */
    public function setValidFrom(?\DateTime $validFrom): void
    {
        $this->validFrom = $validFrom;
    }

    /**
     * @return \DateTime|null
     */
    public function getValidTo(): ?\DateTime
    {
        return $this->validTo;
    }

    /**
     * @param \DateTime|null $validTo
     */
    public function setValidTo(?\DateTime $validTo): void
    {
        $this->validTo = $validTo;
    }
}