<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Class InvoiceDeliveryNote
 * @package AppBundle\Entity
 */
class InvoiceDeliveryNote implements ResourceInterface
{
    /** @var int */
    private $id;

    /** @var Invoice|null */
    private $invoice;

    /** @var string|null */
    private $number;

    /** @var \DateTime|null */
    private $date;

    /** @var ArrayCollection|InvoiceDeliveryNoteItem[] */
    private $items;

    /** @var float|null */
    private $total = 0.0;

    /**
     * InvoiceDeliveryNote constructor.
     */
    public function __construct()
    {
        $this->number = '';
        $this->items = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Invoice|null
     */
    public function getInvoice(): ?Invoice
    {
        return $this->invoice;
    }

    /**
     * @param Invoice|null $invoice
     */
    public function setInvoice(?Invoice $invoice): void
    {
        $this->invoice = $invoice;
    }

    /**
     * @return null|string
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param null|string $number
     */
    public function setNumber(?string $number): void
    {
        $this->number = $number;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     */
    public function setDate(?\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return InvoiceDeliveryNoteItem[]|ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param InvoiceDeliveryNoteItem[]|ArrayCollection $items
     */
    public function setItems($items): void
    {
        $this->items = $items;

        $total = 0.0;

        foreach ($this->items as $item) {
            $total += $item->getTotalPrice();
        }

        $this->setTotal($total);
    }

    /**
     * @param InvoiceDeliveryNoteItem $item
     */
    public function addItem(InvoiceDeliveryNoteItem $item)
    {
        if (!$this->items->contains($item)) {
            $item->setInvoiceDeliveryNote($this);
            $this->items->add($item);

            $this->setTotal($this->total + $item->getTotalPrice());
        }
    }

    /**
     * @param InvoiceDeliveryNoteItem $item
     */
    public function removeItem(InvoiceDeliveryNoteItem $item)
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            $item->setInvoiceDeliveryNote(null);

            $this->setTotal($this->total - $item->getTotalPrice());
        }
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param float|null $total
     */
    public function setTotal(?float $total): void
    {
        $this->total = $total;
    }
}