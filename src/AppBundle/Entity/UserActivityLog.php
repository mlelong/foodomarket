<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class UserActivityLog extends AbstractUtm implements ResourceInterface
{
    use TimestampableTrait;

    /** @var int */
    private $id;

    /** @var string */
    private $email;

    /** @var string */
    private $route;

    /** @var array */
    private $routeParams;

    /** @var array */
    private $queryParams;

    /** @var array */
    private $requestParams;

    /** @var string|null */
    private $referer;

    /** @var string|null */
    private $cookieEmail;

    /** @var string|null */
    private $cookieRestaurant;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute(string $route): void
    {
        $this->route = $route;
    }

    /**
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    /**
     * @param array $routeParams
     */
    public function setRouteParams(array $routeParams): void
    {
        $this->routeParams = $routeParams;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @param array $queryParams
     */
    public function setQueryParams(array $queryParams): void
    {
        $this->queryParams = $queryParams;
    }

    /**
     * @return array
     */
    public function getRequestParams(): array
    {
        return $this->requestParams;
    }

    /**
     * @param array $requestParams
     */
    public function setRequestParams(array $requestParams): void
    {
        $this->requestParams = $requestParams;
    }

    /**
     * @return string|null
     */
    public function getReferer(): ?string
    {
        return $this->referer;
    }

    /**
     * @param string|null $referer
     */
    public function setReferer(?string $referer): void
    {
        $this->referer = $referer;
    }

    /**
     * @return string|null
     */
    public function getCookieEmail(): ?string
    {
        return $this->cookieEmail;
    }

    /**
     * @param string|null $cookieEmail
     */
    public function setCookieEmail(?string $cookieEmail): void
    {
        $this->cookieEmail = $cookieEmail;
    }

    /**
     * @return string|null
     */
    public function getCookieRestaurant(): ?string
    {
        return $this->cookieRestaurant;
    }

    /**
     * @param string|null $cookieRestaurant
     */
    public function setCookieRestaurant(?string $cookieRestaurant): void
    {
        $this->cookieRestaurant = $cookieRestaurant;
    }
}