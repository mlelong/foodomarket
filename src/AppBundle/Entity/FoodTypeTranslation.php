<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\AbstractTranslation;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * FoodTypeTranslation
 */
class FoodTypeTranslation extends AbstractTranslation implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $specialities = [];

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return FoodTypeTranslation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set specialities
     *
     * @param array $specialities
     *
     * @return FoodTypeTranslation
     */
    public function setSpecialities($specialities)
    {
        $this->specialities = $specialities;

        return $this;
    }

    /**
     * Get specialities
     *
     * @return array
     */
    public function getSpecialities()
    {
        return $this->specialities;
    }

    /**
     * @param string $speciality
     */
    public function addSpeciality($speciality)
    {
        $this->specialities = array_unique(array_merge($this->specialities, [$speciality]));
    }

    /**
     * @param string $speciality
     */
    public function removeSpeciality($speciality)
    {
        $this->specialities = array_filter($this->specialities, function($value) use ($speciality) {
            return $value != $speciality;
        });
    }
}
