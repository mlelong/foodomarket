<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

class InvoiceDeliveryNoteItem implements ResourceInterface
{
    /** @var int */
    private $id;

    /** @var InvoiceDeliveryNote|null */
    private $invoiceDeliveryNote;

    /** @var string|null */
    private $name;

    /** @var float|null */
    private $quantity;

    /** @var string|null */
    private $unit;

    /** @var float|null */
    private $unitPrice;

    /** @var float|null */
    private $totalPrice;

    /** @var ProductVariant|null */
    private $variant;

    /** @var SupplierProductPriceHistory|null */
    private $price;

    /** @var string|null */
    private $reference;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return InvoiceDeliveryNote|null
     */
    public function getInvoiceDeliveryNote(): ?InvoiceDeliveryNote
    {
        return $this->invoiceDeliveryNote;
    }

    /**
     * @param InvoiceDeliveryNote|null $invoiceDeliveryNote
     */
    public function setInvoiceDeliveryNote(?InvoiceDeliveryNote $invoiceDeliveryNote): void
    {
        $this->invoiceDeliveryNote = $invoiceDeliveryNote;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float|null $quantity
     */
    public function setQuantity(?float $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string|null $unit
     */
    public function setUnit(?string $unit): void
    {
        $this->unit = $unit;
    }

    /**
     * @return float|null
     */
    public function getUnitPrice(): ?float
    {
        return $this->unitPrice;
    }

    /**
     * @param float|null $unitPrice
     */
    public function setUnitPrice(?float $unitPrice): void
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float|null
     */
    public function getTotalPrice(): ?float
    {
        return $this->totalPrice;
    }

    /**
     * @param float|null $totalPrice
     */
    public function setTotalPrice(?float $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return ProductVariant|null
     */
    public function getVariant(): ?ProductVariant
    {
        return $this->variant;
    }

    /**
     * @param ProductVariant|null $variant
     */
    public function setVariant(?ProductVariant $variant): void
    {
        $this->variant = $variant;
    }

    /**
     * @return SupplierProductPriceHistory|null
     */
    public function getPrice(): ?SupplierProductPriceHistory
    {
        return $this->price;
    }

    /**
     * @param SupplierProductPriceHistory|null $price
     */
    public function setPrice(?SupplierProductPriceHistory $price): void
    {
        $this->price = $price;
    }

    /**
     * @return null|string
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @param null|string $reference
     */
    public function setReference(?string $reference): void
    {
        $this->reference = $reference;
    }
}