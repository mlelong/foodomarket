<?php

namespace AppBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class Onboarding extends AbstractUtm implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $category;

    /**
     * @var float
     */
    protected $turnover = 0.0;

    /**
     * @var string
     */
    protected $supplierName;

    /**
     * @var string
     */
    protected $restaurantName;

    /**
     * @var string
     */
    protected $contactName;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $zipCode;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $telephone;

    /**
     * @var string
     */
    protected $contactEmail;

    /**
     * @var string
     */
    protected $contactPassword;

    /**
     * @var string
     */
    protected $kbis;

    /**
     * @var string
     */
    protected $deliveryForm;

    /**
     * @var string
     */
    protected $deliveryRange;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var Supplier
     */
    protected $chosenSupplier;

    /**
     * @var array
     */
    protected $products = [];

    /**
     * @var int
     */
    protected $productRating;

    /**
     * @var int
     */
    protected $serviceRating;

    /**
     * @var float|null
     */
    protected $userPrice;

    /**
     * @var float|null
     */
    protected $suggestedPrice;

    /**
     * @var Supplier|null
     */
    protected $suggestedSupplier;

    /**
     * @var float|null
     */
    protected $bestPrice;

    /**
     * @var float|null
     */
    protected $worstPrice;

    /**
     * @var int
     */
    protected $purchaseIndex;

    /**
     * @var float
     */
    protected $purchaseCursor;

    /**
     * @var bool
     */
    protected $cron = false;

    /**
     * @var string|null
     */
    protected $step;

    /**
     * @var bool
     */
    protected $hubspotSync = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(?string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return float
     */
    public function getTurnover(): ?float
    {
        return $this->turnover;
    }

    /**
     * @param float $turnover
     */
    public function setTurnover(?float $turnover): void
    {
        $this->turnover = $turnover;
    }

    /**
     * @return array
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }

    /**
     * @param array|null $products
     */
    public function setProducts(?array $products): void
    {
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function getSupplierName(): ?string
    {
        return $this->supplierName;
    }

    /**
     * @param string $supplierName
     */
    public function setSupplierName(?string $supplierName): void
    {
        $this->supplierName = $supplierName;
    }

    /**
     * @return string
     */
    public function getRestaurantName(): ?string
    {
        return $this->restaurantName;
    }

    /**
     * @param string $restaurantName
     */
    public function setRestaurantName(?string $restaurantName): void
    {
        $this->restaurantName = $restaurantName;
    }

    /**
     * @return string
     */
    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     */
    public function setContactName(?string $contactName): void
    {
        $this->contactName = $contactName;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getKbis()
    {
        return $this->kbis;
    }

    /**
     * @param string $kbis
     */
    public function setKbis($kbis)
    {
        $this->kbis = $kbis;
    }

    /**
     * @return string
     */
    public function getDeliveryRange(): ?string
    {
        return $this->deliveryRange;
    }

    /**
     * @param string $deliveryRange
     */
    public function setDeliveryRange(?string $deliveryRange): void
    {
        $this->deliveryRange = $deliveryRange;
    }

    /**
     * @return Supplier
     */
    public function getChosenSupplier(): ?Supplier
    {
        return $this->chosenSupplier;
    }

    /**
     * @param Supplier $chosenSupplier
     */
    public function setChosenSupplier(?Supplier $chosenSupplier): void
    {
        $this->chosenSupplier = $chosenSupplier;
    }

    /**
     * @return string
     */
    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     */
    public function setContactEmail(?string $contactEmail): void
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return string
     */
    public function getDeliveryForm()
    {
        return $this->deliveryForm;
    }

    /**
     * @param string $deliveryForm
     */
    public function setDeliveryForm($deliveryForm)
    {
        $this->deliveryForm = $deliveryForm;
    }

    /**
     * @return string
     */
    public function getContactPassword(): ?string
    {
        return $this->contactPassword;
    }

    /**
     * @param string $contactPassword
     */
    public function setContactPassword(?string $contactPassword): void
    {
        $this->contactPassword = $contactPassword;
    }

    /**
     * @return string
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(?string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getProductRating(): ?int
    {
        return $this->productRating;
    }

    /**
     * @param int $productRating
     */
    public function setProductRating(?int $productRating): void
    {
        $this->productRating = $productRating;
    }

    /**
     * @return int
     */
    public function getServiceRating(): ?int
    {
        return $this->serviceRating;
    }

    /**
     * @param int $serviceRating
     */
    public function setServiceRating(?int $serviceRating): void
    {
        $this->serviceRating = $serviceRating;
    }

    /**
     * @return float|null
     */
    public function getSuggestedPrice(): ?float
    {
        return $this->suggestedPrice;
    }

    /**
     * @param float|null $suggestedPrice
     */
    public function setSuggestedPrice(?float $suggestedPrice): void
    {
        $this->suggestedPrice = $suggestedPrice;
    }

    /**
     * @return Supplier|null
     */
    public function getSuggestedSupplier(): ?Supplier
    {
        return $this->suggestedSupplier;
    }

    /**
     * @param Supplier|null $suggestedSupplier
     */
    public function setSuggestedSupplier(?Supplier $suggestedSupplier): void
    {
        $this->suggestedSupplier = $suggestedSupplier;
    }

    /**
     * @return int
     */
    public function getPurchaseIndex(): ?int
    {
        return $this->purchaseIndex;
    }

    /**
     * @param int $purchaseIndex
     */
    public function setPurchaseIndex(?int $purchaseIndex): void
    {
        $this->purchaseIndex = $purchaseIndex;
    }

    /**
     * @return float
     */
    public function getPurchaseCursor(): ?float
    {
        return $this->purchaseCursor;
    }

    /**
     * @param float $purchaseCursor
     */
    public function setPurchaseCursor(?float $purchaseCursor): void
    {
        $this->purchaseCursor = $purchaseCursor;
    }

    /**
     * @return bool
     */
    public function isCron(): bool
    {
        return $this->cron;
    }

    /**
     * @param bool $cron
     */
    public function setCron(bool $cron): void
    {
        $this->cron = $cron;
    }

    /**
     * @return float
     */
    public function getUserPrice(): ?float
    {
        return $this->userPrice;
    }

    /**
     * @param float $userPrice
     */
    public function setUserPrice(?float $userPrice): void
    {
        $this->userPrice = $userPrice;
    }

    /**
     * @return float|null
     */
    public function getBestPrice(): ?float
    {
        return $this->bestPrice;
    }

    /**
     * @param float|null $bestPrice
     */
    public function setBestPrice(?float $bestPrice): void
    {
        $this->bestPrice = $bestPrice;
    }

    /**
     * @return float|null
     */
    public function getWorstPrice(): ?float
    {
        return $this->worstPrice;
    }

    /**
     * @param float|null $worstPrice
     */
    public function setWorstPrice(?float $worstPrice): void
    {
        $this->worstPrice = $worstPrice;
    }

    /**
     * @return null|string
     */
    public function getStep(): ?string
    {
        return $this->step;
    }

    /**
     * @param null|string $step
     */
    public function setStep(?string $step): void
    {
        $this->step = $step;
    }

    /**
     * @return bool
     */
    public function isHubspotSync(): bool
    {
        return $this->hubspotSync;
    }

    /**
     * @param bool $hubspotSync
     */
    public function setHubspotSync(bool $hubspotSync): void
    {
        $this->hubspotSync = $hubspotSync;
    }
}