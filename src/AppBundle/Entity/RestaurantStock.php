<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * RestaurantStock
 *
 */
class RestaurantStock implements ResourceInterface
{
    use TimestampableTrait;

    /**
     * @var int
     *
     */
    private $id;

    /**
     * @var string
     *
     */
    private $name;

    /**
     * @var bool
     *
     */
    private $deleted;

    /**
     * @var Restaurant
     */
    protected $restaurant;

    /** @var Supplier|null */
    protected $supplier;
    
    /**
     * @var ArrayCollection
     */
    protected $items;

    /**
     * RestaurantStock constructor.
     */
    public function __construct()
    {
        $this->deleted = false;
        $this->items = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RestaurantStock
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return RestaurantStock
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set restaurant
     *
     * @param Restaurant $restaurant
     *
     * @return RestaurantStock
     */
    public function setRestaurant(Restaurant $restaurant = null)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant
     *
     * @return Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Add item
     *
     * @param \AppBundle\Entity\RestaurantStockItem $item
     *
     * @return RestaurantStock
     */
    public function addItem(\AppBundle\Entity\RestaurantStockItem $item)
    {
        $item->setRestaurantStock($this);
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \AppBundle\Entity\RestaurantStockItem $item
     */
    public function removeItem(\AppBundle\Entity\RestaurantStockItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection|RestaurantStockItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return Supplier|null
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param Supplier|null $supplier
     */
    public function setSupplier(?Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }
}
