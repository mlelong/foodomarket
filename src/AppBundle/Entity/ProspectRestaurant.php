<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Addressing\Model\Address;

class ProspectRestaurant extends Address
{
    /**
     * @var Prospect[]|ArrayCollection
     */
    private $prospects;

    /**
     * ProspectRestaurant constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->prospects = new ArrayCollection();
    }

    /**
     * @return Prospect[]|ArrayCollection
     */
    public function getProspects()
    {
        return $this->prospects;
    }

    /**
     * @param Prospect[]|ArrayCollection $prospects
     *
     * @return ProspectRestaurant
     */
    public function setProspects($prospects): ProspectRestaurant
    {
        $this->prospects = $prospects;

        return $this;
    }

    /**
     * @param Prospect $prospect
     *
     * @return ProspectRestaurant
     */
    public function addProspect(Prospect $prospect): ProspectRestaurant
    {
        if ($this->prospects->contains($prospect)) {
            $this->prospects->add($prospect);
        }

        return $this;
    }
}