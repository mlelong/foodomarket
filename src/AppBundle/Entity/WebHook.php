<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\TimestampableEntity;
use Sylius\Component\Resource\Model\ResourceInterface;

class WebHook implements ResourceInterface
{
    use TimestampableEntity;

    /** @var int */
    private $id;

    /** @var string */
    private $hookName;

    /** @var string|null */
    private $hookData;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHookName(): string
    {
        return $this->hookName;
    }

    /**
     * @param string $hookName
     */
    public function setHookName(string $hookName): void
    {
        $this->hookName = $hookName;
    }

    /**
     * @return string|null
     */
    public function getHookData(): ?string
    {
        return $this->hookData;
    }

    /**
     * @param string|null $hookData
     */
    public function setHookData(?string $hookData): void
    {
        $this->hookData = $hookData;
    }
}