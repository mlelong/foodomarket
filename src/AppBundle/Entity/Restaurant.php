<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * Restaurant
 *
 */
class Restaurant implements ResourceInterface, \Serializable
{
    use TimestampableTrait;

    /**
     * @var int
     *
     */
    private $id;

    /**
     * @var string
     *
     */
    private $name;

    /**
     * @var bool
     *
     */
    private $enabled;

    /**
     * @var string
     *
     */
    private $street;

    /**
     * @var string
     *
     */
    private $city;

    /**
     * @var string
     *
     */
    private $postcode;

    /**
     * @var string
     *
     */
    private $countryCode = 'FR';

    /**
     *
     * account email to retrieve mercurials for auto import // deprecated for now.
     * @var string
     *
     */
    private $email;

    /**
     *
     * account password
     * @var string
     *
     */
    private $password;

    /**
     * @var ArrayCollection|DeliveryCost[]
     */
    protected $deliveryCosts;

    /** @var string */
    private $currentSupplier;

    /** @var float */
    private $orderAmountPerMonth;

    /** @var string */
    private $siren;

    /** @var string */
    private $interlocutor;

    /** @var string */
    private $deliveryHours;

    /** @var Customer[]|ArrayCollection */
    private $customers;

    /**
     * constructor.
     *
     * initialize Boolean
     * initialize ArrayCollection
     */
    public function __construct()
    {
        $this->enabled = false;
        $this->customers = new ArrayCollection();
        $this->deliveryCosts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Restaurant
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Restaurant
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Restaurant
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Restaurant
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Restaurant
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return Restaurant
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Add employee
     *
     * @param Customer $customer
     *
     * @return Restaurant
     */
    public function addCustomer(Customer $customer)
    {
        $customer->addRestaurant($this);

        if (!$this->customers->contains($customer)) {
            $this->customers->add($customer);
        }

        return $this;
    }

    /**
     * Remove employee
     *
     * @param Customer $customer
     * @return Restaurant
     */
    public function removeCustomer(Customer $customer)
    {
        $customer->removeRestaurant($this);
        $this->customers->removeElement($customer);

        return $this;
    }

    /**
     * Get customers
     *
     * @return Customer[]|ArrayCollection
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * @param Customer[]|ArrayCollection $customers
     * @return $this
     */
    public function setCustomers($customers)
    {
        foreach ($customers as $customer) {
            $customer->addRestaurant($this);
        }

        $this->customers = $customers;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Restaurant
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Restaurant
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection|DeliveryCost[]
     */
    public function getDeliveryCosts()
    {
        return $this->deliveryCosts;
    }

    /**
     * @param Supplier $supplier
     *
     * @return bool
     */
    public function hasDeliveryCost(Supplier $supplier): bool
    {
        foreach($this->deliveryCosts as $deliveryCost) {
            if ($deliveryCost->getSupplier()->getId() == $supplier->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add deliveryCost
     *
     * @param DeliveryCost $deliveryCost
     *
     * @return Restaurant
     */
    public function addDeliveryCost($deliveryCost)
    {
        $this->deliveryCosts[] = $deliveryCost;
        $deliveryCost->setRestaurant($this);
        return $this;
    }

    /**
     * Remove deliveryCost
     *
     * @param DeliveryCost $deliveryCost
     * @return Restaurant
     */
    public function removeDeliveryCost(DeliveryCost $deliveryCost)
    {
        $this->deliveryCosts->removeElement($deliveryCost);
        $deliveryCost->setRestaurant(null);
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentSupplier(): ?string
    {
        return $this->currentSupplier;
    }

    /**
     * @param string $currentSupplier
     */
    public function setCurrentSupplier(string $currentSupplier): void
    {
        $this->currentSupplier = $currentSupplier;
    }

    /**
     * @return float
     */
    public function getOrderAmountPerMonth(): ?float
    {
        return $this->orderAmountPerMonth;
    }

    /**
     * @param float $orderAmountPerMonth
     */
    public function setOrderAmountPerMonth(float $orderAmountPerMonth): void
    {
        $this->orderAmountPerMonth = $orderAmountPerMonth;
    }

    /**
     * @return string
     */
    public function getSiren(): ?string
    {
        return $this->siren;
    }

    /**
     * @param string $siren
     */
    public function setSiren(string $siren): void
    {
        $this->siren = $siren;
    }

    /**
     * @return string
     */
    public function getInterlocutor(): ?string
    {
        return $this->interlocutor;
    }

    /**
     * @param string $interlocutor
     */
    public function setInterlocutor(string $interlocutor): void
    {
        $this->interlocutor = $interlocutor;
    }

    /**
     * @return string
     */
    public function getDeliveryHours(): ?string
    {
        return $this->deliveryHours;
    }

    /**
     * @param string $deliveryHours
     */
    public function setDeliveryHours(string $deliveryHours): void
    {
        $this->deliveryHours = $deliveryHours;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->name
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        [
            $this->id,
            $this->email,
            $this->name
        ] = $data;
    }

    /**
     * @return string
     */
    public function getFormattedAddress()
    {
        return "{$this->getName()}\n{$this->getStreet()}\n{$this->getPostcode()} {$this->getCity()}";
    }
}
