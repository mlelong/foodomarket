<?php


namespace AppBundle\Entity;


use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

class Message implements ResourceInterface
{
    use TimestampableTrait;

    const MESSAGE_STATUS_WAITING        = 'waiting';
    const MESSAGE_STATUS_SENT           = 'sent';
    const MESSAGE_STATUS_OPENED         = 'opened';
    const MESSAGE_STATUS_ERROR          = 'error';

    const CHANNEL_STATUS_WAITING        = 'new';
    const CHANNEL_STATUS_DELIVERED      = 'delivered';
    const CHANNEL_STATUS_FIRST_OPENED   = 'unique_opened';
    const CHANNEL_STATUS_INVALID_EMAIL  = 'unique_opened';
    const CHANNEL_STATUS_ERROR          = 'error';
    const CHANNEL_STATUS_HARDBOUNCE     = 'hard_bounce';

    const CHANNEL_EMAIL                 = 'email';
    const CHANNEL_SMS                   = 'sms';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var User
     */
    private $recipient;

    /**
     * @var string
     */
    private $recipientType;

    /**
     * @var User
     */
    private $sender;

    /**
     * @var string
     */
    private $senderType;

    /**
     * @var Order|null
     */
    private $order;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $channelStatus;

    /**
     * @var string|null
     */
    private $channelMessageId;

    /**
     * @var bool
     */
    private $important = false;

    /**
     * @var Email|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $token;

    /**
     * @var boolean
     */
    private $read = false;

    /**
     * @var string|null
     */
    private $picture;

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return User
     */
    public function getRecipient(): User
    {
        return $this->recipient;
    }

    /**
     * @param User $recipient
     */
    public function setRecipient(User $recipient): void
    {
        $this->recipient = $recipient;
    }

    /**
     * @return User
     */
    public function getSender(): User
    {
        return $this->sender;
    }

    /**
     * @param User $sender
     */
    public function setSender(User $sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return Order|null
     */
    public function getOrder(): ?Order
    {
        return $this->order;
    }

    /**
     * @param Order|null $order
     */
    public function setOrder(?Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getChannel(): string
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     */
    public function setChannel(string $channel): void
    {
        $this->channel = $channel;
    }

    /**
     * @return string
     */
    public function getChannelStatus(): string
    {
        return $this->channelStatus;
    }

    /**
     * @param string $channelStatus
     */
    public function setChannelStatus(string $channelStatus): void
    {
        $this->channelStatus = $channelStatus;
    }

    /**
     * @return string|null
     */
    public function getChannelMessageId(): ?string
    {
        return $this->channelMessageId;
    }

    /**
     * @param string|null $channelMessageId
     */
    public function setChannelMessageId(?string $channelMessageId): void
    {
        $this->channelMessageId = $channelMessageId;
    }

    /**
     * @return bool
     */
    public function isImportant(): bool
    {
        return $this->important;
    }

    /**
     * @param bool $important
     */
    public function setImportant(bool $important): void
    {
        $this->important = $important;
    }

    /**
     * @return string
     */
    public function getRecipientType(): string
    {
        return $this->recipientType;
    }

    /**
     * @param string $recipientType
     */
    public function setRecipientType(string $recipientType): void
    {
        $this->recipientType = $recipientType;
    }

    /**
     * @return string
     */
    public function getSenderType(): string
    {
        return $this->senderType;
    }

    /**
     * @param string $senderType
     */
    public function setSenderType(string $senderType): void
    {
        $this->senderType = $senderType;
    }

    /**
     * @return Email|null
     */
    public function getEmail(): ?Email
    {
        return $this->email;
    }

    /**
     * @param Email|null $email
     */
    public function setEmail(?Email $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     */
    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->read;
    }

    /**
     * @param bool $read
     */
    public function setRead(bool $read): void
    {
        $this->read = $read;
    }

    /**
     * @return string|null
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string|null $picture
     */
    public function setPicture(?string $picture)
    {
        $this->picture = $picture;
    }
}
