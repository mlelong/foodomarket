<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\ProductVariantInterface;

interface ShoppingItemInterface
{
    /**
     * @return ProductVariantInterface
     */
    public function getProductVariant();

    /**
     * @return Supplier|null
     */
    public function getSupplier();

    /**
     * @return string {"KG", "PC"}
     */
    public function getUnit();

    /**
     * @return float
     */
    public function getQuantity();
}