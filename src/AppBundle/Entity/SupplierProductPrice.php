<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Product\Model\ProductInterface;
use Sylius\Component\Product\Model\ProductVariantInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

class SupplierProductPrice implements ResourceInterface
{
    
    use TimestampableTrait;
    
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $channelCode;

    /**
     * @var Product
     */
    protected $product;    
    
    /**
     * @var ProductVariant
     */
    protected $productVariant;

    /**
     * @var Supplier
     */
    protected $supplier;

    /**
     * @var Restaurant
     */
    protected $restaurant;
    
    /**
     * @var int
     */
    protected $kgPrice;

    /**
     * @var int
     */
    protected $unitPrice;

    /**
     * @var int
     */
    protected $mercurialeId;

    /**
     * @var \DateTime|null
     */
    protected $validFrom;

    /**
     * @var \DateTime|null
     */
    protected $validTo;

    /** @var boolean */
    protected $promo = false;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->mercurialeId = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string) $this->getPrice();
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getChannelCode()
    {
        return $this->channelCode;
    }

    /**
     * {@inheritdoc}
     */
    public function setChannelCode($channelCode)
    {
        $this->channelCode = $channelCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * {@inheritdoc}
     */
    public function setProduct(ProductInterface $product = null)
    {
        $this->product = $product;
    }

    /**
     * @return ProductVariant
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    /**
     * {@inheritdoc}
     */
    public function setProductVariant(ProductVariantInterface $productVariant = null)
    {
        $this->productVariant = $productVariant;
    }

    /**
     * {@inheritdoc}
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * {@inheritdoc}
     */
    public function setSupplier(Supplier $supplier = null)
    {
        $this->supplier = $supplier;
        //$this->setChannelCode($supplier->getChannel()->getCode());
    }   
    
    /**
     * {@inheritdoc}
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * {@inheritdoc}
     */
    public function setRestaurant(Restaurant $restaurant = null)
    {
        $this->restaurant = $restaurant;
    }    
    
    /**
     * {@inheritdoc}
     */
    public function getKgPrice()
    {
        return $this->kgPrice;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setKgPrice($price)
    {
        $this->kgPrice = $price;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setUnitPrice($price)
    {
        $this->unitPrice = $price;
    }

    /**
     * @return int
     */
    public function getMercurialeId(): int
    {
        return $this->mercurialeId;
    }

    /**
     * @param int $mercurialeId
     */
    public function setMercurialeId(int $mercurialeId)
    {
        $this->mercurialeId = $mercurialeId;
        return $this;
    }

    /**
     * @return float
     */
    public function getUnitContentPrice()
    {
        $intPrice = $this->getUnitPrice();

        /** @var ProductVariant $variant */
        $variant = $this->getProductVariant();
        $unitQuantity = $variant->getOptionUnitQuantity();

        if ($unitQuantity === false) {
            $unitQuantity = 1;
        } else {
            $unitQuantity = $unitQuantity->getValue();
        }

        $intPrice /= $unitQuantity;

        return $intPrice;
    }

    /**
     * Returns either the kg price or the unit price, depending on the UC option value
     * @return int
     */
    public function getDefaultPrice()
    {
        /** @var ProductVariant $variant */
        $variant = $this->getProductVariant();

        if ($variant === null)
            return 0;

        switch ($variant->getOptionOrderUnit()) {
            case 'PC':
                return $this->getUnitPrice();
            default:
                return $this->getKgPrice();
        }
    }

    /**
     * @return \DateTime|null
     */
    public function getValidFrom(): ?\DateTime
    {
        return $this->validFrom;
    }

    /**
     * @param \DateTime|null $validFrom
     */
    public function setValidFrom(?\DateTime $validFrom): void
    {
        $this->validFrom = $validFrom;
    }

    /**
     * @return \DateTime|null
     */
    public function getValidTo(): ?\DateTime
    {
        return $this->validTo;
    }

    /**
     * @param \DateTime|null $validTo
     */
    public function setValidTo(?\DateTime $validTo): void
    {
        $this->validTo = $validTo;
    }

    /**
     * @return bool
     */
    public function isPromo(): bool
    {
        return $this->promo;
    }

    /**
     * @param bool $promo
     */
    public function setPromo(bool $promo): void
    {
        $this->promo = $promo;
    }
}