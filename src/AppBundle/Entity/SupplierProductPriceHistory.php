<?php

namespace AppBundle\Entity;

use Sylius\Component\Core\Model\Product;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Product\Model\ProductInterface;
use Sylius\Component\Product\Model\ProductVariantInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

class SupplierProductPriceHistory implements ResourceInterface
{

    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $channelCode;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductVariantInterface
     */
    protected $productVariant;

    /**
     * @var Supplier
     */
    protected $supplier;

    /**
     * @var Restaurant
     */
    protected $restaurant;

    /**
     * @var int
     */
    protected $kgPrice;

    /**
     * @var int
     */
    protected $unitPrice; 

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return (string) $this->getPrice();
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getChannelCode()
    {
        return $this->channelCode;
    }

    /**
     * {@inheritdoc}
     */
    public function setChannelCode($channelCode)
    {
        $this->channelCode = $channelCode;
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * {@inheritdoc}
     */
    public function setProduct(ProductInterface $product = null)
    {
        $this->product = $product;
    }

    /**
     * {@inheritdoc}
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    /**
     * {@inheritdoc}
     */
    public function setProductVariant(ProductVariantInterface $productVariant = null)
    {
        $this->productVariant = $productVariant;
    }

    /**
     * {@inheritdoc}
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * {@inheritdoc}
     */
    public function setSupplier(Supplier $supplier = null)
    {
        $this->supplier = $supplier;
        //$this->setChannelCode($supplier->getChannel()->getCode());
    }

    /**
     * {@inheritdoc}
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * {@inheritdoc}
     */
    public function setRestaurant(Restaurant $restaurant = null)
    {
        $this->restaurant = $restaurant;
    }

    /**
     * {@inheritdoc}
     */
    public function getKgPrice()
    {
        return $this->kgPrice;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setKgPrice($price)
    {
        $this->kgPrice = $price;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setUnitPrice($price)
    {
        $this->unitPrice = $price;
    }
}
