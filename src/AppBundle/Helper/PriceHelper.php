<?php

namespace AppBundle\Helper;

class PriceHelper
{
    /**
     * @param $unit
     * @param $price
     * @param $weight
     * @param $unitQuantity
     * @return array
     * @throws \Exception
     */
    public function getKgPcPrice($unit, $price, $weight, $unitQuantity)
    {
        switch ($unit) {
            case 'PC':
            case 'ORDER_PC':
                $kgPrice = $price / $weight;
                $unitPrice = $price;
                break ;
            case 'CO':
            case 'COLIS':
            case 'CO_COLIS':
                $kgPrice = $price / $weight;
                $unitPrice = $price;
                break ;
            case 'KG':
            case 'L':
            case 'ORDER_KG':
            case 'ORDER_L':
                $kgPrice = $price;
                $unitPrice = $price;
                break ;
            default:
                throw new \Exception("Invalid {$unit} option specified");
        }

        return [
            'kg' => $kgPrice,
            'unit' => $unitPrice
        ];
    }
}