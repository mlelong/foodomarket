<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Security\AccessDeniedHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * This is a temporary workaround until https://github.com/symfony/symfony/issues/28229 is solved.
 *
 * @package App\EventSubscriber
 */
class AccessDeniedSubscriber implements EventSubscriberInterface
{
    private $handler;

    public function __construct(AccessDeniedHandler $handler)
    {
        $this->handler = $handler;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        if ($exception instanceof AccessDeniedException && self::isThrownByFirewall($event->getRequest())) {
            $response = $this->handler->handle($event->getRequest(), $exception);

            $event->setResponse($response);
            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => ['onKernelException', 1],
        ];
    }

    /**
     * Determines, by analyzing the request, if an exception has been thrown by the firewall.
     *
     * @param Request $request
     * @return bool
     */
    private static function isThrownByFirewall(Request $request): bool
    {
        $firewallContext = $request->attributes->get('_firewall_context');

        if (in_array($firewallContext, ['security.firewall.map.context.refresh_api', 'security.firewall.map.context.shop'])) {
            return true;
        }

        return false;
    }
}
