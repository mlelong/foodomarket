<?php


namespace AppBundle\Security;


use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $isAjax = $request->isXmlHttpRequest();
        $route = $request->attributes->get('_route');
        $isApi = preg_match('/^api_/', $route) > 0;

        if ($isAjax || ($route === 'api_graphql_entrypoint' && $request->getMethod() !== 'GET')) {
            $response = new JsonResponse(['ok' => false, 'message' => $accessDeniedException->getMessage()], 403);
        } else {
            $response = new RedirectResponse(
                $this->router->generate($isApi ? 'api_login' : 'app_front_connection_page', [], Router::ABSOLUTE_URL)
            );
        }

        $response->headers->clearCookie('_token');

        return $response;
    }
}
