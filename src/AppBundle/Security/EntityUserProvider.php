<?php

namespace AppBundle\Security;

use Symfony\Bridge\Doctrine\Security\User\EntityUserProvider as BaseEntityUserProvider;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class EntityUserProvider extends BaseEntityUserProvider
{
    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        try {
            return parent::refreshUser($user);
        } catch (\InvalidArgumentException $e) {
            return $user;
        } catch (UsernameNotFoundException $e) {
            return $user;
        }
    }
}