<?php

declare(strict_types=1);

namespace AppBundle\Security\EventListener;

use Symfony\Component\EventDispatcher\Event;

final class UserCartRecalculationListener
{
    /**
     * @param Event $event
     */
    public function recalculateCartWhileLogin(Event $event): void
    {
    }
}
