<?php

namespace AppBundle\Security\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\Event;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class JWTAuthenticationFailureListener
{
    /** @var RouterInterface */
    private $router;

    /** @var RequestStack */
    private $requestStack;

    public function __construct(RouterInterface $router, RequestStack $requestStack)
    {
        $this->router = $router;
        $this->requestStack = $requestStack;
    }

    /**
     * @param Event $event
     */
    public function onAuthenticationFailureResponse($event)
    {
        $exception = $event->getException();
        $message = $exception->getMessage();
        $request = $this->requestStack->getCurrentRequest();
        $isAjax = $request->isXmlHttpRequest();
        $route = $request->attributes->get('_route');
        $isApi = preg_match('/^api_/', $route) > 0;

        // Json response
        if ($isAjax || ($route === 'api_graphql_entrypoint' && $request->getMethod() !== 'GET')) {
            if ($event instanceof JWTExpiredEvent) {
                $response = new JsonResponse(['ok' => false, 'message' => 'TOKEN_EXPIRED'], 401);
            } elseif ($event instanceof JWTNotFoundEvent) {
                $response = new JsonResponse(['ok' => false, 'message' => 'NOT_FOUND'], 403);
            } elseif ($event instanceof JWTInvalidEvent) {
                $response = new JsonResponse(['ok' => false, 'message' => 'INVALID'], 403);
            } elseif ($event instanceof AuthenticationFailureEvent) {
                $response = new JsonResponse(['ok' => false, 'message' => 'BAD_CREDENTIALS', 401]);
            } else {
                $response = new JsonResponse(['ok' => false, 'message' => $message, 403]);
            }
        } else {
            $response = new RedirectResponse(
                $this->router->generate($isApi ? 'api_login' : 'app_front_connection_page', [], Router::ABSOLUTE_URL)
            );
        }

        $response->headers->removeCookie('_token');
        $response->headers->removeCookie('_refresh');

        $event->setResponse($response);


//        if ($event instanceof JWTExpiredEvent) {
//            $response = new JsonResponse(['ok' => false, 'message' => 'TOKEN_EXPIRED'], 401);
//
//            $response->headers->removeCookie('_token');
//            $event->setResponse($response);
//        } else {
//            switch ($message) {
//                case 'User account is disabled.':
//                    $event->setResponse(new JsonResponse(['ok' => false, 'message' => 'USER_ACCOUNT_IS_DISABLED']));
//                    break ;
//                case 'Bad credentials.':
//                    $event->setResponse(new JsonResponse(['ok' => false, 'message' => 'BAD_CREDENTIALS']));
//                    break ;
//                default:
//                    $event->setResponse(new JsonResponse(['ok' => false, 'message' => $message]));
//            }
//        }
    }
}