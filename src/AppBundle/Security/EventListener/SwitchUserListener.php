<?php

namespace AppBundle\Security\EventListener;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;

class SwitchUserListener
{
    /** @var SessionInterface */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onSecuritySwitchUser(SwitchUserEvent $event)
    {
        $newUser = $event->getTargetUser();

        $this->session->set('_foodo_switch', $newUser->getUsername());
    }
}
