<?php


namespace AppBundle\Security\EventListener;


use AppBundle\Entity\AdminUser;
use AppBundle\Entity\ShopUser;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Component\HttpFoundation\Cookie;

class JWTAuthenticatedListener
{
    /**
     * @var JWTEncoderInterface
     */
    private $jwtEncoder;

    public function __construct(JWTEncoderInterface $jwtEncoder)
    {
        $this->jwtEncoder = $jwtEncoder;
    }

    public function attachProspectType(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        if ($user instanceof ShopUser) {
            $type = $user->getCustomer()->getProspect()->getType();

            $data['type'] = $type;
        } elseif ($user instanceof AdminUser) {
            $data['type'] = 'admin';
        }

        try {
            $decodedToken = $this->jwtEncoder->decode($data['token']);
        } catch (JWTDecodeFailureException $e) {
            $decodedToken = ['exp' => 0];
        }

        $cookieToken = new Cookie('_token', $data['token'], $decodedToken['exp']);
//        $cookieToken = new Cookie('_token', $data['token'], (new \DateTime())->modify('+20 second'));
        $cookieRefreshToken = new Cookie('_refresh', $data['refresh_token']);

        $event->getResponse()->headers->setCookie($cookieToken);
        $event->getResponse()->headers->setCookie($cookieRefreshToken);

        $event->setData($data);
    }
}