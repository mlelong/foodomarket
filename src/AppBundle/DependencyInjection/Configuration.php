<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('app');
        $rootNode
            ->children()
                ->arrayNode('ressources')
                    ->children()
                        ->scalarNode('channel_pricing_entity')->end()
                    ->end()
                ->end()
            ->end()
            ->children()
                ->arrayNode('mailjet_api')
                    ->children()
                        ->scalarNode('api_key')->defaultValue('')->end()
                        ->scalarNode('api_secret')->defaultValue('')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}