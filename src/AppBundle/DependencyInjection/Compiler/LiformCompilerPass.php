<?php

namespace AppBundle\DependencyInjection\Compiler;

use AppBundle\Form\Liform\Serializer\Normalizer\InitialValuesNormalizer;
use AppBundle\LiformTransformer\ArrayTransformer;
use AppBundle\LiformTransformer\StringTransformer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class LiformCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        // Override the default string transformer with our custom one
        $stringTransformerDefinition = $container->getDefinition('liform.transformer.string');
        $stringTransformerDefinition->setClass(StringTransformer::class);

        // Add the hidden type to the list of handled transformation type
        $stringTransformerDefinition->addTag('liform.transformer', ['form_type' => 'hidden', 'widget' => 'hidden']);

        // Override the default array transformer with our custom one
        $arrayTransformerDefinition = $container->getDefinition('liform.transformer.array');
        $arrayTransformerDefinition->setClass(ArrayTransformer::class);

        // Override the default initial values normalizer with our custom one
        $initialValuesNormalizerDefinition = $container->getDefinition('liform.serializer.initial_values_normalizer');
        $initialValuesNormalizerDefinition->setClass(InitialValuesNormalizer::class);
    }
}