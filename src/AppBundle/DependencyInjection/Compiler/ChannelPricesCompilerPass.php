<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

use AppBundle\Services\ProductVariantPriceCalculator;
use AppBundle\Form\HasAllVariantPricesDefinedValidator;

class ChannelPricesCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('sylius.calculator.product_variant_price');
        $definition->setClass(ProductVariantPriceCalculator::class);
        $definition->addArgument(new Reference('app.repository.supplier_product_price'));
        $definition = $container->getDefinition('sylius.validator.has_all_prices_defined');
        $definition->setClass(HasAllVariantPricesDefinedValidator::class);
    }
}
