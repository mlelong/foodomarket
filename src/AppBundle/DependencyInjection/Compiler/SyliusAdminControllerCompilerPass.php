<?php

namespace AppBundle\DependencyInjection\Compiler;

use AppBundle\Admin\Controller\DashboardController;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SyliusAdminControllerCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $container->getDefinition('sylius.controller.admin.dashboard')->setClass(DashboardController::class);
    }
}
