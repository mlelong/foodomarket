<?php


namespace AppBundle\DependencyInjection\Compiler;


use AppBundle\Services\ReactRenderer\ExternalServerReactRenderer;
use AppBundle\Services\ReactRenderer\ReactRenderExtension;
use AppBundle\Services\ReactRenderer\SymfonyContextProvider;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ReactRendererCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // Override the default external server react renderer with our custom one
        $externalServerReactRenderDefinition = $container->getDefinition('limenius_react.external_react_renderer');
        $externalServerReactRenderDefinition->setClass(ExternalServerReactRenderer::class);

        $reactRenderExtensionDefinition = $container->getDefinition('limenius_react.render_extension');
        $reactRenderExtensionDefinition->setClass(ReactRenderExtension::class);

        $definition = $container->getDefinition('limenius_react.context_provider');
        $definition->setClass(SymfonyContextProvider::class);
        $definition->addArgument(new Reference('router'));
        $definition->addArgument($container->getParameter('graphql_uri'));
    }
}
