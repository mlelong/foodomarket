<?php

namespace AppBundle\LiformTransformer;

use Limenius\Liform\Transformer\AbstractTransformer;
use Limenius\Liform\Transformer\ExtensionInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class StringTransformer extends AbstractTransformer
{
    /**
     * @param FormInterface $form
     * @param ExtensionInterface[] $extensions
     * @param string|null $widget
     *
     * @return array
     */
    public function transform(FormInterface $form, array $extensions = [], $widget = null)
    {
        $schema = ['type' => 'string'];
        $schema = $this->addCommonSpecs($form, $schema, $extensions, $widget);

        $constraints = $form->getConfig()->getOption('constraints');

        if (!empty($constraints)) {
            /** @var Constraint $constraint */
            foreach ($constraints as $constraint) {
                switch (get_class($constraint)) {
                    case NotBlank::class:
                    case NotNull::class:
                        if (!isset($schema['minLength'])) {
                            $schema['minLength'] = 1;
                        }
                        break ;
                    case Length::class:
                        $schema['minLength'] = $constraint->min;
                        $schema['maxLength'] = $constraint->max;
                        break ;
                }
            }
        }

        return $schema;
    }
}