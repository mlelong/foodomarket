<?php

namespace AppBundle\LiformTransformer;

use Limenius\Liform\Transformer\AbstractTransformer;
use Limenius\Liform\Transformer\ExtensionInterface;
use Symfony\Component\Form\FormInterface;

class FileTransformer extends AbstractTransformer
{
    /**
     * @param FormInterface $form
     * @param ExtensionInterface[] $extensions
     * @param string|null $widget
     *
     * @return array
     */
    public function transform(FormInterface $form, array $extensions = [], $widget = null)
    {
        $schema = ['type' => 'object'];
        $schema = $this->addCommonSpecs($form, $schema, $extensions, $widget);

        return $schema;
    }
}