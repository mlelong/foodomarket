<?php

namespace AppBundle\Command;

use AppBundle\Entity\AdminUser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateAdminUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:adminuser:create')
            ->addArgument('email', InputArgument::REQUIRED, 'Admin user email')
            ->addArgument('password', InputArgument::REQUIRED, 'Admin user password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $adminUserManager = $this->getContainer()->get('sylius.manager.admin_user');
        $adminUserFactory = $this->getContainer()->get('sylius.factory.admin_user');
        /** @var AdminUser $adminUser */
        $adminUser = $adminUserFactory->createNew();

        $adminUser->setEmail($input->getArgument('email'));
        $adminUser->setPlainPassword($input->getArgument('password'));
        $adminUser->setLocaleCode('fr_FR');
        $adminUser->setEnabled(true);

        $adminUserManager->persist($adminUser);
        $adminUserManager->flush();

        $output->writeln("Creating admin user `{$adminUser->getEmail()}`: done");
    }
}