<?php


namespace AppBundle\Command;


use AppBundle\Entity\Supplier;
use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class InitSupplierProductsCommand extends Command
{
    public static $defaultName = 'app:init:supplier-products';

    /** @var ManagerRegistry */
    private $doctrine;

    /** @var Connection */
    private $connection;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('idSupplierSrc', InputArgument::REQUIRED)
            ->addArgument('idSupplierDst', InputArgument::REQUIRED)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $idSupplierSrc = $input->getArgument('idSupplierSrc');
        $idSupplierDst = $input->getArgument('idSupplierDst');

        $supplierRepository = $this->doctrine->getRepository(Supplier::class);

        /** @var Supplier $supplierSrc */
        $supplierSrc = $supplierRepository->find($idSupplierSrc);
        /** @var Supplier $supplierDst */
        $supplierDst = $supplierRepository->find($idSupplierDst);

        if ($supplierSrc === null || $supplierDst === null) {
            $output->writeln("Cannot find either source supplier or dst supplier");

            return -1;
        }

        /** @var Connection $connection */
        $this->connection = $this->doctrine->getConnection();

        $dstChannelCode = $supplierDst->getChannel()->getCode();

        try {
            $output->writeln("Copying product matching and price from supplier `{$supplierSrc->getDisplayName(true)} ($idSupplierSrc)` to supplier `{$supplierDst->getDisplayName(true)} ($idSupplierDst)`...");

            $this->connection->transactional(function (Connection $connection) use($idSupplierSrc, $idSupplierDst, $dstChannelCode) {
                // On nettoie d'abord
                $this->connection->delete("DELETE FROM sylius_supplier_product_informations WHERE supplier_id = :supplierId", ['supplierId' => $idSupplierDst]);
                $this->connection->delete("DELETE FROM sylius_supplier_product_variant_informations WHERE supplier_id = :supplierId", ['supplierId' => $idSupplierDst]);
                $this->connection->delete("DELETE FROM sylius_supplier_product_price WHERE supplier_id = :supplierId", ['supplierId' => $idSupplierDst]);

                $spis = $this->connection->fetchAll("SELECT product_id, label, reference FROM sylius_supplier_product_informations spi WHERE supplier_id = $idSupplierSrc");
                $spvis = $this->connection->fetchAll("SELECT product_id, product_variant_id, product_label, variant_label, reference, weight, content, sales_unit, unit_quantity, display_name, order_ref, manually_created FROM sylius_supplier_product_variant_informations WHERE supplier_id = $idSupplierSrc");
                $spps = $this->connection->fetchAll("SELECT restaurant_id, product_id, product_variant_id, kg_price, unit_price, created_at, updated_at, valid_from, valid_to, promo FROM sylius_supplier_product_price WHERE supplier_id = $idSupplierSrc");

                // Recopie des infos de matching produit
                foreach ($spis as $spi) {
                    $sql = "INSERT INTO sylius_supplier_product_informations SET supplier_id = $idSupplierDst";

                    foreach ($spi as $field => $value) {
                        if ($value !== null) {
                            $sql .= ", $field={$this->escapeValue($value)}";
                        } else {
                            $sql .= ", $field=NULL";
                        }
                    }

                    $connection->executeQuery($sql);
                }

                // Recopie des infos de matching variante
                foreach ($spvis as $spvi) {
                    $sql = "INSERT INTO sylius_supplier_product_variant_informations SET supplier_id = $idSupplierDst";

                    foreach ($spvi as $field => $value) {
                        if ($field === 'product_variant_id') {
                            // On doit dupliquer la variante si on ne l'a pas déjà fait
                            if (!isset($variantCache[$value])) {
                                $variantCache[$value] = $this->duplicateVariant($value);
                            }

                            $value = $variantCache[$value];
                        }

                        if ($value !== null) {
                            $sql .= ", $field={$this->escapeValue($value)}";
                        } else {
                            $sql .= ", $field=NULL";
                        }
                    }

                    $connection->executeQuery($sql);
                }

                // Recopie des prix
                foreach ($spps as $spp) {
                    $sql = "INSERT INTO sylius_supplier_product_price SET supplier_id = $idSupplierDst, channel_code = '$dstChannelCode', mercuriale_id = '{$idSupplierDst}'";

                    foreach ($spp as $field => $value) {
                        if ($field === 'product_variant_id') {
                            // On doit dupliquer la variante si on ne l'a pas déjà fait
                            if (!isset($variantCache[$value])) {
                                $variantCache[$value] = $this->duplicateVariant($value);
                            }

                            $value = $variantCache[$value];
                        }

                        if ($value !== null) {
                            $sql .= ", $field={$this->escapeValue($value)}";
                        } else {
                            $sql .= ", $field=NULL";
                        }
                    }

                    $connection->executeQuery($sql);
                }
            });

            $output->writeln("Done");
        } catch (Throwable $e) {
            $output->writeln($e->getMessage());

            return -1;
        }

        return 0;
    }

    private function escapeValue($value) {
        //remove any ESCAPED double quotes within string.
        $value = str_replace('\\"','"',$value);
        //then force escape these same double quotes And Any UNESCAPED Ones.
        $value = str_replace('"','\"',$value);
        //force wrap value in quotes and return
        return '"'.$value.'"';
    }

    /**
     * @param $variantId
     * @return string
     * @throws DBALException
     */
    private function duplicateVariant($variantId)
    {
        $variant = $this->connection->fetchAssoc("SELECT product_id, tax_category_id, shipping_category_id, code, position, version, on_hold, on_hand, tracked, width, height, depth, weight, shipping_required, enabled FROM sylius_product_variant WHERE id = $variantId");
        $translations = $this->connection->fetchAll("SELECT name, locale FROM sylius_product_variant_translation WHERE translatable_id = $variantId");
        $optionValues = $this->connection->fetchAll("SELECT option_value_id FROM sylius_product_variant_option_value WHERE variant_id = $variantId");

        $sql = "INSERT INTO sylius_product_variant SET product_id = {$variant['product_id']}";

        unset($variant['product_id']);
        $variant['code'] = uniqid();
        $variant['created_at'] = $variant['updated_at'] = (new DateTime())->format('Y-m-d H:i:s');

        foreach ($variant as $field => $value) {
            if ($value !== null) {
                $sql .= ", $field=\"$value\"";
            } else {
                $sql .= ", $field=NULL";
            }
        }

        // Création d'une nouvelle variante
        if ($this->connection->exec($sql)) {
            $newVariantId = $this->connection->lastInsertId($sql);

            // Création des traductions
            foreach ($translations as $translation) {
                $sql = "INSERT INTO sylius_product_variant_translation SET translatable_id = $newVariantId";

                foreach ($translation as $field => $value) {
                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                if (!$this->connection->exec($sql)) {
                    throw new DBALException("Impossible de créer une traduction pour cette variante");
                }
            }

            foreach ($optionValues as $optionValue) {
                $sql = "INSERT INTO sylius_product_variant_option_value SET variant_id = $newVariantId";

                foreach ($optionValue as $field => $value) {
                    if ($value !== null) {
                        $sql .= ", $field=\"$value\"";
                    } else {
                        $sql .= ", $field=NULL";
                    }
                }

                if (!$this->connection->exec($sql)) {
                    throw new DBALException("Impossible de créer les options de variante");
                }
            }

            return $newVariantId;
        } else {
            throw new DBALException("Impossible de créer une nouvelle variante");
        }
    }
}
