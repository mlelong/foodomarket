<?php

namespace AppBundle\Command;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductTaxon;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RepairProductTaxonsCommand extends ContainerAwareCommand
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('app:repair:product-taxons')
            ->addArgument('product', InputArgument::OPTIONAL)
            ->addOption('displaydebug', 'debug', InputOption::VALUE_NONE)
            ->setHelp("Recompute the sylius_product_taxon table based on the product main taxon entries")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initializeDebug($output, $input->getOption('displaydebug'));

        $productId = $input->hasArgument('product') ? $input->getArgument('product') : null;

        if ($productId !== null) {
            /** @var Product|null $product */
            $product = $this->getContainer()->get('sylius.repository.product')->find($productId);

            if ($product === null) {
                $this->critical("Could not find product with id `{$input->getArgument('product')}`");

                return -1;
            }

            /** @var Product[] $products */
            $products = [$product];
        } else {
            /** @var Product[] $products */
            $products = $this->getContainer()->get('sylius.repository.product')->findAll();
        }

        $nbProducts = count($products);

        $this->info("Repairing taxons of $nbProducts products\n");

        $this->start($nbProducts);

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        if ($productId === null) {
            try {
                $cmd = $em->getClassMetadata(ProductTaxon::class);
                $connection = $em->getConnection();
                $dbPlatform = $connection->getDatabasePlatform();

                $connection->query('SET FOREIGN_KEY_CHECKS=0');

                $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());

                $connection->executeUpdate($q);
                $connection->query('SET FOREIGN_KEY_CHECKS=1');
            } catch (DBALException $e) {
                $this->critical($e->getMessage());

                return -1;
            }
        } else {
            $productTaxonRepository = $this->getContainer()->get('sylius.repository.product_taxon');

            $productTaxons = $productTaxonRepository->findBy(['product' => $productId]);

            foreach ($productTaxons as $productTaxon) {
                $productTaxonRepository->remove($productTaxon);
            }
        }

        try {
            $em->transactional(function (EntityManager $manager) use ($products) {
                foreach ($products as $product) {
                    $this->advance("Repairing taxons for product `{$product->getId()} - {$product->getName()}`");

                    $this->repairProductTaxons($product, $manager);
                }

                return true;
            });
        } catch (\Throwable $e) {
            $this->critical("Failed to repair product taxons: {$e->getMessage()}");
        }

        $this->finish();

        return 0;
    }

    /**
     * @param Product $product
     * @param EntityManager $manager
     * @throws \Doctrine\ORM\ORMException
     */
    private function repairProductTaxons(Product $product, EntityManager $manager)
    {
        $productTaxonFactory = $this->getContainer()->get('sylius.factory.product_taxon');

        $taxon = $product->getMainTaxon();

        while ($taxon !== null) {
            /** @var ProductTaxon $productTaxon */
            $productTaxon = $productTaxonFactory->createNew();

            $productTaxon->setProduct($product);
            $productTaxon->setTaxon($taxon);
            $productTaxon->setPosition(0);

            $manager->persist($productTaxon);

            $taxon = $taxon->getParent();
        }
    }
}