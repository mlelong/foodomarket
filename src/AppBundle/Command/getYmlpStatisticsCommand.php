<?php

namespace AppBundle\Command;

use AppBundle\Entity\EmailingStatistics;
use AppBundle\Repository\ProspectRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

class getYmlpStatisticsCommand extends ContainerAwareCommand
{
    use CommandTrait;

    private $count = 20000;
    /**
     * @var EntityManager|object
     */
    private $manager;
    /**
     * @var \AppBundle\Repository\EmailingStatisticsRepository|object
     */
    private $emailingStatisticsRepository;
    /**
     * @var bool|string|string[]|null
     */
    private $save;
    /**
     * @var string
     */
    private $ymlpKey;
    /**
     * @var string
     */
    private $ymlpUsername;
    /**
     * @var \YMLP_API
     */
    private $ymlp;

    protected function configure()
    {
        $this
            ->setName('app:email:get-statistics-from-ymlp')
            ->addOption('action', null, InputOption::VALUE_REQUIRED, 'What to do : list stats')
            ->addOption('newsletter-id', null, InputOption::VALUE_OPTIONAL, 'The newsletter to get statistcs from')
            ->addOption('save', null, InputOption::VALUE_NONE, 'Save into BDD')
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp(
            '
            <info>This command\'s purpose is to list newsletters archives and fetch statistics of opening.</info>
            
            Example : php bin/console app:email:get-statistics-from-ymlp --action=list --displaydebug
            Example : php bin/console app:email:get-statistics-from-ymlp --action=stats --newsletter-id=[ID] --displaydebug --save
            '
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->initializeDebug($output, $input->getOption('displaydebug'));

        /** @var EntityManager $manager */
        $this->manager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        /** @var ProspectRepository $prospectRepository */
        $this->emailingStatisticsRepository = $this->getContainer()->get('app.repository.emailing_statistics');

        $action = $input->getOption('action');
        $newsletterId = intval($input->getOption('newsletter-id'));
        $this->save = $input->getOption('save');

        // variables: your key & username
        $this->ymlpKey = "PU0SEA29XQ8BSAP3KCQR";
        $this->ymlpUsername = "p7p9";

        // create API class
        $this->ymlp = new \YMLP_API($this->ymlpKey, $this->ymlpUsername);

        if (is_null($action)) {
            $action = 'list';
        }

        switch($action) {
            case 'list':
                        $data = $this->getDataList();
                        break;
            case 'stats':
                        $data = $this->getDataStats($newsletterId);
                        $this->updateEmailingStatistics($data);
                        break;
        }
    }

    public function calculateNote(& $stats) {

        $openingRate = bcdiv(bcmul($stats['open'], 100), ($stats['open']+$stats['no-open']),2);
        $clickRate = bcdiv(bcmul($stats['click'], 100), ($stats['open']+$stats['no-open']),2);;

        $stats['opening-rate'] = $openingRate;
        $stats['click-rate'] = $clickRate;

        $note = 0;
        if ($openingRate < 10) {
            $note = 0;
        } elseif ($openingRate < 30) {
            $note = 1;
        } elseif ($openingRate < 50) {
            $note = 2;
        } elseif ($openingRate < 70) {
            $note = 3;
        } elseif ($openingRate <= 100) {
            $note = 4;
        }

        if ($clickRate > 0.50) {
            $note++;
        }

        if ($note > 5) {
            $note = 5;
        }

        $stats['note'] = $note;

        return $note;
    }

    public function updateEmailingStatistics(array $data) {

        $this->info("Ymlp : updating stats");

        $i = 0;
        $this->start(count($data));
        foreach($data as $email => $stats) {

            $i++;
            $this->advance();
            $this->info(str_pad($i, 3, '0', STR_PAD_LEFT)." Getting stats from $email");

            $emailingStatistics = $this->emailingStatisticsRepository->findOneByEmail($email);

            if (is_null($emailingStatistics)) {

                $stats['note'] = $this->calculateNote($stats);
                $this->info("- Opening Rate : " . $stats['opening-rate'] . "% | Click Rate : " . $stats['click-rate'] . "% | Note : " . $stats['note']);

                $emailing = new EmailingStatistics();
                $emailing->setEmail($email)
                         ->setOpeningCount($stats['open'])
                         ->setNoOpeningCount($stats['no-open'])
                         ->setClickCount($stats['click'])
                         ->setOpeningRate($stats['opening-rate'])
                         ->setClickRate($stats['click-rate'])
                         ->setNote($stats['note'])
                ;
                $this->manager->persist($emailing);

            } else {

                $stats['open'] += $emailingStatistics->getOpeningCount();
                $stats['no-open'] += $emailingStatistics->getNoOpeningCount();
                $stats['click'] += $emailingStatistics->getClickCount();

                $stats['note'] = $this->calculateNote($stats);

                $emailingStatistics->setOpeningCount($stats['open']);
                $emailingStatistics->setNoOpeningCount($stats['no-open']);
                $emailingStatistics->setClickCount($stats['click']);
                $emailingStatistics->setNote($stats['note']);
                $emailingStatistics->setOpeningRate($stats['opening-rate']);
                $emailingStatistics->setClickRate($stats['click-rate']);

                $this->info("- Opening Rate : " . $stats['opening-rate'] . "% | Click Rate : " . $stats['click-rate'] . "% | Note : " . $stats['note']);
            }

            if ($this->save) {
                $this->manager->flush();
                $this->manager->clear();
            }
        }
        $this->finish();
    }

    public function getDataList() {

        $this->info("Ymlp : getting newsletters sent ...");

        $Page = '';
        $NumberPerPage = '10';
        $StartDate = '';
        $StopDate = '';
        $Sorting = 'Descending';
        $ShowTestMessages = '0';

        $output = $this->ymlp->ArchiveGetList($Page , $NumberPerPage , $StartDate , $StopDate , $Sorting , $ShowTestMessages);

        if ($this->ymlp->ErrorMessage) {
            $this->info("There's a connection problem: " . $this->ymlp->ErrorMessage);
        } else {
            if ( isset($output["Code"]) ) {
                $this->info("{$output["Code"]} => {$output["Output"]}");
            } elseif (!count($output)) {
                $this->info("No data available");   
            } else {
                foreach ($output as $item) {
                    if ($item['Delivered'] > 20) {
                        $this->info("Id : " . $item['NewsletterID'] . " | Date : " . $item['Date'] . " | Delivered : " . $item['Delivered'] . " | Opening : " . $item['UniqueOpens'] . " | Clicked : " . $item['UniqueClicks'] . " | Open Rate : " . $item['OpenRate'] . "%");
                    }
                }
            }
        }

        return true;

    }

    public function getDataStats(int $newsletterId) {

        $data = [];

        /*********************************************************/
        $this->info("Ymlp : getting newsletters stats : OPEN ...");

        $NewsletterID = $newsletterId;
        $UniqueOpens = '1';
        $Page = '';
        $NumberPerPage = $this->count;
        $Sorting = '';

        $output = $this->ymlp->ArchiveGetOpens($NewsletterID , $UniqueOpens , $Page , $NumberPerPage , $Sorting);

        if ( $this->ymlp->ErrorMessage ) {
            echo "There's a connection problem: " . $this->ymlp->ErrorMessage;
        } else {
            if ( isset($output["Code"]) ) {
                echo "{$output["Code"]} => {$output["Output"]}";
            }
            elseif (!count($output)) {
                echo "No data available";
            }
            else {
                foreach ($output as $item) {
                    if (!array_key_exists($item['Email'], $data)) {
                        $data[$item['Email']] = [
                                                    'open'     => 1,
                                                    'no-open'  => 0,
                                                    'click'    => 0,
                                                    'note'     => 0,
                        ];
                    } else {
                        $data[$item['Email']]['open'] ++;
                    }
                }
            }
        }

        /*********************************************************/
        $this->info("Ymlp : getting newsletters stats : NO-OPEN ...");

        $NewsletterID = $newsletterId;
        $Page = '';
        $NumberPerPage = $this->count;
        $Sorting = '';

        $output = $this->ymlp->ArchiveGetUnopened($NewsletterID , $Page , $NumberPerPage , $Sorting);

        if ( $this->ymlp->ErrorMessage ) {
            echo "There's a connection problem: " . $this->ymlp->ErrorMessage;
        } else {
            if ( isset($output["Code"]) ) {
                echo "{$output["Code"]} => {$output["Output"]}";
            }
            elseif (!count($output)) {
                echo "No data available";
            }
            else {
                foreach ($output as $item) {
                    if (!array_key_exists($item['Email'], $data)) {
                        $data[$item['Email']] = [
                                                    'open'     => 0,
                                                    'no-open'  => 1,
                                                    'click'    => 0,
                                                    'note'     => 0,
                        ];
                    } else {
                        $data[$item['Email']]['no-open'] ++;
                    }
                }
            }
        }

        /*********************************************************/
        $this->info("Ymlp : getting newsletters stats : CLICK ...");

        $NewsletterID = $newsletterId;
        $LinkID = '';
        $UniqueClicks = '';
        $Page = '';
        $NumberPerPage = $this->count;
        $Sorting = '';

        $output = $this->ymlp->ArchiveGetClicks($NewsletterID, $LinkID, $UniqueClicks, $Page , $NumberPerPage , $Sorting);

        if ( $this->ymlp->ErrorMessage ) {
            echo "There's a connection problem: " . $this->ymlp->ErrorMessage;
        } else {
            if ( isset($output["Code"]) ) {
                echo "{$output["Code"]} => {$output["Output"]}";
            }
            elseif (!count($output)) {
                echo "No data available";
            }
            else {
                foreach ($output as $item) {
                    if (!array_key_exists($item['Email'], $data)) {
                        $data[$item['Email']] = [
                                                    'open'     => 0,
                                                    'no-open'  => 0,
                                                    'click'    => 1,
                                                    'note'     => 0,
                        ];
                    } else {
                        $data[$item['Email']]['click'] ++;
                    }
                }
            }
        }

        foreach($data as $email => & $stats) {
            if ($stats['open'] == 0) {
                $stats['open'] = $stats['click'];
            }
        }

        return $data;
    }

}