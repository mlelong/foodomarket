<?php

namespace AppBundle\Command;

use AppBundle\Repository\CarouselRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Repository\TaxonRepository;
use AppBundle\Services\ShopUtils;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Liip\ImagineBundle\Service\FilterService;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\Taxon;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

class GenerateFacebookCatalogProductsCommand extends ContainerAwareCommand
{
    /** @var string */
    private $catalogFilePath;

    /** @var Router */
    private $router;

    /** @var ShopUtils */
    private $shopUtils;

    /** @var TaxonRepository */
    private $taxonRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var FilterService */
    private $liipImagineFilterService;

    /** @var CarouselRepository */
    private $carouselRepository;

    /**
     * @required
     * @param FilterService $filterService
     */
    public function setLiipImagineFilterService(FilterService $filterService)
    {
        $this->liipImagineFilterService = $filterService;
    }

    /**
     * @required
     * @param CarouselRepository $carouselRepository
     */
    public function setCarouselRepository(CarouselRepository $carouselRepository)
    {
        $this->carouselRepository = $carouselRepository;
    }

    protected function configure()
    {
        $this
            ->setName('app:generate:facebook-catalog-products')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->catalogFilePath = $this->getContainer()->getParameter('kernel.project_dir') . '/web/facebook-catalog.csv';
        $this->router = $this->getContainer()->get('router');
        $this->shopUtils = $this->getContainer()->get('app.service.shop_utils');
        $this->taxonRepository = $this->getContainer()->get('sylius.repository.taxon');
        $this->supplierRepository = $this->getContainer()->get('app.repository.supplier');

        $this->generateCatalog($output);
    }

    private function taxonTreeToFlattenedSlugs(array $taxonTree, bool $lastLevelOnly)
    {
        $slugs = [];

        if (isset($taxonTree['children']) && !empty($taxonTree['children'])) {
            foreach ($taxonTree['children'] as $childTaxonTree) {
                $childrenSlugs = $this->taxonTreeToFlattenedSlugs($childTaxonTree, $lastLevelOnly);

                foreach ($childrenSlugs as $childSlug) {
                    $slugs[] = $childSlug;
                }
            }
        }

        if ($lastLevelOnly && (!isset($taxonTree['children']) || empty($taxonTree['children']))) {
            $slugs[] = $taxonTree['slug'];
        } elseif (!$lastLevelOnly && isset($taxonTree['children']) && !empty($taxonTree['children'])) {
            $slugs[] = $taxonTree['slug'];
        }

        return $slugs;
    }

    private function generateCatalog(OutputInterface $output)
    {
        /** @var FilterService */
        $imagine = $this->liipImagineFilterService;
//        $runtimeConfig = [
//            'watermark' => [
//                'image' => 'Resources/watermark/fb-watermark-1.png',
//                'size' => 1,
//                'position' => 'topleft'
//            ]
//        ];

        $taxons = [
            // FRUITS_LEGUMES => partenaires (tous, sauf HP3)
            69 => $this->supplierRepository->getPartners(false),
            // CREMERIE => Jacob, MTC, Daumesnil
            2 => [37, 56, 22],
            // BOUCHERIE => Jacob, MTC, Daumesnil
            34 => [37, 56, 22],
            // CHARCUTERIE => Jacob, MTC, Daumesnil
            507 => [37, 56, 22]
        ];
        $lastLevelSlugs = [];
        $firstLevelSlugs = [];

        // We could of course have taken only taxon 69 to getCatalogProductsByTaxonSlug,
        // but it would have lead to categories "Fruits & Légumes" only for each product
        // instead we take every last level taxon to build a proper tree
        foreach ($taxons as $taxonId => $_suppliers) {
            /** @var Taxon $taxon */
            $taxon = $this->taxonRepository->find($taxonId);
            $taxonTree = $this->taxonRepository->getTaxonTree(
                $taxon,
                $_suppliers,
                1000
            );

            $lastLevelSlugs = array_merge($lastLevelSlugs, $this->taxonTreeToFlattenedSlugs($taxonTree, true));
            $firstLevelSlugs = array_merge($firstLevelSlugs, $this->taxonTreeToFlattenedSlugs($taxonTree, false));
        }

        // Just in case
        $slugs = array_unique(array_merge($lastLevelSlugs, $firstLevelSlugs));

        $handle = fopen($this->catalogFilePath, 'w+');

        // Writes the header
        fputcsv($handle, ['id', 'availability', 'condition', 'description', 'image_link', 'link', 'title', 'product_type', 'price', 'brand', '_title', '_price', '_unit', '_home', '_facebook'], ",");

        $cache = [];

        foreach ($slugs as $id => $slug) {
            $products = $this->shopUtils->getCatalogProductsByTaxonSlug(urlencode($slug));

            $category = array_reduce($products['taxons'], function($carry, $current) {
                if ($current['id'] == 1) {
                    return $carry;
                }

                $current['name'] = trim($current['name']);

                return empty($carry) ? $current['name'] : "$carry > {$current['name']}";
            }, '');

            foreach ($products['products'] as $product) {
                if (isset($cache[$product['id']])) {
                    continue ;
                }

                $cache[$product['id']] = true;

                if (stripos($product['picture'], 'shop-default-product') !== false) {
                    continue ;
                }

                $nbSuppliers = $nbOffers = count($product['variants']);
                $nbOffers .= $nbOffers > 1 ? ' offres' : ' offre';
                $nbSuppliers .= $nbSuppliers > 1 ? ' fournisseurs' : ' fournisseur';
                $description = "$nbOffers à partir de {$product['startingPriceOnline']}";

                $nb = preg_match_all('/(?<price>[\d.]+)€/', $product['startingPriceOnline'], $matches);
                $price = PHP_FLOAT_MAX;

                for ($i = 0; $i < $nb; ++$i) {
                    $tmp = floatval($matches['price'][$i]);

                    if ($tmp < $price) {
                        $price = $tmp;
                    }
                }

                if ($price == 0) {
                    continue ;
                }

                $price = number_format($price, 2, '.', '');

                // 00  - 99  => 100 steps
                // 20% - 10% => 11 steps

                $cents = (int)explode('.', $price)[1];
                $percentageOff = (int)ceil(20 - ($cents / (100 / 11)));

                if ($percentageOff < 10) {
                    $percentageOff = 10;
                } elseif ($percentageOff > 20) {
                    $percentageOff = 20;
                }

                $priceDisplay = "{$price} EUR";

                /** @var Product $productObject */
                $productObject = $this->getContainer()->get('sylius.repository.product')->find($product['id']);
                $imagePath = $productObject->getImages()->first()->getPath();

                $runtimeConfig['watermark2'] = [
                    'image' => "Resources/watermark/reduction-$percentageOff.png",
                    'size' => 1,
                    'position' => 'bottomright'
                ];

//                $resourcePath = $imagine->getUrlOfFilteredImageWithRuntimeFilters(
//                    $imagePath,
//                    'front_big',
//                    $runtimeConfig
//                );

                try {
                    $resourcePath = $imagine->getUrlOfFilteredImage($imagePath, 'front_big');
                } catch (\Exception $e) {
                    $output->writeln($e->getMessage());
                    continue ;
                }

                $isHome = $this->isInCarousel($product['id'], 'home');
                $isFacebook = $this->isInCarousel($product['id'], 'facebook');

                $productLine = [
                    'id' => $product['id'],
                    'availability' => 'available for order',
                    'condition' => 'new',
                    'description' => $description,
                    'image_link' => $resourcePath,
                    'link' => $this->router->generate('app_front_product_page', ['slug' => urlencode($product['slug'])], UrlGeneratorInterface::ABSOLUTE_URL),
                    'title' => mb_convert_case($product['name'], MB_CASE_TITLE) . " sur FoodoMarket : $nbSuppliers",
                    'product_type' => $category,
                    'price' => $priceDisplay,
                    'brand' => 'FoodoMarket',
                    '_title' => mb_convert_case($product['name'], MB_CASE_TITLE),
                    '_price' => $price,
                    '_unit' => stripos($description, 'kg') !== false ? 'Kg' : 'Pc',
                    '_home' => $isHome ? '1' : '0',
                    '_facebook' => $isFacebook ? '1' : '0'
                ];

                fputcsv($handle, $productLine, ",");
            }
        }

        fclose($handle);
    }

    /**
     * @param int|string $productId
     * @param string $carouselName
     * @return bool
     */
    private function isInCarousel($productId, string $carouselName)
    {
        try {
            return 0 < (int)$this->carouselRepository->createQueryBuilder('c')
                    ->select('COUNT(c)')
                    ->join('c.items', 'i')
                    ->andWhere('c.name = :carouselName')
                    ->andWhere('i.product = :product')
                    ->setParameter('carouselName', $carouselName)
                    ->setParameter('product', $productId)
                    ->getQuery()
                    ->getSingleScalarResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return false;
    }
}