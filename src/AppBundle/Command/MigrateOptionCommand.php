<?php

namespace AppBundle\Command;

use AppBundle\Entity\ImportLine;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValue;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateOptionCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $manager;

    /** @var EntityRepository */
    private $importLineRepository;

    protected function configure()
    {
        $this->setName('app:option:migrate');
        $this->addArgument('optionCode', InputArgument::OPTIONAL);
    }

    private function removeNamingBioOption(ProductVariantInterface $variant): void {
        /** @var ProductOptionValueInterface[] $optionValues */
        $optionValues = $variant->getOptionValues();

        foreach ($optionValues as $optionValue) {
            if ($optionValue->getOptionCode() == 'AP' && $optionValue->getValue() == 'BIO') {
                $variant->removeOptionValue($optionValue);
                $this->manager->flush();
                break ;
            }
        }
    }

    private function addBioOption(ProductVariantInterface $variant): void {
        /** @var ProductOptionInterface $bioOption */
        $bioOption = $this->getContainer()->get('sylius.repository.product_option')->findOneBy(['code' => 'BI']);
        /** @var ProductOptionValueInterface[] $optionValues */
        $optionValues = $variant->getOptionValues();
        $found = false;

        foreach ($optionValues as $optionValue) {
            if ($optionValue->getOptionCode() == 'BI' && $optionValue->getCode() == 'BIO') {
                $found = true;
                break ;
            }
        }

        if (!$found) {
            $bioOptionValues = $bioOption->getValues();
            $optionValue = null;

            foreach ($bioOptionValues as $bioOptionValue) {
                if ($bioOptionValue->getCode() == 'BIO') {
                    $optionValue = $bioOptionValue;
                    break ;
                }
            }

            if ($optionValue === null) {
                $optionValue = new ProductOptionValue();
                $optionValue->setCurrentLocale('fr_FR');
                $optionValue->setOption($bioOption);
                $optionValue->setValue('OUI');
                $optionValue->setCode('BIO');

                $this->manager->persist($optionValue);
                $this->manager->flush();

                $bioOption->addValue($optionValue);
                $this->manager->flush();
            }

            $variant->addOptionValue($optionValue);
            $this->manager->flush();
        }
    }

    private function migrateNaming() {
        $qb = $this->importLineRepository->createQueryBuilder('i');
        /** @var ImportLine[] $lines */
        $lines = $qb
            ->where('i.naming = :naming')
            ->andWhere($qb->expr()->isNotNull('i.productVariant'))
            ->setParameter('naming', 'BIO')
            ->getQuery()
            ->getResult();

        foreach ($lines as &$line) {
            $variant = $line->getProductVariant();

            $this->removeNamingBioOption($variant);
            $this->addBioOption($variant);
        }
    }

    private function migrateCategory(OutputInterface $output) {

        /** @var ImportLine[] $lines */
        $lines = $this->importLineRepository->findAll();

        foreach ($lines as &$line) {
            try {
                $content = unserialize($line->getContent());

                if (isset($content['category'])) {
                    switch ($content['category']) {
                        case '':
                            break ;
                        case '1':
                            $content['category'] = 'cc-1';
                            break ;
                        case '2':
                            $content['category'] = 'cc-2';
                            break ;
                        case 'i':
                        case 'I':
                            $content['category'] = 'cc-i';
                            break ;
                        case 'extra':
                        case 'EXTRA':
                            $content['category'] = 'cc-extra';
                            break ;
                        case 'A':
                            $content ['category'] = 'cc-a';
                            break ;
                        default:
                            $output->writeln("Unknown " . $content['category']);
                            break ;
                    }

                    $line->setContent(serialize($content));
                }
            } catch (\Exception $exception) {
                $output->writeln($exception->getMessage());
            }
        }

        $this->manager->flush();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->manager = $this->getContainer()->get('doctrine')->getManager();
        /** @var ProductOptionInterface $categoryOption */
        $categoryOption = $this->getContainer()->get('sylius.repository.product_option')->findOneBy(['code' => 'CC']);
        $this->importLineRepository = $this->getContainer()->get('app.repository.import_line');

        $optionCode = $input->getArgument('optionCode');

        if (!empty($optionCode)) {
            if ($optionCode == 'AP') {
                $this->migrateNaming();
            } else if ($optionCode == 'CC') {
                $this->migrateCategory($output);
            }

            return ;
        }

        $qb = $this->importLineRepository->createQueryBuilder('i');
        $qb->where($qb->expr()->isNotNull('i.category'));
        $qb->andWhere('i.category != \'\'');
        $qb->andWhere($qb->expr()->isNotNull('i.productVariant'));

        /** @var ImportLine[] $lines */
        $lines = $qb->getQuery()->getResult();

        foreach ($lines as &$line) {
            $hasOption = false;

            $variant = $line->getProductVariant();
            /** @var ProductOptionValueInterface[] $optionValues */
            $optionValues = $variant->getOptionValues();

            foreach ($optionValues as $optionValue) {
                if ($optionValue->getOptionCode() == 'CC') {
                    $hasOption = true;
                }
            }

            if (!$hasOption) {
                $categoryOptionValue = null;

                foreach ($categoryOption->getValues() as $optionValue) {
                    if ($optionValue->getValue() == $line->getCategory()) {
                        $categoryOptionValue = $optionValue;
                        break ;
                    }
                }

                if ($categoryOptionValue === null) {
                    $categoryOptionValue = new ProductOptionValue();
                    $categoryOptionValue->setCurrentLocale('fr_FR');
                    $categoryOptionValue->setOption($categoryOption);
                    $categoryOptionValue->setValue($line->getCategory());
                    $categoryOptionValue->setCode(Slugify::create()->slugify('CC_' . $line->getCategory()));

                    $this->manager->persist($categoryOptionValue);
                    $this->manager->flush();

                    $categoryOption->addValue($categoryOptionValue);
                    $this->manager->flush();
                }

                $variant->addOptionValue($categoryOptionValue);
            }
        }

        $this->manager->flush();
    }

}