<?php


namespace AppBundle\Command;


use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceDeliveryNote;
use AppBundle\Entity\InvoiceDeliveryNoteItem;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use FilesystemIterator;
use Iterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use RegexIterator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportInvoicesCommand extends ContainerAwareCommand
{
    private $invoiceVarDir;
    private $invoiceTmpDir;
    private $invoiceProcessingDir;
    private $invoicePreProcessedDir;
    private $invoiceProcessedDir;
    /** @var EntityManager */
    private $entityManager;
    /** @var resource */
    private $reportHandle;

    protected function configure()
    {
        $this
            ->setName('app:import:invoices')
            ->addOption('clean-db', null, InputOption::VALUE_NONE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // On aura besoin de l'entity manager
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        // On va parcourir le dossier temporaire
        $this->invoiceVarDir = $this->getContainer()->getParameter('kernel.project_dir') . '/var/invoice';

        // On va mettre les logs dans ce fichier
        if (!is_resource($this->reportHandle = fopen("{$this->invoiceVarDir}/report.csv", 'w'))) {
            $output->writeln("Impossible d'acquérir le lock sur le fichier de log, on s'arrête !");
            return ;
        }

        fputcsv($this->reportHandle, ['Fichier', 'Erreur']);

        // Les fichiers bruts sont dans le dossier `tmp`
        $this->invoiceTmpDir = $this->invoiceVarDir . '/tmp';
        // On splittera les fichiers brut et on les mettra dans `processing`
        $this->invoiceProcessingDir = $this->invoiceVarDir . '/processing';
        // On déplacera les factures rassemblées dans ce dossier
        $this->invoicePreProcessedDir = $this->invoiceVarDir . '/pre-processed';
        // On déplacera les factures qui ont réussi le parsing et l'intégration dans le dossier `processed`
        $this->invoiceProcessedDir = $this->invoiceVarDir . '/processed';

        // On commence par splitter les fichiers qui sont dans tmp
        // Ca supprimera ces derniers du dossier tmp
        $this->splitFiles($output);

        $output->writeln("\n###### Tout est splitté, on va commencer l'intégration ###### \n");

        // On va maintenant regrouper les fichiers qu'on a splitté, par numéro de facture
        // Ils passeront dans le dossier pre-processed
        $this->joinFiles($output);

        $keepGoing = 1;

        if ($input->getOption('clean-db')) {
            $keepGoing &= $this->truncateTable($output, InvoiceDeliveryNoteItem::class);
            $keepGoing &= $this->truncateTable($output, InvoiceDeliveryNote::class);
            $keepGoing &= $this->truncateTable($output, Invoice::class);
        }

        // On va traiter les factures à proprement parler maintenant
        // Les fichiers qui s'intégreront sans erreur seront déplacés vers le dossier pocessed
        if ($keepGoing) {
            $this->processFiles($output);
        }

        fclose($this->reportHandle);
    }

    private function shouldExclude(string $file)
    {
        return
            // Généraliste, si ce n'est pas un pdf on en veut pas...
            preg_match('/.+\.pdf$/i', $file) === 0
            // Daumesnil
            || preg_match('/^blcli/i', $file) > 0
            || preg_match('/^relcli/i', $file) > 0
            || preg_match('/^prel/i', $file) > 0
            || preg_match('/^relfac/i', $file) > 0
            || preg_match('/^rib/i', $file) > 0
            || preg_match('/conditions/i', $file) > 0
            || preg_match('/^extrait/i', $file) > 0
            // Daumesnil + Souama
            || preg_match('/^relev/i', $file) > 0
            // HP
            || preg_match('/grille/i', $file) > 0
            || preg_match('/^ca\s+/i', $file) > 0
        ;
    }

    private function reportError(string $file, string $error)
    {
        if (!is_resource($this->reportHandle)) {
            $this->reportHandle = fopen("{$this->invoiceVarDir}/report.csv", 'w');
        }

        if (is_resource($this->reportHandle)) {
            fputcsv($this->reportHandle, [$file, $error]);
        }
    }

    private function joinFiles(OutputInterface $output)
    {
        $iterator = $this->getRecursiveDirectoryRegexIterator($this->invoiceProcessingDir, '/^.+\.pdf/i');
        $files = $this->getSortedFiles($iterator);
        $nbFiles = count($files);

        $output->writeln("Il y a {$nbFiles} factures à recomposer par numéro...");

        $parserService = $this->getContainer()->get('app.parser.invoice');

        $lastInvoiceParts = [
            'supplier' => null,
            'num' => null,
            'parts' => []
        ];

        foreach ($files as $file) {
            $output->writeln("\tTraitement du fichier `{$file}`");

            $parser = $parserService->getParserForFile($file, $fileContent);

            if ($parser === null) {
                $currentInvoicePart = [
                    'supplier' => null,
                    'num' => null,
                ];

                $output->writeln("\t\tIl n'y a pas de parseur disponible pour ce fichier");

                $this->reportError($file, "Il n'y a pas de parseur disponible pour ce fichier");
            } else {
                $currentInvoicePart = [
                    'supplier' => $parser->getSupplier()->getDisplayName(),
                    'num' => $parser->getInvoiceNumber($fileContent),
                ];
            }

            // C'est la prochaine page de la facture si le num est null et que le fournisseur est le même que précédemment
            // ou si les numéro de factures sont non null et identiques
            if (($currentInvoicePart['num'] === null && $currentInvoicePart['supplier'] !== null && $currentInvoicePart['supplier'] === $lastInvoiceParts['supplier'])
            || $currentInvoicePart['num'] !== null && $currentInvoicePart['num'] === $lastInvoiceParts['num']) {
                $lastInvoiceParts['parts'][] = $file;
            } elseif ($lastInvoiceParts['num'] !== null) {
                $this->execJoinFiles($output, $lastInvoiceParts['supplier'], $lastInvoiceParts['num'], $lastInvoiceParts['parts']);

                $lastInvoiceParts = [
                    'supplier' => $currentInvoicePart['supplier'],
                    'num' => $currentInvoicePart['num'],
                    'parts' => [$file]
                ];
            } else {
                // C'est une initialisation
                if ($currentInvoicePart['num'] !== null) {
                    $lastInvoiceParts['num'] = $currentInvoicePart['num'];
                    $lastInvoiceParts['supplier'] = $currentInvoicePart['supplier'];
                    $lastInvoiceParts['parts'] = [$file];
                }
            }
        }

        if ($lastInvoiceParts['num'] !== null) {
            $this->execJoinFiles($output, $lastInvoiceParts['supplier'], $lastInvoiceParts['num'], $lastInvoiceParts['parts']);
        }
    }

    private function execJoinFiles(OutputInterface $output, string $supplier, string $num, array $files)
    {
        $dirName = null;
        $hashes = [];

        // On va échapper le nom du fichier avec des " et calculer le nom du répertoire des fichiers sources
        // On va aussi supprimer les doublons de fichiers sur la base de leur md5sum
        foreach ($files as $file) {
            $dirName = $dirName ?: dirname($file);
            $hashes[md5_file($file)] = $file;
        }

        // Il se pourrait bien que les doublons n'aient pas été supprimés comme désiré (car des métadonnées dans le fichier pourraient être différentes alors que le contenu lui est identique...)
        dedupe_files:
        for ($i = 0; $i < count($hashes) - 1; ++$i) {
            $keys = array_keys($hashes);

            $escapedA = escapeshellarg($hashes[$keys[$i]]);
            $escapedB = escapeshellarg($hashes[$keys[$i + 1]]);

            // Cet outil est BEAUCOUP MIEUX que similar_text pour les perf et le résultat ! (facteur 1000 voir 10000 sur nos fichiers de facture)
            // Nécessite l'installation de similarity-tester
            exec("sim_text -p -s -u -a {$escapedA} {$escapedB}", $execOutput, $execReturnVar);

            if ($execReturnVar === 0) {
                $similarities = [0, 0];
                $currentSimilarityIndex = 0;

                foreach ($execOutput as $outputLine) {
                    if (preg_match('/.+ consists for (?<percent>[\d.,]+) % of .+ material/', $outputLine, $match)) {
                        $similarities[$currentSimilarityIndex++] = (int)$match['percent'];
                    }
                }

                // Si les contenus sont identiques, alors il y a de grandes chances que ce soient les mêmes fichiers...
                if ($similarities[0] === 100 && $similarities[1] === 100) {
                    // On supprime un des doublons
                    unlink($hashes[$keys[$i + 1]]);
                    // On l'enlève aussi de notre tableau
                    unset($hashes[$keys[$i + 1]]);
                    // Et on recommence tout le process ! (évitera de boucler sur des clés qui ont été supprimées juste à l'instant)
                    goto dedupe_files;
                }
            } else {
                // Si qqc s'est mal passé alors on prend pas de risque et on retourne (comme ça les fichiers en attente le resteront)
                return ;
            }
        }

        foreach ($hashes as &$file) {
            $file = escapeshellarg($file);
        }

        $files = array_values($hashes);
        $nbFiles = count($files);

        $output->writeln("\t\tRecomposition de la facture `$num` à partir de $nbFiles fichier(s)...");

        $dstDir = "{$this->invoicePreProcessedDir}/{$supplier}";

        if (!is_dir($dstDir)) {
            if (!mkdir("$dstDir", 0777, true)) {
                $output->writeln("\t\t\tImpossible de créer le répertoire `$dstDir`");
                return ;
            }
        }

        $files = implode(' ', $files);
        $dstFile = escapeshellarg("{$dstDir}/$num.pdf");

        exec("pdfunite {$files} {$dstFile}", $pdfUniteOutput, $pdfUniteReturnVar);

        if ($pdfUniteReturnVar === 0) {
            $output->writeln("\t\t\t=> Ok");

            exec("rm -f $files", $rmOutput, $rmReturnVar);

            if ($rmReturnVar === 0) {
                $output->writeln("\t\t\t=> Suppression des fichiers d'origine: OK");
            } else {
                $output->writeln("\t\t\t=> Suppression des fichiers d'origine: KO !");
            }
        } else {
            $output->writeln("\t\t\t=> KO !");
        }

        // Si le répertoire source est vide, alors on le supprime
        if (!(new FilesystemIterator($dirName))->valid()) {
            rmdir($dirName);
        }
    }

    /**
     * @param string $dir
     * @param string $pattern
     * @return RegexIterator
     */
    private function getRecursiveDirectoryRegexIterator(string $dir, string $pattern)
    {
        $directory = new RecursiveDirectoryIterator($dir);
        $iterator = new RecursiveIteratorIterator($directory);

        return new RegexIterator($iterator, $pattern, RecursiveRegexIterator::GET_MATCH);
    }

    /**
     * Trie les fichiers trouvés par nom et numéro
     *
     * @param Iterator $iterator
     * @return array
     */
    private function getSortedFiles(Iterator $iterator)
    {
        $files = [];

        // Puts every result of iterator to array
        foreach ($iterator as $item) {
            $files[] = $item[0];
        }

        // Sort by name and file number
        usort($files, function($a, $b) {
            $aDir = (int)basename(dirname($a));
            $bDir = (int)basename(dirname($b));

            if ($aDir < $bDir) {
                return -1;
            } elseif ($aDir > $bDir) {
                return 1;
            }

            $aName = basename($a);
            $bName = basename($b);

            preg_match('/-(?<num>[\d-]*)\.pdf$/i', $aName, $aMatch);
            preg_match('/-(?<num>[\d-]*)\.pdf$/i', $bName, $bMatch);

            $aName = str_replace($aMatch[0], '', $aName);
            $bName = str_replace($bMatch[0], '', $bName);

            $strCmp = strcmp($aName, $bName);

            if ($strCmp === 0) {
                $aNums = explode('-', $aMatch['num']);
                $bNums = explode('-', $bMatch['num']);
                $minNum = min(count($aNums), count($bNums));

                for ($i = 0; $i < $minNum; ++$i) {
                    if ((int)$aNums[$i] < (int)$bNums[$i]) {
                        return -1;
                    } elseif ((int)$aNums[$i] > (int)$bNums[$i]) {
                        return 1;
                    }
                }
            }

            return $strCmp;
        });

        return $files;
    }

    private function truncateTable(OutputInterface $output, $className)
    {
        $cmd = $this->entityManager->getClassMetadata($className);
        $connection = $this->entityManager->getConnection();

        try {
            $dbPlatform = $connection->getDatabasePlatform();
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');

            return true;
        } catch (DBALException $e) {
            $output->writeln("Impossible de truncate $className");
            return false;
        }
    }

    private function splitFiles(OutputInterface $output)
    {
        // On va récupérer les pdf uniquement
        $directory = new RecursiveDirectoryIterator($this->invoiceTmpDir);
        $iterator = new RecursiveIteratorIterator($directory);
        $regexIterator = new RegexIterator($iterator, '/^.+\.[a-zA-Z0-9]+$/i', RecursiveRegexIterator::GET_MATCH);

        // On compte combien de fichiers sont à traiter
        $nbFiles = iterator_count($regexIterator);
        $output->writeln("Il y a $nbFiles fichiers à parcourir et potentiellement splitter.");

        // Donc on boucle sur tous les pdf du dossier temporaire
        foreach ($regexIterator as $item) {
            $filePath = $item[0];
            $fileName = basename($filePath);
            $fileNameWithoutExtension = substr($fileName, 0, strrpos($fileName, '.'));
            $dir = dirname($filePath);
            $messageId = basename($dir);

            // On traite un fichier qui a été pré-splitté au préalable avant d'être renvoyé sur la boite mail, on n'en veut pas !
            // Edit: Mais le problème c'est que Jacob a un nom de fichier qui match avec cette regexp....
            // Ceci dit, en modifiant la commande qui télécharge les emails, on peut squizzer les doublons, donc je commente cette section
            // qui ne devrait plus servir à grand chose en plus d'être dangereuse
//            if (stripos($fileName, 'Facture FAC') === false) {
//                if (preg_match('/-\d+.pdf$/', $fileName)) {
//                    unlink($filePath);
//
//                    // Si le répertoire source est vide, alors on le supprime aussi
//                    if (!(new FilesystemIterator($dir))->valid()) {
//                        rmdir($dir);
//                    }
//
//                    continue;
//                }
//            }

            if ($this->shouldExclude($fileName)) {
                $output->writeln("Le fichier $fileName ne nous intéresse pas, on le supprime");

                unlink($filePath);

                // Si le répertoire source est vide, alors on le supprime aussi
                if (!(new FilesystemIterator($dir))->valid()) {
                    rmdir($dir);
                }

                continue ;
            }

            $processingDstDir = "{$this->invoiceProcessingDir}/$messageId";

            // On créé le répertoire de traitement s'il n'existe pas
            if (!is_dir($processingDstDir)) {
                if (!mkdir($processingDstDir, 0777, true)) {
                    $output->writeln("[050] Impossible de créer le réperoire de traitement `$processingDstDir`");
                    continue ;
                }
            }

            $escapedFilePath = escapeshellarg($filePath);
            $escapedDstFilePattern = escapeshellarg("$processingDstDir/$fileNameWithoutExtension-%d.pdf");

            $output->writeln("Exécution de la commande `pdfseparate {$escapedFilePath} {$escapedDstFilePattern}`");

            // On split les pages du fichier pdf dans un dossier de traitement effectif
            exec("pdfseparate {$escapedFilePath} {$escapedDstFilePattern}", $execOutput, $execReturnVar);

            // Si on a réussi à splitter, alors on supprime le fichier source temporaire
            if ($execReturnVar === 0) {
                unlink($filePath);

                // Si le répertoire source est vide, alors on le supprime aussi
                if (!(new FilesystemIterator($dir))->valid()) {
                    rmdir($dir);
                }
            } else {
                $output->writeln("[100] Impossible de splitter le PDF `$filePath`");
                continue ;
            }
        }
    }

    private function processFiles(OutputInterface $output)
    {
        $iterator = $this->getRecursiveDirectoryRegexIterator($this->invoicePreProcessedDir, '/^.+\.pdf/i');

        foreach ($iterator as $item) {
            $file = $item[0];
            $supplier = basename(dirname($file));
            $filename = basename($file);

            $output->writeln("Traitement de la facture $supplier : $filename");

            if ($this->processFile($output, $file)) {
                $output->writeln("\t => OK");
            } else {
                $output->writeln("\t => KO !");
            }
        }
    }

    /**
     * @param OutputInterface $output
     * @param string $filePath
     * @return bool
     */
    private function processFile(OutputInterface $output, string $filePath)
    {
        $invoiceParserService = $this->getContainer()->get('app.parser.invoice');

        try {
            $ret = $invoiceParserService->parseFile($filePath);

            /** @var bool $hasDiff */
            $hasDiff = $ret['hasDiff'];
            /** @var Invoice $invoice */
            $invoice = $ret['invoice'];
        } catch (Exception $e) {
            $output->writeln("\t\t{$e->getMessage()}");

            $this->reportError($filePath, $e->getMessage());

            return false;
        }

        if ($hasDiff && $invoice !== null) {
            $output->writeln("\t\tIl y a du nouveau ! On enregistre");

            $invoiceDst = str_replace([':prospect', ':supplier'], [$invoice->getProspect()->getId(), $invoice->getSupplier()->getId()], $this->getContainer()->getParameter('invoice_path'));

            if (!is_dir($invoiceDst)) {
                if (!mkdir($invoiceDst, 0777, true)) {
                    $this->reportError($filePath, "Le dossier de destination public de la facture ne peut être créé.");

                    return false;
                }
            }

            if ($invoice->getDate() === null) {
                $this->reportError($filePath, "La structure de la facture a changé ! Il faut revoir le parseur {$invoice->getSupplier()->getDisplayName()}");

                return false;
            }

            $invoiceDst .= "/{$invoice->getNumber()} - Facture {$invoice->getSupplier()->getDisplayName()} du {$invoice->getDate()->format('d-m-Y')}.pdf";

            $invoice->setInvoicePath($invoiceDst);

            if (!copy($filePath, $invoiceDst)) {
                $this->reportError($filePath, "La facture ne peut pas être déplacée dans son dossier de destination public.");

                return false;
            }

            $processedDir = "{$this->invoiceProcessedDir}/{$invoice->getSupplier()->getDisplayName()}";

            if (!is_dir($processedDir)) {
                if (!mkdir($processedDir, 0777, true)) {
                    $this->reportError($filePath, "Impossible de créer le répertoire fournisseur dans processed.");
                    return false;
                }
            }

            try {
                $this->entityManager->persist($invoice);
                $this->entityManager->flush();
            } catch (ORMException $e) {
                $output->writeln("\t\t{$e->getMessage()}");

                $this->reportError($filePath, $e->getMessage());

                return false;
            }

            // On déplace le fichier de pre-processed vers processed
            $fileName = basename($filePath);
            if (!rename($filePath, "$processedDir/$fileName")) {
                $this->reportError($filePath, "Impossible de déplacer le fichier $filePath vers $processedDir/$fileName");
                return false;
            }

            return true;
        } elseif (!$hasDiff && $invoice !== null) {
            // Supprime le fichier puisqu'il ne nous a rien apporté de nouveau
            $output->writeln("\t\tRien de nouveau, suppression du fichier");
            unlink($filePath);

            return true;
        } else {
            $output->writeln("\t\tCe fichier n'est pas supporté");

            $this->reportError($filePath, "Ce fichier n'est pas supporté");

            return false;
        }
    }
}