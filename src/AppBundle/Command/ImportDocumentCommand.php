<?php

namespace AppBundle\Command;

use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\SupplierProductPrice;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Core\Model\ProductTaxon;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportDocumentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:document:import')
            ->setDescription('Import Document')
            ->addArgument('supplier', InputArgument::REQUIRED, 'Supplier Id')
            ->addArgument('restaurant', InputArgument::REQUIRED, 'Restaurant Id')
            ->addArgument('filename', InputArgument::REQUIRED, 'Filename to import')
            ->addArgument('mercurialeId', InputArgument::OPTIONAL, 'Mercurial Id')
            ->addArgument('automatic-validation', InputArgument::OPTIONAL, 'Automatic validation : 1')
            ->addArgument('automatic-matching', InputArgument::OPTIONAL, 'Automatic matching : 1 : Try to find matching products by similar text score')
            ->addArgument('pricesDate', InputArgument::OPTIONAL, 'Prices date : 20171225')
            ->addArgument('filetype', InputArgument::OPTIONAL, 'File Type : MERCURIAL, BON_DE_COMMANDE, BON_DE_LIVRAISON, FACTURE')
            ->addOption('promo', null, InputOption::VALUE_NONE, 'Set prices imported as promo')
            ->setHelp('
            <info>Import supplier mercuriale</info>
            Example : php bin/console app:document:import 33 13 MATRICE_REST_BOULOGNE.xls 330 1 0 20171225 MERCURIAL
            Example : php bin/console app:document:import 33 13 MATRICE_REST_BOULOGNE.xls 330 0
            Example : php bin/console app:document:import 33 13 MATRICE_REST_BOULOGNE.xls 330 0 --promo
            ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierId = $input->getArgument('supplier');
        $restaurantId = $input->getArgument('restaurant');
        $filename = $input->getArgument('filename');
        $filetype = $input->getArgument('filetype');
        $pricesDate = $input->getArgument('pricesDate');
        $mercurialeId = $input->getArgument('mercurialeId');
        $automaticValidation = $input->getArgument('automatic-validation');
        $automaticMatching = $input->getArgument('automatic-matching');

        if (is_null($mercurialeId)) {
            $mercurialeId = 0;
        }

        if (is_null($automaticValidation)) {
            $automaticValidation = true;
        } else {
            $automaticValidation = boolval($automaticValidation);
        }

        if (is_null($filetype)) {
            $filetype = 'MERCURIAL';
        }

        if (is_null($pricesDate)) {
            $pricesDate = date('Ymd');
        }

        $supplier = $this->getContainer()->get('doctrine')->getManager()
                        ->getRepository('AppBundle:Supplier')->find($supplierId);
        $restaurant = $this->getContainer()->get('doctrine')->getManager()
                        ->getRepository('AppBundle:Restaurant')->find($restaurantId);

        $productManager = $this->getContainer()->get('app.product.manager');
        $doctrine = $this->getContainer()->get('doctrine');
        $manager = $doctrine->getManager();

        // desactivate cache listener
        $eventManager = $manager->getEventManager();
        $eventManager->removeEventListener(
            array('postFlush', 'onFlush'),
            $this->getContainer()->get('app.cache.listener')
        );

        // fetcher to use !
        $fetcherToUse = $supplier->getMercurialPattern();
        $import = $productManager->import($fetcherToUse, $filetype, $filename, $pricesDate, $supplier, $restaurant, $mercurialeId);
        $import->setPromo($input->getOption('promo'));

        // Automatic Matching for line with no taxon or product !
        if ($automaticMatching) {

            $productManager->refreshTaxonAndProduct($import->getId());
            $manager->flush();
        }

        // Automatic Validation
        if ($automaticValidation) {
            $doctrine->getConnection()->beginTransaction();

            if ($pricesDate == date('Ymd')) {

                // delete all prices previously recorded by previous import

                /** @var QueryBuilder $qb */
                $qb = $this->getContainer()->get('app.repository.supplier_product_price')->createQueryBuilder('spp');

                $pricesRemoved = $qb
                    ->delete()
                    ->where('spp.supplier = :supplier')
                    ->andWhere('spp.restaurant = :restaurant')
                    ->andWhere('spp.mercurialeId = :mercurialeId')
                    ->setParameter('supplier', $supplier)
                    ->setParameter('restaurant', $restaurant)
                    ->setParameter('mercurialeId', $mercurialeId)
                    ->getQuery()
                    ->getResult();
            }

            // import the prices in database

            try {
                foreach ($import->getLines() as $line) {
                    if ($line->getStatus() == ImportLine::STATUS_MATCHED_WITH_CATALOG) {
                        $productManager->importProduct($import, $line);
                    }
                }

                $import->setStatus(Import::STATUS_PARTIALLY_IMPORTED);
                $manager->flush();
                $doctrine->getConnection()->commit();

            } catch (\Exception $e) {
                $doctrine->getConnection()->rollBack();
                var_dump($e->getMessage());
            }
        }
    }
}
