<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Gaufrette\Filesystem;
use Gaufrette\Adapter\Local as LocalAdapter;
use Sylius\Component\Core\Model\ProductImage;

class ImportProductPictureCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:product:import-pictures')
            ->setDescription('Import Product Pictures')
            ->addArgument('directory', InputArgument::REQUIRED, 'Pictures root directory')
            ->setHelp('
            <info>Import product pictures</info>
            Example : php bin/console app:product:import-pictures /tmp/foodo-pictures
            ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = $input->getArgument('directory');
        $filesystem = $this->getContainer()->get('knp_gaufrette.filesystem_map');
        $mediaFilesytem = $filesystem->get('sylius_image');
        $productRepository = $this->getContainer()->get('sylius.repository.product');
        $redisCache = $this->getContainer()->get('app.redis.cache');

        $adapter = new LocalAdapter($directory);
        $tmpPicturesFilesystem = new Filesystem($adapter);

        $manager = $this->getContainer()->get('doctrine')->getEntityManager();

        foreach($tmpPicturesFilesystem->listKeys() as $keyFile) {
            foreach($keyFile as $filename){

                $output->writeln('process the file : '.$filename);

                $id = strstr($filename, '-', true);

                if (!is_numeric($id)) {
                    $output->writeln('"'.$id.'" is not an id');
                    continue;
                }

                $reversedId = strrev(str_pad($id%1000, 3, '0',STR_PAD_LEFT));
                $storagePath = '';
                for($i=0; $i<3; $i++) {
                    $storagePath .= $reversedId[$i].'/';
                }
                $newFilename = $storagePath.$filename;

                // get the product
                $product = $productRepository->find($id);
                if (!$product) {
                    $output->writeln('product #'.$id.' not found');
                    continue;
                }
                $output->writeln('update product #'.$id);

                // remove old picture if existing
                if ($product->hasImages()) {
                    foreach($product->getImages() as $image) {
                        $output->writeln('remove the file : '.$filename);
                        $product->removeImage($image);
                    }
                    // the picture remove listener will remove the file
                    $manager->flush();
                }

                // update or insert the path in dbb
                $image = new ProductImage();
                $image->setPath($newFilename);
                $product->addImage($image);

                $output->writeln('add picture : '.$newFilename);
                $output->writeln('------------------------------------------------------------');

                // store the picture in cdn
                $file = $tmpPicturesFilesystem->get($filename);
                $ret = $mediaFilesytem->write($storagePath.$filename, $file->getContent());

                // remove the source picture
                $tmpPicturesFilesystem->delete($filename);

                $manager->flush();

                $output->writeln('remove cache for product : '.$product->getId());
                $output->writeln('------------------------------------------------------------');
                $redisCache->removeCatalogSupplierProductCache('*', $product->getId());

                foreach($product->getVariants() as $variant) {

                    $output->writeln('remove cache for variant : '.$variant->getId());
                    $output->writeln('------------------------------------------------------------');
                    $redisCache->removeRestaurantSupplierVariantCache(
                                    "*",
                                    "*",
                                    $variant->getId()
                    );
                }
            }
        }

    }
}
