<?php


namespace AppBundle\Command;


use AppBundle\Helper\CurlHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SmsAntiChurnCommand extends Command
{
    public static $defaultName = 'app:sms:anti-churn';

    /** @var ParameterBagInterface */
    private $parameterBag;

    /** @var OutputInterface */
    private $output;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        parent::__construct();
        $this->parameterBag = $parameterBag;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        // Only try on the first step for now (testing purpose)
        $steps = [
            [
                'dealstage' => '2628111',
                'sms' => "Bonjour c'est {owner} de FOODOMARKET. Petit message pour savoir si tout se passe bien et si vous avez pu repasser commande de fruits&légumes cette semaine ?"
            ],
//            [
//                'dealstage' => '3114659',
//                'sms' => "Bonjour c'est {owner} (FOODOMARKET), est ce que tout se passe bien ? Nous avons d'autres grossistes qui proposent aussi de la crèmerie/épicerie/viande cela pourrait être intéressant de comparer (filet de poulet frais 3€45/kg, œufs 0,09/pcs etc…)."
//            ],
//            [
//                'dealstage' => '3114660',
//                'sms' => "Bonjour c'est {owner}, je viens aux nouvelles savoir si tout se passe bien, avez-vous besoin de quoi que ce soit ? D'autres fournisseurs ?"
//            ],
        ];

        $ownerMap = [
            '43004639' => ['owner' => 'Pauline', 'tel' => '+33755503081'],
            '43063026' => ['owner' => 'Farid', 'tel' => '+33755503367']
        ];

        foreach ($steps as $step) {
            $contacts = $this->getDataForSmsShooting($step['dealstage']);

            foreach ($contacts as $contact) {
                $phone = preg_replace('/\s+/', '', $contact['phone']);
                $phone = preg_replace('/\.+/', '', $phone);
                $phone = preg_replace('/\(+/', '', $phone);
                $phone = preg_replace('/\)+/', '', $phone);

                $phones = preg_split('/[^0-9]+/', $phone);

                foreach ($phones as $phone) {
                    if (empty($phone)) {
                        continue ;
                    }

                    if (strpos($phone, '0') === 0) {
                        $phone = substr($phone, 1);
                    }

                    if (strpos($phone, '+') !== 0) {
                        $phone = '+33' . $phone;
                    }

                    if (strpos($phone, '+336') === false && strpos($phone, '+337') === false) {
                        continue;
                    }

                    if (isset($ownerMap[$contact['owner']])) {
                        $owner = $ownerMap[$contact['owner']];

                        $this->sendSms($phone, $contact['firstname'], $owner['tel'], str_replace('{owner}', $owner['owner'], $step['sms']));
                    } else {
                        $output->writeln("Unknown owner `{$contact['owner']}` for transaction");
                    }
                }
            }
        }

        return 0;
    }

    private function sendSms(string $phone, string $firstname, string $fromNumber, string $content)
    {
        $this->output->writeln("Sending SMS to $phone, from $fromNumber : $content");

        $apiKey = $this->parameterBag->get('ringover_api_key');

//        return true;

        $ret = CurlHelper::call('https://public-api.ringover.com/v2/push/sms', ["Authorization:$apiKey"], json_encode([
            'archived_auto' => false,
            'from_number' => $fromNumber,
            'to_number' => $phone,
            'content' => $content
        ]));

        sleep(1);

        return true;
    }

    private function getDataForSmsShooting($dealstage)
    {
        $hubspot = $this->initHubspotSDK();

        // Récupération des deal au dealstage ci dessus
        $callback = function($offset = 0, $previous = []) use($hubspot, $dealstage, &$callback) {
            $data = $hubspot->deals()->all(['includeAssociations' => 'true', 'limit' => 250, 'offset' => $offset, 'properties' => ['dealstage', 'hs_created_by_user_id', 'hubspot_owner_id', 'phone']])->getData();
            $current = [];

            foreach ($data->deals as $deal) {
                if (isset($deal->properties->dealstage->value) && $deal->properties->dealstage->value === $dealstage) {
                    $current[] = $deal;
                }
            }

            if (isset($data->hasMore) && $data->hasMore) {
                return array_merge($previous, $current, $callback($data->offset));
            }

            return array_merge($previous, $current);
        };

        $deals = $callback();

        $contacts = [];

        // Parcours des deals pour récupération des informations nécessaires à l'envoi de SMS (un prénom, un numéro de mobile et un propriétaire de la transaction)
        foreach ($deals as $deal) {
            $vids = $deal->associations->associatedVids;

            foreach ($vids as $vid) {
                $contact = $hubspot->contacts()->getById($vid)->getData();

                if (isset($contact->properties->mobilephone)) {
                    $phone = $contact->properties->mobilephone->value;
                } elseif (isset($deal->properties->phone)) {
                    $phone = $deal->properties->phone->value;
                } elseif (isset($contact->properties->phone)) {
                    $phone = $contact->properties->phone->value;
                } else {
                    continue ;
                }

                if (isset($contact->properties->firstname)) {
                    $firstname = $contact->properties->firstname->value;
                } elseif (isset($contact->properties->lastname)) {
                    $firstname = $contact->properties->lastname->value;
                } else {
                    continue ;
                }

                $contacts[] = [
                    'firstname' => $firstname,
                    'phone' => $phone,
                    'owner' => isset($deal->properties->hubspot_owner_id) ? $deal->properties->hubspot_owner_id->value : ''
                ];
            }
        }

        return $contacts;
    }

    /**
     * @return \SevenShores\Hubspot\Factory
     */
    private function initHubspotSDK()
    {
        $handlerStack = \GuzzleHttp\HandlerStack::create();
        $handlerStack->push(
            \SevenShores\Hubspot\RetryMiddlewareFactory::createRateLimitMiddleware(
                \SevenShores\Hubspot\Delay::getConstantDelayFunction()
            )
        );

        $handlerStack->push(
            \SevenShores\Hubspot\RetryMiddlewareFactory::createInternalErrorsMiddleware(
                \SevenShores\Hubspot\Delay::getExponentialDelayFunction(2)
            )
        );

        $guzzleClient = new \GuzzleHttp\Client(['handler' => $handlerStack]);

        $config = [
            'key'      => $this->parameterBag->get('hubspot_api_key'),
            'oauth2'   => false,
        ];

        return new \SevenShores\Hubspot\Factory($config, new \SevenShores\Hubspot\Http\Client($config, $guzzleClient));
    }
}
