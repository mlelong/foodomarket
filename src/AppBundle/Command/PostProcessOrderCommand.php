<?php


namespace AppBundle\Command;


use AppBundle\Entity\Order;
use AppBundle\Repository\OrderRepository;
use AppBundle\Services\OrderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PostProcessOrderCommand extends Command
{
    public static $defaultName = 'app:order:post-process';
    /**
     * @var OrderService
     */
    private $orderService;
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(OrderService $orderService, OrderRepository $orderRepository)
    {
        parent::__construct(null);
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
    }

    public function configure()
    {
        $this
            ->addArgument('orderId', InputArgument::REQUIRED)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($input->getArgument('orderId'));

        try {
            $data = $this->orderService->postCreateOrder($order);

            if (isset($data['orderData'])) {
                $output->writeln("Post create order successful");
            } else {
                $output->writeln("Post create order not successful !");
            }
        } catch (\Exception $e) {
            $output->writeln("Post create order not successful : {$e->getMessage()}");
        }

        return 0;
    }
}