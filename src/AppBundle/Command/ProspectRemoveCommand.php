<?php

namespace AppBundle\Command;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Entity\SupplierAccountLog;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProspectRemoveCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:prospect:remove')
            ->addArgument('emailOrId', InputArgument::REQUIRED)
            ->setDescription("Remove a prospect and all informations (customer, restaurant ....) using email or id")
            ->setHelp('
            <info>php bin/console app:prospect:remove prospect-email@domain.fr</info>
            ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $emailOrId = $input->getArgument('emailOrId');
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');

        if (is_numeric($emailOrId)) {
            /** @var Prospect $prospect */
            $prospect = $prospectRepository->findOneBy(['id' => $emailOrId]);
        } else {
            /** @var Prospect $prospect */
            $prospect = $prospectRepository->findOneBy(['email' => $emailOrId]);
        }

        if ($prospect === null) {
            $output->writeln("Can not find prospect with email or id `$emailOrId`");
            return ;
        }

        $output->writeln("Prepare to remove prospect with email or id `$emailOrId`");

        $customerRepository = $this->getContainer()->get('app.repository.customer');
        $restaurantRepository = $this->getContainer()->get('app.repository.restaurant');
        $shoppingListRepository = $this->getContainer()->get('app.repository.restaurant_stock');

        $customers = $prospect->getCustomers();
        $additionalCustomer = $customerRepository->findOneBy(['email' => $prospect->getEmail()]);

        if ($additionalCustomer !== null) {
            $customers->add($additionalCustomer);
        }

        foreach ($customers as $customer) {
            $output->writeln("Prepare to remove customer `{$customer->getEmail()}`");

            $restaurants = $customer->getRestaurants();

            foreach ($restaurants as $restaurant) {
                /** @var RestaurantStock[] $shoppingLists */
                $shoppingLists = $shoppingListRepository->findBy(['restaurant' => $restaurant]);

                if (!empty($shoppingLists)) {
                    $output->writeln("Prepare to remove shopping lists");

                    foreach ($shoppingLists as $shoppingList) {
                        $output->write("Removing shopping list `{$shoppingList->getName()}`... ");
                        $shoppingListRepository->remove($shoppingList);
                        $output->writeln("Ok");
                    }

                    $output->writeln("Shopping lists removed");
                }

                $output->write("Removing restaurant `{$restaurant->getName()}`... ");
                $restaurantRepository->remove($restaurant);
                $output->writeln("Ok");
            }

            $output->write("Removing customer `{$customer->getEmail()}`");
            $customerRepository->remove($customer);
            $output->writeln("Ok");
        }

        $supplierAccountLogRepository = $this->getContainer()->get('app.repository.supplier_account_log');

        /** @var SupplierAccountLog[] $supplierAccountLogs */
        $supplierAccountLogs = $supplierAccountLogRepository->findBy(['prospect' => $prospect]);

        if (!empty($supplierAccountLogs)) {
            $output->writeln("Prepare to remove Supplier Account Logs");

            foreach ($supplierAccountLogs as $supplierAccountLog) {
                $output->write("Removing supplier account log `{$supplierAccountLog->getSupplierCategory()}`... ");
                $supplierAccountLogRepository->remove($supplierAccountLog);
                $output->writeln("Ok");
            }
        }

        $output->write("Removing prospect...");
        $prospectRepository->remove($prospect);
        $output->writeln("Done");
    }
}