<?php


namespace AppBundle\Command;


use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Product\Model\ProductVariantTranslation;
use Sylius\Component\Product\Model\ProductVariantTranslationInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncProductsCommand extends Command
{
    public static $defaultName = 'app:sync:products';
    /**
     * @var SupplierRepository
     */
    private $supplierRepository;
    /**
     * @var SupplierProductVariantInformationsRepository
     */
    private $spviRepository;

    private $pairs = [];

    private $variantCache = [];

    /** @var ProductVariant[] */
    private $duplicateVariantCache = [];

    /** @var OutputInterface */
    private $output;

    /** @var EntityManagerInterface */
    private $manager;

    /** @var Supplier */
    private $dstSupplier;

    public function __construct(EntityManagerInterface $manager, SupplierRepository $supplierRepository, SupplierProductVariantInformationsRepository $spviRepository)
    {
        parent::__construct();

        $this->manager = $manager;
        $this->supplierRepository = $supplierRepository;
        $this->spviRepository = $spviRepository;
    }

    public function configure()
    {
        $this->addArgument('srcSupplier', InputArgument::REQUIRED, 'SRC partner supplier');
        $this->addArgument('dstSupplier', InputArgument::REQUIRED, 'DST partner supplier');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        /** @var Supplier $srcSupplier */
        $srcSupplier = $this->supplierRepository->find($input->getArgument('srcSupplier'));
        /** @var Supplier $dstSupplier */
        $this->dstSupplier = $dstSupplier = $this->supplierRepository->find($input->getArgument('dstSupplier'));

        if ($srcSupplier === null) {
            $output->writeln("SRC supplier not found");
            return -1;
        }

        if ($dstSupplier === null) {
            $output->writeln("DST supplier not found");
            return -1;
        }

        $output->writeln("Syncing products from {$srcSupplier->getDisplayName(true)} to {$dstSupplier->getDisplayName(true)}");

        /** @var SupplierProductVariantInformations[] $srcInfo */
        $srcInfo = $this->spviRepository->findBy(['supplier' => $srcSupplier]);
        /** @var SupplierProductVariantInformations[] $dstInfo */
        $dstInfo = $this->spviRepository->findBy(['supplier' => $dstSupplier]);

        foreach ($srcInfo as $src) {
            $found = false;

            // Let's find the same product among dst info
            foreach ($dstInfo as $dst) {
                if ($this->isMatching($src, $dst)) {
                    $found = true;
                    $output->writeln("Found matching `{$src->getReference()} {$src->getProductLabel()}`: `{$dst->getReference()} {$dst->getProductLabel()}`");

                    $this->pairs[$src->getId()]['dst'][] = $dst;
                }
            }

            if (!$found) {
                $output->writeln("#### Matching not found for `{$src->getReference()} {$src->getProductLabel()}` ####");
                $this->pairs[$src->getId()]['dst'] = null;
            }

            $this->pairs[$src->getId()]['src'] = $src;
        }

        $this->doSync();

        return 0;
    }

    private function doSync()
    {
        foreach ($this->pairs as $pair) {
            /** @var SupplierProductVariantInformations $src */
            $src = $pair['src'];
            /** @var SupplierProductVariantInformations[] $dsts */
            $dsts = $pair['dst'];

            // Update dst variants and spvi according to src
            if ($dsts !== null) {
                foreach ($dsts as $dst) {
                    // If this variant has already been treated, skip it
                    if (!isset($this->variantCache[$dst->getProductVariant()->getId()])) {
                        // Sync the variant options, weight and so on...
                        $this->syncVariant($src->getProductVariant(), $dst->getProductVariant());

                        // Update the variant cache, so we don't try to compute diff for this one later
                        $this->variantCache[$dst->getProductVariant()->getId()] = true;
                    }

                    // Sync the product matching, so that later imports will correctly update the price
                    $this->syncSpvi($src, $dst);

                    $this->manager->flush();
                }
            }
            // Create a new variant and spvi to associate with dst
            else {
                $this->output->writeln("Creating a copy of SPVI and variant");

                $variant = $this->createVariant($src->getProductVariant());
                $this->createSpvi($src, $this->dstSupplier, $variant);
            }
        }
    }

    private function createVariant(ProductVariant $src)
    {
        if (isset($this->duplicateVariantCache[$src->getId()])) {
            return $this->duplicateVariantCache[$src->getId()];
        }

        $variant = new ProductVariant();

        $variant->setCurrentLocale('fr_FR');
        $variant->setProduct($src->getProduct());
        $variant->setWeight($src->getWeight());
        $variant->setEnabled($src->isEnabled());
        $variant->setCode(uniqid());
        $variant->setName($src->getName());

        foreach ($src->getOptionValues() as $optionValue) {
            $variant->addOptionValue($optionValue);
        }

        $translation = $src->getTranslation('fr_FR');
        $newTranslation = $variant->getTranslation('fr_FR');

        $newTranslation->setName($translation->getName());

        $this->manager->persist($variant);
        $this->manager->flush();

        $this->duplicateVariantCache[$src->getId()] = $variant;

        return $variant;
    }

    private function createSpvi(SupplierProductVariantInformations $src, Supplier $dstSupplier, ProductVariant $dstVariant)
    {
        $spvi = new SupplierProductVariantInformations();

        $spvi->setWeight($src->getWeight());
        $spvi->setSalesUnit($src->getSalesUnit());
        $spvi->setUnitQuantity($src->getUnitQuantity());
        $spvi->setContent($src->getContent());
        $spvi->setDisplayName($src->getDisplayName());
        $spvi->setManuallyCreated($src->isManuallyCreated());
        $spvi->setOrderRef($src->getOrderRef());
        $spvi->setProduct($src->getProduct());
        $spvi->setProductLabel($src->getProductLabel());
        $spvi->setProductVariant($dstVariant);
        $spvi->setReference($src->getReference());
        $spvi->setVariantLabel($src->getVariantLabel());
        $spvi->setSupplier($dstSupplier);

        $this->spviRepository->add($spvi);
    }

    private function syncVariant(ProductVariant $src, ProductVariant $dst)
    {
        $this->output->writeln("Syncing variant {$src->getId()} with {$dst->getId()}...");

        // Look for difference in weight
        if ($dst->getWeight() != $src->getWeight()) {
            $this->output->writeln("\tChanging weight from {$dst->getWeight()}kg to {$src->getWeight()}kg");

            $dst->setWeight($src->getWeight());
        }

        // Look for difference in option value
        foreach ($src->getOptionValues() as $srcOptionValue) {
            $found = false;

            foreach ($dst->getOptionValues() as $dstOptionValue) {
                if ($dstOptionValue->getOptionCode() === $srcOptionValue->getOptionCode()) {
                    $found = true;

                    // Updating option value if different
                    if ($dstOptionValue->getId() !== $srcOptionValue->getId()) {
                        $this->output->writeln("\tChanging option {$srcOptionValue->getOptionCode()}: from {$dstOptionValue->getValue()} to {$srcOptionValue->getValue()}");

                        $dst->removeOptionValue($dstOptionValue);
                        $dst->addOptionValue($srcOptionValue);
                    }

                    break ;
                }
            }

            // Add missing option
            if (!$found) {
                $this->output->writeln("\tOption {$srcOptionValue->getOptionCode()} is not set. Adding with value {$srcOptionValue->getValue()}");

                $dst->addOptionValue($srcOptionValue);
            }
        }

        // Remove obsolete options
        foreach ($dst->getOptionValues() as $dstOptionValue) {
            $found = false;

            foreach ($src->getOptionValues() as $srcOptionValue) {
                if ($srcOptionValue->getOptionCode() === $dstOptionValue->getOptionCode()) {
                    $found = true;
                    break ;
                }
            }

            if (!$found) {
                $this->output->writeln("\tRemoving obsolete option {$dstOptionValue->getOptionCode()}: {$dstOptionValue->getValue()}");
                $dst->removeOptionValue($dstOptionValue);
            }
        }
    }

    private function syncSpvi(SupplierProductVariantInformations $src, SupplierProductVariantInformations $dst)
    {
        $this->output->writeln("Syncing SPVI {$src->getId()}-{$dst->getId()}...");

        if ($dst->getWeight() != $src->getWeight()) {
            $this->output->writeln("\tChanging weight from {$dst->getWeight()}kg to {$src->getWeight()}kg");
            $dst->setWeight($src->getWeight());
        }

        if ($dst->getUnitQuantity() != $src->getUnitQuantity()) {
            $this->output->writeln("\tChanging UQ from {$dst->getUnitQuantity()} to {$src->getUnitQuantity()}");
            $dst->setUnitQuantity($src->getUnitQuantity());
        }

        if ($dst->getSalesUnit() != $src->getSalesUnit()) {
            $this->output->writeln("\tChanging UM from {$dst->getSalesUnit()} to {$src->getSalesUnit()}");
            $dst->setSalesUnit($src->getSalesUnit());
        }
    }

    private function isMatching(SupplierProductVariantInformations $src, SupplierProductVariantInformations $dst)
    {
        if ($dst->getReference() !== null && $src->getReference() !== null && $dst->getReference() === $src->getReference()) {
            return true;
        } elseif ($dst->getProductLabel() === $src->getProductLabel()) {
            return true;
        }

        return false;
    }
}