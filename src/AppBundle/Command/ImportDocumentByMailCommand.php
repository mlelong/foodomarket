<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use AppBundle\Entity\Import;
use AppBundle\Entity\Supplier;

class ImportDocumentByMailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:mailbox:check')
            ->setDescription('Import new documents from Mailboxes')
            ->addOption('markmessageasread', null, InputOption::VALUE_NONE, 'Set the mail as read')
            ->addOption('deletemessage', null, InputOption::VALUE_NONE, 'Delete the message')
            ->addOption('displaydebug',null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp('
            <info>Check mailboxes to get new documents</info>
            Example : php bin/console app:mailbox:check --displaydebug --markmessageasread --deletemessage
            ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $deleteMessage = false;
        $markAsRead = false;
        $displayDebug = false;

        if ($input->getOption('deletemessage')) {
            $deleteMessage = true;
        }
        if ($input->getOption('markmessageasread')) {
            $markAsRead = true;
        }
        if ($input->getOption('displaydebug')) {
            $displayDebug = true;
        }

        $productManager = $this->getContainer()->get('app.product.manager');

        $restaurants = $this->getContainer()->get('doctrine')->getManager()
                        ->getRepository('AppBundle:Restaurant')->findAll();

        $mailbox = $this->getContainer()->getParameter('restaurant_mailbox');
        $mailboxManager = $this->getContainer()->get('app.mailbox.manager');

        foreach($restaurants as $restaurant) {

            $output->writeln('Checking : '.$restaurant->getName());

            if ($restaurant->getEmail()) {
                $emails = $mailboxManager->getNewEmails($mailbox, $restaurant->getEmail(), $restaurant->getPassword());

                if ($emails == false) {
                    continue;
                }

                foreach($emails as $id) {
                    $message = $mailboxManager->getContent($id);

                    $informations = $this->getDocumentInformations($message['content']);

                    $supplier = $this->getContainer()->get('doctrine')->getManager()
                                ->getRepository('AppBundle:Supplier')->find($informations['supplier']);

                    if (! $supplier) {
                        continue;
                    }


                    $output->writeln('Supplier found : '.$supplier->getName());

                    foreach($message['attachments'] as $attachment) {
                        $output->writeln('Importing : '.$attachment->name);
                        $import = $productManager->import($supplier->getMercurialPattern(),
                                                $informations['type'],
                                                $attachment->name,
                                                $informations['date'],
                                                $supplier,
                                                $restaurant)
                                ;
                        if ($import) {
                            //$productManager->matchWithProduct($import);
                        }
                    }

                    if ($markAsRead) {
                        $mailboxManager->markAsRead($id);
                    } else {
                        $mailboxManager->markAsUnread($id);
                    }

                    if ($deleteMessage) {
                        $mailboxManager->delete($id);
                    }
                }
            }
        }
    }

    protected function getDocumentInformations($content) {
        $informations = [
            'date' => date('Y-m-d'),
            'supplier' => null,
            'type' => Import::MERCURIAL,
        ];

        $matches = [];
        preg_match("/DATE([0-9]{8})/m", $content, $matches);
        if (array_key_exists(1, $matches)) {
             $informations['date'] = substr($matches[1], 0, 4).'-'.substr($matches[1], 4, 2).'-'.substr($matches[1], 6, 2);
        }

        $matches = [];
        preg_match("/SUPP([0-9]+)/m", $content, $matches);
        if (array_key_exists(1, $matches)) {
             $informations['supplier'] = $matches[1];
        }

        if (strstr($content, 'FACTURE')) {
            $informations['type'] = Import::FACTURE;
        }
        if (strstr($content, 'COMMANDE')) {
            $informations['type'] = Import::BON_DE_COMMANDE;
        }
        if (strstr($content, 'BL')) {
            $informations['type'] = Import::BON_DE_LIVRAISON;
        }

        return $informations;
    }

}
