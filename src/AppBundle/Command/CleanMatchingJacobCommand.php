<?php


namespace AppBundle\Command;


use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanMatchingJacobCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:clean-matching:jacob')
            ->addArgument('productsApiCsvFile', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $this->removeObsoleteRefs($input, $output);
        $this->repareRefs($input, $output);

        return 0;
    }

    private function repareRefs(InputInterface $input, OutputInterface $output)
    {
        /** @var SupplierProductVariantInformationsRepository $spviRepository */
        $spviRepository = $this->getContainer()->get('app.repository.supplier_product_variant_informations');
        $apiFile = $input->getArgument('productsApiCsvFile');
        $apiHandle = fopen($apiFile, 'r');

        if (!is_resource($apiHandle)) {
            $output->writeln("Impossible d'ouvrir le fichier $apiFile");

            return -1;
        }

        /** @var SupplierProductVariantInformations[] $spvis */
        $spvis = $spviRepository->findBy(['supplier' => 37]);

        $apiCopy = [];
        // Skip header
        fgetcsv($apiHandle);

        while ($line = fgetcsv($apiHandle)) {
            $apiCopy[] = $line;
        }

        fclose($apiHandle);

        foreach ($spvis as $spvi) {
            $found = false;

            foreach ($apiCopy as $line) {
                $product = $this->normalizeSpaces($line[2] . ' ' . $line[6] . ' ' . $line[4]);

                if (trim($spvi->getProductLabel()) === $product) {
                    $ref = "{$line[0]}-{$line[3]}";
                    $ref = substr($ref, 1);

                    $spvi->setReference($ref);
                    $spvi->setOrderRef($line[3]);

                    $found = true;
                    break ;
                }
            }

            if (!$found) {
                $output->writeln("Impossible de trouver le produit {$spvi->getProductLabel()} dans le fichier source de Jacob");
                return -1;
            }
        }

        $this->getContainer()->get('doctrine')->getManager()->flush();

        return 0;
    }

    private function removeObsoleteRefs(InputInterface $input, OutputInterface $output)
    {
        /** @var SupplierProductVariantInformationsRepository $spviRepository */
        $spviRepository = $this->getContainer()->get('app.repository.supplier_product_variant_informations');
        $apiFile = $input->getArgument('productsApiCsvFile');
        $apiHandle = fopen($apiFile, 'r');

        if (!is_resource($apiHandle)) {
            $output->writeln("Impossible d'ouvrir le fichier $apiFile");

            return -1;
        }

        /** @var SupplierProductVariantInformations[] $spvis */
        $spvis = $spviRepository->findBy(['supplier' => 37]);
        $mapped = [];

        foreach ($spvis as $spvi) {
            $mapped[$spvi->getId()] = $spvi->getProductLabel();
        }

        // Strip header
        fgetcsv($apiHandle);

        $toKeep = $toRemove = [];

        while ($line = fgetcsv($apiHandle)) {
            $product = $this->normalizeSpaces($line[2] . ' ' . $line[6] . ' ' . $line[4]);

            if ($key = array_search($product, $mapped)) {
                $toKeep[$key] = $product;
            }
        }

        fclose($apiHandle);

        foreach ($mapped as $key => $value) {
            if (!isset($toKeep[$key])) {
                $toRemove[$key] = $value;
            }
        }

        if (!empty($toRemove)) {
            $nbRemoved = $spviRepository->createQueryBuilder('spvi')
                ->delete()
                ->where('spvi.id IN (:ids)')
                ->setParameter('ids', array_keys($toRemove))
                ->getQuery()
                ->execute()
            ;

            $output->writeln("$nbRemoved infos de matching ont été supprimées");
        }

        return 0;
    }

    protected function normalizeSpaces($str) {
        return trim(preg_replace('/[\s]{2,}/', ' ' , $str));
    }
}
