<?php

namespace AppBundle\Command;

use AppBundle\Entity\Email;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\EmailRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Services\Mailer\AbstractMailManager;
use AppBundle\Services\Mailer\EmailFactory;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Gedmo\Timestampable\TimestampableListener;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

class ReviveRegistrationCommand extends ContainerAwareCommand
{
    /** @var AbstractMailManager */
    private $mailer;

    /** @var EmailFactory */
    private $emailFactory;

    /** @var EntityManager */
    private $prospectManager;

    /** @var SupplierAccountLogRepository */
    private $supplierAccountLogRepository;

    /** @var EmailRepository */
    private $emailRepository;

    /** @var Router */
    private $router;

    /** @var boolean */
    private $isDebug;

    protected function configure()
    {
        $this
            ->setName('app:registration:revive')
            ->setDescription("Envoie des emails de relance d'inscription aux prospects qui n'ont pas fini leur inscription.")
            ->addOption('debug', null, InputOption::VALUE_NONE, 'When specified, does not send email nor update database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->mailer = $this->getContainer()->get('app.sendinblue.manager');
        $this->emailFactory = $this->getContainer()->get('app.factory.email');
        $this->prospectManager = $this->getContainer()->get('app.manager.prospect');
        $this->supplierAccountLogRepository = $this->getContainer()->get('app.repository.supplier_account_log');
        $this->emailRepository = $this->getContainer()->get('app.repository.email');
        $this->router = $this->getContainer()->get('router');
        $this->isDebug = $input->getOption('debug');

        $output->writeln("Debug mode: " . ($this->isDebug ? 'On' : 'Off'));

        $this->disableTimestampable();

        try {
            $this->handlePopupStage($output);
        } catch (Exception $e) {
            $output->writeln("Exception occured in handlePopupStage: {$e->getMessage()}");
        }

        try {
            $this->handleCallbackStage($output);
        } catch (Exception $e) {
            $output->writeln("Exception occured in handleCallbackStage: {$e->getMessage()}");
        }

        try {
            $this->handleSubscriptionStage2($output);
        } catch (Exception $e) {
            $output->writeln("Exception occured in handleSubscriptionStage: {$e->getMessage()}");
        }

        try {
            $this->handleSepa($output);
        } catch (Exception $e) {
            $output->writeln("Exception occured in handleSepa: {$e->getMessage()}");
        }
    }

    /**
     * @param OutputInterface $output
     * @throws Exception
     */
    private function handleSepa(OutputInterface $output)
    {
        /** @var SupplierAccountLog[] $waitings */
        $waitings = $this->supplierAccountLogRepository->createQueryBuilder('o')
            ->andWhere('o.status = :status')
            ->andWhere("o.createdAt >= '2019-05-22'")
            ->setParameter('status', SupplierAccountLog::STATUS_WAITING)
            ->getQuery()
            ->getResult()
        ;
        $prospectCache = [];
        $now = new DateTime();
        $compareDate = (new DateTime())->modify('-7 day');

        $output->writeln("\nComputing prospects at sepa waiting stage");

        foreach ($waitings as $waiting) {
            $prospect = $waiting->getProspect();
            $supplier = $waiting->getSupplierCategory()->getSupplier();

            // Don't take Jacob into account
            if (in_array($supplier->getId(), [37])) {
                continue ;
            }

            $key = "{$prospect->getId()}-{$supplier->getId()}";

            if (isset($prospectCache[$key])) {
                continue ;
            }

            $relatedLogs = $this->supplierAccountLogRepository->getRelatedSupplierAccountLogs($waiting);
            $skip = false;
            $oldestLog = new DateTime();

            foreach ($relatedLogs as $relatedLog) {
                if ($relatedLog->getStatus() !== SupplierAccountLog::STATUS_WAITING) {
                    $skip = true;
                    break ;
                } else {
                    if ($oldestLog > $relatedLog->getCreatedAt()) {
                        $oldestLog = $relatedLog->getCreatedAt();
                    }
                }
            }

            if ($skip) {
                continue ;
            }

            $diff = $now->diff($oldestLog);
            $diffFormat = "{$diff->d} days, {$diff->h} hours and {$diff->i} minutes ago";

            $output->writeln("- Prospect `{$prospect->getEmail()}` is in waiting stage since {$diffFormat} for supplier {$supplier->getDisplayName()}");

            // If log is equal to or older than 7 days before today
            if ($oldestLog <= $compareDate) {
                // Send a revival email if we haven't done so yet
                $qb = $this->emailRepository->createQueryBuilder('e');
                $emails = $qb
                    ->where('e.templateId = 39')
                    ->andWhere($qb->expr()->like('e.to', ':to'))
                    ->andWhere($qb->expr()->like('e.variables', ':supplier'))
                    ->setParameter('to', "%{$prospect->getEmail()}%")
                    ->setParameter('supplier', "%{$supplier->getDisplayName()}%")
                    ->getQuery()
                    ->getResult()
                ;

                if (empty($emails)) {
                    $output->writeln("\tSending sepa revival to {$prospect->getEmail()} for supplier {$supplier->getDisplayName()}");

                    if (!$this->isDebug) {
                        $this->sendSepaRevival($waiting, $relatedLogs);
                    }
                } else {
                    $output->writeln("\tRevival email already sent");
                }
            }

            $prospectCache[$key] = true;
        }
    }

    /**
     * @param SupplierAccountLog $supplierAccountLog
     * @param SupplierAccountLog[] $relatedLogs
     * @throws Exception
     */
    private function sendSepaRevival(SupplierAccountLog $supplierAccountLog, array $relatedLogs)
    {
        $builder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);
        $prospect = $supplierAccountLog->getProspect();
        $categories = [];

        foreach ($relatedLogs as $relatedLog) {
            $categories[] = Supplier::getDisplayCategory($relatedLog->getSupplierCategory()->getCategory());
        }

        $supplierCategories = $supplierAccountLog->getSupplierCategory()->getSupplier()->getDisplayName();
        $supplierCategories .= ' ' . implode(', ', $categories);

        $builder
            ->setTemplateId(39)
            ->addTo($prospect->getEmail(), $prospect->getFirstName())
            ->addVariable('FIRSTNAME', $prospect->getFirstName())
            ->addVariable('SUPPLIER_CATEGORIES', $supplierCategories)
            ->addVariable('DELIVERY_HOURS', $prospect->getDeliveryHours())
        ;

        $email = $builder->build();

        $now = new DateTime();

        // We deactivated the translatable listener so let's put it like this
        $email->setCreatedAt($now);
        $email->setUpdatedAt($now);

        $this->mailer->addEmailToQueue($email);
    }

    /**
     * This method will send email to prospects that are still in popup stage
     *
     * Cases:
     *  - 30 minutes after first fill of popup
     *  - 1 day after first fill of popup
     *  - 7 days after first fill of popup
     *
     * @param OutputInterface $output
     * @throws Exception
     */
    private function handlePopupStage(OutputInterface $output)
    {
        $prospects = $this->getProspectsAtPopupStage();
        $nbProspects = count($prospects);

        $output->writeln("\nThere are $nbProspects prospects at popup stage");

        $now = new DateTime();

        foreach ($prospects as $prospect) {
            $updatedAt = $prospect->getUpdatedAt();
            $diff = $now->diff($updatedAt);

            $diffFormat = "{$diff->d} days, {$diff->h} hours and {$diff->i} minutes ago";

            $output->writeln("- Prospect `{$prospect->getEmail()}` is in popup stage since {$diffFormat}");

            // We never sent the email after 30 minutes
            if ($prospect->getPopupRevival() === 0) {
                if ($diff->d === 0 && $diff->h === 0 && $diff->i >= 30) {
                    $output->writeln("\tSending popup revival 1 to {$prospect->getEmail()}");

                    if (!$this->sendPopupRevival1($prospect)) {
                        $output->writeln("\t\tEmail already sent");
                    }
                }
            }

            // We sent the first email after 30 minutes, now let's send the email after 1 day and same hour
            elseif ($prospect->getPopupRevival() === 1) {
                if ($diff->d === 1 && $diff->h === 0) {
                    $output->writeln("\tSending popup revival 2 to {$prospect->getEmail()}");

                    if (!$this->sendPopupRevival2($prospect)) {
                        $output->writeln("\t\tEmail already sent");
                    }
                }
            }

            // We sent the first email after 1 day, now let's send the email after 7 days and same hour
            elseif ($prospect->getPopupRevival() === 2) {
                if ($diff->d === 7 && $diff->h === 0) {
                    $output->writeln("\tSending popup revival 3 to {$prospect->getEmail()}");

                    if (!$this->sendPopupRevival3($prospect)) {
                        $output->writeln("\t\tEmail already sent");
                    }
                }
            }
        }
    }

    /**
     * @param OutputInterface $output
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function handleCallbackStage(OutputInterface $output)
    {
        $prospects = $this->getProspectsAtCallbackStage();
        $nbProspects = count($prospects);

        $output->writeln("\nThere are $nbProspects prospects at callback stage");

        $now = new DateTime();

        foreach ($prospects as $prospect) {
            $updatedAt = $prospect->getUpdatedAt();
            $diff = $now->diff($updatedAt);

            $diffFormat = "{$diff->d} days, {$diff->h} hours and {$diff->i} minutes ago";

            $output->writeln("- Prospect `{$prospect->getEmail()}` is in callback stage since {$diffFormat}");

            if ($prospect->getCallbackRevival() === 0) {
                if ($diff->d === 3) {
                    $output->writeln("\tSending callback revival 1 to {$prospect->getEmail()}");

                    if (!$this->sendCallbackRevival1($prospect)) {
                        $output->writeln("\t\tEmail already sent");
                    }
                }
            }

            elseif ($prospect->getCallbackRevival() === 1) {
                if ($diff->d === 7) {
                    $output->writeln("\tSending callback revival 2 to {$prospect->getEmail()}");

                    if (!$this->sendCallbackRevival2($prospect)) {
                        $output->writeln("\t\tEmail already sent");
                    }
                }
            }
        }
    }

    /**
     * This method will send email to prospects that have started to fill the form, but did not complete it
     *
     * Cases:
     *    - 1 day after last attempt of filling it
     *    - 3 days after, we reset everything so that the prospect can go back to either popup revival or callback revival
     *      we also reset the registration step to 1
     *
     * @param OutputInterface $output
     * @throws Exception
     */
    private function handleSubscriptionStage(OutputInterface $output)
    {
        $prospects = $this->getProspectsInSubsriptionProcess();
        $nbProspects = count($prospects);

        $output->writeln("\nThere are $nbProspects prospects at subscription stage");

        $now = new DateTime();

        foreach ($prospects as $prospect) {
            $updatedAt = $prospect->getUpdatedAt();
            $diff = $now->diff($updatedAt);

            $diffFormat = "{$diff->d} days, {$diff->h} hours and {$diff->i} minutes ago";

            $output->writeln("- Prospect `{$prospect->getEmail()}` has started subscription process {$diffFormat}");

            if ($prospect->getSubscriptionRevival() === 0) {
                if ($diff->d === 1) {
                    $output->writeln("\tSending registration revival 1 to {$prospect->getEmail()}");

                    $this->sendRegistrationRevival($prospect);
                }
            }

            elseif ($diff->d === 3) {
                $output->writeln("\tResetting registration revival to defaults for prospect {$prospect->getEmail()}");

                if ($this->isDebug) {
                    continue ;
                }

                $prospect->setRegistrationStep(1);
                $prospect->setPopupRevival(0);
                $prospect->setCallbackRevival(0);
                $prospect->setSubscriptionRevival(0);
                $prospect->setUpdatedAt(new DateTime());

                $this->prospectManager->flush($prospect);
            }
        }
    }

    private function handleSubscriptionStage2(OutputInterface $output)
    {
        /** @var ProspectRepository $prospectRepository */
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');
        /** @var Prospect[] $prospects */
        $prospects = $prospectRepository->createQueryBuilder('p')
            ->where('p.enabled = 0')
            ->andWhere("COALESCE(p.hash, '') != ''")
            ->andWhere("p.updatedAt >= '2019-09-07'")
            ->getQuery()
            ->getResult()
        ;

        $nbProspects = count($prospects);

        $output->writeln("There are $nbProspects prospects at subscription stage");

        $now = new DateTime();

        foreach ($prospects as $prospect) {
            $updatedAt = $prospect->getUpdatedAt();
            $diff = $now->diff($updatedAt);

            $diffFormat = "{$diff->d} days, {$diff->h} hours and {$diff->i} minutes ago";

            $output->writeln("- Prospect `{$prospect->getEmail()}` has started subscription process {$diffFormat}");

            if ($prospect->getSubscriptionRevival() === 0) {
                if ($diff->d === 2) {
                    $output->writeln("\tSending registration revival 1 to {$prospect->getEmail()}");

                    if (!$this->isDebug) {
                        $email = $this->getContainer()->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE)
                            ->addTo($prospect->getEmail())
                            ->setTemplateId(45)
                            ->addVariable('FIRSTNAME', $prospect->getFirstName())
                            ->addVariable('VERIFY_URL', $this->getContainer()->get('router')->generate('app_front_finalize_registration_page', ['token' => $prospect->getHash()], UrlGeneratorInterface::ABSOLUTE_URL))
                            ->build()
                        ;

                        $email->setCreatedAt($now);
                        $email->setUpdatedAt($now);

                        $this->getContainer()->get('app.sendinblue.manager')->addEmailToQueue($email);

                        $prospect->setSubscriptionRevival(1);

                        $this->getContainer()->get('doctrine.orm.entity_manager')->flush($prospect);
                    }
                }
            }
        }
    }

    /**
     * Gets the prospects that are just at popup stage (step1 in form and enabled = 0 due to new system)
     * Those are the one that just put their email and restaurant name in the first popup
     *
     * @return Prospect[]
     */
    private function getProspectsAtPopupStage()
    {
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');
        $qb = $prospectRepository->createQueryBuilder('p');
        /** @var Prospect[] $prospects */
        $prospects = $qb
            ->andWhere('p.registrationStep = 1')
            ->andWhere('p.enabled = 0')
            ->andWhere('p.popupRevival != 3')
            ->andWhere("COALESCE(p.phone, '') = ''")
            ->andWhere("COALESCE(p.mobile, '') = ''")
            ->andWhere("p.updatedAt >= '2019-05-22'")
            ->getQuery()
            ->getResult()
        ;

        return $prospects;
    }

    /**
     * Gets the prospects that are just at callback stage (step1 in form and enabled = 0 due to new system)
     * Those are the one that just put their email and restaurant name in the first popup + their phone
     *
     * @return Prospect[]
     */
    private function getProspectsAtCallbackStage()
    {
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');
        $qb = $prospectRepository->createQueryBuilder('p');
        /** @var Prospect[] $prospects */
        $prospects = $qb
            ->andWhere('p.registrationStep = 1')
            ->andWhere('p.enabled = 0')
            ->andWhere('p.callbackRevival != 2')
            ->andWhere($qb->expr()->orX("COALESCE(p.phone, '') != ''", "COALESCE(p.mobile, '') != ''"))
            ->andWhere("p.updatedAt >= '2019-05-22'")
            ->getQuery()
            ->getResult()
        ;

        return $prospects;
    }

    /**
     * Gets the prospects that are in the subscription process, but did not complete it yet (stage 2 to 4)
     *
     * @return Prospect[]
     */
    private function getProspectsInSubsriptionProcess()
    {
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');
        $qb = $prospectRepository->createQueryBuilder('p');
        /** @var Prospect[] $prospects */
        $prospects = $qb
            ->andWhere('p.registrationStep BETWEEN 2 AND 4')
            ->andWhere('p.enabled = 0')
            ->getQuery()
            ->getResult()
        ;

        return $prospects;
    }

    /**
     * @param Prospect $prospect
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendPopupRevival1(Prospect $prospect)
    {
        return $this->sendPopupRevival($prospect, 32, 1);
    }

    /**
     * @param Prospect $prospect
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendPopupRevival2(Prospect $prospect)
    {
        return $this->sendPopupRevival($prospect, 33, 2);
    }

    /**
     * @param Prospect $prospect
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendPopupRevival3(Prospect $prospect)
    {
        return $this->sendPopupRevival($prospect, 34, 3);
    }

    /**
     * @param Prospect $prospect
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendCallbackRevival1(Prospect $prospect)
    {
        return $this->sendCallbackRevival($prospect, 36, 1);
    }

    /**
     * @param Prospect $prospect
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendCallbackRevival2(Prospect $prospect)
    {
        return $this->sendCallbackRevival($prospect, 37, 2);
    }

    /**
     * @param Prospect $prospect
     * @param int $templateId
     * @return bool
     */
    private function isEmailSent(Prospect $prospect, int $templateId)
    {
        $qb = $this->emailRepository->createQueryBuilder('e');

        return !empty($qb
            ->where("e.templateId = $templateId")
            ->andWhere($qb->expr()->like('e.to', ':to'))
            ->setParameter('to', "%{$prospect->getEmail()}%")
            ->getQuery()
            ->getResult()
        );
    }

    /**
     * @param Prospect $prospect
     * @param int $templateId
     * @param int $popupRevival
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendPopupRevival(Prospect $prospect, int $templateId, int $popupRevival)
    {
        if ($this->isDebug) {
            return true;
        }

        if ($this->isEmailSent($prospect, $templateId)) {
            $prospect->setPopupRevival($popupRevival);
            $this->prospectManager->flush($prospect);

            return false;
        }

        $builder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

        $builder
            ->setTemplateId($templateId)
            ->addTo($prospect->getEmail())
            ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
        ;

        $email = $builder->build();

        $now = new DateTime();

        // We deactivated the translatable listener so let's put it like this
        $email->setCreatedAt($now);
        $email->setUpdatedAt($now);

        $this->mailer->addEmailToQueue($email);

        $prospect->setPopupRevival($popupRevival);

        $this->prospectManager->flush($prospect);

        return true;
    }

    /**
     * @param Prospect $prospect
     * @param int $templateId
     * @param int $callbackRevival
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendCallbackRevival(Prospect $prospect, int $templateId, int $callbackRevival)
    {
        if ($this->isDebug) {
            return true;
        }

        if ($this->isEmailSent($prospect, $templateId)) {
            $prospect->setCallbackRevival($callbackRevival);
            $this->prospectManager->flush($prospect);

            return false;
        }

        $builder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

        $builder
            ->setTemplateId($templateId)
            ->addTo($prospect->getEmail())
            ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
        ;

        $email = $builder->build();

        $now = new DateTime();

        // We deactivated the translatable listener so let's put it like this
        $email->setCreatedAt($now);
        $email->setUpdatedAt($now);

        $this->mailer->addEmailToQueue($email);

        $prospect->setCallbackRevival($callbackRevival);

        $this->prospectManager->flush($prospect);

        return true;
    }

    /**
     * @param Prospect $prospect
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function sendRegistrationRevival(Prospect $prospect)
    {
        if ($this->isDebug) {
            return true;
        }

        if ($this->isEmailSent($prospect, 35)) {
            $prospect->setSubscriptionRevival(1);
            $this->prospectManager->flush($prospect);

            return false;
        }

        $builder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

        $builder
            ->setTemplateId(35)
            ->addTo($prospect->getEmail())
            ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
            ->addVariable('URL', $this->getSubscriptionPageUrl($prospect))
        ;

        $email = $builder->build();

        $now = new DateTime();

        // We deactivated the translatable listener so let's put it like this
        $email->setCreatedAt($now);
        $email->setUpdatedAt($now);

        $this->mailer->addEmailToQueue($email);

        $prospect->setSubscriptionRevival(1);

        $this->prospectManager->flush($prospect);

        return true;
    }

    /**
     * Generates the subscription link for a prospect
     * If he has a hash to resume the process, then we add it to the url
     *
     * @param Prospect $prospect
     * @return string
     */
    private function getSubscriptionPageUrl(Prospect $prospect)
    {
        if (!empty($prospect->getHash())) {
            return $this->router->generate('app_front_subscription_page', ['h' => $prospect->getHash()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        return $this->router->generate('app_front_subscription_page', [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * We don't want to update the field updatedAt cause we are working on it !
     * But we also need to update the prospect, so let's disable this listener
     */
    private function disableTimestampable()
    {
        $eventManager = $this->prospectManager->getEventManager();

        foreach ($eventManager->getListeners('onFlush') as $listener) {
            if ($listener instanceof TimestampableListener) {
                $eventManager->removeEventSubscriber($listener);
                break;
            }
        }
    }
}