<?php


namespace AppBundle\Command;


use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\SellAndSign\SellAndSignException;
use AppBundle\Services\SellAndSign\SellAndSignService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestSellAndSignCommand extends ContainerAwareCommand
{
    /** @var SellAndSignService */
    private $sellAndSignService;

    /** @var ProspectRepository */
    private $prospectRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    const DEFAULT_CUSTOMER_NUMBER = 'MTU3MTAxLjE1NTYwMjQ1NzkwNjcuNjYyOTM=';

    protected function configure()
    {
        $this
            ->setName('app:test:sell-and-sign')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sellAndSignService = $this->getContainer()->get('app.service.sell_and_sign');
        $this->prospectRepository = $this->getContainer()->get('app.repository.prospect');
        $this->supplierRepository = $this->getContainer()->get('app.repository.supplier');
        /** @var Prospect $prospect */
        $prospect = $this->prospectRepository->findOneBy(['email' => 'jeremy@foodomarket.com']);
        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find(11);

        $output->writeln("Testing SellAndSign services");

//        $this->testTransaction($output, $prospect, $supplier);
        $this->testSingleStep($output, $prospect, $supplier);
    }

    private function testSendCommandPacket(OutputInterface $output, Prospect $prospect, Supplier $supplier)
    {
        $output->write("\tTest: Prepare Contract Single Step... ");

        $callable = function() use($prospect, $supplier) {
            return $this->sellAndSignService->sendCommandPacket($prospect, $supplier);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testSingleStep(OutputInterface $output, Prospect $prospect, Supplier $supplier)
    {
        $ret = $this->testSendCommandPacket($output, $prospect, $supplier);

        if ($ret) {
            $contractorsAbout = $this->testGetContractorsAbout($output, $ret['contract_id']);

            if ($contractorsAbout) {
                $temporaryToken = $this->testCreateTemporaryToken($output, $ret['customer_number'], $contractorsAbout['elements'][0]['contractId'], $contractorsAbout['elements'][0]['contractorId']);

                if ($temporaryToken) {
                    $urls = $this->sellAndSignService->getSigningUrls($contractorsAbout['elements'][0]['contractId'], $ret['customer_number'], $temporaryToken['token']['token']);

                    $output->writeln("Signing url (iframe) : {$urls['iframe']}");
                    $output->writeln("Signing url (page)   : {$urls['page']}");
                }
            }
        }
    }

    private function testGetContractorsAbout(OutputInterface $output, int $contractId)
    {
        $output->write("\tTest: Get Contractors About... ");

        $callable = function() use($contractId) {
            return $this->sellAndSignService->getContractorsAbout($contractId);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testTransaction(OutputInterface $output, Prospect $prospect, Supplier $supplier)
    {
        $this->testCreateCustomer($output, $prospect);
        $customer = $this->testGetCustomer($output, $prospect);

        if ($customer) {
            $contractor = $this->testGetOrCreateContractor($output, $customer);

            if ($contractor) {
                $contract = $this->testCreateContract($output, $customer['number'], $prospect, $supplier);

                if ($contract) {
                    $uploadContract = $this->testUploadContract($output, $contract['id'], $supplier);

                    if ($uploadContract) {
                        $addContractor = $this->testAddContractorTo($output, $contract['id'], $contractor['id']);

                        if ($addContractor) {
                            $changeSignatureMode = $this->testChangeSignatureMode($output, $addContractor['id']);

                            if ($changeSignatureMode) {
                                $setIban = $this->testSetIban($output, $contract['id'], 'FR1330066335657275626478952');

                                if ($setIban) {
                                    $contractReady = $this->testContractReady($output, $contract['id']);

                                    if ($contractReady) {
                                        $temporaryToken = $this->testCreateTemporaryToken($output, $customer['number'], $contract['id'], $contractor['id']);

                                        if ($temporaryToken) {
                                            $urls = $this->sellAndSignService->getSigningUrls($contract['id'], $customer['number'], $temporaryToken['token']['token']);

                                            $output->writeln("Signing url (iframe) : {$urls['iframe']}");
                                            $output->writeln("Signing url (page)   : {$urls['page']}");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function testGetCustomer(OutputInterface $output, Prospect $prospect)
    {
        $output->write("\tTest: Get Customer... ");

        $callable = function() use($prospect) {
            return $this->sellAndSignService->getCustomer("P{$prospect->getId()}");
        };

        return $this->handleResponse($callable, $output);
    }

    private function testCreateCustomer(OutputInterface $output, Prospect $prospect)
    {
        $output->write("\tTest: Create Customer... ");

        $callable = function() use($prospect) {
            return $this->sellAndSignService->createOrUpdateCustomer($prospect);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testGetOrCreateContractor(OutputInterface $output, array $customer)
    {
        $output->write("\tTest: Create Contractor... ");

        $callable = function() use($customer) {
            return $this->sellAndSignService->getOrCreateContractor($customer);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testCreateContract(OutputInterface $output, string $customerNumber, Prospect $prospect, Supplier $supplier)
    {
        $output->write("\tTest: Create Contract... ");

        $callable = function() use($customerNumber, $prospect, $supplier) {
            return $this->sellAndSignService->createContract($customerNumber, $prospect, $supplier);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testUploadContract(OutputInterface $output, $contractId, Supplier $supplier)
    {
        $output->write("\tTest: Upload Contract... ");

        $callable = function() use($contractId, $supplier) {
            return $this->sellAndSignService->uploadContract($contractId, $supplier);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testAddContractorTo(OutputInterface $output, $contractId, $contractorId)
    {
        $output->write("\tTest: Add Contractor to contract... ");

        $callable = function() use($contractId, $contractorId) {
            return $this->sellAndSignService->addContractorTo($contractId, $contractorId);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testChangeSignatureMode(OutputInterface $output, $contractorId)
    {
        $output->write("\tTest: Change signature mode... ");

        $callable = function() use($contractorId) {
            return $this->sellAndSignService->changeSignatureMode($contractorId);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testSetIban(OutputInterface $output, $contractId, string $iban)
    {
        $output->write("\tTest: Set Iban to contract... ");

        $callable = function() use($contractId, $iban) {
            return $this->sellAndSignService->setIbanOnContract($contractId, $iban);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testContractReady(OutputInterface $output, $contractId)
    {
        $output->write("\tTest: Contract ready... ");

        $callable = function() use($contractId) {
            return $this->sellAndSignService->contractReady($contractId);
        };

        return $this->handleResponse($callable, $output);
    }

    private function testCreateTemporaryToken(OutputInterface $output, $customerId, $contractId, $contractorId)
    {
        $output->write("\tTest: Create Temporary Token... ");

        $callable = function() use($customerId, $contractId, $contractorId) {
            return $this->sellAndSignService->createTemporaryToken($customerId, $contractId, $contractorId);
        };

        return $this->handleResponse($callable, $output);
    }

    /**
     * @param callable $callable
     * @param OutputInterface $output
     * @return array|null|false
     */
    private function handleResponse(callable $callable, OutputInterface $output)
    {
        try {
            $response = $callable();
            $responseString = \GuzzleHttp\json_encode($response);
            $output->writeln("OK => [$responseString]\n");

            return $response;
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (SellAndSignException $e) {
            $output->writeln("KO => $e");

            return false;
        }
    }
}