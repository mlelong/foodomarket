<?php

namespace AppBundle\Command;

use AppBundle\Entity\BlockedPrice;
use AppBundle\Repository\BlockedPriceRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ProductVariant\VariantKnife;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ForwardBlockedPricesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:blocked-prices:forward')
            ->addOption('accept', null, InputOption::VALUE_NONE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository */
        $supplierProductVariantInformationsRepository = $this->get('app.repository.supplier_product_variant_informations');
        /** @var SupplierProductPriceRepository $supplierProductPriceRepository */
        $supplierProductPriceRepository = $this->get('app.repository.supplier_product_price');
        /** @var BlockedPriceRepository $blockedPriceRepository */
        $blockedPriceRepository = $this->get('app.repository.blocked_price');
        /** @var SupplierRepository $supplierRepository */
        $supplierRepository = $this->get('app.repository.supplier');
        /** @var VariantKnife $variantKnife */
        $variantKnife = $this->get('app.service.variant_knife');
        $accept = $input->getOption('accept');
        /** @var BlockedPrice[] $blockedPrices */
        $blockedPrices = $blockedPriceRepository->findAll();
        $nbBlockedPrices = count($blockedPrices);

        $output->writeln("There is $nbBlockedPrices blocked prices. Substitution mode " . ($accept ? 'On' : 'Off'));

        $manager = $this->get('doctrine.orm.entity_manager');

        foreach ($blockedPrices as $blockedPrice) {
            $partnerSupplier = $supplierRepository->getRestaurantPartnerForFacade(
                $blockedPrice->getRestaurant(),
                $blockedPrice->getSupplier()
            );

            if (!$partnerSupplier) {
                continue ;
            }

            $productName = "{$blockedPrice->getProductVariant()->getInternalName()}";
            $supplierName = $blockedPrice->getSupplier()->getDisplayName(true);
            $restaurantName = $blockedPrice->getRestaurant()->getName();

            $kgPrice = bcdiv($blockedPrice->getKgPrice(), 100, 2);
            $unitPrice = bcdiv($blockedPrice->getUnitPrice(), 100, 2);

            $replacement = $variantKnife->getReplacementForVariant($blockedPrice->getProductVariant(), $partnerSupplier, $blockedPrice->getOptionValues()->toArray());

            $output->writeln("$restaurantName a un prix bloqué pour $productName avec $supplierName: {$unitPrice}€/PC - {$kgPrice}€/KG...");

            if ($replacement === null) {
                $output->writeln("\tPas de produit de subsitution trouvé !");
            } elseif ($replacement->getId() != $blockedPrice->getProductVariant()->getId()) {
                $output->writeln("\tProduit de substitution => {$replacement->getInternalName()}");

                if ($accept) {
                    $spp = $supplierProductPriceRepository->getByBlockedPrice($blockedPrice);
                    $spvi = $supplierProductVariantInformationsRepository->getByPrice($spp);

                    $blockedPrice->setProductVariant($replacement);
                    $blockedPrice->setProduct($replacement->getProduct());

                    $spp->setProductVariant($replacement);
                    $spp->setProduct($replacement->getProduct());

                    $spvi->setProductVariant($replacement);
                    $spvi->setProduct($replacement->getProduct());

                    $manager->flush($blockedPrice);
                    $manager->flush($spp);
                    $manager->flush($spvi);
                }
            } else {
                $output->writeln("\tParfait");
            }
        }

        $output->writeln("Done");
    }

    private function get(string $service)
    {
        return $this->getContainer()->get($service);
    }
}