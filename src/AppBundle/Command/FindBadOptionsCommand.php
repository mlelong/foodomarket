<?php


namespace AppBundle\Command;


use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Repository\SupplierProductPriceRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindBadOptionsCommand extends Command
{
    public static $defaultName = 'app:find:bad-options';

    /**
     * @var SupplierProductPriceRepository
     */
    private $supplierProductPriceRepository;

    public function __construct(SupplierProductPriceRepository $supplierProductPriceRepository)
    {
        parent::__construct();
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
    }

    public function configure()
    {
        $this->addArgument('supplierId', InputArgument::REQUIRED, "ID supplier dans spp");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var SupplierProductPrice[] $prices */
        $prices = $this->supplierProductPriceRepository->findBy(['supplier' => $input->getArgument('supplierId')]);

        foreach ($prices as $price) {
            $variant = $price->getProductVariant();

            $salesUnit = $variant->getOptionSupplierSalesUnit();
            $orderUnit = $variant->getOptionOrderUnit();
            $unitQuantity = $variant->getOptionUnitQuantity();
            $unitContentQuantity = $variant->getOptionUnitContentQuantity();
            $unitContentMeasurement = $variant->getOptionUnitContentMeasurement();

            $missings = [];

            if (!$salesUnit) $missings[] = 'UM';
            if (!$orderUnit) $missings[] = 'UC';
            if (!$unitQuantity) $missings[] = 'UQ';
            if (!$unitContentQuantity) $missings[] = 'UCQ';
            if (!$unitContentMeasurement) $missings[] = 'UCM';

            if (!empty($missings)) {
                $output->writeln("Variant ({$variant->getId()}) {$variant->getFullDisplayName()} does not have: " . implode(', ', $missings));
            }
        }
    }
}
