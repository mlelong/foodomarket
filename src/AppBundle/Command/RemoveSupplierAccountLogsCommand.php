<?php


namespace AppBundle\Command;


use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Util\SupplierUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveSupplierAccountLogsCommand extends Command
{
    public static $defaultName = 'app:remove:supplier-account-logs';
    /**
     * @var SupplierRepository
     */
    private $supplierRepository;
    /**
     * @var SupplierAccountLogRepository
     */
    private $supplierAccountLogRepository;

    public function __construct(SupplierRepository $supplierRepository, SupplierAccountLogRepository $supplierAccountLogRepository)
    {
        parent::__construct();

        $this->supplierRepository = $supplierRepository;
        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
    }

    protected function configure()
    {
        $this->addArgument('supplierId', InputArgument::REQUIRED, "Partner supplier id !");
        $this->addOption('force', null,InputOption::VALUE_NONE, "Really do it ?");
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierId = $input->getArgument('supplierId');
        $force = $input->getOption('force');

        if (!$force) {
            $output->writeln("DEBUG MODE ONLY ! Pass --force to apply changes");
        }

        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($supplierId);

        $output->writeln("Supplier: {$supplier->getDisplayName(true)}");

        /** @var SupplierAccountLog[] $supplierAccountLogs */
        $supplierAccountLogs = $this->supplierAccountLogRepository->createQueryBuilder('o')
            ->join('o.supplierCategory', 'c')
            ->join('c.supplier', 's')
            ->where('s = :supplier')
            ->setParameter('supplier', $supplierId)
            ->getQuery()
            ->getResult()
        ;

        $nbLogs = count($supplierAccountLogs);

        $output->writeln("Found $nbLogs supplier account logs");
        $toRemove = 0;

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            $prospect = $supplierAccountLog->getProspect();

            if (SupplierUtil::getDeliveryCost($supplier, $prospect->getZipCode()) === null) {
                $output->writeln("Prospect {$prospect->getFirstName()} ({$prospect->getId()}) is in `{$prospect->getZipCode()}` but {$supplier->getDisplayName()} does not deliver there. Removing...");
                ++$toRemove;

                if ($force) {
                    $prospect->removeSupplier($supplierAccountLog->getSupplierCategory());

                    $this->supplierAccountLogRepository->remove($supplierAccountLog);
                }
            } else {
//                $output->writeln("Prospect {$prospect->getFirstName()} ({$prospect->getId()}) is in `{$prospect->getZipCode()}` : OK !");
            }
        }

        if ($force) {
            $output->writeln("\nDone ! $toRemove accounts have been removed");
        } else {
            $output->writeln("\nDone ! $toRemove accounts could be removed (--force)");
        }

        return 0;
    }
}