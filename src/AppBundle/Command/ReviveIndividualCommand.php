<?php


namespace AppBundle\Command;


use AppBundle\Entity\Customer;
use AppBundle\Entity\Email;
use AppBundle\Entity\Order;
use AppBundle\Entity\Prospect;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueV3Manager;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReviveIndividualCommand extends Command
{
    public static $defaultName = 'app:revive:individual';

    /**
     * @var ProspectRepository
     */
    private $prospectRepository;
    /**
     * @var EmailFactory
     */
    private $emailFactory;
    /**
     * @var SendInBlueV3Manager
     */
    private $sendInBlueV3Manager;

    public function __construct(ProspectRepository $prospectRepository, EmailFactory $emailFactory, SendInBlueV3Manager $sendInBlueV3Manager)
    {
        parent::__construct();
        $this->prospectRepository = $prospectRepository;
        $this->emailFactory = $emailFactory;
        $this->sendInBlueV3Manager = $sendInBlueV3Manager;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $now = new DateTime();

        $output->writeln("It is {$now->format('d/m/Y H:i:s')}");

        $this->sendJPlus1($output);
        $this->sendJPlus3($output);
        $this->sendJPlus7($output);
    }

    private function sendJPlus1(OutputInterface $output)
    {
        $start = new DateTime();
        $end = new DateTime();

        $end->modify('-1 day');
        $start->modify('-1 day');
        $start->modify('-1 hour');

        $output->writeln("Fetching prospects subscribed since {$start->format('d/m/Y H:i:s')} and {$end->format('d/m/Y H:i:s')}");

        // Récupération des particuliers qui n'ont jamais commandé ni recu l'email de relance
        // alors qu'ils se sont inscrits il y a 1 jour
        $individualsWhoHaveNeverOrderedSince1DayOfSubscription = $this->getProspects($start, $end, 55);
        $count = count($individualsWhoHaveNeverOrderedSince1DayOfSubscription);

        $output->writeln("$count individuals have not ordered since their subscription yesterday while not having received the email");

        foreach ($individualsWhoHaveNeverOrderedSince1DayOfSubscription as $prospect) {
            $this->sendRevive($prospect, 55);
        }
    }

    private function sendJPlus3(OutputInterface $output)
    {
        $start = new DateTime();
        $end = new DateTime();

        $end->modify('-3 day');
        $start->modify('-3 day');
        $start->modify('-1 hour');

        $output->writeln("Fetching prospects subscribed since {$start->format('d/m/Y H:i:s')} and {$end->format('d/m/Y H:i:s')}");

        // Récupération des particuliers qui n'ont jamais commandé ni recu l'email de relance
        // alors qu'ils se sont inscrits il y a 3 jours
        $individualsWhoHaveNeverOrderedSince3DaysOfSubscription = $this->getProspects($start, $end, 56);
        $count = count($individualsWhoHaveNeverOrderedSince3DaysOfSubscription);

        $output->writeln("$count individuals have not ordered since their subscription 3 days ago while not having received the revive email");

        foreach ($individualsWhoHaveNeverOrderedSince3DaysOfSubscription as $prospect) {
            $this->sendRevive($prospect, 56);
        }
    }

    private function sendJPlus7(OutputInterface $output)
    {
        $start = new DateTime();
        $end = new DateTime();

        $end->modify('-7 day');
        $start->modify('-7 day');
        $start->modify('-1 hour');

        $output->writeln("Fetching prospects subscribed since {$start->format('d/m/Y H:i:s')} and {$end->format('d/m/Y H:i:s')}");

        // Récupération des particuliers qui n'ont jamais commandé ni recu l'email de relance
        // alors qu'ils se sont inscrits il y a 7 jours
        $individualsWhoHaveNeverOrderedSince7DayOfSubscription = $this->getProspects($start, $end, 59);
        $count = count($individualsWhoHaveNeverOrderedSince7DayOfSubscription);

        $output->writeln("$count individuals have not ordered since their subscription 7 days ago while not having received the email");

        foreach ($individualsWhoHaveNeverOrderedSince7DayOfSubscription as $prospect) {
            $this->sendRevive($prospect, 59);
        }
    }

    /**
     * @param DateTime $start
     * @param DateTime $end
     * @param $templateId
     * @return Prospect[]
     */
    private function getProspects(DateTime $start, DateTime $end, $templateId)
    {
        $qb = $this->prospectRepository->createQueryBuilder('p');

        return $qb
            ->join(Customer::class, 'c', 'WITH', 'c.prospect = p')
            ->leftJoin(Order::class, 'o', 'WITH', 'o.customer = c')
            ->leftJoin(Email::class, 'e', 'WITH', "e.templateId = :template AND e.to LIKE CONCAT('%', p.email, '%')")
            ->where('p.type = :type')
            ->andWhere('p.createdAt BETWEEN :start AND :end')
            ->andWhere($qb->expr()->isNull('o'))
            ->andWhere($qb->expr()->isNull('e'))
            ->setParameter('type', Prospect::TYPE_INDIVIDUAL)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('template', $templateId)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Prospect $prospect
     * @param $templateId
     */
    private function sendRevive(Prospect $prospect, $templateId)
    {
        $email = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE)
            ->setTemplateId($templateId)
            ->addTo($prospect->getEmail())
            ->build()
        ;

//        $this->sendInBlueV3Manager->addEmailToQueue($email);
    }
}