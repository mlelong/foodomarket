<?php

namespace AppBundle\Command;

use AppBundle\Entity\SupplierProductPriceHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveDuplicatePricesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:removeduplicateprices')
            ->addArgument('supplier', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierProductPriceHistoryRepository = $this->getContainer()->get('app.repository.supplier_product_price_history');
//        $supplierProductPriceRepository = $this->getContainer()->get('app.repository.supplier_product_price');

        /** @var SupplierProductPriceHistory[] $allPrices */
        $allPrices = $supplierProductPriceHistoryRepository->findAll();

        $idsToRemove = [];

        foreach ($allPrices as $price) {
            /** @var SupplierProductPriceHistory[] $prices */
            $prices = $supplierProductPriceHistoryRepository->findBy(['supplier' => $price->getSupplier(), 'productVariant' => $price->getProductVariant(), 'createdAt' => $price->getCreatedAt()]);

            // Found duplicate prices for this supplier and productVariant on that date
            if (count($prices) > 1) {
                for ($i = 1; $i < count($prices); ++$i) {
//                    $output->writeln("Suppression de " . $prices[$i]->getProductVariant()->getId() . " - " . $prices[$i]->getSupplier()->getName() . " " . $prices[$i]->getCreatedAt()->format('yyyy-MM-dd'));
//                    $supplierProductPriceHistoryRepository->remove($prices[$i]);

                    $idsToRemove[] = $prices[$i]->getId();
                }
            }
        }

        $manager = $this->getContainer()->get('doctrine')->getEntityManager();

        $idsToRemove = join(',', $idsToRemove);

        $manager->getConnection()->exec("DELETE FROM sylius_supplier_product_price_history WHERE id IN ($idsToRemove)");
    }
}