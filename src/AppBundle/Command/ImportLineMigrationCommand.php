<?php

namespace AppBundle\Command;

use AppBundle\Entity\ImportLine;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportLineMigrationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:importline:migrate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = $this->getContainer()->get('doctrine')->getManager();
        /** @var ImportLine[] $lines */
        $lines = $this->getContainer()->get('app.repository.import_line')->findAll();

        $productRepository = $this->getContainer()->get('sylius.repository.product');

        foreach ($lines as &$line) {
            try {
                $content = unserialize($line->getContent());

                if (isset($content['productName'])) {
                    $line->setProductName($content['productName']);
                }

                if ($line->getProduct() === null && $line->getProductName() !== null) {
                    $product = $productRepository->findByName($line->getProductName(), 'fr_FR');

                    if (count($product) > 1)
                        die(var_dump($product));

                    if (!empty($product)) {
                        $product = $product[0];
                        $line->setProduct($product);
                    }
                }

                $line->setReference($content['reference']);
                $manager->persist($line);
            } catch (\Exception $ignored) {

            }
        }

        $manager->flush();
    }
}