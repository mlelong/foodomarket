<?php

namespace AppBundle\Command;

use AppBundle\Controller\OnboardingController;
use AppBundle\Entity\Onboarding;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OnboardingEmailCommand extends ContainerAwareCommand
{
    /** @var bool */
    private $isDebug;

    protected function configure()
    {
        $this
            ->setName('app:onboarding:email')
            ->addOption('displaydebug')
            ->setHelp('
            <info>Send new onboardings by mail</info>
            Example : php bin/console app:onboarding:email --displaydebug
            ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->isDebug = $input->getOption('displaydebug');

        $manager = $this->getDoctrine()->getManager();
        /** @var Onboarding[] $onboardings */
        $onboardings = $manager->getRepository(Onboarding::class)->findBy(['cron' => false]);

        $dateRef = '2018-02-06 13:07';

        foreach ($onboardings as &$onboarding) {
            $onboardingDate = $onboarding->getCreatedAt()->format('Y-m-d H:i');

            if ($onboardingDate < $dateRef) {
                continue ;
            }

            $total = $onboarding->getUserPrice();
            $min = $onboarding->getBestPrice();
            $max = $onboarding->getWorstPrice();

            $thresholdMin = $min - $min * OnboardingController::THRESHOLD_SEEMS_REAL / 100;
            $thresholdMax = $max + $max * OnboardingController::THRESHOLD_SEEMS_REAL / 100;

            // Sends onboarding with datas
            if ($onboarding->getContactEmail() !== null
                || $onboarding->getTelephone() !== null
                || $total !== null) {
                $this->sendMail($onboarding, $output);
            }

            if (!$this->isDebug) {
                $onboarding->setCron(true);
                $manager->flush();
            }
        }

        $output->writeln('Done');
    }

    private function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }

    /**
     * @param Onboarding $onboarding
     */
    private function sendMail(Onboarding $onboarding, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('swiftmailer.mailer.onboarding');

        $message = new \Swift_Message("Onboarding data seems good `{$onboarding->getId()}`");
        $message->setFrom('jeremy@foodomarket.com');
        $message->setTo(['hello@foodomarket.com']);
//        $message->setTo(['jeremy@foodomarket.com']);
        $message->setContentType('text/plain');

        $contentMessage = "Id onboarding : {$onboarding->getId()}" . PHP_EOL;
        $contentMessage .= "FRONT: https://www.foodomarket.com/onboarding/?onboardingId={$onboarding->getId()}" . PHP_EOL;
        $contentMessage .= "BACK OFFICE: https://www.foodomarket.com/admin/onboardings/{$onboarding->getId()}/show" . PHP_EOL. PHP_EOL;
        $contentMessage .= "Prix du client: {$onboarding->getUserPrice()}€" . PHP_EOL;

        if ($onboarding->getSupplierName() !== null) {
            $contentMessage .= "Fournisseur actuel: {$onboarding->getSupplierName()}" . PHP_EOL;
        }

        $contentMessage .= "Poste de dépense: {$onboarding->getTurnover()}€" . PHP_EOL . PHP_EOL;

        if ($onboarding->getPurchaseIndex() !== null) {
            $contentMessage .= "Indice d'achat (0 à 4): {$onboarding->getPurchaseIndex()}" . PHP_EOL;
        }

        if ($onboarding->getBestPrice() !== null) {
            $contentMessage .= "Meilleur prix possible: {$onboarding->getBestPrice()}€" . PHP_EOL;
        }

        if ($onboarding->getWorstPrice() !== null) {
            $contentMessage .= "Pire prix possible: {$onboarding->getWorstPrice()}€" . PHP_EOL;
        }

        if ($onboarding->getSuggestedSupplier() !== null && $onboarding->getSuggestedPrice() !== null) {
            $contentMessage .= "Fournisseur suggéré: {$onboarding->getSuggestedSupplier()->getName()} - {$onboarding->getSuggestedPrice()}€" . PHP_EOL;
        }

        $contentMessage .= PHP_EOL . PHP_EOL;

        if ($onboarding->getSupplierName() !== null) {
            $contentMessage .= "Fournisseur: {$onboarding->getSupplierName()}" . PHP_EOL;
        }

        if ($onboarding->getRestaurantName() !== null) {
            $contentMessage .= "Restaurant: {$onboarding->getRestaurantName()}" . PHP_EOL;
        }

        if ($onboarding->getAddress() !== null) {
            $contentMessage .= "Adresse du resto: {$onboarding->getAddress()}" . PHP_EOL;
        }

        if ($onboarding->getContactEmail() !== null) {
            $contentMessage .= "Email: {$onboarding->getContactEmail()}" . PHP_EOL;
        } else {
            if ($onboarding->getUtmEmail() !== null) {
                $contentMessage .= "Email: {$onboarding->getUtmEmail()}" . PHP_EOL;
            }
        }

        if ($onboarding->getTelephone() !== null) {
            $contentMessage .= "Téléphone: {$onboarding->getTelephone()}" . PHP_EOL;
        }

        $message->setBody($contentMessage);

        if (!$this->isDebug) {
            $mailer->send($message);
        } else {
            $output->writeln($contentMessage);
        }
    }
}