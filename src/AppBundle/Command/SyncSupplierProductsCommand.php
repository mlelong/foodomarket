<?php


namespace AppBundle\Command;


use AppBundle\Entity\Supplier;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncSupplierProductsCommand extends Command
{
    public static $defaultName = 'app:sync:supplier-products';

    /** @var ManagerRegistry */
    private $doctrine;

    /** @var Connection */
    private $connection;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addArgument('idSupplierSrc', InputArgument::REQUIRED)
            ->addArgument('idSupplierDst', InputArgument::REQUIRED)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $idSupplierSrc = $input->getArgument('idSupplierSrc');
        $idSupplierDst = $input->getArgument('idSupplierDst');

        $supplierRepository = $this->doctrine->getRepository(Supplier::class);

        /** @var Supplier $supplierSrc */
        $supplierSrc = $supplierRepository->find($idSupplierSrc);
        /** @var Supplier $supplierDst */
        $supplierDst = $supplierRepository->find($idSupplierDst);

        if ($supplierSrc === null || $supplierDst === null) {
            $output->writeln("Cannot find either source supplier or dst supplier");

            return -1;
        }

        /** @var Connection $connection */
        $this->connection = $this->doctrine->getConnection();

        $dstChannelCode = $supplierDst->getChannel()->getCode();

        try {
            $output->writeln("Copying product matching and price from supplier `{$supplierSrc->getDisplayName(true)} ($idSupplierSrc)` to supplier `{$supplierDst->getDisplayName(true)} ($idSupplierDst)`...");

            $this->connection->transactional(function (Connection $connection) use($idSupplierSrc, $idSupplierDst, $dstChannelCode) {
                // Nettoyage
                $connection->executeQuery("DELETE FROM sylius_supplier_product_price WHERE supplier_id = $idSupplierDst;");
                $connection->executeQuery("DELETE FROM sylius_supplier_product_informations WHERE supplier_id = $idSupplierDst;");
                $connection->executeQuery("DELETE FROM sylius_supplier_product_variant_informations WHERE supplier_id = $idSupplierDst;");

                // Recopie
                $connection->executeQuery("INSERT INTO sylius_supplier_product_price(supplier_id, restaurant_id, product_id, product_variant_id, kg_price, unit_price, channel_code, created_at, updated_at, mercuriale_id, valid_from, valid_to, promo) SELECT $idSupplierDst, restaurant_id, product_id, product_variant_id, kg_price, unit_price, :channelCode, created_at, updated_at, $idSupplierDst, valid_from, valid_to, promo FROM sylius_supplier_product_price WHERE supplier_id = $idSupplierSrc", ['channelCode' => $dstChannelCode]);
                $connection->executeQuery("INSERT INTO sylius_supplier_product_informations(supplier_id, product_id, label, reference, created_at, updated_at) SELECT $idSupplierDst, product_id, label, reference, created_at, updated_at FROM sylius_supplier_product_informations WHERE supplier_id = $idSupplierSrc");
                $connection->executeQuery("INSERT INTO sylius_supplier_product_variant_informations(supplier_id, product_id, product_variant_id, product_label, variant_label, reference, weight, content, sales_unit, manually_created, unit_quantity, display_name, created_at, updated_at, order_ref) SELECT $idSupplierDst, product_id, product_variant_id, product_label, variant_label, reference, weight, content, sales_unit, manually_created, unit_quantity, display_name, created_at, updated_at, order_ref FROM sylius_supplier_product_variant_informations WHERE supplier_id = $idSupplierSrc");
            });

            $output->writeln("Done");
        } catch (\Throwable $e) {
            $output->writeln($e->getMessage());

            return -1;
        }

        return 0;
    }
}
