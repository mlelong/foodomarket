<?php


namespace AppBundle\Command;


use AppBundle\Entity\Prospect;
use AppBundle\Repository\InvoiceRepository;
use AppBundle\Repository\OrderRepository;
use AppBundle\Services\Hubspot\HubspotService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HubspotFirstOrderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:hubspot:first-order')
            ->setDescription(<<<BOF
Cette commande va recupérer les prospects qui ont passé commande et les mettre dans la colonne hubspot adaptée.
Il vaut mieux faire ça dans une commande, car sinon lors du passage de commande si hubspot est pas réactif ça va nous brider voire pire bloquer la commande.
BOF
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->getContainer()->get('sylius.repository.order');
        /** @var InvoiceRepository $invoiceRepository */
        $invoiceRepository = $this->getContainer()->get('app.repository.invoice');
        /** @var HubspotService $hubspotService */
        $hubspotService = $this->getContainer()->get('app.services.hubspot');
        /** @var EntityManager $manager */
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var Prospect[] $orderProspects */
        $orderProspects = $orderRepository->createQueryBuilder('o')
            ->select('p')
            ->join('o.customer', 'c')
            ->join(Prospect::class, 'p', 'WITH', 'p = c.prospect')
            ->where('p.hubspotDealStage IS NULL')
            ->orWhere('p.hubspotDealStage != :stage')
            ->setParameter('stage', 'decisionmakerboughtin')
            ->getQuery()
            ->getResult()
        ;
        /** @var Prospect[] $invoiceProspects */
        $invoiceProspects = $invoiceRepository->createQueryBuilder('o')
            ->select('p')
            ->join(Prospect::class, 'p', 'WITH', 'p = o.prospect')
            ->where('p.hubspotDealStage IS NULL')
            ->orWhere('p.hubspotDealStage != :stage')
            ->setParameter('stage', 'decisionmakerboughtin')
            ->getQuery()
            ->getResult()
        ;

        /** @var Prospect[] $prospects */
        $prospects = [];

        foreach ($orderProspects as $prospect) {
            $prospects[$prospect->getId()] = $prospect;
        }

        foreach ($invoiceProspects as $prospect) {
            $prospects[$prospect->getId()] = $prospect;
        }

        $nbProspects = count($prospects);

        $output->writeln("Fetched $nbProspects prospects that have already ordered (invoice or shop or both)");

        foreach ($prospects as $prospect) {
            $output->write("Adding prospect `{$prospect->getEmail()}` to 'Première commande passée'\r");

            $hubspotService->createContactAndFirstOrderHoppy($prospect);

            $prospect->setHubspotDealStage('decisionmakerboughtin');

            $manager->flush($prospect);

            // In order not to burn hubspot api rate limit, let's wait 1 second between each call
            sleep(1);
        }

        $output->writeln("\nDone");
    }
}