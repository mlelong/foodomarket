<?php


namespace AppBundle\Command;


use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\ImportLineRepository;
use AppBundle\Repository\ImportRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ProductVariantUtils;
use AppBundle\Services\RedisCacheManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateReportMatchingCommand extends Command
{
    public static $defaultName = 'app:generate:matching-report';

    /** @var SupplierProductVariantInformationsRepository */
    private $spviRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var SupplierProductPriceRepository */
    private $priceRepository;

    /** @var RedisCacheManager */
    private $redisCacheManager;

    /** @var ImportLineRepository */
    private $importLineRepository;

    /** @var ImportRepository */
    private $importRepository;

    public function __construct(
        SupplierProductVariantInformationsRepository $spviRepository,
        SupplierRepository $supplierRepository,
        SupplierProductPriceRepository $priceRepository,
        RedisCacheManager $redisCacheManager,
        ImportLineRepository $importLineRepository,
        ImportRepository $importRepository
    ) {
        $this->spviRepository = $spviRepository;
        $this->supplierRepository = $supplierRepository;
        $this->priceRepository = $priceRepository;
        $this->redisCacheManager = $redisCacheManager;
        $this->importLineRepository = $importLineRepository;
        $this->importRepository = $importRepository;

        parent::__construct();
    }

    public function configure()
    {
        $this->addArgument('supplierId', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($input->getArgument('supplierId'));

        /** @var SupplierProductVariantInformations[] $spvis */
        $spvis = $this->spviRepository->findBy(['supplier' => $supplier]);
        /** @var Import[] $imports */
        $imports = $this->importRepository->findBy(['supplier' => $supplier], ['createdAt' =>'DESC'], 1);
        $import = !empty($imports) ? $imports[0] : null;
        $matching = [];
        $cache = [];
        $ilCache = [];

        $i = 0;
        $limit = PHP_INT_MAX - 1;

        foreach ($spvis as $spvi) {
            if (++$i > $limit) { break; }

            $variant = $spvi->getProductVariant();

            if (!isset($cache[$variant->getId()])) {
                $soldBy = [];

                /** @var Supplier[] $otherSuppliers */
                $otherSuppliers = $this->spviRepository->createQueryBuilder('spvi')
                    ->select('s')
                    ->join(Supplier::class, 's', 'WITH', 's = spvi.supplier')
                    ->andWhere('spvi.product = :product')
                    ->andWhere('spvi.productVariant = :variant')
                    ->andWhere('spvi.supplier != :supplier')
                    ->setParameter('product', $variant->getProduct())
                    ->setParameter('variant', $variant)
                    ->setParameter('supplier', $supplier)
                    ->getQuery()
                    ->getResult();

                foreach ($otherSuppliers as $otherSupplier) {
                    $soldBy[] = [
                        'id' => $supplier->getId(),
                        'name' => $otherSupplier->getDisplayName(true)
                    ];
                }

                $cache[$variant->getId()] = [
                    'weight' => $variant->getWeight(),
                    'name' => $variant->getFullDisplayName() . " ({$variant->getId()})",
                    'um' => $variant->getOptionSupplierSalesUnit() ? $variant->getOptionSupplierSalesUnit()->getValue() : '',
                    'uq' => $variant->getOptionUnitQuantity() ? floatval(str_replace(',', '.', $variant->getOptionUnitQuantity()->getValue())) : '',
                    'prices' => $this->priceRepository->getPrices([$supplier], [$variant->getProduct()], [$variant], [4]),
                    'soldBy' => $soldBy
                ];
            }

            $weight = $cache[$variant->getId()]['weight'];
            $um = $cache[$variant->getId()]['um'];
            $uq = $cache[$variant->getId()]['uq'];

            $hasWeightError = $weight !== $spvi->getWeight();
            $hasUmError = $um === 'COLIS' ? $spvi->getSalesUnit() !== 'CO' : $spvi->getSalesUnit() !== $um;
            $hasUqError = $uq !== floatval(str_replace(',', '.', $spvi->getUnitQuantity()));
            $hasMatchingError = $hasWeightError || $hasUmError || $hasUqError;
            $prices = $cache[$variant->getId()]['prices'];
            $price = !empty($prices) ? $prices[0] : null;

            $reasons = [];

            if (!$variant->isEnabled()) {
                $reasons[] = 'Variante désactivée';
            }

            if (!$variant->getProduct()->isEnabled()) {
                $reasons[] = 'Produit désactivé';
            }

            if ($price === null) {
                $reasons[] = 'Pas de prix';
            }

            if (!array_key_exists($spvi->getProductLabel(), $ilCache) && $import !== null) {
                $qb = $this->importLineRepository->createQueryBuilder('il');

                /** @var ImportLine $importLine */
                $importLine = $qb
                        ->join('il.import', 'i')
//                    ->andWhere('il.import = :import')
                        ->andWhere('i.supplier = :supplier')
                    ->andWhere('il.supplierName = :name')
//                    ->setParameter('import', $import)
                        ->setParameter('supplier', $supplier)
                    ->setParameter('name', $spvi->getProductLabel())
                    ->orderBy('il.createdAt', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();

                $ilCache[$spvi->getProductLabel()] = $importLine;
            }

            $importLine = $ilCache[$spvi->getProductLabel()];

            $matching[$variant->getId()][] = [
                'notOnlineReason' => $reasons,
                'name' => $variant->getFullDisplayName() . " ({$variant->getId()})",
                'content' => $variant->getExplicitContent(),
                'price' => $price ? ProductVariantUtils::getPriceDetailFromSupplierProductPrice($price) : [],
                'vWeight' => $weight,
                'matchingError' => $hasMatchingError,
                'um' => $um,
                'uq' => $uq,
                'ref' => $spvi->getReference(),
                'orderRef' => $spvi->getOrderRef(),
                'label' => $spvi->getProductLabel(),
                'weight' => $spvi->getWeight(),
                'weightError' => $hasWeightError,
                'unit' => $spvi->getSalesUnit(),
                'unitError' => $hasUmError,
                'quantity' => $spvi->getUnitQuantity(),
                'quantityError' => $hasUqError,
                'onSale' => $price !== null,
                'soldBy' => $cache[$variant->getId()]['soldBy'],
                'imported' => [
                    'ts' => $importLine ? $importLine->getUpdatedAt()->getTimestamp() : 0,
                    'at' => $importLine ? $importLine->getUpdatedAt()->format('d/m/Y H:i:s') : '',
                    'price' => $importLine ? $importLine->getPrice() : 0
                ]
            ];

            $maxTs = PHP_INT_MIN;

            foreach ($matching[$variant->getId()] as $match) {
                $maxTs = max($maxTs, $match['imported']['ts']);
            }

            foreach ($matching[$variant->getId()] as $k => $match) {
                $matching[$variant->getId()][$k]['imported']['latest'] = $maxTs === $match['imported']['ts'];
            }

            $matching[$variant->getId()][0]['matchingError'] |= $hasMatchingError;
        }

        usort($matching, function ($a, $b) {
            return strcasecmp($a[0]['name'], $b[0]['name']);
        });

        // Now set in cache
        $this->redisCacheManager->set(str_replace('{supplierId}', $supplier->getId(), RedisCacheManager::CACHE_KEY_ADMIN_PRODUCT_MATCHING_LIST), $matching);

        return 0;
    }
}