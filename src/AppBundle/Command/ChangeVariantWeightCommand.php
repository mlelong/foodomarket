<?php

namespace AppBundle\Command;

use AppBundle\Entity\EmailingStatistics;
use AppBundle\Repository\ProspectRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

class ChangeVariantWeightCommand extends ContainerAwareCommand
{
    use CommandTrait;

    /**
     * @var EntityManager|object
     */
    private $manager;

    protected function configure()
    {
        $this
            ->setName('app:product:change-variant-weight')
            ->addOption('product', null, InputOption::VALUE_OPTIONAL, 'Product to update')
            ->addOption('variant', null, InputOption::VALUE_OPTIONAL, 'Variant to update')
            ->addOption('weight', null, InputOption::VALUE_OPTIONAL, 'New weight')
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->addOption('force', null, InputOption::VALUE_NONE, 'Dont check weight rules')
            ->addOption('save', null, InputOption::VALUE_NONE, 'Save into BDD')
            ->setHelp(
            '
            <info>This command allow to change the weight of one/every variant of an product</info>
            
            Example : php bin/console app:product:change-variant-weight --product=7 --variant=1163 --weight=1.8 --save --force --displaydebug
            Example : php bin/console app:product:change-variant-weight --product=7 --weight=1.8 --displaydebug
            '
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->initializeDebug($output, $input->getOption('displaydebug'));

        /** @var EntityManager $manager */
        $this->manager = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        /** @var ProductRepository $productRepository */
        $this->productRepository = $this->getContainer()->get('sylius.repository.product');

        /** @var SupplierProductVariantInformations $supplierProductVariantInformationsRepository */
        $this->supplierProductVariantInformationsRepository = $this->getContainer()->get('app.repository.supplier_product_variant_informations');

        $productId = $input->getOption('product');
        $variantId = $input->getOption('variant');
        $weight = $input->getOption('weight');
        $force = $input->getOption('force');
        $save = $input->getOption('save');

        // get the product
        $this->info("Gettting product $productId ");
        $product = $this->productRepository->find($productId);
        $this->info("Product : ".$product->getName());
        foreach($product->getVariants() as $variant) {
            if (is_null($variantId) || ($variantId && $variant->getId() == $variantId)) {

                $this->info("\n--------------------------------------------------------------\nVariant : " . $variant->getId() . " | Weight : " . $variant->getWeight() . " | Order Unit : " . $variant->getOptionOrderUnit()->getCode());
                if (!$force) {
                    if (!in_array($variant->getOptionOrderUnit()->getCode(), ['ORDER_PC', 'ORDER_KGPC']) || $variant->getWeight() != 1) {
                        $this->info("I wont touch this variant, use --force to update the weight !");
                        continue;
                    }
                }
                $this->changeVariantWeight($variant, $weight, $force, $save);
            }
        }

        if ($save) {
            $this->manager->flush();
            $this->info("Changes saved");
        }

    }

    public function changeVariantWeight($variant, $weight, $force, $save) {

        $this->info("\n--------------------------------------------------------------\nWeight change for variant : ".$variant->getId());

        $this->info("-> Current order unit is : ".$variant->getOptionOrderUnit()->getCode());
        $this->info("-> Current weight is : ".$variant->getWeight());
        if ($save) {
            $variant->setWeight($weight);
            $this->info("-> New weight is : ".$variant->getWeight());
        }

        $spvi = $this->supplierProductVariantInformationsRepository->findBy([
            'product' => $variant->getProduct(),
            'productVariant' => $variant
        ]);

        foreach($spvi as $informations) {

            $this->info("\nSupplier variant informations change for variant : ".$variant->getId());
            $this->info(" -> Current weight is : ".$informations->getWeight());
            if ($save) {
                $informations->setWeight($weight);
                $this->info(" -> New weight is : ".$informations->getWeight());
            }
        }
    }
}