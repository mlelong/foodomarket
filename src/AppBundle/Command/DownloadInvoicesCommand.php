<?php

namespace AppBundle\Command;

use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadInvoicesCommand extends ContainerAwareCommand
{
    use CommandTrait;

    /** @var string */
    private $invoiceVarDir;

    /** @var string */
    private $invoiceTmpDir;

    /** @var bool */
    private $firstRun;

    /** @var bool */
    private $markAsRead;

    protected function configure()
    {
        $this
            ->setName('app:download:invoices')
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->addOption('first-run', null, InputOption::VALUE_NONE, 'Si présent, téléchargera toutes les pièces jointes de tous les messages')
            ->addOption('mark-as-read', null, InputOption::VALUE_NONE, 'Marque les messages en lus quand traités')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initializeDebug($output, $input->getOption('displaydebug'));
        $this->invoiceVarDir = $this->getContainer()->getParameter('kernel.project_dir') . '/var/invoice';
        $this->invoiceTmpDir = $this->invoiceVarDir . '/tmp';

        $this->firstRun = $input->getOption('first-run');
        $this->markAsRead = $input->getOption('mark-as-read');

        $this->downloadMessages();
    }

    private function downloadMessages()
    {
        $invoiceConnection = $this->getContainer()->get('secit.imap')->get('invoice_connection');

        $mailboxInfo = $invoiceConnection->getMailboxInfo();

        if ($this->firstRun) {
            $messages = $invoiceConnection->searchMailbox();
            $this->start($mailboxInfo->Nmsgs);
        } else {
            $messages = $invoiceConnection->searchMailbox('UNSEEN');
            $this->start($mailboxInfo->Unread);
        }

        $ret = [
            'nbMessages' => $mailboxInfo->Nmsgs,
            'nbUnread' => $mailboxInfo->Unread,
            'nbNewMessages' => 0,
            'nbAttachments' => 0
        ];

        $counter = 1;

        foreach ($messages as $messageId) {
            $this->advance("Handling message `{$messageId}` ($counter/{$ret['nbMessages']})");

            ++$counter;

            // Already treated
            if (is_dir("{$this->invoiceTmpDir}/{$messageId}")) {
                $this->info("Alredy treated, skipping...");
            }

            $ret['nbNewMessages']++;

            $mail = $invoiceConnection->getMail($messageId, false);

            // Avant de refaire le parseur de factures, on renvoyait les fichiers splittés sur la boite mail de facture
            // mais maintenant ca pose problème, alors on en veut plus !
            // Heureusement, c'est mickael qui splittait les factures et les renvoyait, donc on peut juste ne pas prendre
            // en considération ces emails là
            if ($mail->fromAddress === 'mickael@foodomarket.com') {
                $invoiceConnection->markMailAsRead($messageId);
                continue ;
            }

            $attachments = $mail->getAttachments();
            $isOk = true;

            if (!empty($attachments)) {
                foreach ($attachments as $attachment) {
                    $ret['nbAttachments']++;

                    try {
                        $this->moveFile($attachment->filePath, "{$this->invoiceTmpDir}/{$messageId}/{$attachment->name}");
                    } catch (Exception $e) {
                        $isOk = false;
                    }
                }
            }

            if (!$isOk) {
                $invoiceConnection->markMailAsUnread($messageId);
            } elseif ($this->markAsRead) {
                $invoiceConnection->markMailAsRead($messageId);
            }
        }

        $this->info("Done: {$ret['nbNewMessages']} new messages treated, {$ret['nbAttachments']} attachments downloaded");

        $this->finish();
    }

    /**
     * @param string $src
     * @param string $dst
     * @throws Exception
     */
    private function moveFile(string $src, string $dst)
    {
        $dstPath = dirname($dst);

        if (!is_dir($dstPath)) {
            if (!mkdir($dstPath, 0777, true)) {
                throw new Exception("Could not create directory `$dstPath`");
            }
        }

        if (!rename($src, $dst)) {
            throw new Exception("Could not move `$src` to `$dst`");
        }
    }
}