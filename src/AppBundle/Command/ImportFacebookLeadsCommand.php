<?php

namespace AppBundle\Command;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Email;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Services\Hubspot\HubspotService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use FacebookAds\Cursor;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdReportRunFields;
use FacebookAds\Object\Lead;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use FacebookAds\Api;

class ImportFacebookLeadsCommand extends ContainerAwareCommand
{
    const LEADGEN_FORM_ID = '258505061617992';
    const TOKEN = 'EAAC5GtyZCu6cBAL44mcZAKZAqrwBM4DfTEhiMPTSoreQu61wHN6e2OHojL1pfRSaHJ1nQ9tjgIHeRiM7NS1jPsQlCHxBfcJz8TZB6zjZCzqH1Bbq9lVvh1gat6afbR10PjqKZB3hG4QRuP1IneeUWwfNn8LEZA4ZCpOum1PSh6fvKbU3VPI8UNGg';

    protected function configure()
    {
        $this
            ->setName('app:import:facebook-leads')
            ->addOption('fromDate', 'from', InputOption::VALUE_REQUIRED)
            ->addOption('delay', 'delay', InputOption::VALUE_REQUIRED)
            ->setHelp('
            <info>Import the facebook leads into prospects table</info>
            Example : php bin/console app:import:facebook-leads --fromDate=[FROMDATE] --delay=[DELAY]
            ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $appId = $this->getContainer()->getParameter('facebook_app_id');
        $appSecret = $this->getContainer()->getParameter('facebook_app_secret');

        Api::init($appId, $appSecret, self::TOKEN);

        $ad = new Ad(self::LEADGEN_FORM_ID);

        if ($input->hasOption('fromDate') && $input->getOption('fromDate') !== null) {
            /** @var Cursor $leads */
            $leads = $ad->getLeads(array(), array(
                    AdReportRunFields::FILTERING => array(
                        array(
                            'field' => 'time_created',
                            'operator' => 'GREATER_THAN',
                            'value' => (new \DateTime($input->getOption('fromDate')))->getTimestamp(),
                        ),
                    ))
            );
        } else if ($input->getOption('delay') && $input->getOption('delay') !== null) {
            $delta = $input->getOption('delay');

            $dateStart = new \DateTime();
            $dateStart->modify("-$delta day");

            /** @var Cursor $leads */
            $leads = $ad->getLeads(array(), array(
                    AdReportRunFields::FILTERING => array(
                        array(
                            'field' => 'time_created',
                            'operator' => 'GREATER_THAN',
                            'value' => $dateStart->getTimestamp(),
                        ),
                    ))
            );
        } else {
            $leads = $ad->getLeads();
        }

        $leads->setUseImplicitFetch(true);

        $leadsArray = [];

        /** @var Lead $lead */
        foreach ($leads as $lead) {
            $data = $lead->exportAllData();
            $leadData = ['email' => '', 'nom_du_restaurant' => ''];

            foreach ($data['field_data'] as $fieldData) {
                if (isset($leadData[$fieldData['name']])) {
                    $leadData[$fieldData['name']] = implode(',', $fieldData['values']);
                }
            }

            $leadsArray[] = [
                'restaurant' => $leadData['nom_du_restaurant'],
                'email' => $leadData['email']
            ];
        }

        $result = [
            'existing' => 0,
            'new' => 0,
            'errors' => 0
        ];

        if (!empty($leadsArray)) {
            /** @var ProspectRepository $prospectRepository */
            $prospectRepository = $this->getContainer()->get('app.repository.prospect');
            /** @var EntityManager $manager */
            $manager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
            /** @var HubspotService $hubspotService */
            $hubspotService = $this->getContainer()->get('app.services.hubspot');

            foreach ($leadsArray as $lead) {
                $email = strtolower($lead['email']);
                $restaurantName = trim($lead['restaurant'], '"');

                $prospectFoodo = $prospectRepository->findOneBy(['email' => $email]);

                if ($prospectFoodo === null) {
                    $prospectFoodo = new Prospect();

                    $prospectFoodo->setEmail($email);
                    $prospectFoodo->setRestaurantName($restaurantName);
                    $prospectFoodo->setSource('facebook');
                    $prospectFoodo->setUtmCampaign('facebook_lead_form');
                    $prospectFoodo->setNewsletterPrices(true);

                    try {
                        $manager->persist($prospectFoodo);
                        $manager->flush();

                        // send welcome email
                        $sendinBlueManager = $this->getContainer()->get('app.sendinblue.manager');
                        $emailBuilder = $this->getContainer()->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);
                        $emailBuilder
                            ->setTemplateId(17)
                            ->addVariable('EMAIL', $prospectFoodo->getEmail())
                            ->addVariable('FIRSTNAME', $prospectFoodo->getRestaurantName())
                            ->addTo($prospectFoodo->getEmail(), $prospectFoodo->getRestaurantName())
                            ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
                        ;
// REMOVE - ERIC WILL DO IT MANUALLY OR NOT                        $sendinBlueManager->addEmailToQueue($emailBuilder->build());

                        ++$result['new'];
                    } catch (ORMException $e) {
                        ++$result['errors'];
                    }
                } else {
                    $prospectFoodo->setRestaurantName($restaurantName);

                    try {
                        $manager->flush();
                        ++$result['existing'];
                    } catch (ORMException $e) {
                        ++$result['errors'];
                    }
                }

//                $hubspotService->createContactAndSubscriptionHoppy($prospectFoodo);
            }
        }

        $output->writeln("New: {$result['new']}, Existing: {$result['existing']}, Errors: {$result['errors']}");
    }
}