<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use Doctrine\ORM\EntityManager;

class RepairSupplierInvoicesVariantReferenceCommand extends ContainerAwareCommand
{

    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('app:repair:supplier-invoices-variant-reference')
            ->addOption('supplier-id', 'sup', InputOption::VALUE_REQUIRED, 'Supplier Id (Facade)')
            ->addOption('item-id', 'item', InputOption::VALUE_REQUIRED, 'Item Id (invoice delivery note item id) to check')
            ->addOption('variant-id', 'variant', InputOption::VALUE_REQUIRED, 'Variant to use to repair')
            ->addOption('fix-anomalies', 'fix', InputOption::VALUE_NONE, 'Fix the variant id using the reference matching')
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp(
<<<EOF
This command's purpose is to check & correct the variant matched in delivery_note for supplier that manage references.
We can display or correct the anomalies.

php bin/console app:repair:supplier-invoices-variant-reference --supplier-id=27 --displaydebug --fix-anomalies

EOF
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $manager */
        $manager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        /** @var SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository */
        $supplierProductVariantInformationsRepository = $this->getContainer()->get('app.repository.supplier_product_variant_informations');
        /** @var InvoiceDeliveryNoteItemRepository $invoiceDeliveryNoteItemRepository */
        $invoiceDeliveryNoteItemRepository = $this->getContainer()->get('app.repository.invoice_delivery_note_item');
        /** @var variantRepository $variantRepository */
        $variantRepository = $this->getContainer()->get('app.repository.variant');
        /** @var $supplierRepository */
        $supplierRepository = $this->getContainer()->get('app.repository.supplier');

        $supplierId = $input->getOption('supplier-id');
        $itemId = $input->getOption('item-id');
        $variantId = $input->getOption('variant-id');
        $fixAnomalies = $input->getOption('fix-anomalies');
        $debug = $input->getOption('displaydebug');

        $this->initializeDebug($output, $debug);

        $this->info('');
        $this->info('');

        // Get delivery-note-item by supplier or item id

        $qb = $invoiceDeliveryNoteItemRepository->createQueryBuilder('delivery_note_item');

        if ($supplierId) {
            $this->info('Check invoice lines for supplier '.$supplierId);
            $qb
               ->join('delivery_note_item.invoiceDeliveryNote', 'delivery_note')
               ->join('delivery_note.invoice', 'invoice')
               ->andwhere('invoice.supplier = :supplier')
               ->setParameter('supplier', $supplierId);
        }

        if ($itemId) {
            $this->info('Check invoice lines for item '.$itemId);
            $qb->andwhere('delivery_note_item.id = :id')
                ->setParameter('id', $itemId);
        }

        $deliveryNoteItems = $qb
            ->orderBy('delivery_note_item.id', 'DESC')
            ->setMaxResults(8000)
            ->getQuery()
            ->getResult()
        ;

        $this->start(count($deliveryNoteItems));

        foreach($deliveryNoteItems as $item) {

            $variantId = 'NULL';
            if ($item->getVariant()) {
                $variantId = $item->getVariant()->getId();
            }

            $this->advance();

            $suppliers = [];
            $suppliersPartners = $supplierRepository->findBy(['facadeSupplier' => $item->getInvoiceDeliveryNote()->getInvoice()->getSupplier()->getId()]);
            foreach($suppliersPartners as $supplierPartner) {
                $suppliers[] = $supplierPartner->getId();
            }

            if ($item->getVariant()) {
                // get variant informations by variant id
                $qb = $supplierProductVariantInformationsRepository->createQueryBuilder('supplier_informations');
                $informations = $qb
                    ->select('variant.id AS variantId')
                    ->addSelect('supplier_informations.reference as reference')
                    ->addSelect('UPPER(supplier_informations.productLabel) as productLabel')
                    ->join('supplier_informations.productVariant', 'variant')
                    ->andWhere($qb->expr()->in('supplier_informations.supplier', $suppliers))
                    ->andwhere('supplier_informations.productVariant = :variantId')
                    ->setParameter('variantId', $item->getVariant()->getId())
                    ->getQuery()
                    ->getResult();

                $found = false;
                foreach ($informations as $information) {
                        if ($item->getReference() == $information['reference']) {
/*$this->info('-----------------------------------------------------------------------------');
$this->info('Invoice line : #' . $item->getId().' | Reference :'.$item->getReference().' | Label :'.$item->getName().' | Matched Variant Id :'.$variantId);
$this->info(' --> Variant found by id : #' . $information['variantId'] . ' Reference :' . $information['reference'] . ' Label :' . $information['productLabel'] . ' ');
*/
//$this->info(' --> OK');
                        continue 2;
                    } else {
                        $this->info('-----------------------------------------------------------------------------');
                        $this->info('Invoice line : #' . $item->getId().' | Reference :'.$item->getReference().' | Label :'.$item->getName().' | Matched Variant Id :'.$variantId);
                        $this->info(' --> Variant found by id : #' . $information['variantId'] . ' Reference :' . $information['reference'] . ' Label :' . $information['productLabel'] . ' ');
                        $this->info(' --> KO');
                    }
                }
            }

            // get reference informations by reference
            $qb = $supplierProductVariantInformationsRepository->createQueryBuilder('supplier_informations');
            $informations = $qb
                ->select('variant.id AS variantId')
                ->addSelect('supplier_informations.reference as reference')
                ->addSelect('UPPER(supplier_informations.productLabel) as productLabel')
                ->join('supplier_informations.productVariant', 'variant')
                ->andWhere($qb->expr()->in('supplier_informations.supplier', $suppliers))
                ->andwhere('supplier_informations.reference = :reference')
                ->setParameter('reference', $item->getReference())
                ->getQuery()
                ->getResult()
            ;

            $this->info('-----------------------------------------------------------------------------');
            $this->info('Invoice line : #' . $item->getId().' | Reference :'.$item->getReference().' | Label :'.$item->getName().' | Matched Variant Id :'.$variantId);

            if (count($informations) == 0) {
                $this->info(' --> Nothing found by reference');
            }

            foreach($informations as $information) {
                $this->info(' --> Variant found by reference : #' . $information['variantId'].' Reference :'.$information['reference'].' Label :'.$information['productLabel'].' ');
                if ($item->getVariant() && $item->getVariant()->getId() == $information['variantId']) {
                    $this->info(' --> OK');
                    continue 2;
                } else {
                    $this->info(' --> KO');

                    $variant = $variantRepository->find($information['variantId']);
                    $this->info(' --> I can fix the line with variant '.$variant->getId());

                    if ($fixAnomalies) {

                        $item->setVariant($variant);
                        $manager->flush();
                        $this->info(' --> I just fix the line with variant '.$variant->getId());

                    }
                }
            }
        }

        $this->finish();

    }
}