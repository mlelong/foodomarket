<?php

namespace AppBundle\Command;

use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\ImportLineRepository;
use AppBundle\Services\MercurialeParser\AbstractMercurialeParser;
use AppBundle\Services\MercurialeParser\DomafraisParser;
use Cocur\Slugify\Slugify;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MercurialeParserCommand extends ContainerAwareCommand
{
    /**
     * @var string[]
     */
    private $parsers = [
        39 => DomafraisParser::class
    ];

    protected function configure()
    {
        $this
            ->setName('app:mercuriale:parse')
            ->addArgument('pivotFile', InputArgument::REQUIRED, 'Path to a file to be parsed')
            ->addArgument('importId', InputArgument::REQUIRED, 'Import ID for which to update lines data')
        ;
    }

    /**
     * @param int $importId
     * @return Supplier
     */
    private function getSupplierFromImport(int $importId)
    {
        $importRepository = $this->getContainer()->get('app.repository.import');
        /** @var Import|null $import */
        $import = $importRepository->find($importId);

        return $import->getSupplier();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('pivotFile');
        $importId = $input->getArgument('importId');
        $supplier = $this->getSupplierFromImport($importId);

        if (!isset($this->parsers[$supplier->getId()])) {
            $output->writeln("Unsupported Supplier: `{$supplier->getName()}`");
            return ;
        }

        $parserClass = $this->parsers[$supplier->getId()];

        /** @var AbstractMercurialeParser $parser */
        $parser = new $parserClass($file);

        if (!$parser->parse()) {
            $output->writeln("Failed to parse file `$file`");
        }

        $products = $parser->getProducts();
        /** @var ImportLineRepository $importLineRepository */
        $importLineRepository = $this->getContainer()->get('app.repository.import_line');
        $importLineManager = $this->getContainer()->get('app.manager.import_line');

        foreach ($products as $product) {
            /** @var ImportLine|null $line */
            $line = $importLineRepository->findOneBy([
                'import' => $importId, 'supplierName' => $product['label'], 'status' => ImportLine::STATUS_AWAITING_CHECK
            ]);

            if ($line !== null) {
                if (empty($line->getLine())) {
                    $copy = $product;
                    unset($copy['_features']);

                    $line->setLine(implode(' | ', $copy));
                }

                $features = $product['_features'];

                $line->setConditioning($this->getOptionValueCodeByOptionValue('CO', $features['conditioning']));
                $line->setWeight($features['weight']);
                $line->setUnitQuantity(str_replace(',', '.', $features['uq']));
                $line->setUnitContentQuantity(str_replace(',', '.', $features['ucq']));
                $line->setUnitContentMeasurement($this->getOptionValueCodeByOptionValue('UCM', $features['ucm']));
                $line->setBrand($this->getOptionValueCodeByOptionValue('MQ', $features['brand']));
                $line->setOrigin($this->getOptionValueCodeByOptionValue('OV', $features['origin']));
                $line->setKgPrice(bcmul($features['kgPrice'], 100, 2));
                $line->setPriceVariant(bcmul($features['unitPrice'], 100, 2));
                $line->setPrice(bcmul($product['price'], 100, 2));
                $line->setSalesUnit($this->getOptionValueCodeByOptionValue('UM', $features['salesUnit']));
                $line->setOrderUnit($this->getOptionValueCodeByOptionValue('UC', $features['orderUnit']));

                $line->propertiesToContent();

                try {
                    $importLineManager->flush($line);
                } catch (\Exception $e) {
                    $output->writeln($e->getMessage());
                }
            }
        }
    }

    private function getOptionValueCodeByOptionValue(string $optionCode, string $optionValue)
    {
        if (empty($optionValue)) {
            return '';
        }

        static $staticCodes = [
            'UCM' => [
                'Kilogramme' => 'UCM_KG',
                'Gramme' => 'UCM_GR',
                'Litre' => 'UCM_L',
                'Centilitre' => 'UCM_CL',
                'Millilitre' => 'UCM_ML',
                'Piece' => 'UCM_PIECE',
                'Bouteille' => 'UCM_BOUTEILLE'
            ],
            'UM' => [
                'KG' => 'KG',
                'L' => 'L',
                'PC' => 'PC',
                'COLIS' => 'CO'
            ],
            'UC' => [
                'KG' => 'ORDER_KG',
                'L' => 'ORDER_L',
                'PC' => 'ORDER_PC',
                'COLIS' => 'ORDER_COLIS'
            ]
        ];

        if (isset($staticCodes[$optionCode][$optionValue])) {
            return $staticCodes[$optionCode][$optionValue];
        }

        $productOptionValueRepository = $this->getContainer()->get('sylius.repository.product_option_value');
        $qb = $productOptionValueRepository->createQueryBuilder('pov');

        /** @var ProductOptionValue[] $optionValues */
        $optionValues = $qb
            ->innerJoin('pov.option', 'po')
            ->innerJoin('pov.translations', 't')
            ->where('po.code = :option_code')
            ->setParameter('option_code', $optionCode)
            ->andWhere('t.value = :option_value')
            ->setParameter('option_value', "$optionValue")
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (!empty($optionValues)) {
            return $optionValues[0]->getCode();
        }

        $qb = $productOptionValueRepository->createQueryBuilder('pov');

        /** @var ProductOptionValue[] $optionValues */
        $optionValues = $qb
            ->innerJoin('pov.option', 'po')
            ->innerJoin('pov.translations', 't')
            ->where('po.code = :option_code')
            ->setParameter('option_code', $optionCode)
            ->andWhere('t.value LIKE :option_value')
            ->setParameter('option_value', "%$optionValue%")
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (!empty($optionValues)) {
            return $optionValues[0]->getCode();
        }

        try {
            // Create the option value then...
            $pov = new ProductOptionValue();
            $pov->setCurrentLocale('fr_FR');

            $optionValueCode = strtoupper((new Slugify())->slugify($optionValue));

            $pov->setValue($optionValue);
            $pov->setOption($this->getContainer()->get('sylius.repository.product_option')->findOneBy(['code' => $optionCode]));
            $pov->setCode("{$optionCode}_$optionValueCode");

            $productOptionValueRepository->add($pov);

            return $pov->getCode();
        } catch (\Exception $e) {
            return $optionValue;
        }
    }
}