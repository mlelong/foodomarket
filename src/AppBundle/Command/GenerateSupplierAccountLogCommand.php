<?php

namespace AppBundle\Command;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\ShopUser;
use AppBundle\Entity\SupplierAccountLog;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateSupplierAccountLogCommand
 * @package AppBundle\Command
 *
 * This command generates the supplier account log for each prospect if missing
 */
class GenerateSupplierAccountLogCommand extends ContainerAwareCommand
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('app:generate:supplier-account-log')
            ->addOption('prospect', 'p', InputOption::VALUE_REQUIRED)
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');
        $supplierAccountLogRepository = $this->getContainer()->get('app.repository.supplier_account_log');
        $restaurantRepository = $this->getContainer()->get('app.repository.restaurant');
        $manager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $customerRepository = $this->getContainer()->get('sylius.repository.customer');

        if ($input->hasOption('prospect') && ($prospectId = $input->getOption('prospect')) !== null) {
            $prospect = $prospectRepository->find($prospectId);

            if ($prospect === null) {
                $this->critical("Could not find prospect with id `$prospectId`");
                return ;
            }

            $prospects = [$prospect];
        } else {
            $prospects = $prospectRepository->findAll();
        }

        $this->initializeDebug($output, $input->getOption('displaydebug'));
        $this->start(count($prospects));

        /** @var Prospect $prospect */
        foreach ($prospects as $prospect) {
            $this->advance("Assigning account log(s) for prospect `{$prospect->getEmail()}`");

            foreach ($prospect->getSuppliers() as $supplierCategory) {
                $supplierAccountLog = $supplierAccountLogRepository->findOneBy(['prospect' => $prospect, 'supplierCategory' => $supplierCategory]);

                if ($supplierAccountLog === null) {
                    $supplierAccountLog = new SupplierAccountLog();

                    $supplierAccountLog->setProspect($prospect);
                    $supplierAccountLog->setSupplierCategory($supplierCategory);
                    $supplierAccountLog->setStatus(SupplierAccountLog::STATUS_ACCOUNT_OPENED);

                    $supplierAccountLogRepository->add($supplierAccountLog);

                    $this->info("Prospect `{$prospect->getEmail()}` has been assigned {$supplierCategory} ;)");
                } else {
                    $this->info("Prospect `{$prospect->getEmail()}` is OK ;)");
                }
            }

            // We can safely assume there is no restaurant for this prospect, so let's create it
            if ($prospect->getCustomers()->isEmpty()) {
                if (empty($prospect->getRestaurantName()) || empty($prospect->getEmail())
                    || empty($prospect->getFirstName()) || empty($prospect->getAddress())) {
                    continue ;
                }

                $restaurants = $restaurantRepository->findBy(['name' => $prospect->getRestaurantName()]);

                if (empty($restaurants)) {
                    $restaurant = new Restaurant();

                    $restaurant->setName($prospect->getRestaurantName());
                    $restaurant->setEnabled(true);
                    $restaurant->setEmail($prospect->getEmail());

                    if ($prospect->getDeliveryHours() !== null) {
                        $restaurant->setDeliveryHours($prospect->getDeliveryHours());
                    }

                    if ($prospect->getSiren() !== null) {
                        $restaurant->setSiren(substr(preg_replace('/[^ -~]/', '', $prospect->getSiren()), 0, 9));
                    }

                    $restaurant->setCountryCode('FR');
                    $restaurant->setInterlocutor(trim("{$prospect->getFirstName()} {$prospect->getContactName()}"));
                    $restaurant->setStreet($prospect->getAddress());
                    $restaurant->setCity('');
                    $restaurant->setPostcode('');

                    $restaurantRepository->add($restaurant);

                    $this->info("Restaurant {$restaurant->getName()} has been created for prospect {$prospect->getEmail()}");
                } else {
                    $this->info("Restaurant {$prospect->getRestaurantName()} already exists for prospect {$prospect->getEmail()}");
                    $restaurant = $restaurants[0];
                }

                /** @var Customer $customer */
                $customer = $customerRepository->findOneBy(['email' => $prospect->getEmail()]);

                if ($customer !== null) {
                    $customer->addRestaurant($restaurant);

                    $manager->persist($customer);

                    try {
                        $manager->flush();
                        continue ;
                    } catch (OptimisticLockException $e) {
                        $this->critical("Exiting: {$e->getMessage()}");
                        break ;
                    }
                }

                $customer = new Customer();
                $shopUser = new ShopUser();

                $customer->setProspect($prospect);
                $customer->setEmail($prospect->getEmail());
                $customer->setFirstName($prospect->getFirstName());
                $customer->setLastName($prospect->getContactName() !== null ? $prospect->getContactName() : '');
                /** @noinspection PhpParamsInspection */
                $customer->setGroup($this->getContainer()->get('sylius.repository.customer_group')->findOneBy(['code' => 'restaurant']));
                $customer->setPhoneNumber($prospect->getPhone());

                $customer->getRestaurants()->add($restaurant);

                $shopUser->setCustomer($customer);

                $shopUser->setEmail($prospect->getEmail());
                $shopUser->setEnabled(false);
                $shopUser->setPlainPassword('FoodoMarket');
                $shopUser->setUsername($prospect->getEmail());
                $shopUser->setPhone($prospect->getPhone());

                $customer->setShopUser($shopUser);

                $manager->persist($shopUser);
                $manager->persist($customer);

                try {
                    $manager->flush();
                } catch (OptimisticLockException $e) {
                    $this->critical("Exiting: {$e->getMessage()}");
                    break ;
                }
            }
        }

        $this->finish();
    }
}