<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\RedisCacheManager;

class PopulateProductStatisticsCommand extends ContainerAwareCommand
{

    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('app:cache:populate-product-statistics')
            ->addArgument('delay', InputArgument::OPTIONAL)
            ->addArgument('product', InputArgument::OPTIONAL)
            ->addArgument('taxon', InputArgument::OPTIONAL)
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp('
            <info>Populate the cache with product statistics / By default take products with less than 2 days prices updates</info>
            Example : php bin/console app:cache:populate-product-statistics [DELAY] [PRODUCT-ID] [TAXON] [--displaydebug]
            ');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initializeDebug($output, $input->getOption('displaydebug'));

        $doctrineManager = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $delay = $input->getArgument('delay');
        $productId = $input->getArgument('product');
        $taxon = $input->getArgument('taxon');
        $taxon = $this->getContainer()->get('sylius.repository.taxon')->findOneByCode($taxon);

        $currentPeriodEnd = new \DateTime();
        $currentPeriodBegin = null;

        if (is_null($delay)) {
            $delay = 2;
        }

        if (!$productId && !$taxon) {
            $currentPeriodEnd = new \DateTime();
            $currentPeriodBegin = new \DateTime();
            $currentPeriodBegin->modify('-'.$delay.' day');
            $this->info('Get the statistics for products update since '.$delay.' days');
        }

        $utilsProduct = $this->getContainer()->get('app.utils.product');

        $priceHistoryRepository = $this->getContainer()->get('app.repository.supplier_product_price_history');
        $qb = $priceHistoryRepository->createQueryBuilder('history')
                                     ->join('history.product', 'product')
                                     ;

        if ($productId) {
            $qb
                ->andwhere('product = :product')
                ->setParameter('product', $productId)
            ;
            $this->info('Get the statistics for product '.$productId);
        }

        if ($taxon) {
            $qb
                ->join('product.productTaxons', 'taxon')
                ->andwhere('taxon = :taxon')
                ->setParameter('taxon', $taxon)
            ;
            $this->info('Get the statistics for taxon '.$taxon->getCode());
        }

        if ($currentPeriodBegin) {
            $qb
                ->andWhere('history.createdAt BETWEEN :date_start AND :date_end')
                ->setParameter('date_start', $currentPeriodBegin)
                ->setParameter('date_end', $currentPeriodEnd);
        }

        $lastUpdatedProductsQuery = $qb
            ->select('product.id, product.code')
            //->setMaxResults(1000)
            ->distinct()
            ->getQuery();

        $lastUpdatedProducts = $lastUpdatedProductsQuery->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        $this->start(count($lastUpdatedProducts));

        $i=0;
        foreach ($lastUpdatedProducts as $line) {
            $i++;
            $this->advance('Product : #' . $line['id'].' '.$line['code']);
            $utilsProduct->getProductStatistics($line['id'], null, true, false);

            if ($i%40 == 0) {
                $this->advance('Flush & Clear');
                $doctrineManager->flush();
                $doctrineManager->clear();
            }
        }

        $doctrineManager->flush();

        $this->finish();
    }
}