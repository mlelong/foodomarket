<?php


namespace AppBundle\Command;


use AppBundle\Entity\Email;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Helper\CurlHelper;
use AppBundle\Services\SellAndSign\SellAndSignException;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ForwardSignedSepaCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:forward:signed-sepa')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sellAndSignService = $this->getContainer()->get('app.service.sell_and_sign');
        $supplierAccountLogs = $this->getContainer()->get('app.repository.supplier_account_log')->createQueryBuilder('sal')
            ->where('sal.signed = :signed')
            ->andWhere('sal.status IN (:statuses)')
            ->andWhere('sal.updatedAt >= :date')
            ->setParameters([
                'signed' => true,
                'statuses' => [SupplierAccountLog::STATUS_WAITING, SupplierAccountLog::STATUS_ORDERING],
                'date' => new DateTime('2019-12-20')
            ])
            ->getQuery()
            ->getResult()
        ;

        $sendinBlueManager = $this->getContainer()->get('app.sendinblue.manager');
        $emailRepository = $this->getContainer()->get('app.repository.email');
        $emailFactory = $this->getContainer()->get('app.factory.email');
        $uploaderHelper = $this->getContainer()->get('vich_uploader.templating.helper.uploader_helper');
        $uploadHandler = $this->getContainer()->get('vich_uploader.upload_handler');
        $handle = fopen($this->getContainer()->getParameter('kernel.project_dir') . '/var/logs/account-logs-signed-but-waiting.csv', 'w');
        $baseUrl = $this->getContainer()->getParameter('router.request_context.full_url');

        $realAccountLogs = [];

        fputcsv($handle, ['Prospect', 'Fournisseur', 'Date de signature du contrat']);

        /** @var SupplierAccountLog $supplierAccountLog */
        foreach ($supplierAccountLogs as $supplierAccountLog) {
            $realAccountLogs["{$supplierAccountLog->getProspect()->getId()} - {$supplierAccountLog->getSupplierCategory()->getSupplier()->getId()}"][] = $supplierAccountLog;
        }

        /** @var SupplierAccountLog[] $supplierAccountLogs */
        foreach ($realAccountLogs as $supplierAccountLogs) {
            $supplierAccountLog = $supplierAccountLogs[0];
            $prospect = $supplierAccountLog->getProspect();
            $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();

            fputcsv($handle, [
                $prospect->getEmail(),
                $supplier->getDisplayName(true),
                $supplierAccountLog->getUpdatedAt()->format('d/m/Y H:i:s')
            ]);

            try {
                // Check if we have not already sent the email, to bybass the ORDERING to ACCOUNT_OPENED bug
                $facadeSupplier = $supplierAccountLog->getSupplierCategory()->getSupplier()->getFacadeSupplier();

                if ($emailRepository->isTemplatedEmailAlreadySentToDst(50, $facadeSupplier->getCommercialEmails()[0], $prospect->getEmail())) {
                    continue ;
                }

                $contract = $sellAndSignService->getSignedContract($supplierAccountLog->getContractId());
                // On injecte le rib
                $uploadHandler->inject($prospect, 'ribFile');
                // On injecte le kbis
                $uploadHandler->inject($prospect, 'kbisFile');

                $builder = $emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);

                $rib = $prospect->getRibFile();
                $kbis = $prospect->getKbisFile();

                $builder
                    ->setTemplateId(50)
//                    ->addTo($facadeSupplier->getEmail())
                    ->addCc('hello@foodomarket.com')
//                    ->addTo('hello@foodomarket.com')
                    ->addAttachment("SEPA {$supplier->getDisplayName()} - {$prospect->getRestaurantName()}.pdf", $contract)

                    ->addVariable('RESTAURANT_NAME', $prospect->getRestaurantName())
                    ->addVariable('RECIPIENTS', $supplier->getDisplayName())
                    ->addVariable('ADDRESS_FULL', trim("{$prospect->getAddress()} {$prospect->getZipCode()} {$prospect->getCity()}"))
                    ->addVariable('DELIVERY_HOURS', $prospect->getDeliveryHours())
                    ->addVariable('MANAGER_NAME', "{$prospect->getFirstName()} {$prospect->getContactName()}")
                    ->addVariable('INTERLOCUTOR', "{$prospect->getFirstName()} {$prospect->getContactName()}")
                    ->addVariable('TELEPHONE', "{$prospect->getPhone()} / {$prospect->getMobile()}")
                    ->addVariable('EMAIL', $prospect->getEmail())
                ;

                foreach ($facadeSupplier->getCommercialEmails() as $commercialEmail) {
                    $builder->addTo($commercialEmail);
                }

                if ($rib !== null) {
//                    $builder->addAttachment($rib->getFilename(), file_get_contents($rib->getPathname()));

                    $ribUrl = $uploaderHelper->asset($prospect, 'ribFile');
                    $ribUrl = explode('/', $ribUrl);
                    $ribUrl[count($ribUrl) - 1] = rawurlencode($ribUrl[count($ribUrl) - 1]);
                    $ribUrl = implode('/', $ribUrl);

                    $builder->addVariable('RIB_URL', "{$baseUrl}{$ribUrl}");
                } else {
                    $builder->addVariable('RIB_URL', $baseUrl);
                }

                if ($kbis !== null) {
//                    $builder->addAttachment($kbis->getFilename(), file_get_contents($kbis->getPathname()));

                    $kbisUrl = $uploaderHelper->asset($prospect, 'kbisFile');
                    $kbisUrl = explode('/', $kbisUrl);
                    $kbisUrl[count($kbisUrl) - 1] = rawurlencode($kbisUrl[count($kbisUrl) - 1]);
                    $kbisUrl = implode('/', $kbisUrl);

                    $builder->addVariable('KBIS_URL', "{$baseUrl}{$kbisUrl}");
                } else {
                    $builder->addVariable('KBIS_URL', $baseUrl);
                }

                $email = $builder->build();

                $sendinBlueManager->addEmailToQueue($email);

                // On met le compte en ouvert !
                foreach ($supplierAccountLogs as $log) {
                    $log->setStatus(SupplierAccountLog::STATUS_ACCOUNT_OPENED);
                }

                $this->getContainer()->get('doctrine.orm.entity_manager')->flush();

                try {
                    CurlHelper::call('https://hooks.zapier.com/hooks/catch/3050086/oto6093/', [], [
                        'RESTAURANT_NAME' => $prospect->getRestaurantName(),
                        'FIRST_NAME' => $prospect->getFirstName(),
                        'PHONE' => $prospect->getPhone(),
                        'MAIL' => $prospect->getEmail(),
                        'DATE' => (new DateTime())->format('c'),
                        'ADDRESS' => $prospect->getZipCode()
                    ]);
                } catch (Exception $ignored) {
                }
            } catch (SellAndSignException $e) {
//                $output->writeln("SellAndSignException for {$supplierAccountLog}: {$e->getMessage()}");
            }
        }

        fclose($handle);

        $output->writeln("Done");
    }
}