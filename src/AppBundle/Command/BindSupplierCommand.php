<?php


namespace AppBundle\Command;


use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ShopUserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BindSupplierCommand extends Command
{
    public static $defaultName = 'app:bind:supplier';
    /**
     * @var SupplierRepository
     */
    private $supplierRepository;
    /**
     * @var ProspectRepository
     */
    private $prospectRepository;
    /**
     * @var ShopUserService
     */
    private $shopUserService;

    public function __construct(SupplierRepository $supplierRepository, ProspectRepository $prospectRepository, ShopUserService $shopUserService)
    {
        parent::__construct();
        $this->supplierRepository = $supplierRepository;
        $this->prospectRepository = $prospectRepository;
        $this->shopUserService = $shopUserService;
    }

    public function configure()
    {
        $this->addArgument('supplier', InputArgument::REQUIRED);
        $this->addArgument('prospect', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierId = $input->getArgument('supplier');
        $prospectId = $input->getArgument('prospect');

        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($supplierId);
        /** @var Prospect $prospect */
        $prospect = $this->prospectRepository->find($prospectId);

        $output->writeln("Binding `{$supplier->getDisplayName(true)} ($supplierId)` to `{$prospect->getEmail()} ($prospectId)`... ");

        $this->shopUserService->setEmailNotificationEnabled(false);

        $this->shopUserService->openSupplierAccount($prospect, $supplier, SupplierAccountLog::STATUS_ACCOUNT_OPENED);

        $output->writeln("done");
    }
}
