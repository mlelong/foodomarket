<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\RedisCacheManager;

class RemoveProductPriceCacheCommand extends ContainerAwareCommand
{

    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('app:cache:remove-product-price')
            ->addArgument('delay', InputArgument::OPTIONAL)
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp('
            <info>Invalidate the products price cache / By default take products with less than 1 day prices updates</info>
            Example : php bin/console app:cache:remove-products [DELAY] [--displaydebug]
            ');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initializeDebug($output, $input->getOption('displaydebug'));

        $this->redisCache = $this->getContainer()->get('app.redis.cache');

        $delay = $input->getArgument('delay');
        if (is_null($delay)) {
            $delay = 1;
        }

        $currentPeriodEnd = new \DateTime();
        $currentPeriodBegin = new \DateTime();
        $currentPeriodBegin->modify('-'.$delay.' day');
        $this->info('Remove the products cache updated since '.$delay.' days');

        $priceHistoryRepository = $this->getContainer()->get('app.repository.supplier_product_price_history');
        $qb = $priceHistoryRepository->createQueryBuilder('history')
                                     ->join('history.product', 'product')
                                     ;

        if ($currentPeriodBegin) {
            $qb
                ->andWhere('history.createdAt BETWEEN :date_start AND :date_end')
                ->setParameter('date_start', $currentPeriodBegin)
                ->setParameter('date_end', $currentPeriodEnd);
        }

        $lastUpdatedProductsQuery = $qb
            ->select('history')
            ->getQuery();

        $lastUpdatedProducts = $lastUpdatedProductsQuery->getResult();

        $this->start(count($lastUpdatedProducts));

        $i=0;
        foreach ($lastUpdatedProducts as $supplierProductPrice) {
            $i++;
            $this->advance('Product : #' . $supplierProductPrice->getProduct()->getId().' '.$supplierProductPrice->getProduct()->getName());

            $this->redisCache->removeCatalogProductCache($supplierProductPrice->getProduct()->getId());

            $this->info('Remove the key CACHE_KEY_SHOP_CATALOG_PRODUCT '.str_replace('{productId}', $supplierProductPrice->getProduct()->getId(), RedisCacheManager::CACHE_KEY_SHOP_CATALOG_PRODUCT));

            $this->redisCache->removeCatalogSupplierProductCache(
                "*{$supplierProductPrice->getSupplier()->getId()}*",
                $supplierProductPrice->getProduct()->getId()
            );

            $this->info('Remove the key CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCT '.str_replace(['{supplierId}', '{productId}', '{restaurantId}'], [$supplierProductPrice->getSupplier()->getId(), $supplierProductPrice->getProduct()->getId(), $supplierProductPrice->getRestaurant()->getId()], RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCT));

            $this->redisCache->removeRestaurantSupplierVariantCache(
//                $supplierProductPrice->getRestaurant()->getId(),
                '*',
//                "*{$supplierProductPrice->getSupplier()->getId()}*",
                '*',
                $supplierProductPrice->getProductVariant()->getId()
            );
        }

        $this->redisCache->removeRestaurantProductsCache('*');
        $this->redisCache->removeRestaurantSupplierProductsCache('*', '*');
        $this->redisCache->removeRestaurantCatalogProductsCache('*', '*');

        $this->redisCache->removeCatalogSearchCache();
        $this->redisCache->removePricesListingCache();

        $this->finish();
    }
}