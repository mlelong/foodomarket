<?php

namespace AppBundle\Command;

use AppBundle\Entity\EmailingStatistics;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Services\OrderService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

use AppBundle\Entity\ShoppingCart;
use AppBundle\Entity\ShoppingCartItem;


class OrderTestingCommand extends ContainerAwareCommand
{
    use CommandTrait;

    const PROSPECT_ID = 27;
    const PRODUCT_ID = 7;
    const VARIANT_ID = 1063;

    /**
     * @var EntityManager|object
     */
    private $manager;

    private $supplier = null;

    protected function configure()
    {
        $this
            ->setName('app:test:new-order')
            ->addOption('prospect', null, InputOption::VALUE_OPTIONAL, 'prospect to use (id or email)')
            ->addOption('product', null, InputOption::VALUE_OPTIONAL, 'Product to use')
            ->addOption('variant', null, InputOption::VALUE_OPTIONAL, 'Variant to use')
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp(
            '
            <info>This command allow to test the cart & order process</info>
            
            Example : php bin/console app:test:new-order --prospect=27 --product=7 --variant=1063 --displaydebug
            Example : php bin/console app:test:new-order --prospect=juliefarhi@hotmail.com --variant=1063 --displaydebug
            
            '
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->init($input, $output);
        $this->getCustomer();
        $this->getProduct();
        $this->getVariant();
        $this->getSupplier();

        // create the cart
        $this->createCart();

        // build the order
        $this->placeOrder();


        $this->isOrderOk();
    }

    public function isOrderOk()
    {

        $this->info("Is the order OK ?");

        if (count($this->order->getShoppingCart()->getItems())) {
            $this->info("OK");
        } else {
            $this->info("KO");
        }

        return;

        foreach ($this->order->getShoppingCart()->getItems() as $item) {
            dump("Item : " . $item->getProductVariant()->getProduct()->getName());
        }
    }

    public function placeOrder() {

        $this->order = $this->orderService->newFloatingOrder($this->shopUser, $this->cart);

        $this->info("Just placed the order ".$this->order->getId());

    }

    public function createCart() {

        $this->info("Create a cart for restaurant ".$this->restaurant->getName());

        $this->cart = new ShoppingCart();
        $this->cart->setSupplier($this->supplier);
        $this->cart->setRestaurant($this->restaurant);

        $this->manager->persist($this->cart);

        $shoppingCartItem = new ShoppingCartItem();

        $shoppingCartItem->setShoppingCart($this->cart);
        $shoppingCartItem->setProductVariant($this->variant);
        $shoppingCartItem->setQuantity(10);
        $shoppingCartItem->setUnit('KG');

        $this->cart->addItem($shoppingCartItem);

        $this->manager->persist($shoppingCartItem);

    }

    public function init(InputInterface $input, OutputInterface $output) {
        $this->initializeDebug($output, $input->getOption('displaydebug'));

        /** @var EntityManager $manager */
        $this->manager = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        /** @var SupplierRepository $this->supplierRepository */
        $this->supplierRepository = $this->getContainer()->get('app.repository.supplier');

        /** @var ProspectRepository $this->prospectRepository */
        $this->prospectRepository = $this->getContainer()->get('app.repository.prospect');

        /** @var RestaurantRepository $this->restaurantRepository */
        $this->restaurantRepository = $this->getContainer()->get('app.repository.restaurant');

        /** @var CustomerRepository $this->customerRepository */
        $this->customerRepository = $this->getContainer()->get('sylius.repository.customer');

        /** @var ShopUserRepository $this->shopUserRepository */
        $this->shopUserRepository = $this->getContainer()->get('sylius.repository.shop_user');

        /** @var ProductRepository $productRepository */
        $this->productRepository = $this->getContainer()->get('sylius.repository.product');

        /** @var variantRepository $variantRepository */
        $this->variantRepository = $this->getContainer()->get('app.repository.variant');

        /** @var variantInformationsRepository $variantInformationsRepository */
        $this->variantInformationsRepository = $this->getContainer()->get('app.repository.supplier_product_variant_informations');

        /** @var ShoppingCartRepository $shoppingCartRepository */
    //    $this->shoppingCart = $this->getContainer()->get('app.repository.shopping_cart');

        /** @var OrderRepository $orderRepository */
    //    $this->orderRepository = $this->getContainer()->get('sylius.repository.order');

        /** @var OrderService $orderService */
        $this->orderService = $this->getContainer()->get('app.service.order');

        $this->prospectIdOrEmail = $input->getOption('prospect');
        $this->productId = $input->getOption('product');
        $this->variantId = $input->getOption('variant');


        if (empty($this->productId) && empty($this->variantId)) {
            $this->productId = $this::PRODUCT_ID;
            $this->variantId = $this::VARIANT_ID;
        }

        if (empty($this->prospectIdOrEmail)) {
            $this->prospectIdOrEmail  = $this::PROSPECT_ID;
        }
    }

    public function getSupplier() {

        $this->info("Gettting supplier");

        $prospectSuppliers = [];
        foreach($this->prospect->getSuppliers() as $supplierCategory) {
            $prospectSuppliers[] = $supplierCategory->getSupplier()->getFacadesupplier()->getId();
        }

        $supplierId = 0;
        $supplierInformations = $this->variantInformationsRepository->findBy(['productVariant' => $this->variant->getId()]);
        foreach ($supplierInformations as $supplierInformation) {
            if (is_null($supplierInformation->getSupplier()->getFacadeSupplier())) {
                $supplierId = $supplierInformation->getSupplier()->getId();
            } else {
                $supplierId = $supplierInformation->getSupplier()->getFacadeSupplier()->getId();
            }

            if (in_array($supplierId, $prospectSuppliers)) {
                $this->supplier = $this->supplierRepository->findOneById($supplierId);
                $this->info("Supplier found : ".$this->supplier->getId().' | '.$this->supplier->getName());
                return;
            }
        }

        $this->info("KO");
        die();

    }

    public function getCustomer() {

        $this->info("Gettting prospect $this->prospectIdOrEmail ");

        if (is_numeric($this->prospectIdOrEmail)) {
            /** @var Prospect $prospect */
            $this->prospect = $this->prospectRepository->findOneBy(['id' => $this->prospectIdOrEmail]);
        } else {
            /** @var Prospect $prospect */
            $this->prospect = $this->prospectRepository->findOneBy(['email' => $this->prospectIdOrEmail]);
        }

        if (!$this->prospect) {
            $this->info("KO");
        }

        $this->info("Prospect found : ".$this->prospect->getId().' | '.$this->prospect->getFirstName().' | '.$this->prospect->getContactName().' | '.$this->prospect->getRestaurantName());

        $this->info("Gettting customer for ".$this->prospect->getEmail());

        $this->shopUser = null;
        $this->customer = null;
        foreach ($this->prospect->getCustomers() as $customer) {
            $this->customer = $customer;
            $this->shopUser = $customer->getShopUser();
        }

        if (!$this->customer) {
            $this->info("KO");
        }

        $this->info("Customer found : ".$this->customer->getId().' | '.$this->customer->getFirstname().' | '.$this->customer->getLastname().'');

        $this->info("Gettting shop user for ".$this->prospect->getEmail());

        if (!$this->shopUser) {
            $this->info("KO");
        }

        $this->info("Shop user found : ".$this->shopUser->getId().' | '.$this->shopUser->getEmail().' | '.$this->shopUser->getUsername().'');

        $this->info("This user has ".count($this->customer->getRestaurants()) ." restaurants");

        foreach($this->customer->getRestaurants() as $restaurant) {
            $this->restaurant = $restaurant;
            continue;
        }


        if (!$this->restaurant) {
            $this->info("KO");
        }

        $this->info("Restaurant found : ".$this->restaurant->getId().' | '.$this->restaurant->getName().' | '.$this->restaurant->getStreet().' | '.$this->restaurant->getCity().'');

    }

    public function getProduct()
    {
        if (!empty($this->productId)) {

            $this->info("Gettting product for " . $this->productId);
            $this->product = $this->productRepository->findOneById($this->productId);

            if (!$this->product) {
                $this->info("KO");
                return;
            }

            $this->info("Product found : " . $this->product->getId() . ' | ' . $this->product->getName() . '');

            foreach ($this->product->getVariants() as $productVariant) {
                $this->variant = $productVariant;
                continue;
            }

            $this->info("Variant found : " . $this->variant->getId() . ' | ' . $this->variant->getWeight() . 'Kg');
        }
    }

    public function getVariant() {

        if (!empty($this->variantId)) {

            $this->info("Gettting variant for ".$this->variantId);
            $this->variant = $this->variantRepository->findOneById($this->variantId);

            if (!$this->variant) {
                $this->info("KO");
                return;
            }

            $this->product = $this->variant->getProduct();

            $this->info("Product found : " . $this->product->getId() . ' | ' . $this->product->getName() . '');
            $this->info("Variant found : " . $this->variant->getId() . ' | ' . $this->variant->getWeight() . 'Kg');
        }
    }

}