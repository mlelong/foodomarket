<?php


namespace AppBundle\Command;


use AppBundle\Entity\ProductVariant;
use AppBundle\Repository\ProductVariantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindUcUqProblemCommand extends Command
{
    public static $defaultName = 'app:find:ucuq-problem';

    /**
     * @var ProductVariantRepository
     */
    private $variantRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, ProductVariantRepository $variantRepository)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->variantRepository = $variantRepository;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $iterator = $this->variantRepository->createQueryBuilder('v')->getQuery()->iterate();

        foreach ($iterator as $item) {
            /** @var ProductVariant $variant */
            $variant = $item[0];

            $orderUnit = $variant->getOptionOrderUnit();
            $unitQuantity = $variant->getOptionUnitQuantity();

            if ($orderUnit) {
                $orderUnit = $orderUnit->getValue();
            } else {
                continue ;
            }

            if ($unitQuantity) {
                $unitQuantity = $unitQuantity->getValue();
            } else {
                continue ;
            }

            if ($orderUnit === 'PC' && $unitQuantity > 1) {
                $output->writeln("({$variant->getId()}) {$variant->getFullDisplayName()} : UC: PC and UQ: $unitQuantity");
            }

            $this->entityManager->detach($variant);
        }
    }
}
