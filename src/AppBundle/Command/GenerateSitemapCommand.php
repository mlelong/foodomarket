<?php

namespace AppBundle\Command;

use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use Doctrine\ORM\NonUniqueResultException;
use Sylius\Component\Core\Model\ProductTaxon;
use Sylius\Component\Core\Model\Taxon;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Router;

class GenerateSitemapCommand extends ContainerAwareCommand
{
    const HOME_PRIORITY = 1;
    const CATALOG_PRIORITY = 2;
    const PRODUCT_PRIORITY = 3;
    const STATIC_PRIORITY = 4;

    /** @var resource */
    private $xw;

    /** @var Router */
    private $router;

    protected function configure()
    {
        $this
            ->setName('app:generate:sitemap')
        ;
    }

    private function init()
    {
        $this->router = $this->getContainer()->get('router');

        $this->xw = xmlwriter_open_memory();

        xmlwriter_set_indent($this->xw, 1);
        xmlwriter_set_indent_string($this->xw, ' ');
        xmlwriter_start_document($this->xw, '1.0', 'UTF-8');
        xmlwriter_start_element($this->xw, 'urlset');
        xmlwriter_start_attribute($this->xw, 'xmlns');
        xmlwriter_text($this->xw, 'http://www.sitemaps.org/schemas/sitemap/0.9');
        xmlwriter_end_attribute($this->xw);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init();

        $this->addUrl(
            $this->router->generate('app_front_index', [], Router::ABSOLUTE_URL),
            null,
            'weekly',
            self::HOME_PRIORITY
        );

        $supplierRepository = $this->getContainer()->get('app.repository.supplier');

        // Les pages catégories
        $supplierProductPriceRepository = $this->getContainer()->get('app.repository.supplier_product_price');
        $taxonRepository = $this->getContainer()->get('sylius.repository.taxon');
        /** @var Taxon[] $taxons */
        $taxons = $taxonRepository->createQueryBuilder('t')
            ->where('t.level >= 2')
            ->getQuery()
            ->getResult()
        ;

        foreach ($taxons as $taxon) {
            // Check if there is at least an active public product in this category
            $qb = $supplierProductPriceRepository->createQueryBuilder('spp');

            try {
                $nbActivePartnerProductsForTaxon = $qb
                    ->select('COUNT(DISTINCT(spp))')
                    ->join(ProductTaxon::class, 'pt', 'WITH', 'pt.product = spp.product AND pt.taxon = :taxon')
                    ->join('spp.supplier', 's')
                    ->join('spp.product', 'p')
                    ->join('spp.productVariant', 'v')
                    ->where('s.enabled = 0')
                    ->where('p.enabled = 1')
                    ->where('v.enabled = 1')
                    ->andWhere('spp.restaurant = 4')
                    ->setParameter('taxon', $taxon)
                    ->getQuery()
                    ->getSingleScalarResult();
            } catch (NonUniqueResultException $e) {
                $nbActivePartnerProductsForTaxon = 0;
            }

            if ($nbActivePartnerProductsForTaxon > 0) {
                $slug = urlencode($taxon->getSlug());

                $url = $this->router->generate('app_front_catalog_page', ['slug' => $slug], Router::ABSOLUTE_URL);

                $this->addUrl($url, null, 'weekly', self::CATALOG_PRIORITY);
            }
        }

        // Les pages produits
        $products = $supplierRepository->getActiveProducts(...$supplierRepository->getPartners());

        foreach ($products as $product) {
            $slug = urlencode($product->getSlug());

            $url = $this->router->generate('app_front_product_page', ['slug' => $slug], Router::ABSOLUTE_URL);

            $this->addUrl($url, null, null, self::PRODUCT_PRIORITY);
        }

        // Les autres pages (statique, etc ...)

        $this->addUrl($this->router->generate('app_front_static_about_us_page', [], Router::ABSOLUTE_URL), null, null, self::STATIC_PRIORITY);
        $this->addUrl($this->router->generate('app_front_static_legal_notice_page', [], Router::ABSOLUTE_URL), null, null, self::STATIC_PRIORITY);
        $this->addUrl($this->router->generate('app_front_static_privacy_policy_page', [], Router::ABSOLUTE_URL), null, null, self::STATIC_PRIORITY);

        // Voilà c'est fini

        xmlwriter_end_document($this->xw);

        $webDir = $this->getContainer()->getParameter('kernel.web_dir');

        file_put_contents("$webDir/sitemap.xml", xmlwriter_output_memory($this->xw));
    }

    private function addUrl($loc, $lastmod, $changefreq, $priority)
    {
        xmlwriter_start_element($this->xw, 'url');

        xmlwriter_start_element($this->xw, 'loc');
        xmlwriter_text($this->xw, $loc);
        xmlwriter_end_element($this->xw); //url

        if ($lastmod !== null) {
            xmlwriter_start_element($this->xw, 'lastmod');
            xmlwriter_text($this->xw, $lastmod);
            xmlwriter_end_element($this->xw); //url
        }

        if ($changefreq !== null) {
            xmlwriter_start_element($this->xw, 'changefreq');
            xmlwriter_text($this->xw, $changefreq);
            xmlwriter_end_element($this->xw); //url
        }

        if ($priority !== null) {
            xmlwriter_start_element($this->xw, 'priority');
            xmlwriter_text($this->xw, $priority);
            xmlwriter_end_element($this->xw); //url
        }

        xmlwriter_end_element($this->xw); // urlset
    }
}