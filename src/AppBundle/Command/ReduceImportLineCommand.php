<?php

namespace AppBundle\Command;

use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReduceImportLineCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:import-line:reduce')
            ->addArgument('importId', InputArgument::REQUIRED)
            ->setDescription(
            <<<BOF
                Removes the duplicated import lines for an import.
                It will find lines with the same supplier product name and ref, and attempt to remove all but one line like this.
                Only lines with status AWAITING will be removed. If all duplicates are in other state, displays a message bu does nothing.
BOF
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $importId = $input->getArgument('importId');
        $importRepository = $this->getContainer()->get('app.repository.import');
        /** @var Import $import */
        $import = $importRepository->find($importId);
        $importLineManager = $this->getContainer()->get('app.manager.import_line');

        if ($import === null) {
            $output->writeln("Could not find import with id `$importId`");
            return ;
        }

        $cache = [];

        /** @var ImportLine $line */
        foreach ($import->getLines() as $line) {
            $cache["{$line->getReference()} - {$line->getSupplierName()}"][] = $line;
        }

        $duplicates = 0;
        $unduplicates = 0;

        foreach ($cache as $productName => $lines) {
            while (count($lines) > 1) {
                $foundWaiting = false;

                $output->write("Found duplicated entry for `$productName`... ");

                foreach ($lines as $i => $line) {
                    if ($line->getStatus() === ImportLine::STATUS_AWAITING_CHECK) {
                        $foundWaiting = true;
                        ++$duplicates;

                        try {
                            $importLineManager->remove($line);
                            $importLineManager->flush();

                            unset($lines[$i]);

                            $output->writeln("removed");
                            break ;
                        } catch (OptimisticLockException $e) {
                            $output->writeln("Could not remove line: {$e->getMessage()}");
                        }
                    }
                }

                if (!$foundWaiting) {
                    $output->writeln("cannot find a waiting line to remove, skipping");
                    ++$unduplicates;
                    break ;
                }
            }
        }

        $output->writeln("\nFound $duplicates duplicates and $unduplicates unduplicates.");
    }
}