<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Email;

class sendTransactionnalEmailToNewProspectCommand extends ContainerAwareCommand
{

    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('app:email:send-emails-to-new-prospects')
            ->addOption('delay', 'delay', InputOption::VALUE_REQUIRED, 'Count of days since inscription')
            ->addOption('template-id', 'template', InputOption::VALUE_REQUIRED, 'SendInBlue template id')
            ->addOption('all-prospects', null, InputOption::VALUE_NONE, 'Send to prospects and clients, by default, only prospects')
            ->addOption('ignore-rgpd', null, InputOption::VALUE_NONE, 'Ignore RGPD and send to everybody')
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp(
<<<EOF
This command's purpose is to send a few emails to new prospects, days after days.

Example : php bin/console app:email:send-emails-to-new-prospects --delay=[DELAY] --template-id=[TEMPLATEID] --all-prospects --ignore-rgpd --displaydebug
EOF
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->initializeDebug($output, $input->getOption('displaydebug'));

        /** @var EntityManager $manager */
        $manager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        /** @var ProspectRepository $prospectRepository */
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');
        /** @var $sendInBlue */
        $emailManager = $this->getContainer()->get('app.sendinblue.manager');
        /** @var EmailRepository $emailRepository */
        $emailRepository = $this->getContainer()->get('app.repository.email');

        $delay = $input->getOption('delay');
        $templateId = $input->getOption('template-id');
        $allProspects = $input->getOption('all-prospects');
        $ignoreRGPD = $input->getOption('ignore-rgpd');

        if (is_null($delay)) {
            $delay = 2;
        }
        if (is_null($ignoreRGPD)) {
            $ignoreRGPD = false;
        }

        $searchDay = new \DateTime();
        $searchDay->modify('-'.$delay.' day');

        $duplicateSendingSearch = new \DateTime();
        $duplicateSendingSearch->modify('-30 day');

        $this->info('Send email with template '.$templateId.' to prospect created since '.$delay.' days : '.$searchDay->format('Y-m-d 00:00:00'));

        $qb = $prospectRepository->createQueryBuilder('p')
            ->orderBy('p.createdAt', 'ASC')
            ->andWhere('p.createdAt BETWEEN :date_start AND :date_end')
            ->setParameter('date_start', $searchDay->format('Y-m-d 00:00:00'))
            ->setParameter('date_end', $searchDay->format('Y-m-d 23:59:59'));

        if ($ignoreRGPD === false) {
            $qb->andWhere('p.newsletterPrices = 1');
        }

        $prospects = $qb->getQuery()->getResult();

        $this->start(count($prospects));

        $i = 0;
        foreach ($prospects as $prospect) {
            $i++;
            $this->advance();
            if (!$allProspects && count($prospect->getSuppliers())) {
                continue;
            }

            $qb = $emailRepository->createQueryBuilder('e')
                ->select('count(e.id)')
                ->andWhere('e.createdAt > :date_start')
                ->andWhere('e.templateId = :template_id')
                ->andWhere('e.to like :email')
                ->setParameter('date_start', $duplicateSendingSearch->format('Y-m-d 00:00:00'))
                ->setParameter('template_id', $templateId)
                ->setParameter('email', '%'.$prospect->getEmail().'%')
                ;
            $emailsSent = $qb->getQuery()->getSingleScalarResult();

            if ($emailsSent > 0) {
                continue;
            }

            $firstname = '';
            if ($prospect->getFirstName()) {
                $firstname = $prospect->getFirstName();
            } else {
                $firstname = $prospect->getRestaurantName();
            }

            /** @var $emailBuilder */
            $emailBuilder = $this->getContainer()->get('app.factory.email')->createBuilder(Email::MANAGER_SENDINBLUE);

            $emailBuilder
                ->setTemplateId($templateId)
                ->addVariable('EMAIL', $prospect->getEmail())
                ->addVariable('FIRSTNAME', $firstname)
                ->addTo($prospect->getEmail(), $firstname)
                ->addBcc('4333681@bcc.hubspot.com', 'bcc.hubspot.com')
            ;

            $emailManager->addEmailToQueue($emailBuilder->build());
    
            $this->info('#'.str_pad($i, 3, '0', STR_PAD_LEFT).' : Send email to '.$prospect->getEmail().' / '.$firstname);
        }

        $this->finish();

    }
}