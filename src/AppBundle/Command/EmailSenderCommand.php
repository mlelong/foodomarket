<?php

namespace AppBundle\Command;

use AppBundle\Entity\Email;
use AppBundle\Repository\EmailRepository;
use AppBundle\Services\Mailer\AbstractMailManager;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailSenderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:email:send')
            ->addArgument('id', InputArgument::REQUIRED, 'The Email id to send')
            ->setHelp("php bin/console app:email:send '4962'")
        ;
    }

    /**
     * @return Email
     */
    private function generateTestEmail()
    {
        /** @var EmailFactory $emailFactory */
        $emailFactory = $this->getContainer()->get('app.factory.email');

        return $emailFactory
            ->createBuilder(Email::MANAGER_MAILJET)
            ->addTo('jeremyvienne@gmail.com', 'Jérémy Vienne')
            ->addTo('mickael@foodomarket.com')
//            ->addFrom('jeremy@foodomarket.com', 'Jérémy Vienne')
            ->addFrom('olivier@foodomarket.com', 'Olivier de FoodoMarket')
            ->addReplyTo('jeremy@foodomarket.com', 'Jérémy Vienne')
            ->addReplyTo('mickael@foodomarket.com', 'Mickael Lelong')
            ->addCc('jeremy@foodomarket.com', 'Jérémt Vienne')
            ->addCc('eric@foodomarket.com', 'Eric Nivoix')
            ->addBcc('matthieu@foodomarket.com', 'Matthieu Boivent')

            ->setTemplateId(365808)
            ->addVariable('firstname', 'cousin')
//            ->addVariable('MERCURIAL_URL', 'https://www.foodomarket.com')

            ->setHtmlBody('<b>Héhé les futurs vacanciers !</b>')
            ->setTextBody('Juste un texte lambda')
            ->setSubject('Bonjour au nouveau service mailer mailjet ! :)')
            ->addAttachment('tarifs.csv', 'ABRICOT,1.2€,KG')
            ->addAttachment('tarifs.txt', 'COUCOUILLE,trop cher au kilo gro,KG')

            ->build();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        /** @var EmailRepository $emailRepository */
        $emailRepository = $this->getContainer()->get('app.repository.email');
        /** @var Email $email */
        $email = $emailRepository->find($input->getArgument('id'));

        $sendEmails = $this->getContainer()->getParameter('send_emails');
        $emailDebug = $this->getContainer()->getParameter('email_debug');

        /** @var AbstractMailManager $mailManager */
        $mailManager = null;

        switch ($email->getMailManagerName()) {
            case Email::MANAGER_SENDINBLUE:
                $mailManager = $this->getContainer()->get('app.sendinblue.manager');
                break ;
            case Email::MANAGER_MAILJET:
                $mailManager = $this->getContainer()->get('app.mailjet.manager');
                break ;
        }

        if ($mailManager !== null) {

            if ($sendEmails !== true) {
                // replace TO and CC by the debug email
                $email->setTo([]);
                $email->setCc([]);
                $email->setBcc([]);
                $email->setTo([$emailDebug]);
            }

            if ($sendEmails !== false) {
                $mailManager->sendEmail($email);
                $output->writeln('sending email : ' . $email->getId() . ' to : ' . $this->getToTextValue($email->getTo()));
            }

            try {
                $entityManager->flush();
            } catch (OptimisticLockException $e) {
                $output->writeln($e->getTraceAsString());
            } catch (ORMException $e) {
                $output->writeln($e->getTraceAsString());
            }

            // Avoid spamming the api
            $output->writeln('sleep 1');
            sleep(1);

            // Avoid spamming the gmail
            if (stripos($this->getToTextValue($email->getTo()), 'gmail') !== false) {
                $output->writeln('sleep 4');
                sleep(4);
            }

        } else {
            $output->writeln("Unknown Email Manager `{$email->getMailManagerName()}`");
        }
    }

    protected function getToTextValue($to) {

        $first = array_slice($to, 0, 1, TRUE);
        foreach($first as $key => $value) {
            if (is_int($key)) {
                return $value;
            } else
                return $key;
        }

        return '';

    }
}