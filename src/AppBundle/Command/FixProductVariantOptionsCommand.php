<?php

namespace AppBundle\Command;

use AppBundle\Entity\Import;
use AppBundle\Entity\ImportLine;
use AppBundle\Entity\ProductVariant;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixProductVariantOptionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:product-variant-options:fix')
            ->addArgument('importId', InputArgument::REQUIRED)
            ->setDescription("Adds the missing option values on variant that have been imported via import product v2 (by comparison of import line data vs variant data)")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $importId = $input->getArgument('importId');
        /** @var Import|null $import */
        $import = $this->getContainer()->get('app.repository.import')->find($importId);

        if ($import === null) {
            $output->writeln("Cannot find import with id `$importId`");
        }

        $productVariantManager = $this->getContainer()->get('sylius.manager.product_variant');

        /** @var ImportLine $line */
        foreach ($import->getLines() as $line) {
            if ($line->getStatus() === ImportLine::STATUS_IMPORTED) {
                /** @var ProductVariant $variant */
                $variant = $line->getProductVariant();

                $optionValues = $this->getOptionMapping($line);
                $modified = false;

                foreach ($optionValues as $optionCode => $optionValueCode) {
                    if (empty(trim($optionValueCode))) {
                        continue ;
                    }

                    $variantOptionValue = $variant->getOptionValuesByOptionCode($optionCode);

                    if (!$variantOptionValue) {
                        $output->write("[{$variant->getId()}]\t$optionCode => $optionValueCode: not set on variant, adding option value...");

                        $optionValue = $this->getOptionValue($optionCode, $optionValueCode);

                        if ($optionValue !== null) {
                            $variant->addOptionValue($optionValue);
                            $output->writeln("\tOk");
                            $modified = true;
                        } else {
                            $output->writeln("\tOups, $optionCode => $optionValueCode: does not exist as option value");
                        }
                    }
                }

                if ($modified) {
                    try {
                        $productVariantManager->flush($variant);
                    } catch (OptimisticLockException $e) {
                        $output->writeln("Failed to flush variant `{$variant->getId()}`: {$e->getMessage()}");
                    }
                }
            }
        }

        $output->writeln("Done");
    }

    private function getOptionValue(string $optionCode, string $optionValueCode)
    {
        try {
            return $this->getContainer()->get('sylius.repository.product_option_value')->createQueryBuilder('pov')
                ->join('pov.option', 'option')
                ->where('option.code = :optionCode')
                ->setParameter('optionCode', $optionCode)
                ->andWhere('pov.code = :optionValueCode')
                ->setParameter('optionValueCode', $optionValueCode)
                ->getQuery()
                ->getSingleResult()
            ;
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    private function getOptionMapping(ImportLine $importLine) {
        return [
            'UM' => $importLine->getSalesUnit(),
            'UC' => $importLine->getOrderUnit(),
            'UQ' => $importLine->getUnitQuantity(),
            'UCQ' => $importLine->getUnitContentQuantity(),
            'UCM' => $importLine->getUnitContentMeasurement(),
            'VA' => $importLine->getVariety(),
            'MQ' => $importLine->getBrand(),
            'CO' => $importLine->getConditioning(),
            'CA' => $importLine->getCaliber(),
            'AP' => $importLine->getNaming(),
            'OP' => $importLine->getOriginCountry(),
            'OV' => $importLine->getOrigin(),
            'BI' => $importLine->getBio(),
            'CC' => $importLine->getCategory()
        ];
    }
}