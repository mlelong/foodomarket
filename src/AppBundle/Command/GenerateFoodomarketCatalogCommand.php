<?php


namespace AppBundle\Command;


use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateFoodomarketCatalogCommand extends ContainerAwareCommand
{
    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var SupplierProductPriceRepository */
    private $supplierProductPriceRepository;

    /** @var string */
    private $outputFolder;

    /** @var array */
    private $variantDataCache = [];

    private $header = [
        'ID produit',
        'ID variante',
        'Nom du produit',
        'Poids',
        'Prix unitaire',
        'Prix Kilo',
        'Unité de mesure fournisseur',
        'Unité de commande',
        "Pays d'origine",
        "Ville d'origine",
        'Bio',
        'Calibre',
        'Conditionnement',
        'Appellation',
        'Variété',
        'Catégorie',
        'Marque',
        "Nombre d'unités",
        "Contenu de l'unité",
        'Unité de mesure du contenu'
    ];

    protected function configure()
    {
        $this
            ->setName('app:generate:foodomarket-catalog')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init();

        $output->writeln("Dumping supplier catalogs to directory: `{$this->outputFolder}`");

        $partners = $this->supplierRepository->getPartners(true);

        foreach ($partners as $partner) {
            $output->writeln("\tGenerating catalog for supplier {$partner->getDisplayName(true)}");
            $this->dumpSupplierCatalog($partner);
        }

        $output->writeln("Done");
    }

    private function dumpSupplierCatalog(Supplier $supplier)
    {
        $handle = fopen("{$this->outputFolder}/{$supplier->getDisplayName(true)}.csv", 'w+');
        $prices = $this->supplierProductPriceRepository->getRestaurantPricesForSuppliers(4, $supplier);

        fputcsv($handle, $this->header);

        foreach ($prices as $price) {
            fputcsv($handle, $this->priceToLine($price));
        }

        fclose($handle);
    }

    private function priceToLine(SupplierProductPrice $price)
    {
        $variant = $price->getProductVariant();

        if (!isset($this->variantDataCache[$variant->getId()])) {
            $product = $price->getProduct();

            $this->variantDataCache[$variant->getId()] = [
                'ID produit' => $product->getId(),
                'ID variante' => $variant->getId(),
                'Nom du produit' => $product->getName(),
                'Poids' => $variant->getWeight(),
                'Prix unitaire' => 0,
                'Prix kilo' => 0,
                'Unité de mesure fournisseur' => $this->getOptionValue($variant->getOptionSupplierSalesUnit()),
                'Unité de commande' => $this->getOptionValue($variant->getOptionOrderUnit()),
                "Pays d'origine" => $this->getOptionValue($variant->getOptionOriginCountry()),
                "Ville d'origine" => $this->getOptionValue($variant->getOptionValuesByOptionCode('OV')),
                'Bio' => $this->getOptionValue($variant->getOptionBio()),
                'Calibre' => $this->getOptionValue($variant->getOptionCaliber()),
                'Conditionnement' => $this->getOptionValue($variant->getOptionConditionning()),
                'Appellation' => $this->getOptionValue($variant->getOptionValuesByOptionCode('AP')),
                'Variété' => $this->getOptionValue($variant->getOptionVariety()),
                'Catégorie' => $this->getOptionValue($variant->getOptionCategory()),
                'Marque' => $this->getOptionValue($variant->getOptionValuesByOptionCode('MQ')),
                "Nombre d'unités" => $this->getOptionValue($variant->getOptionUnitQuantity()),
                "Contenu de l'unité" => $this->getOptionValue($variant->getOptionUnitContentQuantity()),
                'Unité de mesure du contenu' => $this->getOptionValue($variant->getOptionUnitContentMeasurement())
            ];
        }

        $ret = $this->variantDataCache[$price->getProductVariant()->getId()];

        $ret['Prix unitaire'] = bcdiv($price->getUnitPrice(), 100, 2);
        $ret['Prix kilo'] = bcdiv($price->getKgPrice(), 100, 2);

        return $ret;
    }

    /**
     * @param ProductOptionValue|false $optionValue
     * @return string|void
     */
    private function getOptionValue($optionValue)
    {
        if (!$optionValue) {
            return '';
        }

        return $optionValue->getValue();
    }

    private function init()
    {
        $this->supplierRepository = $this->getContainer()->get('app.repository.supplier');
        $this->supplierProductPriceRepository = $this->getContainer()->get('app.repository.supplier_product_price');
        $this->outputFolder = $this->getContainer()->getParameter('kernel.project_dir') . '/var/catalogs';
    }
}