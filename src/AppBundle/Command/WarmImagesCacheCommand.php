<?php

namespace AppBundle\Command;

use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Liip\ImagineBundle\Service\FilterService;
use Sylius\Component\Core\Model\ProductImage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WarmImagesCacheCommand extends ContainerAwareCommand
{
    use CommandTrait;

    /* @var FilterManager $filterManager */
    private $filterManager;

    /* @var FilterService $filterService */
    private $filterService;

    public function __construct(FilterManager $filterManager, FilterService $filterService)
    {
        $this->filterManager = $filterManager;
        $this->filterService = $filterService;

        parent::__construct('liip:imagine:cache:warm');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $productImageRepository = $this->getContainer()->get('sylius.repository.product_image');
        /** @var ProductImage[] $images */
        $images = $productImageRepository->findAll();

        $this->initializeDebug($output, false, count($images));

        $filters = ['front_tiny', 'front_thumbnail', 'front_big'];

        foreach ($images as $image) {
            $path = $image->getPath();

            $this->advance("Warming $path");

            try {
                foreach ($filters as $filter) {
                    $this->filterService->getUrlOfFilteredImage($path, $filter);
                }
            } catch (\Exception $e) {
                $output->writeln("Error: $path: {$e->getMessage()}");
            }
        }

        $this->finish();
    }
}