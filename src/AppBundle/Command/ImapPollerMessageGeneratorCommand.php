<?php


namespace AppBundle\Command;


use ApiBundle\Dto\Interlocutor;
use AppBundle\Entity\Message;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\MessageRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierRepository;
use SecIT\ImapBundle\Service\Imap;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImapPollerMessageGeneratorCommand extends Command
{
    protected static $defaultName = 'app:poll:imap:generate:message';

    /** @var Imap */
    private $imapService;

    /** @var ProspectRepository */
    private $prospectRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var MessageRepository */
    private $messageRepository;

    public function __construct(Imap $imapService, ProspectRepository $prospectRepository, SupplierRepository $supplierRepository, MessageRepository $messageRepository)
    {
        $this->imapService = $imapService;
        $this->prospectRepository = $prospectRepository;
        $this->supplierRepository = $supplierRepository;
        $this->messageRepository = $messageRepository;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $invoiceConnection = $this->imapService->get('order_connection');
        $invoiceConnection->setAttachmentsIgnore(true);
        $messages = $invoiceConnection->searchMailbox('UNSEEN SINCE "12 Feb 2020"');

//        rsort($messages);

        foreach ($messages as $messageId) {
            $mail = $invoiceConnection->getMail($messageId, true);

            if (preg_match("/\[\[(?<prospect>\d+)-(?<supplier>\d+)]]/", $mail->subject, $matches)) {
                $output->write("Matching subject: $mail->subject, creating message...");

                $this->createMessage($matches['prospect'], $matches['supplier'], $mail->textPlain ?? $mail->textHtml);

                $output->writeln(" OK");
            }
        }

        return 0;
    }

    private function createMessage($prospectId, $supplierId, $content)
    {
        $message = new Message();

        /** @var Prospect $prospect */
        $prospect = $this->prospectRepository->find($prospectId);
        /** @var Supplier $supplier */
        $supplier = $this->supplierRepository->find($supplierId);

        $message->setSender($supplier);
        $message->setSenderType(Interlocutor::TYPE_SUPPLIER);
        $message->setRecipient($prospect);
        $message->setRecipientType(Interlocutor::TYPE_PROSPECT);
        $message->setChannel(Message::CHANNEL_EMAIL);
        $message->setChannelStatus(Message::CHANNEL_STATUS_DELIVERED);
        $message->setStatus(Message::MESSAGE_STATUS_SENT);
        $message->setMessage(nl2br($content));

        $this->messageRepository->add($message);
    }
}
