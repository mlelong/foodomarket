<?php


namespace AppBundle\Command;


use AppBundle\Entity\OrderPayment;
use AppBundle\Repository\OrderPaymentRepository;
use AppBundle\Services\Payment\Lyra\LyraPaymentService;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PollLyraStatusCommand extends Command
{
    public static $defaultName = 'app:poll:lyra-status';
    /**
     * @var OrderPaymentRepository
     */
    private $orderPaymentRepository;
    /**
     * @var LyraPaymentService
     */
    private $lyraPaymentService;

    public function __construct(OrderPaymentRepository $orderPaymentRepository, LyraPaymentService $lyraPaymentService)
    {
        parent::__construct();
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->lyraPaymentService = $lyraPaymentService;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $startDate = (new DateTime())->modify('-4 day');
        $qb = $this->orderPaymentRepository->createQueryBuilder('op');
        $class = OrderPayment::class;

        /** @var OrderPayment[] $orderPaymentsInCreatedStatus */
        $orderPaymentsInCreatedStatus = $qb
            ->where('op.lyraPaymentStatus = :status')
            ->setParameter('status', 'CREATED')
            ->andWhere('op.createdAt >= :date')
            ->andWhere("op.id = (SELECT MAX(o.id) FROM $class o WHERE o.order = op.order)")
            ->setParameter('date', $startDate)
            ->getQuery()
            ->getResult()
        ;

        $count = count($orderPaymentsInCreatedStatus);

        $output->writeln("Found {$count} order payments with CREATED status");

        foreach ($orderPaymentsInCreatedStatus as $payment) {
            $ret = $this->lyraPaymentService->handlePaymentWebhook($payment->getLyraId()) ? 'true' : 'false';

            $output->writeln("Ret for {$payment->getLyraId()}: $ret");
        }
    }
}
