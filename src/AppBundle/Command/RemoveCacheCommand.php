<?php

namespace AppBundle\Command;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Import;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Services\RedisCache;
use AppBundle\Services\RedisCacheManager;
use Sylius\Component\Core\Model\Product;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveCacheCommand extends ContainerAwareCommand
{
    use CommandTrait;

    /** @var RedisCache */
    private $redisCache;

    private $callbacks = [];

    protected function configure()
    {
        $this
            ->setName('app:cache:remove')
            ->addOption('insertions', 'i', InputOption::VALUE_IS_ARRAY|InputOption::VALUE_OPTIONAL, 'Entities that have been inserted')
            ->addOption('updates', 'u', InputOption::VALUE_IS_ARRAY|InputOption::VALUE_OPTIONAL, 'Entities that have been updated')
            ->addOption('deletions', 'd', InputOption::VALUE_IS_ARRAY|InputOption::VALUE_OPTIONAL, 'Entities that have been deleted')
            ->addOption('displaydebug', null, InputOption::VALUE_NONE, 'Display some debug informations')
            ->setHelp('
            <info>Remove keys from cache for selected entities and id</info>
            Example for insertion : php bin/console app:cache:remove \'-iAppBundle\Entity\SupplierProductPrice:120589\'
            Example for update : php bin/console app:cache:remove \'-uAppBundle\Entity\SupplierProductPrice:120589\'
            ');
        ;
    }

    private function init()
    {
        $this->redisCache = $this->getContainer()->get('app.redis.cache');

        $this->callbacks = [
            'insertions' => [
                SupplierProductPrice::class => [$this, 'handleSupplierProductPriceInsertion']
            ],
            'updates' => [
                Import::class => [$this, 'handleImportUpdate'],
                Prospect::class => [$this, 'handleProspectUpdate'],
                SupplierProductPrice::class => [$this, 'handleSupplierProductPriceUpdate'],
                Product::class => [$this, 'handleProductUpdate'],
                ProductVariant::class => [$this, 'handleProductVariantUpdate'],
            ],
            'deletions' => [

            ]
        ];
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initializeDebug($output, $input->getOption('displaydebug'));

        $insertions = $input->getOption('insertions');
        $updates = $input->getOption('updates');
        $deletions = $input->getOption('deletions');

        $this->start(count($insertions) + count($updates) + count($deletions));

        $this->init();

        $this->handleInsertions($this->getEntities($insertions));
        $this->handleUpdates($this->getEntities($updates));
        $this->handleDeletions($this->getEntities($deletions));

        $this->finish();
    }

    private function handleInsertions(array $entities = [])
    {
        foreach ($entities as $entity) {
            $entityClass = get_class($entity);

            if (isset($this->callbacks['insertions'][$entityClass])) {
                call_user_func($this->callbacks['insertions'][$entityClass], $entity);
            } else {
                $this->info("No insert callback defined for entity `$entityClass`, skipping...");
            }
        }
    }

    private function handleUpdates(array $entities = [])
    {
        foreach ($entities as $entity) {
            $entityClass = get_class($entity);

            if (isset($this->callbacks['updates'][$entityClass])) {
                call_user_func($this->callbacks['updates'][$entityClass], $entity);
            } else {
                $this->info("No update callback defined for entity `$entityClass`, skipping...");
            }
        }
    }

    private function handleDeletions(array $entities = [])
    {
        foreach ($entities as $entity) {
            $entityClass = get_class($entity);

            if (isset($this->callbacks['deletions'][$entityClass])) {
                call_user_func($this->callbacks['deletions'][$entityClass], $entity);
            } else {
                $this->info("No deletion callback defined for entity `$entityClass`, skipping...");
            }
        }
    }

    /**
     * @param array $entities
     * @return array
     */
    private function getEntities(array $entities = [])
    {
        $entityObjects = [];

        foreach ($entities as $entity) {
            $entityObjects[] = $this->getEntity($entity);
        }

        return $entityObjects;
    }

    /**
     * @param $entity
     * @return object|null
     */
    private function getEntity($entity)
    {
        $entity = explode(':', $entity);

        return $this->getContainer()->get('doctrine.orm.entity_manager')->find($entity[0], $entity[1]);
    }

    /**
     * ###################
     * INSERTION CALLBACKS
     * ###################
     */

    /**
     * @param SupplierProductPrice $supplierProductPrice
     */
    private function handleSupplierProductPriceInsertion(SupplierProductPrice $supplierProductPrice)
    {
        $this->advance('Remove cache for product: #' . $supplierProductPrice->getProduct()->getId());

        $this->redisCache->removeCatalogProductCache($supplierProductPrice->getProduct()->getId());

        $this->info('Remove the key CACHE_KEY_SHOP_CATALOG_PRODUCT '.str_replace('{productId}', $supplierProductPrice->getProduct()->getId(), RedisCacheManager::CACHE_KEY_SHOP_CATALOG_PRODUCT));

        $this->redisCache->removeCatalogSupplierProductCache(
            "*{$supplierProductPrice->getSupplier()->getId()}*",
            $supplierProductPrice->getProduct()->getId()
        );

        $this->info('Remove the key CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCT '.str_replace(['{supplierId}', '{productId}', '{restaurantId}'], [$supplierProductPrice->getSupplier()->getId(), $supplierProductPrice->getProduct()->getId(), $supplierProductPrice->getRestaurant()->getId()], RedisCacheManager::CACHE_KEY_SHOP_CATALOG_SUPPLIER_PRODUCT));

        $this->redisCache->removeRestaurantSupplierVariantCache(
            $supplierProductPrice->getRestaurant()->getId(),
            "*{$supplierProductPrice->getSupplier()->getId()}*",
            $supplierProductPrice->getProductVariant()->getId()
        );

        $this->redisCache->removeRestaurantProductsCache('*');
        $this->redisCache->removeRestaurantSupplierProductsCache($supplierProductPrice->getSupplier()->getId(), '*');
        $this->redisCache->removeRestaurantCatalogProductsCache('*', '*');

        $this->redisCache->removeCatalogSearchCache();
        $this->redisCache->removePricesListingCache();
    }

    /**
     * ################
     * UPDATE CALLBACKS
     * ################
     */

    /**
     * @param Import $import
     */
    private function handleImportUpdate(Import $import)
    {
    }

    private function handleProspectUpdate(Prospect $prospect)
    {
        $this->advance('Remove cache for prospect: #' .  $prospect->getId());

        $prospect->getCustomers()->forAll(function($key, Customer $customer) {
            $customer->getRestaurants()->forAll(function($key2, Restaurant $restaurant) {
                $this->redisCache->removeRestaurantSuppliers($restaurant->getId());
                $this->redisCache->removeRestaurantSupplierProductsCache('*', $restaurant->getId());
                $this->redisCache->removeRestaurantProductsCache($restaurant->getId());
                $this->redisCache->removeRestaurantShoppingListsCache($restaurant->getId());
                $this->redisCache->removeRestaurantCatalogProductsCache($restaurant->getId(), '*');

                return true;
            });

            return true;
        });
    }

    /**
     * Price updated callback
     *
     * To be removed:
     *  - order line data
     *  - catalog product
     *  - catalog products (for restaurant foodomarket and the others)
     *
     * @param SupplierProductPrice $supplierProductPrice
     */
    private function handleSupplierProductPriceUpdate(SupplierProductPrice $supplierProductPrice)
    {
        $this->advance('Remove cache for product: #' . $supplierProductPrice->getProduct()->getId());

        $this->redisCache->removeRestaurantSupplierVariantCache(
//            $supplierProductPrice->getRestaurant()->getId(),
            '*',
//            $supplierProductPrice->getSupplier()->getId(),
            '*',
            $supplierProductPrice->getProductVariant()->getId()
        );

        $this->info('Remove the key CACHE_KEY_SHOP_VARIANT '.str_replace(['{variantId}', '{supplierId}', '{restaurantId}'], [$supplierProductPrice->getProductVariant()->getId(), $supplierProductPrice->getSupplier()->getId(), $supplierProductPrice->getRestaurant()->getId()], RedisCacheManager::CACHE_KEY_SHOP_VARIANT));

        $this->redisCache->removeCatalogProductCache($supplierProductPrice->getProduct()->getId());

        $this->redisCache->removeCatalogSupplierProductCache(
            "*{$supplierProductPrice->getSupplier()->getId()}*",
            $supplierProductPrice->getProduct()->getId()
        );

        $this->redisCache->removeSupplierCatalogProductsCache($supplierProductPrice->getSupplier()->getId());
        $this->redisCache->removeRestaurantCatalogProductsCache('*', '*');

        $this->redisCache->removeCatalogSearchCache();
        $this->redisCache->removePricesListingCache();
    }

    private function handleProductUpdate(Product $product)
    {
        $this->advance('Remove cache for product: #' .  $product->getId());

        $this->redisCache->removeRestaurantCatalogProductsCache('*', '*');
        $this->redisCache->removeRestaurantSupplierProductsCache('*', '*');
        $this->redisCache->removeCatalogSupplierProductCache('*', $product->getId());
        $this->redisCache->removeSupplierCatalogProductsCache('*');
        $this->redisCache->removeCatalogProductCache($product->getId());
        $this->redisCache->removeRestaurantProductsCache('*');
        $this->redisCache->removeRestaurantShoppingListsCache('*');
        $this->redisCache->removeCatalogSearchCache();
        
        $product->getVariants()->forAll(function($key, ProductVariant $variant) {
            $this->redisCache->removeRestaurantSupplierVariantCache('*', '*', $variant->getId());

            return true;
        });
    }

    private function handleProductVariantUpdate(ProductVariant $productVariant)
    {
        $this->advance('Remove cache for product: #' .  $productVariant->getProduct()->getId());

        $this->redisCache->removeRestaurantCatalogProductsCache('*', '*');
        $this->redisCache->removeRestaurantSupplierProductsCache('*', '*');
        $this->redisCache->removeCatalogSupplierProductCache('*', $productVariant->getProduct()->getId());
        $this->redisCache->removeSupplierCatalogProductsCache('*');
        $this->redisCache->removeCatalogProductCache($productVariant->getProduct()->getId());
        $this->redisCache->removeRestaurantSupplierVariantCache('*', '*', $productVariant->getId());
        $this->redisCache->removeRestaurantProductsCache('*');
        $this->redisCache->removeRestaurantShoppingListsCache('*');
        $this->redisCache->removeCatalogSearchCache();
    }
}