<?php

namespace AppBundle\Command;

use AppBundle\Entity\SupplierProductPrice;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FixSupplierProductPriceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:fix:supplier-product-price')->addOption('accept', null, InputOption::VALUE_NONE);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierProductPriceRepository = $this->getContainer()->get('app.repository.supplier_product_price');
        $pricesIterator = $supplierProductPriceRepository->createQueryBuilder('spp')->getQuery()->iterate();
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $idsToUpdate = [];

        $output->writeln("Prices fetched");

        /** @var SupplierProductPrice[] $row */
        foreach ($pricesIterator as $row) {
            $proceed = true;
            $product = $row[0]->getProduct();
            $variant = $row[0]->getProductVariant();

            if ($variant === null) {
                $output->writeln("Price #{$row[0]->getId()} has no product variant set");
                $proceed = false;
            }

            if ($product === null) {
                $output->writeln("Price #{$row[0]->getId()} has no product set");
                $proceed = false;
            }

            if ($proceed && $variant->getProduct()->getId() != $product->getId()) {
                $idsToUpdate[] = $row[0]->getId();
                $explanationMessage = "{$variant->getInternalName()} should be associated with ({$variant->getProduct()->getId()}) {$variant->getProduct()->getName()} instead of ({$product->getId()}) {$product->getName()}";
                $output->writeln("Price #{$row[0]->getId()} marked for update: $explanationMessage");
            }

            $manager->detach($row[0]);
        }

        $nbIdsToUpdate = count($idsToUpdate);

        if ($nbIdsToUpdate > 0) {
            if ($input->getOption('accept')) {
                $output->writeln("\n$nbIdsToUpdate prices need to be updated. Proceeding...");

                /** @var SupplierProductPrice[] $pricesToUpdate */
                $pricesToUpdate = $supplierProductPriceRepository->findBy(['id' => $idsToUpdate]);

                foreach ($pricesToUpdate as $priceToUpdate) {
                    $exists = $supplierProductPriceRepository->count([
                        'supplier' => $priceToUpdate->getSupplier(),
                        'restaurant' => $priceToUpdate->getRestaurant(),
                        'product' => $priceToUpdate->getProductVariant()->getProduct(),
                        'productVariant' => $priceToUpdate->getProductVariant()
                    ]) > 0;

                    if ($exists) {
                        $output->writeln("Price already exists, removing...");
                        $manager->remove($priceToUpdate);

                        try {
                            $manager->flush($priceToUpdate);
                        } catch (OptimisticLockException $e) {
                            $output->writeln("oO Exception: {$e->getMessage()}");
                            return -1;
                        }
                    } else {
                        $priceToUpdate->setProduct($priceToUpdate->getProductVariant()->getProduct());

                        try {
                            $manager->flush($priceToUpdate);
                        } catch (OptimisticLockException $e) {
                            $output->writeln("oO Exception: {$e->getMessage()}");
                            return -1;
                        }
                    }
                }

                $output->writeln("\nDone");
            } else {
                $output->writeln("\n$nbIdsToUpdate prices need to be updated:");
                $output->writeln(implode(', ', $idsToUpdate));
                $output->writeln("Re-run with --accept to write changes to database");
            }
        } else {
            $output->writeln("Everything's Fine");
        }

        return 0;
    }
}