<?php

namespace AppBundle\Command;

use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FixSupplierProductVariantInformationsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:fix:supplier-product-variant-informations')
            ->addOption('accept', null, InputOption::VALUE_NONE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierProductVariantInformationsRepository = $this->getContainer()->get('app.repository.supplier_product_variant_informations');
        $pricesIterator = $supplierProductVariantInformationsRepository->createQueryBuilder('spvi')->getQuery()->iterate();
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $idsToUpdate = [];

        $output->writeln("Price informations fetched");

        /** @var SupplierProductVariantInformations[] $row */
        foreach ($pricesIterator as $row) {
            $proceed = true;
            $product = $row[0]->getProduct();
            $variant = $row[0]->getProductVariant();

            if ($variant === null) {
                $output->writeln("Price information #{$row[0]->getId()} has no product variant set");
                $proceed = false;
            }

            if ($product === null) {
                $output->writeln("Price information #{$row[0]->getId()} has no product set");
                $proceed = false;
            }

            if ($proceed && $variant->getProduct()->getId() != $product->getId()) {
                $idsToUpdate[] = $row[0]->getId();
                $explanationMessage = "{$variant->getInternalName()} should be associated with ({$variant->getProduct()->getId()}) {$variant->getProduct()->getName()} instead of ({$product->getId()}) {$product->getName()}";
                $output->writeln("Price information #{$row[0]->getId()} marked for update: $explanationMessage");
            }

            $manager->detach($row[0]);
        }

        $nbIdsToUpdate = count($idsToUpdate);

        if ($nbIdsToUpdate > 0) {
            if ($input->getOption('accept')) {
                $output->writeln("\n$nbIdsToUpdate price informations need to be updated. Proceeding...");

                /** @var SupplierProductVariantInformations[] $priceInfosToUpdate */
                $priceInfosToUpdate = $supplierProductVariantInformationsRepository->findBy(['id' => $idsToUpdate]);

                foreach ($priceInfosToUpdate as $priceInfoToUpdate) {
                    $priceInfoToUpdate->setProduct($priceInfoToUpdate->getProductVariant()->getProduct());
                }

                try {
                    $this->getContainer()->get('doctrine.orm.entity_manager')->flush();
                    $output->writeln("\nDone");
                } catch (OptimisticLockException $e) {
                    $output->writeln("oO Exception: {$e->getMessage()}");
                }
            } else {
                $output->writeln("\n$nbIdsToUpdate price informations need to be updated:");
                $output->writeln(implode(', ', $idsToUpdate));
                $output->writeln("Re-run with --accept to write changes to database");
            }
        } else {
            $output->writeln("Everything's Fine");
        }
    }
}