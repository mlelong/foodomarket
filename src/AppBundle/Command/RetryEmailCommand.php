<?php

namespace AppBundle\Command;

use AppBundle\Entity\Email;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RetryEmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:retry:email')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $startingDate = new \DateTime('20191203');
        $emailRepository = $this->getContainer()->get('app.repository.email');
        $sendinblueManager = $this->getContainer()->get('app.sendinblue.manager');
        $varDir = $this->getContainer()->getParameter('kernel.project_dir') . '/var';
        $pidFileLocation = "$varDir/retry-emails.pid";

        if (file_exists($pidFileLocation)) {
            return ;
        }

        $pidCommand = fopen($pidFileLocation, 'w');

        $emailsInError = $emailRepository->createQueryBuilder('e')
            ->where('e.status = :status')
            ->andWhere('e.createdAt >= :date')
            ->setParameter('status', Email::STATUS_ERROR)
            ->setParameter('date', $startingDate)
            ->getQuery()
            ->getResult()
        ;

        foreach ($emailsInError as $email) {
            $retries = 0;

            while (true) {
                if ($retries === 3) {
                    break ;
                }

                if ($sendinblueManager->sendEmail($email)) {
                    break ;
                }

                sleep(5);
                ++$retries;
            }
        }

        fclose($pidCommand);
        unlink($pidFileLocation);
    }
}