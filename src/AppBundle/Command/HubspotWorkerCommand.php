<?php

namespace AppBundle\Command;

use AppBundle\Entity\Onboarding;
use AppBundle\Entity\Supplier;
use AppBundle\Helper\CurlClient;
use Stripe\Error\Api;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Router;

class HubspotWorkerCommand extends ContainerAwareCommand
{
    const HUBSPOT_API_URL_BASE = 'https://api.hubapi.com';

//    const API_KEY = 'demo';
//    const SALE_PIPELINE_ID = '33287c36-4d67-40b8-8de0-1bc75085c9e3';
//    const DEAL_STAGES = [
//        'f12a41d1-1cd9-415c-a03b-f71d9feef51d',
//        '10f50449-2d2b-4945-b452-a1f05c9a376b',
//        'be05394f-8c62-405e-84f8-ebe82dcdfe70'
//    ];

    const API_KEY = 'd7d1f7ee-1aa9-4934-a98b-c05130ce8ec1';
    const SALE_PIPELINE_ID = 'default';
    const DEAL_STAGES = [
        'qualifiedtobuy',                       // Benchmark effectué
    ];

    protected function configure()
    {
        $this
            ->setName('app:hubspot:create-transactions-for-onboarding')
            ->setDescription('Create transactions on hubspot for latest onboarding benchamark created')
            ->setHelp('
            <info>Create transactions in hubspot for lastest benchmarks</info>
            Example : php bin/console app:hubspot:work
            ');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = $this->getContainer()->get('doctrine')->getManager();

        /** @var Onboarding[] $onboardings */
        $onboardings = $manager->getRepository(Onboarding::class)->findBy(['hubspotSync' => 0]);

        /**
         * Ensures we have already defined the custom fields we want to fill along with onboarding data
         * and that it is in sync (create and/or update the fields definition)
         */
//        $onboardingPropertyGroup = $this->getOnboardingDealPropertyGroup();
//
//        if ($onboardingPropertyGroup === null) {
//            $onboardingPropertyGroup = $this->createOnboardingDealPropertyGroup();
//
//            if ($onboardingPropertyGroup === false) {
//                $output->writeln("Could not create onboarding property group !");
//                return ;
//            }
//
//            if (!$this->createOnboardingDealProperties()) {
//                $output->writeln("Could not create onboarding deal properties !");
//                return ;
//            }
//        } else {
//            if (!$this->updateOnboardingDealPropertyGroup()) {
//                $output->writeln("Could not update onboarding deal property group !");
//                return ;
//            }
//            if (!$this->updateOnboardingDealProperties()) {
//                $output->writeln("Could not update onboarding deal properties !");
//                return ;
//            }
//        }

        foreach ($onboardings as $onboarding) {
            sleep(1);

            if ($onboarding->getUserPrice() === null || $onboarding->getUserPrice() == 0) {
                continue ;
            }

            $email = $onboarding->getContactEmail();
            $utmEmail = $onboarding->getUtmEmail();

            if ($email === null || empty($email)) {
                $email = $utmEmail;
            }

            if ($email === null || empty($email)) {
                continue ;
            }

            $user = $this->findHubspotUserByEmail($email);

            $name = "{$onboarding->getContactName()}";
            $phone = "{$onboarding->getTelephone()}";
            $restaurant = "{$onboarding->getRestaurantName()}";

            if ($user === null) {
                $user = $this->createOrUpdateContactByEmail($email, $name, $phone, $restaurant);

                if ($user === null) {
                    $output->writeln("Could not create contact for email `$email`");
                    continue ;
                }
            }

            $userDeals = $this->getSalePipelineDeals($user);

            $newStage = ($onboarding->getSuggestedSupplier() !== null && $onboarding->getSuggestedPrice() < $onboarding->getUserPrice())
                ? self::DEAL_STAGES[0]
                : null
            ;

            if ($newStage === null) {
                continue ;
            }

            switch ($onboarding->getCategory()) {
                case Supplier::CAT_FRUITS_LEGUMES:
                    $dealName = 'F&L - ';
                    break ;
                case Supplier::CAT_CREMERIE:
                    $dealName = 'Crèmerie - ';
                    break ;
                case Supplier::CAT_MAREE:
                    $dealName = 'Marée - ';
                    break ;
                case Supplier::CAT_EPICERIE:
                    $dealName = 'Epicerie - ';
                    break ;
                case Supplier::CAT_BOUCHERIE:
                    $dealName = 'Boucherie - ';
                    break ;
                default:
                    $dealName = $onboarding->getCategory();
                    break ;
            }

            $dealName .= "{$name} - {$restaurant}";
            $data = [
                'properties' => [
                    [
                        'name' => 'turnover',
                        'value' => $onboarding->getTurnover()
                    ],
                    [
                        'name' => 'userprice',
                        'value' => $onboarding->getUserPrice() !== null ? $onboarding->getUserPrice() : 0
                    ],
                    [
                        'name' => 'suggestedprice',
                        'value' => $onboarding->getSuggestedPrice() !== null ? $onboarding->getSuggestedPrice() : 0
                    ],
                    [
                        'name' => 'chosensupplier',
                        'value' => $onboarding->getSuggestedSupplier() !== null
                            ? $onboarding->getSuggestedSupplier()->getName()
                            : ''
                    ],
                    [
                        'name' => 'onboardingfrontlink2',
                        'value' => $this->getContainer()->get('router')->generate(
                            'app_onboarding',
                            ['onboardingId' => $onboarding->getId()],
                            Router::ABSOLUTE_URL
                        )
                    ],
                    [
                        'name' => 'onboardingadminlink2',
                        'value' => $this->getContainer()->get('router')->generate(
                            'app_admin_onboarding_show',
                            ['id' => $onboarding->getId()],
                            Router::ABSOLUTE_URL
                        )
                    ],
                    [
                        'name' => 'createdat',
                        'value' => $onboarding->getCreatedAt()->getTimestamp() * 1000
                    ]
                ]
            ];

            if (empty($userDeals)) {
                if (($createResponse = $this->createDeal($user, $dealName, $newStage, self::SALE_PIPELINE_ID)) !== false) {
                    $dealId = $createResponse['dealId'];
                    $updateResponse = $this->updateDeal($dealId, $data);
                } else {
                    $output->writeln("Could not create deal for onboarding id `{$onboarding->getId()}`");
                    continue ;
                }
            } else {
                $dealId = $userDeals[0]['dealId'];
//                $currentStage = $userDeals[0]['properties']['dealstage']['value'];
//
//                if ($currentStage != $newStage) {
//                    if (($response = $this->changeDealStage($dealId, $newStage)) === false) {
//                        $output->writeln("Could not change deal stage for dealId `{$dealId}`");
//                        continue ;
//                    }
//                }
//
                $updateResponse = $this->updateDeal($dealId, $data);
            }

            if ($updateResponse !== false) {
                $output->writeln("Done for `{$onboarding->getId()}`");
                $onboarding->setHubspotSync(true);
                $manager->flush();
            } else {
                $output->writeln("Could not update the deal with id `$dealId` for onboarding with id `{$onboarding->getId()}`");
            }
        }

        $output->writeln('Done');
    }

    private function getOnboardingDealPropertyGroupData() {
        return [
            'name' => 'onboardinggroup',
            'displayName' => 'Onboarding',
            'properties' => [
                [
                    'description' => "Volume d'achat mensuel",
                    'label' => "Volume d'achat",
                    'fieldType' => 'text',
                    'formField' => true,
                    'type' => 'number',
                    'name' => 'turnover',
                    'readOnlyValue' => false,
                    'readOnlyDefinition' => false,
                    'hidden' => false,
                    'mutableDefinitionNotDeletable' => false,
                    'calculated' => false,
                    'externalOptions' => false,
                    'displayMode' => 'current_value'
                ],
                [
                    'description' => "Montant des produits saisis par l'utilisateur",
                    'label' => "Total utilisateur",
                    'fieldType' => 'text',
                    'formField' => true,
                    'type' => 'number',
                    'name' => 'userprice',
                    'readOnlyValue' => false,
                    'readOnlyDefinition' => false,
                    'hidden' => false,
                    'mutableDefinitionNotDeletable' => false,
                    'calculated' => false,
                    'externalOptions' => false,
                    'displayMode' => 'current_value'
                ],
                [
                    'description' => "Montant suggéré avec les tarifs FoodoMarket",
                    'label' => "Total FoodoMarket",
                    'fieldType' => 'text',
                    'formField' => true,
                    'type' => 'number',
                    'name' => 'suggestedprice',
                    'readOnlyValue' => false,
                    'readOnlyDefinition' => false,
                    'hidden' => false,
                    'mutableDefinitionNotDeletable' => false,
                    'calculated' => false,
                    'externalOptions' => false,
                    'displayMode' => 'current_value'
                ],
                [
                    'description' => "Fournisseur sélectionné",
                    'label' => "Fournisseur sélectionné",
                    'fieldType' => 'text',
                    'formField' => true,
                    'type' => 'string',
                    'name' => 'chosensupplier',
                    //'readOnlyValue' => false,
                    //'readOnlyDefinition' => false,
                    'hidden' => false,
                    //'mutableDefinitionNotDeletable' => false,
                    'calculated' => false,
                    'externalOptions' => false,
                    'displayMode' => 'current_value'
                ],
                [
                    'description' => "Lien vers le résultat de l'onboarding",
                    'label' => "Lien FrontOffice",
                    'fieldType' => 'text',
                    'formField' => false,
                    'type' => 'string',
                    'name' => 'onboardingfrontlink2',
                    'readOnlyValue' => false,
                    'readOnlyDefinition' => false,
                    'hidden' => false,
                    'mutableDefinitionNotDeletable' => false,
                    'calculated' => false,
                    'externalOptions' => false,
                    'displayMode' => 'current_value'
                ],
                [
                    'description' => "Lien vers le BO onboarding",
                    'label' => "Lien BackOffice",
                    'fieldType' => 'text',
                    'formField' => false,
                    'type' => 'string',
                    'name' => 'onboardingadminlink2',
                    'readOnlyValue' => false,
                    'readOnlyDefinition' => false,
                    'hidden' => false,
                    'mutableDefinitionNotDeletable' => false,
                    'calculated' => false,
                    'externalOptions' => false,
                    'displayMode' => 'current_value'
                ],
                [
                    'description' => "Date de création",
                    'label' => "Fait le",
                    'fieldType' => 'date',
                    'formField' => false,
                    'type' => 'datetime',
                    'name' => 'createdat',
                    'readOnlyValue' => false,
                    'readOnlyDefinition' => false,
                    'hidden' => false,
                    'mutableDefinitionNotDeletable' => false,
                    'calculated' => false,
                    'externalOptions' => false,
                    'displayMode' => 'current_value'
                ],
            ]
        ];
    }

    private function createOnboardingDealProperties($propertyName = null) {
        $data = $this->getOnboardingDealPropertyGroupData();

        foreach ($data['properties'] as $dealProperty) {
            if ($propertyName === null || $dealProperty['name'] === $propertyName) {
                $dealProperty['groupName'] = $data['name'];

                $ret = $this->postJson($this->buildUrl('properties/v1/deals/properties/'), $dealProperty);

                if ($ret[1] != 200) {
                    return false;
                }
            }
        }

        return true;
    }

    private function updateOnboardingDealProperties() {
        $data = $this->getOnboardingDealPropertyGroupData();

        foreach ($data['properties'] as $dealProperty) {
            $dealProperty['groupName'] = $data['name'];

            $ret = $this->putJson($this->buildUrl("properties/v1/deals/properties/named/{$dealProperty['name']}"), $dealProperty);

            if ($ret[1] == 404) {
                if ($this->createOnboardingDealProperties($dealProperty['name'])) {
                    continue ;
                } else {
                    return false;
                }
            }

            if ($ret[1] != 200) {
                try {
 /*                   $deleteRet = CurlClient::instance()->request('DELETE', $this->buildUrl("properties/v1/deals/properties/named/{$dealProperty['name']}"), [], [], false);
dump('DELETE', $deleteRet);
                    if ($deleteRet[1] != 204) {
                        return false;
                    }

                    $ret = $this->putJson($this->buildUrl("properties/v1/deals/properties/named/{$dealProperty['name']}"), $dealProperty);
dump('CREATE', $ret);
                    if ($ret[1] != 200) {
                        return false;
                    }*/
                } catch (Api $e) {
                    echo $e->getMessage();
                    return false;
                }

                return false;
            }
        }

        return true;
    }

    private function createOnboardingDealPropertyGroup() {
        $data = $this->getOnboardingDealPropertyGroupData();

        $ret = $this->postJson($this->buildUrl('properties/v1/deals/groups/'), $data);

        if ($ret[1] != 200) {
            return false;
        }

        return json_decode($ret[0], true);
    }

    private function updateOnboardingDealPropertyGroup() {
        $data = $this->getOnboardingDealPropertyGroupData();

        $ret = $this->putJson($this->buildUrl("properties/v1/deals/groups/named/{$data['name']}"), $data);

        if ($ret[1] != 200) {
            return false;
        }

        return json_decode($ret[0], true);
    }

    private function getOnboardingDealPropertyGroup() {
        try {
            $ret = CurlClient::instance()->request(
                'GET',
                $this->buildUrl("properties/v1/deals/groups/named/onboardinggroup",
                    [['name' => 'includeProperties', 'value' => 'true']]), [], [], false);

            if ($ret[1] != 200) {
                return null;
            }

            return json_decode($ret[0], true);
        } catch (Api $e) {
            echo $e->getMessage();
            return null;
        }
    }

    private function getSalePipelineDeals($user) {
        $userDeals = $this->getAssociatedDeals($user);
        $deals = [];

        foreach ($userDeals['deals'] as $deal) {
            if ($deal['properties']['pipeline']['value'] == self::SALE_PIPELINE_ID) {
                $deals[] = $deal;
            }
        }

        return $deals;
    }

    private function changeDealStage($dealId, $newStage) {
        return $this->updateDeal($dealId, ['properties' => [['name' => 'dealstage', 'value' => $newStage]]]);
    }

    private function updateDeal($dealId, $data) {
        $ret = $this->putJson($this->buildUrl("deals/v1/deal/$dealId"), $data);

        if ($ret[1] != 200) {
            return false;
        }

        return json_decode($ret[0], true);
    }

    private function createDeal(array $user, $dealname, $dealstage, $pipeline = 'default') {
        $data = [
            'associations' => [
                'associatedVids' => [
                    $user['vid']
                ],
//                'associatedCompanyIds' => [
//                    $user['companyId']
//                ],
            ],
            'properties' => [
                [
                    'name' => 'pipeline',
                    'value' => $pipeline
                ],
                [
                    'name' => 'dealstage',
                    'value' => $dealstage
                ],
                [
                    'name' => 'dealname',
                    'value' => $dealname
                ],
            ]
        ];

        $ret = $this->postJson($this->buildUrl('deals/v1/deal/'), $data);

        if ($ret[1] != 200) {
            return false;
        }

        return json_decode($ret[0], true);
    }

    private function buildUrl($endpoint, $params = array()) {
        $strParams = '';

        foreach ($params as $param) {
            $strParams .= "&{$param['name']}={$param['value']}";
        }

        return self::HUBSPOT_API_URL_BASE . "/$endpoint?hapikey=" . self::API_KEY . $strParams;
    }

    private function postJson($url, $data) {
        $data = json_encode($data);

        try {
            /** @noinspection PhpParamsInspection */
            return CurlClient::instance()->request('POST', $url, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ], $data, false);
        } catch (Api $e) {
            echo $e->getMessage();
            return null;
        }
    }

    private function putJson($url, $data) {
        $data = json_encode($data);

        try {
            /** @noinspection PhpParamsInspection */
            return CurlClient::instance()->request('PUT', $url, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ], $data, false);
        } catch (Api $e) {
            echo $e->getMessage();
            return null;
        }
    }

    private function getAssociatedDeals($user) {
        try {
            $ret = CurlClient::instance()->request(
                'GET',
                $this->buildUrl(
                    "/deals/v1/deal/associated/CONTACT/{$user['vid']}/paged",
                    [
                        ['name' => 'properties', 'value' => 'dealstage'],
                        ['name' => 'properties', 'value' => 'dealname'],
                        ['name' => 'properties', 'value' => 'pipeline'],
                    ]
                ),
                [],
                null,
                false
            );

            if ($ret[1] != 200) {
                return [];
            }

            return json_decode($ret[0], true);
        } catch (Api $e) {
            echo $e->getMessage();
            return [];
        }
    }

    private function getDealPipelines() {
        try {
            $ret = CurlClient::instance()->request('GET', $this->buildUrl('/deals/v1/pipelines'), [], null, false);

            if ($ret[1] != 200) {
                return [];
            }

            return json_decode($ret[0], true);
        } catch (Api $e) {
            echo $e->getMessage();
            return [];
        }
    }

    private function findHubspotUserByEmail($email) {
        try {
            $ret = CurlClient::instance()->request('GET', $this->buildUrl("contacts/v1/contact/email/$email/profile"), [], null, false);

            $body = $ret[0];
            $code = $ret[1];

            if ($code != 200) {
                return null;
            }

            return json_decode($body, true);

        } catch (Api $e) {
            echo $e->getMessage();
            return null;
        }
    }

    /**
     * @param string $email
     * @param null|string $name
     * @param null|string $telephone
     * @param null|string $restaurant
     * @return array|null
     */
    private function createOrUpdateContactByEmail(string $email, ?string $name, ?string $telephone, ?string $restaurant) {
        try {
            $data = ['properties' => []];

            if ($name !== null) {
                $data['properties'][] = [
                    'property' => 'firstname',
                    'value' => $name
                ];
            }

            if ($telephone !== null) {
                $data['properties'][] = [
                    'property' => 'phone',
                    'value' => $telephone
                ];
            }

            if ($restaurant !== null) {
                $data['properties'][] = [
                    'property' => 'company',
                    'value' => $restaurant
                ];
            }

            $data = json_encode($data);

            /** @noinspection PhpParamsInspection */
            $ret = CurlClient::instance()->request(
                'POST',
                $this->buildUrl("contacts/v1/contact/createOrUpdate/email/$email"),
                array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data)
                ),
                $data,
                false
            );

            if ($ret[1] != 200)
                return null;

            return json_decode($ret[0], true);
        } catch (Api $e) {
            echo $e->getMessage();
            return null;
        }
    }
}