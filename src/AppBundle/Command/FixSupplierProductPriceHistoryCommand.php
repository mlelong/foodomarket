<?php

namespace AppBundle\Command;

use AppBundle\Entity\SupplierProductPrice;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FixSupplierProductPriceHistoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:fix:supplier-product-price-history')->addOption('accept', null, InputOption::VALUE_NONE);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierProductPriceHistoryRepository = $this->getContainer()->get('app.repository.supplier_product_price_history');
        $pricesIterator = $supplierProductPriceHistoryRepository->createQueryBuilder('spp')->getQuery()->iterate();
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $idsToUpdate = [];

        $output->writeln("History prices fetched");

        /** @var SupplierProductPrice[] $row */
        foreach ($pricesIterator as $row) {
            $proceed = true;
            $product = $row[0]->getProduct();
            $variant = $row[0]->getProductVariant();

            if ($variant === null) {
                $output->writeln("History price #{$row[0]->getId()} has no product variant set");
                $proceed = false;
            }

            if ($product === null) {
                $output->writeln("History price #{$row[0]->getId()} has no product set");
                $proceed = false;
            }

            if ($proceed && $variant->getProduct()->getId() != $product->getId()) {
                $idsToUpdate[] = $row[0]->getId();
                $explanationMessage = "{$variant->getInternalName()} should be associated with ({$variant->getProduct()->getId()}) {$variant->getProduct()->getName()} instead of ({$product->getId()}) {$product->getName()}";
                $output->writeln("History price #{$row[0]->getId()} marked for update: $explanationMessage");
            }

            $manager->detach($row[0]);
        }

        $nbIdsToUpdate = count($idsToUpdate);

        if ($nbIdsToUpdate > 0) {
            if ($input->getOption('accept')) {
                $output->writeln("\n$nbIdsToUpdate history prices need to be updated. Proceeding...");

                /** @var SupplierProductPrice[] $pricesToUpdate */
                $pricesToUpdate = $supplierProductPriceHistoryRepository->findBy(['id' => $idsToUpdate]);

                foreach ($pricesToUpdate as $priceToUpdate) {
                    $exists = $supplierProductPriceHistoryRepository->count([
                        'supplier' => $priceToUpdate->getSupplier(),
                        'restaurant' => $priceToUpdate->getRestaurant(),
                        'product' => $priceToUpdate->getProductVariant()->getProduct(),
                        'productVariant' => $priceToUpdate->getProductVariant(),
                        'createdAt' => $priceToUpdate->getCreatedAt()
                    ]) > 0;

                    if ($exists) {
                        $output->writeln("Price history already exists, removing...");
                        $manager->remove($priceToUpdate);

                        try {
                            $manager->flush($priceToUpdate);
                        } catch (OptimisticLockException $e) {
                            $output->writeln("oO Exception: {$e->getMessage()}");
                            return -1;
                        }
                    } else {
                        $priceToUpdate->setProduct($priceToUpdate->getProductVariant()->getProduct());

                        try {
                            $manager->flush($priceToUpdate);
                        } catch (OptimisticLockException $e) {
                            $output->writeln("oO Exception: {$e->getMessage()}");
                            return -1;
                        }
                    }
                }

                $output->writeln("\nDone");
            } else {
                $output->writeln("\n$nbIdsToUpdate history prices need to be updated:");
                $output->writeln(implode(', ', $idsToUpdate));
                $output->writeln("Re-run with --accept to write changes to database");
            }
        } else {
            $output->writeln("Everything's Fine");
        }

        return 0;
    }
}