<?php


namespace AppBundle\Command;


use AppBundle\Entity\Order;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\OrderRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\HarelSystems\HarelSystemsService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestHarelSystemsApiCommand extends ContainerAwareCommand
{
    /** @var HarelSystemsService */
    private $harelSystemsService;
    /**
     * @var SupplierRepository|object
     */
    private $supplierRepository;
    /**
     * @var ProspectRepository|object
     */
    private $prospectRepository;
    /**
     * @var OrderRepository|object
     */
    private $orderRepository;

    protected function configure()
    {
        $this
            ->setName('app:test:harel-systems')
            ->addOption('supplier-id', null, InputOption::VALUE_REQUIRED, 'Supplier Id : 36')
            ->addOption('prospect-id', null, InputOption::VALUE_REQUIRED, 'Prospect Id : 1')
            ->addOption('action', null, InputOption::VALUE_REQUIRED, 'What to do : getcustomers')
            ->addOption('offset', null, InputOption::VALUE_REQUIRED, 'offset')
            ->addOption('product-id', null, InputOption::VALUE_REQUIRED, 'product-id')
            ->addOption('packaging-id', null, InputOption::VALUE_REQUIRED, 'packaging-id')
            ->addOption('order-id', null, InputOption::VALUE_REQUIRED, 'order-id')
            ->addOption('name', null, InputOption::VALUE_REQUIRED, 'name')
            ->addOption('fullname', null, InputOption::VALUE_REQUIRED, 'fullname')
            ->addOption('code', null, InputOption::VALUE_REQUIRED, 'code')
            ->addOption('phone', null, InputOption::VALUE_REQUIRED, 'phone')
            ->addOption('email', null, InputOption::VALUE_REQUIRED, 'email')
            ->setHelp(
                '<info>This command\'s purpose is to call harel systems api</info>
                
                Example : php bin/console app:test:harel-systems --supplier-id=36 --action=getcustomers 
                '
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->harelSystemsService = $this->getContainer()->get('app.service.harelsystems');
        $this->supplierRepository = $this->getContainer()->get('app.repository.supplier');
        $this->prospectRepository = $this->getContainer()->get('app.repository.prospect');
        $this->orderRepository = $this->getContainer()->get('sylius.repository.order');

        $supplierId = $input->getOption('supplier-id');
        $prospectId = $input->getOption('prospect-id');
        $action = $input->getOption('action');
        $offset = $input->getOption('offset');
        $productId = $input->getOption('product-id');
        $packagingId = $input->getOption('packaging-id');
        $orderId = $input->getOption('order-id');

        $name = $input->getOption('name');
        $fullname = $input->getOption('fullname');
        $code = $input->getOption('code');
        $email = $input->getOption('email');
        $phone = $input->getOption('phone');

        /** @var Supplier $supplier */
        //$supplier = $this->supplierRepository->find($supplierId);

        $this->harelSystemsService->setSupplier($supplierId);

        $output->writeln("Testing Harel Systems api");

        if (is_null($action)) {
            $action = 'getcustomers';
        }

        if (is_null($offset)) {
            $offset = 0;
        }

        $data = 'no-data';

        switch($action) {
            case 'getcustomers':
                $callable = function() use ($offset) {
                    return $this->harelSystemsService->getCustomers($offset);
                };
                break;
            case 'getcustomer':
                $callable = function() use($prospectId) {
                    return $this->harelSystemsService->getCustomer($prospectId);
                };
                break;
            case 'searchcustomer':
                $callable = function() use($name, $code) {
                    return $this->harelSystemsService->searchCustomer($name, $code);
                };
                break;
            case 'identifycustomer':
                $callable = function() use($name, $fullname, $code, $phone, $email) {
                    return $this->harelSystemsService->identifyCustomer($name, $fullname, $code, $phone, $email);
                };
                break;
            case 'exportproduct':
                $callable = function() use($name, $code) {
                    return $this->harelSystemsService->exportProduct($name, $code);
                };
                break;
            case 'searchproduct':
                $callable = function() use($name, $code) {
                    return $this->harelSystemsService->searchProduct($name, $code);
                };
                break;
            case 'identifyproduct':
                $callable = function() use($name, $code) {
                    return $this->harelSystemsService->identifyProduct($name, $code);
                };
                break;
            case 'getpackagings':
                $callable = function() use ($offset)  {
                    return $this->harelSystemsService->getPackagings($offset);
                };
                break;
            case 'getpackaging':
                $callable = function() use($packagingId) {
                    return $this->harelSystemsService->getPackaging($packagingId);
                };
                break;
            case 'getproducts':
                $callable = function() use ($offset)  {
                    return $this->harelSystemsService->getProducts($offset);
                };
                break;
            case 'getproduct':
                $callable = function() use($productId) {
                    return $this->harelSystemsService->getProduct($productId);
                };
                break;
            case 'getprices':
                $callable = function() use($prospectId, $productId) {
                    return $this->harelSystemsService->getPrices($prospectId, $productId);
                };
                break;
            case 'createcustomer':
                /** @var Prospect $prospect */
                $prospect = $this->prospectRepository->find($prospectId);
                $output->writeln("Creating prospect ".$prospect->getRestaurantName());
                $callable = function() use($prospect) {
                    return $this->harelSystemsService->createCustomer($prospect);
                };
                break;
            case 'createorder':
                /** @var Order $order */
                $order = $this->orderRepository->find($orderId);
                $output->writeln("Creating order for ".$order->getShoppingCart()->getRestaurant()->getName());
                $callable = function() use($order) {
                    return $this->harelSystemsService->createOrder($order);
                };
                break;
            default:
                $output->writeln("Unknown action, please check");
                return;
                break;
        }

        dump('RESULTS', $this->handleResponse($callable, $output));
    }

    /**
     * @param callable $callable
     * @param OutputInterface $output
     * @return array|null|false
     */
    private function handleResponse(callable $callable, OutputInterface $output)
    {
        try {
            $response = $callable();

            $responseString = \GuzzleHttp\json_encode($response);
            $output->writeln("OK => [$responseString]\n");

            return $response;
        } catch (\Exception $e) {
            $output->writeln("KO => ".$e->getMessage());

            return false;
        }
    }
}