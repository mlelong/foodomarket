<?php


namespace AppBundle\Command;


use AppBundle\Entity\OrderPayment;
use AppBundle\Repository\OrderPaymentRepository;
use AppBundle\Services\Payment\Lyra\Client as ClientLyra;
use AppBundle\Services\Payment\Lyra\Exceptions\LyraException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class SyncLyraPaymentStatusCommand extends Command
{
    public static $defaultName = 'app:sync:lyra-payment-status';

    /**
     * @var ClientLyra
     */
    private $clientLyra;

    /**
     * @var OrderPaymentRepository
     */
    private $orderPaymentRepository;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(ClientLyra $clientLyra, OrderPaymentRepository $orderPaymentRepository, EntityManagerInterface $manager)
    {
        parent::__construct();

        $this->clientLyra = $clientLyra;
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->manager = $manager;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var OrderPayment[] $payments */
        $payments = $this->orderPaymentRepository->findAll();
        $uuidCache = [];

        foreach ($payments as $payment) {
            try {
                $output->write("Fetching transaction data for uuid: {$payment->getLyraId()}...");

                if (!isset($uuidCache[$payment->getLyraId()])) {
                    $uuidCache[$payment->getLyraId()] = $this->clientLyra->get("orders/{$payment->getLyraId()}/transactions");
                }

                $ret = $uuidCache[$payment->getLyraId()];

                $output->write(" OK!");

                if ($ret['count'] > 0) {
                    $output->write(" Saving transaction...");
                    $transaction = $ret['results'][0];

                    $this->manager->transactional(function(EntityManagerInterface $manager) use($payment, $transaction) {
                        // Save transaction status and data
                        $payment->setLyraPaymentStatus($transaction['status']);
                        $payment->setLyraPaymentData($transaction);

                        // Update order payment status accordingly
                        $order = $payment->getOrder();

                        $order->setPaymentState($transaction['status']);
                    });

                    $output->writeln(" OK!");
                } else {
                    $output->writeln(" Nothing found, continue");
                }
            }catch (LyraException $e) {
                $output->writeln(" KO! : {$e->getMessage()}");
            } catch (Throwable $e) {
                $output->writeln(" KO! : {$e->getMessage()}");
            }
        }
    }
}