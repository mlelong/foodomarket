<?php

namespace AppBundle\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

trait CommandTrait
{
    /** @var OutputInterface */
    protected $output;

    /** @var LoggerInterface */
    protected $logger;

    /** @var bool */
    protected $debug;

    /** @var ProgressBar */
    protected $progress;

    /**
     * @required
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function initializeDebug(OutputInterface $output, $debug = false, $max = 0)
    {
        $this->debug = $debug;
        $this->output = $output;

        if ($this->debug) {
            $this->output->writeln('Initialize...');
            return;
        }

        // create a new progress bar
        if ($this->output) {
            $this->progress = new ProgressBar($this->output, $max);
            $this->progress->setEmptyBarCharacter(' ');
            $this->progress->setBarCharacter('.');
            $this->progress->setProgressCharacter('S');
            $this->progress->setFormat('verbose');
            $this->progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% | %elapsed:6s% | %memory:6s% | %message%');
        }
    }

    protected function start($max = null)
    {
        $message = 'Task starts at '.date('d/m/Y H:i:s');

        if ($this->debug) {
            $this->output->writeln($message);
            return;
        }
        // start and displays the progress bar
        $this->progress->setMessage($message);
        $this->progress->start($max);
    }

    protected function finish()
    {
        $message = 'Task is finished at '.date('d/m/Y H:i:s');

        if ($this->debug) {
            $this->output->writeln($message);
            return;
        }
        // ensure that the progress bar is at 100%
        $this->progress->setMessage($message);
        $this->progress->finish();
        $this->output->writeln("\n\n ".$message."!\n");
    }

    protected function advance($message = '', $step = 1)
    {
        if ($this->debug) {
            if ($message) {
                $this->output->writeln($message);
            }
            return;
        }

        // advance the progress bar 1 unit
        if ($message) {
            $this->progress->setMessage('In progress : '.$message);
        } else {
            $this->progress->setMessage('In progress');
        }

        $this->progress->advance($step);
    }

    protected function info($message)
    {
        if ($this->debug) {
            $this->output->writeln($message);
        }
        if ($this->output) {
            $this->logger->info($message);
        }
    }
    protected function critical($message)
    {
        if ($this->debug) {
            $this->output->writeln($message);
        }
        if ($this->output) {
            $this->logger->critical($message);
        }
    }
}
