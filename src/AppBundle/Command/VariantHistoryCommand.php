<?php

namespace AppBundle\Command;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductVariantInformations;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VariantHistoryCommand extends ContainerAwareCommand
{
    use CommandTrait;

    protected function configure()
    {
        $this
            ->setName('app:variant:history')
            ->addArgument('supplier', InputArgument::REQUIRED)
            ->addArgument('variant', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supplierId = $input->getArgument('supplier');
        $variantId = $input->hasArgument('variant') ? $input->getArgument('variant') : null;

        /** @var Supplier|null $supplier */
        $supplier = $this->getContainer()->get('app.repository.supplier')->find($supplierId);

        if ($supplier === null) {
            $this->critical("No supplier found for id `$supplierId`");
            return ;
        }

        $supplierProductPriceHistoryRepository = $this->getContainer()->get('app.repository.supplier_product_price_history');

        if ($variantId !== null) {
            /** @var ProductVariant $variant */
            $variant = $this->getContainer()->get('sylius.repository.product_variant')->find($variantId);

            if ($variant === null) {
                $this->critical("No variant found with id `$variantId`");
                return ;
            }

            $prices = $supplierProductPriceHistoryRepository->findBy(
                ['supplier' => $supplier, 'productVariant' => $variant, 'restaurant' => 4],
                ['createdAt' => 'ASC', 'id' => 'ASC']
            );

            if (empty($prices)) {
                $this->critical("No prices found for supplier `{$supplier->getName()}` and variant `{$variant->getFullDisplayName()}`");
                return ;
            }

            $history = $this->computeHistoryForVariant($supplier, $variant);
        } else {
            $history = $this->computeHistory($supplier);
        }

        foreach ($history as $key => $h) {
            if ($h > 1) {
                $output->writeln("Multiple active variants for {$key}");
            }
        }
    }

    private function computeHistoryForVariant(Supplier $supplier, ProductVariant $variant)
    {
        $currentPrices = $this->getContainer()->get('app.repository.supplier_product_price')->findBy([
            'product' => $variant->getProduct(),
            'supplier' => $supplier,
            'restaurant' => 4
        ]);

        return count($currentPrices);
    }

    private function computeHistory(Supplier $supplier)
    {
        /** @var SupplierProductVariantInformations[] $spvis */
        $spvis = $this->getContainer()->get('app.repository.supplier_product_variant_informations')->findBy(
            ['supplier' => $supplier]
        );

        $history = [];

        foreach ($spvis as $spvi) {
            if ($spvi->getSupplier() === null || $spvi->getProductVariant() === null) {
                continue ;
            }

            $history["{$spvi->getProduct()->getId()}-{$spvi->getProductVariant()->getId()}"] = $this->computeHistoryForVariant($supplier, $spvi->getProductVariant());
        }

        ksort($history);

        return $history;
    }
}