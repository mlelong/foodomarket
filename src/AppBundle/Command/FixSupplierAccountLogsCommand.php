<?php


namespace AppBundle\Command;


use AppBundle\Entity\Prospect;
use AppBundle\Entity\SupplierAccountLog;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FixSupplierAccountLogsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:fix:supplier-account-logs')
            ->addOption('debug', null, InputOption::VALUE_NONE)
            ->setDescription('This command will add SupplierAccountLogs to prospects that just have their supplierCategories bound without supplier account logs. It will make them sync')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $prospectRepository = $this->getContainer()->get('app.repository.prospect');
        $supplierAccountLogRepository = $this->getContainer()->get('app.repository.supplier_account_log');
        $supplierCategoryRepository = $this->getContainer()->get('app.repository.supplier_category');
        $qb = $prospectRepository->createQueryBuilder('p');
        $debug = $input->getOption('debug');

        $output->writeln("Debug mode " . ($debug ? 'On' : 'Off'));

        /** @var Prospect[] $prospectBound */
        $prospectBound = $qb
            ->join('p.suppliers', 's')
            ->getQuery()
            ->getResult()
        ;
        $nbProspectsBound = count($prospectBound);

        $output->writeln("Phase 1 : bound prospects to supplier account logs\n");

        $output->writeln("\tFound {$nbProspectsBound} prospects bound to suppliers");

        foreach ($prospectBound as $prospect) {
            $supplierCategories = $prospect->getSuppliers();
            /** @var SupplierAccountLog[] $supplierAccountLogs */
            $supplierAccountLogs = $supplierAccountLogRepository->findBy(['prospect' => $prospect]);

            foreach ($supplierCategories as $supplierCategory) {
                $found = false;

                foreach ($supplierAccountLogs as $supplierAccountLog) {
                    if ($supplierCategory->getId() === $supplierAccountLog->getSupplierCategory()->getId()) {
                        $found = true;
                        break ;
                    }
                }

                if (!$found) {
                    $output->write("\t\tProspect `{$prospect->getEmail()}` is bound to {$supplierCategory} without an associated supplier account log. Creating...");

                    // Adds the supplier account log
                    $supplierAccountLog = new SupplierAccountLog();
                    $supplierAccountLog->setProspect($prospect);
                    $supplierAccountLog->setSupplierCategory($supplierCategory);
                    $supplierAccountLog->setStatus(SupplierAccountLog::STATUS_ACCOUNT_OPENED);

                    if (!$debug) {
                        $supplierAccountLogRepository->add($supplierAccountLog);
                    }

                    $output->writeln(" Done");
                }
            }
        }

        $output->writeln("Phase 2 : add missing supplier categories to prospect");

        $supplierAccountLogs = $supplierAccountLogRepository->findBy([], ['prospect' => 'ASC']);
        $logs = [];

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            $prospect = $supplierAccountLog->getProspect();
            $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();

            if ($prospect === null || $supplier === null) {
                $output->writeln("/!\ Severe problem !!! ====> {$supplierAccountLog->getId()}");
                continue ;
            }

            $logs[$prospect->getId()][$supplier->getId()][] = $supplierAccountLog;
        }

        foreach ($logs as $prospectId => $prospectLogs) {
            foreach ($prospectLogs as $supplierId => $supplierAccountLogs) {
                $supplierCategories = $supplierCategoryRepository->findBySupplier($supplierId);
                $nbSupplierAccountLogs = count($supplierAccountLogs);
                $nbSupplierCategories = count($supplierCategories);
                $nbMissing = $nbSupplierCategories - $nbSupplierAccountLogs;

                if ($nbMissing != 0) {
                    $output->writeln("\tProspect `{$prospectId}` is missing {$nbMissing} supplier categories for supplier `{$supplierId}`");
                    $missing = [];

                    foreach ($supplierCategories as $supplierCategory) {
                        $found = false;

                        foreach ($supplierAccountLogs as $supplierAccountLog) {
                            if ($supplierAccountLog->getSupplierCategory()->getId() === $supplierCategory->getId()) {
                                $found = true;
                                break ;
                            }
                        }

                        if (!$found) {
                            $missing[] = $supplierCategory;
                        }
                    }

                    foreach ($missing as $miss) {
                        $output->write("\t\t$miss is missing. Creating...");

                        $prospect = $prospectRepository->find($prospectId);
                        $supplierAccountLog = new SupplierAccountLog();
                        $supplierAccountLog->setSupplierCategory($miss);
                        /** @noinspection PhpParamsInspection */
                        $supplierAccountLog->setProspect($prospect);
                        $supplierAccountLog->setStatus(SupplierAccountLog::STATUS_WAITING);

                        if (!$debug) {
                            $supplierAccountLogRepository->add($supplierAccountLog);
                        }

                        $output->writeln(" OK");
                    }
                }
            }
        }

        $output->writeln("Phase 3 : fix supplier account logs state (if at least one is account_opened and the others for a same supplier are waiting, update their state accordingly)\n");

        $supplierAccountLogs = $supplierAccountLogRepository->findBy([], ['prospect' => 'ASC']);
        $logs = [];

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            $prospect = $supplierAccountLog->getProspect();
            $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();

            if ($prospect === null || $supplier === null) {
                $output->writeln("/!\ Severe problem !!! ====> {$supplierAccountLog->getId()}");
                continue ;
            }

            $logs[$prospect->getId()][$supplier->getId()][] = $supplierAccountLog;
        }

        $toUpdate = [];

        foreach ($logs as $prospectId => $prospectLogs) {
            foreach ($prospectLogs as $supplierId => $supplierAccountLogs) {
                $status = null;

                foreach ($supplierAccountLogs as $supplierAccountLog) {
                    if ($status === null) {
                        $status = $supplierAccountLog->getStatus();
                        continue ;
                    }

                    if ($status !== $supplierAccountLog->getStatus()) {
                        $toUpdate["{$prospectId}-{$supplierId}"] = [
                            'prospect' => $supplierAccountLog->getProspect(),
                            'supplier' => $supplierAccountLog->getSupplierCategory()->getSupplier()
                        ];
                    }
                }
            }
        }

        $nbToUpdate = count($toUpdate);

        $output->writeln("\tFound {$nbToUpdate} inconsistencies between SupplierAccountLogs for a same prospect and supplier");

        foreach ($toUpdate as $prospectSupplier) {
            $supplierAccountLogs = $supplierAccountLogRepository->getSupplierAccountLogs($prospectSupplier['prospect'], $prospectSupplier['supplier']);
            $statuses = [];
            $retainedStatus = null;

            foreach ($supplierAccountLogs as $supplierAccountLog) {
                $status = $supplierAccountLog->getStatus();
                $statuses[] = $status;

                if ($retainedStatus === null) {
                    $retainedStatus = $supplierAccountLog;
                }

                if (in_array($status, [SupplierAccountLog::STATUS_ACCOUNT_OPENED, SupplierAccountLog::STATUS_ORDERING, SupplierAccountLog::STATUS_ACCOUNT_BLOCKED])) {
                    $retainedStatus = $supplierAccountLog;
                }
            }

            $statuses = implode(', ', array_unique($statuses));

            $output->write("\t\t{$prospectSupplier['prospect']->getEmail()} is bound to {$prospectSupplier['supplier']->getDisplayName(true)} and has these distinct statuses: [$statuses]. Fixing all to status {$retainedStatus->getStatus()}...");

            if (!$debug) {
                if ($supplierAccountLogRepository->updateAllAccordingTo($retainedStatus)) {
                    $output->writeln(" OK");
                } else {
                    $output->writeln(" ERROR");
                }
            } else {
                $output->writeln(" OK");
            }
        }

        $output->writeln("\nPhase 4 : add the supplier categories corresponding to supplier account logs to the prospects so that they can see the products without the possibility to order\n");

        /** @var SupplierAccountLog[] $supplierAccountLogs */
        $supplierAccountLogs = $supplierAccountLogRepository->findAll();
        $prospectManager = $this->getContainer()->get('app.manager.prospect');

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            $prospect = $supplierAccountLog->getProspect();

            if ($prospect === null) {
                $output->writeln("/!\ Severe problem !!! ====> {$supplierAccountLog->getId()}");
                continue ;
            }

            if (!$prospect->getSuppliers()->contains($supplierAccountLog->getSupplierCategory())) {
                $output->writeln("\tBinding prospect `{$prospect->getEmail()}` to supplier category `{$supplierAccountLog->getSupplierCategory()}`");

                if (!$debug) {
                    $prospect->addSupplier($supplierAccountLog->getSupplierCategory());
                    $prospectManager->flush($prospect);
                }
            }
        }

        $output->writeln("\nDone");
    }
}