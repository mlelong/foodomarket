<?php

namespace AppBundle\Command;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\ShopUserService;
use AppBundle\Util\SupplierUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BatchOpenSupplierAccountCommand extends Command
{
    public static $defaultName = 'app:batch-open:supplier-account';

    /**
     * @var ProspectRepository
     */
    private $prospectRepository;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    /**
     * @var SupplierAccountLogRepository
     */
    private $supplierAccountLogRepository;

    /**
     * @var ShopUserService
     */
    private $shopUserService;

    public function __construct(
        ProspectRepository $prospectRepository,
        SupplierRepository $supplierRepository,
        SupplierAccountLogRepository $supplierAccountLogRepository,
        ShopUserService $shopUserService
    ) {
        parent::__construct();
        $this->prospectRepository = $prospectRepository;
        $this->supplierRepository = $supplierRepository;
        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
        $this->shopUserService = $shopUserService;
    }

    public function configure()
    {
        $this
            ->addArgument('zip', InputArgument::REQUIRED, '')
            ->addArgument('firstname', InputArgument::REQUIRED, '')
            ->addArgument('type', InputArgument::REQUIRED, '')
            ->addArgument('email', InputArgument::REQUIRED, '')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $prospects = $this->fetchProspects(
            $input->getArgument('zip'),
            $input->getArgument('firstname'),
            $input->getArgument('type'),
            $input->getArgument('email')
        );

//        $nb = count($prospects);
//        $output->writeln("$nb");
//        return 0;

        $partners = $this->supplierRepository->getPartners(false);
        $individualPartners = $this->supplierRepository->getIndividualsPartners();

        foreach ($prospects as $prospect) {
            /** @var SupplierAccountLog[] $supplierAccountLogs */
            $supplierAccountLogs = $this->supplierAccountLogRepository->findBy(['prospect' => $prospect]);
            $suppliers = [];

            foreach ($supplierAccountLogs as $supplierAccountLog) {
                $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();

                $suppliers[$supplier->getId()] = [
                    'name' => $supplier->getDisplayName(true),
                    'status' => $supplierAccountLog->getStatus()
                ];
            }

            $retainedPartners = array_filter(
                $prospect->getType() === Prospect::TYPE_INDIVIDUAL ? $individualPartners : $partners,
                function($partner) use($suppliers, $prospect) {
                    $facade = $partner->getFacadeSupplier();

                    // If it is HP, then auto bound only the SP grid
                    if ($facade->getId() == 11 && $partner->getId() != 23) {
                        return false;
                    }

                    if ($prospect->getType() === Prospect::TYPE_PROFESSIONNAL && !$facade->isAutoBind()) {
                        return false;
                    }

                    return !isset($suppliers[$partner->getId()]) && !empty($prospect->getZipCode())
                        && SupplierUtil::getDeliveryCost($partner, $prospect->getZipCode()) !== null
                    ;
                }
            );

            foreach ($retainedPartners as $partner) {
                if ($prospect->getType() === Prospect::TYPE_INDIVIDUAL) {
                    $this->shopUserService->openSupplierAccount($prospect, $partner, SupplierAccountLog::STATUS_ACCOUNT_OPENED);
                } else {
                    $facade = $partner->getFacadeSupplier();

                    $this->shopUserService->openSupplierAccount(
                        $prospect,
                        $partner->getId(),
                        $facade->isPaymentOnline() ? SupplierAccountLog::STATUS_ORDERING : SupplierAccountLog::STATUS_WAITING
                    );
                }
            }

            $isBound = !empty($retainedPartners);

            if ($isBound) {
                $this->shopUserService->sendNewlyBoundNotification($prospect, $retainedPartners);
            }
        }
    }

    /**
     * @param string|null $zip
     * @param string|null $firstname
     * @param string|null $type
     * @param string|null $email
     * @param int $offset
     * @param int $limit
     * @return Prospect[]
     */
    private function fetchProspects(?string $zip = null, ?string $firstname = null, ?string $type = null, ?string $email = null, ?int $offset = null, ?int $limit = null)
    {
        $qb = $this->prospectRepository->createQueryBuilder('p');

        if ($zip) {
            $qb
                ->andWhere($qb->expr()->like('p.zipCode', ':zip'))
                ->setParameter('zip', "$zip%")
            ;
        }

        if ($firstname) {
            $qb
                ->andWhere($qb->expr()->like('p.firstName', ':firstname'))
                ->setParameter('firstname', $firstname)
            ;
        }

        if ($type) {
            $qb
                ->andWhere('p.type = :type')
                ->setParameter('type', $type)
            ;
        }

        if ($email) {
            $qb
                ->andWhere('p.email = :email')
                ->setParameter('email', $email)
            ;
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $qb->orderBy('p.id', 'desc');

        return $qb->setMaxResults($limit)->getQuery()->getResult();
    }
}