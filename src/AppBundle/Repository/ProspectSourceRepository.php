<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\ProspectSource;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ProspectSourceRepository extends EntityRepository
{
    /**
     * @param string $ids Comma separated list of prospect source ids
     * @return ProspectSource[]
     */
    public function findByIds(string $ids)
    {
        return $this->createQueryBuilder('ps')->where("ps.id IN ($ids)")->getQuery()->getResult();
    }

    /**
     * @param Prospect $prospect
     * @return ProspectSource[]
     */
    public function getSources(Prospect $prospect)
    {
        return $this->findBy(['prospect' => $prospect], ['createdAt' => 'ASC']);
    }
}
