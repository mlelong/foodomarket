<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ShoppingCart;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ShoppingCartRepository extends EntityRepository
{
    public function update(ShoppingCart $shoppingCart)
    {
        try {
            $this->_em->flush($shoppingCart);

            return true;
        } catch (OptimisticLockException $e) {
            return false;
        } catch (ORMException $e) {
            return false;
        }
    }
}