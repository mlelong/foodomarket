<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use DateTime;

class InvoiceRepository extends EntityRepository
{
    /**
     * @param Prospect $prospect
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countInvoice(Prospect $prospect){

        return $this->createQueryBuilder('invoice')
            ->select('COUNT(invoice)')
            ->where('invoice.prospect = :prospect')
            ->setParameter('prospect', $prospect->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Prospect $prospect
     * @param DateTime $start
     * @param DateTime $end
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countInvoiceBetweenDate(Prospect $prospect, DateTime $start, DateTime $end){
        return $this->createQueryBuilder('invoice')
            ->select('COUNT(invoice)')
            ->where('invoice.prospect = :prospect')
            ->andWhere('invoice.date BETWEEN :start AND :end')
            ->setParameter('prospect', $prospect->getId())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Supplier $supplier
     * @param Prospect $prospect
     * @return array
     */
    public function getInvoiceDateRange(Supplier $supplier, Prospect $prospect)
    {
        try {
            $minDate = $this->createQueryBuilder('invoice')
                ->select('idn.id, MIN(idn.date) AS minDate')
                ->join('invoice.invoiceDeliveryNotes', 'idn')
                ->where('invoice.prospect = :prospect')
                ->andWhere('invoice.supplier = :supplier')
                ->setParameter('prospect', $prospect)
                ->setParameter('supplier', $supplier->getFacadeSupplier())
                ->groupBy('idn.id')
                ->orderBy('idn.date', 'ASC')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            $minDate = null;
        } catch (NonUniqueResultException $e) {
            $minDate = null;
        }

        try {
            $maxDate = $this->createQueryBuilder('invoice')
                ->select('idn.id, MAX(idn.date) AS maxDate')
                ->join('invoice.invoiceDeliveryNotes', 'idn')
                ->where('invoice.prospect = :prospect')
                ->andWhere('invoice.supplier = :supplier')
                ->setParameter('prospect', $prospect)
                ->setParameter('supplier', $supplier->getFacadeSupplier())
                ->groupBy('idn.id')
                ->orderBy('idn.date', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            $maxDate = null;
        } catch (NonUniqueResultException $e) {
            $maxDate = null;
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        return [
            'min' => !empty($minDate) ? new DateTime($minDate['minDate']) : null,
            'max' => !empty($maxDate) ? new DateTime($maxDate['maxDate']) : null,
            'minId' => !empty($minDate) ? $minDate['id'] : null,
            'maxId' => !empty($maxDate) ? $maxDate['id'] : null,
        ];
    }

    /**
     * @param DateTime|null $minDate
     * @param DateTime|null $maxDate
     * @return Prospect[]
     */
    public function getProspectsThatHaveAlreadyBeenInvoiced(DateTime $minDate = null, DateTime $maxDate = null)
    {
        $qb = $this->createQueryBuilder('i');

        $qb
            ->select('p')
            ->join(Prospect::class, 'p', 'WITH', 'p = i.prospect')
        ;

        if ($minDate !== null) {
            $qb
                ->andWhere('i.date >= :minDate')
                ->setParameter('minDate', $minDate)
            ;
        }

        if ($maxDate !== null) {
            $qb
                ->andWhere('i.date <= :maxDate')
                ->setParameter('maxDate', $maxDate)
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
