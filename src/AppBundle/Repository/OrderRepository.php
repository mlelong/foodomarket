<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use Behat\Behat\Output\Statistics\Statistics;
use DateTime;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\OrderRepository as BaseOrderRepository;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\ProductTaxon;

/**
 * OrderRepository
 *
 */
class OrderRepository extends BaseOrderRepository
{
    /**
     * {@inheritdoc}
     */
    public function createListQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('o')
            ->addSelect('channel')
            ->addSelect('customer')
            ->innerJoin('o.channel', 'channel')
            ->leftJoin('o.customer', 'customer')
            ->andWhere('o.state != :state')
            ->andWhere('o.shoppingCart IS NOT NULL')
            ->setParameter('state', OrderInterface::STATE_CART)
            ;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param Prospect $prospect
     * @param bool $groupBySupplier
     * @param bool $groupByRestaurant
     * @return Statistics[]
     */

    public function getTopSales($startDate, $endDate, $prospect = null, $groupBySupplier = false, $groupByRestaurant = false)
    {
        $query = $this->createQueryBuilder('o')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->orderBy('mois_commande', 'asc')
            ->addOrderBy('montant_total', 'desc')
            ->setParameter('start', $startDate)
            ->setParameter('end', $endDate);

        if ($prospect) {
            $query
                ->andWhere('p.id = :prospect')
                ->setParameter('prospect', $prospect);
        }

        if ($groupByRestaurant) {
            if ($groupBySupplier) {
                $query
                    ->select('SUBSTRING(o.createdAt, 1, 7) as mois_commande, channel.id as id, channel.name as grossiste, COUNT(distinct c.id) as total_client, COUNT(o.id) as total_commande, SUM(o.itemsTotal/100) as montant_total')
                    ->groupBy('mois_commande, id, grossiste')
                    ->join('o.channel', 'channel');
            } else {
                $query
                    ->select('SUBSTRING(o.createdAt, 1, 7) as mois_commande, COUNT(o.id) as total_commande, SUM(o.itemsTotal/100) as montant_total')
                    ->groupBy('mois_commande');
            }
        } else {
            if ($groupBySupplier) {
                $query
                    ->select('p.id as id, p.restaurantName as restaurant, p.firstName as prenom, p.email, p.phone as telephone, p.mobile as mobile, SUBSTRING(o.createdAt, 1, 7) as mois_commande, channel.name as grossiste, COUNT(o.id) as total_commande, SUM(o.itemsTotal/100) as montant_total')
                    ->groupBy('p.id, p.restaurantName, p.firstName, p.email, p.phone, p.mobile, mois_commande, grossiste')
                    ->join('o.channel', 'channel');
            } else {
                $query
                    ->select('p.id as id, p.restaurantName as restaurant, p.firstName as prenom, p.email, p.phone as telephone, p.mobile as mobile, SUBSTRING(o.createdAt, 1, 7) as mois_commande, COUNT(o.id) as total_commande, SUM(o.itemsTotal/100) as montant_total')
                    ->groupBy('p.id, p.restaurantName, p.firstName, p.email, p.phone, p.mobile, mois_commande');
            }
        }

        $data = $query
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as & $result) {

            $result['total_client_evolution'] = 0;
            $result['total_commande_evolution'] = 0;
            $result['montant_total_evolution'] = 0;

            $result['montant_total'] = intval(round($result['montant_total'], 0));

            if ($groupBySupplier) {
                $results[$result['mois_commande']][$result['id'] . '-' . $result['grossiste']] = $result;
            } else {
                $results[$result['mois_commande']][$result['id']] = $result;
            }

        }

        $i = 0;
        $previousMonth = '';
        foreach ($results as $month => $statsByMonth) {

            if ($i == 0) {
                $i++;
                $previousMonth = $month;
                continue;
            }


            foreach ($statsByMonth as $id => $stat) {

                if (array_key_exists('total_client', $stat) && array_key_exists($id, $results[$previousMonth]) && $results[$previousMonth][$id]['total_client'] > 0) {
                    $customerTotalEvolution = bcdiv(bcmul($stat['total_client'], 100), $results[$previousMonth][$id]['total_client'], 0) - 100;
                } else {
                    $customerTotalEvolution = 0;
                }

                if (array_key_exists('total_commande', $stat) && array_key_exists($id, $results[$previousMonth]) && $results[$previousMonth][$id]['total_commande'] > 0) {
                    $quantityTotalEvolution = bcdiv(bcmul($stat['total_commande'], 100), $results[$previousMonth][$id]['total_commande'], 0) - 100;
                } else {
                    $quantityTotalEvolution = 0;
                }

                if (array_key_exists('montant_total', $stat) && array_key_exists($id, $results[$previousMonth]) && $results[$previousMonth][$id]['montant_total'] > 0) {
                    $amountTotalEvolution = bcdiv(bcmul($stat['montant_total'], 100), $results[$previousMonth][$id]['montant_total'], 0) - 100;
                } else {
                    $amountTotalEvolution = 0;
                }

                $results[$month][$id]['total_client_evolution'] = $customerTotalEvolution;
                $results[$month][$id]['total_commande_evolution'] = $quantityTotalEvolution;
                $results[$month][$id]['montant_total_evolution'] = $amountTotalEvolution;

            }

            $i++;
            $previousMonth = $month;
        }
        return $results;
    }

    /**
     * @param Prospect $prospect
     * @return Statistics[]
     */
    public function getProductTopSales($startDate, $endDate, $prospect = null)
    {
        $query = $this->createQueryBuilder('o')
            ->select('SUBSTRING(o.createdAt, 1, 7) as mois_commande, product.id as id, trans.name as produit, SUM(i.quantity) as quantite_totale, SUM(i.unitsTotal/100) as montant_total')
            ->groupBy('produit, product.id, mois_commande')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->join('o.items', 'i')
            ->join('i.variant', 'v')
            ->join('v.product', 'product')
            ->join('product.translations', 'trans', 'with', 'trans.locale = \'fr_FR\'')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->orderBy('mois_commande', 'asc')
            ->addOrderBy('montant_total', 'desc')
            ->setParameter('start', $startDate)
            ->setParameter('end', $endDate);

        if ($prospect) {
            $query
                ->andWhere('p.id = :prospect')
                ->setParameter('prospect', $prospect);
        }

        $data = $query
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as & $result) {
            $result['quantite_totale_evolution'] = 0;
            $result['montant_total_evolution'] = 0;
            $result['quantite_totale'] = round($result['quantite_totale'], 2);
            $result['montant_total'] = round($result['montant_total'], 2);
            $results[$result['mois_commande']][$result['id']] = $result;
        }

        $i = 0;
        $previousMonth = '';
        foreach ($results as $month => $statsByMonth) {

            if ($i == 0) {
                $i++;
                $previousMonth = $month;
                continue;
            }

            foreach ($statsByMonth as $id => $stat) {

                if (array_key_exists($id, $results[$previousMonth]) && $results[$previousMonth][$id]['quantite_totale'] > 0) {
                    $quantityTotalEvolution = bcdiv(bcmul($stat['quantite_totale'], 100), $results[$previousMonth][$id]['quantite_totale'], 0) - 100;
                } else {
                    $quantityTotalEvolution = 0;
                }
                if (array_key_exists($id, $results[$previousMonth]) && $results[$previousMonth][$id]['montant_total'] > 0) {
                    $amountTotalEvolution = bcdiv(bcmul($stat['montant_total'], 100), $results[$previousMonth][$id]['montant_total'], 0) - 100;
                } else {
                    $amountTotalEvolution = 0;
                }
                $results[$month][$id]['quantite_totale_evolution'] = $quantityTotalEvolution;
                $results[$month][$id]['montant_total_evolution'] = $amountTotalEvolution;

            }

            $i++;
            $previousMonth = $month;
        }
        return $results;
    }

    /**
     * @param Prospect $prospect
     * @return Statistics[]
     */
    public function getLastOrderDate($prospect)
    {
        $query = $this->createQueryBuilder('o')
            ->select('channel.name as grossiste, MIN(o.createdAt) as first_date, MAX(o.createdAt) as last_date')
            ->groupBy('grossiste')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->join('o.channel', 'channel')
            ->orderBy('MAX(o.createdAt)', 'desc')
            ->andWhere('p.id = :prospect')
            ->setParameter('prospect', $prospect);

        $data = $query
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as $result) {
            $results[$result['grossiste']]['last_date'] = $result['last_date'];
            $results[$result['grossiste']]['first_date'] = $result['first_date'];
        }

        return $results;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param Prospect $prospect
     * @return Statistics[]
     */
    public function getOrderSales($startDate, $endDate, $prospect = null)
    {
        $query = $this->createQueryBuilder('o')
            ->select('p.id as id, o.id as commande, SUBSTRING(o.createdAt, 1, 7) as mois_commande, o.createdAt as date, channel.name as grossiste, o.itemsTotal/100 as montant_commande')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->join('o.channel', 'channel')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->orderBy('o.id', 'asc')
            ->setParameter('start', $startDate)
            ->setParameter('end', $endDate);

        if ($prospect) {
            $query
                ->andWhere('p.id = :prospect')
                ->setParameter('prospect', $prospect);
        }

        $data = $query
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as & $result) {
            $result['montant_commande'] = round($result['montant_commande'], 2);
            $result['date'] = $result['date']->format('Y-m-d H:i');
            $results[$result['mois_commande']][$result['commande']] = $result;
        }

        return $results;
    }

    /**
     * @param $twoWeeksAgoDate
     * @return Statistics[]
     * @throws Exception
     */
    public function getPreviousPeriodPropects($twoWeeksAgoDate)
    {
        $oldStartDate = new DateTime($twoWeeksAgoDate->format('Y-m-d'));
        $oldStartDate->modify('-365 day');

        $data = $this->createQueryBuilder('o')
            ->select('p.id as id, p.restaurantName as restaurant')
            ->groupBy('p.id, p.restaurantName')
            ->having('count(o.id) > 0')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->orderBy('p.id', 'asc')
            ->setParameter('start', $oldStartDate)
            ->setParameter('end', $twoWeeksAgoDate)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as & $result) {
            $results[$result['id']] = $result['restaurant'];
        }

        return $results;
    }

    /**
     * @param $todayDate
     * @param $twoWeeksAgoDate
     * @return Statistics[]
     * @throws Exception
     */
    public function getCurrentPeriodPropects($todayDate, $twoWeeksAgoDate)
    {

        $oldStartDate = new datetime($todayDate->format('Y-m-d'));
        $oldStartDate->modify('-30 day');

        $data = $this->createQueryBuilder('o')
            ->select('p.id as id, p.restaurantName as restaurant')
            ->groupBy('p.id, p.restaurantName')
            ->having('count(o.id) > 0')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->orderBy('p.id', 'asc')
            ->setParameter('start', $oldStartDate)
            ->setParameter('end', $todayDate)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as & $result) {
            $results[$result['id']] = $result['restaurant'];
        }

        return $results;
    }

    /**
     * @param $todayDate
     * @param $twoWeeksAgoDate
     * @param $previousPeriodProspectsId
     * @return Statistics[]
     */
    public function getNewProspects($todayDate, $twoWeeksAgoDate, $previousPeriodProspectsId)
    {
        $data = $this->createQueryBuilder('o')
            ->select('p.id as id, p.restaurantName as restaurant')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->andWhere('p.id NOT IN (:ids)')
            ->orderBy('p.id', 'asc')
            ->setParameter('start', $twoWeeksAgoDate)
            ->setParameter('end', $todayDate)
            ->setParameter('ids', $previousPeriodProspectsId)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as & $result) {
            $results[$result['id']] = $result['restaurant'];
        }

        return $results;
    }

    /**
     * @param $todayDate
     * @param $twoWeeksAgoDate
     * @param $currentPeriodProspectsId
     * @return Statistics[]
     * @throws Exception
     */
    public function getLostProspects($todayDate, $twoWeeksAgoDate, $currentPeriodProspectsId)
    {
        $oldStartDate = new datetime($twoWeeksAgoDate->format('Y-m-d'));
        $oldStartDate->modify('-75 day');

        $data = $this->createQueryBuilder('o')
            ->select('p.id as id, p.restaurantName as restaurant')
            ->join('o.customer', 'c')
            ->join('c.prospect', 'p')
            ->andWhere('o.createdAt BETWEEN :start AND :end')
            ->andWhere('p.id NOT IN (:ids)')
            ->orderBy('p.id', 'asc')
            ->setParameter('start', $oldStartDate)
            ->setParameter('end', $twoWeeksAgoDate)
            ->setParameter('ids', $currentPeriodProspectsId)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $results = [];
        foreach ($data as & $result) {
            $results[$result['id']] = $result['restaurant'];
        }

        return $results;
    }

    public function getTotalSalesForChannel(ChannelInterface $channel): int
    {
        $qb = $this->createQueryBuilder('o')
            ->select('SUM(o.total)')
//            ->andWhere('o.state = :state')
//            ->setParameter('state', OrderInterface::STATE_FULFILLED)
        ;

        if ($channel->getCode() !== 'all-channels') {
            $qb
                ->andWhere('o.channel = :channel')
                ->setParameter('channel', $channel);
        }

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    public function countFulfilledByChannel(ChannelInterface $channel): int
    {
        $qb = $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
//            ->andWhere('o.state = :state')
//            ->setParameter('state', OrderInterface::STATE_FULFILLED)
        ;

        if ($channel->getCode() !== 'all-channels') {
            $qb
                ->andWhere('o.channel = :channel')
                ->setParameter('channel', $channel);
        }

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Prospect $prospect
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countByTotalOrderForCustomer(Prospect $prospect): int
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->join('o.shoppingCart', 'sc')
            ->andWhere('sc.restaurant IN (:restaurants)')
            ->setParameter('restaurants', $prospect->getBoundRestaurants())
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    /**
     * @param Supplier $supplier
     * @param Prospect $prospect
     * @return array
     */
    public function getOrderDateRange(Supplier $supplier, Prospect $prospect)
    {
        try {
            $minDate = $this->createQueryBuilder('o')
                ->select('o.id, MIN(o.createdAt) AS minDate')
                ->join('o.shoppingCart', 'sc')
                ->andWhere('sc.restaurant IN (:restaurants)')
                ->andWhere('sc.supplier = :supplier')
                ->setParameter('restaurants', $prospect->getBoundRestaurants())
                ->setParameter('supplier', $supplier->getFacadeSupplier())
                ->groupBy('o.id')
                ->orderBy('o.createdAt', 'ASC')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            $minDate = null;
        } catch (NonUniqueResultException $e) {
            $minDate = null;
        }

        try {
            $maxDate = $this->createQueryBuilder('o')
                ->select('o.id, MAX(o.createdAt) AS maxDate')
                ->join('o.shoppingCart', 'sc')
                ->andWhere('sc.restaurant IN (:restaurants)')
                ->andWhere('sc.supplier = :supplier')
                ->setParameter('restaurants', $prospect->getBoundRestaurants())
                ->setParameter('supplier', $supplier->getFacadeSupplier())
                ->groupBy('o.id')
                ->orderBy('o.createdAt', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            $maxDate = null;
        } catch (NonUniqueResultException $e) {
            $maxDate = null;
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        return [
            'min' => !empty($minDate) ? new DateTime($minDate['minDate']) : null,
            'max' => !empty($maxDate) ? new DateTime($maxDate['maxDate']) : null,
            'minId' => !empty($minDate) ? $minDate['id'] : null,
            'maxId' => !empty($maxDate) ? $maxDate['id'] : null
        ];
    }

    /**
     * @param DateTime|null $minDate
     * @param DateTime|null $maxDate
     * @return Prospect[]
     */
    public function getProspectsThatHaveAlreadyOrdered(DateTime $minDate = null, DateTime $maxDate = null)
    {
        $qb = $this->createQueryBuilder('o');

        $qb
            ->select('p')
            ->join('o.shoppingCart', 'sc')
            ->join('sc.restaurant', 'r')
            ->join('r.customers', 'c')
            ->join(Prospect::class, 'p', 'WITH', 'p = c.prospect')
        ;

        if ($minDate !== null) {
            $qb
                ->andWhere('o.createdAt >= :minDate')
                ->setParameter('minDate', $minDate)
            ;
        }

        if ($maxDate !== null) {
            $qb
                ->andWhere('o.createdAt <= :maxDate')
                ->setParameter('maxDate', $maxDate)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @param string $orderBy
     * @return Product[]
     */
    public function getIndividualTopSalesProducts(int $limit, string $orderBy)
    {
        $categories = [
            Supplier::CAT_FRUITS_LEGUMES => ['taxons' => [69], 'limit' => 4],
            Supplier::CAT_BOUCHERIE => ['taxons' => [34, 507], 'limit' => 3],
            Supplier::CAT_CREMERIE => ['taxons' => [2], 'limit' => 3],
        ];

        $results = [];

        foreach ($categories as $category) {
            $results = array_merge($results, $this->createQueryBuilder('o')
                ->select('p')
                ->addSelect('SUM(i.quantity) AS totalQuantity')
                ->addSelect('SUM(i.total) AS totalAmount')
                ->join('o.items', 'i')
                ->join('i.variant', 'v')
                ->join(Product::class, 'p', 'WITH', 'p = v.product AND p.enabled = 1')
                ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.product = p AND spvi.productVariant = v AND spvi.manuallyCreated = 0')
                ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = spvi.product AND spp.productVariant = spvi.productVariant')
                ->join(ProductTaxon::class, 'pt', 'WITH', 'pt.product = p AND pt.taxon IN (:cat)')
                ->join('o.customer', 'c')
                ->join('c.prospect', 'prospect')
                ->where('prospect.type = :type')
                ->andWhere('prospect.id != 27')
                ->setParameter('type', 'individual')
                ->setParameter('cat', $category['taxons'])
                ->groupBy('p')
                ->orderBy($orderBy, 'DESC')
                ->setMaxResults($category['limit'])
                ->getQuery()
                ->getResult()
            );
        }

        $products = [];

        foreach ($results as $result) {
            $products[] = $result[0];
        }

        return $products;
    }
}
