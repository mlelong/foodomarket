<?php


namespace AppBundle\Repository;


use AppBundle\Entity\Message;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    /**
     * @param int|Supplier $supplier
     * @param int|Prospect $prospect
     * @param int $offset
     * @param int $limit
     * @return Message[]
     */
    public function getMessages($supplier, $prospect, int $offset = 0, int $limit = PHP_INT_MAX)
    {
        $qb = $this->createQueryBuilder('m');

        return $qb
            ->orWhere($qb->expr()->andX('m.recipient = :supplier', 'm.sender = :prospect'))
            ->orWhere($qb->expr()->andX('m.recipient = :prospect', 'm.sender = :supplier'))
            ->setParameter('supplier', $supplier)
            ->setParameter('prospect', $prospect)
            ->orderBy('m.createdAt', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }
}
