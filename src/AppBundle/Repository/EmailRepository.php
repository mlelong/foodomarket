<?php

namespace AppBundle\Repository;

use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class EmailRepository extends EntityRepository
{
    /**
     * @param int|string $templateId
     * @param string $dstEmail
     * @param string $srcEmail
     * @return bool
     */
    public function isTemplatedEmailAlreadySentToDst($templateId, string $dstEmail, string $srcEmail)
    {
        $qb = $this->createQueryBuilder('e');
        $emails = $qb
            ->where('e.templateId = :templateId')
            ->setParameter('templateId', $templateId)
            ->andWhere($qb->expr()->like('e.to', ':dstEmail'))
            ->setParameter('dstEmail', "%$dstEmail%")
            ->andWhere($qb->expr()->like('e.variables', ':srcEmail'))
            ->setParameter('srcEmail', "%$srcEmail%")
            ->getQuery()
            ->getResult()
        ;

        return !empty($emails);
    }
}
