<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Mercuriale;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierCategory;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class MercurialeRepository extends EntityRepository
{
    /**
     * @param Supplier|Supplier[]|int|int[] $supplier
     * @return Mercuriale[]
     */
    public function findBySupplier($supplier)
    {
        return $this->findBy(['supplier' => $supplier]);
    }

    /**
     * @param SupplierCategory $supplierCategory
     * @return Mercuriale[]
     */
    public function findBySupplierCategory(SupplierCategory $supplierCategory)
    {
        return $this->findBy([
            'supplier' => $supplierCategory->getSupplier(),
            'category' => $supplierCategory->getCategory(),
        ]);
    }
}