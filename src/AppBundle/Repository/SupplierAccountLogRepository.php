<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Entity\SupplierCategory;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class SupplierAccountLogRepository extends EntityRepository
{
    /**
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @param bool $creditCardOnline
     * @return bool
     */
    public function canOrderWith(Prospect $prospect, Supplier $supplier, bool &$creditCardOnline = false)
    {
        if ($supplier->getEnabled()) {
            $supplier = $this->_em->getRepository(Supplier::class)->getProspectPartnerForFacade($prospect, $supplier);
        }

        // Si le prospect n'est pas relié à des fournisseurs
        if ($prospect->getSuppliers()->isEmpty()) {
            return false;
        }

        // Check if account log is set
        $supplierCategories = $this->_em->getRepository(SupplierCategory::class)->findBySupplier($supplier);
        /** @var SupplierAccountLog[] $supplierAccountLogs */
        $supplierAccountLogs = $this->findBy(['prospect' => $prospect, 'supplierCategory' => $supplierCategories]);

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            if ($supplierAccountLog->getStatus() === SupplierAccountLog::STATUS_ORDERING) {
                $creditCardOnline = true;

                return true;
            }

            if (($supplierAccountLog->isSigned() && $supplierAccountLog->getStatus() !== SupplierAccountLog::STATUS_ACCOUNT_BLOCKED) || in_array($supplierAccountLog->getStatus(), [SupplierAccountLog::STATUS_ACCOUNT_OPENED, SupplierAccountLog::STATUS_ORDERING])) {
                return true;
            }

            if (in_array($supplierAccountLog->getStatus(), [SupplierAccountLog::STATUS_ACCOUNT_BLOCKED])) {
                return false;
            }
        }

        // Si ils acceptent le paiement à la livraison
//        if ($supplier->isPaymentOnline() || $supplier->isPaymentOnDelivery()) {
//            return true;
//        }

        return false;
    }

    public function getAccountsWithStatus(Prospect $prospect, string $status)
    {
        /** @var SupplierAccountLog[] $prospectAccounts */
        $prospectAccounts = $this->findBy(['prospect' => $prospect]);
        $suppliers = [];

        foreach ($prospectAccounts as $prospectAccount) {
            $supplier = $prospectAccount->getSupplierCategory()->getSupplier();

            $suppliers[$prospectAccount->getStatus()][$supplier->getId()] = $supplier;
        }

        if (!isset($suppliers[$status])) {
            return [];
        }

        foreach ($suppliers[$status] as $supplierId => $supplier) {
            foreach ($suppliers as $_status => $_suppliers) {
                if ($_status === $status) {
                    continue ;
                }

                if (isset($_suppliers[$supplierId])) {
                    unset($suppliers[$status][$supplierId]);
                }
            }
        }

        return array_values($suppliers[$status]);
    }

    /**
     * @param Prospect $prospect
     * @return Supplier[]
     */
    public function getWaitingAccounts(Prospect $prospect)
    {
        return $this->getAccountsWithStatus($prospect, SupplierAccountLog::STATUS_WAITING);
    }

    /**
     * @param Prospect $prospect
     * @return Supplier[]
     */
    public function getOpenedAccounts(Prospect $prospect)
    {
        return $this->getAccountsWithStatus($prospect, SupplierAccountLog::STATUS_ACCOUNT_OPENED);
    }

    /**
     * Returns the distinct suppliers bound to prospect via SupplierAccountLog
     *
     * @param Prospect $prospect
     * @return Supplier[]
     */
    public function getSuppliers(Prospect $prospect)
    {
        /** @var SupplierAccountLog[] $supplierAccountLogs */
        $supplierAccountLogs = $this->findBy(['prospect' => $prospect]);
        $suppliers = [];

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            $supplier = $supplierAccountLog->getSupplierCategory()->getSupplier();
            $suppliers[$supplier->getId()] = $supplier;
        }

        return array_values($suppliers);
    }

    /**
     * Returns the most relevant supplier account log
     * Priority is as following:
     *    - STATUS_ACCOUNT_BLOCKED (1st priority)
     *    - STATUS_ACOUNT_OPENED (2nd priority)
     *    - STATUS_WAITING (3rd priority)
     *
     * The one with the highest priority is returned
     *
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @return SupplierAccountLog|null
     */
    public function getRelevantSupplierAccountLog(Prospect $prospect, Supplier $supplier)
    {
        $supplierAccountLogs = $this->getSupplierAccountLogs($prospect, $supplier);
        $supplierAccountLog = null;

        foreach ($supplierAccountLogs as $_supplierAccountLog) {
            if ($supplierAccountLog === null) {
                $supplierAccountLog = $_supplierAccountLog;
                continue ;
            }

            switch ($_supplierAccountLog->getStatus()) {
                case SupplierAccountLog::STATUS_ACCOUNT_OPENED:
                    if ($supplierAccountLog->getStatus() != SupplierAccountLog::STATUS_ACCOUNT_BLOCKED) {
                        $supplierAccountLog = $_supplierAccountLog;
                    }
                    break ;
                case SupplierAccountLog::STATUS_ACCOUNT_BLOCKED:
                    $supplierAccountLog = $_supplierAccountLog;
                    break ;
            }
        }

        return $supplierAccountLog;
    }

    /**
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @return SupplierAccountLog[]
     */
    public function getSupplierAccountLogs(Prospect $prospect, Supplier $supplier)
    {
        /** @var SupplierAccountLog[] $logs */
        $logs = $this->findBy(['prospect' => $prospect]);
        $supplierLogs = [];

        foreach ($logs as $log) {
            if ($log->getSupplierCategory()->getSupplier()->getId() === $supplier->getId()) {
                $supplierLogs[] = $log;
            }
        }

        return $supplierLogs;
    }

    public function updateLog(Prospect $prospect, Supplier $supplier, int $contractId, ?string $token = null, ?int $tokenExpiration = null)
    {
        $supplierLogs = $this->getSupplierAccountLogs($prospect, $supplier);

        foreach ($supplierLogs as $log) {
            $log->setContractId($contractId);
            $log->setToken($token);
            $log->setTokenExpiration($tokenExpiration);

            $this->getEntityManager()->flush($log);
        }
    }

    /**
     * @param int $contractId
     * @param bool $signed
     * @return SupplierAccountLog[]
     */
    public function updateSignature(int $contractId, bool $signed)
    {
        /** @var SupplierAccountLog[] $supplierLogs */
        $supplierLogs = $this->findBy(['contractId' => $contractId]);

        foreach ($supplierLogs as $supplierLog) {
            $supplierLog->setSigned($signed);

//            if ($signed) {
//                $supplierLog->setStatus(SupplierAccountLog::STATUS_ACCOUNT_OPENED);
//            }

            $this->getEntityManager()->flush($supplierLog);
        }

        return $supplierLogs;
    }

    /**
     * Update the status for all supplier account logs concerned by a prospect and supplier (through supplier categories)
     *
     * @param Prospect $prospect
     * @param Supplier $supplier
     * @param string $status
     * @return bool
     */
    public function updateStatus(Prospect $prospect, Supplier $supplier, string $status)
    {
        $supplierAccountLogs = $this->getSupplierAccountLogs($prospect, $supplier);

        foreach ($supplierAccountLogs as $supplierAccountLog) {
            $supplierAccountLog->setStatus($status);
        }

        try {
            $this->getEntityManager()->flush($supplierAccountLogs);

            return true;
        } catch (OptimisticLockException $e) {
            return false;
        } catch (ORMException $e) {
            return false;
        }
    }

    public function updateAllAccordingTo(SupplierAccountLog $supplierAccountLog)
    {
        $supplierAccountLogs = $this->getSupplierAccountLogs(
            $supplierAccountLog->getProspect(),
            $supplierAccountLog->getSupplierCategory()->getSupplier()
        );

        foreach ($supplierAccountLogs as $_supplierAccountLog) {
            $_supplierAccountLog->setStatus($supplierAccountLog->getStatus());
            $_supplierAccountLog->setSigned($supplierAccountLog->isSigned());
            $_supplierAccountLog->setClientCodes($supplierAccountLog->getClientCodes());
            $_supplierAccountLog->setToken($supplierAccountLog->getToken());
            $_supplierAccountLog->setTokenExpiration($supplierAccountLog->getTokenExpiration());
            $_supplierAccountLog->setContractId($supplierAccountLog->getContractId());
            $_supplierAccountLog->setHash($supplierAccountLog->getHash());
        }

        try {
            $this->getEntityManager()->flush($supplierAccountLogs);

            return true;
        } catch (OptimisticLockException $e) {
            return false;
        } catch (ORMException $e) {
            return false;
        }
    }

    /**
     * @param SupplierAccountLog $supplierAccountLog
     * @return SupplierAccountLog[]
     */
    public function getRelatedSupplierAccountLogs(SupplierAccountLog $supplierAccountLog)
    {
        $supplierCategories = $this->createQueryBuilder('o')
            ->select('sc')
            ->from(SupplierCategory::class, 'sc')
            ->where('sc.supplier = :supplier')
            ->setParameter('supplier', $supplierAccountLog->getSupplierCategory()->getSupplier())
            ->getQuery()
            ->getResult()
        ;

        $supplierCategories = array_map(function(SupplierCategory $sc) {
            return $sc->getId();
        }, $supplierCategories);

        $qb = $this->createQueryBuilder('supplierAccountLog');

        return $qb
            ->andWhere('supplierAccountLog.prospect = :prospect')
            ->andWhere($qb->expr()->in('supplierAccountLog.supplierCategory', $supplierCategories))
            ->setParameter('prospect', $supplierAccountLog->getProspect())
            ->getQuery()
            ->getResult()
            ;
    }
}