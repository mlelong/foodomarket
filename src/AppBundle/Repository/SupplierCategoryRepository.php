<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierCategory;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class SupplierCategoryRepository extends EntityRepository
{
    /**
     * @param Supplier|Supplier[]|int|int[] $suppliers
     * @return SupplierCategory[]
     */
    public function findBySupplier($suppliers)
    {
        return $this->findBy(['supplier' => $suppliers]);
    }

    /**
     * @param string $category
     * @return SupplierCategory[]
     */
    public function findByCategory(string $category)
    {
        return $this->findBy(['category' => $category]);
    }
}