<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\Prospect;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class InvoiceDeliveryNoteRepository extends EntityRepository
{
    /**
     * @param Prospect $prospect
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countDelivery(Prospect $prospect){

        return $this->createQueryBuilder('delivery')
            ->select('count(delivery)')
            ->innerJoin('delivery.invoice', 'invoice')
            ->where('invoice.prospect = :prospect')
            ->setParameter('prospect', $prospect->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Prospect $prospect
     * @param DateTime $start
     * @param DateTime $end
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countDeliveryBetweenDate(Prospect $prospect, DateTime $start, DateTime $end){

        return $this->createQueryBuilder('delivery')
        ->select('count(delivery)')
            ->innerJoin('delivery.invoice', 'invoice')
            ->where('invoice.prospect = :prospect')
            ->andWhere('delivery.date BETWEEN :start AND :end')
            ->setParameter('prospect', $prospect->getId())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Prospect $prospect
     * @param DateTime $start
     * @param DateTime $end
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function averageDeliveryBetweenDate(Prospect $prospect, DateTime $start, DateTime $end){
        return $this->createQueryBuilder('delivery')
            ->select('avg(delivery.total)')
            ->innerJoin('delivery.invoice', 'invoice')
            ->where('invoice.prospect = :prospect')
            ->andWhere('delivery.date BETWEEN :start AND :end')
            ->setParameter('prospect', $prospect->getId())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Prospect $prospect
     * @return mixed
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function lastDateDelivery(Prospect $prospect){
        return $this->createQueryBuilder('delivery')
            ->select('max(delivery.date)')
            ->innerJoin('delivery.invoice', 'invoice')
            ->where('invoice.prospect = :prospect')
            ->setParameter('prospect', $prospect->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

}