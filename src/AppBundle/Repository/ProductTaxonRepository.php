<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductTaxonRepository as BaseProductTaxonRepository;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\Taxon;

class ProductTaxonRepository extends BaseProductTaxonRepository
{
    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param Taxon[]|int[] ...$categories
     * @return int
     */
    public function countProductsForSuppliersAndCategories(array $suppliers = null, ...$categories)
    {
        $qb = $this->getQueryBuilder($suppliers, ...$categories);

        try {
            return $qb
                ->select('COUNT(DISTINCT(p))')
                ->getQuery()
                ->getSingleScalarResult()
            ;
        } catch (NonUniqueResultException $e) {
            return 0;
        } catch (NoResultException $e) {
            return 0;
        }
    }

    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param Taxon[]|int[] ...$categories
     * @return Product[]
     */
    public function getProductsForSuppliersAndCategories(array $suppliers = null, ...$categories)
    {
        $qb = $this->getQueryBuilder($suppliers, ...$categories);

        return $qb
            ->select('p')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param Taxon[]|int[] ...$categories
     * @return int
     */
    public function countOffersForSuppliersAndCategories(array $suppliers = null, ...$categories)
    {
        $qb = $this->getQueryBuilder($suppliers, ...$categories);

        try {
            return $qb
                ->select('COUNT(DISTINCT(spp))')
                ->getQuery()
                ->getSingleScalarResult()
            ;
        } catch (NonUniqueResultException $e) {
            return 0;
        } catch (NoResultException $e) {
            return 0;
        }
    }

    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param Taxon[]|int[] ...$categories
     * @return QueryBuilder
     */
    private function getQueryBuilder(array $suppliers = null, ...$categories)
    {
        $qb = $this->createQueryBuilder('pt');

        $qb
            ->join(SupplierProductPrice::class, 'spp', 'WITH', 'spp.product = pt.product')
            ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.product = spp.product AND spvi.productVariant = spp.productVariant AND spvi.manuallyCreated = 0')
            ->join(Product::class, 'p', 'WITH', 'p = spp.product')
            ->join(ProductVariant::class, 'v', 'WITH', 'v = spp.productVariant')
            ->join('p.translations', 't', 'WITH', 't.locale = :locale')
            ->where('pt.taxon IN (:categories)')
            ->andWhere('p.enabled = 1')
            ->andWhere('v.enabled = 1')
            ->andWhere('p.mainTaxon IS NOT NULL')
            ->andWhere('p.mainTaxon != 1')
            ->andWhere('spp.restaurant = 4')
            ->setParameter('locale', 'fr_FR')
            ->setParameter('categories', $categories)
        ;

        if ($suppliers !== null) {
            $qb
                ->join('spp.supplier', 's')
                ->andWhere('s IN (:suppliers)')
                ->setParameter('suppliers', $suppliers)
            ;
        }

        return $qb;
    }
}
