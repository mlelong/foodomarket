<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Supplier;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository as BaseTaxonRepository;
use Sylius\Component\Core\Model\ProductTaxon;
use Sylius\Component\Core\Model\Taxon;

class TaxonRepository extends BaseTaxonRepository
{
    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param int $depthLevel
     * @return array
     */
    public function getShopTaxons(array $suppliers = null, int $depthLevel = 1)
    {
        /** @var ProductTaxonRepository $productTaxonRepository */
        $productTaxonRepository = $this->_em->getRepository(ProductTaxon::class);

        /** @noinspection PhpParamsInspection */
        $taxonTree = [
            'products' => $productTaxonRepository->countProductsForSuppliersAndCategories($suppliers, 69, 2, 11, 34, 507, 9, 10, 503, 537, 275),
            'children' => [
                Supplier::CAT_CAVE => $this->getTaxonTree($this->find(503), $suppliers, $depthLevel),
                Supplier::CAT_BOUCHERIE => [
                    'text' => Supplier::getDisplayCategory(Supplier::CAT_BOUCHERIE),
                    'id' => 34,
                    'slug' => 'boucherie',
                    'products' => $productTaxonRepository->countProductsForSuppliersAndCategories($suppliers, 34, 507),
                    'children' => array_merge(
                        $this->getTaxonTree($this->find(34), $suppliers, $depthLevel)['children'],
                        $this->getTaxonTree($this->find(507), $suppliers, $depthLevel)['children']
                    )
                ],
                Supplier::CAT_CREMERIE => $this->getTaxonTree($this->find(2), $suppliers, $depthLevel),
                Supplier::CAT_EPICERIE => [
                    'text' => Supplier::getDisplayCategory(Supplier::CAT_EPICERIE),
                    'id' => 9,
                    'slug' => 'epicerie-salee',
                    'products' => $productTaxonRepository->countProductsForSuppliersAndCategories($suppliers, 9, 10),
                    'children' => array_merge(
                        $this->getTaxonTree($this->find(9), $suppliers, $depthLevel)['children'],
                        $this->getTaxonTree($this->find(10), $suppliers, $depthLevel)['children']
                    )
                ],
                Supplier::CAT_FRUITS_LEGUMES => $this->getTaxonTree($this->find(69), $suppliers, $depthLevel),
                Supplier::CAT_MAREE => $this->getTaxonTree($this->find(11), $suppliers, $depthLevel),

                Supplier::CAT_TRAITEUR => $this->getTaxonTree($this->find(275), $suppliers, $depthLevel),

                Supplier::CAT_CONSOMMABLES => $this->getTaxonTree($this->find(537), $suppliers, $depthLevel),
            ]
        ];

        $taxonTree = $this->cleanTaxonTree($taxonTree);

        return $taxonTree;
    }

    public function cleanTaxonTree($taxonTree)
    {
        foreach ($taxonTree['children'] as $category => $child) {
            if ($child['products'] == 0) {
                unset($taxonTree['children'][$category]);
                continue ;
            }

            if (isset($child['children'])) {
                $taxonTree['children'][$category] = $this->cleanTaxonTree($child);
            }
        }

        $taxonTree['children'] = array_values($taxonTree['children']);

        return $taxonTree;
    }

    /**
     * @param Taxon $taxon
     * @param Supplier[]|int[]|null $suppliers
     * @param int $depthLevel
     * @param int $currentLevel
     * @return array
     */
    public function getTaxonTree(Taxon $taxon, array $suppliers = null, int $depthLevel = -1, int $currentLevel = 1)
    {
        /** @var ProductTaxonRepository $productTaxonRepository */
        $productTaxonRepository = $this->getEntityManager()->getRepository(ProductTaxon::class);

        if ($depthLevel > -1 && $currentLevel > $depthLevel) {
            return [
                'text' => $taxon->getName(),
                'id' => $taxon->getId(),
                'slug' => $taxon->getSlug(),
                'products' => $productTaxonRepository->countProductsForSuppliersAndCategories($suppliers, $taxon->getId())
            ];
        }

        /** @var \ArrayIterator $iterator */
        $iterator = $taxon->getChildren()->getIterator();

        $iterator->uasort(function (Taxon $a, Taxon $b) {
            return strcmp($a->getName(), $b->getName());
        });

        $children = new ArrayCollection(iterator_to_array($iterator));

        return [
            'text' => $taxon->getName(),
            'id' => $taxon->getId(),
            'slug' => $taxon->getSlug(),
            'products' => $productTaxonRepository->countProductsForSuppliersAndCategories($suppliers, $taxon->getId()),
            'children' => $children->map(function (Taxon $child) use ($suppliers, $depthLevel, $currentLevel) {
                return $this->getTaxonTree($child, $suppliers, $depthLevel, $currentLevel + 1);
            })->toArray()
        ];
    }
}