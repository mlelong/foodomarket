<?php

namespace AppBundle\Repository;

use AppBundle\Entity\BlockedPrice;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;

/**
 * Class SupplierProductPriceRepository
 * @package AppBundle\Repository
 */
class SupplierProductPriceRepository extends EntityRepository
{
    /**
     * @param Supplier[]|int[] ...$suppliers
     * @return SupplierProductPrice[]
     */
    public function getPricesForSuppliers(...$suppliers)
    {
        return $this->getPrices($suppliers, null, null, null, false);
    }

    /**
     * @param Restaurant|int $restaurant
     * @param Supplier[]|int[] ...$suppliers
     * @return SupplierProductPrice[]
     */
    public function getRestaurantPricesForSuppliers($restaurant, ...$suppliers)
    {
        if (($restaurant instanceof Restaurant && $restaurant->getId() == 4) || (is_numeric($restaurant) && $restaurant === 4)) {
            return $this->getPrices($suppliers, null, null, [$restaurant], false);
        }

        $foodomarketPrices = $this->getPrices($suppliers, null, null, [4], false);
        $restaurantPrices = $this->getPrices($suppliers, null, null, [$restaurant], true);

        // Recherche de prix bloqué
        foreach ($foodomarketPrices as $i => $foodomarketPrice) {
            foreach ($restaurantPrices as $restaurantPrice) {
                // Si on trouve un prix bloqué, alors on supprime le prix foodomarket de la liste à retourner
                if ($foodomarketPrice->getProductVariant()->getId() === $restaurantPrice->getProductVariant()->getId()
                    && $foodomarketPrice->getProduct()->getId() === $restaurantPrice->getProduct()->getId()
                    && $foodomarketPrice->getSupplier()->getFacadeSupplier()->getId() === $restaurantPrice->getSupplier()->getId()) {
                    unset($foodomarketPrices[$i]);
                }
            }
        }

        return array_merge($foodomarketPrices, $restaurantPrices);
    }

    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param ProductInterface[]|int[]|null $products
     * @param ProductVariantInterface[]|null $variants
     * @param Restaurant|int $restaurant
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice[]
     */
    public function getRestaurantPrices(
        array $suppliers = null,
        array $products = null,
        array $variants = null,
        $restaurant = 4,
        bool $includeManuallyCreated = false)
    {
        if (($restaurant instanceof Restaurant && $restaurant->getId() == 4) || (is_numeric($restaurant) && $restaurant == 4)) {
            return $this->getPrices($suppliers, $products, $variants, [$restaurant], false);
        }

        $foodomarketPrices = $this->getPrices($suppliers, $products, $variants, [4], false);
        $restaurantPrices = $this->getPrices($suppliers, $products, $variants, [$restaurant], $includeManuallyCreated);

        $now = new \DateTime();
        $now->setTime(0, 0, 0, 0);

        // Recherche de prix bloqué
        foreach ($foodomarketPrices as $i => $foodomarketPrice) {
            foreach ($restaurantPrices as $j => $restaurantPrice) {
                // Si on trouve un prix bloqué...
                if ($foodomarketPrice->getProductVariant()->getId() === $restaurantPrice->getProductVariant()->getId()
                    && $foodomarketPrice->getProduct()->getId() === $restaurantPrice->getProduct()->getId()
                    && $foodomarketPrice->getSupplier()->getFacadeSupplier()->getId() === $restaurantPrice->getSupplier()->getId()) {

                    // Et si la plage de validité du prix est valide, alors on supprime le prix foodomarket
                    if ($restaurantPrice->getValidFrom() <= $now && $restaurantPrice->getValidTo() >= $now) {
                        unset($foodomarketPrices[$i]);
                    }
                    // Sinon on supprime le prix bloqué restaurant périmé
                    else {
                        unset($restaurantPrices[$j]);
                    }
                }
            }
        }

        return array_merge($foodomarketPrices, $restaurantPrices);
    }

    /**
     * @param ProductInterface[]|int[] ...$products
     * @return SupplierProductPrice[]
     */
    public function getPricesForProducts(...$products)
    {
        return $this->getPrices(null, $products, null, null, false);
    }

    /**
     * @param ProductVariantInterface[]|int[] ...$variants
     * @return SupplierProductPrice[]
     */
    public function getPricesForVariants(...$variants)
    {
        return $this->getPrices(null, null, $variants, null, false);
    }

    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param ProductInterface[]|int[]|null $products
     * @param ProductVariantInterface[]|null $variants
     * @param Restaurant[]|null $restaurants
     * @param bool $includeManuallyCreated
     * @return SupplierProductPrice[]
     */
    public function getPrices(
        array $suppliers = null,
        array $products = null,
        array $variants = null,
        array $restaurants = null,
        bool $includeManuallyCreated = false
    ) {
        $qb = $this->getPricesQueryBuilder($suppliers, $products, $variants, $restaurants, $includeManuallyCreated);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param ProductInterface[]|int[]|null $products
     * @param ProductVariantInterface[]|null $variants
     * @param Restaurant[]|null $restaurants
     * @param bool $includeManuallyCreated
     * @return int
     */
    public function countPrices(
        array $suppliers = null,
        array $products = null,
        array $variants = null,
        array $restaurants = null,
        bool $includeManuallyCreated = false
    ) {
        $qb = $this->getPricesQueryBuilder($suppliers, $products, $variants, $restaurants, $includeManuallyCreated);

        try {
            return $qb
                ->select('COUNT(DISTINCT(spp))')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    /**
     * @param Supplier[]|int[]|null $suppliers
     * @param ProductInterface[]|int[]|null $products
     * @param ProductVariantInterface[]|null $variants
     * @param Restaurant[]|null $restaurants
     * @param bool $includeManuallyCreated
     * @return QueryBuilder
     */
    private function getPricesQueryBuilder(
        array $suppliers = null,
        array $products = null,
        array $variants = null,
        array $restaurants = null,
        bool $includeManuallyCreated = false
    ) {
        $qb = $this->createQueryBuilder('spp');

        $qb
            ->join('spp.product', 'p')
            ->join('spp.productVariant', 'v')
            ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.supplier = spp.supplier AND spvi.product = spp.product AND spvi.productVariant = spp.productVariant AND spvi.manuallyCreated IN (:manuallyCreated)')
            ->setParameter('manuallyCreated', $includeManuallyCreated ? [0, 1] : 0)
            ->andWhere('p.enabled = 1')
            ->andWhere('v.enabled = 1')
        ;

        if (!empty($suppliers)) {
            $qb->andWhere('spp.supplier IN (:suppliers)')->setParameter('suppliers', $suppliers);
        }

        if (!empty($products)) {
            $qb->andWhere('spp.product IN (:products)')->setParameter('products', $products);
        }

        if (!empty($variants)) {
            $qb->andWhere('spp.productVariant IN (:variants)')->setParameter('variants', $variants);
        }

        if (!empty($restaurants)) {
            $qb->andWhere('spp.restaurant IN (:restaurants)')->setParameter('restaurants', $restaurants);
        }

        return $qb;
    }

    /**
     * @param Supplier[]|int[] $suppliers
     * @return SupplierProductPrice[]
     */
    public function getBlockedPrices(array $suppliers)
    {
        return $this->createQueryBuilder('spp')
            ->join('spp.supplier', 'supplier')
            ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.supplier = spp.supplier AND spvi.product = spp.product AND spvi.productVariant = spp.productVariant AND spvi.manuallyCreated = 0')
            ->where('supplier.enabled = 1')
            ->andWhere('supplier.id IN (:suppliers)')
            ->setParameter('suppliers', $suppliers)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param BlockedPrice $price
     * @return SupplierProductPrice|null
     */
    public function getByBlockedPrice(BlockedPrice $price)
    {
        try {
            return $this->createQueryBuilder('spp')
                ->andWhere('spp.supplier = :supplier')
                ->andWhere('spp.restaurant = :restaurant')
                ->andWhere('spp.product = :product')
                ->andWhere('spp.productVariant = :variant')
                ->setParameters([
                    'supplier' => $price->getSupplier(),
                    'restaurant' => $price->getRestaurant(),
                    'product' => $price->getProduct(),
                    'variant' => $price->getProductVariant()
                ])
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
