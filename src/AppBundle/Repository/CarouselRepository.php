<?php


namespace AppBundle\Repository;


use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Core\Model\Product;

class CarouselRepository extends EntityRepository
{
    /**
     * @param string $name
     * @return Product[]
     */
    public function getProductsForCarousel(string $name)
    {
        $carousel = $this->findOneBy(['name' => $name]);

        if (!$carousel) {
            return [];
        }

        $products = [];

        foreach ($carousel->getItems() as $item) {
            $products[] = $item->getProduct();
        }

        return $products;
    }
}
