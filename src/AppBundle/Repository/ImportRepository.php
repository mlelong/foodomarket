<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Import;
use AppBundle\Entity\Supplier;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ImportRepository extends EntityRepository
{
    /**
     * Returns last import made by mercuriale id, or null
     * @param Supplier|int $supplier
     * @param int $mercurialeId
     * @return Import|null
     */
    public function getLastInserted($supplier, int $mercurialeId)
    {
        $import = $this->findBy(['supplier' => $supplier, 'mercurialeId' => $mercurialeId], ['createdAt' => 'DESC'], 1);

        return empty($import) ? null : $import[0];
    }
}