<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use DateTime;
use Doctrine\ORM\ORMException;
use Exception;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class ProspectRepository extends EntityRepository implements UserLoaderInterface
{
    /**
     * @param Prospect $prospect
     * @return Prospect
     */
    public function refresh(Prospect $prospect)
    {
        try {
            $this->getEntityManager()->refresh($prospect);
        } catch (ORMException $e) {
        }

        return $prospect;
    }

    public function loadUserByUsername($username)
    {
        $prospects = $this->createQueryBuilder('p')
            ->where('p.email = :email')
            ->setParameter('email', $username)
            ->getQuery()
            ->getResult()
        ;

        return empty($prospects) ? null : $prospects[count($prospects) - 1];
    }

    public function createListQueryBuilder()
    {
        return $this->createQueryBuilder('o')->leftJoin('o.suppliers', 's');
    }

    /**
     * @param string $ids Comma separated list of prospect ids
     * @return Prospect[]
     */
    public function findByIds(string $ids)
    {
        return $this->createQueryBuilder('p')->where("p.id IN ($ids)")->getQuery()->getResult();
    }

    /**
     * @param Supplier $partnerSupplier
     * @return Prospect[]
     */
    public function getProspectsBoundToPartnerSupplier(Supplier $partnerSupplier)
    {
        return $this->createQueryBuilder('p')
            ->join('p.suppliers', 's')
            ->where('s.supplier = :supplier')
            ->setParameter('supplier', $partnerSupplier)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Prospect $prospect
     * @return Restaurant[]
     */
    public function getProspectRestaurants(Prospect $prospect)
    {
        $restaurants = [];

        foreach ($prospect->getCustomers() as $customer) {
            foreach ($customer->getRestaurants() as $restaurant) {
                $restaurants[$restaurant->getId()] = $restaurant;
            }
        }

        return $restaurants;
    }

    public function getProspect(array $criteria)
    {
        /** @var Prospect $prospect */
        $prospect = $this->findOneBy($criteria);

        if ($prospect !== null && $prospect->getCustomers()->isEmpty()) {
            return $prospect;
        }

        return null;
    }

    public function getClient(array $criteria)
    {
        /** @var Prospect $prospect */
        $prospect = $this->findOneBy($criteria);

        if ($prospect !== null && $prospect->getCustomers()->isEmpty()) {
            return null;
        }

        return $prospect;
    }

    /**
     * Creates or update a prospect
     *
     * @param string $firstname
     * @param string $restaurant
     * @param string $email
     * @param string $telephone
     * @param string|null $source
     * @return Prospect|null
     */
    public function addProspect(string $firstname, string $restaurant, string $email, string $telephone, ?string $source = null)
    {
        /** @var Prospect|null $prospect */
        $prospect = $this->findOneBy(['email' => $email]);

        if ($prospect) {
            try {
                $prospect->setUpdatedAt(new DateTime());
            } catch (Exception $e) {
            }

            $prospect->addSource('forceupdate');
        } else {
            $prospect = new Prospect();
        }

        $prospect->setFirstName($firstname);
        $prospect->setRestaurantName($restaurant);
        $prospect->setEmail($email);
        $prospect->setPhone($telephone);
        $prospect->setMobile($telephone);

        if ($source) {
            $prospect->addSource($source);
        }

        try {
            $this->_em->persist($prospect);
            $this->_em->flush($prospect);
        } catch (ORMException $e) {
            return null;
        }

        return $prospect;
    }
}