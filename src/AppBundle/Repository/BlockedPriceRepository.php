<?php

namespace AppBundle\Repository;

use AppBundle\Entity\BlockedPrice;
use AppBundle\Entity\Restaurant;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class BlockedPriceRepository extends EntityRepository
{
    /**
     * @param Restaurant|int $restaurant
     * @param bool $activeOnly
     * @return BlockedPrice[]
     */
    public function getBlockedPricesByRestaurant($restaurant, bool $activeOnly = false)
    {
        $qb = $this->createQueryBuilder('bp')
            ->where('bp.restaurant = :restaurant')
            ->setParameter('restaurant', $restaurant)
        ;

        if ($activeOnly) {
            try {
                $now = new \DateTime();
            } catch (\Exception $e) {
                $now = date('Y-m-d');
            }

            $qb
                ->andWhere('bp.validFrom <= :from')
                ->andWhere('bp.validTo >= :to')
                ->setParameter('from', $now)
                ->setParameter('to', $now)
            ;
        }

        return $qb->getQuery()->getResult();
    }
}