<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ProductVariantRepository as BaseProductVariantRepository;

class ProductVariantRepository extends BaseProductVariantRepository
{
    /**
     * @param string $phrase
     * @param Supplier|int $supplier
     * @return ProductVariant[]
     */
    public function findByPhraseForSupplier(string $phrase, $supplier)
    {
        $qb = $this->createQueryBuilder('o');

        return $qb
            ->innerJoin('o.translations', 'translation')
            ->innerJoin('o.product', 'product')
            ->innerJoin('product.translations', 'product_translation')

            ->join(SupplierProductPrice::class, 'p', 'WITH', 'p.productVariant = o AND p.product = product')
            ->join(SupplierProductVariantInformations::class, 'spvi', 'WITH', 'spvi.product = product AND spvi.productVariant = o AND spvi.manuallyCreated = 0 AND spvi.supplier = p.supplier')

            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('translation.name', ':name'),
                    $qb->expr()->like('product_translation.name', ':name')
                )
            )
            ->andWhere('p.supplier = :supplier')
            ->andWhere('translation.locale = :locale')
            ->andWhere('product_translation.locale = :locale')
            ->setParameter('name', "%$phrase%")
            ->setParameter('locale', 'fr_FR')
            ->setParameter('supplier', $supplier)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    *    Search by label / variant id / supplier reference
    */
    public function findByPhraseOrReference($phrase, $locale)
    {
        $qb = $this->createQueryBuilder('o');
        return $qb
            ->innerJoin('o.translations', 'translation')
            ->innerJoin('o.product', 'product')
            ->innerJoin('product.translations', 'product_translation')
            ->innerjoin('AppBundle:SupplierProductVariantInformations', 'spvi', 'WITH', 'o = spvi.productVariant')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('translation.name', ':name'),
                    $qb->expr()->like('product_translation.name', ':name'),
                    $qb->expr()->eq('spvi.reference', ':reference'),
                    $qb->expr()->eq('o.id', ':variantid')
                )
            )
            ->andWhere('translation.locale = :locale')
            ->andWhere('product_translation.locale = :locale')
            ->setParameter('name', "%$phrase%")
            ->setParameter('locale', $locale)

            ->setParameter('reference', "$phrase")
            ->setParameter('variantid', "$phrase")

            ->getQuery()
            ->getResult()
            ;
    }

    public function findByPhrase($phrase, $locale)
    {
        $qb = $this->createQueryBuilder('o');

        return $qb
            ->innerJoin('o.translations', 'translation')
            ->innerJoin('o.product', 'product')
            ->innerJoin('product.translations', 'product_translation')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('translation.name', ':name'),
                    $qb->expr()->like('product_translation.name', ':name')
                )
            )
            ->andWhere('translation.locale = :locale')
            ->andWhere('product_translation.locale = :locale')
            ->setParameter('name', "%$phrase%")
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Returns all variants in restaurant_stock for the given customer (restaurant)
     *
     * @param Customer $customer
     * @param Array $variantIds
     * @return Variants[]
     */
    public function getVariants($customer, $variantIds)
    {
        $qb = $this->createQueryBuilder('variant');

        return $qb
            ->select('distinct variant')
            ->join('AppBundle:SupplierProductPrice', 'supplierProductPrice', 'WITH', 'supplierProductPrice.restaurant = :restaurant')
            ->andWhere('variant = supplierProductPrice.productVariant')
            ->andWhere('supplierProductPrice.productVariant in (:variants)')
            ->setParameters(
                    [
                        'restaurant'    => $customer->getRestaurant(),
                        'variants'      => $variantIds,
                    ])
            ->getQuery()
            ->getResult();
    }


}