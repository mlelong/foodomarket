<?php

namespace AppBundle;

use AppBundle\DependencyInjection\Compiler\ChannelPricesCompilerPass;
use AppBundle\DependencyInjection\Compiler\FetcherCompilerPass;
use AppBundle\DependencyInjection\Compiler\InvoiceParserCompilerPass;
use AppBundle\DependencyInjection\Compiler\LiformCompilerPass;
use AppBundle\DependencyInjection\Compiler\ReactRendererCompilerPass;
use AppBundle\DependencyInjection\Compiler\SyliusAdminControllerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ChannelPricesCompilerPass());
        $container->addCompilerPass(new FetcherCompilerPass());
        $container->addCompilerPass(new LiformCompilerPass());
        $container->addCompilerPass(new SyliusAdminControllerCompilerPass());
        $container->addCompilerPass(new InvoiceParserCompilerPass());
        $container->addCompilerPass(new ReactRendererCompilerPass());
    }
}
