<?php


namespace ApiBundle\Dto;


use DateTime;
use Doctrine\Common\Collections\Collection;

class Order
{
    /** @var string|null */
    public $id;

    /** @var string|null */
    public $reference;

    /** @var DateTime */
    public $date;

    /** @var string */
    public $supplier;

    /** @var string */
    public $supplierId;

    /** @var float */
    public $total;

    /** @var Address */
    public $shippingAddress;

    /** @var Collection<OrderItem> */
    public $items;

    /** @var string */
    public $currentStatus;

    /** @var Collection<OrderStatus> */
    public $statusHistory;

    /** @var string|null */
    public $paymentStatus;

    /** @var string|null */
    public $paymentUrl;

    /** @var string|null */
    public $paymentException;

    /** @var float */
    public $shippingCost;
}
