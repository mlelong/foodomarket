<?php


namespace ApiBundle\Dto;

use DateTime;
use Doctrine\Common\Collections\Collection;

/**
 * Class OrderInput
 * @package ApiBundle\Dto
 */
class OrderInput
{
    /** @var string|null */
    public $id;

    /** @var string */
    public $restaurantId;

    /** @var string */
    public $supplierId;

    /** @var DateTime */
    public $scheduledDate;

    /** @var string|null */
    public $note;

    /** @var Address */
    public $shippingAddress;

    /** @var OrderItemInput[]|Collection<OrderItemInput> */
    public $items;

    /** @var string|null */
    public $orderLocation;

    /** @var boolean|null */
    public $useAlias;

    /** @var boolean|null */
    public $createAlias;

    /** @var int|null */
    public $aliasId;
}
