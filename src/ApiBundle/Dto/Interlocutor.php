<?php


namespace ApiBundle\Dto;


use AppBundle\Entity\Prospect as ProspectModel;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Entity\User;

final class Interlocutor
{
    const TYPE_SUPPLIER = 'supplier';
    const TYPE_PROSPECT = 'user';

    public function __construct(?User $user)
    {
        if ($user instanceof SupplierModel) {
            $this->type = self::TYPE_SUPPLIER;
            $this->email = $user->getEmail();
            $this->name = $user->getDisplayName();
        } elseif ($user instanceof ProspectModel) {
            $this->type = self::TYPE_PROSPECT;
            $this->email = $user->getEmail();
            $this->name = implode(' ', array_filter([$user->getFirstName(), $user->getContactName()]));
        }
    }

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $name;
}
