<?php


namespace ApiBundle\Dto;


class Status
{
    /** @var int */
    public $code;

    /** @var string|null */
    public $message;
}
