<?php


namespace ApiBundle\Dto;


/**
 * Class OrderItemInput
 * @package ApiBundle\Dto
 */
final class OrderItemInput
{
    /**
     * @var string
     */
    public $variantId;

    /** @var float */
    public $quantity;

    /** @var string */
    public $unit;
}
