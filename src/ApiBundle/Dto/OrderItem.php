<?php


namespace ApiBundle\Dto;


use ApiBundle\Entity\Traits\ProductTrait;

class OrderItem
{
    use ProductTrait;

    /** @var string|null */
    public $id;

    /** @var string */
    public $vid;

    /** @var float */
    public $quantity;

    /** @var float */
    public $total;
}
