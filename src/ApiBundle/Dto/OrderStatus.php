<?php


namespace ApiBundle\Dto;


use DateTime;

class OrderStatus
{
    /** @var string|null */
    public $id;

    /** @var string */
    public $status;

    /** @var DateTime */
    public $date;
}
