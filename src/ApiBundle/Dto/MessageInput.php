<?php


namespace ApiBundle\Dto;


class MessageInput
{
    /** @var string */
    public $supplierId;

    /** @var string */
    public $content;

    /** @var bool|null */
    public $important;

    /** @var string|null */
    public $picture;
}
