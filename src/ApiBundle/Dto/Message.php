<?php


namespace ApiBundle\Dto;


use DateTime;

class Message
{
    /**
     * @var string|null
     */
    public $id;

    /**
     * @var Interlocutor
     */
    public $recipient;

    /**
     * @var Interlocutor
     */
    public $sender;

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $status;

    /**
     * @var DateTime
     */
    public $creationDate;

    /**
     * @var string
     */
    public $channel;

    /**
     * @var string
     */
    public $channelStatus;

    /**
     * @var string|null
     */
    public $channelMessageId;

    /**
     * @var bool
     */
    public $important;

    /**
     * @var string|null
     */
    public $picture;
}
