<?php


namespace ApiBundle\Dto;


/**
 * Class Address
 * @package ApiBundle\Dto
 */
final class Address
{
    /**
     * @var string|null
     */
    public $number;

    /**
     * @var string
     */
    public $street;

    /**
     * @var string
     */
    public $zip;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $country;
}
