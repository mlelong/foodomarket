<?php


namespace ApiBundle\Dto;


final class SupplierInput
{
    /** @var string|null */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string|null */
    public $phone;
}
