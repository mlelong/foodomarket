<?php


namespace ApiBundle\Dto;


class Restaurant
{
    /**
     * @var string|null
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var Address
     */
    public $address;
}
