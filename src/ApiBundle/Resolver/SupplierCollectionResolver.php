<?php


namespace ApiBundle\Resolver;

use ApiBundle\Entity\Supplier;
use ApiBundle\Resolver\Traits\SupplierModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\QueryCollectionResolverInterface;
use AppBundle\Entity\Prospect;
use AppBundle\Repository\SupplierRepository;
use GraphQL\Type\Definition\ResolveInfo;

class SupplierCollectionResolver implements QueryCollectionResolverInterface
{
    use TokenAwareTrait,
        SupplierModelToPopo;

    /** @var SupplierRepository */
    private $supplierRepository;

    /**
     * SupplierCollectionResolver constructor.
     * @param SupplierRepository $supplierRepository
     */
    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        /** @var Supplier[] $ret */
        $ret = [];

        switch ($info->fieldName) {
            case 'availableSuppliers':
                $qb = $this->supplierRepository->createQueryBuilder('s');
                /** @var \AppBundle\Entity\Supplier[] $suppliers */
                $suppliers = $qb
                    ->where('s.enabled = 0')
                    ->andWhere('s.facadeSupplier IS NOT NULL')
                    ->andWhere($qb->expr()->orX('s.partner = 1', 's.createdByUser = 0'))
                    ->getQuery()
                    ->getResult()
                ;

                foreach ($suppliers as $supplier) {
                    $ret[$supplier->getFacadeSupplier()->getId()] = $this->model2Popo($supplier->getFacadeSupplier(), $this->getUser()->getCustomer()->getProspect());
                }

                $ret = array_values($ret);

                break ;
            case 'restaurantSuppliers':
                $restaurantId = $context['args']['restaurantId'];
                $user = $this->getUser();
                $customerId = $user->getCustomer()->getId();
                $qb = $this->supplierRepository->createQueryBuilder('s');
                /** @var \AppBundle\Entity\Supplier[] $suppliers */
                $suppliers = $qb
                    ->select('sf')
                    ->join(\AppBundle\Entity\Supplier::class, 'sf', 'WITH','sf = s.facadeSupplier')
                    ->join(Prospect::class, 'p')
                    ->join('p.customers', 'c')
                    ->join('p.suppliers', 'sc')
                    ->andWhere(':customer MEMBER OF p.customers')
                    ->andWhere(':restaurant MEMBER OF c.restaurants')
                    ->andWhere('s = sc.supplier')
                    ->setParameter('customer', $customerId)
                    ->setParameter('restaurant', $restaurantId)
                    ->getQuery()
                    ->getResult()
                ;

                foreach ($suppliers as $supplier) {
                    $ret[] = $this->model2Popo($supplier, $this->getUser()->getCustomer()->getProspect(), true);
                }

                break ;
        }

        return $ret;
    }
}
