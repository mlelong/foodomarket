<?php


namespace ApiBundle\Resolver;


use ApiBundle\Entity\Product;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\ProductOptionValue;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Entity\SupplierProductPrice;
use AppBundle\Entity\SupplierProductVariantInformations;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\SupplierProductPriceRepository;
use AppBundle\Repository\SupplierProductVariantInformationsRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Repository\TaxonRepository;
use AppBundle\Services\RedisCacheManager;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use GraphQL\Type\Definition\ResolveInfo;
use Sylius\Component\Core\Model\Product as ProductModel;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Product\Model\ProductOption;
use Throwable;

class ProductMutationResolver implements MutationResolverInterface
{
    /** @var ManagerRegistry */
    private $doctrine;

    /** @var ProductRepository */
    private $productRepository;

    /** @var ProductVariantRepository */
    private $productVariantRepository;

    /** @var TaxonRepository */
    private $taxonRepository;

    /** @var RestaurantRepository */
    private $restaurantRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var SupplierProductPriceRepository */
    private $supplierProductPriceRepository;

    /** @var SupplierProductVariantInformationsRepository */
    private $supplierProductVariantInformationsRepository;

    /** @var RedisCacheManager */
    private $cacheManager;

    public function __construct(
        ManagerRegistry $doctrine,
        ProductRepository $productRepository,
        ProductVariantRepository $productVariantRepository,
        TaxonRepository $taxonRepository,
        RestaurantRepository $restaurantRepository,
        SupplierRepository $supplierRepository,
        SupplierProductPriceRepository $supplierProductPriceRepository,
        SupplierProductVariantInformationsRepository $supplierProductVariantInformationsRepository,
        RedisCacheManager $cacheManager
    ) {
        $this->doctrine = $doctrine;
        $this->productRepository = $productRepository;
        $this->productVariantRepository = $productVariantRepository;
        $this->taxonRepository = $taxonRepository;
        $this->restaurantRepository = $restaurantRepository;
        $this->supplierRepository = $supplierRepository;
        $this->supplierProductPriceRepository = $supplierProductPriceRepository;
        $this->supplierProductVariantInformationsRepository = $supplierProductVariantInformationsRepository;
        $this->cacheManager = $cacheManager;
    }

    /**
     * @param Product|null $item
     * @param array $context
     * @return Product|object|null
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'createProduct':
                $variant = $this->createProduct(
                    $context['args']['input']['restaurantId'],
                    $context['args']['input']['supplierId'],
                    $context['args']['input']['name'],
                    $context['args']['input']['unit'],
                    $context['args']['input']['categoryId'],
                    $context['args']['input']['code'] ?? null
                );

                if ($variant === null) {
                    return null;
                }

                $ret = new Product();
                /** @var Taxon $taxon */
                $taxon = $this->taxonRepository->find($context['args']['input']['categoryId']);

                $ret->id = $variant->getId();
                $ret->pid = $variant->getProduct()->getId();
                $ret->unit = $variant->getOptionOrderUnit()->getValue();
                $ret->name = $variant->getProduct()->getName();
                $ret->price = 0;
                $ret->image = '';
                $ret->imageLarge = '';
                $ret->explicitContent = '';
                $ret->priceDetail = [];
                $ret->supplierId = $context['args']['input']['supplierId'];
                $ret->mainCategory = "{$context['args']['input']['categoryId']}";
                $ret->category = "Catalogue > {$taxon->getName()}";
                $ret->createdByUser = true;

                $this->cacheManager->deleteItem(str_replace(
                    ['{supplierId}', '{restaurantId}'],
                    [$context['args']['input']['supplierId'], $context['args']['input']['restaurantId']],
                    RedisCacheManager::CACHE_KEY_SHOP_USER_SUPPLIER_PRODUCTS
                ));

                return $ret;
            case 'editProduct':
                $variant = $this->editProduct(
                    $context['args']['input']['productId'],
                    $context['args']['input']['restaurantId'],
                    $context['args']['input']['supplierId'],
                    $context['args']['input']['name'],
                    $context['args']['input']['unit'],
                    $context['args']['input']['categoryId'],
                    $context['args']['input']['code'] ?? null
                );

                if ($variant === null) {
                    return null;
                }

                $ret = new Product();
                /** @var Taxon $taxon */
                $taxon = $this->taxonRepository->find($context['args']['input']['categoryId']);

                $ret->id = $variant->getId();
                $ret->pid = $variant->getProduct()->getId();
                $ret->unit = $variant->getOptionOrderUnit()->getValue();
                $ret->name = $variant->getProduct()->getName();
                $ret->price = 0;
                $ret->image = '';
                $ret->imageLarge = '';
                $ret->explicitContent = '';
                $ret->priceDetail = [];
                $ret->supplierId = $context['args']['input']['supplierId'];
                $ret->mainCategory = "{$context['args']['input']['categoryId']}";
                $ret->category = "Catalogue > {$taxon->getName()}";
                $ret->createdByUser = true;

                $this->cacheManager->deleteItem(str_replace(
                    ['{variantId}', '{supplierId}', '{restaurantId}'],
                    [$variant->getId(), $context['args']['input']['supplierId'], $context['args']['input']['restaurantId']],
                    RedisCacheManager::CACHE_KEY_SHOP_VARIANT
                ));

                return $ret;
        }

        return null;
    }

    /**
     * @param string $productId
     * @param string $restaurantId
     * @param string $supplierId
     * @param string $name
     * @param string $unit
     * @param int $categoryId
     * @param string|null $code
     * @return ProductVariant|null
     */
    private function editProduct(string $productId, string $restaurantId, string $supplierId, string $name, string $unit, int $categoryId, ?string $code = null)
    {
        if (!in_array($unit, ['KG', 'PC', 'CO', 'L'])) {
            return null;
        }

        if (!in_array($categoryId, [34, 507, 9, 10, 69, 11, 2, 537, 503, 275, 296])) {
            return null;
        }

        /** @var ProductVariant|null $variant */
        $variant = $this->productVariantRepository->find($productId);

        if ($variant === null) {
            return null;
        }

        /** @var ProductModel $product */
        $product = $variant->getProduct();

        $prices = $this->supplierProductPriceRepository->getPrices(
            [$supplierId],
            [$product],
            [$variant],
            [$restaurantId],
            true
        );

        if (empty($prices)) {
            return null;
        }

        $information = $this->supplierProductVariantInformationsRepository->getByPrice($prices[0], true);

        if ($information === null) {
            return null;
        }

        $information->setReference($code);
        $information->setSalesUnit($unit);

        $product->setMainTaxon($this->taxonRepository->find($categoryId));
        $product->setName($name);

        $productOptionRepository = $this->doctrine->getRepository(ProductOption::class);
        $productOptionValueRepository = $this->doctrine->getRepository(ProductOptionValue::class);

        $salesUnitOption = $productOptionRepository->findOneBy(['code' => 'UM']);
        $orderUnitOption = $productOptionRepository->findOneBy(['code' => 'UC']);
        $salesUnitOptionValue = $productOptionValueRepository->findOneBy(['option' => $salesUnitOption, 'code' => $unit]);
        $orderUnitOptionValue = $productOptionValueRepository->findOneBy(['option' => $orderUnitOption, 'code' => "ORDER_$unit"]);

        foreach ($variant->getOptionValues() as $optionValue) {
            if (in_array($optionValue->getOptionCode(), ['UM', 'UC'])) {
                $variant->removeOptionValue($optionValue);
            }
        }

        $variant->addOptionValue($salesUnitOptionValue);
        $variant->addOptionValue($orderUnitOptionValue);

        $this->doctrine->getManager()->flush();

        return $variant;
    }

    /**
     * @param string $restaurantId
     * @param string $supplierId
     * @param string $name
     * @param string $unit
     * @param int $categoryId
     * @param string|null $code
     * @return ProductVariant|null
     */
    private function createProduct(string $restaurantId, string $supplierId, string $name, string $unit, int $categoryId, ?string $code = null)
    {
        if (!in_array($unit, ['KG', 'PC', 'CO', 'L'])) {
            return null;
        }

        if (!in_array($categoryId, [34, 507, 9, 10, 69, 11, 2, 537, 503, 275, 296])) {
            return null;
        }

        /** @var EntityManager $em */
        $em = $this->doctrine->getManager();

        try {
            /** @var Supplier $supplier */
            $supplier = $this->supplierRepository->find($supplierId);
            /** @var Restaurant $restaurant */
            $restaurant = $this->restaurantRepository->find($restaurantId);

            $variantCode = Slugify::create()->slugify("$restaurantId-$supplierId-$name");

            /** @var ProductVariant|null $variant */
            if ($variant = $this->productVariantRepository->findOneBy(['code' => $variantCode])) {
                return $variant;
            }

            return $em->transactional(function (EntityManager $em) use($variantCode, $restaurant, $supplier, $restaurantId, $supplierId, $name, $unit, $categoryId, $code) {
                $product = new ProductModel();

                $product->setCurrentLocale('fr_FR');
                $product->setEnabled(true);
                $product->setName($name);
                $product->setCode(uniqid("$restaurantId-$supplierId-"));
                $product->setSlug(Slugify::create()->slugify("$restaurantId-$supplierId-$name"));
                $product->setMainTaxon($this->taxonRepository->find($categoryId));

                $em->persist($product);

                $variant = new ProductVariant();

                $variant->setProduct($product);
                $variant->setCurrentLocale('fr_FR');
                $variant->setEnabled(true);
                $variant->setWeight(1);
                $variant->setCode($variantCode);
                $variant->setName('-');

                $productOptionRepository = $this->doctrine->getRepository(ProductOption::class);
                $productOptionValueRepository = $this->doctrine->getRepository(ProductOptionValue::class);

                $salesUnitOption = $productOptionRepository->findOneBy(['code' => 'UM']);
                $orderUnitOption = $productOptionRepository->findOneBy(['code' => 'UC']);
                $unitQuantityOption = $productOptionRepository->findOneBy(['code' => 'UQ']);
                $unitContentQuantityOption = $productOptionRepository->findOneBy(['code' => 'UCQ']);
                $unitContentMeasurementOption = $productOptionRepository->findOneBy(['code' => 'UCM']);

                $salesUnitOptionValue = $productOptionValueRepository->findOneBy(['option' => $salesUnitOption, 'code' => $unit]);
                $orderUnitOptionValue = $productOptionValueRepository->findOneBy(['option' => $orderUnitOption, 'code' => "ORDER_$unit"]);
                $unitQuantityOptionValue = $productOptionValueRepository->findOneBy(['option' => $unitQuantityOption, 'code' => '1']);
                $unitContentQuantityOptionValue = $productOptionValueRepository->findOneBy(['option' => $unitContentQuantityOption, 'code' => '1']);
                $unitContentMeasurementOptionValue = $productOptionValueRepository->findOneBy(['option' => $unitContentMeasurementOption, 'code' => 'KG']);

                $variant->addOptionValue($salesUnitOptionValue);
                $variant->addOptionValue($orderUnitOptionValue);
                $variant->addOptionValue($unitQuantityOptionValue);
                $variant->addOptionValue($unitContentQuantityOptionValue);
                $variant->addOptionValue($unitContentMeasurementOptionValue);

                $em->persist($variant);

                $information = new SupplierProductVariantInformations();

                $information->setSupplier($supplier);
                $information->setProductVariant($variant);
                $information->setProduct($product);
                $information->setProductLabel($name);
                $information->setVariantLabel($name);
                $information->setDisplayName($name);
                $information->setWeight(1);
                $information->setSalesUnit($unit);
                $information->setUnitQuantity(1);
                $information->setContent(1);
                $information->setManuallyCreated(true);

                if ($code) {
                    $information->setReference($code);
                }

                $em->persist($information);

                $price = new SupplierProductPrice();

                $price->setKgPrice(0);
                $price->setUnitPrice(0);
                $price->setProduct($product);
                $price->setProductVariant($variant);
                $price->setMercurialeId(-1);
                $price->setRestaurant($restaurant);
                $price->setSupplier($supplier);
                $price->setChannelCode($supplier->getChannel()->getCode());

                $em->persist($price);

                return $variant;
            });
        } catch (Throwable $e) {
            return null;
        }
    }
}
