<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Entity\Product;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;
use AppBundle\Util\PriceModifier;

trait ProductModelToPopo
{
    private function model2Popo(array $product, Supplier $supplier, Prospect $client, string $zipCode)
    {
        $p = new Product();

        $p->id = "{$product['productId']}";
        $p->pid = "{$product['pid']}";
        $p->name = $product['productName'];
        $p->supplierId = "{$product['supplierId']}";
        $p->image = $product['productPicture'];
        $p->imageLarge = $product['productPictureLarge'];
        $p->unit = $product['quantityUnitSelected'];
        $p->createdByUser = $product['manuallyCreated'];
        $p->weight = $product['weight'];

        $category = [];

        foreach ($product['tree'] as $i => $taxon) {
            $category[] = ($i === 0 ? 'Catalogue' : $taxon['name']);

            switch ($taxon['id']) {
                case 69: // Fruits & Légumes
                case 11: // Marée
                case 2: // Crèmerie
                case 34: // Boucherie
                case 507: // Charcuterie
                case 9: // Epicerie salée
                case 10: // Epicerie sucrée
                case 296: // Divers
                case 503: // Boissons
                case 275: // Traiteur
                case 537: // Consommmables
                    $p->mainCategory = $taxon['id'];
                    break;
            }
        }

        $p->category = implode(' > ', $category);

        if ($p->mainCategory === null) {
            $p->mainCategory = isset($product['tree'][1]) ? $product['tree'][1]['id'] : '';
        }

        switch ($p->unit) {
            case 'KG':
            case 'L':
                $p->price = floatval(bcdiv($product['productPrice'], 100, 2));
                break ;
            default:
                $p->price = floatval(bcdiv($product['productPriceUnit'], 100, 2));
                break ;
        }

        $clientType = $client->getType();
        $p->price = PriceModifier::modify($p->price, $client, $zipCode, $supplier);

        $p->explicitContent = $product['explicitContent'];
        $p->priceDetail = $product['priceDetail'];

        // Apply taxes on explicit content
        if ($clientType === Prospect::TYPE_INDIVIDUAL) {
            foreach ($p->priceDetail as &$detail) {
                $detail = preg_replace_callback('/[\d.]+/', function (array $matches) use($client, $zipCode, $supplier) {
                    return PriceModifier::modify($matches[0], $client, $zipCode, $supplier);
                }, $detail);
            }
        }

        return $p;
    }
}
