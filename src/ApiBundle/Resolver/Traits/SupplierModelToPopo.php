<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Dto\Interlocutor;
use ApiBundle\Entity\Supplier;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\MessageRepository;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Util\SupplierUtil;

trait SupplierModelToPopo
{
    /** @var SupplierAccountLogRepository */
    private $_supplierAccountLogRepository;

    /** @var SupplierRepository */
    private $_supplierRepository;

    /** @var MessageRepository */
    private $_messageRepository;

    /**
     * @required
     * @param SupplierAccountLogRepository $supplierAccountLogRepository
     */
    public function setSupplierAccountLogRepository(SupplierAccountLogRepository $supplierAccountLogRepository)
    {
        $this->_supplierAccountLogRepository = $supplierAccountLogRepository;
    }

    /**
     * @required
     * @param SupplierRepository $supplierRepository
     */
    public function setSupplierRepository(SupplierRepository $supplierRepository)
    {
        $this->_supplierRepository = $supplierRepository;
    }

    /**
     * @required
     * @param MessageRepository $messageRepository
     */
    public function setMessageRepository(MessageRepository $messageRepository)
    {
        $this->_messageRepository = $messageRepository;
    }

    /**
     * @param SupplierModel $supplierModel
     * @param Prospect $prospect
     * @param bool $computeUnreadMessages
     * @return Supplier
     */
    private function model2Popo(SupplierModel $supplierModel, Prospect $prospect, bool $computeUnreadMessages = false)
    {
        $supplier = new Supplier();

        $supplier->id = $supplierModel->getId();
        $supplier->partner = $supplierModel->isPartner();
        $supplier->createdByUser = $supplierModel->isCreatedByUser();
        $supplier->email = $supplierModel->getEmail();
        $supplier->name = $supplierModel->getDisplayName();
        $supplier->telephone = $supplierModel->getPhone();
        $supplier->minOrderAmount = $supplierModel->getMinimumOrder();
        $supplier->orderCutOff = $supplierModel->getOrderLimitTime();
        $supplier->openingDays = $supplierModel->getOpeningDays();
        $supplier->closingDays = $supplierModel->getClosingDays();
        $supplier->deliveryCost = SupplierUtil::getDeliveryCost($supplierModel, $prospect->getZipCode());
        $supplier->categories = $supplierModel->getFacadeSupplier()->getCategories();
        $supplier->chronofreshDelivery = $supplierModel->isChronofreshDelivery();
        // Jacob and Beaugrain deliver everywhere in France
        $supplier->deliveryWeightUnit = in_array($supplierModel->getFacadeSupplier()->getId(), [79, 83]) ? 25 : 0;

        $partner = $this->_supplierRepository->getProspectPartnerForFacade($prospect, $supplierModel);

        /** @var SupplierAccountLog[] $supplierAccountLog */
        $supplierAccountLog = $this->_supplierAccountLogRepository->createQueryBuilder('sal')
            ->join('sal.supplierCategory', 'sc')
            ->andWhere('sal.prospect = :prospect')
            ->andWhere('sc.supplier = :supplier')
            ->setParameter('prospect', $prospect)
            ->setParameter('supplier', $partner)
            ->getQuery()
            ->getResult()
        ;

        if (!empty($supplierAccountLog)) {
            $supplier->status = $supplierAccountLog[0]->getStatus();
        } else {
            $supplier->status = 'WAITING';
        }

        if ($computeUnreadMessages) {
            $supplier->nbUnreadMessages = $this->_messageRepository->count([
                'senderType' => Interlocutor::TYPE_SUPPLIER,
                'recipientType' => Interlocutor::TYPE_PROSPECT,
                'sender' => $supplierModel,
                'recipient' => $prospect,
                'read' => false
            ]);
        }

        return $supplier;
    }
}
