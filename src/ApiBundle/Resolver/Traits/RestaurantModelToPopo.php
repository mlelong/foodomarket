<?php


namespace ApiBundle\Resolver\Traits;

use ApiBundle\Dto\Address;
use ApiBundle\Entity\Restaurant;
use AppBundle\Entity\Restaurant as RestaurantModel;

trait RestaurantModelToPopo
{
    private function model2Popo(RestaurantModel $restaurantModel)
    {
        $restaurant = new Restaurant();

        $restaurant->id = $restaurantModel->getId();
        $restaurant->name = $restaurantModel->getName();

        $a = new Address();

        $streetNumberPattern = '/^(\d+\S+|\d+)/';

        if (preg_match($streetNumberPattern, $restaurantModel->getStreet(), $match)) {
            $a->number = $match[0];
            $a->street = trim(preg_replace($streetNumberPattern, '', $restaurantModel->getStreet()));
        } else {
            $a->street = $restaurantModel->getStreet();
        }

        $a->zip = $restaurantModel->getPostcode();
        $a->city = $restaurantModel->getCity();
        $a->country = $restaurantModel->getCountryCode();

        $restaurant->address = $a;

        return $restaurant;
    }
}