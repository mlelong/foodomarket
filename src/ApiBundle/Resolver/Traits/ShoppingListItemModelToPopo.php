<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Entity\ShoppingListItem;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Services\Product\ProductFinderInterface;

trait ShoppingListItemModelToPopo
{
    /** @var ProductFinderInterface */
    protected $productFinder;

    /**
     * @required
     * @param ProductFinderInterface $productFinder
     */
    public function setProductFinder(ProductFinderInterface $productFinder)
    {
        $this->productFinder = $productFinder;
    }

    /**
     * @param RestaurantStockItem $shoppingListItem
     * @return ShoppingListItem
     */
    private function model2Popo(RestaurantStockItem $shoppingListItem)
    {
        $item = new ShoppingListItem();

        $item->id = $shoppingListItem->getId();
        $item->pid = $shoppingListItem->getProductVariant()->getProduct()->getId();
        $item->vid = $shoppingListItem->getProductVariant()->getId();
        $item->unit = $shoppingListItem->getUnit();
        $item->supplierId = $shoppingListItem->getSupplier()->getId();

        $productData = $this->productFinder->getProduct(
            $shoppingListItem->getProductVariant(),
            $shoppingListItem->getSupplier(),
            $shoppingListItem->getRestaurantStock()->getRestaurant()
        );

        if (!empty($productData)) {
            switch ($item->unit) {
                case 'KG':
                case 'L':
                    $item->price = floatval(bcdiv($productData['productPrice'], 100, 2));
                    break ;
                default:
                    $item->price = floatval(bcdiv($productData['productPriceUnit'], 100, 2));
                    break ;
            }

            $item->explicitContent = $productData['explicitContent'];
            $item->priceDetail = $productData['priceDetail'];
            $item->name = $productData['productName'];
            $item->image = $productData['productPicture'];
            $item->imageLarge = $productData['productPictureLarge'];
            $item->createdByUser = $productData['manuallyCreated'];

            $category = [];

            foreach ($productData['tree'] as $i => $taxon) {
                $category[] = ($i === 0 ? 'Catalogue' : $taxon['name']);

                switch ($taxon['id']) {
                    case 69: // Fruits & Légumes
                    case 11: // Marée
                    case 2: // Crèmerie
                    case 34: // Boucherie
                    case 507: // Charcuterie
                    case 9: // Epicerie salée
                    case 10: // Epicerie sucrée
                    case 296: // Divers
                    case 503: // Boissons
                    case 275: // Traiteur
                    case 537: // Consommmables
                        $item->mainCategory = $taxon['id'];
                        break;
                }
            }

            $item->category = implode(' > ', $category);

            if ($item->mainCategory === null) {
                $item->mainCategory = isset($product['tree'][1]) ? $productData['tree'][1]['id'] : '';
            }
        } else {
            $item->name = $shoppingListItem->getProductVariant()->getProduct()->getName();
            $item->image = "";
            $item->imageLarge = "";
            $item->category = "";
            $item->mainCategory = 0;
            $item->createdByUser = false;
            $item->price = 0;
            $item->explicitContent = "";
            $item->priceDetail = [];
        }

        return $item;
    }
}