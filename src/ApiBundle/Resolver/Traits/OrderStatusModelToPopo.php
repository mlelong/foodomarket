<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Entity\OrderStatus;
use AppBundle\Entity\OrderStatus as OrderStatusModel;

trait OrderStatusModelToPopo
{
    /**
     * @param OrderStatusModel $orderStatusModel
     * @return OrderStatus
     */
    private function model2Popo(OrderStatusModel $orderStatusModel)
    {
        $orderStatus = new OrderStatus();

        $orderStatus->id = $orderStatusModel->getId();
        $orderStatus->status = $orderStatusModel->getStatus();
        $orderStatus->date = $orderStatusModel->getUpdatedAt();

        return $orderStatus;
    }
}
