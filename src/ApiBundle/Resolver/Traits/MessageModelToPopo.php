<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Dto\Interlocutor;
use ApiBundle\Entity\Message;
use AppBundle\Entity\Message as MessageModel;
use AppBundle\Entity\User;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierRepository;

trait MessageModelToPopo
{
    /** @var SupplierRepository */
    protected $supplierRepository;

    /** @var ProspectRepository */
    protected $prospectRepository;

    /**
     * @required
     * @param SupplierRepository $supplierRepository
     */
    public function setSupplierRepository(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * @required
     * @param ProspectRepository $prospectRepository
     */
    public function setProspectRepository(ProspectRepository $prospectRepository)
    {
        $this->prospectRepository = $prospectRepository;
    }

    private function model2Popo(MessageModel $messageModel)
    {
        $message = new Message();

        $message->id = $messageModel->getId();
        $message->content = $messageModel->getMessage();
        $message->status = $messageModel->getStatus();
        $message->channel = $messageModel->getChannel();
        $message->channelStatus = $messageModel->getStatus();
        $message->channelMessageId = $messageModel->getChannelMessageId();
        $message->important = $messageModel->isImportant();
        $message->creationDate = $messageModel->getCreatedAt();

        $picture = $messageModel->getPicture();

        if (is_resource($picture)) {
            $picture = stream_get_contents($picture);
        }

        $message->picture = $picture;
        $message->sender = new Interlocutor($this->getInterlocutor($messageModel->getSenderType(), $messageModel->getSender()->getId()));
        $message->recipient = new Interlocutor($this->getInterlocutor($messageModel->getRecipientType(), $messageModel->getRecipient()->getId()));

        return $message;
    }

    /**
     * @param string $type
     * @param int $id
     * @return User
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    private function getInterlocutor(string $type, int $id)
    {
        switch ($type) {
            case Interlocutor::TYPE_SUPPLIER:
                return $this->supplierRepository->find($id);
            case Interlocutor::TYPE_PROSPECT:
                return $this->prospectRepository->find($id);
            default:
                return null;
        }
    }
}
