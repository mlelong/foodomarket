<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Entity\ShoppingList;
use AppBundle\Entity\RestaurantStock;

trait ShoppingListModelToPopo
{
    private function model2Popo(RestaurantStock $shoppingListModel)
    {
        $shoppingList = new ShoppingList();

        $shoppingList->id = $shoppingListModel->getId();
        $shoppingList->name = $shoppingListModel->getName();
        $shoppingList->supplierId = $shoppingListModel->getSupplier()->getId();
        $shoppingList->supplier = $shoppingListModel->getSupplier()->getDisplayName();
        $shoppingList->items = [];

        return $shoppingList;
    }
}
