<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Entity\OrderItem;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem as OrderItemModel;
use AppBundle\Entity\Prospect;
use AppBundle\Services\Product\ProductFinderInterface;

trait OrderItemModelToPopo
{
    /** @var ProductFinderInterface */
    private $productFinder;

    /**
     * @required
     * @param ProductFinderInterface $productFinder
     */
    public function setProductFinder(ProductFinderInterface $productFinder)
    {
        $this->productFinder = $productFinder;
    }


    private function model2Popo(OrderItemModel $orderItemModel)
    {
        $orderItem = new OrderItem();
        /** @var Order $order */
        $order = $orderItemModel->getOrder();
        $supplier = $order->getShoppingCart()->getSupplier();
        /** @var Prospect $prospect */
        $prospect = $order->getCustomer()->getProspect();

        $orderItem->id = $orderItemModel->getId();
        $orderItem->supplierId = $supplier->getId();

        if ($prospect->getType() === Prospect::TYPE_INDIVIDUAL) {
            $orderItem->total = floatval(bcdiv((int)($orderItemModel->getTotal() * 1.055), 100, 2));
            $orderItem->price = floatval(bcdiv((int)($orderItemModel->getUnitPrice() * 1.055), 100, 2));
        } else {
            $orderItem->total = floatval(bcdiv($orderItemModel->getTotal(), 100, 2));
            $orderItem->price = floatval(bcdiv($orderItemModel->getUnitPrice(), 100, 2));
        }

        $orderItem->quantity = $orderItemModel->getQuantityFloat();
        $orderItem->pid = $orderItemModel->getProduct()->getId();
        $orderItem->vid = $orderItemModel->getVariant()->getId();

        /** @noinspection PhpParamsInspection */
        $productData = $this->productFinder->getProduct(
            $orderItemModel->getVariant(),
            $supplier,
            $order->getShoppingCart()->getRestaurant()
        );

        if (!empty($productData)) {
            $orderItem->name = $productData['productName'];
            $orderItem->image = $productData['productPicture'];
            $orderItem->imageLarge = $productData['productPictureLarge'];
            $orderItem->createdByUser = $productData['manuallyCreated'];

            $category = [];

            foreach ($productData['tree'] as $i => $taxon) {
                $category[] = ($i === 0 ? 'Catalogue' : $taxon['name']);

                switch ($taxon['id']) {
                    case 69: // Fruits & Légumes
                    case 11: // Marée
                    case 2: // Crèmerie
                    case 34: // Boucherie
                    case 507: // Charcuterie
                    case 9: // Epicerie salée
                    case 10: // Epicerie sucrée
                    case 296: // Divers
                    case 503: // Boissons
                    case 275: // Traiteur
                    case 537: // Consommmables
                        $orderItem->mainCategory = $taxon['id'];
                        break;
                }
            }

            $orderItem->category = implode(' > ', $category);

            if ($orderItem->mainCategory === null) {
                $orderItem->mainCategory = isset($product['tree'][1]) ? $productData['tree'][1]['id'] : '';
            }
        } else {
            $orderItem->name = $orderItemModel->getProductName();
            $orderItem->image = "";
            $orderItem->imageLarge = "";
            $orderItem->category = "";
            $orderItem->mainCategory = 0;
            $orderItem->createdByUser = false;
        }

        foreach ($order->getShoppingCart()->getItems() as $shoppingCartItem) {
            if ($shoppingCartItem->getProductVariant()->getId() == $orderItemModel->getVariant()->getId()) {
                $orderItem->unit = $shoppingCartItem->getUnit();
                break;
            }
        }

        return $orderItem;
    }
}
