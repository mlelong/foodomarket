<?php


namespace ApiBundle\Resolver\Traits;


use ApiBundle\Dto\Address;
use ApiBundle\Entity\Order;
use AppBundle\Entity\Order as OrderModel;
use AppBundle\Entity\OrderPayment;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Supplier;

trait OrderModelToPopo
{
    /**
     * @param OrderModel $orderModel
     * @param string|null $paymentUrl
     * @param string|null $paymentException
     * @param Supplier|null $supplier
     * @return Order|null
     */
    private function model2Popo(OrderModel $orderModel, ?string $paymentUrl = null, ?string $paymentException = null, ?Supplier $supplier = null)
    {
        if ($supplier === null) {
            $supplier = $orderModel->getShoppingCart()->getSupplier();
        }

        $order = new Order();

        $order->id = $orderModel->getId();
        $order->reference = $orderModel->getNumber();
        $order->supplier = $supplier->getDisplayName();
        $order->supplierId = $supplier->getId();

        /** @var Prospect $prospect */
        $prospect = $orderModel->getCustomer()->getProspect();

        if ($prospect->getType() === Prospect::TYPE_INDIVIDUAL) {
            $order->total = floatval(bcdiv((int)($orderModel->getItemsTotal() * 1.055), 100, 2));
            $order->shippingCost = floatval(bcdiv($orderModel->getShippingTotal(), 100, 2));
        } else {
            $order->total = floatval(bcdiv($orderModel->getItemsTotal(), 100, 2));
            $order->shippingCost = 0.0;
        }

        $order->date = $orderModel->getCreatedAt();
        $order->currentStatus = $orderModel->getState();

        $address = new Address();

        $shippingAddress = $orderModel->getShippingAddress();

        $address->number = "";
        $address->street = $shippingAddress->getStreet();
        $address->city = $shippingAddress->getCity();
        $address->country = $shippingAddress->getCountryCode();
        $address->zip = $shippingAddress->getPostcode();

        $order->shippingAddress = $address;

        $order->items = [];
        $order->statusHistory = [];

        $order->paymentStatus = $orderModel->getPaymentState();
        $order->paymentUrl = $paymentUrl;
        $order->paymentException = $paymentException;

        return $order;
    }
}
