<?php


namespace ApiBundle\Resolver;


use ApiBundle\Resolver\Traits\ShoppingListItemModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\RestaurantStockItemRepository;
use AppBundle\Repository\RestaurantStockRepository;
use GraphQL\Type\Definition\ResolveInfo;

class ShoppingListItemMutationResolver implements MutationResolverInterface
{
    use TokenAwareTrait,
        ShoppingListItemModelToPopo;

    /** @var RestaurantStockItemRepository */
    private $shoppingListItemRepository;

    /** @var RestaurantStockRepository */
    private $shoppingListRepository;

    /** @var ProductVariantRepository */
    private $productVariantRepository;

    public function __construct(
        RestaurantStockItemRepository $shoppingListItemRepository,
        RestaurantStockRepository $shoppingListRepository,
        ProductVariantRepository $productVariantRepository
    ) {
        $this->shoppingListItemRepository = $shoppingListItemRepository;
        $this->shoppingListRepository = $shoppingListRepository;
        $this->productVariantRepository = $productVariantRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'createShoppingListItem':
                $shoppingListId = $context['args']['input']['shoppingListId'];
                $variantId = $context['args']['input']['variantId'];
                $unit = strtoupper($context['args']['input']['unit']);

                /** @var RestaurantStock $shoppingList */
                $shoppingList = $this->shoppingListRepository->find($shoppingListId);

                if ($shoppingList === null) {
                    return null;
                }

                /** @var RestaurantStockItem $shoppingListItemModel */
                $shoppingListItemModel = $this->shoppingListItemRepository->findOneBy(['restaurantStock' => $shoppingListId, 'productVariant' => $variantId, 'supplier' => $shoppingList->getSupplier()]);

                if ($shoppingListItemModel !== null) {
                    return $this->model2Popo($shoppingListItemModel);
                }

                /** @var ProductVariant $variant */
                $variant = $this->productVariantRepository->find($variantId);

                if ($variant === null) {
                    return null;
                }

                $shoppingListItemModel = new RestaurantStockItem();

                $shoppingListItemModel->setRestaurantStock($shoppingList);
                $shoppingListItemModel->setSupplier($shoppingList->getSupplier());
                $shoppingListItemModel->setProductVariant($variant);
                $shoppingListItemModel->setStock(1);
                $shoppingListItemModel->setStockUnit($unit);

                $this->shoppingListItemRepository->add($shoppingListItemModel);

                return $this->model2Popo($shoppingListItemModel);
            default:
                return null;
        }
    }
}
