<?php


namespace ApiBundle\Resolver;

use ApiBundle\Entity\Restaurant;
use ApiBundle\Resolver\Traits\RestaurantModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\Restaurant as RestaurantModel;
use AppBundle\Repository\RestaurantRepository;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class RestaurantMutationResolver
 * @package ApiBundle\Resolver
 */
class RestaurantMutationResolver implements MutationResolverInterface
{
    use TokenAwareTrait,
        RestaurantModelToPopo;

    /** @var RestaurantRepository */
    private $restaurantRepository;

    /**
     * RestaurantMutationResolver constructor.
     * @param RestaurantRepository $restaurantRepository
     */
    public function __construct(RestaurantRepository $restaurantRepository)
    {
        $this->restaurantRepository = $restaurantRepository;
    }

    /**
     * @param Restaurant $item
     * @param array $context
     * @return Restaurant
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        if (!in_array($info->fieldName, ['createRestaurant', 'editRestaurant'])) {
            return null;
        }

        // Persist
        if ($item->id === null) {
            $restaurantModel = new RestaurantModel();

            $restaurantModel->addCustomer($this->getUser()->getCustomer());
        } else {
            $restaurantModel = $this->restaurantRepository->find($item->id);
        }

        $restaurantModel->setName($item->name);
        $restaurantModel->setCity($item->address->city);
        $restaurantModel->setCountryCode(strtoupper(substr($item->address->country, 0, 2)));
        $restaurantModel->setStreet(implode(' ', array_filter([$item->address->number, $item->address->street])));
        $restaurantModel->setPostcode($item->address->zip);
        $restaurantModel->setEnabled(true);

        $this->restaurantRepository->add($restaurantModel);

        // Prepare response
        return $this->model2Popo($restaurantModel);
    }
}
