<?php


namespace ApiBundle\Resolver;


use ApiBundle\Entity\Supplier;
use ApiBundle\Resolver\Traits\SupplierModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Entity\SupplierCategory;
use AppBundle\Repository\SupplierAccountLogRepository;
use AppBundle\Repository\SupplierCategoryRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\RedisCacheManager;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use GraphQL\Type\Definition\ResolveInfo;
use Sylius\Bundle\ChannelBundle\Doctrine\ORM\ChannelRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Channel\Factory\ChannelFactory;
use Sylius\Component\Core\Model\Channel;

class SupplierMutationResolver implements MutationResolverInterface
{
    use TokenAwareTrait,
        SupplierModelToPopo;

    /** @var ManagerRegistry */
    private $doctrine;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var ChannelRepository */
    private $channelRepository;

    /** @var EntityRepository */
    private $localeRepository;

    /** @var EntityRepository */
    private $currencyRepository;

    /** @var ChannelFactory */
    private $channelFactory;

    /** @var SupplierCategoryRepository */
    private $supplierCategoryRepository;

    /** @var SupplierAccountLogRepository */
    private $supplierAccountLogRepository;

    /** @var RedisCacheManager */
    private $cacheManager;

    public function __construct(
        ManagerRegistry $doctrine, SupplierRepository $supplierRepository, ChannelRepository $channelRepository,
        SupplierCategoryRepository $supplierCategoryRepository, EntityRepository $localeRepository, EntityRepository $currencyRepository,
        ChannelFactory $channelFactory, SupplierAccountLogRepository $supplierAccountLogRepository, RedisCacheManager $cacheManager)
    {
        $this->doctrine = $doctrine;
        $this->supplierRepository = $supplierRepository;
        $this->channelRepository = $channelRepository;
        $this->localeRepository = $localeRepository;
        $this->currencyRepository = $currencyRepository;
        $this->channelFactory = $channelFactory;
        $this->supplierCategoryRepository = $supplierCategoryRepository;
        $this->supplierAccountLogRepository = $supplierAccountLogRepository;
        $this->cacheManager = $cacheManager;
    }

    private function removeShopUserSuppliersCache()
    {
        $prospect = $this->getUser()->getCustomer()->getProspect();

        foreach ($prospect->getCustomers() as $customer) {
            foreach ($customer->getRestaurants() as $restaurant) {
                $this->cacheManager->deleteItem(str_replace('{restaurantId}', $restaurant->getId(), RedisCacheManager::CACHE_KEY_SHOP_USER_SUPPLIERS));
            }
        }
    }

    /**
     * @param Supplier|null $item
     * @param array $context
     * @return object|null
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'createSupplier':
                $data = $this->createSupplier($item);
                $this->associateSupplier($data['supplierCategory']);

                /** @var SupplierModel $model */
                $model = $data['supplier'];

                $this->removeShopUserSuppliersCache();

                return $this->model2Popo($model, $this->getUser()->getCustomer()->getProspect());
            case 'editSupplier':
                $model = $this->editSupplier($item);

                $this->removeShopUserSuppliersCache();

                return $this->model2Popo($model, $this->getUser()->getCustomer()->getProspect());
            case 'bindSupplier':
                $supplierId = $context['args']['input']['supplierId'];
                /** @var SupplierModel $model */
                $model = $this->supplierRepository->find($supplierId);
                $suppliers = $model->getPartnerSuppliers()->toArray();
                $supplierCategories = $this->supplierCategoryRepository->findBy(['supplier' => $suppliers]);

                // On crée les supplierCategories s'il n'en existe pas déjà pour ce fournisseur
                if (empty($supplierCategories)) {
                    foreach ($suppliers as $partner) {
                        $supplierCategory = new SupplierCategory();

                        $supplierCategory->setSupplier($partner);
                        $supplierCategory->setCategory(SupplierModel::CAT_DEFAULT);

                        $this->doctrine->getManager()->persist($supplierCategory);

                        $supplierCategories[] = $supplierCategory;
                    }

                    $this->doctrine->getManager()->flush();
                }

                foreach ($supplierCategories as $supplierCategory) {
                    if ($supplierId == 11 && $supplierCategory->getSupplier()->getId() == 18) {
                        $this->associateSupplier($supplierCategory);
                    } elseif ($supplierId != 11) {
                        $this->associateSupplier($supplierCategory);
                    }
                }

                $this->removeShopUserSuppliersCache();

                return $this->model2Popo($model, $this->getUser()->getCustomer()->getProspect());
        }

        return null;
    }

    private function associateSupplier(SupplierCategory $supplierCategory)
    {
        $prospect = $this->getUser()->getCustomer()->getProspect();

        if ($this->supplierAccountLogRepository->count(['prospect' => $prospect, 'supplierCategory' => $supplierCategory]) === 0) {
            $supplierAccountLog = new SupplierAccountLog();

            $supplierAccountLog->setProspect($prospect);
            $supplierAccountLog->setSupplierCategory($supplierCategory);
            $supplierAccountLog->setStatus(SupplierAccountLog::STATUS_ACCOUNT_OPENED);

            $this->doctrine->getManager()->persist($supplierAccountLog);
        }

        $prospect->addSupplier($supplierCategory);

        $this->doctrine->getManager()->flush();
    }

    /**
     * @param Supplier $dto
     * @return SupplierModel|null
     */
    private function editSupplier(Supplier $dto)
    {
        /** @var SupplierModel $supplierModel */
        $supplierModel = $this->supplierRepository->find($dto->id);

        if ($supplierModel && $supplierModel->isCreatedByUser()) {
            $supplierModel->setName($dto->name);
            $supplierModel->setEmail($dto->email);
            $supplierModel->setOrderEmails([$dto->email]);
            $supplierModel->setPhone($dto->telephone);

            $this->doctrine->getManager()->flush();
        }

        return $supplierModel;
    }

    /**
     * @param Supplier $dto
     * @return array|null
     */
    private function createSupplier(Supplier $dto)
    {
        /** @var EntityManager $em */
        $em = $this->doctrine->getManager();

        try {
            return $em->transactional(function (EntityManager $em) use ($dto) {
                // Creates the facade supplier
                $supplier = new SupplierModel();

                $supplier->setName($dto->name);
                $supplier->setEnabled(true);
                $supplier->addCategory(SupplierModel::CAT_DEFAULT);
                $supplier->setPartner($dto->partner);
                $supplier->setCreatedByUser($dto->createdByUser);
                $supplier->setEmail($dto->email);
                $supplier->addOrderEmail($dto->email);
                $supplier->setPhone($dto->telephone);
                $supplier->setStreet("");
                $supplier->setCity("");
                $supplier->setPostcode("");
                $supplier->setCountryCode("");
                $supplier->setShippingCost(0.0);
                $supplier->setFreePort(0.0);
                $supplier->setMinimumOrder(0.0);
                $supplier->setDeliveryOnMonday(1);
                $supplier->setDeliveryOnTuesday(1);
                $supplier->setDeliveryOnWednesday(1);
                $supplier->setDeliveryOnThursday(1);
                $supplier->setDeliveryOnFriday(1);
                $supplier->setDeliveryOnSaturday(1);
                $supplier->setDeliveryOnSunday(1);
                $supplier->setMinimumOrder(0);

                // Creates the channel
                /** @var Channel $channel */
                $channel = $this->channelFactory->createNamed($supplier->getName());

                $channel->setCode(Slugify::create()->slugify($supplier->getName()));
                $channel->setEnabled(true);
                $channel->setTaxCalculationStrategy('order_items_based');
                $channel->setDefaultLocale($this->localeRepository->findOneBy(['code' => 'fr_FR']));
                $channel->setBaseCurrency($this->currencyRepository->findOneBy(['code' => 'EUR']));

                // Persist the channel
                $em->persist($channel);

                // Affect the channel
                $supplier->setChannel($channel);

                // Persist the supplier
                $em->persist($supplier);

                $partner = new SupplierModel();

                $partner->setName('Foodomarket-' . Slugify::create()->slugify($dto->name));
                $partner->setEnabled(false);
                $partner->addCategory(SupplierModel::CAT_DEFAULT);
                $partner->setPartner($dto->partner);
                $partner->setCreatedByUser($dto->createdByUser);
                $partner->setEmail($dto->email);
                $partner->addOrderEmail($dto->email);
                $partner->setPhone($dto->telephone);
                $partner->setStreet("");
                $partner->setCity("");
                $partner->setPostcode("");
                $partner->setCountryCode("");
                $partner->setShippingCost(0.0);
                $partner->setFreePort(0.0);
                $partner->setMinimumOrder(0.0);
                $partner->setFacadeSupplier($supplier);

                $supplier->addPartnerSupplier($partner);

                // Creates the channel
                /** @var Channel $partnerChannel */
                $partnerChannel = $this->channelFactory->createNamed($partner->getName());

                $partnerChannel->setCode(Slugify::create()->slugify($partner->getName()));
                $partnerChannel->setEnabled(true);
                $partnerChannel->setTaxCalculationStrategy('order_items_based');
                $partnerChannel->setDefaultLocale($this->localeRepository->findOneBy(['code' => 'fr_FR']));
                $partnerChannel->setBaseCurrency($this->currencyRepository->findOneBy(['code' => 'EUR']));

                // Persist the channel
                $em->persist($partnerChannel);

                $em->persist($partner);

                // Creates a new SupplierCategory
                $supplierCategory = new SupplierCategory();

                $supplierCategory->setSupplier($partner);
                $supplierCategory->setCategory(SupplierModel::CAT_DEFAULT);

                // Persist the SupplierCategory
                $em->persist($supplierCategory);

                return ['supplier' => $supplier, 'partner' => $partner, 'supplierCategory' => $supplierCategory];
            });
        } catch (\Throwable $e) {
            return null;
        }
    }
}
