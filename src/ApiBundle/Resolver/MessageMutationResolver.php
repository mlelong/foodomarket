<?php


namespace ApiBundle\Resolver;


use ApiBundle\Dto\Interlocutor;
use ApiBundle\Resolver\Traits\MessageModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\Email;
use AppBundle\Entity\Message as MessageModel;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Repository\MessageRepository;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueV3Manager;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\Routing\RouterInterface;

class MessageMutationResolver implements MutationResolverInterface
{
    use TokenAwareTrait,
        MessageModelToPopo;

    /** @var MessageRepository */
    private $messageRepository;

    /** @var EmailFactory */
    private $emailFactory;

    /** @var SendInBlueV3Manager */
    private $emailManager;

    /** @var RouterInterface */
    private $router;

    public function __construct(MessageRepository $messageRepository, EmailFactory $emailFactory, SendInBlueV3Manager $emailManager, RouterInterface $router)
    {
        $this->messageRepository = $messageRepository;
        $this->emailFactory = $emailFactory;
        $this->emailManager = $emailManager;
        $this->router = $router;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'createMessage':
                $supplierId = $context['args']['input']['supplierId'];
                $content = $context['args']['input']['content'];
                $important = $context['args']['input']['important'] ?? false;
                $picture = $context['args']['input']['picture'] ?? null;

                /** @var SupplierModel $supplier */
                $supplier = $this->supplierRepository->find($supplierId);
                $prospect = $this->getUser()->getCustomer()->getProspect();

                $trials = 0;
                $realToken = null;

                while ($trials < 10) {
                    try {
                        $token = bin2hex(random_bytes(64));

                        if ($this->messageRepository->count(['token' => $token]) === 0) {
                            $realToken = $token;
                            break ;
                        }
                    } catch (Exception $e) {
                        ++$trials;
                    }
                }

                // Sends the email
                $builder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);
                $builder->addTo($supplier->getFacadeSupplier()->getEmail());

//                foreach ($supplier->getOrderEmails() as $email) {
//                    $builder->addTo($email);
//                }

                $content2 = nl2br($content);
                $replyUrl = $this->router->generate('api_supplier_message_index', ['token' => $realToken], RouterInterface::ABSOLUTE_URL);

                $fromName = "{$prospect->getFirstName()} {$prospect->getContactName()} ({$prospect->getRestaurantName()})";

                $htmlContent = "
                    <div>
                        <p>Nouveau message de la part de: $fromName</p><br/>
                        <div style=\"padding:10px;border:2px dashed #C2C2C2;border-radius:10px\">
                            $content2
                        </div>
                        <br/>
                        <p style=\"color:red;font-weight:bolder;font-family:arial;font-size:20px\">/!\</p>
                        <p style=\"color:red;font-weight:bolder;font-family:arial;font-size:20px\">Attention : merci de ne pas répondre directement à cet email.
                            Pour contacter votre client, cliquez sur le bouton ci-dessous.
                        </p>
                        <p style=\"color:red;font-weight:bolder;font-family:arial;font-size:20px\">/!\</p>
                        <br/>
                        <a href=\"$replyUrl\" style=\"display:block;color:white;background-color:#33d573;border-radius:15px;padding:20px 50px;text-decoration:none;font-family:arial;font-size:30px;text-transform:uppercase\">Répondre au client</a>
                    </div>
                ";

                $email = $builder
                    ->setSubject("[FoodoMarket] Conversation avec {$this->getUser()->getCustomer()->getFirstName()} : nouveau message")
                    ->setHtmlBody($htmlContent)
                    ->addReplyTo('no-reply@foodomarket.com')
                    ->addBcc('serviceclient@foodomarket.com')
                    ->addFrom('olivier@foodomarket.com')
                    ->build()
                ;

                $this->emailManager->addEmailToQueue($email);

                // Register the message
                $message = new MessageModel();

                $message->setMessage($content);
                $message->setSenderType(Interlocutor::TYPE_PROSPECT);
                $message->setSender($prospect);
                $message->setRecipientType(Interlocutor::TYPE_SUPPLIER);
                $message->setRecipient($supplier);
                $message->setStatus(MessageModel::MESSAGE_STATUS_WAITING);
                $message->setChannel(MessageModel::CHANNEL_EMAIL);
                $message->setChannelStatus(MessageModel::CHANNEL_STATUS_WAITING);
                $message->setImportant($important);
                $message->setEmail($email);
                $message->setToken($realToken);
                $message->setPicture($picture);

                $this->messageRepository->add($message);

                return $this->model2Popo($message);
        }

        return null;
    }
}
