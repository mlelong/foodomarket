<?php


namespace ApiBundle\Resolver;


use ApiBundle\Entity\Product;
use ApiBundle\Resolver\Traits\ProductModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\QueryItemResolverInterface;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\Product\ProductFinderInterface;
use GraphQL\Type\Definition\ResolveInfo;

class ProductItemResolver implements QueryItemResolverInterface
{
    use TokenAwareTrait,
        ProductModelToPopo;

    /** @var ProductFinderInterface */
    private $productFinder;

    /** @var ProductVariantRepository */
    private $productVariantRepository;

    /** @var RestaurantRepository */
    private $restaurantRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    public function __construct(
        ProductFinderInterface $productFinder,
        ProductVariantRepository $productVariantRepository,
        RestaurantRepository $restaurantRepository,
        SupplierRepository $supplierRepository
    ) {
        $this->productFinder = $productFinder;
        $this->productVariantRepository = $productVariantRepository;
        $this->restaurantRepository = $restaurantRepository;
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'getProduct':
                $productId = $context['args']['productId'];
                $restaurantId = $context['args']['restaurantId'];
                $supplierId = $context['args']['supplierId'];
                /** @var Restaurant $restaurant */
                $restaurant = $this->restaurantRepository->find($restaurantId);
                /** @var Supplier $supplier */
                $supplier = $this->supplierRepository->find($supplierId);

                /** @noinspection PhpParamsInspection */
                $product = $this->productFinder->getProduct(
                    $this->productVariantRepository->find($productId),
                    $supplier,
                    $restaurant
                );

                if ($product === null) {
                    return null;
                }

                return $this->model2Popo($product, $supplier, $this->getUser()->getCustomer()->getProspect(), $restaurant->getPostcode());
        }

        return null;
    }
}
