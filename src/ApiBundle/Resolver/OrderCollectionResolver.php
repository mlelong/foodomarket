<?php


namespace ApiBundle\Resolver;


use ApiBundle\Entity\Order;
use ApiBundle\Resolver\Traits\OrderModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ArrayPaginator;
use ApiPlatform\Core\GraphQl\Resolver\QueryCollectionResolverInterface;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Order as OrderModel;
use AppBundle\Repository\OrderRepository;
use GraphQL\Type\Definition\ResolveInfo;

class OrderCollectionResolver implements QueryCollectionResolverInterface
{
    use TokenAwareTrait,
        OrderModelToPopo;

    /** @var OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];
        $ret = [];
        $offset = $context['args']['offset'] ?? 0;
        $limit = $context['args']['limit'] ?? PHP_INT_MAX;

        switch ($info->fieldName) {
            case 'getAllOrders':
                $customers = $this->getCustomers();
                /** @var OrderModel[] $orders */
                $orders = $this->orderRepository->createQueryBuilder('o')
                    ->andWhere('o.customer IN (:customer)')
                    ->andWhere('o.shoppingCart IS NOT NULL')
                    ->addOrderBy('o.createdAt', 'DESC')
                    ->setParameter('customer', $customers)
                    ->setFirstResult($offset)
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getResult()
                ;
                $ret = $this->orderModelCollectionToOrderPopoCollection($orders);

                break ;
            case 'restaurantOrders':
                $customers = $this->getRestaurantCustomers($context['args']['restaurantId']);
                /** @var OrderModel[] $orders */
                $orders = $this->orderRepository->createQueryBuilder('o')
                    ->andWhere('o.customer IN (:customer)')
                    ->andWhere('o.shoppingCart IS NOT NULL')
                    ->addOrderBy('o.createdAt', 'DESC')
                    ->setParameter('customer', $customers)
                    ->setFirstResult($offset)
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getResult()
                ;
                $ret = $this->orderModelCollectionToOrderPopoCollection($orders);

                break ;
        }

        return new ArrayPaginator($ret, 0, $limit !== PHP_INT_MAX ? $limit : count($ret));
    }

    /**
     * @param OrderModel[] $orders
     * @return Order[]
     */
    private function orderModelCollectionToOrderPopoCollection(array $orders)
    {
        $ret = [];

        foreach ($orders as $order) {
            if ($popo = $this->model2Popo($order)) {
                $ret[] = $popo;
            }
        }

        return $ret;
    }

    /**
     * @return Customer[]
     */
    private function getCustomers()
    {
        $customers = [];

        foreach ($this->getUser()->getCustomer()->getProspect()->getCustomers() as $customer) {
            $customers[] = $customer;
        }

        return $customers;
    }

    /**
     * @param string $restaurantId
     * @return Customer[]
     */
    private function getRestaurantCustomers(string $restaurantId)
    {
        $customers = $this->getCustomers();
        $restaurantCustomers = [];

        foreach ($customers as $customer) {
            foreach ($customer->getRestaurants() as $restaurant) {
                if ($restaurant->getId() == $restaurantId) {
                    $restaurantCustomers[$customer->getId()] = $customer;
                }
            }
        }

        return array_values($restaurantCustomers);
    }
}