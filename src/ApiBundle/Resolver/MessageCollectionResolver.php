<?php


namespace ApiBundle\Resolver;


use ApiBundle\Dto\Interlocutor;
use ApiBundle\Entity\Message;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\QueryCollectionResolverInterface;
use AppBundle\Entity\Message as MessageModel;
use Doctrine\Common\Persistence\ManagerRegistry;
use GraphQL\Type\Definition\ResolveInfo;

class MessageCollectionResolver implements QueryCollectionResolverInterface
{
    use TokenAwareTrait;

    /** @var ManagerRegistry */
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'getMessages':
                if (isset($context['args']['markAsRead']) && $context['args']['markAsRead']) {
                    /** @var Message $item */
                    foreach ($collection as $item) {
                        // On marque les messages du supplier en lus
                        if ($item->sender->type !== Interlocutor::TYPE_PROSPECT) {
                            $messageModel = $this->doctrine->getRepository(MessageModel::class)->findOneBy(['id' => $item->id, 'read' => false]);

                            if ($messageModel) {
                                $messageModel->setRead(true);
                            }
                        }
                    }

                    $this->doctrine->getManager()->flush();
                }

                return $collection;
            default:
                return [];
        }
    }
}
