<?php


namespace ApiBundle\Resolver;


use ApiBundle\Entity\User;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\QueryItemResolverInterface;
use AppBundle\Entity\Order;
use AppBundle\Entity\SupplierAccountLog;
use AppBundle\Repository\OrderRepository;
use GraphQL\Type\Definition\ResolveInfo;

class UserItemResolver implements QueryItemResolverInterface
{
    use TokenAwareTrait;

    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'getUser':
                $prospect = $this->getUser()->getCustomer()->getProspect();
                $user = new User();

                $user->id = $prospect->getId();

                // Count restaurants
                $restaurants = [];

                foreach ($prospect->getCustomers() as $customer) {
                    foreach ($customer->getRestaurants() as $restaurant) {
                        $restaurants[$restaurant->getId()] = $restaurant;
                    }
                }

                // Count orders
                /** @var Order[] $orders */
                $orders = $this->orderRepository->createQueryBuilder('o')
                    ->select('o')
                    ->join('o.shoppingCart', 'sc')
                    ->where('sc.restaurant IN (:restaurants)')
                    ->orderBy('o.createdAt', 'ASC')
                    ->setParameter('restaurants', $restaurants)
                    ->getQuery()
                    ->getResult()
                ;

                // Count active supplier accounts
                $nbActiveSupplierAccounts = 0;
                $bySupplier = [];

                foreach ($prospect->getSupplierAccountLogs() as $supplierAccountLog) {
                    $bySupplier[$supplierAccountLog->getSupplierCategory()->getSupplier()->getId()] = $supplierAccountLog->getStatus();
                }

                foreach ($bySupplier as $supplierId => $status) {
                    if (in_array($status, [SupplierAccountLog::STATUS_ACCOUNT_OPENED, SupplierAccountLog::STATUS_ORDERING])) {
                        ++$nbActiveSupplierAccounts;
                    }
                }

                // Compute average daily spendings
                $avgDailySpending = $totalOrderAmount = 0;
                $nbOrders = count($orders);

                if ($nbOrders > 0) {
                    $firstOrderDate = $orders[0]->getCreatedAt();
                    $lastOrderDate = $orders[$nbOrders - 1]->getCreatedAt();

                    foreach ($orders as $order) {
                        $totalOrderAmount += (float)bcdiv($order->getItemsTotal(), 100, 2);
                    }

                    $nbDaysSinceFirstOrder = $lastOrderDate->diff($firstOrderDate)->days;
                    $avgDailySpending = $nbDaysSinceFirstOrder === 0 ? $totalOrderAmount : $totalOrderAmount / $nbDaysSinceFirstOrder;
                }

                $user->stats = [
                    'nbRestaurants' => count($restaurants),
                    'nbOrders' => $nbOrders,
                    'nbActiveSupplierAccounts' => $nbActiveSupplierAccounts,
                    'avgDailySpending' => (float)number_format($avgDailySpending, 2, '.', ''),
                    'totalSpending' => (float)number_format($totalOrderAmount, 2, '.', ''),
                ];

                return $user;
            default:
                return null;
        }
    }
}
