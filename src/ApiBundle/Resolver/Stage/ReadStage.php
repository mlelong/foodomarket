<?php


namespace ApiBundle\Resolver\Stage;


use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\GraphQl\Resolver\Stage\ReadStageInterface;

final class ReadStage implements ReadStageInterface
{
    /** @var ReadStageInterface */
    private $defaultReadStage;

    /** @var IriConverterInterface */
    private $iriConverter;

    public function __construct(ReadStageInterface $readStage)
    {
        $this->defaultReadStage = $readStage;
    }

    /**
     * @required
     * @param IriConverterInterface $iriConverter
     */
    public function setIriConverter(IriConverterInterface $iriConverter)
    {
        $this->iriConverter = $iriConverter;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(?string $resourceClass, ?string $rootClass, string $operationName, array $context)
    {
        if (isset($context['args']['id']) && is_numeric($context['args']['id'])) {
            $context['args']['id'] = $this->iriConverter->getItemIriFromResourceClass($resourceClass, [$context['args']['id']]);
        }

        if (isset($context['args']['input']['id']) && is_numeric($context['args']['input']['id'])) {
            $context['args']['input']['id'] = $this->iriConverter->getItemIriFromResourceClass($resourceClass, [$context['args']['input']['id']]);
        }

        return $this->defaultReadStage->__invoke($resourceClass, $rootClass, $operationName, $context);
    }
}
