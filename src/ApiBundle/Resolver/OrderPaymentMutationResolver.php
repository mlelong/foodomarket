<?php


namespace ApiBundle\Resolver;


use ApiBundle\Resolver\Traits\OrderModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\Order;
use AppBundle\Entity\Order as OrderModel;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\OrderPayment;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Restaurant as RestaurantModel;
use AppBundle\Entity\ShoppingCartItem;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Repository\LyraAliasRepository;
use AppBundle\Repository\OrderPaymentRepository;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\OrderService;
use AppBundle\Services\Payment\Lyra\Client as ClientLyra;
use AppBundle\Services\Payment\Lyra\Exceptions\LyraException;
use AppBundle\Services\ShoppingCartManager;
use AppBundle\Util\PriceModifier;
use AppBundle\Util\SupplierUtil;
use AppBundle\Util\UserUtil;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class OrderPaymentMutationResolver implements MutationResolverInterface
{
    use TokenAwareTrait,
        OrderModelToPopo;

    /** @var EntityManagerInterface */
    private $manager;

    /** @var OrderService */
    private $orderService;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var RestaurantRepository */
    private $restaurantRepository;

    /** @var ProductVariantRepository */
    private $productVariantRepository;

    /** @var OrderPaymentRepository */
    private $orderPaymentRepository;

    /** @var LyraAliasRepository */
    private $lyraAliasRepository;

    /** @var ShoppingCartManager */
    private $shoppingCartManager;

    /** @var ClientLyra */
    private $clientLyra;

    /** @var ParameterBagInterface */
    private $params;

    /** @var UrlGeneratorInterface */
    private $router;

    /** @var string */
    private $kernelEnvironment;

    /** @var string */
    private $hostname;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        EntityManagerInterface $manager,
        OrderService $orderService,
        SupplierRepository $supplierRepository,
        RestaurantRepository $restaurantRepository,
        ProductVariantRepository $productVariantRepository,
        OrderPaymentRepository $orderPaymentRepository,
        LyraAliasRepository $lyraAliasRepository,
        ClientLyra $clientLyra,
        ParameterBagInterface $params,
        UrlGeneratorInterface $router,
        LoggerInterface $logger,
        string $kernelEnvironment,
        string $hostname
    ) {
        $this->manager = $manager;
        $this->orderService = $orderService;
        $this->supplierRepository = $supplierRepository;
        $this->restaurantRepository = $restaurantRepository;
        $this->productVariantRepository = $productVariantRepository;
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->lyraAliasRepository = $lyraAliasRepository;
        $this->clientLyra = $clientLyra;
        $this->params = $params;
        $this->router = $router;
        $this->kernelEnvironment = $kernelEnvironment;
        $this->hostname = $hostname;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'createWithPaymentOrder':
                $order = $this->createOrder($context);
                break;
            case 'paymentShopOrder':
                $order = $this->createOrderFromCartId($context['args']['input']['shoppingCartId']);
                break;
            default:
                return null;
        }

        $supplier = $order->getShoppingCart()->getSupplier();
        $paymentUrl = $this->proceedWithPayment($order, $context);

        if ($paymentUrl instanceof LyraException) {

            if ($info->fieldName === 'paymentShopOrder') {
                $shoppingCart = $order->getShoppingCart();
                $shoppingCart->setOrdered(false);

                $order->setState('failed');
                $order->setShoppingCart(null);

                /** @var OrderItem $item */
                foreach ($order->getItems() as $item) {
                    $item->setShoppingCartItem(null);
                }

                $this->manager->flush();
            }

            return $this->model2Popo($order, null, $paymentUrl->getMessage(), $supplier);
        } else {
            return $this->model2Popo($order, $paymentUrl, null);
        }
    }

    /**
     * @param string $shoppingCartId
     * @return OrderModel
     */
    private function createOrderFromCartId(string $shoppingCartId)
    {
        $shoppingCart = $this->manager->getRepository(ShoppingCart::class)->find($shoppingCartId);

        $order = $shoppingCart->getOrder();

        if ($order) {
            return $order;
        }

        $order = $this->orderService->newFloatingOrder(
            $this->getUser(),
            $shoppingCart
        );

        $shoppingCart->setOrdered(true);
        $shoppingCart->setOrder($order);

        $this->manager->flush();

        return $order;
    }

    /**
     * @param array $context
     * @return OrderModel
     */
    private function createOrder(array $context)
    {
        /** @var SupplierModel $supplier */
        $supplier = $this->supplierRepository->find($context['args']['input']['supplierId']);
        /** @var RestaurantModel $restaurant */
        $restaurant = $this->restaurantRepository->find($context['args']['input']['restaurantId']);

        $shoppingCart = new ShoppingCart();

        $shoppingCart->setSupplier($supplier);
        $shoppingCart->setRestaurant($restaurant);
        $shoppingCart->setNote($context['args']['input']['note'] ?? null);

        try {
            $shoppingCart->setScheduledDate(new DateTime($context['args']['input']['scheduledDate']));
        } catch (Exception $e) {
            // TODO: Handle this case
        }

        foreach ($context['args']['input']['items'] as $item) {
            if ($item['quantity'] <= 0) {
                continue;
            }

            /** @var ProductVariant $variant */
            $variant = $this->productVariantRepository->find($item['variantId']);
            $shoppingCartItem = new ShoppingCartItem();

            $shoppingCartItem->setProductVariant($variant);
            $shoppingCartItem->setQuantity($item['quantity']);
            $shoppingCartItem->setUnit($item['unit']);

            $shoppingCart->addItem($shoppingCartItem);
        }

        $this->manager->persist($shoppingCart);
        $this->manager->flush();

        $order = $this->orderService->newFloatingOrder(
            $this->getUser(),
            $shoppingCart
        );

        $shoppingCart->setOrdered(true);
        $shoppingCart->setOrder($order);

        $this->manager->flush();

        return $order;
    }

    /**
     * Returns the payment url if successfully prepared and executed
     *
     * @param OrderModel $order
     * @param array $context
     * @return string|LyraException
     * @throws Exception
     */
    private function proceedWithPayment(Order $order, array $context)
    {
        $supplier = $order->getShoppingCart()->getSupplier();

        if ($this->kernelEnvironment === 'prod' && $this->hostname === 'www.foodomarket.com') {
            $lyraSellerId = $supplier->getLyraUuid();
        } else {
            $lyraSellerId = $supplier->getLyraDevUuid();
        }

        if (empty($lyraSellerId)) {
            return null;
        }

        $prospect = UserUtil::getProspect($this->getUser());

        try {
            $shoppingCart = $order->getShoppingCart();
            $amountTTC = PriceModifier::modify(
                $order->getItemsTotal(),
                $prospect,
                $shoppingCart->getRestaurant()->getPostcode(),
                $shoppingCart->getSupplier(),
                true
            );
            $deliveryCost = (int)(SupplierUtil::getDeliveryCost($supplier, $prospect->getZipCode()) * 100);

            if (in_array($supplier->getId(), [79, 83]) && $deliveryCost > 0) {
                $totalWeight = $order->getTotalWeight();
                $nbPackages = (int)ceil($totalWeight / 25);
                $amountTTC += ($nbPackages * $deliveryCost);
            }

            $description = "Commande {$order->getNumber()}";

            /**
             * Préparation du paiement
             */

            if ($prospect->getType() === 'professionnal') {
                $amountTTC = (int)($amountTTC * 1.3);
            }

            $phone = $prospect->getMobile() ?: $prospect->getPhone();
            $phone = $phone ?: "0101010101";

            $lyraOrderData = [
                "marketplace" => $this->params->get('lyra.marketplace'),
                "reference" => "{$order->getNumber()}",
                "description" => $description,
                "currency" => "EUR",
                "awaiting_validation" => true,
                "items" => array(
                    array(
                        "seller" => $lyraSellerId,
                        "reference" => "{$order->getNumber()}",
                        "description" => $description,
                        "amount" => $amountTTC,
                        "is_commission" => false,
                    ),
                ),
                "buyer" => array(
                    "type" => "PRIVATE",
                    "email" => $prospect->getEmail(),
                    "first_name" => $prospect->getFirstName(),
                    "last_name" => '',
                    "phone_number" => $phone,
                    "reference" => "P{$prospect->getId()}",
                ),
                "shipping" => array("shipping_method" => "PACKAGE_DELIVERY_COMPANY"),
            ];

            //Setting the proper return url
            if (isset($context['args']['input']['orderLocation'])) {
                switch ($context['args']['input']['orderLocation']) {
                    case 'shop':
                        $lyraOrderData['url_return'] = $this->router->generate('app_front_shopping_cart_page', [
                            'orderId' => $order->getId(),
                            'createAlias' => isset($context['args']['input']['createAlias']) && $context['args']['input']['createAlias'],
                        ], UrlGeneratorInterface::ABSOLUTE_URL);
                        break;
                }
            }

            //Checking if a payment by alias has been requested
            if (isset($context['args']['input']['useAlias']) &&
                $context['args']['input']['useAlias'] &&
                isset($context['args']['input']['aliasId']) &&
                $context['args']['input']['aliasId']) {

                //Getting the alias
                $lyraAlias = $this->lyraAliasRepository->find($context['args']['input']['aliasId']);

                //@TODO: reduce nesting
                if ($lyraAlias) {
                    //Checking alias ownership and validity
                    if ($lyraAlias->getProspect() === $prospect) {
                        $now = new DateTime();
                        if ($now < $lyraAlias->getExpirationDate()) {
                            $lyraOrderData['alias'] = $lyraAlias->getLyraAliasUuid();
                        }
                    }
                }
            }

            $ret = $this->clientLyra->post("orders?expand=items", $lyraOrderData);
            $transactions = $this->clientLyra->get("orders/{$ret['uuid']}/transactions");

            $orderPayment = new OrderPayment();

            $orderPayment->setOrder($order);
            $orderPayment->setLyraId($ret['uuid']);
            $orderPayment->setLyraOrderStatus($ret['status']);
            $orderPayment->setLyraOrderData($ret);

            if (isset($transactions['results']) && !empty($transactions['results'])) {
                $orderPayment->setLyraPaymentStatus($transactions['results'][0]['status']);
                $orderPayment->setLyraPaymentData($transactions['results'][0]);

                $order->setPaymentState($orderPayment->getLyraPaymentStatus());
            } else {
                $orderPayment->setLyraPaymentStatus($orderPayment->getLyraOrderStatus());
                $orderPayment->setLyraPaymentData([]);

                // The add below will flush...
                $order->setPaymentState($orderPayment->getLyraOrderStatus());
            }

            $this->orderPaymentRepository->add($orderPayment);

            /**
             * Maintenant on peut exécuter le paiement
             */

            //Checking if an alias creation has been requested
            if (isset($context['args']['input']['createAlias']) && $context['args']['input']['createAlias']) {
                $ret = $this->clientLyra->get("orders/{$orderPayment->getLyraId()}/execute/token");

            } elseif (isset($context['args']['input']['useAlias']) && $context['args']['input']['useAlias']) {
                try {
                    $ret = $this->clientLyra->get("orders/{$orderPayment->getLyraId()}/execute-mit");
                } catch (LyraException $exception) {
                    $ret['error'] = true;
                }

                //If response contains an error key, the alias is probably invalid, try without alias
                if (isset($ret['error'])) {
                    $ret = $this->clientLyra->get("orders/{$orderPayment->getLyraId()}/execute");
                }

            } else {
                $ret = $this->clientLyra->get("orders/{$orderPayment->getLyraId()}/execute");
            }

            if (isset($context['args']['input']['useAlias']) && $context['args']['input']['useAlias']) {
                //Getting associated transaction
                $transactions = $this->clientLyra->get("orders/{$orderPayment->getLyraId()}/transactions");

                return $transactions['results'][0]['auto_code'];
            } else {
                return $ret['payment_url'];
            }

        } catch (LyraException $e) {
            $this->logger->emergency("Lyra for prospect {$prospect->getId()}: {$e->getMessage()}}");
            return $e;
        }
    }
}
