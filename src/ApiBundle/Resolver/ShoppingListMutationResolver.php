<?php


namespace ApiBundle\Resolver;


use ApiBundle\Resolver\Traits\ShoppingListModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\Restaurant as RestaurantModel;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\RestaurantStockRepository;
use AppBundle\Repository\SupplierRepository;
use GraphQL\Type\Definition\ResolveInfo;

class ShoppingListMutationResolver implements MutationResolverInterface
{
    use TokenAwareTrait,
        ShoppingListModelToPopo;

    /** @var RestaurantStockRepository */
    private $shoppingListRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var RestaurantRepository */
    private $restaurantRepository;

    public function __construct(
        RestaurantStockRepository $shoppingListRepository,
        SupplierRepository $supplierRepository,
        RestaurantRepository $restaurantRepository
    ) {
        $this->shoppingListRepository = $shoppingListRepository;
        $this->supplierRepository = $supplierRepository;
        $this->restaurantRepository = $restaurantRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'createShoppingList':
                $supplierId = $context['args']['input']['supplierId'];
                $restaurantId = $context['args']['input']['restaurantId'];
                $shoppingListName = $context['args']['input']['listName'];

                // Try to find an existing shoppingCart with these parameters
                /** @var RestaurantStock $shoppingListModel */
                $shoppingListModel = $this->shoppingListRepository->findOneBy(['restaurant' => $restaurantId, 'supplier' => $supplierId]);

                if ($shoppingListModel !== null) {
                    return $this->model2Popo($shoppingListModel);
                }

                $shoppingListModel = new RestaurantStock();

                /** @var SupplierModel $supplier */
                $supplier = $this->supplierRepository->find($supplierId);
                /** @var RestaurantModel $restaurant */
                $restaurant = $this->restaurantRepository->find($restaurantId);

                $shoppingListModel->setSupplier($supplier);
                $shoppingListModel->setRestaurant($restaurant);
                $shoppingListModel->setName($shoppingListName);

                $this->shoppingListRepository->add($shoppingListModel);

                return $this->model2Popo($shoppingListModel);
            default:
                return null;
        }
    }
}
