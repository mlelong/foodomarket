<?php


namespace ApiBundle\Resolver;


use ApiBundle\Entity\Product;
use ApiBundle\Resolver\Traits\ProductModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ArrayPaginator;
use ApiPlatform\Core\GraphQl\Resolver\QueryCollectionResolverInterface;
use AppBundle\Entity\Restaurant;
use AppBundle\Entity\Supplier;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\Product\ProductFinderInterface;
use GraphQL\Type\Definition\ResolveInfo;

class ProductCollectionResolver implements QueryCollectionResolverInterface
{
    use TokenAwareTrait,
        ProductModelToPopo;

    /** @var ProductFinderInterface */
    private $productFinder;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var RestaurantRepository */
    private $restaurantRepository;

    public function __construct(ProductFinderInterface $productFinder, SupplierRepository $supplierRepository, RestaurantRepository $restaurantRepository)
    {
        $this->productFinder = $productFinder;
        $this->supplierRepository = $supplierRepository;
        $this->restaurantRepository = $restaurantRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(iterable $collection, array $context): iterable
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];
        $ret = [];

        switch ($info->fieldName) {
            case 'supplierProducts':
                $restaurantId = $context['args']['restaurantId'];
                $supplierId = $context['args']['supplierId'];
                $offset = $context['args']['offset'];
                /** @var Restaurant $restaurant */
                $restaurant = $this->restaurantRepository->find($restaurantId);
                /** @var Supplier $supplier */
                $supplier = $this->supplierRepository->find($supplierId);
                $products = $this->productFinder->getRestaurantSupplierProducts($restaurant, $supplier);

                foreach ($products as $product) {
                    $ret[] = $this->model2Popo($product, $supplier, $this->getUser()->getCustomer()->getProspect(), $restaurant->getPostcode());
                }

                return new ArrayPaginator($ret, $offset, $context['args']['limit'] ?? count($ret));
        }

        return new ArrayPaginator($ret, 0, count($ret));
    }
}
