<?php


namespace ApiBundle\Resolver;


use ApiBundle\Resolver\Traits\ShoppingListModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\QueryItemResolverInterface;
use AppBundle\Entity\RestaurantStock;
use AppBundle\Repository\RestaurantStockRepository;
use GraphQL\Type\Definition\ResolveInfo;

class ShoppingListItemResolver implements QueryItemResolverInterface
{
    use TokenAwareTrait,
        ShoppingListModelToPopo;

    /** @var RestaurantStockRepository */
    private $shoppingListRepository;

    public function __construct(RestaurantStockRepository $shoppingListRepository)
    {
        $this->shoppingListRepository = $shoppingListRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'getShoppingList':
                $supplierId = $context['args']['supplierId'];
                $restaurantId = $context['args']['restaurantId'];
                /** @var RestaurantStock $shoppingListModel */
                $shoppingListModel = $this->shoppingListRepository->findOneBy(['supplier' => $supplierId, 'restaurant' => $restaurantId]);

                if ($shoppingListModel === null) {
                    return null;
                }

                return $this->model2Popo($shoppingListModel);
            default:
                return null;
        }
    }
}
