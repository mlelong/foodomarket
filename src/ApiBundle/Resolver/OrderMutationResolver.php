<?php


namespace ApiBundle\Resolver;


use ApiBundle\Resolver\Traits\OrderModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use AppBundle\Entity\Order;
use AppBundle\Entity\Order as OrderModel;
use AppBundle\Entity\OrderStatus as OrderStatusModel;
use AppBundle\Entity\ProductVariant;
use AppBundle\Entity\Prospect;
use AppBundle\Entity\Restaurant as RestaurantModel;
use AppBundle\Entity\ShoppingCartItem;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Entity\ShoppingCart;
use AppBundle\Repository\OrderRepository;
use AppBundle\Repository\ProductVariantRepository;
use AppBundle\Repository\RestaurantRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\OrderService;
use AppBundle\Services\ShoppingCartManager;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;

class OrderMutationResolver implements MutationResolverInterface
{
    use TokenAwareTrait,
        OrderModelToPopo;

    /** @var EntityManagerInterface */
    private $manager;

    /** @var OrderService */
    private $orderService;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var RestaurantRepository */
    private $restaurantRepository;

    /** @var ProductVariantRepository */
    private $productVariantRepository;

    /** @var ShoppingCartManager */
    private $shoppingCartManager;

    public function __construct(
        EntityManagerInterface $manager,
        OrderService $orderService,
        SupplierRepository $supplierRepository,
        RestaurantRepository $restaurantRepository,
        ProductVariantRepository $productVariantRepository
    ) {
        $this->manager = $manager;
        $this->orderService = $orderService;
        $this->supplierRepository = $supplierRepository;
        $this->restaurantRepository = $restaurantRepository;
        $this->productVariantRepository = $productVariantRepository;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($item, array $context)
    {
        /** @var ResolveInfo $info */
        $info = $context['info'];

        switch ($info->fieldName) {
            case 'createOrder':
                if ($this->getUser()->getCustomer()->getProspect()->getType() === Prospect::TYPE_INDIVIDUAL) {
                    return null;
                }

                $order = $this->createOrder($context);
                $this->orderService->postCreateOrder($order);

                return $this->model2Popo($order);
            case 'changeStatusOrder':
                $status = $context['args']['input']['status'];

                if (!in_array($status, ['received'])) {
                    return null;
                }

                /** @var OrderRepository $orderRepository */
                $orderRepository = $this->manager->getRepository(Order::class);
                /** @var Order $order */
                $order = $orderRepository->find($item->id);

                if ($order === null) {
                    return null;
                }

                // Security check
                if ($order->getUser()->getId() !== $this->getUser()->getId()) {
                    return null;
                }

                $orderStatus = new OrderStatusModel();

                $orderStatus->setStatus($status);
                $order->addStatusHistory($orderStatus);
                $order->setState($status);

                $this->manager->flush();

                return $this->model2Popo($order);
            default:
                return null;
        }
    }

    /**
     * @param array $context
     * @return OrderModel
     */
    private function createOrder(array $context)
    {
        /** @var SupplierModel $supplier */
        $supplier = $this->supplierRepository->find($context['args']['input']['supplierId']);
        /** @var RestaurantModel $restaurant */
        $restaurant = $this->restaurantRepository->find($context['args']['input']['restaurantId']);

        $shoppingCart = new ShoppingCart();

        $shoppingCart->setSupplier($supplier);
        $shoppingCart->setRestaurant($restaurant);
        $shoppingCart->setNote($context['args']['input']['note'] ?? null);

        try {
            $shoppingCart->setScheduledDate(new DateTime($context['args']['input']['scheduledDate']));
        } catch (\Exception $e) {
            // TODO: Handle this case
        }

        foreach ($context['args']['input']['items'] as $item) {
            if ($item['quantity'] <= 0) {
                continue ;
            }

            /** @var ProductVariant $variant */
            $variant = $this->productVariantRepository->find($item['variantId']);
            $shoppingCartItem = new ShoppingCartItem();

            $shoppingCartItem->setProductVariant($variant);
            $shoppingCartItem->setQuantity($item['quantity']);
            $shoppingCartItem->setUnit($item['unit']);

            $shoppingCart->addItem($shoppingCartItem);
        }

        $this->manager->persist($shoppingCart);
        $this->manager->flush();

        $order = $this->orderService->newFloatingOrder(
            $this->getUser(),
            $shoppingCart
        );

        $shoppingCart->setOrdered(true);
        $shoppingCart->setOrder($order);

        $this->manager->flush();

        return $order;
    }
}