<?php


namespace ApiBundle\Controller;

use AppBundle\Services\Payment\Lyra\LyraPaymentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PaymentController extends AbstractController
{
    /**
     * @var LyraPaymentService
     */
    private $lyraPaymentService;

    public function __construct(LyraPaymentService $lyraPaymentService)
    {
        $this->lyraPaymentService = $lyraPaymentService;
    }

    public function lyraWebhookAction(Request $request)
    {
        $ret = null;

        $lyraId = $request->get('order');

        if($lyraId) {
            $ret = $this->lyraPaymentService->handlePaymentWebhook($lyraId);
        }

        $tokenUuid = $request->get('token');

        if($tokenUuid) {
            $ret = $this->lyraPaymentService->handleTokenWebhook($tokenUuid);
        }

        return $this->json(['ok' => $ret]);
    }
}
