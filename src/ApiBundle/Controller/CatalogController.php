<?php


namespace ApiBundle\Controller;


use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\Catalog\Catalog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CatalogController extends AbstractController
{
    /**
     * @var Catalog
     */
    private $catalog;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    public function __construct(Catalog $catalog, SupplierRepository $supplierRepository)
    {
        $this->catalog = $catalog;
        $this->supplierRepository = $supplierRepository;
    }

    public function treeAction()
    {
        $this->catalog->setSuppliers($this->supplierRepository->getIndividualsPartners());
        $tree = $this->catalog->getTaxonTree();

        return $this->json(['tree' => $tree]);
    }

    public function taxonAction(string $slug)
    {
        $this->catalog->setSuppliers($this->supplierRepository->getIndividualsPartners());
        $tree = $this->catalog->getTreeByTaxonSlug($slug);

        return $this->json($tree);
    }
}
