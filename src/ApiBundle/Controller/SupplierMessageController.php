<?php


namespace ApiBundle\Controller;


use ApiBundle\Dto\Interlocutor;
use AppBundle\Entity\Email;
use AppBundle\Entity\Message;
use AppBundle\Entity\Prospect;
use AppBundle\Repository\MessageRepository;
use AppBundle\Repository\ProspectRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Services\Mailer\EmailBuilder;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueV3Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

class SupplierMessageController extends AbstractController
{
    /** @var MessageRepository */
    private $messageRepository;

    /** @var SupplierRepository */
    private $supplierRepository;

    /** @var ProspectRepository */
    private $prospectRepository;

    /** @var EmailFactory */
    private $emailFactory;

    /** @var SendInBlueV3Manager */
    private $emailManager;

    public function __construct(
        MessageRepository $messageRepository,
        SupplierRepository $supplierRepository,
        ProspectRepository $prospectRepository,
        EmailFactory $emailFactory,
        SendInBlueV3Manager $emailManager
    ) {
        $this->messageRepository = $messageRepository;
        $this->supplierRepository = $supplierRepository;
        $this->prospectRepository = $prospectRepository;
        $this->emailFactory = $emailFactory;
        $this->emailManager = $emailManager;
    }

    public function indexAction(Request $request)
    {
        $token = $request->attributes->get('token');
        /** @var Message $message */
        $message = $this->messageRepository->findOneBy(['token' => $token]);

        if ($message === null) {
            return $this->redirectToRoute('app_front_404_page');
        }

        $messages = $this->messageRepository->getMessages(
            $message->getRecipient()->getId(),
            $message->getSender()->getId()
        );

        /** @var Prospect $prospect */
        $prospect = $this->prospectRepository->find($message->getSender()->getId());

        $messagesDisplay = [];

        foreach ($messages as $message) {
            $picture = $message->getPicture();

            if (is_resource($picture)) {
                $picture = stream_get_contents($picture);
            } else {
                $picture = null;
            }

            $messagesDisplay[] = [
                'isTemplate' => $message->getEmail() !== null && $message->getEmail()->getTemplateId() !== null,
                'message' => $message->getMessage(),
                'senderType' => $message->getSenderType(),
                'picture' => $picture
            ];
        }

        return $this->render('@Api/SupplierMessage/index.html.twig', [
            'messages' => $messagesDisplay,
            'interlocutor' => '',
            'firstname' => $prospect->getFirstName(),
            'restaurant' => $prospect->getRestaurantName(),
            'replyForm' => $this->getReplyForm($token)->createView()
        ]);
    }

    /** @noinspection PhpParamsInspection */
    public function replyAction(Request $request)
    {
        $token = $request->attributes->get('token');
        /** @var Message $message */
        $message = $this->messageRepository->findOneBy(['token' => $token]);

        if ($message === null) {
            return $this->redirectToRoute('app_front_404_page');
        }

        $form = $this->getReplyForm($token);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $content = $form->get('content')->getData();

            /** @var Prospect $prospect */
            $prospect = $this->prospectRepository->find($message->getSender()->getId());

            $newMessage = new Message();

            $newMessage->setSenderType(Interlocutor::TYPE_SUPPLIER);
            $newMessage->setRecipientType(Interlocutor::TYPE_PROSPECT);
            $newMessage->setSender($this->supplierRepository->find($message->getRecipient()->getId()));
            $newMessage->setRecipient($prospect);
            $newMessage->setMessage($content);
            $newMessage->setStatus(Message::MESSAGE_STATUS_SENT);
            $newMessage->setChannel(Message::CHANNEL_EMAIL);
            $newMessage->setChannelStatus(Message::CHANNEL_STATUS_DELIVERED);

            $this->messageRepository->add($newMessage);

            $this->notifyServiceClient($prospect, $content);

            return $this->json([
                'ok' => true,
                'replyForm' => $this->renderView('@Api/SupplierMessage/_replyForm.html.twig', ['replyForm' => $this->getReplyForm($token)->createView()]),
                'message' => ['content' => nl2br($content), 'date' => $newMessage->getCreatedAt()]
            ]);
        }

        return $this->json([
            'ok' => false,
            'replyForm' => $this->renderView('@Api/SupplierMessage/_replyForm.html.twig', ['replyForm' => $this->getReplyForm($token)->createView()])
        ]);
    }

    private function notifyServiceClient(Prospect $prospect, string $content)
    {
        $email = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE)
            ->setSubject("[FoodoMarket] Conversation avec {$prospect->getFirstName()} : nouveau message fournisseur")
            ->setHtmlBody($content)
            ->addTo('serviceclient@foodomarket.com')
            ->addFrom('olivier@foodomarket.com')
            ->build()
        ;

        $this->emailManager->addEmailToQueue($email);
    }

    private function getReplyForm(string $token)
    {
        return $this->createFormBuilder(null, ['action' => $this->generateUrl('api_supplier_message_reply', ['token' => $token])])
            ->add('content', TextareaType::class, ['label' => 'Réponse'])
            ->getForm()
        ;
    }
}
