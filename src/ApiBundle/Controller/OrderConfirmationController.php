<?php


namespace ApiBundle\Controller;


use ApiBundle\Dto\Interlocutor;
use AppBundle\Entity\Email;
use AppBundle\Entity\Message;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderStatus;
use AppBundle\Entity\Prospect;
use AppBundle\Repository\OrderRepository;
use AppBundle\Services\Mailer\EmailFactory;
use AppBundle\Services\Mailer\SendInBlueV3Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class OrderConfirmationController extends AbstractController
{
    /** @var OrderRepository */
    private $orderRepository;

    /** @var EmailFactory */
    private $emailFactory;

    /** @var SendInBlueV3Manager */
    private $emailManager;

    public function __construct(OrderRepository $orderRepository, EmailFactory $emailFactory, SendInBlueV3Manager $emailManager)
    {
        $this->orderRepository = $orderRepository;
        $this->emailFactory = $emailFactory;
        $this->emailManager = $emailManager;
    }

    public function confirmAction(Request $request)
    {
        $id = $request->attributes->get('id');
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if ($order === null) {
            return $this->redirectToRoute('app_front_404_page');
        }

        $orderStatus = new OrderStatus();

        $orderStatus->setStatus('confirmed');

        $order->addStatusHistory($orderStatus);

        $order->setState($orderStatus->getStatus());

        $this->getDoctrine()->getManager()->flush();

        $this->postConfirm($order);

        return $this->render('@Api/OrderConfirmation/index.html.twig');
    }

    private function postConfirm(Order $order)
    {
        // Création d'un message pour le client
        $message = new Message();

        $restaurant = $order->getShoppingCart()->getRestaurant();
        $supplier = $order->getShoppingCart()->getSupplier();
        /** @var Prospect $prospect */
        $prospect = $restaurant->getCustomers()->first()->getProspect();

        $message->setStatus(Message::MESSAGE_STATUS_WAITING);
        $message->setRecipientType(Interlocutor::TYPE_PROSPECT);
        $message->setSenderType(Interlocutor::TYPE_SUPPLIER);
        $message->setSender($supplier);
        $message->setRecipient($prospect);
        $message->setMessage("Bonjour, votre commande a bien été prise en compte.");
        $message->setChannel(Message::CHANNEL_EMAIL);
        $message->setChannelStatus(Message::CHANNEL_STATUS_WAITING);

        $builder = $this->emailFactory->createBuilder(Email::MANAGER_SENDINBLUE);
        $email = $builder
            ->addTo($prospect->getEmail())
            ->addFrom('no-reply@foodomarket.com')
            ->setSubject("[FoodoMarket] Confirmation de votre commande")
            ->setTextBody("Bonjour {$prospect->getFirstName()},\nVotre commande {$supplier->getDisplayName()} a été validée.")
            ->build()
        ;

        $this->emailManager->addEmailToQueue($email);

        $message->setEmail($email);

        $em = $this->getDoctrine()->getManager();

        $em->persist($message);
        $em->flush();
    }
}
