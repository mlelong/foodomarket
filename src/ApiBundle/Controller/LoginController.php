<?php


namespace ApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormInterface;

class LoginController extends Controller
{
    public function loginAction()
    {
        $form = $this->getLoginForm();

        $response = $this->render('@Api/Login/index.html.twig', ['form' => $form->createView()]);

        $response->headers->set('Content-Type', 'text/html');

        return $response;
    }

    /**
     * @return FormInterface
     */
    private function getLoginForm()
    {
        return $this->get('form.factory')->createNamedBuilder(null, 'Symfony\Component\Form\Extension\Core\Type\FormType', null, ['action' => $this->generateUrl('api_login_check')])
            ->add('_username', EmailType::class, ['label' => 'Email'])
            ->add('_password', PasswordType::class, ['label' => 'Mot de passe'])
            ->getForm()
        ;
    }
}
