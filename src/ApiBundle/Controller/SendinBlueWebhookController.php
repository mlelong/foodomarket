<?php


namespace ApiBundle\Controller;


use AppBundle\Entity\Email;
use AppBundle\Entity\Message;
use AppBundle\Entity\OrderStatus;
use AppBundle\Repository\EmailRepository;
use AppBundle\Repository\MessageRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class SendinBlueWebhookController extends AbstractController
{
    /** @var EmailRepository */
    private $emailRepository;

    /** @var MessageRepository */
    private $messageRepository;

    /**
     * @required
     * @param EmailRepository $emailRepository
     */
    public function setEmailRepository(EmailRepository $emailRepository)
    {
        $this->emailRepository = $emailRepository;
    }

    /**
     * @required
     * @param MessageRepository $messageRepository
     */
    public function setMessageRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function hookAction(Request $request)
    {
        $content = $request->getContent();
        $this->debugHook($content);

        $content = json_decode($content, true);
        $messageId = $content['message-id'] ?? null;
        $event = $content['event'] ?? null;
        $date = $content['date'] ?? null;

        if ($messageId !== null && $event !== null && $date !== null) {
            $qb = $this->emailRepository->createQueryBuilder('e');

            /** @var Email $email */
            $email = $qb->where($qb->expr()->like('e.apiResponse', ':messageId'))
                ->setParameter('messageId', "%$messageId%")
                ->getQuery()
                ->getOneOrNullResult()
            ;

            if ($email) {
                /** @var Message|null $message */
                $message = $this->messageRepository->findOneBy(['email' => $email]);

                if ($message) {
                    $message->setChannelMessageId($messageId);
                    $message->setChannelStatus($event);

                    try {
                        $message->setUpdatedAt(new DateTime($date));
                    } catch (\Exception $e) {
                        $message->setUpdatedAt(new DateTime());
                    }

                    switch ($event) {
                        case Message::CHANNEL_STATUS_DELIVERED:
                            $message->setStatus(Message::MESSAGE_STATUS_SENT);
                            break ;
                        case Message::CHANNEL_STATUS_FIRST_OPENED:
                            $message->setStatus(Message::MESSAGE_STATUS_OPENED);
                            break ;
                        case Message::CHANNEL_STATUS_HARDBOUNCE:
                        case Message::CHANNEL_STATUS_INVALID_EMAIL:
                        case Message::CHANNEL_STATUS_ERROR:
                            $message->setStatus(Message::MESSAGE_STATUS_ERROR);
                            break ;
                    }

                    if ($message->getOrder() !== null) {
                        $orderStatus = new OrderStatus();

                        $orderStatus->setStatus($message->getStatus());

                        $message->getOrder()->addStatusHistory($orderStatus);
                        $message->getOrder()->setState($message->getStatus());
                    }

                    $this->getDoctrine()->getManager()->flush();
                }
            }
        }

        return $this->json(['ok' => true]);
    }

    private function debugHook(string $content)
    {
        $out = $this->getParameter('kernel.project_dir') . '/var/hook.json';

        file_put_contents($out, $content);
    }
}
