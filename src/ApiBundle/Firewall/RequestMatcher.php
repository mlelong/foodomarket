<?php


namespace ApiBundle\Firewall;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class RequestMatcher  implements RequestMatcherInterface
{
    /**
     * @inheritDoc
     */
    public function matches(Request $request)
    {
        $route = $request->attributes->get('_route');

        switch ($route) {
            case 'api_graphql_entrypoint':
                if (strpos($request->getContent(), 'IntrospectionQuery') !== false) {
                    return false;
                }

                return true;
            default:
                if (preg_match('/^api_/', $route)) {
                    return true;
                }

                return false;
        }
    }
}
