<?php


namespace ApiBundle\DataPersister;


use ApiBundle\Entity\LyraAlias;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use AppBundle\Repository\LyraAliasRepository;

class LyraAliasItemDataPersister implements ContextAwareDataPersisterInterface
{
    /** @var LyraAliasRepository */
    private $lyraAliasRepository;

    public function __construct(LyraAliasRepository $lyraAliasRepository)
    {
        $this->lyraAliasRepository = $lyraAliasRepository;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof LyraAlias;
    }

    /**
     * @inheritDoc
     */
    public function persist($data, array $context = [])
    {
        $lyraAlias = $this->lyraAliasRepository->find($data->id);
        $lyraAlias->setLabel($data->label);

        $this->lyraAliasRepository->add($lyraAlias);
    }

    /**
     * @param LyraAlias $data
     * @param array $context
     */
    public function remove($data, array $context = [])
    {
        $lyraAlias = $this->lyraAliasRepository->find($data->id);
        $this->lyraAliasRepository->remove($lyraAlias);
    }
}