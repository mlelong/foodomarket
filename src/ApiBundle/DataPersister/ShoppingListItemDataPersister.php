<?php


namespace ApiBundle\DataPersister;


use ApiBundle\Entity\ShoppingListItem;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use AppBundle\Repository\RestaurantStockItemRepository;

class ShoppingListItemDataPersister implements ContextAwareDataPersisterInterface
{
    /** @var RestaurantStockItemRepository */
    private $shoppingListItemRepository;

    public function __construct(RestaurantStockItemRepository $shoppingListItemRepository)
    {
        $this->shoppingListItemRepository = $shoppingListItemRepository;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof ShoppingListItem && $context['graphql_operation_name'] === 'delete';
    }

    /**
     * @inheritDoc
     */
    public function persist($data, array $context = [])
    {
    }

    /**
     * @param ShoppingListItem $data
     * @param array $context
     */
    public function remove($data, array $context = [])
    {
        $shoppingListModel = $this->shoppingListItemRepository->find($data->id);
        $this->shoppingListItemRepository->remove($shoppingListModel);
    }
}
