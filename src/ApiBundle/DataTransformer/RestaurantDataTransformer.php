<?php


namespace ApiBundle\DataTransformer;


use ApiBundle\Dto\Restaurant as RestaurantDto;
use ApiBundle\Entity\Restaurant;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;

class RestaurantDataTransformer implements DataTransformerInterface
{
    /**
     * @param Restaurant $object
     * @param string $to
     * @param array $context
     * @return RestaurantDto
     */
    public function transform($object, string $to, array $context = [])
    {
        /** @var RestaurantDto $to */
        $to = new $to();

        $to->id = "{$object->id}";
        $to->name = $object->name;
        $to->address = $object->address;

        return $to;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $data instanceof Restaurant && $to === RestaurantDto::class;
    }
}