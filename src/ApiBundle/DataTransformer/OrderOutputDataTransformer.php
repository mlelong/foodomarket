<?php


namespace ApiBundle\DataTransformer;


use ApiBundle\Dto\Order as OrderDto;
use ApiBundle\Entity\Order;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;

class OrderOutputDataTransformer implements DataTransformerInterface
{
    /**
     * @param Order $object
     * @param string $to
     * @param array $context
     * @return OrderDto
     */
    public function transform($object, string $to, array $context = [])
    {
        $dto = new OrderDto();

        $dto->supplier = $object->supplier;
        $dto->shippingAddress = $object->shippingAddress;
        $dto->total = $object->total;
        $dto->supplierId = $object->supplierId;
        $dto->date = $object->date;
        $dto->id = $object->id;
        $dto->items = $object->items;
        $dto->statusHistory = $object->statusHistory;
        $dto->currentStatus = $object->currentStatus;
        $dto->reference = $object->reference;
        $dto->paymentStatus = $object->paymentStatus;
        $dto->paymentUrl = $object->paymentUrl;
        $dto->paymentException = $object->paymentException;
        $dto->shippingCost = $object->shippingCost;

        return $dto;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $data instanceof Order && $to === OrderDto::class;
    }
}