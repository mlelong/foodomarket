<?php


namespace ApiBundle\DataTransformer;


use ApiBundle\Dto\Message as MessageDto;
use ApiBundle\Entity\Message;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;

class MessageOutputDataTransformer implements DataTransformerInterface
{
    /**
     * @param Message $object
     * @param string $to
     * @param array $context
     * @return MessageDto
     */
    public function transform($object, string $to, array $context = [])
    {
        $message = new MessageDto();

        $message->channel = $object->channel;
        $message->recipient = $object->recipient;
        $message->sender = $object->sender;
        $message->status = $object->status;
        $message->id = $object->id;
        $message->creationDate = $object->creationDate;
        $message->channelStatus = $object->channelStatus;
        $message->channelMessageId = $object->channelMessageId;
        $message->content = $object->content;
        $message->important = $object->important;
        $message->picture = $object->picture;

        return $message;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $data instanceof Message && $to === MessageDto::class;
    }
}
