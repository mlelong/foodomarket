<?php


namespace ApiBundle\DataTransformer;

use ApiBundle\Dto\SupplierInput;
use ApiBundle\Entity\Supplier;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;

class SupplierInputDataTransformer implements DataTransformerInterface
{
    /**
     * @param SupplierInput $object
     * @param string $to
     * @param array $context
     * @return Supplier|object
     */
    public function transform($object, string $to, array $context = [])
    {
        $supplier = new Supplier();

        $supplier->id = isset($context['object_to_populate']) ? $context['object_to_populate']->id : null;
        $supplier->name = $object->name;
        $supplier->email = $object->email;
        $supplier->telephone = $object->phone;
        $supplier->partner = false;
        $supplier->createdByUser = true;

        return $supplier;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Supplier) {
            return false;
        }

        return Supplier::class === $to && null !== ($context['input']['class'] ?? null) && SupplierInput::class === $context['input']['class'];
    }
}
