<?php


namespace ApiBundle\DataTransformer;


use ApiBundle\Dto\OrderItem as OrderItemDto;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use AppBundle\Entity\OrderItem;

class OrderItemOutputDataTransformer implements DataTransformerInterface
{
    /**
     * @inheritDoc
     */
    public function transform($object, string $to, array $context = [])
    {
        $a = 1;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $data instanceof OrderItem && $to === OrderItemDto::class;
    }
}
