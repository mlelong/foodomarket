<?php


namespace ApiBundle\Entity;


use ApiBundle\Resolver\UserItemResolver;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class User
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "read"=false,
 *          "write"=false,
 *     },
 *     graphql={
 *          "get"={
 *              "item_query"=UserItemResolver::class,
 *              "args"={}
 *          }
 *     }
 * )
 */
class User
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;

    /** @var array */
    public $stats;
}
