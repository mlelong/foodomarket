<?php

namespace ApiBundle\Entity\Traits;

trait ProductTrait
{
    /**
     * @var string|null
     */
    public $id;

    /** @var string */
    public $pid;

    /** @var string */
    public $supplierId;

    /** @var string */
    public $mainCategory;

    /** @var string */
    public $category;

    /** @var string */
    public $name;

    /** @var string */
    public $image;

    /** @var string */
    public $imageLarge;

    /** @var string */
    public $unit;

    /** @var float */
    public $price;

    /** @var string */
    public $explicitContent;

    /** @var array */
    public $priceDetail;

    /** @var bool */
    public $createdByUser;

    /** @var float */
    public $weight;
}
