<?php


namespace ApiBundle\Entity;


use ApiBundle\Dto\OrderStatus as OrderStatusDto;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class OrderStatus
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false,
 *          "output"=OrderStatusDto::class
 *     },
 *     graphql={
 *          "item_query"
 *     }
 * )
 */
class OrderStatus extends OrderStatusDto
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;
}
