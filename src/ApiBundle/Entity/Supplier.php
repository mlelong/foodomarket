<?php


namespace ApiBundle\Entity;

use ApiBundle\Dto\SupplierInput;
use ApiBundle\Resolver\SupplierCollectionResolver;
use ApiBundle\Resolver\SupplierMutationResolver;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class Supplier
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false,
 *          "write"=false
 *     },
 *     graphql={
 *          "item_query",
 *          "collection_query",
 *          "available"={
 *              "collection_query"=SupplierCollectionResolver::class
 *          },
 *          "restaurant"={
 *              "collection_query"=SupplierCollectionResolver::class,
 *              "args"={
 *                  "restaurantId"={"type"="String!", "description"="L'id du restaurant"}
 *              }
 *          },
 *          "create"={
 *              "input"=SupplierInput::class,
 *              "mutation"=SupplierMutationResolver::class
 *          },
 *          "edit"={
 *              "input"=SupplierInput::class,
 *              "mutation"=SupplierMutationResolver::class
 *          },
 *          "bind"={
 *              "mutation"=SupplierMutationResolver::class,
 *              "args"={
 *                  "supplierId"={"type"="String!", "description"="L'id du fournisseur à associer avec l'utilisateur qui émet la requête."}
 *              }
 *          },
 *     }
 * )
 */
class Supplier
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;

    /** @var string|null */
    public $name;

    /** @var string|null */
    public $email;

    /** @var string|null */
    public $telephone;

    /** @var bool */
    public $partner;

    /** @var bool */
    public $createdByUser;

    /** @var float */
    public $minOrderAmount;

    /** @var string */
    public $status;

    /** @var int|null */
    public $nbUnreadMessages;

    /** @var string|null */
    public $orderCutOff;

    /** @var array */
    public $openingDays;

    /** @var array */
    public $closingDays;

    /** @var float|null */
    public $deliveryCost;

    /** @var float */
    public $deliveryWeightUnit;

    /** @var array */
    public $categories;

    /** @var bool|null */
    public $chronofreshDelivery;
}
