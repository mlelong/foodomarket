<?php


namespace ApiBundle\Entity;

use ApiBundle\Dto\Message as MessageDto;
use ApiBundle\Dto\MessageInput;
use ApiBundle\Resolver\MessageCollectionResolver;
use ApiBundle\Resolver\MessageMutationResolver;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class Message
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "write"=false,
 *          "output"=MessageDto::class
 *     },
 *     graphql={
 *          "item_query",
 *          "get"={
 *              "collection_query"=MessageCollectionResolver::class,
 *              "args"={
 *                  "supplierId"={"type"="String!"},
 *                  "markAsRead"={"type"="Boolean"}
 *              }
 *          },
 *          "create"={
 *              "mutation"=MessageMutationResolver::class,
 *              "input"=MessageInput::class,
 *              "read"=false,
 *              "deserialize"=false,
 *          }
 *     }
 * )
 */
class Message extends MessageDto
{
    /**
     * @ApiProperty(identifier=true, required=false)
     * @var string|null
     */
    public $id;
}
