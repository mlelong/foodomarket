<?php


namespace ApiBundle\Entity;

use ApiBundle\Resolver\ShoppingListItemResolver;
use ApiBundle\Resolver\ShoppingListMutationResolver;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\Collection;

/**
 * Class ShoppingList
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false,
 *          "write"=false
 *     },
 *     graphql={
 *          "get"={
 *              "item_query"=ShoppingListItemResolver::class,
 *              "args"={
 *                  "supplierId"={"type"="String!", "description"="L'id du fournisseur"},
 *                  "restaurantId"={"type"="String!", "description"="L'id du restaurant"},
 *              }
 *          },
 *          "create"={
 *              "mutation"=ShoppingListMutationResolver::class,
 *              "deserialize"=false,
 *              "args"={
 *                  "supplierId"={"type"="String!", "description"="L'id du fournisseur"},
 *                  "restaurantId"={"type"="String!", "description"="L'id du restaurant"},
 *                  "listName"={"type"="String!", "description"="Le nom de la liste"},
 *              }
 *          }
 *     }
 * )
 */
class ShoppingList
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $supplierId;

    /** @var string */
    public $supplier;

    /**
     * @var Collection<ShoppingListItem>
     * @ApiSubresource
     */
    public $items;
}
