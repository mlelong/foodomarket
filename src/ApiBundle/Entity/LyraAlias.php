<?php


namespace ApiBundle\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;

/**
 * Class LyraAlias
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false,
 *     },
 *     graphql={
 *          "item_query",
 *          "collection_query",
 *          "delete",
*           "edit"={
 *              "args"={
 *                  "label"={"type"="String!"},
 *                  "id"={"type"="Int!"}
 *              },
 *          }
 *     }
 * )
 */
class LyraAlias
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;

    /** @var string */
    public $number;

    /** @var string */
    public $brand;

    /** @var DateTime */
    public $expirationDate;

    /** @var string|null */
    public $label;
}