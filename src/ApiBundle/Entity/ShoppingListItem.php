<?php


namespace ApiBundle\Entity;

use ApiBundle\Dto\Status;
use ApiBundle\Resolver\ShoppingListItemMutationResolver;
use ApiBundle\Entity\Traits\ProductTrait;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class ShoppingListItem
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false
 *     },
 *     graphql={
 *          "item_query",
 *          "create"={
 *              "mutation"=ShoppingListItemMutationResolver::class,
 *              "deserialize"=false,
 *              "args"={
 *                  "shoppingListId"={"type"="String!"},
 *                  "variantId"={"type"="String!"},
 *                  "unit"={"type"="String!"},
 *              }
 *          },
 *          "delete"
 *     }
 * )
 */
class ShoppingListItem
{
    use ProductTrait;

    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;

    /** @var string */
    public $vid;

    /** @var bool */
    public $deleted = false;
}
