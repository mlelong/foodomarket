<?php


namespace ApiBundle\Entity;

use ApiBundle\Entity\Traits\ProductTrait;
use ApiBundle\Resolver\ProductCollectionResolver;
use ApiBundle\Resolver\ProductItemResolver;
use ApiBundle\Resolver\ProductMutationResolver;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class Product
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "write"=false,
 *     },
 *     graphql={
 *          "supplier"={
 *              "collection_query"=ProductCollectionResolver::class,
 *              "args"={
 *                  "restaurantId"={"type"="String!", "description"="L'id du restaurant"},
 *                  "supplierId"={"type"="String!", "description"="L'id du grossiste"},
 *                  "offset"={"type"="Int!", "description"="Offset du premier produit"},
 *                  "limit"={"type"="Int", "description"="Nombre max de produits à retourner"},
 *              }
 *          },
 *          "create"={
 *              "mutation"=ProductMutationResolver::class,
 *              "args"={
 *                  "restaurantId"={"type"="String!", "description"="L'id du restaurant"},
 *                  "supplierId"={"type"="String!", "description"="L'id du grossiste"},
 *                  "name"={"type"="String!", "description"="Le nom du produit"},
 *                  "unit"={"type"="String!", "description"="Unité de commande pour le produit"},
 *                  "categoryId"={"type"="Int!", "description"="L'id de la catégorie principale"},
 *                  "code"={"type"="String", "description"="La référence du produit chez le grossiste"}
 *              }
 *          },
 *          "edit"={
 *              "mutation"=ProductMutationResolver::class,
 *              "args"={
 *                  "productId"={"type"="String!", "description"="L'id du produit"},
 *                  "restaurantId"={"type"="String!", "description"="L'id du restaurant"},
 *                  "supplierId"={"type"="String!", "description"="L'id du grossiste"},
 *                  "name"={"type"="String!", "description"="Le nom du produit"},
 *                  "unit"={"type"="String!", "description"="Unité de commande pour le produit"},
 *                  "categoryId"={"type"="Int!", "description"="L'id de la catégorie principale"},
 *                  "code"={"type"="String", "description"="La référence du produit chez le grossiste"},
 *              }
 *          },
 *          "get"={
 *              "item_query"=ProductItemResolver::class,
 *              "args"={
 *                  "productId"={"type"="String!", "desription"="L'id du produit"},
 *                  "restaurantId"={"type"="String!", "desription"="L'id du restaurant"},
 *                  "supplierId"={"type"="String!", "desription"="L'id du grossiste"}
 *              }
 *          }
 *     }
 * )
 */
class Product
{
    use ProductTrait;

    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;
}
