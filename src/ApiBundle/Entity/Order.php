<?php


namespace ApiBundle\Entity;

use ApiBundle\Dto\Order as OrderDto;
use ApiBundle\Dto\OrderInput;
use ApiBundle\Resolver\OrderCollectionResolver;
use ApiBundle\Resolver\OrderMutationResolver;
use ApiBundle\Resolver\OrderPaymentMutationResolver;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\Collection;

/**
 * Class Order
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "write"=false,
 *          "output"=OrderDto::class
 *     },
 *     graphql={
 *          "item_query",
 *          "collection_query",
 *          "getAll"={
 *              "collection_query"=OrderCollectionResolver::class,
 *              "args"={
 *                  "offset"={"type"="Int!", "description"="L'offset du dernier élement"},
 *                  "limit"={"type"="Int", "description"="Nombre de résultats à retourner"},
 *              }
 *          },
 *          "restaurant"={
 *              "collection_query"=OrderCollectionResolver::class,
 *              "args"={
 *                  "restaurantId"={"type"="String!", "description"="L'id du restaurant"}
 *              },
 *          },
 *          "create"={
 *              "mutation"=OrderMutationResolver::class,
 *              "input"=OrderInput::class,
 *              "deserialize"=false,
 *              "read"=false
 *          },
 *          "createWithPayment"={
 *              "mutation"=OrderPaymentMutationResolver::class,
 *              "input"=OrderInput::class,
 *              "deserialize"=false,
 *              "read"=false
 *          },
 *          "paymentShop"={
 *              "mutation"=OrderPaymentMutationResolver::class,
 *              "args"={
 *                  "shoppingCartId"={"type"="String!"},
*                   "orderLocation"={"type"="String!"},
 *                  "createAlias"={"type"="Boolean!"},
 *                  "useAlias"={"type"="Boolean!"},
 *                  "aliasId"={"type"="Int"}
 *              },
 *              "read"=false
 *          },
 *          "changeStatus"={
 *              "mutation"=OrderMutationResolver::class,
 *              "read"=false,
 *              "args"={
 *                  "id"={"type"="ID!"},
 *                  "status"={"type"="String!", "description"="Le nouveau statut"}
 *              }
 *          }
 *     }
 * )
 */
class Order extends OrderDto
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;

    /**
     * @var Collection<OrderItem>
     * @ApiSubresource
     */
    public $items;

    /**
     * @var Collection<OrderStatus>
     * @ApiSubresource
     */
    public $statusHistory;
}
