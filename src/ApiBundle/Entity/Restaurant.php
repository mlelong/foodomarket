<?php


namespace ApiBundle\Entity;

use ApiBundle\Dto\Restaurant as RestaurantDto;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiBundle\Resolver\RestaurantMutationResolver;

/**
 * Class Restaurant
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false,
 *          "write"=false,
 *          "output"=RestaurantDto::class,
 *     },
 *     graphql={
 *          "item_query",
 *          "collection_query",
 *          "create"={
 *              "mutation"=RestaurantMutationResolver::class
 *          },
 *          "edit"={
 *              "mutation"=RestaurantMutationResolver::class,
 *          },
 *     }
 * )
 */
class Restaurant extends RestaurantDto
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;
}
