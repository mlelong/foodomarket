<?php


namespace ApiBundle\Entity;


use ApiBundle\Dto\OrderItem as OrderItemDto;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class OrderItem
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false,
 *          "output"=OrderItemDto::class
 *     },
 *     graphql={
 *          "item_query"
 *     }
 * )
 */
class OrderItem extends OrderItemDto
{
    /**
     * @var string|null
     * @ApiProperty(identifier=true, required=false)
     */
    public $id;
}
