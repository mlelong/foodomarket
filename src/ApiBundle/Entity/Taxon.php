<?php


namespace ApiBundle\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Class Taxon
 * @package ApiBundle\Entity
 * @ApiResource(
 *     attributes={
 *          "pagination_enabled"=false,
 *          "write"=false
 *     },
 *     collectionOperations={},
 *     itemOperations={
 *          "get"={
 *              "path"="/taxon/{slug}",
 *              "requirements"={"slug"="\S+"},
 *          }
 *     },
 *     graphql={
 *          "item_query"
 *     }
 * )
 */
class Taxon
{
    /**
     * @ApiProperty(identifier=true)
     * @var string
     */
    public $slug;

    /**
     * @ApiProperty(identifier=false)
     * @var int
     */
    public $id;

    /** @var string */
    public $name;

    /** @var array */
    public $taxons;

    /** @var int */
    public $nbProducts;

    /** @var array */
    public $products;
}
