<?php


namespace ApiBundle\Type\Definition;


use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type as GraphQLType;

class MessageInputType extends InputObjectType
{
    public function __construct(array $config = [])
    {
        $config['name'] = 'MessageInput';
        $config['fields'] = [
            'supplierId' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'content' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'important' => ['type' => GraphQLType::boolean()],
            'picture' => ['type' => GraphQLType::string()],
        ];

        parent::__construct($config);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
