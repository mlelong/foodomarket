<?php


namespace ApiBundle\Type\Definition;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type as GraphQLType;

class StatusType extends ObjectType
{
    public function __construct(array $config = [])
    {
        $config['name'] = 'Status';
        $config['fields'] = [
            'code' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'message' => ['type' => GraphQLType::string()],
        ];

        parent::__construct($config);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
