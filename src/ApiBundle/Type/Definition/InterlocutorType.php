<?php


namespace ApiBundle\Type\Definition;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type as GraphQLType;

class InterlocutorType extends ObjectType
{
    public function __construct(array $config = [])
    {
        $config['name'] = 'Interlocutor';
        $config['fields'] = [
            'type' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'email' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'name' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
        ];

        parent::__construct($config);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
