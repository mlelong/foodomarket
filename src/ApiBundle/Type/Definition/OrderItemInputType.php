<?php


namespace ApiBundle\Type\Definition;


use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type as GraphQLType;

class OrderItemInputType extends InputObjectType
{
    public function __construct(array $config = [])
    {
        $config['name'] = 'OrderItemInput';
        $config['fields'] = [
            'variantId' => ['name' => 'variantId', 'type' => GraphQLType::nonNull(GraphQLType::string())],
            'quantity' => ['name' => 'quantity', 'type' => GraphQLType::nonNull(GraphQLType::float())],
            'unit' => ['name' => 'unit', 'type' => GraphQLType::nonNull(GraphQLType::string())],
        ];

        parent::__construct($config);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
