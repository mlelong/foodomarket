<?php


namespace ApiBundle\Type\Definition;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type as GraphQLType;

class AddressType extends ObjectType
{
    public function __construct(array $config = [])
    {
        $config['name'] = 'Address';
        $config['fields'] = [
            'number' => ['type' => GraphQLType::string()],
            'street' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'city' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'zip' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'country' => ['type' => GraphQLType::nonNull(GraphQLType::string())]
        ];

        parent::__construct($config);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
