<?php


namespace ApiBundle\Type\Definition;


use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type as GraphQLType;

class AddressInputType extends InputObjectType
{
    public function __construct(array $config = [])
    {
        $config['name'] = 'AddressInput';
        $config['fields'] = [
            'number' => ['type' => GraphQLType::string()],
            'street' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'city' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'zip' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
            'country' => ['type' => GraphQLType::nonNull(GraphQLType::string())],
        ];

        parent::__construct($config);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
