<?php


namespace ApiBundle\Type;


use ApiBundle\Dto\Address;
use ApiBundle\Dto\Interlocutor;
use ApiBundle\Dto\MessageInput;
use ApiBundle\Dto\OrderItemInput;
use ApiPlatform\Core\GraphQl\Type\TypeConverterInterface;
use GraphQL\Type\Definition\Type as GraphQLType;
use Symfony\Component\PropertyInfo\Type;

final class TypeConverter implements TypeConverterInterface
{
    /** @var TypeConverterInterface */
    private $defaultTypeConverter;

    public function __construct(TypeConverterInterface $defaultTypeConverter)
    {
        $this->defaultTypeConverter = $defaultTypeConverter;
    }

    /**
     * @inheritDoc
     */
    public function convertType(Type $type, bool $input, ?string $queryName, ?string $mutationName, string $resourceClass, string $rootResource, ?string $property, int $depth)
    {
        if ($resourceClass === Address::class) {
            return $input ? 'AddressInput' : 'Address';
        }

        if ($input && $resourceClass === OrderItemInput::class) {
            return 'OrderItemInput';
        }

        if ($resourceClass === MessageInput::class) {
            return 'MessageInput';
        }

        if ($resourceClass === Interlocutor::class) {
            return 'Interlocutor';
        }

        return $this->defaultTypeConverter->convertType($type, $input, $queryName, $mutationName, $resourceClass, $rootResource, $property, $depth);
    }

    /**
     * @inheritDoc
     */
    public function resolveType(string $type): ?GraphQLType
    {
        return $this->defaultTypeConverter->resolveType($type);
    }
}
