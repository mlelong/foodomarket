<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\OrderStatus;
use ApiBundle\Resolver\Traits\OrderStatusModelToPopo;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Entity\OrderStatus as OrderStatusModel;
use AppBundle\Repository\OrderStatusRepository;

class OrderStatusItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    use OrderStatusModelToPopo;

    /** @var OrderStatusRepository */
    private $orderStatusRepository;

    /**
     * OrderStatusItemDataProvider constructor.
     * @param OrderStatusRepository $orderStatusRepository
     */
    public function __construct(OrderStatusRepository $orderStatusRepository)
    {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var OrderStatusModel $item */
        $item = $this->orderStatusRepository->find($id);

        return $this->model2popo($item);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === OrderStatus::class;
    }
}
