<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Order;
use ApiPlatform\Core\DataProvider\ArrayPaginator;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Order as OrderModel;
use ApiBundle\Resolver\Traits\OrderModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Repository\OrderRepository;

class OrderCollectionDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    use TokenAwareTrait,
        OrderModelToPopo;

    /** @var OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritDoc
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $customers = $this->getCustomers();
        /** @var OrderModel[] $orders */
        $orders = $this->orderRepository->createQueryBuilder('o')
            ->andWhere('o.customer IN (:customer)')
            ->andWhere('o.shoppingCart IS NOT NULL')
            ->addOrderBy('o.createdAt', 'DESC')
            ->setParameter('customer', $customers)
//            ->setFirstResult($offset)
//            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;

        $ret = [];

        foreach ($orders as $order) {
            $ret[] = $this->model2Popo($order);
        }

        return new ArrayPaginator($ret, 0, count($ret));
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === Order::class && $operationName === 'collection_query';
    }

    /**
     * @return Customer[]
     */
    private function getCustomers()
    {
        $customers = [];

        foreach ($this->getUser()->getCustomer()->getProspect()->getCustomers() as $customer) {
            $customers[] = $customer;
        }

        return $customers;
    }
}
