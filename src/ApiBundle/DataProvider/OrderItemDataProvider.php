<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Order;
use ApiBundle\Resolver\Traits\OrderModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Entity\Order as OrderModel;
use AppBundle\Repository\OrderRepository;

class OrderItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    use TokenAwareTrait,
        OrderModelToPopo;

    /** @var OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var OrderModel $orderModel */
        $orderModel = $this->orderRepository->find($id);

        return $this->model2Popo($orderModel);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === Order::class;
    }
}
