<?php


namespace ApiBundle\DataProvider;


use AppBundle\Entity\OrderItem as OrderItemModel;
use ApiBundle\Entity\OrderItem;
use ApiBundle\Resolver\Traits\OrderItemModelToPopo;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface;
use Sylius\Component\Order\Repository\OrderItemRepositoryInterface;

class OrderItemCollectionDataProvider implements RestrictedDataProviderInterface, SubresourceDataProviderInterface
{
    use OrderItemModelToPopo;

    /** @var OrderItemRepositoryInterface */
    private $orderItemRepository;

    /**
     * OrderItemItemDataProvider constructor.
     * @param OrderItemRepositoryInterface $orderItemRepository
     */
    public function __construct(OrderItemRepositoryInterface $orderItemRepository)
    {
        $this->orderItemRepository = $orderItemRepository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === OrderItem::class;
    }

    /**
     * @inheritDoc
     */
    public function getSubresource(string $resourceClass, array $identifiers, array $context, string $operationName = null)
    {
        /** @var OrderItemModel $items */
        $items = $this->orderItemRepository->findBy(['order' => $identifiers['id']]);
        $ret = [];

        foreach ($items as $item) {
            $ret[] = $this->model2Popo($item);
        }

        return $ret;
    }
}
