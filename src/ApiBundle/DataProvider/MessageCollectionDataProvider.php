<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Message;
use ApiBundle\Resolver\Traits\MessageModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ArrayPaginator;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Repository\MessageRepository;

class MessageCollectionDataProvider implements RestrictedDataProviderInterface, ContextAwareCollectionDataProviderInterface
{
    use TokenAwareTrait,
        MessageModelToPopo;

    /** @var MessageRepository */
    private $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @inheritDoc
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $messages = [];
        $supplierId = $context['filters']['supplierId'];
        $messageModels = $this->messageRepository->getMessages($supplierId, $this->getUser()->getCustomer()->getProspect());

        foreach ($messageModels as $messageModel) {
            $messages[] = $this->model2Popo($messageModel);
        }

        return new ArrayPaginator($messages, 0, count($messages));
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === Message::class && $operationName === 'get';
    }
}
