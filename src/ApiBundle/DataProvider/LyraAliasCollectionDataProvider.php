<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\LyraAlias;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Repository\LyraAliasRepository;
use AppBundle\Repository\MessageRepository;

class LyraAliasCollectionDataProvider implements RestrictedDataProviderInterface, ContextAwareCollectionDataProviderInterface
{
    use TokenAwareTrait;

    /** @var MessageRepository */
    private $lyraAliasRepository;

    public function __construct(LyraAliasRepository $lyraAliasRepository)
    {
        $this->lyraAliasRepository = $lyraAliasRepository;
    }

    /**
     * @inheritDoc
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $prospect = $this->getUser()->getCustomer()->getProspect();
        $aliases = $this->lyraAliasRepository->findBy(['prospect' => $prospect]);

        $aliasesCollection = [];
        foreach ($aliases as $alias) {
            $popo = new LyraAlias();
            $popo->id = $alias->getId();
            $popo->number = $alias->getNumber();
            $popo->brand = $alias->getBrand();
            $popo->expirationDate = $alias->getExpirationDate();
            $popo->label = $alias->getLabel();
            $aliasesCollection[] = $popo;
        }

        return $aliasesCollection;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === LyraAlias::class;
    }
}