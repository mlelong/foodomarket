<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Restaurant;
use ApiBundle\Resolver\Traits\RestaurantModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;

class RestaurantItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    use TokenAwareTrait,
        RestaurantModelToPopo;

    /**
     * @inheritDoc
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        if (!$user = $this->getUser()) {
            return null;
        }

        foreach ($user->getCustomer()->getRestaurants() as $restaurant) {
            if ($restaurant->getId() == $id) {
                return $this->model2Popo($restaurant);
            }
        }

        return null;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Restaurant::class === $resourceClass;
    }
}
