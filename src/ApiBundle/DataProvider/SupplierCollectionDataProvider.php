<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Supplier;
use ApiBundle\Resolver\Traits\SupplierModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Repository\SupplierRepository;

class SupplierCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    use TokenAwareTrait,
        SupplierModelToPopo;

    /** @var SupplierRepository */
    private $supplierRepository;

    /**
     * SupplierCollectionResolver constructor.
     * @param SupplierRepository $supplierRepository
     */
    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * @inheritDoc
     */
    public function getCollection(string $resourceClass, string $operationName = null)
    {
        $ret = [];
        $qb = $this->supplierRepository->createQueryBuilder('s');
        /** @var SupplierModel[] $suppliers */
        $suppliers = $qb
            ->where('s.enabled = 0')
            ->andWhere('s.facadeSupplier IS NOT NULL')
            ->andWhere($qb->expr()->orX('s.partner = 1', 's.createdByUser = 0'))
            ->orderBy('s.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        foreach ($suppliers as $supplier) {
            $ret[$supplier->getFacadeSupplier()->getId()] = $this->model2Popo($supplier->getFacadeSupplier(), $this->getUser()->getCustomer()->getProspect());
        }

        return $ret;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === Supplier::class && $operationName === 'collection_query';
    }
}
