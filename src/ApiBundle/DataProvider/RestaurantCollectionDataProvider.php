<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Restaurant;
use ApiBundle\Resolver\Traits\RestaurantModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;


class RestaurantCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    use TokenAwareTrait,
        RestaurantModelToPopo;

    /**
     * @inheritDoc
     */
    public function getCollection(string $resourceClass, string $operationName = null)
    {
        if (!$user = $this->getUser()) {
            return null;
        }

        $restaurants = [];

        foreach ($user->getCustomer()->getRestaurants() as $restaurant) {
            $restaurants[$restaurant->getId()] = $this->model2Popo($restaurant);
        }

        return $restaurants;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Restaurant::class === $resourceClass;
    }
}
