<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Supplier;
use ApiBundle\Resolver\Traits\SupplierModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Entity\Supplier as SupplierModel;
use AppBundle\Repository\SupplierRepository;

class SupplierItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    use TokenAwareTrait,
        SupplierModelToPopo;

    /** @var SupplierRepository */
    private $supplierRepository;

    /**
     * SupplierCollectionResolver constructor.
     * @param SupplierRepository $supplierRepository
     */
    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var SupplierModel $supplier */
        $supplier = $this->supplierRepository->find($id);

        return $this->model2Popo($supplier, $this->getUser()->getCustomer()->getProspect(), true);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === Supplier::class;
    }
}
