<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\ShoppingListItem;
use ApiBundle\Resolver\Traits\ShoppingListItemModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Repository\RestaurantStockItemRepository;

class ShoppingListItemItemDataProvider implements RestrictedDataProviderInterface, ItemDataProviderInterface
{
    use TokenAwareTrait,
        ShoppingListItemModelToPopo;

    /** @var RestaurantStockItemRepository */
    private $shoppingListItemRepository;

    public function __construct(RestaurantStockItemRepository $shoppingListItemRepository)
    {
        $this->shoppingListItemRepository = $shoppingListItemRepository;
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var RestaurantStockItem $shoppingListItemModel */
        $shoppingListItemModel = $this->shoppingListItemRepository->find($id);

        if ($shoppingListItemModel === null) {
            return null;
        }

        return $this->model2Popo($shoppingListItemModel);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === ShoppingListItem::class;
    }
}
