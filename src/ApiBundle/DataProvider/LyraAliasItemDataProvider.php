<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\LyraAlias;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Repository\LyraAliasRepository;

class LyraAliasItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    use TokenAwareTrait;

    /** @var LyraAliasRepository */
    private $lyraAliasRepository;

    public function __construct(LyraAliasRepository $lyraAliasRepository)
    {
        $this->lyraAliasRepository = $lyraAliasRepository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === LyraAlias::class;
    }

    /**
     * @inheritDoc
     */
    public function getItem(
        string $resourceClass, $id, string $operationName = null, array $context = []
    ) {

        $prospect = $this->getUser()->getCustomer()->getProspect();
        $alias = $this->lyraAliasRepository->find($id);

        if (!$alias) {
            return null;
        }

        if($alias->getProspect() !== $prospect)
        {
            return null;
        }

        $popo = new LyraAlias();
        $popo->id = $alias->getId();
        $popo->number = $alias->getNumber();
        $popo->brand = $alias->getBrand();
        $popo->expirationDate = $alias->getExpirationDate();
        $popo->label = $alias->getLabel();

        return $popo;
    }
}