<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\OrderStatus;
use ApiBundle\Resolver\Traits\OrderStatusModelToPopo;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface;
use AppBundle\Entity\OrderStatus as OrderStatusModel;
use AppBundle\Repository\OrderStatusRepository;

class OrderStatusCollectionDataProvider implements RestrictedDataProviderInterface, SubresourceDataProviderInterface
{
    use OrderStatusModelToPopo;

    /** @var OrderStatusRepository */
    private $orderStatusRepository;

    /**
     * OrderItemItemDataProvider constructor.
     * @param OrderStatusRepository $orderStatusRepository
     */
    public function __construct(OrderStatusRepository $orderStatusRepository)
    {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === OrderStatus::class;
    }

    /**
     * @inheritDoc
     */
    public function getSubresource(string $resourceClass, array $identifiers, array $context, string $operationName = null)
    {
        /** @var OrderStatusModel $items */
        $items = $this->orderStatusRepository->findBy(['order' => $identifiers['id']]);
        $ret = [];

        foreach ($items as $item) {
            $ret[] = $this->model2Popo($item);
        }

        return $ret;
    }
}
