<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\ShoppingListItem;
use ApiBundle\Resolver\Traits\ShoppingListItemModelToPopo;
use ApiBundle\Traits\TokenAwareTrait;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SubresourceDataProviderInterface;
use AppBundle\Entity\RestaurantStockItem;
use AppBundle\Repository\RestaurantStockItemRepository;

class ShoppingListItemCollectionDataProvider implements RestrictedDataProviderInterface, SubresourceDataProviderInterface
{
    use TokenAwareTrait,
        ShoppingListItemModelToPopo;

    /** @var RestaurantStockItemRepository */
    private $shoppingListItemRepository;

    public function __construct(RestaurantStockItemRepository $shoppingListItemRepository)
    {
        $this->shoppingListItemRepository = $shoppingListItemRepository;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === ShoppingListItem::class;
    }

    /**
     * @inheritDoc
     */
    public function getSubresource(string $resourceClass, array $identifiers, array $context, string $operationName = null)
    {
        /** @var RestaurantStockItem[] $shoppingListItems */
        $shoppingListItems = $this->shoppingListItemRepository->findBy(['restaurantStock' => $identifiers['id']]);
        $items = [];

        foreach ($shoppingListItems as $shoppingListItem) {
            if ($shoppingListItem->getSupplier()->getId() !== $shoppingListItem->getRestaurantStock()->getSupplier()->getId()) {
                continue ;
            }

            $items[] = $this->model2Popo($shoppingListItem);
        }

        return $items;
    }
}
