<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\OrderItem;
use ApiBundle\Resolver\Traits\OrderItemModelToPopo;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Entity\OrderItem as OrderItemModel;
use Sylius\Component\Order\Repository\OrderItemRepositoryInterface;

class OrderItemItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    use OrderItemModelToPopo;

    /** @var OrderItemRepositoryInterface */
    private $orderItemRepository;

    /**
     * OrderItemItemDataProvider constructor.
     * @param OrderItemRepositoryInterface $orderItemRepository
     */
    public function __construct(OrderItemRepositoryInterface $orderItemRepository)
    {
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var OrderItemModel $item */
        $item = $this->orderItemRepository->find($id);

        return $this->model2popo($item);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === OrderItem::class;
    }
}
