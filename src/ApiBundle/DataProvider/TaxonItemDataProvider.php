<?php


namespace ApiBundle\DataProvider;


use ApiBundle\Entity\Taxon;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ProductTaxonRepository;
use AppBundle\Repository\SupplierRepository;
use AppBundle\Repository\TaxonRepository;
use Sylius\Component\Core\Model\Product;
use Sylius\Component\Core\Model\Taxon as TaxonModel;

class TaxonItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var TaxonRepository
     */
    private $taxonRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var SupplierRepository
     */
    private $supplierRepository;

    /**
     * @var ProductTaxonRepository
     */
    private $productTaxonRepository;

    public function __construct(ProductTaxonRepository $productTaxonRepository, TaxonRepository $taxonRepository, ProductRepository $productRepository, SupplierRepository $supplierRepository)
    {
        $this->productTaxonRepository = $productTaxonRepository;
        $this->taxonRepository = $taxonRepository;
        $this->productRepository = $productRepository;
        $this->supplierRepository = $supplierRepository;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        /** @var TaxonModel $taxonModel */
        $taxonModel = $this->taxonRepository->find($id);
        $taxon = new Taxon();

        $taxon->id = $taxonModel->getId();
        $taxon->name = $taxonModel->getName();
        $taxon->slug = $taxonModel->getSlug();

        $products = [];
        /** @var Product[] $productModels */
        $productModels = $this->productRepository->findBy(['mainTaxon' => $taxonModel]);

        foreach ($productModels as $productModel) {
            $product = [
                'id' => $productModel->getId(),
                'name' => $productModel->getName(),
                'slug' => $productModel->getSlug(),
            ];

            $products[] = $product;
        }

        $taxon->products = $products;

        foreach ($taxonModel->getChildren() as $childModel) {
            $child = new Taxon();

            $child->id = $childModel->getId();
            $child->name = $childModel->getName();
            $child->slug = $childModel->getSlug();

            $taxon->taxons[] = $child;
        }

        return $taxon;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return $resourceClass === Taxon::class;
    }
}