<?php

namespace ApiBundle\Traits;

use AppBundle\Entity\AdminUser;
use AppBundle\Entity\ShopUser;
use AppBundle\Repository\ShopUserRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

trait TokenAwareTrait
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var SessionInterface */
    private $session;

    /** @var ShopUserRepository */
    private $shopUserRepository;

    /**
     * @required
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage): void
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @required
     * @param SessionInterface $session
     */
    public function setSession(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @required
     * @param ShopUserRepository $shopUserRepository
     */
    public function setShopUserRepository(ShopUserRepository $shopUserRepository): void
    {
        $this->shopUserRepository = $shopUserRepository;
    }

    protected function getUser(): ?ShopUser
    {
        if (!$token = $this->tokenStorage->getToken()) {
            return null;
        }

        $user = $token->getUser();

        if (($switch = $this->session->get('_foodo_switch')) !== null && $user instanceof AdminUser) {
            /** @noinspection PhpIncompatibleReturnTypeInspection */
            return $this->shopUserRepository->findOneByEmail($switch);
        }

        return $user instanceof ShopUser ? $user : null;
    }
}
