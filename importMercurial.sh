#!/bin/bash

TODO=NONE

if [ $# = 1 ]; then
	TODO=$1
fi

AUTO_MATCHING=0

if [ $TODO == FOODOMARKET_LALAUZE ]; then
    DIRECTORY=auto_importation/foodomarket/lalauze/
    SUPPLIER=52
    RESTAURANT=4
    MERCURIALE_ID=520
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_MEATEAM ]; then
    DIRECTORY=auto_importation/foodomarket/meateam/
    SUPPLIER=50
    RESTAURANT=4
    MERCURIALE_ID=500
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_VERGER_DE_SOUAMA ]; then
    DIRECTORY=auto_importation/foodomarket/verger_de_souama/
    SUPPLIER=45
    RESTAURANT=4
    MERCURIALE_ID=450
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_GRAND_AUGUSTE ]; then
    DIRECTORY=auto_importation/foodomarket/grand_auguste/
    SUPPLIER=43
    RESTAURANT=4
    MERCURIALE_ID=430
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_DOMAFRAIS ]; then
    DIRECTORY=auto_importation/foodomarket/domafrais/
    SUPPLIER=39
    RESTAURANT=4
    MERCURIALE_ID=390
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_WINESNROSES_CAVE ]; then
    DIRECTORY=auto_importation/foodomarket/winesnroses_cave/
    SUPPLIER=35
    RESTAURANT=4
    MERCURIALE_ID=350
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_JACOB ]; then
    DIRECTORY=auto_importation/foodomarket/jacob/
    SUPPLIER=37
    RESTAURANT=4
    MERCURIALE_ID=37
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_JACOB_PARTICULIER ]; then
    DIRECTORY=auto_importation/foodomarket/jacob-particulier/
    SUPPLIER=80
    RESTAURANT=4
    MERCURIALE_ID=80
    AUTO_MATCHING=0
fi

#if [ $TODO == FOODOMARKET_JACOB_EPICERIE ]; then
#    DIRECTORY=auto_importation/foodomarket/jacob_epicerie/
#    SUPPLIER=37
#    RESTAURANT=4
#    MERCURIALE_ID=370
#    AUTO_MATCHING=0
#fi
#
#if [ $TODO == FOODOMARKET_JACOB_BOUCHERIE ]; then
#    DIRECTORY=auto_importation/foodomarket/jacob_boucherie/
#    SUPPLIER=37
#    RESTAURANT=4
#    MERCURIALE_ID=371
#    AUTO_MATCHING=0
#fi
#
#if [ $TODO == FOODOMARKET_JACOB_CREMERIE ]; then
#    DIRECTORY=auto_importation/foodomarket/jacob_cremerie/
#    SUPPLIER=37
#    RESTAURANT=4
#    MERCURIALE_ID=372
#    AUTO_MATCHING=0
#fi

if [ $TODO == FOODOMARKET_LAURANCEPRIMEUR ]; then
    DIRECTORY=auto_importation/foodomarket/laurance-primeur/
    SUPPLIER=29
    RESTAURANT=4
    MERCURIALE_ID=290
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_VERGERETOILE ]; then
    DIRECTORY=auto_importation/foodomarket/verger-etoile/
    SUPPLIER=31
    RESTAURANT=4
    MERCURIALE_ID=300
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_REYNAUD_SALAISON ]; then
    DIRECTORY=auto_importation/foodomarket/reynaud_salaison/
    SUPPLIER=24
    RESTAURANT=4
    MERCURIALE_ID=242
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_REYNAUD_COQUILLAGE ]; then
    DIRECTORY=auto_importation/foodomarket/reynaud_coquillage/
    SUPPLIER=24
    RESTAURANT=4
    MERCURIALE_ID=241
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_REYNAUD_POISSON ]; then
    DIRECTORY=auto_importation/foodomarket/reynaud_poisson/
    SUPPLIER=24
    RESTAURANT=4
    MERCURIALE_ID=240
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_JOCEANE ]; then
    DIRECTORY=auto_importation/foodomarket/joceane/
    SUPPLIER=26
    RESTAURANT=4
    MERCURIALE_ID=260
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_DAUMESNIL_FRUITSLEGUMES ]; then
    DIRECTORY=auto_importation/foodomarket/daumesnil_fruitslegumes/
    SUPPLIER=22
    RESTAURANT=4
    MERCURIALE_ID=220
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_DAUMESNIL_CREMERIE ]; then
    DIRECTORY=auto_importation/foodomarket/daumesnil_cremerie/
    SUPPLIER=22
    RESTAURANT=4
    MERCURIALE_ID=221
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_HP5 ]; then
    DIRECTORY=auto_importation/foodomarket/halles-prestige-sp/
    SUPPLIER=23
    RESTAURANT=4
    MERCURIALE_ID=182
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_HP3 ]; then
    DIRECTORY=auto_importation/foodomarket/halles-prestige-3/
    SUPPLIER=17
    RESTAURANT=4
    MERCURIALE_ID=181
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_HP3_PARTICULIER ]; then
    DIRECTORY=auto_importation/foodomarket/halles-prestige-3-particulier/
    SUPPLIER=82
    RESTAURANT=4
    MERCURIALE_ID=82
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_HP4 ]; then
    DIRECTORY=auto_importation/foodomarket/halles-prestige-4/
    SUPPLIER=18
    RESTAURANT=4
    MERCURIALE_ID=180
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_DEMARNE ]; then
    DIRECTORY=auto_importation/foodomarket/demarne/
    SUPPLIER=19
    RESTAURANT=4
    MERCURIALE_ID=190
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_ALAZARD ]; then
    DIRECTORY=auto_importation/foodomarket/alazard/
    SUPPLIER=21
    RESTAURANT=4
    MERCURIALE_ID=210
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_PEYRAUD ]; then
    DIRECTORY=auto_importation/foodomarket/abc-peyraud/
    SUPPLIER=20
    RESTAURANT=4
    MERCURIALE_ID=200
    AUTO_MATCHING=0
fi

if [ $TODO == VITALIS_MANDAR ]; then
    DIRECTORY=auto_importation/vitalis/mandar/
    SUPPLIER=6
    RESTAURANT=1
    MERCURIALE_ID=6
    AUTO_MATCHING=0
fi

if [ $TODO == VITALIS_DEMARNE ]; then
    DIRECTORY=auto_importation/vitalis/demarne/
    SUPPLIER=1
    RESTAURANT=1
    MERCURIALE_ID=19
    AUTO_MATCHING=0
fi

if [ $TODO == VITALIS_HP ]; then
    DIRECTORY=auto_importation/vitalis/halles-prestige/
    SUPPLIER=11
    RESTAURANT=1
    MERCURIALE_ID=17
    AUTO_MATCHING=0
fi

if [ $TODO == VITALIS_TERRAZUR ]; then
    DIRECTORY=auto_importation/vitalis/terrazur/
    SUPPLIER=15
    RESTAURANT=1
    MERCURIALE_ID=15
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_BEAUGRAIN ]; then
    DIRECTORY=auto_importation/foodomarket/beaugrain/
    SUPPLIER=54
    RESTAURANT=4
    MERCURIALE_ID=54
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_MTC ]; then
    DIRECTORY=auto_importation/foodomarket/mtc/
    SUPPLIER=56
    RESTAURANT=4
    MERCURIALE_ID=56
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_SUN7FRUITS ]; then
    DIRECTORY=auto_importation/foodomarket/sun7fruits/
    SUPPLIER=58
    RESTAURANT=4
    MERCURIALE_ID=58
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_SAINFRUIT_FRUITSLEGUMES ]; then
    DIRECTORY=auto_importation/foodomarket/sainfruit_fruitslegumes/
    SUPPLIER=76
    RESTAURANT=4
    MERCURIALE_ID=760
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_SAINFRUIT_MODE_DAUMESNIL ]; then
    DIRECTORY=auto_importation/foodomarket/sainfruit_mode_daumesnil/
    SUPPLIER=62
    RESTAURANT=4
    MERCURIALE_ID=620
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_TOPATLANTIQUE ]; then
    DIRECTORY=auto_importation/foodomarket/topatlantique/
    SUPPLIER=64
    RESTAURANT=4
    MERCURIALE_ID=640
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_ESTIVEAU ]; then
    DIRECTORY=auto_importation/foodomarket/estiveau/
    SUPPLIER=68
    RESTAURANT=4
    MERCURIALE_ID=680
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_GARONNE_FRUITS ]; then
    DIRECTORY=auto_importation/foodomarket/garonne_fruits/
    SUPPLIER=69
    RESTAURANT=4
    MERCURIALE_ID=690
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_PRIMADOUR ]; then
    DIRECTORY=auto_importation/foodomarket/primadour/
    SUPPLIER=70
    RESTAURANT=4
    MERCURIALE_ID=700
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_SOBOMAR ]; then
    DIRECTORY=auto_importation/foodomarket/sobomar/
    SUPPLIER=72
    RESTAURANT=4
    MERCURIALE_ID=720
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_AQUITAINE_PRIMEURS ]; then
    DIRECTORY=auto_importation/foodomarket/aquitaine_primeurs/
    SUPPLIER=73
    RESTAURANT=4
    MERCURIALE_ID=730
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_BRUYERRE ]; then
    DIRECTORY=auto_importation/foodomarket/bruyerre/
    SUPPLIER=88
    RESTAURANT=4
    MERCURIALE_ID=880
    AUTO_MATCHING=0
fi

if [ $TODO == FOODOMARKET_CENTRALE_DU_FRAIS ]; then
    DIRECTORY=auto_importation/foodomarket/centrale-du-frais/
    SUPPLIER=90
    RESTAURANT=4
    MERCURIALE_ID=900
    AUTO_MATCHING=0
fi

if [ -z $DIRECTORY ]; then
    echo "No action for $TODO"
    exit 0
fi

echo ''
date +%Y-%m-%d' '%H:%M:%S
echo Process directory $DIRECTORY
echo Cmd to use : time php bin/console app:document:import $SUPPLIER $RESTAURANT INSERT_FILE_HERE $MERCURIALE_ID 1 $AUTO_MATCHING --env=prod

for FILE in mercurials/"$DIRECTORY"*
do

    if [[ $FILE != *"*" ]]; then
        # remove the "mercurials/"
        FILETOPROCESS="${FILE/mercurials\//}"

        echo File to process : $FILETOPROCESS

        # exec the mercurial import
        echo Executing now : time php bin/console app:document:import $SUPPLIER $RESTAURANT "$FILETOPROCESS" $MERCURIALE_ID 1 $AUTO_MATCHING --env=prod
        time php bin/console app:document:import $SUPPLIER $RESTAURANT  "$FILETOPROCESS" $MERCURIALE_ID 1 $AUTO_MATCHING --env=prod

        # remove the file
        unlink "$FILE"
        echo Delete file : $FILE

    else
        echo No file to import
    fi
done

exit 0

