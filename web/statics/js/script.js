// Semantic UI related error markup
// function markFieldAsError(field) {
//     field.closest('.field').addClass('error');
// }
//
// function markFieldAsSuccess(field) {
//     field.closest('.field').removeClass('error');
// }

// Bootstrap MD related error markup
function markFieldAsError(field) {
    field.removeClass('is-valid').addClass('is-invalid');
}

function markFieldAsSuccess(field) {
    field.removeClass('is-invalid').addClass('is-valid');
}

function validate() {
    var valid = true;
    var phoneReg = /^((\+)33|0)[1-9](\d{2}){4}$/;
    var emailReg = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)])/;
    var fields = {
        'full_name': $('#full_name'),
        'full_address': $('#autocomplete'),
        'number': $('#street_number'),
        'street': $('#route'),
        'city': $('#locality'),
        'zip': $('#postal_code'),
        'country': $('#country'),
        'email': $('#email'),
        'mobile': $('#mobile'),
        'restaurant_name': $('#restaurant_name'),
    };

    $.each(fields, function (fieldName, field) {
        var fieldValue = field.val();
        var isValid = true;

        // Required
        if (field.is('[required]') && fieldValue.length === 0) {
            isValid = false;
        } else if (fieldName === 'password_confirm' && $('#password').val() !== fieldValue) {
            isValid = false;
        } else if ($.inArray(fieldName, ['street', 'number', 'city', 'zip', 'country']) !== -1) {
            // if (fieldValue.length === 0) {
            //     markFieldAsError($('#autocomplete'));
            //     valid = false;
            // } else {
            //     markFieldAsSuccess($('#autocomplete'));
            // }

            return;
        } else {
            // Not required or length > 0, do the check
            switch (fieldName) {
                case 'mobile':
                    isValid = phoneReg.test(fieldValue);
                    break;
                case 'email':
                    isValid = emailReg.test(fieldValue);
                    break;
            }
        }

        if (!isValid) {
            markFieldAsError(field);
            valid = false;
        } else {
            markFieldAsSuccess(field);
        }
    });

    if (valid) {
        fbq('track', 'CompleteRegistration', {'currency': 'EUR', 'value': 'professional'});
        amplitude.getInstance().logEvent('registration complete');
    }

    return valid;
}

function isProfessional() {
    document.cookie = 'redirectModalOpened=true';
}

$(document).ready(function () {
    $("#redirect-modal").modal('show');
});
